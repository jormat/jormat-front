/**
*  Constantes Globales
*/

(function() {
    'use strict';

    angular
        .module('app')
        .value('serviceURL', 'http://localhost:9000/api/u_forecast_api')
        .value('PLN_VIEWS_FOLDER', '../apps/u-migration-ui/components/')
        .value('PLN_CSS_FOLDER', '..../apps/u-migration-ui/css/')
})();
