'use strict';
 
var path = require('path'),
    gulp = require('gulp'),
    webpack = require('gulp-webpack-build'),
    webpackstream = require('webpack-stream');
    var mainBowerFiles = require('main-bower-files');
 
var src = './src',
    dest = './dist1',
    webpackOptions = {
        debug: true,
        devtool: '#source-map',
        watchDelay: 200
    },
    webpackConfig = {
        useMemoryFs: true,
        progress: true
    },
    CONFIG_FILENAME = webpack.config.CONFIG_FILENAME;
    console.log(CONFIG_FILENAME);

 gulp.task('default', function() {
  return gulp.src('src/app.js')
   .pipe(webpackstream({
        watch: true,
        module: {
          loaders: [
            { test: /\.css$/, loader: 'style!css' },
          ],
        },
      }))
    .pipe(webpackstream( require('./webpack.config.js') ))
    .pipe(gulp.dest('dist/'));
});
 

// gulp.task('bundle', function() {
//   return gulp.src('bower_components/**/**/*.css') // Gets all files ending with .scss in app/scss and children dirs
//     .pipe(css())
//     .pipe(gulp.dest('css'))
// })

var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat');



// // gulp.task('js', function () {
// //    return gulp.src('bower_components/**/**/*.js')
// //       .pipe(jshint())
// //       .pipe(jshint.reporter('default'))
// //       .pipe(uglify())
// //       .pipe(concat('app.js'))
// //       .pipe(gulp.dest('css'));
// // });
var inject = require('gulp-inject');
      function buildJS() {
// gulp.task('scripts1', function() {
        // gulp.src(['./css/**/*.js','./css/**/**/*.js','!css/**/**/*.min.js','!css/**/*.min.js'])
        //   .pipe(concat('app.js'))
        //   .pipe(uglify())
        //   .pipe(gulp.dest('./css/'))

         // gulp.src('src/public/index.html')
         //  .pipe(inject(gulp.src(['../bower_components/angular-material/angular-material.js']), {
         //  starttag: '/* inject:json */',
         //  endtag: '/* endinject */'
         //    transform: function (filepath, file, i, length) {
         //    return file.contents.toString('utf8');
         //    }
         //  }))
         //  .pipe(gulp.dest("src/public/"));


  // gulp.src('./src/public/index.html')
  // .pipe(inject(gulp.src( './src/*.js', {read: false}), 
  //   {name: 'head'},{
  //               transform: function (filepath, file, i, length) { 
  //                 // if (filepath.slice(-5) === '.docx') {
  //                 return '<li><a href="' + filepath + '">' + filepath + '</a></li>';
  //                 // }
  //                 // Use the default transform as fallback: 
  //                 // return inject.transform.apply(inject.transform, arguments);
  //              } 
  //         }))
  // .pipe(gulp.dest('src/public/'));


gulp.task('inject', function() {
    gulp.src('./src/public/index.html')
        .pipe(inject(
            gulp.src(['./src/*.js'], {read: false }),
            {
                transform: function (filePath, file, i, length) {
                    var newPath = filePath.replace('/Content/js/', '');
                    console.log('inject script = '+ newPath);
                    return '<script src="/static/' + newPath  + '"></script>';
                }
            })
        )
        .pipe(gulp.dest('src/public/'));
});

  }

// });
 
    // gulp.task('scripts', function() {
      function buildBOWER() {
          return gulp.src(mainBowerFiles({
            paths: {
            bowerDirectory: 'bower_components',
            bowerrc: '.bowerrc',
            bowerJson: 'bower.json'
            }
            }), { base: 'bower_components' })
            .pipe(gulp.dest('css'))
      }
    // });
gulp.task('bowerbuild',buildBOWER) 
gulp.task('bowerbuildjs', buildJS) 
gulp.task('build', ['bowerbuild', 'bowerbuildjs'])


gulp.task('webpack', [], function() {
    return gulp.src(path.join(src, '**', CONFIG_FILENAME), { base: path.resolve(src) })
        .pipe(webpack.init(webpackConfig))
        .pipe(webpack.props(webpackOptions))
        .pipe(webpack.run())
        .pipe(webpack.format({
            version: false,
            timings: true
        }))
        .pipe(webpack.failAfter({
            errors: true,
            warnings: true
        }))
        .pipe(gulp.dest(dest));
});
 
gulp.task('watch', function() {
    gulp.watch(path.join(src, '**/*.*')).on('change', function(event) {
        if (event.type === 'changed') {
            gulp.src(event.path, { base: path.resolve(src) })
                .pipe(webpack.closest(CONFIG_FILENAME))
                .pipe(webpack.init(webpackConfig))
                .pipe(webpack.props(webpackOptions))
                .pipe(webpack.watch(function(err, stats) {
                    gulp.src(this.path, { base: this.base })
                        .pipe(webpack.proxy(err, stats))
                        .pipe(webpack.format({
                            verbose: true,
                            version: false
                        }))
                        .pipe(gulp.dest(dest));
                }));
        }
    });
});