# jormat-front
## Getting Started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node ^4.2.3, npm ^2.14.7

### Developing

1. Run `npm install` to install server dependencies.

2. Run `bower install` to install components (npm install bower -g)

3. Config services path in `local.env.json` file.

4. Run `npm start` to run the server.

