/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> companyInfoController
* # As Controller --> companyInfo													
*/


  export default class companyInfoController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,clientsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
        vm.viewMore = viewMore
	    
	    function cancel() {
			$modalInstance.dismiss('chao');
		}

        function viewMore() {

            var url = $state.href("app.accountStatus",{ 
                idClient: $scope.clientId
            });

            window.open(url,'_blank');
        }



    }

  }

  companyInfoController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','clientsServices','$location','UserInfoConstant','ngNotify','$state'];
