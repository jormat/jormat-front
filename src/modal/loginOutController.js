
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> loginOutController
* # As Controller --> loginOutCtrl
*/

export default class loginOutController{
	constructor($scope,$timeout,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modalInstance,$localStorage,$cookies){
		this.$modalInstance = $modalInstance;
		this.$mdDialog = $mdDialog;
		this.$localStorage = $localStorage;
		this.$cookies = $cookies;
		this.$scope = $scope;
		this.time = 2400; //Segundos de espera de respuesta a Modal (configurado en 40 min.)
		this.countTime();
	}//FIN CONSTRUCTOR

	// Funciones

	countTime() {
		this.setTime = setTimeout(() => {
			this.time--
			if (this.time == 0){
				delete this.$localStorage.userLogin
				this.$cookies.put('hasDemand','')
				this.$cookies.remove('isAdmin')
				window.location.href = '/login'
				return;
			}
			this.countTime()
		}, 1000);
	}

	cancel() {
		this.$modalInstance.dismiss('chao');
		clearTimeout(this.setTime)
	}
}

loginOutController.$inject = ['$scope','$timeout','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modalInstance','$localStorage','$cookies'];

