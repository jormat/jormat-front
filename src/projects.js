//import modules
import main from './app/main-start'
import adminPanel from './app/admin-panel'
import sales from './app/sales'
import warehouse from './app/warehouse'
import purchases from './app/purchases'
import items from './app/items'
import finance from './app/finance'
import billing from './app/billing'
import reports from './app/reports'
import humanResources from './app/human-resources'


export default angular.module('app.jormat', [
  main,
  adminPanel,
  items,
  purchases,
  warehouse,
  sales,
  finance,
  billing,
  humanResources,
  reports
  ])
  .name;
