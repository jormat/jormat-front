import mainTemplate from './templates/main-template.html'

export let occurrenceView = {
	bindings: {
		occurrence: "=",
		options: "<",
		alwaysEdit: "<",
		routesEdit: "=",
		expandPanels: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', '$state', '$timeout',
	function ($scope, $rootScope, $templateCache, $state, $timeout) {
		const notTypefields = ['occurrences', 'dynamicOccurrences', 'panel']
		this.$onInit = () => {
			this.expandPanels = this.expandPanels || false
			this.state = {}
		}

		this.setState = (index) => {
			this.state[index] = this.expandPanels
		}

		this.showDivider = (typefield, isLast) => {
			return (!isLast && notTypefields.indexOf(typefield) < 0)
		}

		this.editSection = (subStructure) => {
			this.routesEdit[subStructure.label].params = angular.extend({}, {structureId: subStructure.id_item}, this.routesEdit[subStructure.label].params)
			$state.go(this.routesEdit[subStructure.label].url, this.routesEdit[subStructure.label].params)
		}
	}]
}