import mainTemplate from './current-time.html'

export let currentTime = {
	bindings: {
		options: "<"
	},
	template: mainTemplate,
	controllerAs: 'diag',
	controller: ['$scope', '$rootScope', '$templateCache', 'moment', '$interval',
	function ($scope, $rootScope, $templateCache, moment, $interval) {
		this.time = null
		this.default = {
			format: "h:mm:ss"
		}

		this.opt = angular.extend({}, this.default, this.options)

		this.refresh = () => {
			this.time = moment().format(this.opt.format)
		}

		$interval(() => {
			this.refresh()
		}, 1000)

	}]
}