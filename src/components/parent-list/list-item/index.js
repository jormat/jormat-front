import mainTemplate from './list-item.html'

export let listItem = {
	bindings: {
		item: "<",
		label: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 
	function ($scope, $rootScope, $templateCache) {

		this.$onInit = () => {
			$scope.show = false
			for (let label in this.item) {
				if (label != 'errors') {
					if (Object.keys(this.item[label].errors).length > 0) {
						$scope.show = true
					}
				} else if (label == 'errors') {
					if (this.item[label].length > 0) {
						$scope.show = true
					}
				}
			}
			return $scope.show
		}
	}]
}