import mainTemplate from './templates/main-template.html'

export let parentList = {
	bindings: {
		list: "<",
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 
	function ($scope, $rootScope, $templateCache) {

		this.$onInit = () => {
			this.list = this.list || []
		}
	}]
}