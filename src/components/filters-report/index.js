import mainTemplate from './templates/main-template.html'

export let filtersReport = {
	bindings: {
		options: "<",
		filters: "<",
		onSubmit: "&",
		onDownload: "&",
		onToggleHide: "&",
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'uFunctions', 
	function ($scope, $rootScope, $templateCache, uFunctions) {
		$templateCache.put('horizontal-template.html', require('./templates/horizontal-template.html'))
		$templateCache.put('vertical-template.html', require('./templates/vertical-template.html'))
		$scope.showFilters = true
		const Helpers = uFunctions
		const inputsArray = ['multipleSelect', 'multipleSelectSuggest', 'listGroup']
		const inputsInLine = ['text', 'number', 'textArea', 'switch', 'datetime', 'checkbox']
		const inputsObject = ['select', 'selectSuggest', 'datetimeRange']
		const allInputs = inputsArray.concat(inputsInLine, inputsObject)
		/**
		* Default config report
		**/
		this.optDefaults = {
			title: 'Reporte',
			complement: '',
			emptyMessage: "Seleccione un filtro",
			view: "horizontal", // ['horizontal', 'vertical']
			btnSave: true, // Boolean
		}

		/**
		* Default config to all inputs
		**/
		this.configInputDefault = {
			cols: 4,
			isRequired: true,
			isAdvanced: false, 
			typefield: "text",
			data: []
		}

		/**
		* Initial function
		**/
		this.$onInit = () => {
			this.onToggleHide = this.onToggleHide || (() => {})
			this.opt = angular.extend({}, this.optDefaults, this.options)
			let inputs = []
			if (this.filters && this.filters.inputs && this.filters.inputs.length > 0) {
				this.filters.inputs.forEach((filter) => {
					inputs.push(angular.extend({}, this.configInputDefault, filter))
				})
			}
			this.regularInputs = Helpers.chunkData({data: _.filter(inputs, item => !item.isAdvanced)}, 12)
			this.advancedInputs = Helpers.chunkData({data: _.filter(inputs, item => item.isAdvanced)}, 12)
		}

		this.submit = () => {
			if (!this.disabledSubmit()) {
				this.onSubmit({filters: this.getAllInputs()})
			}
		}

		this.onChange = (field, cleanModel) => {
			this.manageInputsDependencies(field, cleanModel)
			if (!this.opt.btnSave) {
				this.submit()
			}
		}

		this.manageInputsDependencies = (field, cleanModel) => {
			for (let i in this.regularInputs) {
				this.checkDependencies(this.regularInputs[i], field, cleanModel)
			}
			for (let i in this.advancedInputs) {
				this.checkDependencies(this.advancedInputs[i], field, cleanModel)
			}
		}

		this.checkDependencies = (inputs, field, cleanModel) => {
			inputs.forEach((f) => {
				f.params = f.params || {}
				for (let i in f.params) {
					if (i == field.label || i == field.fieldName) {
						f.model = !cleanModel ? null : f.model
						if (field.model !== null) {
							f.addParams = angular.extend({}, f.addParams, Helpers.getParam(field.model, f.params[i]))
							f.reload = !f.reload
							f.callSrv = true
						} else {
							f.addParams = f.addParams || {}
							f.reload = !f.reload
							f.callSrv = false
						}
					}
				}

				if (typeof f.pathService == 'object' && !Array.isArray(f.pathService)) {
					let index = f.pathService[field.label]
					if (index) {
						if (field.model) {
							f.model = null
							f.itemList = []
							if (Array.isArray(field.model[index])) {
								field.model[index].forEach((item) => {
									let row = Helpers.getItemForModel({}, item)
									f.itemList.push(row)
								})
							}
							f.reloadList = !f.reloadList
							f.isDisabled = f.initialDisabled
						} else {
							f.model = null
							f.itemList = []
							f.reloadList = !f.reloadList
						}
					}
				}
			})
		}

		this.download = () => {
			if (!this.disabledSubmit()) {
				this.onDownload({filters: this.getAllInputs()})
			}
		}

		this.getAllInputs = () => {
			let filters = {}
			this.regularInputs.forEach((row) => {
				row.forEach((input, index) => {
					let fielName = input.fieldName || input.label || 'input_r' + index
					filters[fielName] = input.model
				})
			})
			this.advancedInputs.forEach((row) => {
				row.forEach((input, index) => {
					let fielName = input.fieldName || input.label || 'input_' + index
					filters[fielName] = input.model
				})
			})
			return filters
		}

		this.disabledSubmit = () => {
			this.regularInputs = this.regularInputs || []
			this.advancedInputs = this.advancedInputs || []
			let inputs = this.regularInputs.concat(this.advancedInputs)
			let disabled = false
			inputs.forEach((row) => {
				row.forEach((input) => {
					if (input.isRequired) {
						if (inputsInLine.indexOf(input.typefield) >= 0) {
							if (input.model === null || input.model === undefined || input.model == '') {
								disabled = true
							}
						} else if (inputsObject.indexOf(input.typefield) >= 0) {
							if (input.model === null || input.model === undefined || (!input.model.id_code && !input.model.startDate)) {
								disabled = true
							}
						} else if (inputsArray.indexOf(input.typefield) >= 0) {
							if (input.model === null || input.model === undefined || input.model.length <= 0) {
								disabled = true
							}
						}
					}
				})
			})
			return disabled
		}

		this.toggleHide = () => {
			this.listToggle = !this.listToggle
			this.onToggleHide({isHide: this.listToggle})
		}

	}]
}