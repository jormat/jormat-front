import mainTemplate from './templates/changes-history.html'

export let changesHistory = {
	bindings: {
		history: "<",
		options: "<",
	},
	template: mainTemplate,
	controllerAs: 'HistCtrl',
	controller: ['$scope', '$rootScope', '$templateCache', '$filter', 
	function ($scope, $rootScope, $templateCache, $filter) {}]
}