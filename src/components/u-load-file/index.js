import mainTemplate from './templates/main-template.html'

export let uLoadFile = {
	bindings: {
		options: "=",
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', '$filter', 'FileUploader', 'ngNotify', '$timeout', '$state', '$window', 'uiGridConstants', '$mdDialog', 'Dialogs',
	function ($scope, $rootScope, $templateCache, $filter, FileUploader, ngNotify, $timeout, $state, $window, uiGridConstants, $mdDialog, Dialogs) {
		$templateCache.put('loaded-results-view.html', require('./templates/loaded-results-view.html'))
		const modalShowError =  require('./templates/cell-template-show-errors.html')
		$scope.uploader = new FileUploader()
		this.progressbarClass = 'active'
		this.doPing = true
		this.viewResults = false
		this.showRecordsLoaded = true
		this.uiGrid = {
			enableFiltering: true,
			enablePaging: true,
			enableGridMenu: true,
			paginationPageSizes: [25, 50, 100, 200, 500, 10000],
			paginationPageSize: 25,
            useExternalPagination: true,
            columnDefs: [], 
		}
		this.loadProcess = {
			response: {}, 
			timer: null, 
			currentStatus: 1,
			status: "Subiendo archivo al servidor",
		}
		this.errorHeader = 'u-load-file component: '
		this.default = {
			type: "", 
			title: "IMPROVE_LOAD_FILE",
			headerColor: "bg-primary", 
			btnColor: "bg-primary",
			pathCsvDownload: null,
			urlGoBack: false,
			callServiceResults: false,
			service: null,
			paramsToGoBack: {},
			loadParams: {},
			pingServices: false, 
			alias: false,
			grid: {
				useExternalPagination: true
			}
		}

		this.$onInit = () => {
			this.config = angular.extend({}, this.default, this.options)
			this.uiGrid = angular.extend({}, this.uiGrid, this.options.grid)
			this.createUploadObject()
		}

		this.csvDownload = () => {
			if (this.config.pathCsvDownload) {
				$window.open(this.config.pathCsvDownload + this.config.type)
			} else {
				console.error(this.errorHeader + 'atribute (pathCsvDownload) not defined in options')
			}
		}

		this.goBack = () => {
			this.doPing = false
			if (this.config.urlGoBack) {
				$state.go(this.config.urlGoBack, this.config.paramsToGoBack)
			} else {
				window.history.back()
			}
		}

		this.clickUpload = () => {
			$scope.uploader.formData = [this.config.loadParams]
			uploadButton.click()
		}

		this.createUploadObject = () => {
			let url = this.config.service

			$scope.uploader.url = url
			$scope.uploader.method = 'POST'
			$scope.uploader.alias = this.config.alias
			$scope.uploader.formData = [this.config.loadParams]

			$scope.uploader.filters.push({
				name: 'customFilter',
				fn: function(item, options) {
					return this.queue.length < 10
				}
			})

			$scope.uploader.onAfterAddingFile = (fileItem) => {
				fileItem.upload()
			}

			$scope.uploader.onSuccessItem = (fileItem, response, status, headers) => {
				this.loadProcess.response = response
				this.fileId = this.loadProcess.response.id_file
				if (response.status) {
					ngNotify.set('Error con servicio de PING', 'error')
				} else {
					this.loadProcess.status = 'Ejecutando ETL'
					this.ping(response.id_status)
				}
			}

			$scope.uploader.onErrorItem = (fileItem, response, status, headers) => {
				ngNotify.set(response.error + ', ErrorCod: ' + status ,'error')
				this.$state.go(this.$state.current, {}, {reload: true})
				if (response.status == 1){
					this.error(response.error)
				}
			}

			$scope.uploader.onProgressItem =  (item, progress) => {
				this.progressLoad = progress > 0 ? progress / 2 : 0
			}

			$scope.uploader.onCompleteItem = (fileItem, response, status, headers) => {
			}
		}

		this.showFilesScreen = () => {
			this.viewResults = false
		}

		this.goViewResults = () => {
			this.viewResults = true
			this.uiGrid.useExternalPagination
			this.recordsRejected = angular.copy(this.uiGrid)
			this.recordsLoaded = angular.copy(this.uiGrid)
			if (this.uiGrid.useExternalPagination) {
				this.recordsRejected.onRegisterApi = this.recordsRejectedOnRegister
				this.recordsLoaded.onRegisterApi = this.recordsLoadedOnRegister
			}

			let params = {
				CORRECTOS: true, 
				ID_FILE: this.fileId,
				newPage: this.uiGrid.useExternalPagination ? 1 : undefined,
				pageSize: this.uiGrid.useExternalPagination ? 25 : undefined
			}

			this.getResultsData(params, (data, size) => {
				this.recordsLoaded.columnDefs = this.makeUiGrid(data)
				this.recordsLoaded.data = data
				this.recordsLoaded.totalItems = size
			})

			let incorrectParams = angular.copy(params)
			incorrectParams.CORRECTOS = false

			this.getResultsData(incorrectParams, (data, size) => {
				this.recordsRejected.columnDefs = this.makeUiGrid(data)
				this.recordsRejected.data = data
				this.recordsRejected.totalItems = size
			})
		}

		this.getResultsData = (params, callback) => {
			if (this.config.callServiceResults) {
				this.config.callServiceResults(params).$promise.then((response) => {
					if (response.status) {
						this.ngNotify.set('Ha ocurrido un error en el Servicio', 'error')
					} else {
						callback(response.data, response.size)
					}
				}, (error) => {
					this.ngNotify.set('No se pudo conectar con el Servicio', 'error')
				})
			}
		}

		this.makeUiGrid = (data) => {
			data = data || []
			let cols = []
			for (let i in data) {
				for (let x in data[i]) {
					let item = {
						displayName: x,
						field: x,
						cellTemplate: x == 'error' ? modalShowError : undefined, 
						filter: {
							term: '',
							type: uiGridConstants.filter.CONTAINS
						}
					}
					cols.push(item)
				}
				break
			}
			return cols
		}

		this.showErrorsDetails = (row, col, ev) => {
			let opt = {
				title: "Detalle de Errores",
				fullscreen: true, 
				list: row.entity.error.map(item => item.name_error)				
			}

			Dialogs.info(opt)
		}

		this.ping = (id_status) => {
			if (this.config.pingServices) {
				this.config.pingServices({ID_STATUS: id_status}).$promise.then((response) => {
					let status = response.data[0].id_status
					if (status == 3 || status == 5) {
						this.progressLoad = status == 3 ? 1 : 100
						this.doPing = false
						this.progressbarClass = status == 3 ? 'error' : 'warning'
						ngNotify.set('Error al cargar archivo' ,'error')
						this.loadProcess.status = status == 3 ? 'Error al Iniciar el ETL!' : 'Proceso Finalizado con Errores!'
						this.viewResults = true

						if (status == 3) {
							ngNotify.set('Error al Iniciar el ETL!' ,'error')
						} else {
							this.goViewResults()
							ngNotify.set('Proceso Finalizado con Errores!' ,'warn')
						}
					} else {
						if (status != this.loadProcess.currentStatus) {
							this.loadProcess.currentStatus = status

							if (status == 2) {
								this.progressLoad += 25
							} else {
								this.progressLoad = 100
								this.progressbarClass = 'success'
								this.loadProcess.status = 'Proceso de Carga Finalizado!!'
								this.doPing = false

								this.goViewResults()

								this.viewResults = true
								ngNotify.set('Archivo Cargado Exitosamente!' ,'success')
							}
						}
					}

					if ((status == 3 || status == 4 || status == 5) || !this.doPing) {
						$timeout.cancel(this.loadProcess.timer)
					} else {
						this.loadProcess.timer = $timeout(() => {
							this.ping(id_status)
						}, 1000)
					}
				}, (err) => {
					ngNotify.set('Error con servicio de PING', 'error')
				})
			} else {
				console.error(this.errorHeader + 'atribute (pingServices) not defined in options')
			}
		}

		this.recordsLoadedOnRegister = (gridApi) => {
			let localGridApi = gridApi

			localGridApi.core.on.filterChanged($scope, () => {
				var grid = localGridApi.grid.renderContainers.body.grid;
				var filters={}

				for(let i in grid.columns){
					if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term !== undefined)){
						filters[grid.columns[i].field] = grid.columns[i].filters[0].term
					}
				}

				this.recordsLoaded.paginationNumber = 1
				let params = {
					CORRECTOS: true, 
					newPage: 1,
					ID_FILE: this.fileId,
					pageSize: this.recordsLoaded.paginationPageSize,
					filters: filters
				}
				this.config.callServiceResults(params).$promise.then((data) => {
					this.recordsLoaded.data = data.data
					this.recordsLoaded.totalItems = data.size || 0
				});
			});

			localGridApi.pagination.on.paginationChanged($scope, (newPage, pageSize) => {
				var grid = localGridApi.grid.renderContainers.body.grid
				var filters={}
				for(let i in grid.columns){
					if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term !== undefined)){
						filters[grid.columns[i].field] = grid.columns[i].filters[0].term
					}
				}

				let params = {
					CORRECTOS: true, 
					ID_FILE: this.fileId,
					newPage: newPage,
					pageSize: pageSize,
					filters: filters
				}

				this.config.callServiceResults(params).$promise.then((data) => {
					this.recordsLoaded.data = data.data
					this.recordsLoaded.totalItems = data.size || 0
				})
			})
		}

		this.recordsRejectedOnRegister = (gridApi) => {
			let localGridApi = gridApi

			localGridApi.core.on.filterChanged($scope, () => {
				var grid = localGridApi.grid.renderContainers.body.grid;
				var filters={}

				for(let i in grid.columns){
					if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term !== undefined)){
						filters[grid.columns[i].field] = grid.columns[i].filters[0].term
					}
				}

				this.recordsRejected.paginationNumber = 1
				let params = {
					CORRECTOS: false, 
					newPage: 1,
					ID_FILE: this.fileId,
					pageSize: this.recordsRejected.paginationPageSize,
					filters: filters
				}
				this.config.callServiceResults(params).$promise.then((data) => {
					this.recordsRejected.data = data.data
					this.recordsRejected.totalItems = data.size || 0
				});
			});

			localGridApi.pagination.on.paginationChanged($scope, (newPage, pageSize) => {
				var grid = localGridApi.grid.renderContainers.body.grid
				var filters={}
				for(let i in grid.columns){
					if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term !== undefined)){
						filters[grid.columns[i].field] = grid.columns[i].filters[0].term
					}
				}

				let params = {
					CORRECTOS: false, 
					ID_FILE: this.fileId,
					newPage: newPage,
					pageSize: pageSize,
					filters: filters
				}

				this.config.callServiceResults(params).$promise.then((data) => {
					this.recordsRejected.data = data.data
					this.recordsRejected.totalItems = data.size || 0
				})
			})
		}

	}]
}