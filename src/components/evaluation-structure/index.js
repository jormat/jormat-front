import mainTemplate from './evaluation-structure.html'

export let evaluationStructure = {
	bindings: {
		nodes: '=',
		typesFormula: "=", 
		noDrag: "<", 
		editOrCreate: "<",
		learnings: "=",
		competitions: "=",
		view: "<"
	},
	template: mainTemplate,
	controllerAs: 'structure',
	controller: ['$scope', '$rootScope', '$templateCache', 'moment', '$filter', 'editableThemes', 'editableOptions', '$mdDialog',
	function ($scope, $rootScope, $templateCache, moment, $filter, editableThemes, editableOptions, $mdDialog) {
		$templateCache.put('edit_or_create_template.html', require('./templates/edit_or_create_template.html'))
		$templateCache.put('just_view_info.html', require('./templates/just_view_info.html'))
		$templateCache.put('class_panel_structure.html', require('./templates/class_panel_structure.html'))
		$templateCache.put('improve_structure.html', require('./templates/improve_structure.html'))
		$templateCache.put('improve_edit_template.html', require('./templates/improve_edit_template.html'))
		$templateCache.put('confirm.html', require('./dialogs/confirm.html'))

		this.moment = moment
		editableThemes.bs3.inputClass = 'input-sm';
		editableThemes.bs3.buttonsClass = 'btn-sm';
		editableOptions.theme = 'bs3'
		this.competitions = this.competitions || []
		this.learnings = this.learnings || []
		this.competenciesList = this.competitions !== undefined ? this.competitions.map(item => ({value: item.id_code, text: item.ds_name})) : []
		this.learningResultsList = this.learnings !== undefined ? this.learnings.map(item => ({value: item.id_code, text: item.ds_name})) : []

		this.view = this.view || 'classpanel'

		this.functions = [
			{displayName: "Promedio()", startFunct: "Promedio(", endFunct: ")"},
			{displayName: "Mejor()", startFunct: "Mejor(", endFunct: ")"},
			{displayName: "Peor()", startFunct: "Peor(", endFunct: ")"},
			{displayName: "MejorX()", startFunct: "MejorX(", endFunct: ")"},
			{displayName: "PeorX()", startFunct: "PeorX(", endFunct: ")"},
		]

		this.dateOpen = ($event, node) => {
			$event.preventDefault()
			$event.stopPropagation()

			node.opened = true
		}

		this.modalConfirm = (opt) => {
			return $mdDialog.show($mdDialog.confirm({
				controller   : ['$scope', '$mdDialog', 'options', function ($scope, $mdDialog, options) {
					$scope.options = options
					$scope.close = (response) => {
						$mdDialog.hide(response)
					}
				}],
				controllerAs : 'diag',
				templateUrl  : 'confirm.html',
				locals       : {options : opt},
				animation    : true,
				parent       : angular.element(document.body),
				clickOutsideToClose : false
			}))
		}

		this.newSubItem = (scope) => {
			var nodeData = scope.$modelValue;
			nodeData.nodes.push({
				id: null,
				parentId: nodeData.id,
				evaluationDate: this.moment().format('YYYY-MM-DD'),
				finalCollegiate: 0,
				finalHasf: 0,
				componentId: null,
				title: "Sin titulo",
				alias_component: "ST",
				id_type_component: null,
				type_component: null,
				formula: null,
				ds_formula_desc: "",
				porcentaje: 0,
				fraccion: null,
				observacion: null,
				is_final: 0,
				list_evidences: [], 
				nodes: []
			})
		}

		$scope.treeOptions = {
			accept: (sourcScope, destScope, index) => {
				let accept = destScope.$nodeScope === null && this.nodes.length >= 1 ? false : true
				return accept
			}
		}

		this.addFunction = (node, func) => {
			node.formula = (node.formula !== undefined) ? node.formula + func.startFunct : func.startFunct
		}

		this.addToFormula = (node, child) => {
			node.formula = (node.formula !== undefined) ? node.formula + child.alias_component : child.alias_component
		}

		this.sumaAllChildren = (node) => {
			let strSum = node.nodes.map(function (child) {
				return child.alias_component
			})
			node.formula = (node.formula !== undefined) ? node.formula + strSum.join('+') : strSum.join('+')
		}

		this.concatAllChildren = (node) => {
			let strSum = node.nodes.map(function (child) {
				return child.alias_component
			})
			node.formula = (node.formula !== undefined) ? node.formula + strSum.join(',') : strSum.join(',')
		}

		this.validatingTypeView = () => {
			if (['improve', 'classpanel'].indexOf(this.view) >= 0) {
				return false
			}
			return true
		}

		this.onChangePercentage = (node) => {
			node.ponderacion = {}

			for (let i in node.nodes) {
				node.ponderacion[i] = node.ponderacion[i] || {}			
				node.ponderacion[i].porcentaje = node.nodes[i].porcentaje
				node.ponderacion[i].alias = node.nodes[i].alias_component
			}
			node.formula = ''
			let f = []

			for (let x in node.ponderacion) {
				let porcentaje = parseFloat(node.ponderacion[x].porcentaje)
				f.push('(' + (porcentaje/100).toFixed(4) + '*' + node.ponderacion[x].alias + ')')
			}

			node.formula = f.join('+')
		}

		this.main = (nodes) => {
			nodes.forEach((node) => {
				if (node.type_calc !== null && node.type_calc !== undefined) {
					this.refreshFraction(node)
				}
				if (node.nodes.length > 0) {
					this.main(node.nodes)
				}
			})
		}

		$scope.$watch('structure.nodes', (nodes) => {
			if (nodes !== undefined && this.view == 'classpanel') {
				this.main(this.nodes)
			}
		});


		this.refreshFraction = (node) => {
			if (node.type_calc == '1') {
				this.onChangePercentage(node)
			}
			if (node.type_calc == '2') {
				this.onChangeFraction(node)
			}
		}

		this.addEvidence = (newCompetencia, newLearning, newStrategy, node) => {
			if (newCompetencia !== null && newLearning !== null && newStrategy !== null && newStrategy != '') {
				node.list_evidences = node.list_evidences || []
				node.list_evidences.push({
					id: null,
					nameEvidencia: newStrategy,
					idCompetencia: newCompetencia,
					idResAprendiz: newLearning
				})
			}
		}

		this.deleteCompetencia = (key, node) => {
			let opt = {
				message: 'Esta seguro de eliminar esta evidencia?',
				title: node.list_evidences[key].nameEvidencia,
			}
			this.modalConfirm(opt).then((response) => {
				if (response) {
					node.list_evidences.splice(key, 1)
				}
			})
		}

		this.onChangeFraction = (node) => {
			node.fraction_values = {}

			for (let i in node.nodes) {
				node.fraction_values[i] = node.fraction_values[i] || {}
				node.nodes[i].valores = node.nodes[i].valores || {}

				let num = node.nodes[i].valores.numerador
				let den = node.nodes[i].valores.denominador
				
				node.nodes[i].fraccion = (num !== null && num !== undefined && num != '' && den !== null && den !== undefined && den != '') ? num + '/' + den : false
				node.nodes[i].ds_formula_desc = node.nodes[i].fraccion
				node.fraction_values[i].str_fraction = node.nodes[i].fraccion
				node.fraction_values[i].alias = node.nodes[i].alias_component
			}
			node.formula = ''
			let f = []

			for (let x in node.fraction_values) {
				let fra = node.fraction_values[x].str_fraction
				f.push('(' + fra + '*' + node.fraction_values[x].alias + ')')
			}

			node.formula = f.join('+')
		}
	}]
}