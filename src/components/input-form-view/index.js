import mainTemplate from './templates/main-template.html'

export let inputFormView = {
	bindings: {
		input: "=",
		options: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 
	function ($scope, $rootScope, $templateCache) {
		$templateCache.put('text-view.html', require('./templates/text-view.html'))
		$templateCache.put('list-view.html', require('./templates/list-view.html'))
		$templateCache.put('select-view.html', require('./templates/select-view.html'))
		$templateCache.put('switch-view.html', require('./templates/switch-view.html'))
		$templateCache.put('textarea-view.html', require('./templates/textarea-view.html'))
		$templateCache.put('structure-view.html', require('./templates/structure-view.html'))
		$templateCache.put('multiple-select.html', require('./templates/multiple-select.html'))
		this.uiTableOptions = {
			inEdit: false,
			baseUrl: '',
			addParams: {}
		}

		this.$onInit = () => {
			let optionsDefault = {
				container: "panel"
			}
			this.options = angular.extend({}, optionsDefault, this.options)
		}
	}]
}