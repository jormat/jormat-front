import mainTemplate from './templates/main-template.html'
import itemDummy from './item-dummy.json'
export let uTree = {
	bindings: {
		eNodes: "=",
		eChange: "&",
		parent: "<",
		eOptions: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'moment',
	function ($scope, $rootScope, $templateCache, moment) {
		$templateCache.put('competency-levels-template.html', require('./templates/competency-levels-template.html'))
		$templateCache.put('structure-template.html', require('./templates/structure-template.html'))
		$templateCache.put('simple-panel.html', require('./templates/panels/simple-panel.html'))
		this.moment = moment
		/**
		* Default config
		**/
		let optDefault = {
			inEdit: false, 
			allowMultiLevels: true, 
			template: "syllabus",
			getEmptyElement: this.getNewItemComponent, 
		}
		this.datePickerOptions = {
			singleDatePicker: true
		}

		/**
		* Main function
		**/
		this.$onInit = () => {
			this.eChange = this.eChange || (() => {})
			this.eNodes.model = null
			this.eNodes.initialState = this.eNodes.data
			this.options = angular.extend({}, optDefault, this.eOptions)
			this.taOptions = [['h1','h2','h3'],['bold','italics'],['ul','ol'],['justifyLeft','justifyCenter','justifyRight'],['undo','redo']]
		}

		this.onChange = (field) => {
			this.eNodes.model = this.makeModel(angular.copy(this.eNodes.data))
			this.eChange({utree: this.eNodes})
		}

		this.dateOpen = ($event, node) => {
			$event.preventDefault()
			$event.stopPropagation()

			node.opened = true
		}

		this.getStatusField = (field) => {
			if (!this.options.inEdit) {
				field.isDisabled = true
			}
		}

		this.setDateFormatting = (node) => {
			node.componentDate = node.componentDate || ''
		}

		this.makeModel = (nodes) => {
			nodes = nodes || []
			nodes.forEach((node) => {
				node.DYNAMIC_INPUTS = []
				node.dynamicInputs = node.dynamicInputs || []
				node.dynamicInputs.forEach((panel) => {
					panel.data = panel.data || []
					panel.data.forEach((field) => {
						let inp = {}
						inp[field.label] = field.model
						node.DYNAMIC_INPUTS.push(inp)
					})
				})
				if (node.nodes.length > 0) {
					node.nodes = this.makeModel(node.nodes)
				}
			})
			return nodes
		}

		this.newSubItem = (node) => {
			this.options.getEmptyElement((response) => {
				let item = response.data
				item.ID_EVALUATION_PARENT_CODE = node.competencyId || 0
				item.id_parent = node.competencyId || 0
				item.nodes = []

				if (!node.nodes) {
					node.data = node.data || []
					node.data.push(item)
				} else {
					node.nodes = node.nodes || []
					node.nodes.push(item)
				}
			})
		}

		this.getNewItemComponent = (callback) => {
			callback({data: angular.copy(itemDummy)})
		}

		/**
		* Restriction to ensure that there is always a single parent component of all
		**/
		$scope.treeOptions = {
			accept: (sourcScope, destScope, index) => {
				// console.log('sourcScope->', sourcScope)
				// console.log('destScope->', destScope)
				// console.log('index->', index)
				// console.log('-------------------------------------------------------------')
				let accept = destScope.$nodeScope === null && this.eNodes.length >= 1 ? false : true
				return accept
			}
		}
	}]
}