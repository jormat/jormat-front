
import inputMainTemplate from './main-input.html'

export let dinamicInput = {
	bindings: {
		field: "=",
		onChange: "&",
		onSearch: "&",
		config: "<",
		baseServiceUrl: '<',
		additionalParams: '=',
		inPanel: '<',
		showTitle: '<',
		typeForm: "@",
		allowClear: "<",
		allowSelectAll: "<",
	},
	template: inputMainTemplate,
	controllerAs: 'inputCtrl',
	controller: ['$scope', '$rootScope', '$templateCache', '$http', 'ngNotify', '$mdDialog', 'Dialogs', '$filter', 'uFunctions', 'moment',
	function ($scope, $rootScope, $templateCache, $http, ngNotify, $mdDialog, Dialogs, $filter, uFunctions, moment) {
		$templateCache.put('input-multi-select-suggest.html', require('./inputSelect/input-multi-select-suggest.html'))
		$templateCache.put('input-datetime-range.html', require('./inputDatetime/input-datetime-range.html'))
		$templateCache.put('input-select-suggest.html', require('./inputSelect/input-select-suggest.html'))
		$templateCache.put('input-multi-select.html', require('./inputSelect/input-multi-select.html'))
		$templateCache.put('input-datetime.html', require('./inputDatetime/input-datetime.html'))
		$templateCache.put('input-textarea.html', require('./inputText/input-textarea.html'))
		$templateCache.put('input-select.html', require('./inputSelect/input-select.html'))
		$templateCache.put('input-switch.html', require('./inputSwitch/input-switch.html'))
		$templateCache.put('input-number.html', require('./inputText/input-number.html'))
		$templateCache.put('input-list.html', require('./inputList/input-list.html'))
		$templateCache.put('input-text.html', require('./inputText/input-text.html'))
		const Functions = uFunctions

		this.field.allowClear = (this.field.allowClear === null || this.field.allowClear === undefined) ? true : this.field.allowClear
		this.allowClear = (this.allowClear === null || this.allowClear === undefined) ? this.field.allowClear  : this.allowClear

		this.field.allowSelectAll = (this.field.allowSelectAll === null || this.field.allowSelectAll === undefined) ? false : this.field.allowSelectAll
		this.allowSelectAll = (this.allowSelectAll === null || this.allowSelectAll === undefined) ? this.field.allowSelectAll  : this.allowSelectAll

		this.$translate = $filter('translate')
		this.uiTableOptions = {
			inEdit: !this.field.isDisabled,
			baseUrl: this.baseServiceUrl,
			addParams: this.additionalParams
		}

		$rootScope.$on('$translateChangeSuccess', () => {
			this.setModel()
		})

		$scope.$watch('inputCtrl.field.reload', (reload) => {
			if (reload !== undefined) {
				this.field.itemList = []
				if (this.field.callSrv) {
					this.field.isDisabled = this.field.initialDisabled
					let localParams = angular.extend({}, this.additionalParams, this.field.addParams)
					this.callService(localParams, (response) => {
						if (response.data.length > 0) {
							this.field.itemList = response.data || []
							if (this.field.selectFirst) {
								if (Array.isArray(this.field.model)) {
									this.field.model.push(this.field.itemList[0])
								} else if (typeof this.field.model == 'object') {
									this.field.model = this.field.itemList[0]
								}
								this.ngChange()
							}
						}
					})
				}
			}
		})

		// typeForm available [inline | not-inline | none]
		this.typeForm = this.typeForm || 'inline'

		this.inPanel = this.inPanel || false
		this.showTitle = (this.showTitle !== undefined) ? this.showTitle : true

		$scope.newItem = null
		this.taOptions = [['h1','h2','h3'],['bold','italics'],['ul','ol'],['justifyLeft','justifyCenter','justifyRight'],['undo','redo']]
		this.onChange = this.onChange || (() => {})
		this.onSearch = this.onSearch || (() => {})
		this.field.itemList = []
		let confDefault = {name: "ds_name", id: "id_code"}

		this.$onInit = () => {
			this.additionalParams = this.additionalParams || {}
			this.conf = angular.extend({}, confDefault)
			this.field.params = this.field.params || {}
			this.field.model = null
			this.field.initialState = null
			this.field.initialDisabled = this.field.isDisabled
			this.datePickerOptions = {singleDatePicker: true }
			this.datePickerRangeOptions = {
				applyClass: 'btn-success',
				locale: {
					applyLabel: "Ok",
					fromLabel: "Desde",
					format: "YYYY-MM-DD",
					toLabel: "Hasta",
					cancelLabel: 'Cancelar',
					customRangeLabel: 'Personalizado'
				},
				ranges: {
					'Últimos 7 días': [moment().subtract(6, 'days'), moment()],
					'Últimos 15 días': [moment().subtract(14, 'days'), moment()],
					'Últimos 30 días': [moment().subtract(29, 'days'), moment()]
				}
			}

			if (['select', 'selectSuggest', 'multipleSelect', 'multipleSelectSuggest', 'listGroup'].indexOf(this.field.typefield) >= 0) {
				for (let i in this.field.data) {
					let item = Functions.getItemForModel(this.field, this.field.data[i])
					if ((['multipleSelect', 'multipleSelectSuggest', 'listGroup'].indexOf(this.field.typefield) >= 0)) {
						if (item) {
							this.field.model = this.field.model || []
							this.field.model.push(item)
						}
					} else {
						this.field.model = item ? angular.copy(item) : null
					}
				}

				this.field.initialState = angular.copy(this.field.model)

				if (['select', 'multipleSelect', 'listGroup'].indexOf(this.field.typefield) >= 0) {

					let params = angular.extend({}, this.additionalParams, (this.field.addParams || {}))
					this.field.itemList = []

					this.field.params = this.field.params || {}
					if (Object.keys(this.field.params).length <= 0 || (Object.keys(this.field.params).length == 1 && this.field.params['ID_PARENT_CODE'])) {
						this.callService(params, (response) => {
							if (response.data.length > 0) {
								this.field.itemList = response.data || []
								if (this.field.selectFirst) {
									if (Array.isArray(this.field.model)) {
										this.field.model.push(this.field.itemList[0])
									} else if (typeof this.field.model == 'object') {
										this.field.model = this.field.itemList[0]
									}
									this.ngChange()
								}
							}
						})
					} else {
						this.field.isDisabled = true
					}
				}

			}

			this.setModel()
			this.ngChange(true)

		}

		this.getDatetimeRangeModel = (value) => {
			let dates = value.split('|')
			return {startDate: moment(dates[0]).format('YYYY-MM-DD'), endDate: moment(dates[1]).format('YYYY-MM-DD')}
		}

		this.setModel = () => {
			let notFound = '<div class="not-found"><p>' + this.$translate('INFO_NOT_FOUND') + '</p></div>'
			if (['text', 'number', 'textArea', 'switch'].indexOf(this.field.typefield) >= 0) {
				this.field.model = (this.field.data[0]) ? this.field.data[0].value : ((this.field.isDisabled && this.field.typefield == 'textArea') ? notFound : null)
			} else if (['datetime', 'datetimeRange'].indexOf(this.field.typefield) >= 0) {
				if (this.field.typefield == 'datetimeRange') {
					this.field.model = (this.field.data[0] && this.field.data[0].value) ? this.getDatetimeRangeModel(this.field.data[0].value) : ''
					this.field.date = this.field.model
				} else {
					this.field.model = (this.field.data[0] && this.field.data[0].value) ? moment(this.field.data[0].value).format('YYYY-MM-DD') : ''
					this.field.date = (this.field.data[0] && this.field.data[0].value && this.field.data[0].value.length > 0) ? moment(this.field.data[0].value.substring(0, 19)) : ''
				}
			}
			this.field.initialState = angular.copy(this.field.model)
		}

		$scope.$watch('inputCtrl.field.date', (date) => {
			if (date !== undefined && (['datetime', 'datetimeRange'].indexOf(this.field.typefield) >= 0)) {
				if (this.field.date) {
					this.field.model = (this.field.typefield == 'datetime') ? this.field.date.format('YYYY-MM-DD') :this.field.date
					this.ngChange()
				}
			}
		})

		this.ngChange = (cleanModel) => {
			this.field.model = (this.field.model === undefined) ? null : this.field.model
			this.onChange({field: this.field, cleanModel: cleanModel})
		}

		this.callService = (params, callback) => {
			if ((!this.field.pathService || this.field.pathService == '') || this.field.initialDisabled) {
				callback({data: []})
				return
			}
			let url = Array.isArray(this.field.pathService) ? this.field.pathService : ((this.baseServiceUrl) ? (this.baseServiceUrl + this.field.pathService) : [])

			if (Array.isArray(url)) {
				callback({data: url})
			} else if (this.field.pathService !== undefined && this.field.pathService !== null && this.field.pathService != '') {
				$http({
					method: "GET", 
					url: url,
					params: params
				}).then((response) => {

					if (!response.data.status) {
						callback({data: []})
						return
					} else {
						let data = []

						if (Array.isArray(response.data.data)) {
							response.data.data.forEach((item) => {
								let row = Functions.getItemForModel(this.field, item)
								data.push(row)
							})
						}

						callback({data: data})
					}
				}, (error) => {
					callback({data: []})
					return
				})
			} else {
				callback({data: []})
				return
			}
		}

		this.searching = (text) => {
			if (text.length > 0) {
				let params = angular.extend({}, this.additionalParams, (this.field.addParams || {}))
				params.text = text
				this.field.itemList = []

				this.callService(params, (response) => {
					if (response.data.length > 0) {
						this.field.itemList = response.data
					}
				})
			}
		}

		this.addItem = (text) => {
			let item = {}
			if (this.selectedItem !== null) {
				item = Functions.getItemForModel(this.field, this.selectedItem)
			} else if (text) {
				item[this.conf.name] = text
				item[this.conf.id] = null
				item = Functions.getItemForModel(this.field, item)
			}
			this.field.model = this.field.model || []
			this.field.model.push(item)
			this.ngChange()
		}

		this.deleteItem = (i) => {

			let opt = {
				message: "WARNING_MESSAGE_DELETE_DEFAULT",
				title: this.field.model[i].ds_name
			}

			Dialogs.modalConfirm(opt).then((response) => {
				if (response) {
					this.field.model.splice(i, 1)
					this.ngChange()
				}
			})
		}

		this.querySearch = (text) => {
			let matcher = new RegExp(text,'gi')
			let arr = []

			this.field.itemList.forEach((item) => {
				if (item.ds_name.match(matcher)) {
					arr.push(item)
				}
			})
			return arr
		}

		this.toolTip = () => {
			if ((this.field.label == 'MAIN_SECTION_TEACHER_MAIL' || this.field.label == 'MAIN_SECTION_TEACHER_PHONE') && !this.field.isDisabled )  {
				return true
			}
			return false
		}

		this.selecteAll = (check) => {
			this.field.itemList = this.field.itemList || []
			this.field.model = []

			if (check) {
				for (let i in this.field.itemList) {
					this.field.model.push(this.field.itemList[i])
				}
			}
		}

	}]
}