/**
* @name APP u-ClassPanel
* @autor over.martinez@u-planner.com
* @description
* Controller to confirmations
* @controller 
* # name: confirmationController
* # alias: dialog
*/

export default class confirmationController {

	constructor($scope, $mdDialog, options) {
		this.$scope = $scope
		this.$mdDialog = $mdDialog
		this.options = options
	}

	close(response) {
		this.$mdDialog.hide(response)
	}

}

confirmationController.$inject = ['$scope', '$mdDialog', 'options']