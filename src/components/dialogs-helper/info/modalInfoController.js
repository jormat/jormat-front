/**
* @name APP u-Planner
* @autor over.martinez@u-planner.com
* @description
* Generic modal to show any info
* @controller 
* # name: modalInfoController
* # alias: dialog
*/

export default class modalInfoController {

	constructor($scope, $mdDialog, options) {
		this.$scope = $scope
		this.$mdDialog = $mdDialog
		this.options = options
	}

	close() {
		this.$mdDialog.hide()
	}

}

modalInfoController.$inject = ['$scope', '$mdDialog', 'options']