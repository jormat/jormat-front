import confirmationController from './confirm/confirmationController'
import descriptionController from './description/descriptionController'
import modalTableController from './modalTable/modalTableController'
import modalInfoController from './info/modalInfoController'

class Dialogs {
	constructor ($mdDialog) {

		return {

			modalConfirm: (opt) => {
				return $mdDialog.show($mdDialog.confirm({
					controller   : confirmationController,
					controllerAs : 'dialog',
					template  	 : require('./confirm/confirmation.html'),
					locals       : {options : opt},
					animation    : true,
					parent       : angular.element(document.body),
					clickOutsideToClose : false
				}))
			},

			modalEnterDescription: (opt) => {
				return $mdDialog.show($mdDialog.confirm({
					controller   : descriptionController,
					controllerAs : 'dialog',
					template  	 : require('./description/description.html'),
					locals       : {options : opt},
					animation    : true,
					parent       : angular.element(document.body),
					clickOutsideToClose : false
				}))
			},

			modalTable: (opt) => {
				return $mdDialog.show($mdDialog.confirm({
					controller   : modalTableController,
					controllerAs : 'dialog',
					template  	 : require('./modalTable/modalTable.html'),
					locals       : {options : opt},
					animation    : true,
					parent       : angular.element(document.body),
					clickOutsideToClose : false
				}))
			},

			info: (opt) => {
				return $mdDialog.show($mdDialog.confirm({
					controller   : modalInfoController,
					controllerAs : 'dialog',
					template  	 : require('./info/info.html'),
					locals       : {options : opt},
					animation    : true,
					fullscreen   : opt.fullscreen || false,
					parent       : angular.element(document.body),
					clickOutsideToClose : false
				}))
			}

		}
	}
}

Dialogs.$inject = ['$mdDialog']
export default angular.module('services.Dialogs', []).service('Dialogs', Dialogs).name