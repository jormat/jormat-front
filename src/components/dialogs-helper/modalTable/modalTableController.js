/**
* @name APP u-Improve
* @autor over.martinez@u-planner.com
* @description
* Descripcion del controlador
* @controller 
* # name: modalTableController
* # alias: dialog
*/

export default class modalTableController {

	constructor($scope, $state, $stateParams, $mdDialog, options) {
		this.$state = $state
		this.$scope = $scope
		this.$stateParams = $stateParams
		this.$mdDialog = $mdDialog
		this.optDefaults = {
			title: "Detalle",
			message: "Detalles",
			field: {
				rows: [[{displayName: "None", value: "Sin Informacion"}]], 
			}
		}
		this.options = angular.extend({}, this.optDefaults, options)
	}

	close() {
		this.$mdDialog.hide()
	}

}

modalTableController.$inject = ['$scope', '$state', '$stateParams', '$mdDialog', 'options']