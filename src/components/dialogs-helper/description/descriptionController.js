/**
* @name APP u-Improve
* @autor over.martinez@u-planner.com
* @description
* Descripcion del controlador
* @controller 
* # name: descriptionController
* # alias: dialog
*/

export default class descriptionController {

	constructor($scope, $mdDialog, options) {
		this.$scope = $scope
		this.$mdDialog = $mdDialog
		this.options = options
		this.taOptions = [['h1','h2','h3'],['bold','italics'],['ul','ol'],['justifyLeft','justifyCenter','justifyRight'],['undo','redo']]
		this.description = options.description
	}

	close(response,op) {
		this.$mdDialog.hide({agree: response,option: op, description: this.description})
	}

}

descriptionController.$inject = ['$scope', '$mdDialog', 'options']