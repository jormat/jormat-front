
/**
* @name planning
* @autor over.martinez@u-planner.com
* @description : Pool of commons functions to help development
* # planning
* @name factory: uFunctions
*/

class uFunctions {

	constructor($resource, SERVICE_URL_CONSTANT, $filter, $http, ngNotify) {
		this.$translate = $filter('translate')
		this.inputsArray = ['multipleSelect', 'multipleSelectSuggest', 'listGroup', 'table']
		this.inputsInLine = ['text', 'number', 'textArea', 'switch', 'datetime']
		this.inputsObject = ['select', 'selectSuggest']
		this.typefieldsNotEvaluate = ['ui-grid', 'history', 'structure']
		this.inputs = this.inputsInLine.concat(this.inputsObject, this.inputsArray)

		/**
		* Funcion para validar estructuras de arbol como la del syllabus
		**/
		this.checkStructureInputsRequire = (structure, errors, parent) => {
			structure = structure || []
			errors = errors || {status: true}
			parent = parent || 'MAIN_ACCORDION'
			errors[parent] = errors[parent] || {errors: []}

			for (let i in structure) {
				if (this.inputs.indexOf(structure[i].typefield) >= 0) {
					if (structure[i].isRequired) {

						if (this.inputsArray.indexOf(structure[i].typefield) >= 0) {
							/**
							* Validando los campos de tipo arreglo como ['multipleSelect', 'multipleSelectSuggest', 'listGroup']
							**/
							if (structure[i].data.length <= 0) {
								errors.status = false
								errors[parent].errors.push({
									label: this.$translate(structure[i].label),
									text: "Es requerido"
								})
							}
						} else if (this.inputsObject.indexOf(structure[i].typefield) >= 0) {
							/**
							* Validando los campos de tipo Object como ['select', 'selectSuggest']
							**/
							if (Object.keys(structure[i].data).length <= 0) {
								errors.status = false
								errors[parent].errors.push({
									label: this.$translate(structure[i].label),
									text: "Es requerido"
								})
							}
						} else {
							/**
							* Validando los campos lineales como ['text', 'number', 'textArea', 'switch', 'datetime']
							**/
							structure[i].data[0] = structure[i].data[0] || {}
							if (structure[i].data[0].value === undefined || structure[i].data[0].value === null || structure[i].data[0].value === '') {
								errors.status = false
								errors[parent].errors.push({
									label: this.$translate(structure[i].label),
									text: "Es requerido"
								})
							}
						}
					}
				} else if (this.typefieldsNotEvaluate.indexOf(structure[i].typefield) >= 0) {
					errors[parent].errors = errors[parent].errors || []
				} else {
					/**
					* Validacion recursiva
					**/
					let err = this.checkStructureInputsRequire(structure[i].data, errors, structure[i].label)
					errors[parent][structure[i].label] = err.errors
					errors.status = err.status
				}
			}
			return {errors: errors[parent], status: errors.status}
		}

		/**
		* Funcion generica para obtener los parametros para los servicios de listado de los dynamic-inputs
		**/
		this.getParam = (model, paramName) => {
			let p = {}
			if (Array.isArray(model)) {
				p[paramName] = model.map(item => item.id_code)
			} else if (typeof model == 'object') {
				if (model.id_code) {
					p[paramName] = model.id_code
				}
			} else {
				p[paramName] = model
			}
			return p
		}

		this.chunkData = (arrayList, limit) => {
			limit = limit || 2
			let chunk = {}
			let x = 0
			let cols = 0

			arrayList.data.forEach((sec) => {
				cols += sec.col || ((typeof sec.cols == 'string') ? parseInt(sec.cols) : null) || 1
				if (cols > limit) {
					cols = sec.col || ((typeof sec.cols == 'string') ? parseInt(sec.cols) : null) || 1
					x++
				}

				chunk[x] = chunk[x] || []
				chunk[x].push(sec)
			})
			return Object.values(chunk)
		}
	
		this.defineStructure = (section, order) => {
			if (!section) { return false }
			/**
			* Otros tipos de orden
			*  first | last | first-last | full | none
			**/
			order = order || 'first-last'
			let isPar = (section.data.length % 2) == 0 ? 1 : 2
			section.data = section.data.sort((a, b) => a.orderBy - b.orderBy)
			for (let i in section.data) {
				if (order == 'first-last') {
					if (i == 0 || i == (section.data.length-1)) {
						section.data[i].col = 2
						continue
					} 
					section.data[i].col = isPar != 0 ? isPar : 1
					isPar = 0
				} else if (order == 'full') {
					section.data[i].col = 2
				} else if (order == 'first') {
					if (i == 0) {
						section.data[i].col = 2
						continue	
					}

					section.data[i].col = isPar != 0 ? isPar : 1
					isPar = 0
				} else if (order == 'last') {
					if (i == (section.data.length-1)) {
						section.data[i].col = 2
						continue
					}

					section.data[i].col = isPar != 0 ? isPar : 1
					isPar = 0
				} else if (order == 'none') {
					section.data[i].col = 1
				}

			}
			section.data = this.chunkData(section)

			section.data.forEach((row) => {
				row.forEach((panel) => {
					if (panel.typefield == 'panel') {
						panel.data = this.definePanel(panel)
					} else if (panel.typefield == 'dynamicOccurrences') {
						panel.data.forEach((ocurrency) => {
							ocurrency.data = this.definePanel(ocurrency, true)
						})
					} else if (panel.typefield == 'occurrences') {
						panel.data.forEach((ocurrency) => {
							ocurrency.col = panel.col
							ocurrency.data = this.definePanel(ocurrency)
						})
					}
				})
			})

			return section
		}

		this.definePanel = (panel, isOcur) => {
			let items = panel.data.length

			panel.data = panel.data.sort((a, b) => a.orderBy - b.orderBy)
			panel.data.forEach((field) => {
				field.col = ((panel.col == 2 && items > 1)) ? 1 : 2
				field.col = (isOcur) ? 1 : field.col
				if (['occurrences', 'dynamicOccurrences'].indexOf(field.typefield) >= 0) {
					field.data = field.data || []
					field.data.forEach((acco) => {
						acco = this.defineStructure(acco, 'none')
					})
				}
			})

			panel.data = this.chunkData(panel)
			return panel.data
		}

		this.getItemForModel = (field, data = {}) => {
			let conf = {name: "ds_name", id: "id_code"}
			let localField = null
			let item = {}
			for (let f in data) {
				let fi = (['value', 'adminitstrativeName', 'teacherName', 'ModalityName', 'languajeName'].indexOf(f) >= 0) ? conf.name : f
				localField = (['id', 'idAdministrative', 'teacherId', 'modalityId', 'languajeId'].indexOf(fi) >= 0) ? conf.id : fi
				item[localField] = data[f]
			}

			field.addParams = field.addParams || {}
			if (Object.keys(field.addParams).length > 0) {
				for (let i in field.params){
					item[field.params[i]] = field.addParams[field.params[i]]
				}

			}

			return _.size(item) > 1 ? item : null
		}

		this.organizeElements = (section, order) => {
			if (!section) return false;
			section.data = section.data || []
			if (Array.isArray(section.data[0])) {
				return section
			}

			/**
			* Otros tipos de orden
			*  first | last | first-last | full | none
			**/
			order = order || 'first-last'
			let isPar = (section.data.length % 2) == 0 ? 1 : 2
			section.data = section.data.sort((a, b) => a.orderBy - b.orderBy)

			for (let i in section.data) {
				if (order == 'first-last') {
					section.data[i].col = isPar != 0 ? isPar : 1
					isPar = 0

					if (i == 0 || i == (section.data.length-1)) {
						section.data[i].col = 2
					} 

				} else if (order == 'full') {

					section.data[i].col = 2

				} else if (order == 'first') {
					section.data[i].col = isPar != 0 ? isPar : 1
					isPar = 0

					if (i == 0) {
						section.data[i].col = 2
					}

				} else if (order == 'last') {
					if (i == (section.data.length-1)) {
						section.data[i].col = isPar == 1 ? 1 : 2
					} else {
						section.data[i].col = 1
					}

				} else if (order == 'none') {

					section.data[i].col = 1

				}

			}

			section.data = this.chunkData(section)
			return section
		}

		this.saveChanges = (opt, params, callback) => {
			callback = callback || (() => {})
			$http({
				url: opt.url,
				method: opt.method || "POST",
				data: params
			}).then((response) => {
				if (response.data.status) {
					ngNotify.set(this.$translate('UPDATE_SUCCESS_MESSAGE'), 'success')
					callback(true)
				} else {
					callback(response.data)
					ngNotify.set('Ocurrio un error con el servicio', 'error')
				}
			}, (error) => {
				callback(false)
				ngNotify.set('Imposible conectar con el servicio ' + opt.url, 'error')
			})
		}

		return {
			checkStructureInputsRequire: this.checkStructureInputsRequire,
			getParam: this.getParam,
			chunkData: this.chunkData,
			defineStructure: this.defineStructure,
			definePanel: this.definePanel,
			getItemForModel: this.getItemForModel,
			organizeElements: this.organizeElements,
			saveChanges: this.saveChanges,
		}
	}
}

uFunctions.$inject=['$resource','SERVICE_URL_CONSTANT', '$filter', '$http', 'ngNotify']

export default angular.module('services.uFunctions', [])
.service('uFunctions', uFunctions)
.name;