import mainTemplate from './list-filter.html'

export let listFilter = {
	bindings: {
		list: "<",
		onClick: "&",
		filters: "<",
		options: "<",
		functions: "<",
		onToggleHide: "&",
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'uFunctions', '$filter', '$timeout', 
	function ($scope, $rootScope, $templateCache, uFunctions, $filter, $timeout) {
		$templateCache.put('extended-view.html', require('./templates/extended-view.html'))
		$templateCache.put('compacted-view.html', require('./templates/compacted-view.html'))
		$templateCache.put('simpled-view.html', require('./templates/simpled-view.html'))
		$templateCache.put('generic-view.html', require('./templates/generic-view.html'))
		$templateCache.put('optimal-view.html', require('./templates/optimal-view.html'))
		$templateCache.put('coordinatorStudent-view.html', require('./templates/coordinatorStudent-view.html'))
		this.showFilters = true
		this.showAdvancedFilters = false
		this.filters = this.filters || {}
		const Helpers = uFunctions
		const $translate = $filter('translate')
		const views = ['extended', 'compacted', 'simpled', 'generic', 'optimal', 'coordinatorStudent']

		this.$onInit = () => {
			this.records = []
			this.alreadyFilter = []
			this.onToggleHide = this.onToggleHide || (() => {})
			this.filters.inputs = this.filters.inputs || []
			this.optDefault = {title: false, view: "generic", itemSelectedId: null, cache: true, pageSize: 50, page: 1}
			this.config = angular.extend({}, this.optDefault, this.options)
		}

		this.onClickList = (item) => {
			this.itemSelected = item.id_code
			this.Item = angular.copy(item)
			if (this.onClick !== undefined) {
				this.onClick({item: this.Item})
			}
		}

		$rootScope.$on('$translateChangeSuccess', () => {
			this.setLangLabels()
		})

		this.setLangLabels = () => {
			this.records = this.records || []
			for (let i in this.records) {
				this.records[i].rolLabel = this.getRolLabels(this.records[i])
			}
		}

		$scope.$watch('Ctrl.list', (list) => {
			if (list !== undefined) {
				this.main() 
			}
		})

		this.getRolLabels = (item) => {
			item.rolLabels = item.rolLabels || []
			return {
				codes: item.rolLabels.map((item) => item.id_code).join(' - '),
				labels: item.rolLabels.map((item) => $translate(item.ds_name)).join(' - '),
			}
		}

		this.main = () => {
			let inputDefault = false
			this.filters.inputs.forEach((input) => {
				if (input.typefield == 'text') {
					inputDefault = true
				}
			})

			if (!inputDefault) {
				this.filters.inputs.push({label: "SEARCH_TEXT", typefield: "text", data: []})
			}
			
			this.config.page = 1
			this.list = this.list || []
			this.alreadyFilter = angular.copy(this.list)
			let alreadySet = false
			if (this.alreadyFilter.length > 0) {
				if (this.config.itemSelectedId) {
					for (let i in this.alreadyFilter) {
						if (this.alreadyFilter[i].id_code == this.config.itemSelectedId || this.alreadyFilter[i].sectionId == this.config.itemSelectedId) {
							this.onClickList(this.alreadyFilter[i])
							alreadySet = true
						}
					}
				}
				if (!alreadySet) {
					this.onClickList(this.alreadyFilter[0])
				}
			}
			this.records = []
			this.loadMore()
		}

		this.loadMore = () => {
			if (this.config.cache) {
				this.loadFromCache()
			} else {
				this.alreadyFilter = angular.copy(this.list)
			}
		}

		this.loadFromCache = () => {
			let x = 0
			let current = this.records.length
			for (let i in this.alreadyFilter) {
				let index = ((this.config.page*this.config.pageSize)-this.config.pageSize)+x
				if (i < index) {continue;}

				if (x < this.config.pageSize) {
					this.records[index] = this.alreadyFilter[i]
					x++
				} else if (current != this.records.length) {
					this.config.page++
					break
				}
			}
			this.setLangLabels()
		}

		this.areThereAdvanced = () => {
			for (let i in this.filters.inputs) {
				if (this.filters.inputs[i].isAdvanced) {
					return true
				}
			}
			return false
		}
		
		this.validatingTypeView = () => {
			if (views.indexOf(this.config.view) >= 0) {
				return false
			}
			return true
		}

		this.onChange = (field, cleanModel) => {
			this.filters.inputs.forEach((f) => {
				f.params = f.params || {}
				for (let i in f.params) {
					if (i == field.label) {
						f.model = !cleanModel ? null : f.model
						if (field.model !== null) {
							f.addParams = angular.extend({}, f.addParams, Helpers.getParam(field.model, f.params[i]))
							f.reload = !f.reload
							f.callSrv = true
						} else {
							f.addParams = f.addParams || {}
							f.reload = !f.reload
							f.callSrv = false
						}
					}
				}
				if (typeof f.pathService == 'object' && !Array.isArray(f.pathService)) {
					let index = f.pathService[field.label]
					if (index) {
						if (field.model) {
							f.model = null
							f.itemList = []
							if (Array.isArray(field.model[index])) {
								field.model[index].forEach((item) => {
									let row = Helpers.getItemForModel({}, item)
									f.itemList.push(row)
								})
							}
							f.reloadList = !f.reloadList
							f.isDisabled = f.initialDisabled
						} else {
							f.model = null
							f.itemList = []
							f.reloadList = !f.reloadList
						}
					}
				}
			})
			if (typeof field.onChange == 'function'){
				field.onChange(field.model)
			} else {
				this.alreadyFilter = []
				if (!this.filtering) {
					this.filtering = true
					this.config.page = 1
					$timeout(() => {
						let x = 0
						for (let i in this.list) {
							if (this.getFiltering(this.list[i])) {
								this.alreadyFilter[x] = this.list[i]
								x++
							}
						}
						this.filtering = false
						this.records = []
						this.loadMore()
					}, 400)
				}
			}
		}

		this.searchInField = (field, text, list) => {
			let excludeFields = ['$$hashKey', 'id_code']
			let search = new RegExp(text, 'gi')
			for (let i in list) {
				if (excludeFields.indexOf(i) >= 0) { continue; }
				if (list[i]) {
					if (['string', 'number'].indexOf(typeof list[i]) >= 0 && list[i].toString().match(search)) {
						return true
					} else if (i == 'descriptionList') {
						for (let w in list[i]) {
							if (list[i][w] && list[i][w].value && list[i][w].value.toString().match(search)) {
								return true
							}
						}
					} else if (i == 'badges') {
						for (let w in list[i]) {
							if (list[i][w] && list[i][w].label && list[i][w].label.toString().match(search)) {
								return true
							}
						}
					} else if (i == 'rolLabel') {
						if (list[i] && list[i].labels.toString().match(search)) {
							return true
						}
					} else if (Array.isArray(list[i])) {
						for (let w in list[i]) {
							if (list[i][w].toString().match(search)) {
								return true
							}
						}
					}
				}
			}
			return false
		}

		this.getFiltering = (list) => {
			for (let x in this.filters.inputs) {
				let field = this.filters.inputs[x]
				/**
				* Buscando coincidencia con los tipo text y number dentro de la lista de secciones
				**/
				if ((field.typefield == 'text')||(field.typefield == 'number')) {
					if (field.model !== undefined && field.model !== null && field.model != '') {
						return this.searchInField(field, field.model.toString(), list)
					}
				}

				/**
				* Buscando coincidencia con los tipo select dentro de la lista de secciones
				**/
				if (field.typefield == 'select') {
					if (typeof field.onChange != 'function') {
						if (field.model && field.model.ds_name !== undefined && field.model.ds_name !== null && field.model.ds_name != '') {
							return this.searchInField(field, field.model.ds_name.toString(), list)
						}
					}
				}

				/**
				* Buscando coincidencia con los tipo multipleSelect dentro de la lista de secciones
				**/
				if (field.typefield == 'multipleSelect') {
					if (typeof field.onChange != 'function'){
						if (field.model.length > 0) {
							for (let y in field.model) {
								if (this.searchInField(field, field.model[y].ds_name.toString(), list)) {
									return true
								}
							}
							return false
						}
					}
				}
			}
			return true
		}

		this.isMultiTitle = (title) => {
			if (Array.isArray(title)) {
				return true
			}
			return false
		}

		this.toggleHide = () => {
			this.listToggle = !this.listToggle
			this.onToggleHide({isHide: this.listToggle})
		}

	}]
}