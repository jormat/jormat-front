import mainTemplate from './templates/u-dynamic-panel.html'

export let uDynamicPanel = {
	bindings: {
		panel: "<",
		options: "<",
		parent: "<",
		onContentChange: "&", 
		onSubmit: "&", 
		baseServiceUrl: "<",
		autoSave: "<",
		addParams: "<", 
		emptySubstructureFolter: "@", 
	},
	template: mainTemplate,
	controllerAs: 'panelCtrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'uFunctions', '$http',
	function ($scope, $rootScope, $templateCache, uFunctions, $http) {
		const Functions = uFunctions
		const inputsArray  = ['multipleSelect', 'multipleSelectSuggest', 'listGroup', 'table']
		const inputsInLine = ['text', 'number', 'textArea', 'switch']
		const inputsObject = ['select', 'selectSuggest']
		const inputsContent = ['structure']
		const inputsDatetime = ['datetime']
		const allInputs = inputsInLine.concat(inputsObject, inputsArray, inputsContent, inputsDatetime)
		
		this.$onInit = () => {
			$scope.showBtnActions = true
			this.onSubmit = this.onSubmit || (() => {})
			this.panel = this.panel || {}
			this.panel.data = this.panel.data || []
			this.panel.grandParentInfo = {
				id_item: this.parent.id_item,
				id_code: this.parent.id_code,
				typefield: this.parent.typefield,
				setService: this.parent.setService, 
				label: this.parent.label, 
				grandParentInfo: this.parent.grandParentInfo
			}
			this.uTreeOptions = {
				inEdit: true,
				getEmptyElement: this.getEmptyElement, 
				addParams: this.addParams,
				baseUrl: this.baseServiceUrl
			}

			let children = 0
			this.panel.data.forEach((item) => {
				children++
				if (item.typefield == 'occurrences' || item.typefield == 'dynamicOccurrences') {
					$scope.showBtnActions = false
					return
				}
			})
			this.parent = this.parent || {}
			let order = (this.parent.typefield == 'mainAccordion' && $scope.showBtnActions && children > 1) ? 'none' : 'full'
			this.panel.data = this.panel.data || []
			this.panel.data.forEach((f) => {
				f.params = f.params || {}
				f.addParams = f.addParams || {}
				for (let i in f.params) {
					if (i == 'ID_PARENT_CODE') {
						f.addParams[f.params[i]] = this.panel.id_code
					}
				}
			})
			this.panel = Functions.organizeElements(this.panel, order)
		}

		this.onInputChange = (field, cleanModel) => {
			this.panel.data.forEach((row) => {
				row.forEach((f) => {
					f.params = f.params || {}
					for (let i in f.params) {
						if (i == field.label) {
							f.model = !cleanModel ? null : cleanModel
							if (field.model !== null) {
								f.addParams = angular.extend(Functions.getParam(field.model, f.params[i]))
								if (Object.keys(f.addParams).length > 0) {
									f.reload = !f.reload
									f.callSrv = true
								}
							} else {
								f.addParams = {}
								f.reload = !f.reload
								f.callSrv = false
							}
						}
					}
				})
			})

			this.makeModel((panel) => {
				this.onContentChange({panel: panel})
			})
		}

		this.makeModel = (callback) => {
			callback = callback || (() => {})
			this.panel.model = {}
			this.panel.data.forEach((row) => {
				row.forEach((field) => {
					if (allInputs.indexOf(field.typefield) >= 0) {
						if (!field.isDisabled && !angular.equals(field.model, field.initialState)) {
							this.panel.model[field.label] = field.model || field.structure || field.rows
						}
					} else {
						for (let i in field.model) {
							this.panel.model[i] = field.model[i]
						}
					}
				})
			})

			callback(this.panel)
		}

		this.getEmptyElement = (callback) => {
			callback = callback || (() => {})
			let params = angular.extend({}, this.addParams, {subStructureId: this.panel.id_item})
			let folder = this.emptySubstructureFolter || 'syllabus'
			
			$http({
				url: this.baseServiceUrl + '/' + folder + '/getEmptySubStructure',
				method: "GET",
				params: params
			}).then((response) => {
				if (response.data.status) {
					response.data.data[0] = response.data.data[0] || {}
					response.data.data[0].data = response.data.data[0].data || []
					let item = response.data.data[0].data[0] || {}

					callback({data: item})
				}
			}, (error) => {
				this.ngNotify.set('Imposible conectar con el servicio getEmptySubStructure', 'error')
			})
		}

		this.onSubmitChanges = () => {
			this.makeModel((panel) => {
				this.onSubmit({panel: panel})
			})
		}

	}]
}