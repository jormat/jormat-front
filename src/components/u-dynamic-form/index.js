import mainTemplate from './templates/u-dynamic-form.html'

export let uDynamicForm = {
	bindings: {
		sectionData: "<",
		sectionModel: "=",
		additionalParams: '=',
		baseServiceUrl: '<',
		onSubmit: "&",
		onOccurrenceSubmit: "&",
		onOccurrenceDelete: "&",
		typeOrder: "@",
		singleSave: "<",
		emptySubstructureFolter: "@"
	},
	template: mainTemplate,
	controllerAs: 'formCtrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'Dialogs', 'uFunctions', 'UserInfoConstant', '$http',
	function ($scope, $rootScope, $templateCache, Dialogs, uFunctions, UserInfoConstant, $http) {
		const Functions = uFunctions
		const inputsArray = ['multipleSelect', 'multipleSelectSuggest', 'listGroup']
		const inputsInLine = ['text', 'number', 'textArea']
		const inputsObject = ['select', 'selectSuggest']
		const allInputs = inputsArray.concat(inputsInLine, inputsObject)
		
		this.$onInit = () => {
			this.singleSave = false
			this.onSubmit = this.onSubmit || (() => {})
			this.onOccurrenceSubmit = this.onOccurrenceSubmit || (() => {})
			this.onOccurrenceDelete = this.onOccurrenceDelete || (() => {})
			this.sectionModel = this.sectionModel || {}
			this.addParams = this.additionalParams || {}
			/**
			* Otros tipos de orden
			*  first | last | first-last | full | none
			**/
			this.order = this.typeOrder || 'full'
			this.sectionData = this.sectionData || {}
			this.sectionData.data = this.sectionData.data || []
			this.sectionData.data.forEach((main) => {
				if (main.typefield == 'occurrences' || main.typefield == 'dynamicOccurrences') {
					this.singleSave = true
					return
				} else if (main.typefield == 'panel') {

					main.data = main.data || []
					main.data.forEach((item) => {
						if (item.typefield == 'occurrences' || item.typefield == 'dynamicOccurrences') {
							this.singleSave = true
							return
						}
					})
				}
			})
			this.section = Functions.organizeElements(this.sectionData, this.order)
		}

		this.onPanelContentChange = (panel) => {
			this.makeModel((section) => {
				this.sectionModel = section.model
			})
		}

		this.makeModel = (callback) => {
			callback = callback || (() => {})
			this.section.model = {}
			this.section.data.forEach((row) => {
				row.forEach((field) => {
					if (allInputs.indexOf(field.typefield) >= 0) {
						if (!field.isDisabled && !angular.equals(field.model, field.initialState)) {
							this.section.model[field.label] = (field.model !== false) ? (field.model || field.structure || field.rows || null) : false
						}
					} else if (field.typefield == 'panel') {
						for (let i in field.model) {
							this.section.model[field.label] = this.section.model[field.label] || {}
							this.section.model[field.label][i] = field.model[i]
						}
					}
				})
			})
			callback(this.section)
		}

		this.onPanelSubmit = (panel) => {
			let params = angular.copy(this.additionalParams)
			params.data = {}
			
			for (let i in panel.model) {
				if (Array.isArray(panel.model[i]) && panel.model[i].length > 0) {
					params.data[i] = panel.model[i]
				} else if (typeof panel.model[i] == 'object') {
					params.data[i] = panel.model[i].id_code
				} else {
					params.data[i] = panel.model[i]
				}
			}
			let opt = {
				url: this.baseServiceUrl + this.section.setService,
				method: "POST", 
			}
			
			Functions.saveChanges(opt, params, (response) => {
				console.log('response->', response)
			})

		}

		this.submit = () => {
			let params = angular.copy(this.additionalParams)

			params.data = {}
			this.section.data.forEach((rows) => {
				rows.forEach((content) => {
					for (let i in content.model) {
						if (content.model[i]) {
							params.data[i] = content.model[i]
						}
					}
				})
			})
			let opt = {
				url: this.baseServiceUrl + this.section.setService,
				method: "POST", 
			}
			
			Functions.saveChanges(opt, params, (response) => {
				console.log('response->', response)
			})
		}

	}]
}