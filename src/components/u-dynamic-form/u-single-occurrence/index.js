import mainTemplate from './templates/u-single-occurrence.html'

export let uSingleOccurrence = {
	bindings: {
		options: "<",
		parent: "<", 
		occurrence: "<",
		baseServiceUrl: "<",
		addParams: "<",
		onDelete: "&",
		onSubmit: "&", 
		singleSave: "<",
		index: "<",
		emptySubstructureFolter: "@", 
		rowKey: "<"
	},
	template: mainTemplate,
	controllerAs: 'singleOccCtrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'uFunctions',
	function ($scope, $rootScope, $templateCache, uFunctions) {
		const Functions = uFunctions
		const inputsArray  = ['multipleSelect', 'multipleSelectSuggest', 'listGroup', 'table']
		const inputsInLine = ['text', 'number', 'textArea', 'switch']
		const inputsObject = ['select', 'selectSuggest']
		const inputsDatetime = ['datetime']
		const allInputs = inputsInLine.concat(inputsObject, inputsArray, inputsDatetime)

		this.$onInit = () => {
			$scope.showBtnActions = true
			this.occurrence.grandParentInfo = {
				id_item: this.parent.id_item,
				id_code: this.parent.id_code,
				typefield: this.parent.typefield,
				label: this.parent.label,
				setService: this.parent.setService, 
				grandParentInfo: this.parent.grandParentInfo
			}
			this.uTreeOptions = {
				inEdit: true,
				addParams: this.addParams,
				baseUrl: this.baseServiceUrl
			}
			this.options = this.options || {}
			this.onDelete = this.onDelete || (() => {})
			this.onSubmit = this.onSubmit || (() => {})
			this.occurrence = this.occurrence || {}
			this.occurrence.data = this.occurrence.data || []
			this.occurrence.data.forEach((item) => {
				if (item.typefield == 'occurrences' || item.typefield == 'dynamicOccurrences') {
					$scope.showBtnActions = false
					return
				}
			})

			this.occurrence.data = this.occurrence.data || []
			this.occurrence.data.forEach((f) => {
				f.params = f.params || {}
				f.addParams = f.addParams || {}
				for (let i in f.params) {
					if (i == 'ID_PARENT_CODE') {
						f.addParams[f.params[i]] = this.occurrence.id_code
					}
				}
			})

			this.occurrence = Functions.organizeElements(this.occurrence, 'none')
		}

		this.onFieldChange = (field, cleanModel) => {
			let localParams = angular.extend({}, {occurrenceId: this.occurrence.id_code})

			this.occurrence.data.forEach((row) => {
				row.forEach((f) => {
					f.params = f.params || {}
					for (let i in f.params) {
						if (i == field.label) {
							f.model = !cleanModel ? null : cleanModel
							if (field.model !== null) {
								f.addParams = angular.extend(Functions.getParam(field.model, f.params[i]), localParams)
								if (Object.keys(f.addParams).length > 1) {
									f.reload = !f.reload
									f.callSrv = true
								}
							} else {
								f.addParams = {}
								f.reload = !f.reload
								f.callSrv = false
							}
						}
					}
				})
			})
			this.makeModel()
		}

		this.makeModel = (callback) => {
			callback = callback || (() => {})
			this.occurrence.model = {}
			this.occurrence.data.forEach((row) => {
				row.forEach((field) => {
					if (allInputs.indexOf(field.typefield) >= 0) {
						if (!field.isDisabled && !angular.equals(field.model, field.initialState)) {
							this.occurrence.model[field.label] = (field.model !== false) ? (field.model || field.structure || field.rows || null) : false
						}
					} else {
						for (let i in field.model) {
							this.occurrence.model[i] = field.model[i]
						}
					}
				})
			})
			callback(this.occurrence)
		}

		this.onOccurrenceDelete = () => {
			this.onDelete({occurrence: this.occurrence, index: this.index})
		}

		this.onOccurrenceSubmit = () => {
			this.onSubmit({occurrence: this.occurrence})
		}

		this.onPanelSubmit = (panel) => {
			console.log('-------------------------------------------------------------')
			console.log('panel->', panel)
			console.log('-------------------------------------------------------------')
		}

	}]
}