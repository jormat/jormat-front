import mainTemplate from './templates/u-dynamic-occurrence.html'

export let uDynamicOccurrences = {
	bindings: {
		occurrences: "<",
		addParams: '<',
		baseServiceUrl: "<",
		parent: "<",
		autoSave: "<",
		options: "<",
		emptySubstructureFolter: "@"
	},
	template: mainTemplate,
	controllerAs: 'OccsCtrl',
	controller: ['$scope', '$rootScope', '$templateCache', '$http', '$filter', 'ngNotify', 'Dialogs', 'uFunctions',
	function ($scope, $rootScope, $templateCache, $http, $filter, ngNotify, Dialogs, uFunctions) {
		const $translate = $filter('translate')
		const Functions = uFunctions

		this.$onInit = () => {
			this.newOccurrence = null
			this.globals = angular.copy(this.addParams)
			this.occurrences = this.occurrences || {data: []}
			this.occurrences.data = this.occurrences.data || []
			let occu = this.splitAvailableData(this.occurrences.data)
			this.occurrences.data = occu.current
			this.occurrences.availableList = occu.available
		}

		this.splitAvailableData = (data) => {
			let available = []
			let current = []
			data = data || []
			for (let i in data) {

				if (data[i].hasData == 0) {
					available.push(data[i])
				} else {
					current.push(data[i])
				}
			}
			return {current: current, available: available}
		}

		this.addOccurrenceStatic = () => {
			this.occurrences.data.push(this.newOccurrence)
			for (let i in this.occurrences.availableList) {
				if (this.occurrences.availableList[i].id_code == this.newOccurrence.id_code) {
					this.occurrences.availableList.splice(i, 1)
				}
			}
			this.newOccurrence = null
		}

		this.addOccurrence = () => {
			let params = angular.extend({}, this.addParams, {subStructureId: this.occurrences.id_item})
			let folder = this.emptySubstructureFolter || 'syllabus'
			$http({
				url: this.baseServiceUrl + '/' + folder + '/getEmptySubStructure',
				method: "GET",
				params: params
			}).then((response) => {
				if (response.data.status) {
					this.occurrences.data.push(response.data.data[0] || {})
				}
			}, (error) => {
				this.ngNotify.set('Imposible conectar con el servicio getEmptySubStructure', 'error')
			})
		}

		this.saveOccurrence = (occurrence) => {
			this.parent = this.parent || {}
			let params = angular.copy(this.globals)
			params.data = this.getParams(occurrence.model)
			params.subStructureId = occurrence.id_item
			params.occurrenceId = (this.parent.id_code) ? [this.parent.id_code, occurrence.id_code] : occurrence.id_code

			let opciones = {
				url: this.baseServiceUrl + this.occurrences.setService
			}
			Functions.saveChanges(opciones, params)
		}

		this.deleteOccurrence = (occurrence, index) => {
			let opt = {
				title: "WARNING_MESSAGE_DELETE_DEFAULT",
				message: occurrence.label
			}
			Dialogs.modalConfirm(opt).then((response) => {

				if (response) {
					this.parent = this.parent || {}
					let params = angular.copy(this.globals)
					params.data = this.getParams(occurrence.model)
					params.data.isActive = 0
					params.subStructureId = occurrence.id_item
					params.occurrenceId = (this.parent.id_code) ? [this.parent.id_code, occurrence.id_code] : occurrence.id_code || false
					
					if (!params.occurrenceId) {
						this.occurrences.data.splice(index, 1)
					} else {
						let opciones = {
							url: this.baseServiceUrl + occurrence.grandParentInfo.setService
						}
						Functions.saveChanges(opciones, params, (response) => {
							if (response) {
								this.occurrences.data.splice(index, 1)
							}
						})
					}
				}

			})
		}

		this.getParams = (model) => {
			let params = {}
			model = model || {}

			for (let i in model) {
				if (Array.isArray(model[i])) {
					params[i] = model[i]
				} else if (typeof model[i] == 'object') {
					if (model[i]) {
						if (model[i].id_code) {
							params[i] = model[i]
						} else {
							params[i] = this.getParams(model[i])
						}
					}
				} else {
					if (model[i]) {
						params[i] = model[i]
					}
				}
			}
			return params
		}

	}]
}