import mainTemplate from './templates/main-template.html'

export let uPensumSignaturesEdit = {
	bindings: {
		period: "=",
		options: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 
	function ($scope, $rootScope, $templateCache) {

		this.$onInit = () => {
			this.courses = {
				label: "PENSUM_TABLE_PERIOD_COURSES",
				typefield: "table",
				isDisabled: false, 
				data: [], 
				rows: [],
				header: [
					{displayName: "FACULTY_DEPARTMENT", pathService: "", typefield: "selectSuggest"},
					{displayName: "IMPROVE_COURSE_NAME", pathService: "", params: {FACULTY_DEPARTMENT: "facultyDepartmentId"}, typefield: "select"},
					{displayName: "CREDITS", paramsVal: {IMPROVE_COURSE_NAME: "credits"}, isDisabled: true, typefield: "number"}
				]
			}
		}
	}]
}