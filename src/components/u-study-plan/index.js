import mainTemplate from './templates/main-template.html'

export let uStudyPlan = {
	bindings: {
		matrix: "<",
		options: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'Dialogs', '$filter',
	function ($scope, $rootScope, $templateCache, Dialogs, $filter) {
		this.currentStructure = []
		this.$translate = $filter('translate')

		this.$onInit = () => {
			if (this.matrix.data.length > 0) {
				this.currentStructure = angular.copy(this.matrix.data[0].courses)
				let courses = angular.copy(this.currentStructure)
				this.specials = this.matrix.data[0].specialties
				this.blocks = this.matrix.data[0].blocks
				this.academicPeriods = this.matrix.data[0].academicPeriods
				let table = this.buildingTable(courses, this.matrix.data[0].academicPeriods)
				this.Rows = table.rows
				this.Asignatures = table.asigns
			}
		}

		this.spliceItem = (course) => {
			for (let i in this.currentStructure) {
				if (_.isEqual(course, this.currentStructure[i])) {
					this.currentStructure.splice(i, 1)
					break
				}
			}
			return angular.copy(this.currentStructure)
		}

		this.showInPensum = (special) => {
			let oblig = []
			special.courses.forEach((course) => {
				if (special.checked) {
					course.showing = true
					this.currentStructure.push(course)
					oblig = angular.copy(this.currentStructure)
				} else {
					oblig = this.spliceItem(course)
				}
			})
			let table = this.buildingTable(oblig, this.matrix.data[0].academicPeriods)
			this.Rows = table.rows
			this.Asignatures = table.asigns
		}

		this.buildingTable = (Data, AcademicPeriods) => {
			let asigs = {}
			let cantRows = this.getRowsCount(Data)
			let Rows = _.range(cantRows)
			Rows.forEach((row) => {
				asigs[row] = asigs[row] || {}

				AcademicPeriods.forEach((sem) => {
					asigs[row][sem.periodId] = asigs[row][sem.periodId] || {}

					Data.forEach((item, index) => {
						if (item.periodsId.indexOf(sem.periodId) >= 0) {
							let asigned = false
							for (let i in item.periodsId) {

								if (i == 0) {

									if (asigs[row][item.periodsId[i]].visible === undefined) {
										item.visible = true
										asigs[row][sem.periodId] = item

										if (item.periodsId.length == 1) {
											Data.splice(index, 1)
										}

										asigned = true
									}

								} else {
									asigs[row][item.periodsId[i]] = asigs[row][item.periodsId[i]] || {}

									if (asigs[row][item.periodsId[i]].visible === undefined && asigned) {
										asigs[row][item.periodsId[i]] = {visible: false, periodsId: [item.periodsId[i]]}

										if ((item.periodsId.length - 1) == i) {
											Data.splice(index, 1)
										}
									}
								}
							}
						}
					})

					if (asigs[row][sem.periodId].visible === undefined) {
						asigs[row][sem.periodId] = {visible: true, periodsId: [sem.periodId]}
					}

				})

			})

			return {
				rows: Rows,
				asigns: Object.values(asigs)
			}
		}

		this.getColorBox = (item, comp) => {
			if (item.showing) return 'bg-warning'

			if (item.competencia_id == comp) {

				if (item.note === null || item.note === undefined) return 'bg-primary'
				if (item.note > 4.6) return 'bg-success'
				if (item.note > 3.6 && item.note <= 4.6) return 'bg-accent'
				if (item.note <= 3.6) return 'bg-danger'

			} else {
				return 'bg-primary'
			}
		}

		this.getRowsCount = (data) => {
			let list = {}
			data.forEach((item) => {
				for (let i in item.periodsId) {
					list[item.periodsId[i]] = list[item.periodsId[i]] || []
					list[item.periodsId[i]].push(1)
				}
			})

			let countRows = Object.values(list)

			return Math.max.apply(Math, countRows.map((el) => el.length))
		}

		this.onClickSpecials = (special) => {
			let opt = {
				title: this.$translate('DETAIL'),
				message: special.specialtyCode + ' - ' + special.specialtyName,
				field: {rows: []}
			}

			special.courses = special.courses || []
			special.courses.forEach((course) => {
				let col = []
				col.push({displayName: "COURSE_CODE", value: course.courseCode})
				col.push({displayName: "ASIGNATURE", value: course.courseName})
				col.push({displayName: "COURSE_CREDITS", value: course.credits})
				col.push({displayName: "SEMESTER", value: Array.isArray(course.semestre_name) ? course.semestre_name.join(' / ') : 'none'})
				opt.field.rows.push(col)
			})

			Dialogs.modalTable(opt)
		}

		this.onClickBlocks = (block) => {
			let opt = {
				title: block.blockCode + ' - ' + block.blockName,
				message: this.$translate('MIN_CREDITS') + ': ' + block.min_credits,
				field: {rows: []}
			}

			block.courses = block.courses || []
			block.courses.forEach((asign) => {
				let col = []
				col.push({displayName: "COURSE_CODE", value: asign.courseCode})
				col.push({displayName: "ASIGNATURE", value: asign.courseName})
				col.push({displayName: "COURSE_CREDITS", value: asign.credits})
				opt.field.rows.push(col)
			})

			Dialogs.modalTable(opt)
		}
	}]
}