import mainTemplate from './templates/main-template.html'

export let uComponentMatrix = {
	bindings: {
		matrix: "=",
		options: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'Dialogs', '$filter',
	function ($scope, $rootScope, $templateCache, Dialogs, $filter) {
		this.currentStructure = []
		this.$translate = $filter('translate')		

		this.$onInit = () => {
			this.currentStructure = angular.copy(this.matrix.data.obligatorias)
			let obligatorias = angular.copy(this.currentStructure)
			this.specials = this.matrix.data.especiales
			this.blocks = this.matrix.data.bloque
			this.semestres = this.matrix.data.semestres
			let table = this.buildingTable(obligatorias, this.matrix.data.semestres)
			this.Rows = table.rows
			this.Asignatures = table.asigns
		}

		this.spliceItem = (asign) => {
			for (let i in this.currentStructure) {
				if (_.isEqual(asign, this.currentStructure[i])) {
					this.currentStructure.splice(i, 1)
					break
				}
			}
			return angular.copy(this.currentStructure)
		}

		this.showInPensum = (special) => {
			let oblig = []
			special.asigns.forEach((asign) => {
				if (special.checked) {
					asign.showing = true
					this.currentStructure.push(asign)
					oblig = angular.copy(this.currentStructure)
				} else {
					oblig = this.spliceItem(asign)
				}
			})
			let table = this.buildingTable(oblig, this.matrix.data[0].semestres)
			this.Rows = table.rows
			this.Asignatures = table.asigns
		}

		this.buildingTable = (Data, Semestres) => {
			let asigs = {}
			let cantRows = this.getRowsCount(Data)
			let Rows = _.range(cantRows)

			Rows.forEach((row) => {
				asigs[row] = asigs[row] || {}

				Semestres.forEach((sem) => {
					asigs[row][sem.id_code] = asigs[row][sem.id_code] || {}

					Data.forEach((item, index) => {
						if (item.semestre_id.indexOf(sem.id_code) >= 0) {
							let asigned = false
							for (let i in item.semestre_id) {

								if (i == 0) {

									if (asigs[row][item.semestre_id[i]].visible === undefined) {
										item.visible = true
										asigs[row][sem.id_code] = item

										if (item.semestre_id.length == 1) {
											Data.splice(index, 1)
										}

										asigned = true
									}

								} else {
									asigs[row][item.semestre_id[i]] = asigs[row][item.semestre_id[i]] || {}

									if (asigs[row][item.semestre_id[i]].visible === undefined && asigned) {
										asigs[row][item.semestre_id[i]] = {visible: false, semestre_id: [item.semestre_id[i]]}

										if ((item.semestre_id.length - 1) == i) {
											Data.splice(index, 1)
										}
									}
								}
							}
						}
					})

					if (asigs[row][sem.id_code].visible === undefined) {
						asigs[row][sem.id_code] = {visible: true, semestre_id: [sem.id_code]}
					}

				})

			})

			return {
				rows: Rows,
				asigns: Object.values(asigs)
			}
		}

		this.getColorBox = (item, comp) => {
			if (item.showing) return 'bg-warning'

			if (item.competencia_id == comp) {

				if (item.note === null || item.note === undefined) return 'bg-primary'
				if (item.note > 4.6) return 'bg-success'
				if (item.note > 3.6 && item.note <= 4.6) return 'bg-accent'
				if (item.note <= 3.6) return 'bg-danger'

			} else {
				return 'bg-primary'
			}
		}

		this.getRowsCount = (data) => {
			let list = {}
			data.forEach((item) => {
				for (let i in item.semestre_id) {
					list[item.semestre_id[i]] = list[item.semestre_id[i]] || []
					list[item.semestre_id[i]].push(1)
				}
			})

			let countRows = Object.values(list)

			return Math.max.apply(Math, countRows.map((el) => el.length))
		}

		this.onClickSpecials = (special) => {
			let opt = {
				title: this.$translate('DETAIL'),
				message: special.ds_name,
				field: {rows: []}
			}

			special.asigns.forEach((asign) => {
				let col = []
				col.push({displayName: "COURSE_CODE", value: asign.ds_publicId})
				col.push({displayName: "ASIGNATURE", value: asign.ds_name})
				col.push({displayName: "COURSE_CREDITS", value: asign.credits})
				col.push({displayName: "SEMESTER", value: Array.isArray(asign.semestre_name) ? asign.semestre_name.join(' / ') : 'none'})
				opt.field.rows.push(col)
			})

			Dialogs.modalTable(opt)
		}

		this.onClickBlocks = (block) => {
			let opt = {
				title: block.ds_name,
				message: this.$translate('MIN_CREDITS') + ': ' + block.min_credits,
				field: {rows: []}
			}

			block.asigns.forEach((asign) => {
				let col = []
				col.push({displayName: "COURSE_CODE", value: asign.ds_publicId})
				col.push({displayName: "ASIGNATURE", value: asign.ds_name})
				col.push({displayName: "COURSE_CREDITS", value: asign.credits})
				opt.field.rows.push(col)
			})

			Dialogs.modalTable(opt)
		}		

	}]
}