import mainTemplate from './templates/main-template.html'

export let uPensumMatrixEdit = {
	bindings: {
		periods: "=",
		options: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 
	function ($scope, $rootScope, $templateCache) {

		this.$onInit = () => {
			
		}
	}]
}