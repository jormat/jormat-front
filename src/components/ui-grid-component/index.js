import mainTemplate from './ui-grid-component.html'

export let uiGridComponent = {
	bindings: {
		field: "=",
		inEdit: "<"
	},
	template: mainTemplate,
	controllerAs: 'uiGridCtrl',
	controller: ['$scope', '$rootScope', '$templateCache', '$filter',
	function ($scope, $rootScope, $templateCache, $filter) {
		this.$translate = $filter('translate')
		$rootScope.$on('$translateChangeSuccess', () => {
			this.mainFunction()
		})

		this.uiGrid = {
			enableColumnResizing: true, 
			enableFiltering: true,
			enablePaging: true,
			enableGridMenu: true,
			rowHeight: 36
		}

		/****************************************************************
		* Esta funccion armar los columnDefs y la data segun los 
		* registros que llegan
		*****************************************************************/
		this.mainFunction = () => {
			this.inEdit = false
			this.uiGrid.columnDefs = []
			let uiGridData = {}
			let model = {}
			this.field.rows.forEach((row, key) => {
				row.forEach((field) => {

					if (key == 0) {
						this.uiGrid.columnDefs.push({
							displayName: this.$translate(field.displayName), 
							name: this.$translate(field.displayName),
							field: field.displayName
						})
					}

					uiGridData[key] = uiGridData[key] || {}
					uiGridData[key][field.displayName] = field.value
					
					if (field.isSelected) {
						uiGridData[key].isSelected = true
						model[key] = model[key] || {}
						model[key][field.displayName] = field.value
					}
				})
			})
			this.field.model = Object.values(model)
			this.uiGrid.data = Object.values(uiGridData)

			if (this.inEdit) {
				this.setOnRegisterApi()
			}
		}

		this.setOnRegisterApi = () => {
			this.uiGrid.onRegisterApi = (gridApi) => {
				this.checkRowsSelected(gridApi)

				gridApi.selection.on.rowSelectionChanged($scope, (row) => {
					this.field.model = []
					this.field.model = gridApi.selection.getSelectedRows()
				})

				gridApi.selection.on.rowSelectionChangedBatch($scope, (rows) => {
					this.field.model = []
					this.field.model = gridApi.selection.getSelectedRows()
				})
			}
		}

		this.checkRowsSelected = (gridApi) => {
			gridApi.grid.modifyRows(this.uiGrid.data)

			for (let i in this.uiGrid.data) {
				if (this.uiGrid.data[i].isSelected) {
					gridApi.selection.selectRow(this.uiGrid.data[i])
				}
			}
		}

		this.mainFunction()
	}]
}