import mainTemplate from './templates/main-template.html'

export let panelView = {
	bindings: {
		panel: "=",
		options: "<",
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 
	function ($scope, $rootScope, $templateCache) {}]
}