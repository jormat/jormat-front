import mainTemplate from './templates/main-template.html'

export let occurrence = {
	bindings: {
		options: "<",
		occurrence: "=",
		baseServiceUrl: "<",
		addParams: "=",
		onDelete: "&",
		onSubmit: "&", 
		singleSave: "<",
		index: "<",
		rowKey: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'uFunctions',
	function ($scope, $rootScope, $templateCache, uFunctions) {
		$templateCache.put('panel-template.html', require('./templates/panel-template.html'))
		$templateCache.put('accordion-template.html', require('./templates/accordion-template.html'))
		const Functions = uFunctions
		this.$onInit = () => {
			this.options = this.options || {}
			this.onDelete = this.onDelete || (() => {})
			this.onSubmit = this.onSubmit || (() => {})
		}

		this.onFieldChange = (field, cleanModel) => {
			let localParams = angular.extend({}, {occurrenceId: this.occurrence.id_code})

			this.occurrence.data.forEach((row) => {
				row.forEach((f) => {
					for (let i in f.params) {
						if (i == field.label) {
							f.model = !cleanModel ? null : cleanModel
							if (field.model !== null) {
								f.addParams = angular.extend(Functions.getParam(field.model, f.params[i]), localParams)
								if (Object.keys(f.addParams).length > 1) {
									f.reload = !f.reload
									f.callSrv = true
								}
							} else {
								f.addParams = {}
								f.reload = !f.reload
								f.callSrv = false
							}
						}
					}
				})
			})
		}

		this.onThemeDelete = () => {
			this.onDelete({rowKey: this.rowKey,  index: this.index, occurrence: this.occurrence})
		}

		this.onThemeSubmit = () => {
			this.onSubmit({rowKey: this.rowKey,  index: this.index, occurrence: this.occurrence})
		}

	}]
}