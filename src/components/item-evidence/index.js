import mainTemplate from './item-evidence.html'

export let itemEvidence = {
	bindings: {
		evidence: "=",
		options: "<",
		onSubmit: '&',
	},
	template: mainTemplate,
	controllerAs: 'eCtrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'FileUploader', '$filter', 'ngNotify', '$window',
	function ($scope, $rootScope, $templateCache, FileUploader, $filter, ngNotify, $window) {
		$templateCache.put('load-file.html', require('./templates/load-file.html'))

		this.$onInit = () => {
			this.options.params = angular.extend({}, this.options.params, {evidenceId: this.evidence.evidenceId})
			this.onSubmit = this.onSubmit || (() => {})
			this.makingUploaderFiles()
			this.makingEvidenceInputs()
		}

		this.makingUploaderFiles = () => {
			let url = this.options.baseUrl + this.options.pathService
			this.uploader = new FileUploader({
				url: url, 
				method: "POST",
				alias: "evidences", 
				formData: [this.options.params]
			})

			this.uploader.filters.push({
				name: 'customFilter',
				fn: function(item, options) {
					return this.queue.length < 10
				}
			})

			this.uploader.onProgressItem = (fileItem, progress) => {
			}
			this.uploader.onProgressAll = (progress) => {
			}
			
			this.uploader.onSuccessItem = (fileItem, response, status, headers) => {
				let path = response.data.destination + response.data.finalName
				this.evidence.filesLoaded.push({
					fileId: response.data.fileId,
					filename: response.data.originalname,
					path: path
				})
			}
			this.uploader.onCompleteAll = () => {
				ngNotify.set('Archivos cargados exitosamente!', 'success')
			}
		}

		this.makingEvidenceInputs = () => {
			this.evidence.inputs = {
				results: {typefield: "textArea", label: "IMPROVE_EVALUATION_RESULTS", data: [{value: this.evidence.results}]},
				actions: {typefield: "textArea", label: "IMPROVE_UPDATE_ACTIONS", data: [{value: this.evidence.actions}]},
				tracking: {typefield: "textArea", label: "IMPROVE_PROPOSED_MONITORING", data: [{value: this.evidence.tracking}]}
			}
		}

		this.getClassFile = (file) => {
			let classes = {
				w: 'mdi-file-word',
				p: 'mdi-file-pdf',
				x: 'mdi-file-excel',
				o: 'mdi-file-document',
				i: 'mdi-file-image'
			}
			let limitTo = 10
			let fx = file.path.toString().split('.')
			let fn = file.filename.toString().split('.')
			file.showName = $filter('limitTo')(fn[0], limitTo) + (fn[0].length > limitTo ? '...' : '') + (fn[1] || '')
			let ext = (fx.length > 0) ? fx[fx.length-1].toLowerCase() : 'o'
			if (['doc', 'docx'].indexOf(ext) >= 0) {
				return classes['w']
			}
			if (['xls', 'xlsx'].indexOf(ext) >= 0) {
				return classes['x']
			}
			if (['pdf'].indexOf(ext) >= 0) {
				return classes['p']
			}
			if (['jpg', 'jpeg', 'png', 'bmp', 'gif', 'svg'].indexOf(ext) >= 0) {
				return classes['i']
			}
			return classes['o']
		}

		this.saveChanges = () => {
			let model = {}
			for (let i in this.evidence) {
				if (i !== 'inputs') {
					model[i] = this.evidence[i]
				}
			}

			model.results = this.evidence.inputs.results.model
			model.actions = this.evidence.inputs.actions.model
			model.tracking = this.evidence.inputs.tracking.model

			this.onSubmit({evidence: model})
		}

		this.downloadFile = (file) => {
			let url = this.options.baseUrl + this.options.pathDownload
			$window.open(url + file.path)
		}

	}]
}