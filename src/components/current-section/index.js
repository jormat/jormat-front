import mainTemplate from './current-section.html'

export let currentSection = {
	bindings: {
		currentSection: "=",
		options: "<",
		onUpdateClick: "&"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', '$state', function ($scope, $rootScope, $templateCache, $state) {
		this.stage = 2
		this.stages = [
			{id: 1, name: "Pendiente"},
			{id: 2, name: "Iniciada"},
			{id: 3, name: "Sin clase actualmente"}
		]
		
		this.getTitleButton = () => {
			if (this.currentSection !== undefined) {
				let btnLabel = 'Iniciar clase'
				if (!this.currentSection.nextClass) {
					if (this.currentSection.idStatusOcurrencia === 1) {
						this.stage = 0
						this.options.title = 'Debes dictar la clase:'
					} else {
						this.stage = 1
						this.options.title = 'Actualmente estás dictando la clase:'
						btnLabel = 'Finalizar clase'
					}
				} else {
					this.options.title = 'Próxima clase que debes dictar:'
					this.stage = 2
				}
				return btnLabel
			}
		}


		this.buttonUpdateStatus = () => {
			if (this.onUpdateClick !== undefined && typeof this.onUpdateClick == 'function') {
				this.onUpdateClick({stage: this.stages[this.stage]})
			}
		}

		this.goTo = (link) => {
			let params = {}
			if (link.params !== undefined && this.currentSection !== undefined) {
				for (let i in link.params) {
					params[i] = this.currentSection[link.params[i]]
				}
			}
			$state.go(link.url, params)
		}

	}]
}