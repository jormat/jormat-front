import mainTemplate from './templates/main-template.html'

export let uiEditTable = {
	bindings: {
		field: "=",
		onChange: "&",
		utOptions: "=",
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', '$filter', '$http', 'ngNotify', 'Dialogs', '$timeout', 'uFunctions',
	function ($scope, $rootScope, $templateCache, $filter, $http, ngNotify, Dialogs, $timeout, uFunctions) {
		const Functions = uFunctions
		$scope.iForm = {}
		this.$translate = $filter('translate')
		$rootScope.$on('$translateChangeSuccess', () => {
			let columnDefs = {}
			this.field.columnDefs = this.field.columnDefs || []
			this.field.columnDefs.forEach((col) => {
				col.displayName = this.$translate(col.field)
				col.name = this.$translate(col.field)
			})
		})

		$scope.$watch('Ctrl.field.rows', (rows) => {
			if (rows !== undefined) {
				this.makeModel()
			}
		}, true)

		this.$onInit = () => {
			this.onChange = this.onChange || (() => {})
			this.field.model = null
			this.field.initialState = null
			this.columnDefs = {}
			this.options = angular.extend({}, this.utOptions)
			this.inputs = []
			this.buildingHeader(this.field.header)
		}

		this.onFieldChange = (field, cleanModel, index) => {
			this.isFirtsChange = this.isFirtsChange || {}
			this.isFirtsChange[index] = this.isFirtsChange[index] || {}
			this.isFirtsChange[index][field.label] = (this.isFirtsChange[index][field.label] === undefined) ? true : false
			this.field.rows[index].forEach((f) => {
				for (let i in f.params) {
					if (i == field.label) {
						f.model = this.isFirtsChange[index][field.label] ? f.model : null
						this.isFirtsChange[index][field.label] = false
						if (field.model !== null) {
							f.addParams = angular.extend(Functions.getParam(field.model, f.params[i]))
							if (Object.keys(f.addParams).length > 0) {
								f.reload = !f.reload
								f.callSrv = true
							}
						} else {
							f.addParams = {}
							f.reload = !f.reload
							f.callSrv = false
						}
					}
				}
			})
			this.makeModel()
		}

		this.onInputsChange = (field, cleanModel) => {
			this.inputs.forEach((f) => {
				for (let i in f.params) {
					if (i == field.label) {
						f.model = !cleanModel ? null : cleanModel
						if (field.model !== null) {
							f.addParams = angular.extend(Functions.getParam(field.model, f.params[i]))
							if (Object.keys(f.addParams).length > 0) {
								f.reload = !f.reload
								f.callSrv = true
							}
						} else {
							f.addParams = {}
							f.reload = !f.reload
							f.callSrv = false
						}
					}
				}
			})
		}

		this.makeModel = () => {
			this.field.model = []
			this.field.rows = this.field.rows || []

			for (let i in this.field.rows) {
				let item = {}
				for (let x in this.field.rows[i]) {
					item[this.field.rows[i][x].displayName] = this.field.rows[i][x].model
				}
				this.field.model.push(item)
			}
			this.onChange({field: this.field})
		}

		this.buildingHeader = (header) => {
			header = header || []
			let opt = {
				method: "GET",
				params: this.options.addParams, 
			}

			header.forEach((col, index) => {
				let field = {
					typefield: col.typefield,
					label: col.displayName,
					pathService: col.pathService,
					isDisabled: col.isDisabled || false,
					params: col.params,
					data: [{}]
				}

				this.columnDefs[col.displayName] = {
					displayName: this.$translate(col.displayName), 
					name: this.$translate(col.displayName),
					field: col.displayName
				}

				if (['select'].indexOf(field.typefield) >= 0) {
					this.callService(opt, field, (response) => {
						field.pathService = (!field.params) ? response.data : field.pathService
						this.inputs[index] = field
						this.buildingRows(field)
					})
				} else {
					this.buildingRows(field)
					this.inputs[index] = field
				}

			})
			this.field.columnDefs = Object.values(this.columnDefs)
			this.makeModel()
		}

		this.buildingRows = (headerField) => {
			this.field.rows.forEach((row, key) => {
				$scope.iForm[key] = {$visible: false}
				row.forEach((field, fKey) => {
					$scope.rendered = $scope.rendered || {}
					$scope.rendered[key] = $scope.rendered[key] || {}
					$scope.rendered[key][fKey] = true

					if (field.displayName == headerField.label) {
						field.typefield = headerField.typefield
						field.data = field.data || [{id_code: field.id, value: field.value}]
						field.pathService = headerField.pathService
						field.params = headerField.params
						field.isDisabled = headerField.isDisabled
						field.label = field.displayName
					}
				})
			})
		}

		this.callService = (opt, field, callback) => {
			callback = callback || (() => ({}))
			if (this.field.isDisabled || !this.options.inEdit) {
				callback({data: []})
				return
			}
			if (Array.isArray(field.pathService)) {
				callback({data: field.pathService})
				return
			} else if (field.pathService && field.pathService.length > 0) {
				opt.url = this.options.baseUrl + field.pathService

				$http(opt).then((response) => {

					if (response.data.status) {
						callback({data: []})
						return 
						ngNotify.set('Ocurrio un error en el servicio', 'warn')
					} else {
						let data = []

						if (Array.isArray(response.data.data)) {
							response.data.data.forEach((item) => {
								let row = this.getItemForModel(item)
								data.push(row)
							})
						}

						callback({data: data})
						return 
					}
				}, (error) => {
					ngNotify.set('Imposible conectar con el servicio', 'error')
					callback({data: []})
					return
				})
			} else {
				callback({data: []})
				return 
			}
		}

		this.getOptionsList = (field) => {
			for (let i in this.inputs) {
				if (this.inputs[i].label == field) {
					return this.inputs[i].listField
				}
			}
			return []
		}

		this.saveRow = (key) => {
			$scope.iForm[key].$visible = !$scope.iForm[key].$visible
		}

		this.addRow = () => {
			let valid = true

			this.inputs.forEach((input) => {
				if (input.typefield == 'select') {
					if (!input.model) {
						valid = false
					}
				} else {
					if (!input.model || input.model == '') {
						valid = false
					}
				}
			})

			if (valid) {
				this.createNewRow()
			}
		}

		this.createNewRow = () => {
			let row = []

			this.inputs.forEach((input) => {
				if (input.typefield == 'select') {
					if (input.model !== null) {
						row.push({
							displayName: input.label,
							label: input.label,
							pathService: input.pathService,
							typefield: input.typefield, 
							params: input.params,
							data: [input.model],
						})
					}
				} else {
					if (input.model !== null || input.model !== '') {
						row.push({
							displayName: input.label,
							label: input.label,
							pathService: input.pathService,
							typefield: input.typefield, 
							data: [{value: input.model}],
						})
					}
				}
				input.model = null

			})
			this.field.rows.push(row)
			$scope.iForm[this.field.rows.length-1] = {$visible: false}
		}

		this.deleteRow = (key) => {
			Dialogs.modalConfirm().then((response) => {
				if (response) {
					this.field.rows.splice(key, 1)
					this.makeModel()
				}
			})
		}

		this.getItemForModel = (data = {}) => {
			let field = null
			let item = {}
			for (let f in data) {
				let fi = (['value', 'adminitstrativeName', 'teacherName', 'ModalityName', 'languajeName'].indexOf(f) >= 0) ? this.conf.name : f
				field = (['id', 'idAdministrative', 'teacherId', 'modalityId', 'languajeId'].indexOf(fi) >= 0) ? this.conf.id : fi
				item[field] = data[f]
			}
			return _.size(item) > 1 ? item : null
		}

	}]
}