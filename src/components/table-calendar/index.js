import template from './table-calendar.html'

export let tableCalendar = {
	bindings: {
		modulesByDay: '<',
		availableModules: '<',
		selectedModules: '<',
		disableSelection: '=',
		selectAll: '<',
		viewSelect: '<',
		onSelect: '&',
		onClick: '&',
		alreadyLoaded: '&',
	},
	template: template,
	controllerAs: 'acalendar',
	controller: ['$scope', '$rootScope', tableCalendarCtrl]
}

function tableCalendarCtrl($scope, $rootScope) {
	var This = this
	This.weekDaysNames = {
		"0": "Mod",
		"1": "Lunes",
		"2": "Martes",
		"3": "Mi&eacute;rcoles",
		"4": "Jueves",
		"5": "Viernes",
		"6": "S&aacute;bado",
		"7": "Domingo",
	}
	This.rangeSelected = rangeSelected
	This.buildingTable = buildingTable
	This.whitoutModule = whitoutModule
	This.countingCero = countingCero
	This.validaCheckbox = validaCheckbox
	This.onModuleClick = onModuleClick
	This.selectAllItems = selectAllItems
	This.selectColumn = selectColumn
	This.selectRow = selectRow
	This.isAvailable = isAvailable
	This.isColumnChecked = isColumnChecked
	This.isRowChecked = isRowChecked
	This.showClassroom = showClassroom
	This.allEvents = []
	This.viewSelected = angular.extend({type: 'cell', showLabelOnEdit: true}, this.viewSelect)
	This.weekDays = "1,2,3,4,5,6,7".split(',')
	This.check = {}
	This.aCalendar = {
		daysArray: [],
		modulesDays: [],
		availableModules: this.modulesByDay, 
		events: []
	}

	function buildingTable() {
		This.aCalendar.modulesDays = []
		let days = []
		var pos = 0
		let modulesOfArray = []

		for (let i in This.availableModules) {
			let modules = This.availableModules[i]
			for (let x in modules) {
				for (let y in This.aCalendar.daysArray) {

					if (modulesOfArray[modules[x].nm_sequence] === undefined) {
						modulesOfArray[modules[x].nm_sequence] = []
					}

					if (This.aCalendar.daysArray[y].group == modules[x].grupoID) {
						modulesOfArray[modules[x].nm_sequence].push({
							id: modules[x].nm_sequence, 
							group: modules[x].grupoID,
							day: This.aCalendar.daysArray[y].day, 
							start: modules[x].start_hour.substr(0,5),
							end: modules[x].end_hour.substr(0,5)
						})
					}
				}
			}
		}

		for (let i in modulesOfArray) {
			if (modulesOfArray[i] !== undefined) {
				This.aCalendar.modulesDays.push(modulesOfArray[i])
			}
		}
		
		selectModuleInTable();
	}

	function makeDaysArray() {
		This.aCalendar.daysArray = []
		let daysOfArray = []
		let days = []
		var pos = 0
		for (let i in This.availableModules) {
			for (let y in This.availableModules[i]) {
				days = This.availableModules[i][y].dias.split(',')
				if (checkForAllDays()) {
					This.buildingTable()
					return
				}
				let grp = This.availableModules[i][y].grupoID
				let map = daysOfArray.map(function(item) {return item.group; })

				if (daysOfArray[daysOfArray.length-1] != undefined && map.indexOf(grp) < 0) {
					daysOfArray[daysOfArray.length] = {
						group: This.availableModules[i][y].grupoID,
						day: "0", 
					}
				} else {
					if (daysOfArray.length == 0) {
						daysOfArray[0] = {
							group: This.availableModules[i][y].grupoID,
							day: "0", 
						}
					}
				}
				for (let x in days) {
					if (!inArray(days[x], daysOfArray)) {
						pos = parseInt(days[x]) + This.countingCero(daysOfArray)
						daysOfArray[pos] = {
							group: This.availableModules[i][y].grupoID,
							day: days[x].toString(), 
						}
					}
				}
			}
		}
		for (let i in daysOfArray) {
			if (daysOfArray[i] !== undefined) {
				This.aCalendar.daysArray.push(daysOfArray[i])
			}
		}

		let tmpArray = angular.copy(This.aCalendar.daysArray)
		for (let z in This.weekDays) {
			if (!inArray(This.weekDays[z], tmpArray)) {
				for (let i in This.aCalendar.daysArray) {
					if (This.aCalendar.daysArray[i].day != '0' && This.aCalendar.daysArray[i].day > This.weekDays[z]) {
						This.aCalendar.daysArray.splice(i, 0, {
							group: 0,
							day: This.weekDays[z].toString(), 
						})
					}
				}
				if (!inArray(This.weekDays[z], This.aCalendar.daysArray)) {
					This.aCalendar.daysArray.push({
						group: 0,
						day: This.weekDays[z].toString(), 
					})
				}
			}
		}
		This.buildingTable()
	}
	
	makeDaysArray()

	function countingCero(inArray) {
		let ceros = -1
		for (let i in inArray) {
			if (inArray[i].day == 0) {
				ceros++
			}
		}
		return ceros
	}

	function checkForAllDays() {
		let d = "1,2,3,4,5,6,7".split(',')
		let map = This.aCalendar.daysArray.map(function(item) {
			return item.day;
		})
		for (let i in d) {
			if (map.indexOf(d[i]) < 0) {
				return false
			}
		}
		return true
	}

	function inArray(needle, haystack) {
		for (let i in haystack) {
			if (haystack[i] !== undefined) {
				if (haystack[i].day == needle) {
					return true
				}
			}
		}
		return false
	}

	function whitoutModule(days, modules) {
		let flag = true
		for (let i in modules) {
			if (days.day == modules[i].day && days.group == modules[i].group) {
				flag = false
			}
		}
		return flag
	}

	function selectModuleInTable() {
		This.allEvents = [];
		This.aCalendar.events = [];
		for (let i in This.selectedModules) {
			if (This.selectedModules[i].days !== undefined) {
				for (let x in This.selectedModules[i].days.modules) {
					for (let z in This.aCalendar.modulesDays) {
						for (let y in This.aCalendar.modulesDays[z]) {
							if (This.aCalendar.modulesDays[z][y].day == i && 
								This.aCalendar.modulesDays[z][y].id == This.selectedModules[i].days.modules[x].MODULECODE) {

								This.allEvents.push({
									nm_dayoftheweek: i,
									nm_sequence: This.selectedModules[i].days.modules[x].MODULECODE, 
									id_group: This.aCalendar.modulesDays[z][y].group
								});
							}
						}
					}
				}
			} else {
				for (let x in This.aCalendar.modulesDays) {
					for (let y in This.aCalendar.modulesDays[x]) {
						if (This.aCalendar.modulesDays[x][y].day == This.selectedModules[i].nm_dayoftheweek &&
							This.aCalendar.modulesDays[x][y].id == This.selectedModules[i].nm_sequence) {

							This.allEvents.push({
								nm_dayoftheweek: This.selectedModules[i].nm_dayoftheweek,
								nm_sequence: This.selectedModules[i].nm_sequence,
								id_module: This.selectedModules[i].id_module,
								name_day: This.selectedModules[i].name_day,
								start_hour: ((This.selectedModules[i].start_hour !== undefined) ? This.selectedModules[i].start_hour.substr(0,5) : ''),
								end_hour: ((This.selectedModules[i].end_hour !== undefined) ? This.selectedModules[i].end_hour.substr(0,5) : ''),
								name_classroom: This.selectedModules[i].name_classroom,
								id_classroom: This.selectedModules[i].id_classroom,
								id_group: This.aCalendar.modulesDays[x][y].group
							})
						}
					}
				}
			}
		}

		This.aCalendar.events = This.allEvents;

		if (This.onSelect !== undefined) {
			This.onSelect({calendar: This.aCalendar})
		}
		if (This.alreadyLoaded !== undefined) {
			This.alreadyLoaded({calendar: This.aCalendar})
		}
	}

	function onModuleClick(nm_sequence, day, id_module, start, end) {
		if (This.onClick === undefined) {return; } 
		
		for (let i in This.allEvents) {
			if (This.allEvents[i].nm_sequence == nm_sequence && This.allEvents[i].nm_dayoftheweek == day) {
				This.onClick({item: This.allEvents[i]})
				break
			}
		}
	}

	function rangeSelected(group, nm_sequence, day, id_module, start, end, check) {
		/***********************************************************************
		* Cancelar accion si se encuentra desabilitada la seleccion de modulos *
		************************************************************************/
			if (This.disableSelection) {return} 
		/***********************************************************************/
		let x = false;
		This.aCalendar.events = [];
		for (let i in This.allEvents) {
			if (This.allEvents[i].nm_sequence == nm_sequence && This.allEvents[i].nm_dayoftheweek == day) {
				This.allEvents.splice(i, 1)
				x = true;
				break;
			}
		}
		
		if (!x || check) {
			This.allEvents.push({
				nm_sequence: nm_sequence,
				nm_dayoftheweek: day,
				id_module: id_module,
				name_day: This.weekDaysNames[day], 
				start_hour: start,
				end_hour: end,
				id_group: group
			});
		}
		for (let i in This.allEvents) {
			This.aCalendar.events.push(This.allEvents[i]);
		}

		if (This.onSelect !== undefined) {
			This.onSelect({calendar: This.aCalendar})
		}
	}

	function showClassroom(available) {
		for (let i in This.aCalendar.events) {
			if (This.aCalendar.events[i].nm_sequence == available.nm_sequence && 
				This.aCalendar.events[i].nm_dayoftheweek == available.nm_dayoftheweek) {
				return This.aCalendar.events[i].name_classroom || false
			}
		}
		return false
	}

	function validaCheckbox(available) {
		for (let i in This.aCalendar.events) {
			if (This.aCalendar.events[i].nm_sequence == available.nm_sequence && 
				This.aCalendar.events[i].nm_dayoftheweek == available.nm_dayoftheweek) {
				This.check[available.nm_dayoftheweek] = This.check[available.nm_dayoftheweek] || {}
				This.check[available.nm_dayoftheweek][available.nm_sequence] = This.check[available.nm_dayoftheweek][available.nm_sequence] || {}
				This.check[available.nm_dayoftheweek][available.nm_sequence] = true
				return true
			}
		}
		return false
	}

	function selectAllItems(check) {
		This.aCalendar.events = []
		This.allEvents = []
		if (check) {
			This.modulesByDay.forEach((item) => {
				This.allEvents.push({
					id_module: item.id_module,
					nm_sequence: item.nm_sequence,
					nm_dayoftheweek: item.nm_dayoftheweek,
					name_day: This.weekDaysNames[item.nm_dayoftheweek], 
					start_hour: item.start_hour,
					end_hour: item.end_hour,
					id_group: item.id_group,
				});
			});
		}

		This.aCalendar.events = This.allEvents

		if (This.onSelect !== undefined) {
			This.onSelect({calendar: This.aCalendar})
		}
	}

	function isColumnChecked(day) {
		let flag
		for (let x in This.aCalendar.modulesDays) {
			for (let y in This.aCalendar.modulesDays[x]) {
				if (This.aCalendar.modulesDays[x][y].day == day) {
					flag = false
					for (let i in This.allEvents) {
						if (This.allEvents[i].nm_dayoftheweek == day && This.aCalendar.modulesDays[x][y].id == This.allEvents[i].nm_sequence) {
							flag = true
						}
					}
					if (!flag) {return false}
				}	
			}
		}
		return true
	}

	function isRowChecked(sequence, group) {
		let flag
		for (let x in This.aCalendar.modulesDays) {
			for (let y in This.aCalendar.modulesDays[x]) {
				if (This.aCalendar.modulesDays[x][y].id == sequence && This.aCalendar.modulesDays[x][y].group == group && This.aCalendar.modulesDays[x][y].day != '0') {
					flag = false
					for (let i in This.allEvents) {
						if (This.allEvents[i].nm_sequence == sequence && 
							This.allEvents[i].id_group == group && 
							This.aCalendar.modulesDays[x][y].day == This.allEvents[i].nm_dayoftheweek) {
							flag = true
						}
					}
					if (!flag) {return false}
				}	
			}
		}
		return true
	}

	function selectColumn(check, day) {
		This.aCalendar.events = []
		let evs = []
		for (let i in This.allEvents) {
			if (This.allEvents[i].nm_dayoftheweek != day) {
				evs.push(This.allEvents[i])
			}
		}

		This.allEvents = []
		This.allEvents = evs

		if (check) {
			This.modulesByDay.forEach((item) => {
				if (item.nm_dayoftheweek == day) {
					This.allEvents.push({
						id_module: item.id_module,
						nm_sequence: item.nm_sequence,
						nm_dayoftheweek: item.nm_dayoftheweek,
						name_day: This.weekDaysNames[item.nm_dayoftheweek], 
						start_hour: item.start_hour,
						end_hour: item.end_hour,
						id_group: item.id_group,
					});
				}
			});
		}

		This.aCalendar.events = This.allEvents

		if (This.onSelect !== undefined) {
			This.onSelect({calendar: This.aCalendar})
		}
	}

	function selectRow(check, sequence, group) {
		This.aCalendar.events = []
		let evs = []
		for (let i in This.allEvents) {
			if (This.allEvents[i].nm_sequence != sequence || This.allEvents[i].id_group != group) {
				evs.push(This.allEvents[i])
			}
		}

		This.allEvents = []
		This.allEvents = evs

		if (check) {
			This.modulesByDay.forEach((item) => {
				if (item.nm_sequence == sequence && item.id_group == group) {
					This.allEvents.push({
						id_module: item.id_module,
						nm_sequence: item.nm_sequence,
						nm_dayoftheweek: item.nm_dayoftheweek,
						name_day: This.weekDaysNames[item.nm_dayoftheweek], 
						start_hour: item.start_hour,
						end_hour: item.end_hour,
						id_group: item.id_group
					});
				}
			});
		}

		This.aCalendar.events = This.allEvents

		if (This.onSelect !== undefined) {
			This.onSelect({calendar: This.aCalendar})
		}
	}

	function isAvailable(day) {
		let flag = true
		This.modulesByDay.forEach((item) => {
			if (item.nm_dayoftheweek == day) {
				flag = false
			}
		});
		return flag
	}
}