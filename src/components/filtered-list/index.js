import mainTemplate from './filtered-list.html'

export let filteredList = {
	bindings: {
		filteredList: "=",
		onClick: "&",
		filters: "<",
		options: "=",
		functions: "<"
	},
	template: mainTemplate,
	controllerAs: 'Ctrl',
	controller: ['$scope', '$rootScope', '$templateCache', 'uFunctions', '$filter', '$timeout', 
	function ($scope, $rootScope, $templateCache, uFunctions, $filter, $timeout) {
		$templateCache.put('extended-view.html', require('./templates/extended-view.html'))
		$templateCache.put('compacted-view.html', require('./templates/compacted-view.html'))
		$templateCache.put('simpled-view.html', require('./templates/simpled-view.html'))
		$templateCache.put('generic-view.html', require('./templates/generic-view.html'))
		this.filters = this.filters || {}
		const $translate = $filter('translate')
		const Functions = uFunctions
		const views = ['extended', 'compacted', 'simpled', 'generic']

		this.onClickList = (item) => {
			this.itemSelected = item.id_code || item.sectionId

			if (this.onClick !== undefined) {
				this.onClick({item: item})
			}
		}

		$rootScope.$on('$translateChangeSuccess', () => {
			this.setLangLabels()
		})

		this.setLangLabels = () => {
			this.filteredList = this.filteredList || []

			this.filteredList.forEach((section, index) => {
				this.rolLabel = this.rolLabel || []
				this.rolLabel[index] = this.rolLabel[index] || {}
				this.rolLabel[index] = this.getRolLabels(section)
			})
		}

		this.onChange = (field, cleanModel) => {
			field.onChange = field.onChange || (() => {})
			this.filters.inputs.forEach((f) => {
				f.params = f.params || {}
				for (let i in f.params) {
					if (i == field.label) {
						f.model = !cleanModel ? null : cleanModel
						if (field.model !== null) {
							f.addParams = angular.extend(Functions.getParam(field.model, f.params[i]))
							f.reload = !f.reload
							f.callSrv = true
						} else {
							f.addParams = {}
							f.reload = !f.reload
							f.callSrv = false
						}
					}
				}
			})
			
			field.onChange(field.model)
		}

		$scope.$watch('Ctrl.filteredList', (filteredList) => {
			if (filteredList !== undefined) {
				$scope.start = true
				this.main() 
			}
		})

		this.getRolLabels = (item) => {
			item.rolLabels = item.rolLabels || []
			return {
				codes: item.rolLabels.map((item) => item.id_code).join(' - '),
				labels: item.rolLabels.map((item) => $translate(item.ds_name)).join(' - '),
			}
		}

		this.main = () => {
			let inputDefault = false
			this.filters.inputs = this.filters.inputs || []
			this.optDefault = {title: false, view: "extended", itemSelectedId: null}

			this.config = angular.extend({}, this.optDefault, this.options)
			this.filters.inputs.forEach((input) => {
				if (input.typefield == 'text') {
					inputDefault = true
				}
			})

			if (!inputDefault) {
				this.filters.inputs.push({
					label: "SEARCH_TEXT", typefield: "text", data: [], 
				})
			}
			
			this.filteredList = this.filteredList || []
			if (this.filteredList.length > 0) {
				if (this.config.itemSelectedId) {
					for (let i in this.filteredList) {
						if (this.filteredList[i].id_code == this.config.itemSelectedId || this.filteredList[i].sectionId == this.config.itemSelectedId) {
							this.onClickList(this.filteredList[i])
							this.setLangLabels()
							return
						}
					}
					this.onClickList(this.filteredList[0])
				} else {
					this.onClickList(this.filteredList[0])
				}
			}
			this.setLangLabels()
		}

		$scope.getHeightList = () => {
			let h = $('.filters').height() || 0
			let v = $( window ).height() || 100
			return v - (h + 100)
		}
		
		this.validatingTypeView = () => {
			if (views.indexOf(this.config.view) >= 0) {
				return false
			}
			return true
		}

		this.getFiltering = (list) => {
			let excludeFields = ['$$hashKey', 'id_code']            
			for (let x in this.filters.inputs) {
				let field = this.filters.inputs[x]
				/**
				* Buscando coincidencia con los tipo text y number dentro de la lista de secciones
				**/
				if ((field.typefield == 'text')||(field.typefield == 'number')) {
					if (field.model !== undefined && field.model !== null && field.model != '') {
						for (let i in list) {                            
							if (excludeFields.indexOf(i) >= 0) { continue }
							let search = new RegExp(field.model.toString(), 'gi')
							if (list[i] && list[i].toString().match(search)) {
								return true
							}
						}
						return false
					}
				}

				/**
				* Buscando coincidencia con los tipo select dentro de la lista de secciones
				**/
				if (field.typefield == 'select') {
					if (typeof field.onChange != 'function'){
						if (field.model.ds_name !== undefined && field.model.ds_name !== null && field.model.ds_name != '') {
							for (let i in list) {                                
								if (excludeFields.indexOf(i) >= 0) { continue }
								let search = new RegExp(field.model.ds_name.toString(), 'gi')
								if (list[i] && list[i].toString().match(search)) {
									return true
								}
							}
							return false
						}
					}
				}

				/**
				* Buscando coincidencia con los tipo multipleSelect dentro de la lista de secciones
				**/
				if (field.typefield == 'multipleSelect') {
					if (typeof field.onChange != 'function'){
						if (field.model.length > 0) {
							for (let y in field.model) {
								for (let i in list) {                                
									if (excludeFields.indexOf(i) >= 0) { continue }
									let search = new RegExp(field.model[y].ds_name.toString(), 'gi')
									if (list[i] && list[i].toString().match(search)) {
										return true
									}
								}
							}
							return false
						}
					}
				}
			}

			return true
		}

		this.VirtualRepeat = {
			loadedPages: {},
			numItems: 0,
			PAGE_SIZE: 50,

			fetchNumItems_: function () {
				$timeout(angular.noop, 300).then(angular.bind(this, function() {
					this.numItems = 50000;
				}))
			},

			getItemAtIndex: function (index) {
				let pageNumber = Math.floor(index / this.PAGE_SIZE)
				let page = this.loadedPages[pageNumber]
				if (page) {
					return page[index % this.PAGE_SIZE]
				} else if (page !== null) {
					this.fetchPage_(pageNumber)
				}
			},

			getLength: function () {
				return this.numItems
			},

			fetchPage_: function (pageNumber) {
				this.loadedPages[pageNumber] = null

				$timeout(angular.noop, 300).then(angular.bind(this, function() {
					this.loadedPages[pageNumber] = []
					let pageOffset = pageNumber * this.PAGE_SIZE

					for (let i = pageOffset; i < pageOffset + this.PAGE_SIZE; i++) {
						this.loadedPages[pageNumber].push(i)
					}
				}))

			}
		}

		// this.VirtualRepeat.fetchNumItems_()

	}], 
}