


  export default class asideHeader {

    constructor($scope, $http, $timeout, $resource, $location, MenuConstant, UserInfoConstant, permissionList, permissions, $mdSidenav, $state, $rootScope, $translate, $cookies,$modal) {
    
       var localenv=require('../local.env.json');
        $scope.language_layer=false;
        if(localenv.LANGUAGE_LAYER!=false){
           $scope.language_layer=true;
        }

       $scope.lang = $cookies.get('lang');
       $scope.languages=[{'key':'es','value':'Spanish','src':'assets/images/flags/es.png'},
                         {'key':'en','value':'English','src':'assets/images/flags/en.png'},
                         {'key':'por','value':'Portuguese','src':'assets/images/flags/en.png'}];

      if(typeof  $scope.lang==='undefined') $scope.lang = 'es';


     $translate.use($scope.lang);

     $scope.languageHeader= updateLanguageKey($scope.lang, $scope.languages);


    // $scope.updateLanguage = function() {
    //     console.log($scope.language)
    //     $translate.use($scope.language.key);
    //   };


    $scope.updateLanguage1 = function(key) {
        console.log(key)
         $translate.use(key);
          $scope.languageHeader= updateLanguageKey(key, $scope.languages);
           console.log( $scope.languageHeader)
      };


      $rootScope.$on('$translateChangeSuccess', function(event, data) {
            console.log('translateChangeSucces');
            var language = data.language;
            $cookies.put('lang', language);
            $rootScope.lang = language;

        });

     // $scope.loadImage  = function(image) {
     //      return require(image);
     //  };


      function updateLanguageKey(key,value){

          for(var i in value){
             if(value[i].key==key){
              return value[i];
             }
          }

      }

      $scope.openAside = function () {
        $timeout(function() {
          $mdSidenav('aside').toggle();
        });
      }

      $scope.showInfo = function () { 
        var modalInstance  = $modal.open({
                        template: require('./modal/company-info.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'companyInfoController',
                        controllerAs: 'companyInfo',
                        // size: 'sm'
                })
      }




   }
}

 asideHeader.$inject = ['$scope', '$http', '$timeout', '$resource', '$location', 'MenuConstant', 'UserInfoConstant', 'permissionList', 'permissions', '$mdSidenav', '$state', '$rootScope', '$translate', '$cookies','$modal'];
