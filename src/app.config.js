routing.$inject = ['$urlRouterProvider', '$locationProvider','$stateProvider','$httpProvider'];

export default function routing($urlRouterProvider, $locationProvider,$stateProvider, $httpProvider) {
    $locationProvider.html5Mode(true);
    var localenvjson = require('../local.env.json')
    var clientName=localenvjson.clientName;
    $urlRouterProvider.otherwise(localenvjson.HOMEPAGE);

    $httpProvider.interceptors.push(['$q', '$location', '$localStorage','UserInfoConstant','$cookies','LOCAL_ENV_CONSTANT',
            function($q, $location, $localStorage,UserInfoConstant,$cookies,LOCAL_ENV_CONSTANT) {

       var token='';

        return {
            'request': function (config) {
                config.headers = config.headers || {};
                token=$cookies.get('token')

				if (token === undefined)
					return config

                //  if (config.url.substr(config.url.length - 5) == '.html' || config.url.indexOf(LOCAL_ENV_CONSTANT.SERVER_NAME) == 0 ||  config.url.indexOf('api')==-1) {

                // } else{

                //       config.headers['x-access-token']= token;
                // }

				config.headers['x-access-token'] = token
				const tokens = JSON.stringify({ token: token })
				config.headers['Authorization'] = 'Bearer ' + btoa(tokens)

              return config;

            }
        };
    }]);



    $stateProvider
    .state('app', {
        url: '/app',
        abstract: false,
        views: {
            '': {
                template: require('./layout/layout.html'),
            },
            'content': {
                template: require('./layout/content.html'),
            },
            'aside': {
                template: require('./layout/aside.html'),
                controller: 'AsideCtrl',
            },
            'header': {
                template: require('./layout/header-'+clientName+'.html'),
                controller: 'asideHeader',
            }
        }
    })
    .state('login', {
        abstract: false,
        url: '/login',
        'content': {
            template: '',
        },
        'aside': {
            template:'',
        },
        'header': {
            template:'',
            controller: 'asideHeader',
        },
        template: require('./login/login.html'),
        controller: 'loginValidationCtrl',
        controllerAs:'loginCtrl'
    })
    .state('app.unauthorized', {
        abstract: false,
        url: '/unauthorized',
        template: require('./unauthorized/unauthorized.html')
    })
    .state('log', {
        abstract: false,
        url: '/log',
        views: {
            '': {
                template: require('./layout/content.html'),
            }
        }        
    })    
}