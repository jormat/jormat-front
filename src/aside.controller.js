/**
* @name APP u-Improve
* @description
* Aside Controller
* @controller 
* # name: AsideCtrl
*/

export default class AsideCtrl {

	constructor($scope, $http, $timeout, $resource, $location, MenuConstant, UserInfoConstant, permissionList, permissions, $mdSidenav, $state, $cookies, $stateParams, $rootScope, $translate,$localStorage, Dialogs,$modal) {

		let langs = {
			es: "Espa�ol",
			en: "English",
			por: "Portugues"
		}
		var config = require('../local.env.json')
		var url = config.USER_API_URL + '/api/user-api'
		$rootScope.goBack = $rootScope.goBack || (() => {
			$state.get().forEach((route) => {
				if (route.url == config.HOMEPAGE.substr(4)) {
					$state.go(route.name)
					return
				}
			})
		})
		let n=1
		logTimeOut(n)

		$scope.lang = $cookies.get('lang')
		$scope.languages = []
		config.AVAILABLE_LANG = config.AVAILABLE_LANG || ["es"]
		for (let i in config.AVAILABLE_LANG) {
			$scope.languages.push({
				key: config.AVAILABLE_LANG[i],
				value: langs[config.AVAILABLE_LANG[i]],
				src: "assets/images/flags/"+ config.AVAILABLE_LANG[i] +".png", 
			})
		}

		$scope.lang = $scope.lang || 'es'

		$translate.use($scope.lang)
		$scope.languageHeader = updateLanguageKey($scope.lang, $scope.languages)

		$scope.updateLanguage1 = function(key) {
			$translate.use(key)
			$scope.languageHeader= updateLanguageKey(key, $scope.languages)
		}


		$rootScope.$on('$translateChangeSuccess', function(event, data) {
			var language = data.language
			$cookies.put('lang', language)
			$rootScope.lang = language
		})

		function updateLanguageKey(key,value){
			for(var i in value){
				if(value[i].key==key){
					return value[i]
				}
			}
		}

		function logTimeOut(){
			setTimeout(function() {
				logTimeOut()
				viewCommentary()
			}, 2700000);// Tiempo en que aparece el Modal (configurado en 45 min.)
		}

		function viewCommentary(){
			const opt = {
				message : 'Desea Mantener las sesión?',
				count : true
			}

				var modalInstance  = $modal.open({
					template: require('./modal/loginOut.html'),
					animation: true,
					backdrop: false,
					scope: $scope,
					controller: 'loginOutController',
					controllerAs: 'loginOutCtrl'
			})
		}

		var flag = true
		$scope.MenuClick = function (e, index) {
			if(flag==true){
				if ($('#parent_li_' + index).hasClass('active')) {
					$('#parent_li_' + index).removeClass('active')
				} else {
					$('#parent_li_' + index).addClass('active')
				}
			}
			flag = true
		}

		$scope.super_menu= function (element, index) {
			if ($('#navtoolkit #super_menu_li_' + index).hasClass('active')) {
				$('#navtoolkit #super_menu_li_' + index).removeClass('active')
			} else {
				$('#navtoolkit #super_menu_li_' + index).addClass('active')
			}
			flag = false
		}

		$scope.MenuLogout = function (e, index) {

		    var id=$localStorage.userLogin.user.id

			delete $localStorage.userLogin
			$cookies.put('hasDemand','')
			$cookies.remove('isAdmin')


				window.location.href = '/login'


		}

		$scope.toogleAccountClass=function(){
			$('#nav,#account').toggleClass('hide')
		}

		$scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
			$timeout(function () {
				if (UserInfoConstant[0].details[0].user['url'].indexOf($location.path()) < 0) {
					// MUESTRA HTML ACCESO DENEGADO
					$location.path('/app/unauthorized')
				}
			}, 200)
		})

		$scope.openAside = function () {
			$timeout(function() {
				$mdSidenav('aside').toggle()
			})
		}

		$scope.validatePermissions = (stateName, children) => {
			/********************************************************
			* Se habilitan todas las url's para trabajar localmente *
			*********************************************************/
			// return true
			if (stateName != '' && stateName != '#' && stateName !== undefined) {
				let currentState = $state.get(stateName)
				if ($scope.urlAllowed !== undefined && currentState !== null) {
					for (let i in $scope.urlAllowed) {
						if ($scope.urlAllowed[i] == '/app' + currentState.url.split('?')[0]){
							return true	
						} 
					}
				}
				return false
			} else {
				if (children !== undefined && children.length > 0) {
					for (let i in children) {
						if ($scope.validatePermissions(children[i].url, children[i].superMenu)) {
							return true
						}
					}
					return false
				} else {
					return true
				}
			}
		}

		/**
		* Funcion para volver atras
		**/
		$rootScope.$on('$stateChangeSuccess', (ev, to, toParams, from, fromParams) => {
			$rootScope.goBack = () => {
				$state.go(from.name, fromParams)
			}
		})

	}
}

AsideCtrl.$inject = ['$scope', '$http', '$timeout', '$resource', '$location', 'MenuConstant', 'UserInfoConstant', 'permissionList', 'permissions', '$mdSidenav', '$state', '$cookies', '$stateParams', '$rootScope', '$translate','$localStorage','Dialogs','$modal']
