/*********************************************************************************
* @name APP Jormat
* file that allows loading of dependencies, libraries and main application drivers
**********************************************************************************/

//require configuration file  
var localenvjson = require('../local.env.json')
window.taTools = {}

//load libraries 
require("angular-bootstrap-grid-tree")
require("select2")
require("angular-ui-select2")
require("angular-ui-select")
require("ng-notify")
require('angular-translate')
require('angular-translate-loader-static-files')
require('angular-cookies')
require('angular-translate-storage-cookie')
require('angular-translate-storage-local')
require('angular-bootstrap')
require('angular-loading-bar')
require('angular-bootstrap-nav-tree')
require('angular-ui-router')
require('angular-ui-utils')
require('angular-ui-map')
require('ngstorage')
require('oclazyload')
require('angular-resource')
require('angular-sanitize')
require('lodash')
require('angular-socket-io')
require('angular-validation-match')
require('angular-bootstrap-contextmenu')
require('angular-filter')
require('d3')
require('angular-daterangepicker')
require('angularUtils-pagination')
require('desandro-matches-selector')
require('angular-disable-all')
require('ng-csv')
require('jquery-ui')
require('bootstrap')
require('angular-material')
require("angular-loading-bar")
require("angular-bootstrap-nav-tree")
require("angular-xeditable")
require('angular-animate')
require('angular-aria')
require('angular-messages')
require('angular-touch')
require("ngImgCrop")
require("animate.css")
require("datatables")
require("footable")
require("waves")
require("pivottable")
require("slick-carousel")
require("c3")
require('angular-file-upload')
require('slick-carousel')
require('angular-slick')
require('textAngular')
require('angular-sanitize')
//require Script
require('../bower_components/html2canvas/build/html2canvas.min.js');
require("script!../bower_components/jspdf/dist/jspdf.min.js")
require("script!../bower_components/proj4/dist/proj4.js")
require("script!../bower_components/highcharts/highcharts.js")
require("script!../bower_components/highcharts/highcharts-more.js")
require("script!../bower_components/highcharts/modules/data.js")
require("script!../bower_components/highcharts/modules/map.js")
require("script!../bower_components/highcharts/modules/exporting.js")
require('script!../bower_components/highcharts-ng/dist/highcharts-ng.js')
require('script!../bower_components/moment/moment.js')
require('script!../bower_components/moment/locale/es.js')
require('script!../bower_components/angular-ui-tree/dist/angular-ui-tree.js')// angular-ui-tree js
require("script!../bower_components/alasql/dist/alasql.min.js")
require("script!../bower_components/alasql/dist/alasql.js")
require("script!../bower_components/js-xlsx/dist/xlsx.core.min.js")
require('script!../bower_components/select2/select2.js')
require("script!../bower_components/angular-bootstrap-calendar/dist/js/angular-bootstrap-calendar-tpls.js")
require('script!../bower_components/angular-ui-utils/ui-utils.js')
require('script!../bower_components/pdfmake/build/pdfmake.min.js')
require('script!../bower_components/pdfmake/build/vfs_fonts.js')
//require Script & CSS
require("../bower_components/angular-bootstrap-calendar/dist/css/angular-bootstrap-calendar.min.css")
require('../bower_components/angular-ui-tree/dist/angular-ui-tree.min.css')
require('../bower_components/select2/select2.css')
require('../bower_components/angular-ui-select/dist/select.js')
require("../bower_components/pivottable/dist/c3_renderers.js")
require("../bower_components/pivottable/dist/d3_renderers.js")
require("../bower_components/pivottable/dist/export_renderers.js")
require("../bower_components/font-awesome/css/font-awesome.css")
require("../bower_components/angular-ui-grid/ui-grid.js")
require("../bower_components/angular-ui-grid/ui-grid.css")
require('../bower_components/flot/jquery.flot.js');
require('../bower_components/flot/jquery.flot.pie.js');
require('../bower_components/flot-spline/js/jquery.flot.spline.min.js');
require('../bower_components/flot.orderbars/js/jquery.flot.orderBars.js');
require('../bower_components/flot.tooltip/js/jquery.flot.tooltip.js');
// require('../bower_components/angular-rut-validator/src/angular-rut-validator.js'); 
require("../bower_components/angular-xeditable/dist/css/xeditable.css")    
require('angular-flot');
require('../bower_components/angular-save-html-to-pdf/dist/saveHtmlToPdf.min.js');      

import "script!../bower_components/jquery/dist/jquery.min.js"
import "script!../bower_components/mapbox.js/mapbox.js"

//require file script permission
require('./services/permissionIndex')

//require file script and variable for switch styles according to customer
var clientName=localenvjson.clientName;
require("./assets/stylesheets/app-"+clientName+".less")

//require main files
import routing from './app.config'
import AsideCtrl from './aside.controller'
import loginOutController from './modal/loginOutController'
import companyInfoController from './modal/companyInfoController'
import asideHeader from './aside.header'
import GlobalSrv from './globalSrv'
import ValidationCtrl from './login/loginValidationCtrl'
import projects from './projects.js'

// components
import { filteredList } from './components/filtered-list'
import { evaluationStructure } from './components/evaluation-structure'
import { uiGridComponent } from './components/ui-grid-component'
import { dinamicInput } from './components/dinamic-inputs'
import { currentTime } from './components/current-time'
import { itemEvidence } from './components/item-evidence'
import { uStudyPlan } from './components/u-study-plan'
import { uComponentMatrix } from './components/u-study-plan/u-component-matrix'
import { filtersReport } from './components/filters-report'
import { currentSection } from './components/current-section'
import { occurrence } from './components/occurrence'
import { uTree } from './components/u-tree'
import { uiEditTable } from './components/ui-edit-table'
import { uLoadFile } from './components/u-load-file'
import { inputFormView } from './components/input-form-view'
import { occurrenceView } from './components/occurrence-view'
import { panelView } from './components/panel-view'
import { uDynamicForm } from './components/u-dynamic-form'
import { uDynamicOccurrences } from './components/u-dynamic-form/u-dynamic-occurrences'
import { uSingleOccurrence } from './components/u-dynamic-form/u-single-occurrence'
import { uDynamicPanel } from './components/u-dynamic-form/u-dynamic-panel'
import { changesHistory } from './components/changes-history'
import { parentList } from './components/parent-list'
import { listItem } from './components/parent-list/list-item'
import { tableCalendar } from './components/table-calendar'
import { uPensumMatrixEdit } from './components/u-study-plan/u-pensum-matrix-edit'
import { uPensumSignaturesEdit } from './components/u-study-plan/u-pensum-signatures-edit'
import uFunctions from './components/u-functions'
import dialogsHelpers from './components/dialogs-helper'

import myDirective from './services/userAuth.directive.js';
import * as pbi from '../bower_components/powerbi-client/dist/powerbi.js'
window['powerbi-client']=pbi
import powerBiDirective from './services/powerbi.directive.js';
import powerBiSrv from './services/powerBiSrv';

//dependencies
angular.module('app', 
    [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ngSanitize',
        'ngTouch',
        'ngMaterial',
        'ngStorage',
        'btford.socket-io',
        'ui.router',
        'ui.utils',
        'ui.bootstrap',
        'validation.match',
        'ui.router',
        'pascalprecht.translate',
        'angular-loading-bar',
        'ui.bootstrap.contextMenu',
        'angular.filter',
        'xeditable',
        'highcharts-ng',
        'treeGrid',
        'angularUtils.directives.dirPagination',
        'ui.select2',
        'ui.select',
        'daterangepicker',
        'ui.grid',
        'ui.grid.selection',
        'ui.grid.treeView',
        'ui.grid.grouping',
        'ui.grid.exporter',
        'ui.grid.cellNav',
        'ui.grid.edit',
        'ngNotify',
        'disableAll',
        'ui.grid.expandable',
        'ui.grid.pinning',
        'ui.grid.pagination',
        'ui.grid.resizeColumns',
        'ui.map',
        'slick',
        'uplannerPermission',
        'angularFileUpload',
        'ngCsv',
        'mwl.calendar',
        'ui.tree',
        'textAngular',
        'powerbiScopeWithController',
        'htmlToPdfSave',
        // 'gp.rutValidator',
        myDirective,
        projects,
        GlobalSrv,
        dialogsHelpers,
        uFunctions,
        powerBiSrv
    ])
    .config(routing)
    .component('filteredList', filteredList)
    .component('evaluationStructure', evaluationStructure)
    .component('uiGridComponent', uiGridComponent)
    .component('dinamicInput', dinamicInput)
    .component('currentTime', currentTime)
    .component('itemEvidence', itemEvidence)
    .component('uStudyPlan', uStudyPlan)
    .component('uComponentMatrix', uComponentMatrix)
    .component('filtersReport', filtersReport)
    .component('currentSection', currentSection)
    .component('occurrence', occurrence)
    .component('uTree', uTree)
    .component('uiEditTable', uiEditTable)
    .component('uLoadFile', uLoadFile)
    .component('inputFormView', inputFormView)
    .component('occurrenceView', occurrenceView)
    .component('panelView', panelView)
    .component('uDynamicForm', uDynamicForm)
    .component('uDynamicOccurrences', uDynamicOccurrences)
    .component('uSingleOccurrence', uSingleOccurrence)
    .component('uDynamicPanel', uDynamicPanel)
    .component('changesHistory', changesHistory)
    .component('parentList', parentList)
    .component('listItem', listItem)
    .component('tableCalendar', tableCalendar)
    .component('uPensumMatrixEdit', uPensumMatrixEdit)
    .component('uPensumSignaturesEdit', uPensumSignaturesEdit)
    .controller('AsideCtrl', AsideCtrl)
    .controller('loginOutController', loginOutController) 
    .controller('companyInfoController', companyInfoController) 
    .controller('asideHeader', asideHeader)
    .controller('loginValidationCtrl', ValidationCtrl)
    .constant("MenuConstant", [
        {
            "menu": []
        }
    ])
    .constant("ClientBaseFilePathConstant", [
        {
            "constant_url": []
        }
    ])
    .constant("UserInfoConstant", [
        {
            "details": []
        }
    ])
    .value("permissionList", [
        {
            "details": []
        }
    ])
	.filter('pesosChilenos', function() {
		return function(input) {
		  if (!isNaN(input)) {
			return input.toLocaleString('es-CL');
		  }
		  return input;
		};
	})
	.filter('pesosChilenosConSigno', function() {
		return function(input) {
		  if (!isNaN(input)) {
			return '$ ' + input.toLocaleString('es-CL');
		  }
		  return input;
		};
	})
	.filter('pesosInternacionales', function() {
		return function(input) {
		  if (!isNaN(input)) {
			return input.toLocaleString('es-ES', {
			  style: 'currency',
			  currency: 'EUR'
			});
		  }
		  return input;
		};
	  })
	  .filter('dolares', function() {
		return function(input) {
		  if (!isNaN(input)) {
			return input.toLocaleString('en-US', {
			  style: 'currency',
			  currency: 'USD'
			});
		  }
		  return input;
		};
	  })
    .constant('SERVICE_URL_CONSTANT', localenvjson.SERVICE_URL_CONSTANT)
    .constant('CLIENT_NAME', localenvjson.clientName)
    .constant('LOCAL_ENV_CONSTANT', localenvjson)
    .run(['$rootScope','$templateCache','$state','$stateParams','$timeout', 'permissions', 'permissionList','editableOptions', 'editableThemes', '$location',
        function ( $rootScope,$templateCache,$state,$stateParams,$timeout ,permissions,permissionList, editableOptions, editableThemes, $location) {

            // texto con valor editable
            editableOptions.theme = 'bs3'
            editableThemes['default'].submitTpl = '<button type="submit" class="aceptar"><i class="mdi-action-done"></i></button>'
            editableThemes['default'].cancelTpl = '<button type="button" class="cancelar" ng-click="$form.$cancel()"><i class="mdi-content-clear"></i></button>',

            $templateCache.put('aside.nav.uikit.html', require('./partials/aside.nav.uikit.html'))
            $rootScope.$state = $state
            $rootScope.$stateParams = $stateParams
        }
    ]).directive('errSrc', function() {
  return {
        link: function(scope, element, attrs) {

          var watcher = scope.$watch(function() {
              return attrs['ngSrc'];
            }, function (value) {
              if (!value) {
                element.attr('src', attrs.errSrc);  
              }
          });

          element.bind('error', function() {
            element.attr('src', attrs.errSrc);
          });

          //unsubscribe on success
          element.bind('load', watcher);

        }
      }
    });