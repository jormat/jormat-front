
(function() {
  angular.module('uplannerPermission', [])
        .factory('permissions',['$rootScope',permissions])
        .directive('hasPermission',['permissions', hasPermission ])
        .directive('hasDisabled',['permissions', hasDisabled])  
        .directive('hasDemand',['permissions','$cookies','$timeout', hasDemand])      
        .run(['$rootScope', '$templateCache','$state', '$stateParams', '$timeout', 'MenuConstant', 'permissions','permissionList','$cookies',
            function ( $rootScope,$templateCache,   $state,   $stateParams,$timeout,MenuConstant, permissions,permissionList,$cookies) {
              $rootScope.$state = $state;
              $rootScope.$stateParams = $stateParams;
              // console.log("run",permissionList )
              permissions.setPermissions(permissionList);
            }
        ])
        function hasPermission(permissions) {
          return {
            link: link
          }
          function link(scope, element, attrs) {
            console.log("");

            if(!_.isString(attrs.hasPermission)) {
              throw 'hasPermission value must be a string'
            }
            var value = attrs.hasPermission.trim();
            var notPermissionFlag = value[0] === '!';
            // console.log('vignesh',value+'',notPermissionFlag);
            if(notPermissionFlag) {
              value = value.slice(1).trim();
            }


            function toggleVisibilityBasedOnPermission() {
             
              var hasPermission = permissions.hasPermission(value);
               // console.log("permissionsChang", value)
              if(hasPermission) {
                // console.log("true , permision chan" , element)
                $(element).show(); 
              }
              else {
                 // console.log("false , permision chan" , element)
                 $(element).hide(); 
              }
            }
            function toggleVisibilityBasedOnPermissionDisabled() {
             
              var hasPermission = permissions.hasPermission(value);
               // console.log("permissionsChang", value)
              if(hasPermission) {
                // console.log("true , permision chan" , element)
                $(element).prop( "disabled", false );
              }
              else {
                 // console.log("false , permision chan" , element)
                 $(element).prop( "disabled", true );
              }
            }
             toggleVisibilityBasedOnPermission();
             scope.$on('permissionsChanged', toggleVisibilityBasedOnPermission);
          }
        }
        function hasDisabled(permissions) {
          return {
            link: link
          }
          function link(scope, element, attrs ) {
            console.log("link disabled");

            if(!_.isString(attrs.hasDisabled)) {
              throw 'hasDisabled value must be a string'
            }
            var value = attrs.hasDisabled.trim();
            var notPermissionFlag = value[0] === '!';
            // console.log('vignesh',value+'',notPermissionFlag);
            if(notPermissionFlag) {
              value = value.slice(1).trim();
            }
            console.log(value)


            function toggleVisibilityBasedOnPermission() {
             
              var hasPermission = permissions.hasPermission(value);
               !hasPermission ? $(element).attr("disabled", "disabled").off('click') :$(element).removeAttr("disabled");
        

            }

             toggleVisibilityBasedOnPermission();
             scope.$on('permissionsChanged', toggleVisibilityBasedOnPermission);
           }
        }   
      function hasDemand(permissions,$cookies,$timeout) {
          return {
            link: link
          }
          function link(scope, element, attrs) {
           
            function toggleVisibilityBasedOnPermission() {

              if(!_.isString(attrs.hasDemand)) {
              throw 'hasdemand value must be a string'
              }
              $timeout(function () {
                var value = attrs.hasDemand.trim();
                var hasdemand=$cookies.get('hasDemand');
                console.log('hasdemand',$cookies.get('hasDemand'))
                if(hasdemand=='true') {
                   $(element).show(); 
                }
                else {
                  $(element).hide(); 
                }
                        
              }, 200);
        
            }
             toggleVisibilityBasedOnPermission();
             scope.$on('permissionsChanged', toggleVisibilityBasedOnPermission);
          }
        }

        function permissions($rootScope) {

          var permissionList;
          return {
            setPermissions: setPermissions,
            hasPermission: hasPermission,
            hasDemand: hasDemand
          }
          function setPermissions(permission) {
            permissionList = permission;
            // console.log("setPermissions", $rootScope.$broadcast)

           $rootScope.$broadcast('permissionsChanged' );
          }
          
          function hasPermission(permission) {
            //  console.log("change peermission ",permissionList)
            if (typeof permissionList !=='undefined'){
            //   console.log(permissionList[permission] )
              if (typeof permissionList[permission] === 'undefined'){
                // console.log("false")
                //turnoff has-disabled and has permissions
                // return true;
              }else{
                return permissionList[permission] ;
              }
            }
          }

        }

})();
