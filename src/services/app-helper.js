// angular.module('cmmApp').factory('ClientBaseFilePath', function($http,ClientBaseFilePathConstant) {
   
//      return {
//             GetClientBaseFilePath: function(text){
//                    var re = new RegExp(text, "i");
//                    var ClientFolderName='';
//                    if(ClientBaseFilePathConstant[0]['constant_url']==''){
//                    $.ajax({
//                      url: "/api/user-api/directory",
//                      async :false
//                    })
//                      .done(function( data ) {
//                                ClientBaseFilePathConstant[0]['constant_url']=data;      
//                      });
//                     }
                    
//                     for (var i = 0; i < ClientBaseFilePathConstant[0]['constant_url'].dir.length; i++) {
//                         if (ClientBaseFilePathConstant[0]['constant_url'].dir[i].search(re) != -1) {
//                             ClientFolderName= ClientBaseFilePathConstant[0]['constant_url'].dir[i];
//                         }
//                      }
//                    return ClientFolderName;
//         } 
//     } 
    
// });



export default  class ClientBaseFilePath {
      constructor($http,ClientBaseFilePathConstant) {

     
       return {
              GetClientBaseFilePath: function(text){
                     var re = new RegExp(text, "i");
                     var ClientFolderName='';
                     if(ClientBaseFilePathConstant[0]['constant_url']==''){
                     $.ajax({
                       url: "/api/user-api/directory",
                       async :false
                     })
                       .done(function( data ) {
                                 ClientBaseFilePathConstant[0]['constant_url']=data;      
                       });
                      }
                      
                      for (var i = 0; i < ClientBaseFilePathConstant[0]['constant_url'].dir.length; i++) {
                          if (ClientBaseFilePathConstant[0]['constant_url'].dir[i].search(re) != -1) {
                              ClientFolderName= ClientBaseFilePathConstant[0]['constant_url'].dir[i];
                          }
                       }
                     return ClientFolderName;
          } 
      } 
      
  }
}

  ClientBaseFilePath.$inject=['$http','ClientBaseFilePathConstant']
  
  export default  angular.module('services.ClientBaseFilePath', [])
  .service('ClientBaseFilePath', ClientBaseFilePath)
  .name;


