export default angular.module('directive.userAuthDirective', [])
	.directive('userAuthDirective', function () {
		const controller = ['$rootScope','$scope', '$http', '$timeout', '$resource', '$location', 'MenuConstant', 'UserInfoConstant', 'permissionList', 'permissions', '$mdSidenav', '$state', '$cookies', '$stateParams','$localStorage','GlobalSrv','ngNotify',
			function ($rootScope,$scope, $http, $timeout, $resource, $location, MenuConstant, UserInfoConstant, permissionList, permissions, $mdSidenav, $state, $cookies, $stateParams,$localStorage,GlobalSrv,ngNotify) {
				const localenv=require('../../local.env.json');
				
				if (localenv.USER_API_URL != false) {
							const url = localenv.USER_API_URL + '/api/users/login'
							const serverUrl = localenv.SERVER_NAME + 'app' + $state.current.url

							$cookies.remove('hasDemand')
							$cookies.remove('hasType');
							$cookies.remove('groupids')

							const protocol = $location.protocol();

							if(protocol=='http'){
								$cookies.put('ESTSAUTH_INTIAL','true')
							}
							
							const ESTSAUTH = $cookies.get('ESTSAUTH_INTIAL');
							
							if((typeof ESTSAUTH ==='undefined' || ESTSAUTH=='false') && protocol=='https'){
								window.location = url
								return
							} else {
								if(localenv.loginType=='web') {
									wsdlFunctionCallback()
								}else if(localenv.loginType=='saml' || localenv.loginType=='adal'){
										samlFunctionCallback()
								}else{
										casAdalFunctionCallback()
								}
							}
				} else {
					$scope.admin = userSettingDefault.user
					UserInfoConstant[0].details.push(userSettingDefault)
					permissions.setPermissions(userSettingDefault.user.permissions[0])
					$scope.subitems = MenuConstant[0].menu
				}

				const flag = true

				function casAdalFunctionCallback(){
							$.ajax({
							dataType: 'jsonp',
							data: "returnTo="+serverUrl,
							jsonp: 'callback',
							url: url,
							cache: true,
							async: true,
							headers:  {
								'Access-Control-Allow-Origin': '*',
								'Access-Control-Allow-Methods' : 'GET,OPTIONS',
								'Access-Control-Allow-Headers' : 'X-Requested-With, Content-Type',
								'Content-Type' : 'text/plain',
								'Accept-Language' : 'en-US'
							},
							xhrFields: {
								withCredentials: true
							},
							crossDomain: true,
							success: function(response) {
							var temp=JSON.parse(decodeURIComponent(atob(response).split('').map(function(c) {
								return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
								}).join('')));

								response=temp
								setResponseData(response)
							},
							error: function (xhr, ajaxOptions, thrownError) {
								window.location = url;
								console.log(thrownError);
							}
							});
					}

				function samlFunctionCallback(){

					var searchObject = $location.search().idUserToken;

					if(typeof searchObject !=='undefined'){

						var temp=JSON.parse(decodeURIComponent(atob(searchObject).split('').map(function(c) {
								return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
									}).join('')));

						console.log(temp)


						GlobalSrv.getUserInfo.POST(temp).$promise.then((response) => {
							var data =response.data
						if (response.status == 1)
						{
							$localStorage.userLogin=data
							$location.search('idUserToken', null);

							if(typeof data ==='undefined'){
								window.location = url;
							}else{
								setResponseData(data)
							}
						
						}else{
							
							//  $cookies.remove('userLogin');
								delete $localStorage.userLogin;
								// window.location = url;
						}
						}).catch((error) => {
								window.location = url;
						})


					

					}else{

						var cookieObject=$localStorage.userLogin;

						if(typeof cookieObject ==='undefined'){
							window.location = url;
							}else{
							setResponseData(cookieObject)
							}

					}


				



				}

				function wsdlFunctionCallback(){
					const cookieObject=$localStorage.userLogin;
					
					if(typeof cookieObject ==='undefined') {
						$state.go('login');
					} else {
						setResponseData(cookieObject)
					}
				}

				function setResponseData(cookieObject){
					console.log(' cookieObject :::: ', cookieObject)
					const response =cookieObject;
					$scope.admin = response.user;
					$rootScope.admin = response.user;
					$localStorage.userLogin=response
					$cookies.put('token', response.token)
					UserInfoConstant[0].details.push(response);
					permissions.setPermissions(response.user.permissions[0]);
					$timeout(() => {
						$rootScope.urlAllowed=UserInfoConstant[0].details[0].user['url']
						$rootScope.subitems = MenuConstant[0].menu
						$rootScope.urlImage=response.user.picture
						console.log(response.user.picture)
						$rootScope.defaultImage=require('../images/user.png')
					}, 200)
				}
			}];
		
		return {
			restrict: 'EA', //Default in 1.3+
			scope: {
				datasource: '=',
				add: '&'//,
				// WORKSPACEID:'@workspaceid',
				// REPORTID:'@reportid'
			},
			controller: controller,
			// template: template
		}
	})
	.name

