/**
* @name APP ClassPanel
* @autor vignesh kumar somayaji
* @description
* # name Services --> powerBiSrv
* file for call at services for assistance page
*/
//  powerBiSrv.refreshDataset('f774845d-03f5-4650-b147-35ab22f19b6f','119eaef5-38e8-4cac-85ab-fba07439fb7d');
class powerBiSrv {

  constructor ($http,$resource, SERVICE_URL_CONSTANT, $q,$cookies,$localStorage,$timeout,LOCAL_ENV_CONSTANT) {


    var currentClass = null




    var refreshDataset = function (groupId,dataset) {
      var defered = $q.defer()
      var promise = defered.promise
      var token=null


           $http({
                    crossDomain: true,
                    xhrFields: {withCredentials: false},
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    method: 'POST',
                    url: LOCAL_ENV_CONSTANT.POWERBI_CONSTANT.API_URL
                }).then(function(response) {

                  var data=response.data.data
                  var tokenAttributes=JSON.parse(data);
                  var token=tokenAttributes.access_token

                  $localStorage.embeded=tokenAttributes
                  var token=$localStorage.embeded.access_token

            
                       
                   

                var dataset=LOCAL_ENV_CONSTANT.POWERBI_CONSTANT.CLIENT_DATASET

                var j=0
                  for(var i in dataset){
                   j++;
                    let get = {
                      groupId: LOCAL_ENV_CONSTANT.POWERBI_CONSTANT.CLIENT_GROUP_ID,
                      dataset: dataset[i]
                     }


                        $timeout(() => {

                            $http.post('https://api.powerbi.com/v1.0/myorg/groups/'+get.groupId+'/datasets/'+get.dataset+'/refreshes'
                            ,{
                            headers: {  'Authorization': 'Bearer  '+token,
                            'Content-Type': 'application/json; charset=utf-8'}
                            }).then(function (response) {

                               // console.log('response true',response)
                                defered.resolve(response)

                            }, function (error) {
                                 // $scope.reportError = error;
                            });



                      },  j * 1000)



                  }
 
                },function(response) {
                      defered.reject("Error:   "+JSON.stringify(response))
                });

          return promise

        }

 



    return {
     
      refreshDataset: refreshDataset,
    
    }
  }
}

powerBiSrv.$inject = ['$http','$resource', 'SERVICE_URL_CONSTANT', '$q','$cookies','$localStorage','$timeout','LOCAL_ENV_CONSTANT']

export default angular.module('services.powerBiSrv', [])
  .service('powerBiSrv', powerBiSrv)
  .name
