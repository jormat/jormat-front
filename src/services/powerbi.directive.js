
(function() {
  angular.module('powerbiScopeWithController', [])
.directive('powerbiScopeWithController', function () {
      
    var controller = ['$scope','$http','$timeout','$localStorage','LOCAL_ENV_CONSTANT',function ($scope,$http,$timeout,$localStorage,LOCAL_ENV_CONSTANT) {

          function init() {

          	   if(typeof $scope.WORKSPACEID==='undefined'){
          	     	return false
          	   }

          	    var  selectedWorkspace=$scope.WORKSPACEID

             
                if(typeof $localStorage.embeded==='undefined') 
                  var expiry=false 
                else
                   var expiry=parseInt($localStorage.embeded.expires_on);

                var offset=300;

                // expiry=1511196676 
        console.log(expiry,'expirytime',Math.round(new Date().getTime() / 1000.0) + offset)
				if (expiry && (expiry >  Math.round(new Date().getTime() / 1000.0) + offset)) {
          // console.log(expiry,'tttt',Math.round(new Date().getTime() / 1000.0) + offset)
				  // return true
          // delete $localStorage.embeded
				} else {
          delete $localStorage.embeded
				}

				 var getItems=$localStorage.embeded


               if(typeof getItems==='undefined'){

               	  $http({
                    crossDomain: true,
                    xhrFields: {withCredentials: false},
                    headers: {
                    	'Accept': 'application/json',
                        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
                    method: 'POST',
                    // data:$.param(param),
                    url: LOCAL_ENV_CONSTANT.POWERBI_CONSTANT.API_URL
                }).then(function(response) {

                        var data=response.data.data
                        // console.log("sample: "+JSON.stringify(data));
                    	var tokenAttributes=JSON.parse(data);
                    	var token=tokenAttributes.access_token

                        $localStorage.embeded=tokenAttributes
                  
            
                   getReport(selectedWorkspace,token);

				// 	// Get the list of reports



                    
                },function(response) {
                    console.log("Error:   "+JSON.stringify(response));
                });

               }else{
               	 getReport(selectedWorkspace,getItems.access_token);
               }
      

          }

          init();


          function getReport(selectedWorkspace ,token){


          			 $timeout(() => {
					        	$http.get('https://api.powerbi.com/v1.0/myorg/' + (selectedWorkspace ? 'groups/' + selectedWorkspace + '/reports' : 'reports')
										,{
								headers: {  'Authorization': 'Bearer  '+token,
							                'Content-Type': 'application/json; charset=utf-8'}
								}).then(function (response) {


									$scope.reports = response.data.value;

									for(var i in $scope.reports){  //'U-Forecast Datos'
										if($scope.reports[i].name==$scope.REPORTID){
										   $scope.selectedReport = $scope.reports[i];
										}
									}
									// $scope.selectedReport = $scope.reports[0].embedUrl;

				 
				                     var models = window['powerbi-client'].models;
									var embedConfiguration = {
									    type: 'report',
									    accessToken: token,
									    embedUrl: $scope.selectedReport.embedUrl,
									    permissions: models.Permissions.All,

									}; 
									 

									var $reportContainer = $('#embedContainer');
									 
									var report = powerbi.embedNew($reportContainer.get(0), embedConfiguration);
									 
									}, function (error) {
									$scope.reportError = error;
									});

                          }, 2000)




          }

      }],
        
      template ='<div id="embedContainer" style="height: 600.823px;"></div>';
      
      return {
          restrict: 'EA', //Default in 1.3+
          scope: {
              datasource: '=',
              add: '&',
              WORKSPACEID:'@workspaceid',
              REPORTID:'@reportid'
          },
          controller: controller,
          template: template
      };
  })

})();
