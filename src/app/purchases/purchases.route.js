
routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default function routes($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode(true);
      //$urlRouterProvider.otherwise('/');

  $stateProvider
     .state('app.providers', {
            abstract: false,
            url: '/proveedores',
            template: require('./providers/list/providers-list.html'),
            controller: 'providersListController',
            controllerAs:'providersList'
      })
     .state('app.providersCreate', {
            abstract: false,
            url: '/proveedores/crear-proveedor',
            template: require('./providers/create/providers-create.html'),
            controller: 'providersCreateController',
            controllerAs:'providersCreate'
        })
     .state('app.providersUpdate', {
            abstract: false,
            url: '/proveedores/actualizar-proveedor?idProvider',
            template: require('./providers/update/providers-update.html'),
            controller: 'providersUpdateController',
            controllerAs:'providersUpdate'
        })
     .state('app.invoicesProvider', {
            abstract: false,
            url: '/facturas-proveedores',
            template: require('./invoices-provider/list/invoices-provider-list.html'),
            controller: 'invoicesProviderListController',
            controllerAs:'invoicesProviderList'
      })
     .state('app.invoicesProviderCreate', {
            abstract: false,
            url: '/facturas-proveedores/crear-factura',
            template: require('./invoices-provider/create/invoices-provider-create.html'),
            controller: 'invoicesProviderCreateController',
            controllerAs:'invoicesProviderCreate'
        })
     .state('app.updateInvoiceProvider', {
            abstract: false,
            url: '/facturas-proveedores/crear-desde-importacion?importId',
            template: require('./invoices-provider/update/invoices-provider-update.html'),
            controller: 'invoicesProviderUpdateController',
            controllerAs:'invoicesProviderUpdate'
        })

     .state('app.invoicesProviderDraft', {
            abstract: false,
            url: '/facturas-proveedores/borrador?idInvoice',
            template: require('./invoices-provider/draft/invoices-provider-draft.html'),
            controller: 'invoicesProviderDraftController',
            controllerAs:'invoicesProviderDraft'
        })

     .state('app.invoicesProviderPrint', {
            abstract: false,
            url: '/facturas-proveedores/visualizar-factura?idInvoice',
            template: require('./invoices-provider/print/invoices-provider-print.html'),
            controller: 'invoicesProviderPrintController',
            controllerAs:'invoicesProviderPrint'
        })

        .state('app.warehouseInvoicesProvider', {
              abstract: false,
              url: '/facturas-proveedores/formato-bodega?idInvoice&originId',
              template: require('./invoices-provider/warehouse-print/warehouse-invoices.html'),
              controller: 'warehouseInvoicesProvidersController',
              controllerAs:'warehouseInvoicesProviders'
        })

     .state('app.paymentsProviders', {
            abstract: false,
            url: '/facturas-proveedores/pagos?idInvoice&type&providerName&total&date',
            template: require('./invoices-provider/payments/provider-payments.html'),
            controller: 'providerPaymentsController',
            controllerAs:'providerPayments'
        })

     .state('app.imports', {
            abstract: false,
            url: '/importaciones',
            template: require('./imports/list/imports-list.html'),
            controller: 'importsListController',
            controllerAs:'importsList'
      })

     .state('app.importsCreate', {
            abstract: false,
            url: '/importaciones/crear-importacion',
            template: require('./imports/create/imports-create.html'),
            controller: 'importsCreateController',
            controllerAs:'importsCreate'
      })

      .state('app.importDraft', {
       abstract: false,
       url: '/importaciones/borrador?idImport',
       template: require('./imports/draft/import-draft.html'),
       controller: 'importDraftController',
       controllerAs:'importDraft'
   })

     .state('app.importsPrint', {
            abstract: false,
            url: '/importaciones/visualizar-importacion?importId',
            template: require('./imports/print/imports-print.html'),
            controller: 'importsPrintController',
            controllerAs:'importsPrint'
      })

      .state('app.warehouseImportPrint', {
       abstract: false,
       url: '/importaciones/formato-bodega?importId&originId',
       template: require('./imports/warehouse-print/warehouse-imports.html'),
       controller: 'warehouseImportsController',
       controllerAs:'warehouseImports'
 })

     .state('app.importRegistration', {
            abstract: false,
            url: '/importaciones/registro-importacion',
            template: require('./import-registration/list/record-list.html'),
            controller: 'importRegistrationController',
            controllerAs:'importRegistration'
      })

     .state('app.importRegistrationCreate', {
            abstract: false,
            url: '/importaciones/registro-importacion/nuevo',
            template: require('./import-registration/create/record-create.html'),
            controller: 'importRegistrationCreateController',
            controllerAs:'importRegistrationCreate'
      })
     .state('app.importRegistrationUpdate', {
            abstract: false,
            url: '/importaciones/registro-importacion/actualizar?idRecord&total',
            template: require('./import-registration/update/record-update.html'),
            controller: 'importRegistrationUpdateController',
            controllerAs:'importRegistrationUpdate'
      })
      .state('app.importFiles',{
       abstract: false,
       url: './import-registration/update/upload-files',
       template: require('./import-registration/update/upload-files/import-upload.html'),
       controller: 'importRegistrationFilesUpController',
       controllerAs:'importFiles'    
       })
     .state('app.supplierReturn', {
            abstract: false,
            url: '/devolucion-proveedor',
            template: require('./supplier-return/list/supplier-return-list.html'),
            controller: 'supplierReturnListController',
            controllerAs:'supplierReturnList'
      })

     .state('app.supplierReturnCreate', {
            abstract: false,
            url: '/devolucion-proveedor/crear',
            template: require('./supplier-return/create/supplier-return-create.html'),
            controller: 'supplierReturnCreateController',
            controllerAs:'supplierReturnCreate'
      })

     .state('app.wishlist', {
            abstract: false,
            url: '/listado-deseos',
            template: require('./wishlist/list/wish-list.html'),
            controller: 'wishListController',
            controllerAs:'wishList'
      })

     .state('app.preOrders', {
            abstract: false,
            url: '/pre-pedidos',
            template: require('./pre-orders/list/pre-orders-list.html'),
            controller: 'preOrdersListController',
            controllerAs:'preOrdersList'
      })

     .state('app.preOrdersCreate', {
            abstract: false,
            url: '/pre-pedidos/crear?parameters',
            template: require('./pre-orders/create/pre-orders-create.html'),
            controller: 'preOrdersCreateController',
            controllerAs:'preOrdersCreate'
      })

     .state('app.preOrdersPrint', {
            abstract: false,
            url: '/pre-pedidos/imprimir?orderId',
            template: require('./pre-orders/print/pre-orders-print.html'),
            controller: 'preOrdersPrintController',
            controllerAs:'preOrdersPrint'
      })


      ;
 
}
