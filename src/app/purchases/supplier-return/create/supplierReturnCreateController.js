
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> supplierReturnCreateController
* # As Controller --> supplierReturnCreate														
*/

export default class supplierReturnCreateController{

        constructor($scope,UserInfoConstant,$timeout,supplierReturnServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal){
            var vm = this;
            vm.suplierReturnCreate = suplierReturnCreate
            vm.setName = setName
            vm.setInvoice = setInvoice
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.removeItems = removeItems
            vm.getNet = getNet
            vm.showPanel = showPanel
            vm.loadItems = loadItems
            vm.webItemUpdate = webItemUpdate
            vm.invoiceCreate = invoiceCreate
            $scope.parseInt = parseInt
            $scope.discount = 0
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }
            $scope.historyItems = []

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            providersServices.providers.getProviders().$promise.then((dataReturn) => {
                  $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            loadItems()

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.supplierReturnCreate.loadItem(row.entity)" type="button" class="btn btn-danger btn-xs">+</button>'+
                                   '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.supplierReturnCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{row.entity.vatPrice}}</strong></p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    }
                ]
            }
            
            function loadItems(){
                itemsServices.items.getItems().$promise.then((dataReturn) => {
                    $scope.itemsGrid.data = dataReturn.data;
                },(err) => {
                    console.log('No se ha podido conectar con el servicio',err);
                })
            }

            $scope.invoiceItems = []

            function loadItem(row){
                 $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    price: row.netPrice,
                    minPrice: row.netPrice,
                    webType: row.webType, 
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                });

                 $scope.warningItems = false    
            }

            function getNet() {
                var net = 0
                  var i = 0
                     for (i=0; i<$scope.invoiceItems.length; i++) {
                         var valueItems = $scope.invoiceItems[i] 
                         net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 

                      }

                      $scope.NET =  net
                      $scope.netoInvoice = net - (($scope.discount*net)/100) 
                      $scope.total = Math.round($scope.netoInvoice * 1.19)
                      $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                      return net;
                      
            }


            function removeItems(index) {

                $scope.invoiceItems.splice(index, 1);
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                $scope.warehouseId = warehouseId
            }

            function showPanel() {
                $scope.panelInvoice = true
            }

            function setName(providers) {
                $scope.providerName = providers.providerName
                $scope.providerId = providers.providerId
                $scope.selectedRut = providers.rut
                getListInvoice()
            }

            function setInvoice(fac) {
               $scope.invoiceId = fac.invoiceId
            }

            function getListInvoice(){
                let param = {
                    dsCode: $scope.selectedRut
                }

                supplierReturnServices.invoices.getListInvoices(param).$promise.then((dataReturn) => {
                    $scope.listInvoice = dataReturn.data;
                },(err) => {
                    console.log('No se ha podido conectar con el servicio',err);
                })
                
            }

            function suplierReturnCreate() {

                        let model = {
                            userId: $scope.userInfo.id,
                            invoiceId: $scope.invoiceId,
                            providerId : $scope.providerId,
                            originId : $scope.warehouseId,
                            type: Number($scope.type),
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            comment : $scope.observation,
                            itemsInvoices : $scope.invoiceItems 
                        }

                          console.log('data',model);
                          
                          for(var  i in model.itemsInvoices){
                            let modelo = {
                                itemId: model.itemsInvoices[i].itemId,
                                originId :$scope.warehouseId,
                                quantity: model.itemsInvoices[i].quantity
                             }
            
                                itemsServices.items.getHistoric(modelo).$promise.then((dataReturn) => {
                                   $scope.item = dataReturn.data;
        
                                    $scope.historyItems.push({
                                    itemId: $scope.item[0].itemId,
                                    itemDescription: $scope.item[0].itemDescription,
                                    location : $scope.item[0].locations,
                                    previousStock: $scope.item[0].stock,
                                    price: $scope.item[0].vatPrice,
                                    quantity: modelo.quantity,
                                    generalStock: $scope.item[0].generalStock,
                                    currentprice: $scope.item[0].vatPrice,
                                    currentLocation: $scope.item[0].locations
                                    })
                                        
                                    console.log('that', $scope.historyItems);
        
                                    if (i== $scope.historyItems.length-1) {
                                          
                                        invoiceCreate(model,$scope.historyItems)
                                        $scope.ultimo = "último registro";
                                        } else {
                                        $scope.ultimo = "item"+i;
                                        }
                                        console.log( $scope.ultimo); 
                                        },(err) => {
                                          console.log('No se ha podido conectar con el servicio',err);
                                    })
                        }
                            

                        
                    }

            function invoiceCreate(model,historyItems){

                supplierReturnServices.supplierReturn.createSupplierReturn(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado la devolución correctamente','success')
                    $scope.result = dataReturn.data

                    $state.go('app.supplierReturn')

                    for(var  i in historyItems){

                        $scope.item = dataReturn.data;
  
                        let itemsValue = {
                          documentId: $scope.result,
                          itemId: historyItems[i].itemId,
                          itemDescription: historyItems[i].itemDescription,
                          location : historyItems[i].location,
                          previousStock: historyItems[i].previousStock,
                          price: historyItems[i].price,
                          quantity: historyItems[i].quantity,
                          originId: $scope.warehouseId,
                          userId: $scope.userInfo.id,
                          document: "DEV",
                          type: "Salida -",
                          generalStock: historyItems[i].generalStock,
                          currentprice: historyItems[i].price,
                          currentLocation: historyItems[i].currentLocation,
                          destiny: undefined
  
                        }
                       
                       console.log('that',itemsValue);
         
                       itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                              $scope.result2 = dataReturn.data;
                              console.log('Historial del item actualizado correctamente');
                          },(err) => {
                              console.log('No se ha podido conectar con el servicio',err);
                          })
                    }

                        for(var  i in model.itemsInvoices){

                                console.log('entra',model.itemsInvoices)

                                if ($scope.invoiceItems[i].webType == "SI"){
                                    console.log('tipo web')

                                    let modelo = {
                                        itemId : $scope.invoiceItems[i].itemId
                                    }

                                    itemsServices.items.getGeneralStockWeb(modelo).$promise.then((dataReturn) => {
                                       $scope.result = dataReturn.data;
                                       $scope.stock = $scope.result[0].generalStock;

                                       webItemUpdate(modelo.itemId,$scope.stock)

                                      },(err) => {
                                          console.log('No se ha podido conectar con el servicio',err);
                                      })

                                     }
                                else{
                                    console.log('normal')
                                    // $state.go('app.supplierReturn')
                                }
                             }

                        
                      },(err) => {
                          ngNotify.set('Error al crear devolución','error')
                      })

            }

            function webItemUpdate(itemId,stock) {

                let data = {
                                    sku : itemId.toString(),
                                    stock_quantity: stock
                                }

                                itemsServices.woocommerceAPI.itemUpdate(data).$promise.then((dataReturn) => {
                                  $scope.result = dataReturn.data;
                                  $scope.message= dataReturn.message;
                                  $scope.status= dataReturn.status;

                                  if ($scope.status==200  ) {

                                     ngNotify.set('API web: '+ $scope.message,'info')
                                     // $state.go('app.supplierReturn')

                                      }else{
                                        console.log('Error actualizar stock web',$scope.message);
                                        ngNotify.set( 'API web: ' + $scope.message, {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                        })
                                        // $state.go('app.supplierReturn')
                                     }
                                  
                                  },(err) => {
                                        console.log('Error actualizar stock web',err);
                                        ngNotify.set( 'Error API Web', {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                      })
                                  })

            }

           
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

supplierReturnCreateController.$inject = ['$scope','UserInfoConstant','$timeout','supplierReturnServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal'];

