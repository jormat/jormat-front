/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> supplierReturnViewController
* # As Controller --> supplierReturnView														
*/


  export default class supplierReturnViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,supplierReturnServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.validateNote = validateNote
        $scope.returnId = $scope.supplierReturnData.returnId
        $scope.providerName = $scope.supplierReturnData.providerName

        //function to show userId

        $scope.UserInfoConstant = UserInfoConstant
        $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
            if (details !== undefined) {
                $scope.userInfo = details.user
            }
        })
            
        let params = {
                returnId: $scope.returnId
              }

        supplierReturnServices.supplierReturn.getSupplierReturnDetails(params).$promise.then((dataReturn) => {
              $scope.supplierReturn = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getSupplierReturnDetails',err);
              })

        supplierReturnServices.supplierReturn.getSupplierReturnItems(params).$promise.then((dataReturn) => {
              $scope.supplierReturnItems = dataReturn.data

              var subTotal = 0
                var i = 0
                   for (i=0; i<$scope.supplierReturnItems.length; i++) {
                       subTotal = subTotal + $scope.supplierReturnItems[i].total  
                    }
                    
                    $scope.subTotality =  subTotal
                    
              console.log('that',$scope.supplierReturnItems);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getSupplierReturnItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function validateNote() {
            let model = {
                    returnId : $scope.returnId,
                    userId : $scope.userInfo.id 
                }

                console.log('that',model)

                supplierReturnServices.creditNotes.validateNote(model).$promise.then((dataReturn) => {

                    ngNotify.set('Nota de crédito actualizada exitosamente','success');
                    $state.reload("app.creditNotes")
                    $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al validar Nota','error')
                  })
        }

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

		


    }

  }

  supplierReturnViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','supplierReturnServices','$location','UserInfoConstant','ngNotify','$state'];
