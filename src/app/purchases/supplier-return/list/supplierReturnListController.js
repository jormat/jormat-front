
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> supplierReturnListController
* # As Controller --> supplierReturnList														
*/

export default class supplierReturnListController{

        constructor($scope,UserInfoConstant,$timeout,supplierReturnServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,$window,LOCAL_ENV_CONSTANT,i18nService){
            var vm = this
            vm.viewInvoice = viewInvoice 
            vm.viewSupplierReturn = viewSupplierReturn
            vm.viewNotePayments = viewNotePayments
            vm.searchData=searchData
            vm.createXML =createXML
            vm.loadNotes = loadNotes
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }


            loadNotes()
            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.supplierReturnGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'devoluciones_proveedor.csv',
                showColumnFooter: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'returnId',  
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.supplierReturnList.viewSupplierReturn(row.entity)">{{row.entity.returnId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'providerName',
                      width: '32%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.providerName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Factura',
                      field: 'invoiceId',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.supplierReturnList.viewInvoice(row.entity)"> <p class="badge bg-accent">{{row.entity.invoiceId}}</p></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '10%',
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{row.entity.total}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '11%'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-secondary font-italic" style="font-size: 12px;">{{row.entity.type}}</p>' +
                                 '</div>'   
                    },
                    
                    
                    // { 
                    //   name: 'Estado', 
                    //   field: 'statusName', 
                    //   width: '9%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="{{(row.entity.statusName == \'Aplicada\')?\'badge bg-warning\':\'badge bg-danger\'}}">{{row.entity.statusName}}</p>' +
                    //              '</div>'
                    // },
                    
                    // { 
                    //   name: 'Usuario', 
                    //   field: 'userCreation', 
                    //   width: '10%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class=" badge bg-secondary font-italic" style="font-size: 12px;">{{row.entity.userCreation}}</p>' +
                    //              '</div>'   
                    // },
                    
                    { 
                      name: 'Acciones',
                      width: '8%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadNotes(){

            supplierReturnServices.supplierReturn.getSupplierReturn().$promise.then((dataReturn) => {
              $scope.supplierReturnGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }


            function searchData() {
              supplierReturnServices.supplierReturn.getSupplierReturn().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.supplierReturnGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewSupplierReturn(row){
	            $scope.supplierReturnData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/supplier-return-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'supplierReturnViewController',
	                    controllerAs: 'supplierReturnView',
	                    size: 'lg'
	            })
	          }

            function viewInvoice(row){
                $scope.invoiceData = row
                var modalInstance  = $modal.open({
                        template: require('../../invoices-provider/view/invoices-provider-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'invoicesProviderViewController',
                        controllerAs: 'invoicesProviderView',
                        size: 'lg'
                })
            }

            function createXML(row) { 
              // $window.open('http://localhost/jormat-system/XML_note.php?noteId='+row.creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
              $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_note.php?noteId='+row.creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
 
            }

            function viewNotePayments(row){

              console.log('row',row)
              
              $state.go("app.paymentNotesView",{ 
                    noteId: row.creditNoteId,
                    idClient: row.clientId,
                    clientName: row.clientName,
                    total: row.total,
                    date: row.date
                });

            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

supplierReturnListController.$inject = ['$scope','UserInfoConstant','$timeout','supplierReturnServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','$window','LOCAL_ENV_CONSTANT','i18nService'];

