/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> supplierReturnServices
* file for call at services for supplier Return
*/

class supplierReturnServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var supplierReturn = $resource(SERVICE_URL_CONSTANT.jormat + '/notes/returns/:route',{},{
      
        getSupplierReturn: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getSupplierReturnDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createSupplierReturn: {
            method:'POST',
            params : {
                route: ''
            }
        },
        validateNote: {
            method:'PUT',
            params : {
                route: 'validate'
            }
        },

        createCreditNotesFailure: {
            method:'POST',
            params : {
                route: 'failure'
            }
        },
        disabledCreditNotes: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        getSupplierReturnItems: {
            method:'GET',
            params : {
                route: 'items'
            }
        },
        getReportNotes: {
            method:'GET',
            params : {
                route: 'report'
            }
        }
    })

    var invoices = $resource(SERVICE_URL_CONSTANT.jormat + '/invoices/:route',{},{
        getListInvoices: {
            method:'GET',
            params : {
                route: 'list'
            }
        }
    })

    return { 
        supplierReturn : supplierReturn,
        invoices : invoices
    }
  }
}

  supplierReturnServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.supplierReturnServices', [])
  .service('supplierReturnServices', supplierReturnServices)
  .name;