

import routing from './purchases.route';
import run from './purchases.run';

//providers controllers
import providersListController from './providers/list/providersListController';
import providersViewController from './providers/view/providersViewController';
import providersCreateController from './providers/create/providersCreateController';
import providersUpdateController from './providers/update/providersUpdateController';
import providersDeleteController from './providers/delete/providersDeleteController';
import providersServices from './providers/providersServices';

//imports controllers
import importsListController from './imports/list/importsListController'; 
import importsViewController from './imports/view/importsViewController';
import importsCreateController from './imports/create/importsCreateController';
import importsPrintController from './imports/print/importsPrintController';
import importDraftController from './imports/draft/importDraftController';
import itemsHistoryController from './imports/items-history/itemsHistoryController'; 
import itemsTransitController from './imports/items-transit/itemsTransitController'; 
import warehouseImportsController from './imports/warehouse-print/warehouseImportsController';
import importsServices from './imports/importsServices';

import importRegistrationController from './import-registration/list/importRegistrationController'; 
import importRegistrationCreateController from './import-registration/create/importRegistrationCreateController';
import importRegistrationUpdateController from './import-registration/update/importRegistrationUpdateController';
import importRecordAlertController from './import-registration/alert/importRecordAlertController'; 
import importRegistrationFilesUpController from './import-registration/update/upload-files/importRegistrationFilesUpController';
import importPaymentsDeleteController from './import-registration/delete/importPaymentsDeleteController'; 
import importRegistrationServices from './import-registration/importRegistrationServices';
import uploadCsvDialogController from './imports/create/dialogs/uploadCsv/uploadCsvDialogController';
import imageViewController from './import-registration/imageView/imageViewController';


//providers invoices controllers
import invoicesProviderListController from './invoices-provider/list/invoicesProviderListController';
import invoicesProviderViewController from './invoices-provider/view/invoicesProviderViewController';
import invoicesProviderCreateController from './invoices-provider/create/invoicesProviderCreateController';
import invoicesProviderUpdateController from './invoices-provider/update/invoicesProviderUpdateController';
import invoicesProviderDeleteController from './invoices-provider/delete/invoicesProviderDeleteController';
import invoicesProviderPrintController from './invoices-provider/print/invoicesProviderPrintController';
import invoicesProviderDraftController from './invoices-provider/draft/invoicesProviderDraftController';
import providerPaymentsController from './invoices-provider/payments/providerPaymentsController'; 
import payDeleteController from './invoices-provider/pay-delete/payDeleteController';
import warehouseInvoicesProvidersController from './invoices-provider/warehouse-print/warehouseInvoicesProvidersController';
import invoicesProviderServices from './invoices-provider/invoicesProviderServices';

//supplier-return
import supplierReturnListController from './supplier-return/list/supplierReturnListController';
import supplierReturnViewController from './supplier-return/view/supplierReturnViewController';
import supplierReturnCreateController from './supplier-return/create/supplierReturnCreateController';
// import providersUpdateController from './providers/update/providersUpdateController';
// import providersDeleteController from './providers/delete/providersDeleteController';
import supplierReturnServices from './supplier-return/supplierReturnServices';

//wishlist
import wishListController from './wishlist/list/wishListController';
import wishlistCreateController from './wishlist/create/wishlistCreateController';
import wishlistDeleteController from './wishlist/delete/wishlistDeleteController';
import addItemsController from './wishlist/actions/addItemsController';
import wishlistServices from './wishlist/wishlistServices';

//pre-orders
import preOrdersListController from './pre-orders/list/preOrdersListController';
import preOrdersViewController from './pre-orders/view/preOrdersViewController';
import preOrdersPrintController from './pre-orders/print/preOrdersPrintController';
import preOrdersCreateController from './pre-orders/create/preOrdersCreateController'; 
import preOrdersServices from './pre-orders/preOrdersServices';


export default angular.module('app.purchases', [providersServices,invoicesProviderServices,importsServices,importRegistrationServices,supplierReturnServices,wishlistServices,preOrdersServices])
  .config(routing)
  .controller('imageViewController', imageViewController)
  .controller('providersListController', providersListController)
  .controller('providersViewController', providersViewController)
  .controller('providersCreateController', providersCreateController)
  .controller('providersUpdateController', providersUpdateController)
  .controller('invoicesProviderDraftController', invoicesProviderDraftController)
  .controller('providersDeleteController', providersDeleteController)
  .controller('importsListController', importsListController) 
  .controller('importsViewController', importsViewController) 
  .controller('importsCreateController', importsCreateController) 
  .controller('importDraftController', importDraftController)
  .controller('importsPrintController', importsPrintController)
  .controller('warehouseImportsController', warehouseImportsController)
  .controller('importRegistrationController', importRegistrationController) 
  .controller('importRegistrationCreateController', importRegistrationCreateController) 
  .controller('importRegistrationUpdateController', importRegistrationUpdateController) 
  .controller('importRegistrationFilesUpController', importRegistrationFilesUpController) 
  .controller('importPaymentsDeleteController', importPaymentsDeleteController) 
  .controller('importRecordAlertController', importRecordAlertController)
  .controller('itemsHistoryController', itemsHistoryController) 
  .controller('itemsTransitController', itemsTransitController)
  .controller('invoicesProviderListController', invoicesProviderListController)
  .controller('invoicesProviderViewController', invoicesProviderViewController)
  .controller('invoicesProviderCreateController', invoicesProviderCreateController)
  .controller('invoicesProviderUpdateController', invoicesProviderUpdateController)
  .controller('invoicesProviderDeleteController', invoicesProviderDeleteController)
  .controller('invoicesProviderPrintController', invoicesProviderPrintController) 
  .controller('warehouseInvoicesProvidersController', warehouseInvoicesProvidersController)
  .controller('providerPaymentsController', providerPaymentsController) 
  .controller('payDeleteController', payDeleteController)   
  .controller('supplierReturnListController', supplierReturnListController) 
  .controller('supplierReturnViewController', supplierReturnViewController) 
  .controller('supplierReturnCreateController', supplierReturnCreateController)
  .controller('invoicesProviderPrintController', invoicesProviderPrintController)
  .controller('uploadCsvDialogController', uploadCsvDialogController) 
  .controller('wishListController', wishListController) 
  .controller('wishlistCreateController', wishlistCreateController)
  .controller('wishlistDeleteController', wishlistDeleteController) 
  .controller('addItemsController', addItemsController)
  .controller('preOrdersListController', preOrdersListController) 
  .controller('preOrdersViewController', preOrdersViewController) 
  .controller('preOrdersCreateController', preOrdersCreateController) 
  .controller('preOrdersPrintController', preOrdersPrintController) 
  .run(run)
  .constant("purchases", [{
        "title": "Mercadería",
        "icon":"mdi-truck",
        "subMenu":[
            {
            'title' : 'Facturas Proveedor',
            'url': 'app.invoicesProvider',
            },
            {
            'title' : 'Proveedores',
            'url': 'app.providers',
            },
            {
            'title' : 'Importaciones',
            'url': 'app.imports',
            },
            {
            'title' : 'Embarque',
            'url': 'app.importRegistration',
            },
            {
            'title' : 'Lista de deseos',
            'url': 'app.wishlist',
            },
            {
            'title' : 'Devolución proveedor',
            'url': 'app.supplierReturn',
            }
        ]
    }])
  .name



