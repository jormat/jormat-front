
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> importRegistrationCreateController
* # As Controller --> importRegistrationCreate														
*/

export default class importRegistrationCreateController{

        constructor($scope,UserInfoConstant,$timeout,importRegistrationServices,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,$modal){
            var vm = this;
            vm.recordCreate = recordCreate
            vm.validateCurrency = validateCurrency
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            $scope.currencySymbol = '$'

            function validateCurrency(currency){
                if (currency == 'Euro') {
                    $scope.currencySymbol = '€'
                }else{
                    $scope.currencySymbol = '$'
                }
            }

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    let model = {
                            userId : $scope.userInfo.id
                        }

                    warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                      $scope.warehouses = dataReturn.data;
                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                    })
                }
            })
            
            //

            providersServices.providers.getProviders().$promise.then((dataReturn) => {
                  $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
     

            function setName(providers) {

                $scope.providerName = providers.providerName
                $scope.providerId = providers.providerId
            }

            function setWarehouseId(warehouseId){
                $scope.warehouseId = warehouseId
            }


            function recordCreate() {

                        let model = {
                            userId: $scope.userInfo.id,
                            providerId : $scope.providerId,
                            originId : $scope.warehouseId,
                            codeInvoice : $scope.code,
                            shipmentNumber : $scope.shipmentNumber,
                            total : $scope.total,
                            currency : $scope.currency,
                            currencyValue : $scope.valueCurrency,
                            packing : $scope.packing,
                            packingQuantity : $scope.packingQuantity,
                            arrivalDate : moment($scope.arrivalDate).format("YYYY-MM-DD"),
                            receptionDate : moment($scope.receptionDate).format("YYYY-MM-DD"),
                            customsTax : $scope.customsTax,
                            // payDateTax : moment($scope.payDateTax).format("YYYY-MM-DD"),
                            comment : $scope.observation
                            
                        }      

                        console.log('modelo',model)

                        importRegistrationServices.importRegistration.recordsCreate(model).$promise.then((dataReturn) => {
                          $scope.result = dataReturn.data       
                             ngNotify.set( 'Registro de importación creado exitosamente ', {
                                button: true,
                                type : 'success'
                            })
                            // $state.go("app.importRegistration");
                            $scope.recordData = $scope.result;
                                var modalInstance  = $modal.open({
                                        template: require('../alert/import-record-alert.html'),
                                        animation: true,
                                        scope: $scope,
                                        controller: 'importRecordAlertController',
                                        controllerAs: 'importRecordAlert',
                                        backdrop: 'static'
                                })
                
                          },(err) => {
                              ngNotify.set('Error al crear documento','error')
                          })
                    }
           
        }//FIN CONSTRUCTOR

        // Funciones
        

    }   

importRegistrationCreateController.$inject = ['$scope','UserInfoConstant','$timeout','importRegistrationServices','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','$modal'];

