/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> importPaymentsDeleteController
* # As Controller --> importPaymentsDelete														
*/


  export default class importPaymentsDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,importRegistrationServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.deletePayment = deletePayment
	    $scope.paymentsId = $scope.paymentsId
        $scope.recordId = $scope.recordId 
        $scope.total = $scope.total

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

	    function cancel() {
			$modalInstance.dismiss('chao');
		}

		function deletePayment() {
			let model = {
                    paymentId : $scope.paymentsId,
                    recordId : $scope.recordId,
                    userId : $scope.userInfo.id
                }

                importRegistrationServices.importRegistration.disabledPayment(model).$promise.then((dataReturn) => {
                    ngNotify.set('Pago eliminado exitosamente','success');
                    $state.reload("app.importRegistrationUpdate")
					$modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al eliminar pago','error')
                  })
		}
	    
    }

  }

  importPaymentsDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','importRegistrationServices','$location','UserInfoConstant','ngNotify','$state'];
