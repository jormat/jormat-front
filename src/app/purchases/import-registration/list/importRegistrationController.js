
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> importRegistrationController
* # As Controller --> importRegistration														
*/

export default class importRegistrationController{

        constructor($scope,UserInfoConstant,$timeout,importRegistrationServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.viewRecord = viewRecord
            vm.searchData = searchData
            vm.loadDocuments= loadDocuments
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');
            loadDocuments()

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.importsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_registros_importacion.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'importId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.importRegistration.viewRecord(row.entity)">{{row.entity.importId}}</a>' +
                                 '</div>'    
                    },
                    { 
                      name:'Proveedor',
                      field: 'providerName',
                      width: '28%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;">{{row.entity.providerName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{row.entity.total}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Invoice', 
                      field: 'codeInvoice', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-accent" style="font-size: 11px;">{{row.entity.codeInvoice}}</p>' +
                                 '</div>'   
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '10%'
                    },
                    
                    
                    { 
                      name: 'Moneda', 
                      field: 'currency',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" label bg-info" > {{row.entity.currency }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Estado', 
                      field: 'arrivalStatus', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="{{(row.entity.arrivalStatus == \'Recepcionada\')?\'badge bg-primary\':\'badge bg-secundary\'}}" style="font-size: 9px;"> {{row.entity.arrivalStatus}}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Pago', 
                      field: 'statusName', 
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="{{(row.entity.statusName == \'PAG\')?\'label bg-success\':\'label bg-warning\'}}" style="font-size: 10px;"  > {{row.entity.statusName }}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: '',
                      width: '5%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadDocuments(){

            importRegistrationServices.importRegistration.getRecords().$promise.then((dataReturn) => {
              $scope.importsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

        
            function searchData() {
              importRegistrationServices.importRegistration.getRecords().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.importsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewRecord(row){
	             $state.go("app.importRegistrationUpdate", { idRecord: row.importId,total: row.total});
	       }


            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

importRegistrationController.$inject = ['$scope','UserInfoConstant','$timeout','importRegistrationServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

