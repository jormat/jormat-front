/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> imageViewController
* # As Controller --> imageView														
*/
export default class imageViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,itemsServices,$location,UserInfoConstant,ngNotify,$state, SERVICE_URL_CONSTANT) {
	    this.$scope = $scope
		//this.importId = this.$scope.importData.importId 
		this.baseUrl = SERVICE_URL_CONSTANT
		this.$modalInstance = $modalInstance

		

		this.$scope.picture= require("../../../../images/unknown.png");

	    this.$scope.UserInfoConstant = UserInfoConstant
		this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
			if (details !== undefined) {
				this.$scope.userInfo = details.user
				this.main()
			}
		})
    }

	main() {
		if(this.$scope.importData){
			this.importId = this.$scope.importData.importId 
			this.importName = this.$scope.importData.name
			
			this.$scope.imageUrl = this.baseUrl.jormat + '/purchases/file/' + this.importId + `?fileName=${this.importName}`

		}else{
			console.log("error en main");
		}
	}

	cancel() {
		this.$modalInstance.dismiss('chao');
	}

  }

  imageViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','itemsServices','$location','UserInfoConstant','ngNotify','$state', 'SERVICE_URL_CONSTANT'];
