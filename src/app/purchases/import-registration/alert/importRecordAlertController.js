/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> importRecordAlertController
* # As Controller --> importRecordAlert														
*/


  export default class importRecordAlertController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,invoicesProviderServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.loadPayments = loadPayments
	    $scope.recordId = $scope.recordData
      console.log('recordId',$scope.recordId )

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
        //

	    function cancel() {
			$modalInstance.dismiss('chao');
            $state.go("app.importRegistration");
            
		}

		function loadPayments(){
              $state.go("app.importRegistrationUpdate", { idRecord: $scope.recordId});
            }
	    


    }

  }

  importRecordAlertController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','invoicesProviderServices','$location','UserInfoConstant','ngNotify','$state'];
