
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> importsPrintController
* # As Controller --> importsPrint
														
*/


  export default class importsPrintController {

    constructor($scope, $filter,$rootScope, $http,importsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.transformToInvoice = transformToInvoice
        vm.validateImport = validateImport
        $scope.importId = $stateParams.importId

        let params = {
                importId: $scope.importId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        importsServices.imports.getImportsDetails(params).$promise.then((dataReturn) => {
              $scope.import = dataReturn.data
                if ($scope.import[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.import[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                    }else{
                        $scope.currencySymbol = '$'
                    }

                }else{
                    $scope.internationalType = false
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Imports Details',err);
              })

        importsServices.imports.getImportsItems(params).$promise.then((dataReturn) => {
              $scope.importItems = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Imports Items',err);
              })

    	function cancel() {

		 $state.go("app.imports")
		}

        function transformToInvoice(row){

          $state.go("app.updateInvoiceProvider", { importId: $scope.importId});

        }

        function print() {
              window.print();

        }

        function validateImport(value) {
            
         let model = {
                    importId : $scope.importId,
                    userId : $scope.userInfo.id,
                    statusValue : value 
                }

                console.log('that',model)

                  importsServices.imports.updateImport(model).$promise.then((dataReturn) => {

                    ngNotify.set('documento importacion validado exitosamente','success');
                    $state.go("app.imports")
                  },(err) => {
                      ngNotify.set('Error al validar documento','error')
                  })

                

                
        }


    }

  }

  importsPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','importsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
