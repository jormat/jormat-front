
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> importRegistrationUpdateController
* # As Controller --> importRegistrationUpdate														
*/

export default class importRegistrationUpdateController {

    constructor($scope, UserInfoConstant, importRegistrationServices, paymentsServices, ngNotify, $state, $mdDialog, uiGridConstants, $http, $stateParams, $modal, SERVICE_URL_CONSTANT) {
        var vm = this
		this.$mdDialog = $mdDialog 
        this.$scope = $scope
		this.$stateParams = $stateParams
		this.$state = $state
		this.$modal = $modal
		this.importRegistrationServices = importRegistrationServices
		this.ngNotify = ngNotify
        this.$scope.importId = this.$stateParams.idRecord
        this.total = this.$stateParams.total
        this.currencySymbol = '$'
		this.paymentsServices = paymentsServices
        // $scope.updateImportRegistration = true
        this.totalDocs = 4;
        this.uploadedDocs = 0;
        this.$scope.files = [];

		$scope.getFiles = function () {
			const filesUrl = SERVICE_URL_CONSTANT.jormat + `/purchases/files/` + $scope.importId
			$http.get(filesUrl).then(function(response) {
				$scope.files = response.data
			}); 
		}

        this.$scope.$watch('files', () => {
            this.$scope.getFiles()
		}, true)
        this.$scope.UserInfoConstant = UserInfoConstant
        this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
            if (details !== undefined) {
                this.$scope.userInfo = details.user
				this.main()
            }
        })
    }

	main(){
		this.loadImportRegistrationDetails()
		this.getPayments()
		this.loadPayments()
	}

	loadImportRegistrationDetails() {
		const params = {
			importId: this.$scope.importId
		}

		this.importRegistrationServices.importRegistration.getImportsRegistrationDetails(params).$promise.then((dataReturn) => {
			this.record = dataReturn.data;
			this.$scope.dataRecord = {
				providerName: this.record[0].providerName,
				providerId: this.record[0].providerId,
				codeInvoice: this.record[0].codeInvoice,
				origin: this.record[0].origin,
				date: this.record[0].date,
				shipmentNumber: this.record[0].shipmentNumber,
				total: this.record[0].total,
				status: this.record[0].statusName,
				arrivalStatus: this.record[0].arrivalStatus,
				currency: this.record[0].currency,
				valueCurrency: this.record[0].valueCurrency,
				receptionDate: this.record[0].receptionDate,
				packing: this.record[0].packing,
				packingQuantity: this.record[0].packingQuantity,
				arrivalDate: this.record[0].arrivalDate,
				payDateTax: this.record[0].payDateTax,
				closeDate: this.record[0].closeDate,
				customsTax: this.record[0].customsTax,
				observation: this.record[0].commentary
			}

			if (this.record[0].currency == 'Euro') {
				this.currencySymbol = '€'
			} else {
				this.currencySymbol = '$'
			}

		}, (err) => {
			console.log('No se ha podido conectar con el servicio', err);
		})
	}

	loadPayments() {
		const  model = {
				recordId: this.$stateParams.idRecord,
				userId: this.$scope.userInfo.id
			}

		this.importRegistrationServices.importRegistration.importRegistrationPaymentList(model).$promise.then((dataReturn) => {
			this.$scope.paymentsGrid = dataReturn.data;
			console.log('paymets',this.$scope.paymentsGrid)

			let sumPayments = 0
			let i = 0
			for (i = 0; i < this.$scope.paymentsGrid.length; i++) {
				sumPayments = sumPayments + this.$scope.paymentsGrid[i].total
			}
			this.$scope.sum = sumPayments
			this.$scope.pending = this.$stateParams.total - this.$scope.sum
			if (this.$scope.sum >= this.$stateParams.total) {
				this.$scope.updateImportRegistration = false
				this.importRegistrationServices.importRegistration.updateStatusRecord(model).$promise.then((dataReturn) => {
					this.ngNotify.set('Este documento de importación ha sido pagado completamente', 'info')
					this.loadImportRegistrationDetails()
				}, (err) => {
					console.log('No se ha podido conectar con el servicio update Status Record', err);
				})
			} else {
				this.$scope.updateImportRegistration = true
				this.loadImportRegistrationDetails()
			}
		}, (err) => {
			console.log('error al cargar payments', err);
		})
	}

	getPayments(){
		this.paymentsServices.paymentForms.getPaymentForms().$promise.then((dataReturn) => {
            this.$scope.paymentForms = dataReturn.data;
        }, (err) => {
            console.log('No se ha podido conectar con el servicio getPaymentForms', err);
        })
	}

	savePayment() {
		const model = {
			userId: this.$scope.userInfo.id,
			recordId: this.$stateParams.idRecord,
			total: this.$scope.payment,
			currencyValue: this.$scope.currencyValue,
			numberDocument: this.$scope.document,
			paymentFormId: this.$scope.paymentFormId,
			comment: this.$scope.observation
		}

		this.importRegistrationServices.importRegistration.createImportRegistrationPayment(model).$promise.then((dataReturn) => {
			this.ngNotify.set('Se ha generado el pago correctamente', 'warn')
			this.loadPayments()
			this.$scope.observation = ''
			this.$scope.payment = ''
			this.$scope.document = ''
			this.$scope.currencyValue = ''
		}, (err) => {
			this.ngNotify.set('Error al generar pago', 'error')
		})
	}

	deletePayment(paymentsId, total) {
		this.$scope.paymentsId = paymentsId;
		this.$scope.recordId = this.$stateParams.idRecord
		this.$scope.total = total
		let modalInstance = this.$modal.open({
			template: require('../delete/payments-delete.html'),
			animation: true,
			scope: this.$scope,
			controller: 'importPaymentsDeleteController',
			controllerAs: 'importPaymentsDelete'
		})
	}

	updateImportRegistration() {
		const model = {
			userId: this.$scope.userInfo.id,
			recordId: this.$stateParams.idRecord,
			codeInvoice: this.$scope.dataRecord.codeInvoice,
			shipmentNumber: this.$scope.dataRecord.shipmentNumber,
			packingQuantity: this.$scope.dataRecord.packingQuantity,
			total: this.$scope.dataRecord.total,
			customsTax: this.$scope.dataRecord.customsTax,
			currencyValue: this.$scope.dataRecord.valueCurrency,
			closeDate: moment(this.$scope.closeDate).format("YYYY-MM-DD"),
			payDateTaxCustom: moment(this.$scope.payDateTaxCustom).format("YYYY-MM-DD"),
			comment: this.$scope.dataRecord.observation
		}

		this.importRegistrationServices.importRegistration.updateImportRegistration(model).$promise.then((dataReturn) => {
			this.ngNotify.set('Se ha actualizado el documento de manera correcta', 'success')
			this.loadImportRegistrationDetails()
		}, (err) => {
			this.ngNotify.set('Error al actualizar documento', 'error')
		})
	}

	closeRecord(value) {
		const model = {
			userId: this.$scope.userInfo.id,
			recordId: this.$stateParams.idRecord,
			closeDate: moment(this.$scope.closeDate).format("YYYY-MM-DD"),
			value: value
		}

		this.importRegistrationServices.importRegistration.closeImportRegistration(model).$promise.then((dataReturn) => {
			this.ngNotify.set('Fecha de recepción actualizada exitosamente', 'grimace')
			this.loadImportRegistrationDetails()
		}, (err) => {
			this.ngNotify.set('Error al actualizar documento', 'error')
		})
	}

    openFile (file) {

		if('.jpg' === file.name.substr(-4, 4) || '.jpeg' === file.name.substr(-5, 5) || '.png' === file.name.substr(-4, 4)){
			this.viewImage(file.name)
		}else {
			window.open(file.url2, '_blank');
		}
    }

	deleteFile (file) {
		const json = {
			fileName : file.name,
			id : this.$scope.importId
		}

		this.importRegistrationServices.purchases.deleteFile(json).$promise.then((dataReturn) => {
			this.ngNotify.set('Archivo eliminado exitosamente', 'success')
			this.$state.reload('app.importRegistrationUpdate')
		}, (err) => {
			this.ngNotify.set('Error al eliminar archivo', 'error')
		})
		
	}

	viewImage(fileName) {
		this.$scope.importData = {
			importId: this.$scope.importId, 
			name : fileName
		}
		const 	modalInstance  = this.$modal.open({
													template: require('./../imageView/imageView.html'),
													animation: true,
													scope: this.$scope,
													controller: 'imageViewController',
													controllerAs: 'imageView'
												})
	}

    uploadIMG() {
        const id = this.$scope.importId

        this.confirm = this.$mdDialog.confirm({
            controller: 'importRegistrationFilesUpController',
            controllerAs: 'importFiles',
            template: require('./upload-files/import-upload.html'),
            locals: { importId: id },
            animation: true,
            parent: angular.element(document.body),
            clickOutsideToClose: true,
        })
        this.$mdDialog.show(this.confirm).then(() => {
			this.$state.reload('app.importRegistrationUpdate')
			
        })
    }


}

importRegistrationUpdateController.$inject = ['$scope', 'UserInfoConstant', 'importRegistrationServices', 'paymentsServices', 'ngNotify', '$state', '$mdDialog', 'uiGridConstants', '$http', '$stateParams', '$modal', 'SERVICE_URL_CONSTANT'];

