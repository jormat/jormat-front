/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> importsViewController
* # As Controller --> importsView														
*/


  export default class importsViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,importsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.currencySymbol = '$'
        $scope.importId = $scope.invoiceData.importId
        $scope.providerName = $scope.invoiceData.providerName 
      
        let params = {
            importId: $scope.importId
          }

        importsServices.imports.getImportsDetails(params).$promise.then((dataReturn) => {
              $scope.import = dataReturn.data
                if ($scope.import[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.import[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                    }else{
                        $scope.currencySymbol = '$'
                    }

                }else{
                    $scope.internationalType = false
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

        importsServices.imports.getImportsItems(params).$promise.then((dataReturn) => {
              $scope.importItems = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get getInvoicesItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

		


    }

  }

  importsViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','importsServices','$location','UserInfoConstant','ngNotify','$state'];
