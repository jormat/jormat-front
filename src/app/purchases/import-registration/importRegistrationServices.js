/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> importRegistrationServices
* file for call at services for providers
*/

class importRegistrationServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var importRegistration = $resource(SERVICE_URL_CONSTANT.jormat + '/documents/import/registration/:route',{},{
      
        getRecords: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        recordsCreate: {
            method:'POST',
            params : {
               route: ''
            }
        },
        getImportsRegistrationDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        importRegistrationPaymentList: {
            method:'GET',
            params : {
               route: 'payments'
            }
        },
        createImportRegistrationPayment: {
            method:'POST',
            params : {
                route: 'payments'
            }
        },

        disabledPayment: {
            method:'DELETE',
            params : {
                route: 'payments'
            }
        },

        updateStatusRecord: {
            method:'PUT',
            params : {
                route: 'status'
            }
        },

        updateImportRegistration: {
            method:'PUT',
            params : {
                route: ''
            }
        },

        closeImportRegistration: {
            method:'PUT',
            params : {
                route: 'close'
            }
        }
    })

	const purchases = $resource(SERVICE_URL_CONSTANT.jormat + '/purchases/:route',{},{
		deleteFile: {
			method:'DELETE',
			params: {
				route: 'file'
			}
		}
	})

  

    return { 

            importRegistration : importRegistration,
			purchases : purchases
           }
  }
}

  importRegistrationServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.importRegistrationServices', [])
  .service('importRegistrationServices', importRegistrationServices)
  .name;