
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> providerPaymentsController
* # As Controller --> providerPayments														
*/

export default class providerPaymentsController{

        constructor($scope,UserInfoConstant,$timeout,paymentsServices,invoicesProviderServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams,warehousesServices,itemsServices,$modal,clientsInvoicesServices){
            var vm = this
            vm.viewInvoice = viewInvoice 
            vm.deletePayment = deletePayment
            vm.createPayment = createPayment
            vm.getInvoiceDetails= getInvoiceDetails
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.currencySymbol = '$'

            getInvoiceDetails()

            //function to show userId


            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.userId = $scope.userInfo.id
                    getPayments($scope.userId)
                }
            })
            //


            function getInvoiceDetails(){
                  let params = {
                    invoiceId: $stateParams.idInvoice 
                }

                console.log('servicio devuelve estado factura');

                invoicesProviderServices.invoicesProviders.getInvoicesDetails(params).$promise.then((dataReturn) => {
                  $scope.invoices = dataReturn.data;
                  $scope.status = $scope.invoices[0].statusName
                  $scope.currencyName = $scope.invoices[0].currency
                  $scope.invoiceCode = $scope.invoices[0].invoiceCode
                  $scope.rut = $scope.invoices[0].rut
                  $scope.type = $scope.invoices[0].type
                  $scope.origin = $scope.invoices[0].origin
                  console.log('servicio devuelve estado factura',$scope.invoices);

                  if ($scope.invoices[0].currency == 'Euro') {
                     $scope.currencySymbol = '€'
                    }else{
                     $scope.currencySymbol = '$'
                    }

                  },(err) => {
                      console.log('No se ha podido llamar con el servicio getInvoicesProvidersDetails',err);
                })
            } 

            

            $scope.invoiceId = $stateParams.idInvoice
            $scope.typeShort = $stateParams.type
            $scope.providerName = $stateParams.providerName

            $scope.total = parseInt($stateParams.total)
            $scope.date = $stateParams.date
            // $scope.pending = $stateParams.pending
            $scope.status = $stateParams.status

            if ($scope.pending == 0) {
                $scope.statusPayment = 'Cancelada' 
            }else{
                $scope.statusPayment = 'Pendiente'
            }

        
            function getPayments(user){
                let params = {
                    invoiceId: $stateParams.idInvoice,
                    userId: user
                }
                paymentsServices.payments.getProvidersPayments(params).$promise.then((dataReturn) => {
                  $scope.paymentsGrid = dataReturn.data;
                  let sumPayments = 0
                  let i = 0
                     for (i=0; i<$scope.paymentsGrid.length; i++) {
                         sumPayments = sumPayments + parseInt($scope.paymentsGrid[i].total)
                      }
                      $scope.sum =  sumPayments
                      $scope.pending = $scope.total - $scope.sum
                      if ($scope.pending == 0) {
                            $scope.statusPayment = 'Cancelada'
                            paymentsServices.payments.updateProviderInvoiceStatus(params).$promise.then((dataReturn) => {
                                ngNotify.set('Esta factura ha sido cancelada completamente','warn')
                            },(err) => {
                                console.log('No se ha podido conectar con el servicio getPaymentForms',err);
                            })
                        }else{
                            $scope.statusPayment = 'Pendiente'
                        }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err)
                  })

            }
            
            paymentsServices.paymentForms.getPaymentForms().$promise.then((dataReturn) => {
              $scope.paymentForms = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getPaymentForms',err);
            })

            function deletePayment(paymentsId){
                  $scope.paymentsId = paymentsId;
                  $scope.invoiceId = $stateParams.idInvoice
                  var modalInstance  = $modal.open({
                          template: require('../pay-delete/pay-delete.html'),
                          animation: true,
                          scope: $scope,
                          controller: 'payDeleteController',
                          controllerAs: 'payDelete'
                  })
            } 

            function viewInvoice(invoiceId){
                $scope.invoiceData = {
                    invoiceId:invoiceId,
                    providerName:$scope.providerName,
                    invoiceCode:  $scope.invoiceCode
                }
                var modalInstance  = $modal.open({
                        template: require('../../invoices-provider/view/invoices-provider-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'invoicesProviderViewController',
                        controllerAs: 'invoicesProviderView',
                        size: 'lg'
                })
            }

            function createPayment() {

                    let model = {
                        userId: $scope.userInfo.id,
                        invoiceId: $stateParams.idInvoice,
                        clientId : $stateParams.idClient,
                        date : $scope.CurrentDate,
                        total : $scope.payment,
                        numberDocument : $scope.document,
                        paymentFormId : $scope.paymentFormId,
                        comment : $scope.observation
                    }     

                     paymentsServices.payments.createProviderPayment(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado el pago correctamente','success')
                        getInvoiceDetails()
                        getPayments(model.userId)
                        $scope.observation = ''
                        $scope.payment = ''
                        $scope.document = ''
                      },(err) => {
                        ngNotify.set('Error al ingresar pago','error')
                     })
                }
            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

providerPaymentsController.$inject = ['$scope','UserInfoConstant','$timeout','paymentsServices','invoicesProviderServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams','warehousesServices','itemsServices','$modal','clientsInvoicesServices'];

