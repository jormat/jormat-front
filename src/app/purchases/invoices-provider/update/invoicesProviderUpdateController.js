
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> invoicesProviderUpdateController
* # As Controller --> invoicesProviderUpdate														
*/

export default class invoicesProviderUpdateController{

        constructor($scope,UserInfoConstant,$timeout,invoicesProviderServices,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,$modal,$stateParams,importsServices){
            var vm = this;
            $scope.importId= $stateParams.importId 
            vm.createInvoice = createInvoice
            vm.getNetTotal = getNetTotal
            vm.validateCurrency = validateCurrency
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.loadItems = loadItems
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getNet = getNet
            vm.invoiceCreate = invoiceCreate
            vm.webItemUpdate= webItemUpdate
            $scope.internacionalInvoice = false
            $scope.nacionalInvoice = true
            $scope.parseInt = parseInt
            $scope.invoiceItems = []
            $scope.discount = 0
            $scope.observation = "Factura basada en documento importacion N° " + $scope.importId
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }

            function validateCurrency(currency){
                if (currency == 'Euro') {
                    $scope.currencySymbol = '€'
                }else{
                    $scope.currencySymbol = '$'
                }
            }

            $scope.historyItems = []

            loadImport()
            loadItems()

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    let model = {
                            userId : $scope.userInfo.id
                        }

                    warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                      $scope.warehouses = dataReturn.data;
                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                    })
                }
            })
            
            //

            providersServices.providers.getProviders().$promise.then((dataReturn) => {
                  $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })



            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="label bg-accent" ng-click="grid.appScope.invoicesProviderUpdate.loadItem(row.entity)">+</a>' +
                                 '</div>'
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.invoicesProviderUpdate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '44%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '39%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  
                    }
                ]
            };

            function loadItems(){
                itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }
            
            // function validateType(type) {
            //   $scope.typeInvoice = type

            //     if (type == 2) {
            //         $scope.internacionalInvoice = true
            //         $scope.nacionalInvoice = false
            //     }else{
            //         $scope.internacionalInvoice = false
            //         $scope.nacionalInvoice = true
            //     }
            // }

            function loadItem(row){

              // var i = 0
              // for (i=0; i<$scope.invoiceItems.length; i++) {
              //       var id = $scope.invoiceItems[i].itemId

              //       if(id == row.itemId){
              //         ngNotify.set('Item '+ row.itemId +' Duplicado en este Documento','warn')
              //         $scope.invoiceItems.splice(index, 1)

              //       }else{
              //         console.log("item no puplicado",row.itemId)
              //       }
                    
              //     }

                  
                 $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    referenceKey: row.brandCode,
                    price: row.netPurchaseValue,
                    netPurchaseValue: row.netPurchaseValue,
                    quantity: 1,
                    disscount: 0

                });

                 // $scope.warningItems = false    
            }

            function getNet() {
                
                var net = 0
                  var i = 0
                     for (i=0; i<$scope.invoiceItems.length; i++) {
                         var valueItems = $scope.invoiceItems[i] 
                         net += valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100; 

                      }

                      if ($scope.typeInvoice == 1){

                        $scope.NET =  net
                        $scope.netoInvoice = net - (($scope.discount*net)/100) 
                        $scope.total = $scope.netoInvoice * 1.19
                        $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                        return net;

                        }else{

                            $scope.NET =  net
                            $scope.netoInvoice = net - (($scope.discount*net)/100) 
                            $scope.total = $scope.netoInvoice
                            $scope.VAT = 0
                            return net;
                        }
                             
            }

            function getNetTotal() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += valueItems.netPurchaseValue * valueItems.quantity; 

                  }
                  
                  $scope.netTotal =  net
                  return $scope.netTotal;
                      
            }

            function removeItems(index) {

                $scope.invoiceItems.splice(index, 1);
            }

            function setName(providers) {

                $scope.providerName = providers.providerName
                $scope.providerId = providers.providerId
            }

            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                $scope.warehouseId = warehouseId
            }

            function showPanel() {
                $scope.panelInvoice = true

            }

            function loadImport(){

                let model = {
                    importId: $scope.importId
                  }
          
                importsServices.imports.getImportsDetails(model).$promise.then((dataReturn) => {
                   $scope.importDetails = dataReturn.data
                   $scope.valueCurrency = $scope.importDetails[0].currencyValue
                },(err) => {
                    console.log('No se ha podido conectar con el servicio getImportsDetails',err);
                })

                importsServices.imports.getImportsItems(model).$promise.then((dataReturn) => {
                      $scope.importItems = dataReturn.data
                      console.log('$scope.importItems',$scope.importItems)
                      for(var i in $scope.importItems){
                            $scope.invoiceItems.push({
                                itemId: $scope.importItems[i].itemId,
                                itemDescription: $scope.importItems[i].itemDescription,
                                referenceKey: $scope.importItems[i].brandCode,
                                price: $scope.importItems[i].price,
                                netPurchaseValue: $scope.importItems[i].netPurchaseValue,
                                disscount: $scope.importItems[i].discount,
                                quantity: $scope.importItems[i].quantityItems
                         })
                    }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get import items',err);
                    })
            }

            function createInvoice() {

                        let model = {
                            userId: $scope.userInfo.id,
                            invoiceCode: $scope.code,
                            providerId : $scope.providerId,
                            date : $scope.CurrentDate,
                            originId : $scope.warehouseId,
                            type: $scope.selected,
                            type: 2,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            netTotal : $scope.netTotal,
                            comment : $scope.observation,
                            currency : $scope.currency,
                            valueCurrency : $scope.valueCurrency,
                            importId : $scope.importId,
                            itemsInvoices : $scope.invoiceItems 
                        }      

                        console.log('modelo',model)

                        for(var  i in model.itemsInvoices){
                            let modelo = {
                                itemId: model.itemsInvoices[i].itemId,
                                originId :$scope.warehouseId,
                                quantity: model.itemsInvoices[i].quantity
                             }
            
                                itemsServices.items.getHistoric(modelo).$promise.then((dataReturn) => {
                                   $scope.item = dataReturn.data;
        
                                    $scope.historyItems.push({
                                    itemId: $scope.item[0].itemId,
                                    itemDescription: $scope.item[0].itemDescription,
                                    location : $scope.item[0].locations,
                                    previousStock: $scope.item[0].stock,
                                    price: $scope.item[0].vatPrice,
                                    quantity: modelo.quantity,
                                    generalStock: $scope.item[0].generalStock,
                                    currentprice: $scope.item[0].vatPrice,
                                    currentLocation: $scope.item[0].locations
                                    })
                                        
                                    console.log('that', $scope.historyItems);
        
                                    if (i== $scope.historyItems.length-1) {
                                          
                                      invoiceCreate(model,$scope.historyItems)
                                      $scope.ultimo = "último registro";
                                      } else {
                                        $scope.ultimo = "item"+i;
                                      }
                                        console.log( $scope.ultimo); 
                                       },(err) => {
                                          console.log('No se ha podido conectar con el servicio',err);
                                    })
                        }
                        
            }


            function invoiceCreate(model,historyItems){

                console.log('now',model,historyItems)

                invoicesProviderServices.invoicesProviders.createInvoiceProvider(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado la factura correctamente','success')
                    $scope.result = dataReturn.data
                    $state.go('app.invoicesProvider')

                    for(var  i in historyItems){

                        $scope.item = dataReturn.data;
  
                        let itemsValue = {
                          documentId: $scope.result,
                          itemId: historyItems[i].itemId,
                          itemDescription: historyItems[i].itemDescription,
                          location : historyItems[i].location,
                          previousStock: historyItems[i].previousStock,
                          price: historyItems[i].price,
                          quantity: historyItems[i].quantity,
                          originId: $scope.warehouseId,
                          userId: $scope.userInfo.id,
                          document: "PRO",
                          type: "Entrada +",
                          generalStock: historyItems[i].generalStock,
                          currentprice: historyItems[i].price,
                          currentLocation: historyItems[i].currentLocation,
                          destiny: undefined
  
                        }
                       
                       console.log('that',itemsValue);
         
                       itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                              $scope.result2 = dataReturn.data;
                              console.log('Historial del item actualizado correctamente');
                          },(err) => {
                              console.log('No se ha podido conectar con el servicio',err);
                          })
                    }

                    for(var  i in model.itemsInvoices){

                        console.log('entra',model.itemsInvoices)
  
                        if ($scope.invoiceItems[i].webType == "SI"){
                            console.log('tipo web')
  
                            let modelo = {
                                itemId : $scope.invoiceItems[i].itemId
                            }
  
                            itemsServices.items.getGeneralStockWeb(modelo).$promise.then((dataReturn) => {
                               $scope.result = dataReturn.data;
                               $scope.stock = $scope.result[0].generalStock;
  
                               webItemUpdate(modelo.itemId,$scope.stock)
  
                              },(err) => {
                                  console.log('No se ha podido conectar con el servicio',err);
                              })
                             }
  
                        else{
                            console.log('item normal')
                        }
                    }
                  },(err) => {
                      ngNotify.set('Error al crear factura','error')
                  })
            }

            function webItemUpdate(itemId,stock) {

                let data = {
                                    sku : itemId.toString(),
                                    stock_quantity: stock
                                }

                                itemsServices.woocommerceAPI.itemUpdate(data).$promise.then((dataReturn) => {
                                  $scope.result = dataReturn.data;
                                  $scope.message= dataReturn.message;
                                  $scope.status= dataReturn.status;

                                  if ($scope.status==200  ) {

                                     ngNotify.set('API web: '+ $scope.message,'info')

                                      }else{
                                        console.log('Error actualizar stock web',$scope.message);
                                        ngNotify.set( 'API web: ' + $scope.message, {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                        })
                                     }
                                  
                                  },(err) => {
                                        console.log('Error actualizar stock web',err);
                                        ngNotify.set( 'Error API Web', {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                      })
                                  })

            }
           
        }//FIN CONSTRUCTOR

        // Funciones
          
    }   
invoicesProviderUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','invoicesProviderServices','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','$modal','$stateParams','importsServices'];

