
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> invoicesProviderListController
* # As Controller --> invoicesProviderList														
*/

export default class invoicesProviderListController{

        constructor($scope,UserInfoConstant,$timeout,invoicesProviderServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.viewInvoice = viewInvoice
            vm.updateInvoice= updateInvoice
            vm.deleteInvoice = deleteInvoice
            vm.printInvoice = printInvoice
            vm.payments = payments
            vm.searchData=searchData
            vm.loadInvoices= loadInvoices
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');
            loadInvoices()

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.invoicesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_facturas_proveedor.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'invoiceId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.invoicesProviderList.viewInvoice(row.entity)">{{row.entity.invoiceId}}</a>' +
                                 '</div>'    
                    },
                    { 
                      name:'Codigo',
                      field: 'invoiceCode',  
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 10px;"><em> {{row.entity.invoiceCode}}</em></p>' +
                                 '</div>'        
                    }, 
                    { 
                      name:'Proveedor',
                      field: 'providerName',
                      width: '23%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">{{row.entity.providerName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Total $/€',
                      field: 'total',
                      width: '8%',
                      cellTemplate:`<div class="ui-grid-cell-contents">`+ 
								 `<p><strong>{{ (row.entity.type === 'NAC') ? (row.entity.total | pesosChilenosConSigno) : ((row.entity.currency === 'Dolar') ? (row.entity.total | dolares) : (row.entity.total | pesosInternacionales)) }}</strong></p>`+
                                 `</div>`
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" label bg-warning" > {{row.entity.origin }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" label bg-info" style="font-size: 10px;" > {{row.entity.type }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Moneda', 
                      field: 'currency',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" label bg-secundary" style="font-size: 10px;"> {{row.entity.currency }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Import. ID', 
                      field: 'importId',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<p> <strong>{{row.entity.importId }}</strong></p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 11px;" class="badge bg-{{row.entity.style}}">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: '',
                      width: '7%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadInvoices(){

              invoicesProviderServices.invoicesProviders.getInvoicesProviders().$promise.then((dataReturn) => {
              $scope.invoicesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

        
            function searchData() {
              invoicesProviderServices.invoicesProviders.getInvoicesProviders().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.invoicesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewInvoice(row){
	            $scope.invoiceData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/invoices-provider-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'invoicesProviderViewController',
	                    controllerAs: 'invoicesProviderView',
	                    size: 'lg'
	            })
	       }

            function printInvoice(row){
             $state.go("app.invoicesProviderPrint", { idInvoice: row.invoiceId});
            }

            function payments(row){
              console.log(row)

             $state.go("app.paymentsProviders", { 
                idInvoice: row.invoiceId,
                type: row.type,                
                providerName: row.providerName,
                total: row.total,
                date: row.date
              });
            }

            function updateInvoice(row){
              $state.go("app.invoicesProviderDraft", { idInvoice: row.invoiceId});
            }

            function deleteInvoice(row){
              $scope.invoiceData = row;
              var modalInstance  = $modal.open({
                      template: require('../delete/invoices-provider-delete.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'invoicesProviderDeleteController',
                      controllerAs: 'invoicesProviderDelete'
              })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

invoicesProviderListController.$inject = ['$scope','UserInfoConstant','$timeout','invoicesProviderServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

