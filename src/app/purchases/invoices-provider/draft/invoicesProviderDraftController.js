
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> invoicesProviderDraftController
* # As Controller --> invoicesProviderDraft														
*/

export default class invoicesProviderDraftController{

        constructor($scope,UserInfoConstant,$timeout,invoicesProviderServices,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,$modal,$stateParams,importsServices){
            var vm = this;
            $scope.invoiceId= $stateParams.idInvoice 
            vm.invoiceSave = invoiceSave
            vm.getNetTotal = getNetTotal
            vm.validateCurrency = validateCurrency
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.loadItems = loadItems
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getNet = getNet
            $scope.internacionalInvoice = false
            $scope.parseInt = parseInt
            $scope.invoiceItems = []
            $scope.discount = 0
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }

            function validateCurrency(currency){
                if (currency == 'Euro') {
                    $scope.currencySymbol = '€'
                }else{
                    $scope.currencySymbol = '$'
                }
            }

            loadImport()
            loadItems()

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    let model = {
                            userId : $scope.userInfo.id
                        }

                    warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                      $scope.warehouses = dataReturn.data;
                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                    })
                }
            })
            
            //

            providersServices.providers.getProviders().$promise.then((dataReturn) => {
                  $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })



            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="label bg-accent" ng-click="grid.appScope.invoicesProviderDraft.loadItem(row.entity)">+</a>' +
                                 '</div>'
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.invoicesProviderDraft.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '44%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '39%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  
                    }
                ]
            };

            function loadItems(){
                itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }
            
         

            function loadItem(row){

                  
                 $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    referenceKey: row.brandCode,
                    price: row.netPurchaseValue,
                    netPurchaseValue: row.netPurchaseValue,
                    quantity: 1,
                    disscount: 0

                });

                 // $scope.warningItems = false    
            }

            function getNet() {
                
                var net = 0
                  var i = 0
                     for (i=0; i<$scope.invoiceItems.length; i++) {
                         var valueItems = $scope.invoiceItems[i] 
                         net += valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100; 

                      }

                      if ($scope.invoiceDetails.type == "Nacional"){

                        $scope.NET =  net
                        $scope.netoInvoice = net - (($scope.invoiceDetails.discount*net)/100) 
                        $scope.total = $scope.netoInvoice * 1.19
                        $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                        return net;

                        }else{

                            $scope.NET =  net
                            $scope.netoInvoice = net - (($scope.invoiceDetails.discount*net)/100) 
                            $scope.total = $scope.netoInvoice
                            $scope.VAT = 0
                            return net;
                        }
                             
            }

            function getNetTotal() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += valueItems.netPurchaseValue * valueItems.quantity; 

                  }
                  
                  $scope.netTotal =  net
                  return $scope.netTotal;
                      
            }


            function removeItems(index) {

                $scope.invoiceItems.splice(index, 1);
            }

            function setName(providers) {

                $scope.providerName = providers.providerName
                $scope.providerId = providers.providerId
            }

            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                $scope.warehouseId = warehouseId
            }

            function showPanel() {
                $scope.panelInvoice = true

            }

            function loadImport(){

                let model = {
                    invoiceId: $scope.invoiceId
                  }
          
                invoicesProviderServices.invoicesProviders.getInvoicesDetails(model).$promise.then((dataReturn) => {
                   $scope.dataInvoice = dataReturn.data
                   console.log('datos',$scope.dataInvoice)
                   $scope.currency = $scope.dataInvoice[0].currency
                   validateCurrency($scope.currency)

                   $scope.invoiceDetails  = { 
                      invoiceCode : $scope.dataInvoice[0].invoiceCode,
                      currency : $scope.dataInvoice[0].currency, 
                      originId : $scope.dataInvoice[0].originId,
                      type : $scope.dataInvoice[0].type, 
                      observation : $scope.dataInvoice[0].comment,
                      vatPrice : $scope.dataInvoice[0].vatPrice,
                      discount : $scope.dataInvoice[0].discount,
                      purchasePrice : $scope.dataInvoice[0].purchasePrice, 
                      mayorPrice : $scope.dataInvoice[0].mayorPrice
                    }

                   if ($scope.dataInvoice[0].type == "Internacional") {
                        $scope.internacionalInvoice = true
                        $scope.type = 2
                    }else{
                        $scope.internacionalInvoice = false
                        $scope.type = 1
                    }
                   $scope.valueCurrency = $scope.dataInvoice[0].currencyValue

                },(err) => {
                    console.log('No se ha podido conectar con el servicio getInvoicesDetails',err);
                })

                invoicesProviderServices.invoicesProviders.getInvoicesItems(model).$promise.then((dataReturn) => {
                      $scope.importItems = dataReturn.data
                      console.log('$scope.importItems',$scope.importItems)
                      for(var i in $scope.importItems){
                            $scope.invoiceItems.push({
                                itemId: $scope.importItems[i].itemId,
                                itemDescription: $scope.importItems[i].itemDescription,
                                referenceKey: $scope.importItems[i].brandCode,
                                price: $scope.importItems[i].price,
                                netPurchaseValue: $scope.importItems[i].netPurchaseValue,
                                disscount: $scope.importItems[i].discount,
                                quantity: $scope.importItems[i].quantityItems
                         })
                    }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get import items',err);
                    })
            }

            function invoiceSave(statusValue) {

                        let model = {
                            userId: $scope.userInfo.id,
                            invoiceCode: $scope.invoiceDetails.invoiceCode,
                            providerId : $scope.providerId,
                            date : $scope.CurrentDate,
                            originId : $scope.invoiceDetails.originId,
                            type: $scope.type,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.invoiceDetails.discount,
                            total : $scope.total,
                            netTotal : $scope.netoInvoice,
                            comment : $scope.invoiceDetails.observation,
                            currency : $scope.currency,
                            valueCurrency : $scope.valueCurrency,
                            invoiceId : $scope.invoiceId,
                            value: statusValue,
                            itemsInvoices : $scope.invoiceItems 
                        }    

                        console.log('modelo',model)

                        if (statusValue == 2) {

                          invoicesProviderServices.invoicesProviders.updateInvoiceProviderDraft(model).$promise.then((dataReturn) => {
                            ngNotify.set('Borrador factura guardado correctamente','warn')
                            // $state.go('app.invoicesProvider')
                          },(err) => {
                              ngNotify.set('Error al guardar borrador','error')
                          })

                        }else{

                          invoicesProviderServices.invoicesProviders.updateInvoice(model).$promise.then((dataReturn) => {
                            ngNotify.set('Factura creada correctamente','success')
                            $state.go('app.invoicesProvider')
                          },(err) => {
                              ngNotify.set('Error al guardar borrador','error')
                          })
                        }

                        
                    }
           
        }//FIN CONSTRUCTOR

        // Funciones
          
    }   
invoicesProviderDraftController.$inject = ['$scope','UserInfoConstant','$timeout','invoicesProviderServices','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','$modal','$stateParams','importsServices'];

