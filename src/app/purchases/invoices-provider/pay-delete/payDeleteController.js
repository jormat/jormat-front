/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> payDeleteController
* # As Controller --> payDelete														
*/


  export default class payDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,paymentsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.deletePayment = deletePayment
	    $scope.paymentsId = $scope.paymentsId
      $scope.invoiceId = $scope.invoiceId

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

	    function cancel() {
			$modalInstance.dismiss('chao');
		}

		function deletePayment() {
			let model = {
                    paymentId : $scope.paymentsId,
                    invoiceId : $scope.invoiceId,
                    userId : $scope.userInfo.id
                }

                paymentsServices.providersPayments.disabledProviderPayment(model).$promise.then((dataReturn) => {
                    ngNotify.set('Pago eliminado exitosamente','success');
                    $state.reload("app.paymentsProviders")
					           $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al eliminar pago','error')
                  })
		}
	    
    }

  }

  payDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','paymentsServices','$location','UserInfoConstant','ngNotify','$state'];
