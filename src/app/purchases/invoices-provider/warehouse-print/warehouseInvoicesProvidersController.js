/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> warehouseInvoicesProvidersController
* # As Controller --> warehouseInvoicesProviders														
*/


  export default class warehouseInvoicesProvidersController {

    constructor($scope, $filter,$rootScope, $http,invoicesProviderServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel 
        vm.print=print
        $scope.invoiceId = $stateParams.idInvoice
        $scope.originId = $stateParams.originId

        let params = {
                invoiceId: $scope.invoiceId,
                originId: $scope.originId

            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        invoicesProviderServices.invoicesProviders.getInvoicesDetails(params).$promise.then((dataReturn) => {
              $scope.invoice = dataReturn.data
                if ($scope.invoice[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.invoice[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                    }else{
                        $scope.currencySymbol = '$'
                    }

                }else{
                    $scope.internationalType = false
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

        invoicesProviderServices.invoicesProviders.getInvoicesItemsLocation(params).$promise.then((dataReturn) => {
              $scope.invoiceItems = dataReturn.data
              var subTotal = 0
              var i = 0
                 for (i=0; i<$scope.invoiceItems.length; i++) {
                     subTotal = subTotal + $scope.invoiceItems[i].total  
                  }
                  $scope.subTotality =  subTotal
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get getInvoicesItems',err);
              })

    	function cancel() {
            console.log('in');
                window.close();
                
		}

        function print() {
            console.log('on');
              window.print();
             

        }


    }

  }

  warehouseInvoicesProvidersController .$inject = ['$scope', '$filter','$rootScope', '$http','invoicesProviderServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];

