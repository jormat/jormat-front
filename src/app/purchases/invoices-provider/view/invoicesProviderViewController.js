/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> invoicesProviderViewController
* # As Controller --> invoicesProviderView														
*/


  export default class invoicesProviderViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,invoicesProviderServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.currencySymbol = '$'
          $scope.invoiceId = $scope.invoiceData.invoiceId
          $scope.providerName = $scope.invoiceData.providerName 
          $scope.invoiceCode = $scope.invoiceData.invoiceCode
      
        let params = {
            invoiceId: $scope.invoiceId
          }

        invoicesProviderServices.invoicesProviders.getInvoicesDetails(params).$promise.then((dataReturn) => {
              $scope.invoice = dataReturn.data
                if ($scope.invoice[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.invoice[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                    }else{
                        $scope.currencySymbol = '$'
                    }

                }else{
                    $scope.internationalType = false
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

        invoicesProviderServices.invoicesProviders.getInvoicesItems(params).$promise.then((dataReturn) => {
              $scope.invoiceItems = dataReturn.data
              var subTotal = 0
                  var i = 0
                     for (i=0; i<$scope.invoiceItems.length; i++) {
                         subTotal = subTotal + $scope.invoiceItems[i].total  
                      }
                      
                      $scope.subTotality =  subTotal
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get getInvoicesItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

		


    }

  }

  invoicesProviderViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','invoicesProviderServices','$location','UserInfoConstant','ngNotify','$state'];
