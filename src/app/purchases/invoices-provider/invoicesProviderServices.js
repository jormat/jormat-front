/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> invoicesProviderServices
* file for call at services for providers
*/

class invoicesProviderServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var invoices = $resource(SERVICE_URL_CONSTANT.jormat + '/invoices/:route',{},{
      
        getInvoices: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getInvoicesDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createInvoice: {
            method:'POST',
            params : {
                route: ''
            }
        },
        updateInvoice: {
            method:'PUT',
            params : {
                route: ''
            }
        },
        disabledInvoice: {
            method:'DELETE',
            params : {
                route: ''
            }
        }
    })

    var invoicesProviders = $resource(SERVICE_URL_CONSTANT.jormat + '/invoices/providers/:route',{},{
      
        
        getInvoicesProviders: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        
        getInvoicesDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },

        createInvoiceProvider: {
            method:'POST',
            params : {
               route: ''
            }
        },
        
        updateInvoice: {
            method:'PUT',
            params : {
                route: ''
            }
        },

        disabledInvoice: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        getInvoicesItems: {
            method:'GET',
            params : {
               route: 'items'
            }
        },
        createInvoiceProviderDraft: {
            method:'POST',
            params : {
               route: 'draft'
            }
        },
        updateInvoiceProviderDraft: {
            method:'PUT',
            params : {
               route: 'draft'
            }
        },
        getInvoicesItemsLocation: {
            method:'GET',
            params : {
               route: 'items-location'
            }
        }
    })

    return { 

            invoices : invoices,
            invoicesProviders : invoicesProviders
           }
  }
}

  invoicesProviderServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.invoicesProviderServices', [])
  .service('invoicesProviderServices', invoicesProviderServices)
  .name;


  