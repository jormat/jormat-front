
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> invoicesProviderPrintController
* # As Controller --> 
														
*/


  export default class invoicesProviderPrintController {

    constructor($scope, $filter,$rootScope, $http,invoicesProviderServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.invoiceWarehousePrint = invoiceWarehousePrint
        vm.transformToInvoice = transformToInvoice
        $scope.invoiceId = $stateParams.idInvoice

        let params = {
                invoiceId: $scope.invoiceId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        invoicesProviderServices.invoicesProviders.getInvoicesDetails(params).$promise.then((dataReturn) => {
              $scope.invoice = dataReturn.data
                if ($scope.invoice[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.invoice[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                    }else{
                        $scope.currencySymbol = '$'
                    }

                }else{
                    $scope.internationalType = false
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

        invoicesProviderServices.invoicesProviders.getInvoicesItems(params).$promise.then((dataReturn) => {
              $scope.invoiceItems = dataReturn.data
              var subTotal = 0
              var i = 0
                 for (i=0; i<$scope.invoiceItems.length; i++) {
                     subTotal = subTotal + $scope.invoiceItems[i].total  
                  }
                  $scope.subTotality =  subTotal
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get getInvoicesItems',err);
              })

    	function cancel() {

		 $state.go("app.invoicesProvider")
		}

        function transformToInvoice(row){

          $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.quotationId,discount:$scope.quotations[0].discount,type:'cotizacion'});

        }

        function print() {
              window.print();

        }

        function invoiceWarehousePrint(){

            console.log('imprimir print ');
            var url = $state.href("app.warehouseInvoicesProvider",{
                idInvoice: $scope.invoiceId,
                originId: $scope.invoice[0].originId
            });

        window.open(url,'_blank',"width=600,height=700");

          // $state.go("app.warehouseQuotationPrint", { idQuotation: $scope.quotationId,warehouse:$scope.originId});

        }


    }

  }

  invoicesProviderPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','invoicesProviderServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
