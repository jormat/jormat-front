
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> providersListController
* # As Controller --> providersList														
*/

export default class providersListController{

        constructor($scope,UserInfoConstant,$timeout,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this
            vm.searchData=searchData
            vm.deleteProvider= deleteProvider
            vm.viewProvider= viewProvider
            vm.updateProvider = updateProvider
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')

            $scope.providersGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_proveedores.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'providerId',  
                      width: '7%' 
                    },
                    { 
                      name:'Nombre Proveedor',
                      field: 'providerName',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase"><strong> {{row.entity.providerName}}</strong> </p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Rut',
                      field: 'rut',
                      width: '11%' 
                    },
                    { 
                      name: 'Telefono', 
                      field: 'phone', 
                      width: '12%'
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type',
                      // cellFilter: 'date', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 9px;" >{{row.entity.type}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Email', 
                      field: 'email',  
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.email}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name: '',
                      enableFiltering: false,
                      width: '5%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
         		providersServices.providers.getProviders().$promise.then((dataReturn) => {
         		  $scope.providersGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            function searchData() {
              providersServices.providers.getProviders().$promise
	              .then(function(data){
	                  $scope.data = data.data;
	                  $scope.providersGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
	              });
            }

            function deleteProvider(row){
            	$scope.providerData = row;
	            var modalInstance  = $modal.open({
                    template: require('../delete/providers-delete.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'providersDeleteController',
                    controllerAs: 'providersDelete'
            })
        }

        function viewProvider(row){
            $scope.providerData = row;
            var modalInstance  = $modal.open({
                    template: require('../view/providers-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'providersViewController',
                    controllerAs: 'providersView',
            })
        }

        function updateProvider(row){
            $state.go("app.providersUpdate", { idProvider: row.providerId});
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

providersListController.$inject = ['$scope','UserInfoConstant','$timeout','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

