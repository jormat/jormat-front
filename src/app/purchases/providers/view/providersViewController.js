/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> providersViewController
* # As Controller --> providersView														
*/


  export default class providersViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,providersServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.edit = edit;
	    $scope.providerId = $scope.providerData.providerId
	    $scope.providerName = $scope.providerData.providerName

	    let params = {
            providerId: $scope.providerId
          }

        providersServices.providers.getProvidersDetails(params).$promise.then((dataReturn) => {
         		  $scope.provider = dataReturn.data
         		  if($scope.provider[0].active == 1){
         		  	 	$scope.statusName= "Activo"
         		  }else{
         		    	$scope.statusName= "Inactivo"
         		  }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function edit() {
			$state.go("app.providersUpdate");
		}

		


    }

  }

  providersViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','providersServices','$location','UserInfoConstant','ngNotify','$state'];
