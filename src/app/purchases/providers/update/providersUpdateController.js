
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> providersUpdateController
* # As Controller --> providersUpdate														
*/

export default class providersUpdateController{

        constructor($scope,UserInfoConstant,$timeout,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams){
            var vm = this;
            vm.updateProvider = updateProvider

            let params = {
                providerId: $stateParams.idProvider
            }
            
          
            providersServices.providers.getProvidersDetails(params).$promise.then((dataReturn) => {
              $scope.provider = dataReturn.data;

              $scope.providerDetails  = { 
                name : $scope.provider[0].providerName,
                email : $scope.provider[0].email,
                rut : $scope.provider[0].rut,
                cityName : $scope.provider[0].cityName,
                address : $scope.provider[0].address,
                phone : $scope.provider[0].phone,
                mobile : $scope.provider[0].mobile,
                web : $scope.provider[0].web,
                commercialBusiness : $scope.provider[0].commercialBusiness,
                bankAccount : $scope.provider[0].bankAccount,
                type : $scope.provider[0].type
              }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
            
          
         	function updateProvider() {

                let model = {

                  providerId: $stateParams.idProvider,
                  providerName: $scope.providerDetails.name,
                  phone: $scope.providerDetails.phone,
                  mobile: $scope.providerDetails.mobile,
                  rut: $scope.providerDetails.rut,
                  email: $scope.providerDetails.email,
                  address: $scope.providerDetails.address,
                  cityName: $scope.providerDetails.cityName,
                  web: $scope.providerDetails.web,
                  bankAccount: $scope.providerDetails.bankAccount,
                  commercialBusiness: $scope.providerDetails.commercialBusiness,
                  type: $scope.providerDetails.type
                }

                console.log(model)

                providersServices.providers.updateProvider(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha actualizado el proveedor correctamente','success')
                    $state.go('app.providers')
                  },(err) => {
                      ngNotify.set('Error al actualizar proveedor','error')
                  })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

providersUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams'];

