/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> providersServices
* file for call at services for providers
*/

class providersServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var providers = $resource(SERVICE_URL_CONSTANT.jormat + '/providers/:route',{},{
      
        getProviders: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getProvidersDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createProvider: {
            method:'POST',
            params : {
                route: ''
            }
        },
        updateProvider: {
            method:'PUT',
            params : {
                route: ''
            }
        },
        disabledProvider: {
            method:'DELETE',
            params : {
                route: ''
            }
        }
    })

    return { providers : providers
             
           }
  }
}

  providersServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.providersServices', [])
  .service('providersServices', providersServices)
  .name;