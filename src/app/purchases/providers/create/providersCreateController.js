
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> providersCreateController
* # As Controller --> providersCreate														
*/

export default class providersCreateController{

        constructor($scope,UserInfoConstant,$timeout,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter){
            var vm = this;
            vm.createProvider = createProvider            
            
         	function createProvider() {

                let model = {
                    providerName: $scope.providerName,
                    rut: $scope.rut,
                    phone: $scope.phone,
                    mobile: $scope.mobile,
                    email: $scope.email,
                    address: $scope.address,
                    cityName: $scope.city,
                    web: $scope.web,
                    bankAccount: $scope.bank,
                    commercialBusiness: $scope.bussiness,
                    type: $scope.type
                }
                console.log(model)

                providersServices.providers.createProvider(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado el proveedor correctamente','success')
                    $state.go('app.providers')
                  },(err) => {
                      ngNotify.set('Error al crear proveedor','error')
                })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

providersCreateController.$inject = ['$scope','UserInfoConstant','$timeout','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter'];

