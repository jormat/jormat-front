/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> providersDeleteController
* # As Controller --> providersDelete														
*/


  export default class providersDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,providersServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.deleteProvider = deleteProvider;
	    $scope.providerId = $scope.providerData.providerId
	    $scope.rut = $scope.providerData.rut
	    $scope.providerName = $scope.providerData.providerName

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteProvider() {
			let model = {
                    providerId : $scope.providerId,
                    userId : 1
                }

                providersServices.providers.disabledProvider(model).$promise.then((dataReturn) => {

                    ngNotify.set('Proveedor dado de baja exitosamente','success');
					   $modalInstance.dismiss('chao');
                       $state.reload("app.providers");
                  },(err) => {
                      ngNotify.set('Error al eliminar Proveedor','error')
                  })
		}


    }

  }

  providersDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','providersServices','$location','UserInfoConstant','ngNotify','$state'];
