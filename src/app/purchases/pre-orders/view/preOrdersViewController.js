/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> preOrdersViewController
* # As Controller --> preOrdersView														
*/


  export default class preOrdersViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,preOrdersServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.currencySymbol = '$'
        $scope.orderId = $scope.invoiceData.orderId
        $scope.providerName = $scope.invoiceData.providerName 
      
        let params = {
            orderId: $scope.orderId
          }

        preOrdersServices.preOrders.getPreOrderDetails(params).$promise.then((dataReturn) => {
              $scope.invoice = dataReturn.data
                if ($scope.invoice[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.invoice[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                    }else{
                        $scope.currencySymbol = '$'
                    }

                }else{
                    $scope.internationalType = false
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

        preOrdersServices.preOrders.getPreOrdersItems(params).$promise.then((dataReturn) => {
              $scope.invoiceItems = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get getInvoicesItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

		


    }

  }

  preOrdersViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','preOrdersServices','$location','UserInfoConstant','ngNotify','$state'];
