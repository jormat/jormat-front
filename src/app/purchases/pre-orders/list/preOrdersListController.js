
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> preOrdersListController
* # As Controller --> preOrdersList														
*/

export default class preOrdersListController{

        constructor($scope,UserInfoConstant,$timeout,preOrdersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.viewOrders = viewOrders
            vm.updateInvoice= updateInvoice
            vm.printOrders = printOrders
            vm.payments = payments
            vm.searchData=searchData
            vm.loadOrders= loadOrders
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');
            loadOrders()

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.invoicesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_facturas_proveedor.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'orderId',  
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.preOrdersList.viewOrders(row.entity)">{{row.entity.orderId}}</a>' +
                                 '</div>'    
                    },
                    { 
                      name:'Proveedor',
                      field: 'providerName',
                      width: '36%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 13px;">{{row.entity.providerName}}</p>' +
                                 '</div>'        
                    },
                    // { 
                    //   name:'Total $/€',
                    //   field: 'total',
                    //   width: '8%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents">'+
                    //              '<p><strong>{{row.entity.total}}</strong></p>' +
                    //              '</div>' 
                    // },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" label bg-warning" > {{row.entity.origin }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" label bg-info" style="font-size: 10px;" > {{row.entity.type }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Usuario', 
                      field: 'userCreation',
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" label bg-secundary" style="font-size: 10px;"> {{row.entity.userCreation }}</p>' +
                                 '</div>'
                    },
                    // { 
                    //   name: 'Estado', 
                    //   field: 'statusName', 
                    //   width: '9%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p style="font-size: 11px;" class="{{(row.entity.statusName == \'Pagado\')?\'badge bg-warning\':\'badge bg-danger\'}}">{{row.entity.statusName}}</p>' +
                    //              '</div>'
                    // },
                    
                    { 
                      name: '',
                      width: '10%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadOrders(){

            preOrdersServices.preOrders.getPreOrders().$promise.then((dataReturn) => {
              $scope.invoicesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

        
            function searchData() {
              preOrdersServices.preOrders.getPreOrders().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.invoicesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewOrders(row){
	            $scope.invoiceData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/pre-orders-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'preOrdersViewController',
	                    controllerAs: 'preOrdersView',
	                    size: 'lg'
	            })
	       }

            function printOrders(row){
             $state.go("app.preOrdersPrint", { orderId: row.orderId});
            }

            function payments(row){
              console.log(row)

             $state.go("app.paymentsProviders", { 
                idInvoice: row.invoiceId,
                type: row.type,                
                providerName: row.providerName,
                total: row.total,
                date: row.date
              });
            }

            function updateInvoice(row){
              $state.go("app.invoicesProviderUpdate", { idInvoice: row.invoiceId});
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

preOrdersListController.$inject = ['$scope','UserInfoConstant','$timeout','preOrdersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

