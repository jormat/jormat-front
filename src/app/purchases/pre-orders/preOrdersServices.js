/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> preOrdersServices
* file for call at services for pre Orders Services
*/

class preOrdersServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var preOrders = $resource(SERVICE_URL_CONSTANT.jormat + '/orders/:route',{},{
      
        getPreOrders: {
            method:'GET',
            params : {
               route: 'providers'
            }
        },
        getPreOrderDetails: {
            method:'GET',
            params : {
               route: 'providers-details'
            }
        },
        getPreOrdersItems: {
            method:'GET',
            params : {
               route: 'providers-items'
            }
        },
        createPreOrders: {
            method:'POST',
            params : {
                route: 'providers'
            }
        }
    })



    return { 

            preOrders : preOrders
           }
  }
}

  preOrdersServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.preOrdersServices', [])
  .service('preOrdersServices', preOrdersServices)
  .name;


  