
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> preOrdersCreateController
* # As Controller --> preOrdersCreate														
*/

export default class preOrdersCreateController{

        constructor($scope,UserInfoConstant,$timeout,wishlistServices,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,$modal,preOrdersServices,$stateParams){
            var vm = this;
            vm.ordersCreate = ordersCreate
            vm.validateType = validateType
            vm.validateCurrency = validateCurrency 
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.loadItems = loadItems
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.ItemsCreate = ItemsCreate
            $scope.internacionalInvoice = false
            $scope.parameters= $stateParams.parameters
            $scope.nacionalInvoice = true
            $scope.parseInt = parseInt
            $scope.discount = 0
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.priceVATest = 56000
            $scope.editables = {
      			    disccount: '0',
      			    quantity: '1'
      			}

            $scope.invoiceItems = []

            function validateCurrency(currency){
                if (currency == 'Euro') {
                    $scope.currencySymbol = '€'
                }else{
                    $scope.currencySymbol = '$'
                }
            }

            loadItems()

            if ($scope.parameters == undefined) {
                console.log('no aplica',$scope.parameters)

            }else{

                $scope.array = JSON.parse($scope.parameters)
                console.log('parameters',$scope.parameters,$scope.array);

                for(var i in $scope.array){
                        $scope.invoiceItems.push({
                            wishListId: $scope.array[i].wishListId,
                            brandCode: $scope.array[i].brandCode,
                            itemDescription: $scope.array[i].itemDescription,
                            brand: $scope.array[i].brand,
                            jormatId: $scope.array[i].jormatId,
                            quantity: $scope.array[i].quantities
                     })
                        $scope.warningItems = false
                }
            }

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    let model = {
                            userId : $scope.userInfo.id
                        }

                    warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                      $scope.warehouses = dataReturn.data;
                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                    })
                }
            })
            
            //

            providersServices.providers.getProviders().$promise.then((dataReturn) => {
                  $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })



            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                   '<button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.preOrdersCreate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                   '</div>'
                    },

                    { 
                      name:'Cod. Marca',
                      field: 'brandCode',
                      enableFiltering: true,
                      width: '19%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning" style="font-size: 11px;"><strong> {{row.entity.brandCode}}</strong></p>' +
                                 '</div>'
                    },
                   
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '32%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 10px;" tooltip-placement="right" tooltip=" Cantidad {{row.entity.quantities}}" ><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },

                     { 
                      name:'Referencias',
                      field: 'references', 
                      enableFiltering: true,
                      width: '23%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 10px;"><em>{{row.entity.references}}</em></a>' +
                                 '</div>'
                    },
                     { 
                      name:'id',
                      field: 'jormatId', 
                      enableFiltering: true,
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.preOrdersCreate.viewItem(row.entity)"><strong>{{row.entity.jormatId}}</strong></a>' +
                                 '</div>' 
                    },
                  
                    { 
                      name:'Marca',
                      field: 'brand',
                      enableFiltering: true,
                      width: '18%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;"><strong> {{row.entity.brand}}</strong></p>' +
                                 '</div>'  
                    }
                ]
            };

            function loadItems(){
                wishlistServices.wishlist.getWishlist().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }
            
            function validateType(type) {
              // $scope.typeInvoice = type

              // $scope.invoiceItems = []
              //   if (type == 2) {
              //       $scope.internacionalInvoice = true
              //       $scope.nacionalInvoice = false
              //   }else{
              //       $scope.internacionalInvoice = false
              //       $scope.nacionalInvoice = true
              //   }
            }


            

            function loadItem(row){
                 $scope.invoiceItems.push({
                    wishListId: row.wishListId,
                    brandCode: row.brandCode,
                    itemDescription: row.itemDescription,
                    brand: row.brand,
                    jormatId: row.jormatId,
                    quantity: row.quantities

                });

                 $scope.warningItems = false    
            }


            function removeItems(index) {

                $scope.invoiceItems.splice(index, 1);
            }

            function setName(providers) {

                $scope.providerName = providers.providerName
                $scope.providerId = providers.providerId
            }

            function viewItem(row){
                $scope.itemsData = {
                    itemId:row.jormatId,
                    itemDescription:row.itemDescription
                }
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                $scope.warehouseId = warehouseId
            }

            function showPanel() {
                // $scope.panelInvoice = true

            }

            function ItemsCreate() {
                var url = $state.href("app.itemsCreate");

                window.open(url,'_blank',"width=600,height=700");

            }

            function ordersCreate() {

                        let model = {
                            userId: $scope.userInfo.id,
                            providerId : $scope.providerId,
                            originId : $scope.warehouseId,
                            type: $scope.selected,
                            comment : $scope.observation,
                            itemsInvoices : $scope.invoiceItems 
                        }      

                        console.log('modelo',model)

                        preOrdersServices.preOrders.createPreOrders(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha creado la pre-orden correctamente','success')
                            $state.go('app.preOrders')
                          },(err) => {
                              ngNotify.set('Error al crear documento','error')
                          })
                    }
           
        }//FIN CONSTRUCTOR

        // Funciones
        

    }   

preOrdersCreateController.$inject = ['$scope','UserInfoConstant','$timeout','wishlistServices','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','$modal','preOrdersServices','$stateParams'];

