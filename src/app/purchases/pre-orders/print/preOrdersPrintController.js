
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> preOrdersPrintController
* # As Controller --> preOrdersPrint
														
*/


  export default class preOrdersPrintController {

    constructor($scope, $filter,$rootScope, $http,preOrdersServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.transformToInvoice = transformToInvoice
        $scope.orderId = $stateParams.orderId

        let params = {
                orderId: $scope.orderId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        preOrdersServices.preOrders.getPreOrderDetails(params).$promise.then((dataReturn) => {
              $scope.invoice = dataReturn.data
                if ($scope.invoice[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.invoice[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                    }else{
                        $scope.currencySymbol = '$'
                    }

                }else{
                    $scope.internationalType = false
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

        preOrdersServices.preOrders.getPreOrdersItems(params).$promise.then((dataReturn) => {
              $scope.invoiceItems = dataReturn.data
              console.log('taht',$scope.invoiceItems)
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get item pre orders',err);
              })

    	function cancel() {

		 $state.go("app.invoicesProvider")
		}

        function transformToInvoice(row){

          $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.quotationId,discount:$scope.quotations[0].discount,type:'cotizacion'});

        }

        function print() {
              window.print();

        }


    }

  }

  preOrdersPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','preOrdersServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
