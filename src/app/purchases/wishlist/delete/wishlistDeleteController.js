/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> wishlistDeleteController
* # As Controller --> wishlistDelete														
*/


  export default class wishlistDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,wishlistServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.deleteItem = deleteItem;
	    $scope.itemId = $scope.itemsData.jormatId
	    $scope.wishListId = $scope.itemsData.wishListId
	    $scope.itemName = $scope.itemsData.itemDescription
	    vm.index=$scope.index;
	    vm.grid=$scope.data;

	    vm.$translate = $filter('translate')
	        $rootScope.$on('$translateChangeSuccess', function () {
	   
	        })

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
        //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteItem() {

			let model = {
                    wishListId : $scope.wishListId,
                    userId : $scope.userInfo.id
                }

                wishlistServices.wishlist.wishlistDisabled(model).$promise.then((dataReturn) => {

                    ngNotify.set('Item eliminado exitosamente','success');
					$state.reload("app.wishlist");
					$modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al eliminar Item','error')
                  })
			
		}

    }

  }

  wishlistDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','wishlistServices','$location','UserInfoConstant','ngNotify','$state'];
