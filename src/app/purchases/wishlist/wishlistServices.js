/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> wishlistServices
* file for call at services to wishlist
*/

class wishlistServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var wishlist = $resource(SERVICE_URL_CONSTANT.jormat + '/items/:route',{},{

      getWishlist: {
        method:'GET',
        params : {
          route: 'wish-list'
          }
        },

      getWishlistItems: {
        method:'GET',
        params : {
          route: 'wishlist-items'
          }
        },

      wishlistAddItems: {
        method:'POST',
        params : {
          route: 'wish-list'
          }
        },

      referencesAddItems: {
        method:'POST',
        params : {
          route: 'wishlist-references'
          }
        },

      wishlistDisabled: {
        method:'DELETE',
        params : {
          route: 'wish-list'
          }
        },

        wishlistUpdate: {
        method:'PUT',
        params : {
          route: 'wish-list'
          }
        },

        wishlistUpdateId: {
        method:'PUT',
        params : {
          route: 'wishlist-id'
          }
        }
    })

    return { wishlist : wishlist
             
           }
  }
}

  wishlistServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.wishlistServices', [])
  .service('wishlistServices', wishlistServices)
  .name;