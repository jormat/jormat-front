/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> wishlistCreateController
* # As Controller --> wishlistCreate														
*/


  export default class wishlistCreateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,wishlistServices,$location,UserInfoConstant,ngNotify,$state,providersServices,itemsServices) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.searchItems = searchItems;
        vm.itemAdd = itemAdd
        vm.setName = setName
        vm.setNameProviders=setNameProviders

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        providersServices.providers.getProviders().$promise.then((dataReturn) => {
              $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })


        function setName(references) {
                console.log('please', references)
                $scope.itemName = references.itemName
                $scope.itemId = references.itemId
                $scope.brand = references.brand
                $scope.brandCode = references.brandCode

                let params = {
                    itemId: $scope.itemId
                  }

                itemsServices.items.itemsList(params).$promise.then((dataReturn) => {
                $scope.item = dataReturn.data;
    
                $scope.itemDetails  = { 
                    name : $scope.item[0].itemName,
                    references:   $scope.item[0].references
                  }

                  $scope.referencesArray=[]
                  if($scope.item[0].references.length>0){
                      for(var i in $scope.item[0].references){
                        $scope.referencesArray.push({title:$scope.item[0].references[i],done:false})
                      }
                    
                    }
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
            }

            function setNameProviders(providers) {

                $scope.providerName = providers.providerName
                $scope.providerId = providers.providerId
            }


        function searchItems (value) {

            let model = {
                references: value
            }

            wishlistServices.wishlist.getWishlistItems(model).$promise.then((dataReturn) => {
                  $scope.references = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            return false;
          }

	    
	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        
        function itemAdd() {
            console.log('new items add');
            let model = {
                            userId: $scope.userInfo.id,
                            itemId : $scope.itemId ,
                            itemName : $scope.itemName,
                            brand : $scope.brand,
                            brandCode : $scope.brandCode,
                            providerId : $scope.providerId, 
                            type : $scope.type, 
                            comments : $scope.comments,
                            quantities : $scope.quantities,
                            references : $scope.referencesArray 
                        }      

                        console.log('modelo',model)   

                        wishlistServices.wishlist.wishlistAddItems(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha ingresado el Repuesto correctamente','success')
                            $scope.result = dataReturn.data

                            for(var i in model.references){
                                $scope.reference = model.references[i].title

                                let params = {
                                    wishlistId : $scope.result,
                                    reference : $scope.reference
                                } 
                            
                            wishlistServices.wishlist.referencesAddItems(params).$promise.then((dataReturn) => {
                                console.log('Se ha asociado la referencia correctamente')
                                $scope.result = dataReturn.data

                                 $state.reload('app.wishlist')
                                 $modalInstance.dismiss('chao');

                          },(err) => {
                              ngNotify.set('Error al asociar referencia','error')
                          })
                        }

                           

                          },(err) => {
                              ngNotify.set('Error al registrar item','error')
                          })


        }

        

		$scope.referencesArray = [];

        $scope.referenceAdd = function(reference) {
            
            $scope.referencesArray.push({'title': $scope.reference, 'done':false})
            $scope.reference = ''
            // $scope.saveButton  = false
        }

        $scope.referenceDelete = function(index) {  
            $scope.referencesArray.splice(index, 1);
        }

    }

  }

  wishlistCreateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','wishlistServices','$location','UserInfoConstant','ngNotify','$state','providersServices','itemsServices'];
