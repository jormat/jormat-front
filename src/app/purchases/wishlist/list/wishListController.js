
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> wishListController
* # As Controller --> wishList														
*/

export default class wishListController{

        constructor($scope,UserInfoConstant,$timeout,wishlistServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService,uiGridSelectionConstants){
            var vm = this
            $scope.selectedwishlist = []
            vm.searchData=searchData
            vm.wishListCreate= wishListCreate
            vm.viewItem=viewItem
            vm.wishlistUpdate = wishlistUpdate
            vm.loadwishlist = loadwishlist
            vm.disabledWishListItem = disabledWishListItem
            vm.addItemsWishList = addItemsWishList
            vm.itemCreate = itemCreate
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
              
            //

            loadwishlist()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.wishlistGrid = {
                enableFiltering: true,
                enableSelectAll: true,
                enableGridMenu: true,
                showColumnFooter: true,
                enableRowSelection: true,
                enableCellEditOnFocus : true,
                exporterCsvFilename: 'Listado_de_deseos.csv',
                multiSelect : true,
                enableRowSelection: true,
                selectionRowHeaderWidth: 35,
                removeSelectedAfterFilterd : true,
                // isRowSelectable: function(row) {
                //   if(row.entity.statusName == "PEN" || row.entity.statusName == "DEP"){
                //           return true;
                //         } else {
                //           return false;
                //         }
                // },
                onRegisterApi: function(gridApi){
                  $scope.gridApi = gridApi;

                  gridApi.selection.on.rowSelectionChanged($scope,function(row,$index){
                    $scope.selectedwishlist = gridApi.selection.getSelectedRows()
                    // $scope.selectedwishlist = gridApi.selection.selectAllRows()  
                  });

                  gridApi.selection.on.rowSelectionChangedBatch($scope, function() {

                    // check for ui-grid version to revert opposite value
                    var isAllSelected = gridApi.selection.getSelectAllState();
                    console.log('rray',isAllSelected)

                    if (isAllSelected) { 

                        // unselect all rows as select all button selects all rows by default
                        gridApi.selection.clearSelectedRows();

                        /* Will fetch only grid rows visible on page. It will fetch the correct 
                           rows even if you have applied sorting, pagination and filters. */
                        var gridRows = this.grid.getVisibleRows();


                        // function to set Selected to only visible rows
                        angular.forEach(gridRows, function(gridRow) { 
                            gridRow.setSelected(true);
                        });
                    }

                    if (isAllSelected == false) { $scope.selectedwishlist = gridApi.selection.getSelectedRows();}
                      else{$scope.selectedwishlist = gridApi.selection.clearSelectedRows(); $scope.selectedwishlist = []}

                    });



                },
                columnDefs: [ 
                    { 
                      name:'id',
                      field: 'wishListId',  
                      width: '6%',
                      enableCellEdit: false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a style="font-size: 10px;">{{row.entity.wishListId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cod. Marca',
                      field: 'brandCode',
                      width: '9%',
                      enableCellEdit: true,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p tooltip-placement="right" tooltip="Tipo: {{row.entity.type}}" class="uppercase label bg-warning" style="font-size: 10px;">{{row.entity.brandCode}}</p>' +
                                 '</div>' 
                    },
                    
                    { 
                      name: 'Proveedor', 
                      field: 'providerName', 
                      width: '15%',
                      allowCellFocus : false,
                      cellTemplate:'<div class="ui-grid-cell-contents uppercase">'+
                                 '<p style="font-size: 10px;">{{row.entity.providerName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Cod. Int.',
                      field: 'jormatId',
                      width: '7%',
                      enableCellEdit: false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.wishList.viewItem(row.entity)" class="label bg-secundary uppercase" style="font-size: 10px;"><strong>{{row.entity.jormatId}}</strong></a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      width: '21%',
                      enableCellEdit: true,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 10px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>'  
                    },
                     
                   
                    { 
                      name:'Referencias',
                      field: 'references',
                      width: '15%',
                      enableCellEdit: false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 9px;"><strong>{{row.entity.references}}</strong></p>' +
                                 '</div>'  
                    },
                   
                    
                    
                    { 
                      name: 'Cant.', 
                      field: 'quantities', 
                      width: '5%',
                      enableCellEdit: true,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p tooltip-placement="right" tooltip="Detalle: {{row.entity.comments}} by {{row.entity.userCreate}} " class="uppercase badge bg-danger" style="font-size: 10px;">{{row.entity.quantities}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Marca',
                      field: 'brand',
                      width: '10%',
                      enableCellEdit: true,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 10px;"><strong>{{row.entity.brand}}</strong></p>' +
                                 '</div>' 
                    },
                     { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '7%',
                      enableCellEdit: false,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p style="font-size: 10px;"> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: '',
                      width: '2%', 
                      field: 'href',
                      enableCellEdit: false,
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          function loadwishlist(){
            wishlistServices.wishlist.getWishlist().$promise.then((dataReturn) => {
                  $scope.wishlistGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

          }

          function itemCreate(row) {
            // $state.go("app.itemsClone", { data: row,type:'wishlist'});

            $scope.wishListData = JSON.stringify(row);
            // console.log(JSON.stringify($scope.wishListData));

            $state.go("app.itemsClone", { parameters: $scope.wishListData,type:'wishlist'});
        }

          $scope.selectAll = function() {
            $scope.selectedwishlist = $scope.gridApi.selection.selectAllRows();

        };
         	

        function searchData() {
              wishlistServices.wishlist.getWishlist().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.wishlistGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

        function wishListCreate(){
                var modalInstance  = $modal.open({
                        template: require('../create/wishlist-create.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'wishlistCreateController',
                        controllerAs: 'wishlistCreate',
                        size: 'lg',
                        backdrop: 'static', keyboard: false
                })
        }

        function wishlistUpdate(row){
          $scope.wishlistData = row;
          console.log('row',row)

          let model = {
                    wishListId: row.wishListId,
                    userId : $scope.userInfo.id,
                    brandCode: row.brandCode,
                    brand: row.brand,
                    itemDescription: row.itemDescription,
                    quantities: row.quantities
                }

                console.log(model)

                wishlistServices.wishlist.wishlistUpdate(model).$promise.then((dataReturn) => {
                    ngNotify.set('Item actualizado correctamente','success')
                    $state.reload("app.wishlist");
                  },(err) => {
                      ngNotify.set('Error al actualizar item','error')
                })

        }

        function disabledWishListItem(row){
            $scope.itemsData = row;
            var modalInstance  = $modal.open({
                    template: require('../delete/wishlist-delete.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'wishlistDeleteController',
                    controllerAs: 'wishlistDelete'
            })
         }

         function viewItem(row){
                $scope.itemsData = {
                    itemId:row.jormatId,
                    itemDescription:row.itemDescription
                }
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

         function addItemsWishList(){
            $scope.wishListData = JSON.stringify($scope.selectedwishlist);
            // console.log(JSON.stringify($scope.wishListData));

            $state.go("app.preOrdersCreate", { parameters: $scope.wishListData});
            // $state.go("app.preOrdersCreate", { parameters: ["1", "2", "3"]});
            
            // var modalInstance  = $modal.open({
            //             template: require('../actions/add-items.html'),
            //             animation: true,
            //             scope: $scope,
            //             controller: 'addItemsController',
            //             controllerAs: 'addItems'
            //     }) 
        }
            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

wishListController.$inject = ['$scope','UserInfoConstant','$timeout','wishlistServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService','uiGridSelectionConstants'];

