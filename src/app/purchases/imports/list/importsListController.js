
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> importsListController
* # As Controller --> importsList														
*/

export default class importsListController{

        constructor($scope,UserInfoConstant,$timeout,importsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.viewImports = viewImports
            vm.deleteInvoice = deleteInvoice
            vm.importPrint = importPrint
            vm.searchData = searchData
            vm.viewItems = viewItems
            vm.importDraft = importDraft
            vm.transitItemsView = transitItemsView
            vm.loadDocuments= loadDocuments
            vm.transformToInvoice = transformToInvoice
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');
            loadDocuments()

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.importsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_documentos_importacion.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'importId',  
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.importsList.viewImports(row.entity)">{{row.entity.importId}}</a>' +
                                 '</div>'    
                    },
                    { 
                      name:'Proveedor',
                      field: 'providerName',
                      width: '35%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.providerName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.total | dolares}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '10%'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date }}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Moneda', 
                      field: 'currency',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" label bg-secundary" > {{row.entity.currency }}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="badge bg-{{row.entity.style}}" style="font-size: 11px;"  > {{row.entity.statusName }}</p>' +
                                 '</div>'
                    },
                    // { 
                    //   name: 'Usuario', 
                    //   field: 'userCreation', 
                    //   width: '12%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class=" badge bg-warning" style="font-size: 11px;">{{row.entity.userCreation}}</p>' +
                    //              '</div>'   
                    // },
                    
                    { 
                      name: '',
                      width: '7%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadDocuments(){

              importsServices.imports.getImports().$promise.then((dataReturn) => {
              $scope.importsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            function importDraft(row){

              $state.go("app.importDraft", { idImport: row.importId});

            }

        
            function searchData() {
              importsServices.imports.getImports().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.importsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewItems(){

                var modalInstance  = $modal.open({
                        template: require('../items-history/items-history.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsHistoryController',
                        controllerAs: 'itemsHistory',
                        size: 'lg'
                })
           }


            function transitItemsView(){

                var modalInstance  = $modal.open({
                        template: require('../items-transit/items-transit.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsTransitController',
                        controllerAs: 'itemsTransit',
                        size: 'lg'
                })
            }

            function viewImports(row){
	            $scope.invoiceData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/imports-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'importsViewController',
	                    controllerAs: 'importsView',
	                    size: 'lg'
	            })
	       }

            function importPrint(row){
             $state.go("app.importsPrint", { importId: row.importId});
            }

            function transformToInvoice(row){
              $state.go("app.updateInvoiceProvider", { importId: row.importId});

            }

            function deleteInvoice(row){
              $scope.invoiceData = row;
              var modalInstance  = $modal.open({
                      template: require('../delete/invoices-provider-delete.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'invoicesProviderDeleteController',
                      controllerAs: 'invoicesProviderDelete'
              })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

importsListController.$inject = ['$scope','UserInfoConstant','$timeout','importsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

