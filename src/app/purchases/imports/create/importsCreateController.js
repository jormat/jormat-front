
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> importsCreateController
* # As Controller --> importsCreate														
*/

export default class importsCreateController{

        constructor($scope,UserInfoConstant,$timeout,importsServices,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,$modal){
            this.$mdDialog = $mdDialog
            var vm = this;
            vm.importCreate = importCreate
            vm.validateCurrency = validateCurrency
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.loadItems = loadItems
            vm.loadItemCSV = loadItemCSV
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getNet = getNet
            vm.ItemsCreate = ItemsCreate
            $scope.internacionalInvoice = false
            $scope.nacionalInvoice = true
            $scope.parseInt = parseInt
            $scope.discount = 0
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")

            function validateCurrency(currency){
                if (currency == 'Euro') {
                    $scope.currencySymbol = '€'
                }else{
                    $scope.currencySymbol = '$'
                }
            }

            loadItems()

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    let model = {
                            userId : $scope.userInfo.id
                        }

                    warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                      $scope.warehouses = dataReturn.data;
                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                    })
                }
            })
            
            //

            providersServices.providers.getProviders().$promise.then((dataReturn) => {
                  $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })



            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.importsCreate.loadItem(row.entity)" type="button" class="btn btn-warning btn-xs">+</button>'+
                                   '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.importsCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }  
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{row.entity.vatPrice}}</strong></p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    }
                ]
            };

            function loadItems(){
                itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }
            

            $scope.importItems = []

            function loadItem(row){

              console.log("item data push",row)

              // var i = 0
              // for (i=0; i<$scope.importItems.length; i++) {
              //       var id = $scope.importItems[i].itemId

              //       if(id == row.itemId){
              //         ngNotify.set('Item '+ row.itemId +' Duplicado en este Documento','warn')
              //         $scope.importItems.splice(index, 1)

              //       }else{
              //         console.log("item no puplicado",row.itemId)
              //       }
                    
              //     }
                 $scope.importItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    referenceKey : row.brandCode,
                    price: Number(row.netPrice),
                    quantity: 1,
                    disscount: 0

                });

                 $scope.warningItems = false    
            }

            function loadItemCSV(row){
                 $scope.importItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    referenceKey : row.referenceKey,
                    // price: Math.round(row.netPrice),
                    price: Number(row.netPrice),
                    quantity: Number(row.quantity),
                    disscount: 0

                });

                 $scope.warningItems = false    
            }

            function getNet() {
                
                var net = 0
                  var i = 0
                     for (i=0; i<$scope.importItems.length; i++) {
                         var valueItems = $scope.importItems[i] 
                         net += valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100; 

                      }

                      $scope.NET =  net
                      $scope.netoInvoice = net - (($scope.discount*net)/100) 
                      $scope.total = $scope.netoInvoice
                      $scope.VAT = 0
                      return net;
                        
                             
            }



            function removeItems(index) {

                $scope.importItems.splice(index, 1);
            }

            function setName(providers) {

                $scope.providerName = providers.providerName
                $scope.providerId = providers.providerId
            }

            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                $scope.warehouseId = warehouseId
            }

            function showPanel() {
                $scope.panelImport = true

            }

            function ItemsCreate() {
                var url = $state.href("app.itemsCreate");

                window.open(url,'_blank',"width=600,height=700");

            }

            function importCreate(value) {
                  console.log('valor',value)
                        let model = {
                            userId: $scope.userInfo.id,
                            providerId : $scope.providerId,
                            originId : $scope.warehouseId,
                            // type: $scope.selected,
                            type: 2,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            comment : $scope.observation,
                            currency : $scope.currency,
                            currencyValue : $scope.valueCurrency,
                            importItems : $scope.importItems 
                        }      

                        console.log('modelo',model)

                        if (value == "draft") {

                          console.log('draft',value)
                          importCreateDraft(model)

                        }else{
                          invoiceSave(model)
                        } 
                    }

      function importCreateDraft(model){
        importsServices.imports.importCreateDraft(model).$promise.then((dataReturn) => {
            $scope.result = dataReturn.data;
            ngNotify.set('Se ha creado el Borrador correctamente','warn')
            $state.go("app.importDraft", { idImport: $scope.result});
        },(err) => {
            console.log('No se ha podido conectar con el servicio',err);
        })

         }

      function invoiceSave (model){

        importsServices.imports.createImport(model).$promise.then((dataReturn) => {
          ngNotify.set('Se ha creado documento importación correctamente','success')
          $state.go('app.imports')
        },(err) => {
            ngNotify.set('Error al crear documento','error')
        })
      }

    }
        
        uploadCsv() {
            this.confirm = this.$mdDialog.confirm({
                controller         : 'uploadCsvDialogController',
                controllerAs       : 'uploadCsv',
                template           : require('./dialogs/uploadCsv/uploadCsv.html'),
                locals           	 : {},
                animation          : true,
                parent             : angular.element(document.body),
                clickOutsideToClose: true,
            })
            this.$mdDialog.show(this.confirm).then((row) => {
                if (row) {
					const dataReturn = row.data
					if(dataReturn.length < 2) {
						this.loadItemCSV(dataReturn[0])
					}else {
						for(const i in dataReturn){
							this.loadItemCSV(dataReturn[i])
						}
					}
 					
                }
            })
        }
        // Funciones
        

    }   

importsCreateController.$inject = ['$scope','UserInfoConstant','$timeout','importsServices','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','$modal'];

