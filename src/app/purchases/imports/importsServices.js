/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> importsServices
* file for call at services for providers
*/

class importsServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var imports = $resource(SERVICE_URL_CONSTANT.jormat + '/documents/import/:route',{},{
      
        getImports: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getImportsDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        getImportsItems: {
            method:'GET',
            params : {
               route: 'items'
            }
        },
        createImport: {
            method:'POST',
            params : {
                route: ''
            }
        },
        updateImport: {
            method:'PUT',
            params : {
                route: ''
            }
        },

        disabledInvoice: {
            method:'DELETE',
            params : {
                route: ''
            }
        },

        importCreateDraft: {
            method:'POST',
            params : {
                route: 'draft'
            }
        },

        updateImportDraft: {
            method:'PUT',
            params : {
                route: 'draft' 
            }
        },
        updateImportPostDraft: {
            method:'PUT',
            params : {
                route: 'update'
            }
        },
        getImportsItemsLocation: {
            method:'GET',
            params : {
               route: 'items-location'
            }
        }
    })

  

    return { 

            imports : imports
           }
  }
}

  importsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.importsServices', [])
  .service('importsServices', importsServices)
  .name;


  