/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> warehouseImportsController
* # As Controller --> warehouseImports														
*/


  export default class warehouseImportsController {

    constructor($scope, $filter,$rootScope, $http,importsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.transformToInvoice = transformToInvoice
        vm.validateImport = validateImport
        $scope.importId = $stateParams.importId
        $scope.originId = $stateParams.originId

        let params = {
                importId: $scope.importId,
                originId: $scope.originId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        importsServices.imports.getImportsDetails(params).$promise.then((dataReturn) => {
              $scope.import = dataReturn.data
                if ($scope.import[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.import[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                    }else{
                        $scope.currencySymbol = '$'
                    }

                }else{
                    $scope.internationalType = false
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Imports Details',err);
              })

        importsServices.imports.getImportsItemsLocation(params).$promise.then((dataReturn) => {
              $scope.importItems = dataReturn.data
              var subTotal = 0
              var i = 0
                 for (i=0; i<$scope.importItems.length; i++) {
                     subTotal = subTotal + $scope.importItems[i].total  
                  }
                  $scope.subTotality =  subTotal
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Imports Items',err);
              })

    	function cancel() {
            window.close();
		}

        function transformToInvoice(row){

          $state.go("app.updateInvoiceProvider", { importId: $scope.importId});

        }

        function print() {
              window.print();

        }

        function validateImport(value) {
            
         let model = {
                    importId : $scope.importId,
                    userId : $scope.userInfo.id,
                    statusValue : value 
                }

                console.log('that',model)

                  importsServices.imports.updateImport(model).$promise.then((dataReturn) => {

                    ngNotify.set('documento importacion validado exitosamente','success');
                    $state.go("app.imports")
                  },(err) => {
                      ngNotify.set('Error al validar documento','error')
                  })

                

                
        }


    }

  }

  warehouseImportsController .$inject = ['$scope', '$filter','$rootScope', '$http','importsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];

