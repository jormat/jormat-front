/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsHistoryController
* # As Controller --> itemsHistory														
*/

  export default class itemsHistoryController {

    constructor($scope,$modal,$filter,$rootScope, $http,$modalInstance,reportsItemsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
        vm.loadItems = loadItems
        vm.viewItem = viewItem
        vm.viewImport = viewImport

        $scope.itemsSoldGridReport = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'reporte_items_importacion.csv',
                exporterPdfHeader: { 
                    text: "Reporte items solicitados", 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [20,20,20,20] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },

                    
                columnDefs: [
                    { 
                      name:'Id',
                      field: 'itemId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a style="font-size: 12px;" ng-click="grid.appScope.itemsHistory.viewItem(row.entity)" tooltip="Ver Item">{{row.entity.itemId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name:'Descripción Item',
                      field: 'itemDescription',
                      width: '24%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'referencias',  
                      width: '14%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 10px;">{{row.entity.referencias}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Proveedor',
                      field: 'providerName',
                      width: '16%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;"><strong></strong> {{row.entity.providerName}}</p>' +
                                 '</div>'  
                    },
                    // { 
                    //   name:'ID',
                    //   field: 'clientId',
                    //   width: '6%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                    //              '<p tooltip="Código Cliente"><span class="label bg-info text-center" >{{row.entity.clientId}}</span></p>' +
                    //              '</div>'  
                    // },
                    { 
                      name:'Doc.',
                      field: 'importId',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<a style="font-size: 12px;" ng-click="grid.appScope.itemsHistory.viewImport(row.entity)"><span tooltip="Ver importación" class="badge bg-accent">{{row.entity.importId}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Precio',
                      field: 'price',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;">$<strong> {{row.entity.price}}</strong></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Cant.',
                      field: 'quantityItems',
                      enableFiltering: false,
                      width: '6%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<a><span class="label bg-secundary">{{row.entity.quantityItems}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Fecha',
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p style="font-size: 11px;"> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>' 
                    },
                    // { 
                    //   name:'BOD',
                    //   field: 'origin',
                    //   width: '4%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="uppercase label bg-secundary">{{row.entity.origin}}</p>' +
                    //              '</div>'  
                    // },
                    { 
                      name:'Ubicacion',
                      field: 'locations',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning" style="font-size: 8px;">{{row.entity.locations}}</p>' +
                                 '</div>'  
                    }
                ]
            };

        loadItems()

        function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

        function viewImport(row){
              $scope.invoiceData = row;
                var modalInstance  = $modal.open({
                        template: require('../view/imports-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'importsViewController',
                        controllerAs: 'importsView',
                        size: 'lg'
                })
            }
           
        function loadItems() {

          $scope.start = moment($scope.startDate).format("YYYY-MM-DD").toString()
          $scope.end = moment($scope.endDate).format("YYYY-MM-DD").toString()

          if( $scope.start == $scope.end ){

                $scope.start = ''
                $scope.end = ''

              }

            let model = {
                startDate: $scope.start,
                endDate: $scope.end
            }

            reportsItemsServices.reports.getImportsItems(model).$promise.then((dataReturn) => {
              $scope.itemsSoldGridReport.data = dataReturn.data

              if ($scope.itemsSoldGridReport.data.length == 0){
                      $scope.messageInvoices = true
                      ngNotify.set( 'No existes documentos creados entre '+ model.startDate +' y '+ model.endDate , {
                      sticky: true,
                      button: true,
                      type : 'warn'
                  })
                  }else{
                      $scope.messageInvoices = false
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }


    	function cancel() {
			$modalInstance.dismiss('chao');
		}


    }

  }

  itemsHistoryController.$inject = ['$scope','$modal', '$filter','$rootScope', '$http','$modalInstance','reportsItemsServices','$location','UserInfoConstant','ngNotify','$state'];
