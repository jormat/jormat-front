

import routing from './reports.route';
import run from './reports.run';

//items files
import itemsSoldListController from './items/sold/itemsSoldListController';
import historicalItemsSoldController from './items/sold/historical/historicalItemsSoldController';
import itemsOutputsListController from './items/outputs/itemsOutputsListController';
import itemsPurchasedListController from './items/purchased/itemsPurchasedListController';
import itemsInventoryController from './items/inventory/itemsInventoryController';
import dailyInventoryController from './items/daily-inventory/dailyInventoryController'; 
import warehouseInventoryController from './items/warehouse-inventory/warehouseInventoryController'; 
import itemsFaultyListController from './items/faulty/itemsFaultyListController'; 
import saleProductsController from './items/sold/sale-products/saleProductsController'; 
import notSoldPbiController from './items/not-sold/notSoldPbiController';
import reportsItemsServices from './items/reportsItemsServices';

//sales files
import userSalesController from './sales/user-sales/userSalesController';
import profitabilitySalesController from './sales/user-sales/profitability-sales/profitabilitySalesController';
import warehouseSalesController from './sales/warehouse-sales/warehouseSalesController';
import salesReportServices from './sales/salesReportServices';
import incomeStatementController from './sales/income-statement/incomeStatementController'
import salesPowerBiController from './sales/sales-power-bi/salesPowerBiController'  


export default angular.module('app.reports', [reportsItemsServices,salesReportServices])
  .config(routing)
  .controller('itemsSoldListController', itemsSoldListController)
  .controller('historicalItemsSoldController', historicalItemsSoldController)
  .controller('itemsOutputsListController', itemsOutputsListController)
  .controller('itemsPurchasedListController', itemsPurchasedListController)
  .controller('itemsInventoryController', itemsInventoryController)
  .controller('dailyInventoryController', dailyInventoryController) 
  .controller('warehouseInventoryController', warehouseInventoryController) 
  .controller('userSalesController', userSalesController)
  .controller('profitabilitySalesController', profitabilitySalesController)
  .controller('incomeStatementController', incomeStatementController)
  .controller('warehouseSalesController', warehouseSalesController)
  .controller('itemsFaultyListController', itemsFaultyListController) 
  .controller('saleProductsController', saleProductsController)
  .controller('incomeStatementController', incomeStatementController)
  .controller('salesPowerBiController', salesPowerBiController) 
  .controller('notSoldPbiController', notSoldPbiController) 
  .run(run)
  .constant("reports", [{
        "title": "Reportes",
        "icon":"mdi-newspaper",
        "subMenu":[
           {
            'title' : 'Reportes Repuestos',
            "superMenu":[
            
                    {
                    'title' : 'Items Vendidos',
                    'url': 'app.itemsSold'
                    },
                    {
                    'title' : 'Items despacho',
                    'url': 'app.itemsOutputs'
                    },
                    {
                    'title' : 'Items comprados',
                    'url': 'app.itemsPurchased'
                    },
                    {
                    'title' : 'Items con falla',
                    'url': 'app.faultyItems'
                    } ,
                    {
                    'title' : 'Rotación Items',
                    'url': 'app.itemsNotSold'
                    } ,
                    {
                    'title' : 'Ventas general',
                    'url': 'app.salesItemsPBI'
                    }  
                ]
            },
            {
            'title' : 'Reportes Ventas',
            "superMenu":[
            
                    {
                    'title' : 'Ventas usuarios',
                    'url': 'app.userSales'
                    },
                    {
                    'title' : 'Ventas Sucursal',
                    'url': 'app.warehouseSales'
                    }     
                ]
            }
        ]
    }])
  .name;