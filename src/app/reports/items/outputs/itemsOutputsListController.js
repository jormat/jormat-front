
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsOutputsListController
* # As Controller --> itemsOutputsList														
*/

export default class itemsOutputsListController{

        constructor($scope,UserInfoConstant,$timeout,reportsItemsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.viewItem= viewItem
            vm.searchData=searchData
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');
            

            $scope.itemsOutputsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'reporte_items_despachados.csv',
                columnDefs: [
                    

                    { 
                      name:'Id',
                      field: 'itemId',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a style="font-size: 11px;" ng-click="grid.appScope.itemsOutputsList.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name:'Item',
                      field: 'itemDescription',
                      width: '28%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'references',  
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">{{row.entity.references}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">{{row.entity.clientName}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Guia',
                      field: 'guideId',
                      width: '7%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p><span tooltip="Código guia" class="badge bg-warning">{{row.entity.guideId}}</span></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Cant.',
                      field: 'quantityItems',
                      width: '5%' ,
                      enableFiltering: false
                    },
                    { 
                      name:'Fecha',
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'De:',
                      field: 'origin',
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary">{{row.entity.origin}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'A:',
                      field: 'destination',
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary">{{row.entity.destination}}</p>' +
                                 '</div>'  
                    }
                ]
            };
            
          
         	reportsItemsServices.reports.getItemsOutputs().$promise.then((dataReturn) => {
         		  $scope.itemsOutputsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function searchData() {
              reportsItemsServices.reports.getItemsOutputs().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.itemsOutputsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

itemsOutputsListController.$inject = ['$scope','UserInfoConstant','$timeout','reportsItemsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

