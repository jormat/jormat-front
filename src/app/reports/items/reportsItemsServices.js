/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> reportsItemsServices
* file for call at services for providers
*/

class reportsItemsServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var reports = $resource(SERVICE_URL_CONSTANT.jormat + '/reports/:route',{},{
      
		getItemsSold: {
			method:'GET',
			params : {
				route: 'items-sold'
			}
		},

		getItemsSoldPaginated: {
			method:'GET',
			params : {
				route: 'items-sold-paginated'
			}
		},

        getItemsSoldHistoric: {
            method:'GET',
              params : {
                route: 'items-sold-historic'
                }
          },

       getItemsOutputs: {
        method:'GET',
          params : {
            route: 'items-outputs'
            }
          },
        
        getItemsPurchased: {
          method:'GET',
          params : {
            route: 'items-purchased'
            }
          },
          
        getItemsInventory: {
          method:'GET',
          params : {
            route: 'inventory'
            }
          },

        getDailyInventory: {
          method:'GET',
          params : {
            route: 'daily-inventory'
            }
          },

        getItemsInventoryByWarehouse: {
          method:'GET',
          params : {
            route: 'warehouse-inventory'
            }
          },

        getCardexItems: {
          method:'GET',
          params : {
            route: 'items-cardex'
            }
          },

        getImportsItems: {
          method:'GET',
          params : {
            route: 'items-imports'
            }
          },
          
        getTransitItems: {
          method:'GET',
          params : {
            route: 'items-transit-imports'
            }
          },

        getFaultyItems: {
          method:'GET',
          params : {
            route: 'faulty-items'
            }
          }
    })

    return { reports : reports
             
           }
  }
}

  reportsItemsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.reportsItemsServices', [])
  .service('reportsItemsServices', reportsItemsServices)
  .name;