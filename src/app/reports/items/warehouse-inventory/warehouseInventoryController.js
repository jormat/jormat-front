
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> warehouseInventoryController
* # As Controller --> warehouseInventory														
*/

export default class warehouseInventoryController{

        constructor($scope,UserInfoConstant,$timeout,reportsItemsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService,warehousesServices,itemsServices,locationsServices){
            var vm = this;
            vm.viewItem= viewItem 
			this.mayorCero = false
			this.ngNotify = ngNotify
            // vm.callLocations= callLocations
            vm.callItems = callItems
            vm.selectAllItemsMajorCero = selectAllItemsMajorCero
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }
            


            i18nService.setCurrentLang('es');

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            function callItems (warehouse){
                console.log('warehouse',warehouse)
                $scope.warehouseId = warehouse.warehouseId 
                $scope.warehouseName = warehouse.warehouseName
                $scope.listItems =  true
                $scope.itemsGrid.data = []
                let params = {
                    warehouseId : warehouse.warehouseId,
					mayorCero : this.mayorCero
                }

                reportsItemsServices.reports.getItemsInventoryByWarehouse(params).$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
            }

			function selectAllItemsMajorCero (mayorCero){
				if ($scope.itemsGrid.data.length === 0){
					this.ngNotify.set('Seleccione bodega','warn');
					this.mayorCero = !mayorCero
					return true
				}

				const params = {
                    warehouseId : $scope.warehouseId,
					mayorCero
                }

				reportsItemsServices.reports.getItemsInventoryByWarehouse(params).$promise.then((dataReturn) => {
					$scope.itemsGrid.data = dataReturn.data;
  
				},(err) => {
					console.log('No se ha podido conectar con el servicio',err);
				})
			}


            // function callLocations(warehouse) {
            //     console.log('warehouse',warehouse)
            //     $scope.warehouseId = warehouse.warehouseId 
            //     $scope.warehouseName = warehouse.warehouseName

            //     let params = {
            //         warehouseId : $scope.warehouseId
            //     }

            //     locationsServices.locations.getLocations(params).$promise.then((dataReturn) => {
            //       $scope.locations = dataReturn.data;
            //       $scope.locationsInput = false
            //       $scope.locations.selected = []
            //       },(err) => {
            //           console.log('No se ha podido conectar con el servicio',err);
            //     })
            // }
            

            $scope.itemsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Inventario_general.csv',
                exporterPdfHeader: { 
                    // text: "Inventario Bodega"+ $scope.namewarehouse +"Ubicacion"+$scope.locationName,
                    text: "Inventario Bodega ", 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [20,20,20,20] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },

                    
                columnDefs: [
                    { 
                      name:'Id',
                      field: 'itemId',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.warehouseInventory.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name:'Descripcion',
                      field: 'itemDescription',
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'referencias',  
                      width: '17%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 10px;">{{row.entity.referencias}}</p>' +
                                 '</div>'  
                    },
                    
                    { 
                      name:'Marca',
                      field: 'brand',
                      width: '9%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<a style="font-size: 10px;" ng-click="grid.appScope.itemsInventory.viewInvoice(row.entity)"><span class="badge bg-warning" style="font-size: 11px;">{{row.entity.brand}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.vatPrice }}</p>' +
                                 '</div>' 
                    },
                    
                    
                    
                    { 
                      name:'Stock.',
                      field: 'itemQty',
                      width: '5%',
                      cellClass:'red'
                    },

                    { 
                      name:'Proveedor',
                      field: 'providers',
                      width: '14%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secundary uppercase" style="font-size: 9px;">{{row.entity.providers}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Ubicacion',
                      field: 'locations',
                      width: '14%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning">{{row.entity.locations}}</p>' +
                                 '</div>'  
                    }
                ]
            };
            

            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

warehouseInventoryController.$inject = ['$scope','UserInfoConstant','$timeout','reportsItemsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService','warehousesServices','itemsServices','locationsServices'];

