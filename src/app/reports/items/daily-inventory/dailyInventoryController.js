
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> dailyInventoryController
* # As Controller --> dailyInventory														
*/

export default class dailyInventoryController{

        constructor($scope,UserInfoConstant,$timeout,reportsItemsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService,warehousesServices){
            var vm = this;
            vm.viewItem= viewItem 
            vm.viewInvoice = viewInvoice 
            vm.searchData=searchData
            vm.loadItems = loadItems
            vm.setIdWarehouse = setIdWarehouse
            $scope.messageInvoices = false
            $scope.CurrentDate = moment($scope.day).format("YYYY-MM-DD")
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //
            

            $scope.itemsSoldGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'inventario_'+ moment($scope.day).format("DD-MM-YYYY")+'.csv',
                exporterPdfHeader: { 
                    text: "Inventario diario: " + moment($scope.day).format("DD-MM-YYYY"),
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [20,20,20,20] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },

                    
                columnDefs: [
                    { 
                      name:'Item',
                      field: 'itemId',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.dailyInventory.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      width: '28%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'reference',  
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">{{row.entity.reference}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Marca',
                      field: 'brand',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="uppercase label bg-info"> {{row.entity.brand }}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Id Doc.',
                      field: 'documentId',
                      width: '6%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<span class="badge bg-secundary">{{row.entity.documentId}}</span>' +
                                 '</div>'
                    },
                    { 
                      name:'tipo',
                      field: 'codeDoc',
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning">{{row.entity.codeDoc}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Salida',
                      field: 'outputs',
                      width: '6%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<a><span class="label bg-danger">{{row.entity.outputs}}</span></a>' +
                                 '</div>'
                    },
                    
                    
                    { 
                      name:'Stock',
                      field: 'stock',
                      width: '5%',
                      cellClass:'red',
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<p style="font-size: 15;">{{row.entity.stock}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Ubicaciones',
                      field: 'locations',
                      width: '15%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 12px;">{{row.entity.locations}}</p>' +
                                 '</div>'  
                    }
                ]
            };
            
          
         	// reportsItemsServices.reports.getDailyInventory().$promise.then((dataReturn) => {
         	// 	  $scope.itemsSoldGrid.data = dataReturn.data;
          //     },(err) => {
          //         console.log('No se ha podido conectar con el servicio',err);
          //     })

            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function viewInvoice(row){
              $scope.invoiceData = row;
              var modalInstance  = $modal.open({
                      template: require('../../../sales/clients-invoices/view/clients-invoices-view.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'clientsInvoicesViewController',
                      controllerAs: 'clientsInvoicesView',
                      size: 'lg'
              })
            }

            function searchData() {
              reportsItemsServices.reports.getDailyInventory().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.itemsSoldGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

            function setIdWarehouse(warehouse) {
                $scope.warehouseId = warehouse.warehouseId
                $scope.warehouseName = warehouse.warehouseName
            }

            function loadItems(){

            let model = {
                day: moment($scope.day).format("DD/MM/YY").toString(),
                warehouseId: $scope.warehouseId
            }

          

            reportsItemsServices.reports.getDailyInventory(model).$promise.then((dataReturn) => {
                  $scope.itemsSoldGrid.data = dataReturn.data;

              if ($scope.itemsSoldGrid.data.length == 0){
                      ngNotify.set('La fecha ingresada no poseen items asociados. ','warn')
                  }else{
                      console.log('fechas con data');
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

dailyInventoryController.$inject = ['$scope','UserInfoConstant','$timeout','reportsItemsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService','warehousesServices'];

