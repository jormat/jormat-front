/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> historicalItemsSoldController
* # As Controller --> historicalItemsSold items sold														
*/

  export default class historicalItemsSoldController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,reportsItemsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
        vm.loadItems = loadItems
        vm.viewDocument = viewDocument

        $scope.itemsSoldGridReport = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'reporte_items_vendidos.csv',
                exporterPdfHeader: { 
                    text: "Reporte items vendidos", 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [20,20,20,20] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },

                    
                columnDefs: [
                    { 
                      name:'ID',
                      field: 'itemId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a style="font-size: 11px;" ng-click="grid.appScope.itemsSoldList.viewItem(row.entity)" tooltip="Ver Item">{{row.entity.itemId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name:'Descripción Item',
                      field: 'itemDescription',
                      width: '14%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 10px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'referencias',  
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 9px;">{{row.entity.referencias}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '11%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 10px;"><strong>({{row.entity.clientId}}) </strong> {{row.entity.clientName}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Id',
                      field: 'clientId',
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<p tooltip="Código Cliente"><span class="label bg-info text-center" >{{row.entity.clientId}}</span></p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Doc.',
                      field: 'documentId',
                      width: '9%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<a ng-click="grid.appScope.itemsSoldList.viewDocument(row.entity)"><span tooltip="Ver documento" class="badge bg-accent" style="font-size: 10px;">{{row.entity.documentType}}: {{row.entity.documentId}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Precio',
                      field: 'price',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">$<strong> {{row.entity.price}}</strong></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Un.',
                      field: 'quantityItems',
                      enableFiltering: false,
                      width: '6%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<a><span class="label bg-secundary">{{row.entity.quantityItems}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Fecha',
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p style="font-size: 11px;"> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>' 
                    },
                    // { 
                    //   name:'BOD',
                    //   field: 'origin',
                    //   width: '4%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="uppercase label bg-secundary">{{row.entity.origin}}</p>' +
                    //              '</div>'  
                    // },
                    { 
                      name:'Ubicacion',
                      field: 'locations',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning" style="font-size: 8px;">{{row.entity.locations}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'BOD',
                      field: 'dsCode',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning" tooltip="Codigo Bodega" style="font-size: 9px;">{{row.entity.dsCode}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'S/G',
                      field: 'generalStock',
                      enableFiltering: false,
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 10px;" class="badge bg-danger" tooltip="Sucursal/General">{{row.entity.warehouseStock}}/{{row.entity.generalStock}}</p>' +
                                 '</div>'  
                    }
                ]
            }


        function viewDocument(row){
                console.log('No se ha podido conectar con el servicio',row);
              
                $scope.invoiceData = row;

                if ($scope.invoiceData.documentType == "FACT") {

                    $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName,
                    }
                    console.log('ver factura');
                    var modalInstance  = $modal.open({
                            template: require('../../../../sales/clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.documentType == "NN") { 

                    $scope.invoiceData = {
                        documentId:$scope.invoiceData.documentId,
                         clientName:$scope.invoiceData.clientName,
                    }
                    console.log('ver documento nn');
                    var modalInstance  = $modal.open({
                            template: require('../../../../sales/documents-nn/view/documents-nn-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'documentsNnViewController',
                            controllerAs: 'documentsNnView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.documentType == "BOL") { 

                    $scope.ballotsData = {
                        ballotId:$scope.invoiceData.documentId,
                         clientName:$scope.invoiceData.clientName,
                    }
                    console.log('Boleta');
                    var modalInstance  = $modal.open({
                            template: require('../../../../sales/ballots/view/ballots-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }

        }


        function loadItems() {

            let model = {
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            reportsItemsServices.reports.getItemsSoldHistoric(model).$promise.then((dataReturn) => {
              $scope.itemsSoldGridReport.data = dataReturn.data

              if ($scope.itemsSoldGridReport.data.length == 0){
                      $scope.messageInvoices = true
                      ngNotify.set( 'No existes facturas creadas entre '+ model.startDate +' y '+ model.endDate , {
                      sticky: true,
                      button: true,
                      type : 'warn'
                  })
                  }else{
                      $scope.messageInvoices = false
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }


    	function cancel() {
			$modalInstance.dismiss('chao');
		}


    }

  }

  historicalItemsSoldController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','reportsItemsServices','$location','UserInfoConstant','ngNotify','$state'];
