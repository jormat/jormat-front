
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsSoldListController
* # As Controller --> itemsSoldList														
*/

export default class itemsSoldListController{

	constructor($scope,UserInfoConstant,$timeout,reportsItemsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService, $rootScope, uiGridExporterConstants, uiGridExporterService, $translate){
		var vm = this;
		this.$scope = $scope
		this.$translate = $translate
		this.$modal = $modal
		this.$timeout = $timeout
		this.ngNotify = ngNotify
		this.uiGridExporterConstants = uiGridExporterConstants
		this.uiGridExporterService = uiGridExporterService
		this.reportsItemsServices = reportsItemsServices
		this.$rootScope = $rootScope
		this.$interval = $interval
		this.searchOptions = {
			updateOn: 'default blur',
			debounce:{
				'default': 100,
				'blur': 0
			}
		}

		i18nService.setCurrentLang('es')
		this.tableInitializeMethod()
		
		this.$scope.UserInfoConstant = UserInfoConstant
		this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
			if (details !== undefined) {
				this.$scope.userInfo = details.user
				this.main()
			}
		})
	}

	main() {
		const searchText = ''
		this.params = {
			newPage    	   : 1,
			pageSize       : 2000,
			searchText
		}
		this.getItemsSolds()
		this.$rootScope.$on('$translateChangeSuccess', () => {
			this.tableInitializeMethod()
		})
	}

	dataDownload(items) {
		let dataReturn = []

		items.forEach((row) => {
			row = row.entity ? row.entity : row
			const rowData = [
				{ value: row.itemId },
				{ value: row.itemDescription },
				{ value: row.referencias },
				{ value: row.clientName },
				{ value: row.clientId },
				{ value: row.documentId },
				{ value: row.documentType },
				{ value: row.price },
				{ value: row.quantityItems },
				{ value: row.date },
				{ value: row.dsCode },
				{ value: row.locations },
				{ value: row.generalStock }
			]

			dataReturn.push(rowData)
		})

		return dataReturn
	}

	exportCSV() {
		const exportService = this.uiGridExporterService
		const dataFilter = this.dataDownload(this.gridApi.grid.rows)
		const grid = this.gridApi.grid
		const fileName = 'reporte_items_vendidos.csv'

		exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
			const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
			let formatColums = []

			exportColumnHeaders.map((colums) => {
					formatColums.push(colums)
			})
			const csvContent = exportService.formatAsCsv(formatColums, dataFilter, grid.options.exporterCsvColumnSeparator)

			exportService.downloadFile(fileName, csvContent, grid.options.exporterOlderExcelCompatibility)
		}).catch(() => {
			this.ngNotify.set('ERROR', 'error')
		})
	}
	
	exportPDF() {
		const exportService = this.uiGridExporterService
		const dataFilter = this.dataDownload(this.gridApi.grid.rows)
		const grid = this.gridApi.grid

		exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
			const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
			let formatColums = []

			exportColumnHeaders.map((colums) => {
					formatColums.push(colums)
			})

			const content = exportService.prepareAsPdf(grid, formatColums, dataFilter)
			pdfMake.createPdf(content).open()
		}).catch(() => {
			this.ngNotify.set('ERROR', 'error')
		})
	}

	exportAllCSV() {
		const exportService = this.uiGridExporterService
		this.reportsItemsServices.reports.getItemsSold().$promise.then(
			response => {
				if (!response.status) {
					const msn = response.messageError || response.message || this.$translate('MSJ_ERROR')

					this.ngNotify.set(msn, 'warn')
				} else {
					const dataFilter = this.dataDownload(response.data)
					const grid = this.gridApi.grid
					const fileName = 'reporte_items_vendidos.csv'

					exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
						const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
						let formatColums = []

						exportColumnHeaders.map((colums) => {
								formatColums.push(colums)
						})

						const csvContent = exportService.formatAsCsv(formatColums, dataFilter, grid.options.exporterCsvColumnSeparator)

						exportService.downloadFile(fileName, csvContent, grid.options.exporterOlderExcelCompatibility)
					}).catch(() => {
						this.ngNotify.set('ERROR', 'error')
					})
				}
			},
			() => {
				this.ngNotify.set(this.$translate('MSJ_NO_DATA_RELATED_YOUR_SEARCH') + ' getDownloadAllCsv', 'error')
			},
		)
	}

	tableInitializeMethod() {
		this.$scope.gridOptions = {
			paginationPageSizes  : [ 500, 1000, 2000 ],
			paginationPageSize   : 2000,
			enablePaging         : true,
			useExternalPagination: true,
			enableFiltering      : true,
			enableGridMenu       : true,
			enableSelectAll      : true,
			exporterCsvFilename  : 'reporte_items_vendidos.csv',
			exporterMenuPdf      : false,
			exporterMenuCsv      : false,
			exporterMenuAllData  : false,
			multiSelect          : true,
			exporterPdfHeader: { 
				text: "Reporte items vendidos", 
				style: 'headerStyle',
				alignment: 'center'
			},
			exporterPdfFooter: function ( currentPage, pageCount ) {
				return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
			},
			exporterPdfCustomFormatter: function ( docDefinition ) {
				docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [20,20,20,20] };
				docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
				return docDefinition;
			},
			onRegisterApi        : (gridApi) => {
				this.gridApi = gridApi

				this.$interval(() => {
					this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar vista como csv', action: () => {
						this.exportCSV()
					}, order: 100 } ])
					this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar todo como csv', action: () => {
						this.exportAllCSV()
					}, order: 100 } ])
					this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar vista como PDF', action: () => {
						this.exportPDF()
					}, order: 100 } ])
				}, 0, 1)

				gridApi.pagination.on.paginationChanged(this.$scope, (newPage, pageSize) => {
					this.params.newPage = newPage
					this.params.pageSize = pageSize

					this.reportsItemsServices.reports.getItemsSoldPaginated(this.params).$promise.then(
						response => {
							if (!response.status) {
								const msn = response.messageError || response.message || 'ERROR'
			
								this.ngNotify.set(msn, 'warn')
							} else {
								this.items = angular.copy(response.data)
								this.$scope.gridOptions.data = response.data
								this.$scope.gridOptions.totalItems = response.count || 0
							}
						},
						() => {
							this.ngNotify.set('Sin respuesta items-sold-paginated', 'error')
						},
					)
				})

				gridApi.core.on.filterChanged(this.$scope, () => {
					const grid = gridApi.grid.renderContainers.body.grid
					let filters = {}

					for (let i in grid.columns) {
						if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term != undefined)) {
							filters[grid.columns[i].field] = grid.columns[i].filters[0].term
						}
					}

					this.params.newPage = 1
					this.params.filters = filters

					this.reportsItemsServices.reports.getItemsSoldPaginated(this.params).$promise.then(
						response => {
							if (!response.status) {
								const msn = response.messageError || response.message || 'ERROR'
			
								this.ngNotify.set(msn, 'warn')
							} else {
								this.items = angular.copy(response.data)
								this.$scope.gridOptions.data = response.data
								this.$scope.gridOptions.totalItems = response.count || 0
							}
						},
						() => {
							this.ngNotify.set('Sin respuesta items-sold-paginated', 'error')
						},
					)
				})
			},
			exporterHeaderFilter: (displayName) => {
				if (displayName === 'id') {
					return ''
				} else {
					return displayName
				}
			},
			exporterFieldCallback: (grid, row, col, input) => {
				// if( col.name == 'Índice' ){
				//   return grid.renderContainers.body.visibleRowCache.indexOf(row)+1;
				// } else {
				return input
				// }
			},
			columnDefs: [
				{ 
					name:'ID',
					field: 'itemId',  
					width: '5%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<a ng-click="grid.appScope.itemsSoldList.viewItem(row.entity)"><span style="font-size: 10px;">{{row.entity.itemId}}</span></a>' +
								'</div>'  
				},
				{ 
					name:'Descripción',
					field: 'itemDescription',
					width: '18%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p class="uppercase" style="font-size: 10px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
								'</div>' 
				},
				{ 
					name:'Referencias',
					field: 'referencias',  
					width: '11%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p class="uppercase" style="font-size: 10px;"><em>{{row.entity.referencias}}<em></p>' +
								'</div>'  
				},
				{ 
					name:'Cliente',
					field: 'clientName',
					width: '9%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p class="uppercase" style="font-size: 10px;"><strong>{{row.entity.clientName}}</strong></p>' +
								'</div>'  
				},
				{ 
					name:'Id',
					field: 'clientId',
					width: '5%',
					cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
								'<p tooltip="Código Cliente"><span class="label bg-info text-center" >{{row.entity.clientId}}</span></p>' +
								'</div>'  
				},

				{ 
					name:'Doc.',
					field: 'documentId',
					enableFiltering: false,
					width: '6%' ,
					cellTemplate:'<div class="ui-grid-cell-contents">'+
								'<a ng-click="grid.appScope.itemsSoldList.viewDocument(row.entity)"><span tooltip="Ver documento" class="badge bg-accent" style="font-size: 10px;">{{row.entity.documentId}}</span></a>' +
								'</div>'
				},
				{ 
					name:'Tipo',
					field: 'documentType',
					enableFiltering: false,
					width: '6%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p style="font-size: 10px;" class="label bg-secundary">{{row.entity.documentType}}</p>' +
								'</div>'  
				},

				
				{ 
					name:'Precio',
					field: 'price',
					enableFiltering: false,
					width: '6%' ,
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p class="uppercase" style="font-size: 11px;">$<strong>{{row.entity.price}}</strong></p>' +
								'</div>'
				},
				{ 
					name:'Un.',
					field: 'quantityItems',
					enableFiltering: false,
					width: '5%' ,
					cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
								'<a><span class="label bg-secundary" style="font-size: 10px;">{{row.entity.quantityItems}}</span></a>' +
								'</div>'
				},
				{ 
					name:'Fecha',
					field: 'date',
					width: '6%',
					cellTemplate:'<div class="ui-grid-cell-contents">'+
								'<p style="font-size: 10px;"> <strong> {{row.entity.date | date:\'dd/MM/yyyy\'}}</strong></p>' +
								'</div>' 
				},
				{ 
					name:'BOD',
					field: 'dsCode',
					width: '7%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p class="uppercase label bg-warning" tooltip="Codigo Bodega" style="font-size: 9px;">{{row.entity.dsCode}}</p>' +
								'</div>'  
				},

				{ 
					name:'Ubicacion',
					field: 'locations',
					enableFiltering: false,
					width: '9%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p class="uppercase label bg-warning" style="font-size: 8px;">{{row.entity.locations}}</p>' +
								'</div>'  
				}
				,{ 
					name:'S/G',
					field: 'generalStock',
					width: '7%',
					enableFiltering: false,
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p style="font-size: 10px;" class="label bg-danger" tooltip-placement="left" tooltip="Stock Sucursal/General {{row.entity.stockWarehouses}}">{{row.entity.warehouseStock}} / {{row.entity.generalStock}}</p>' +
								'</div>'  
				}
			],
		}

		this.$rootScope.$watch('sidenavState', () => {
			this.$timeout(() => {
				console.log('there was a resize')
				this.gridApi.grid.handleWindowResize()
			}, 500)
		})
	}

	getItemsSolds() {
		this.reportsItemsServices.reports.getItemsSoldPaginated(this.params).$promise
			.then((data) => {
				this.items = angular.copy(data.data)
				this.$scope.gridOptions.data = data.data
				this.$scope.gridOptions.totalItems = data.count || 0
		},(err) => {
			console.log('No se ha podido conectar con el servicio',err);
		})
	}

	viewHistoricReport() {
		const 	modalInstance  = this.$modal.open({
					template: require('./historical/historical-items-sold.html'),
					animation: true,
					scope: this.$scope,
					controller: 'historicalItemsSoldController',
					controllerAs: 'historicalItemsSold',
					size: 'lg'
			})
	}

	viewPowerBI() {
		const	modalInstance  = this.$modal.open({
					template: require('./sale-products/sale-products.html'),
					animation: true,
					scope: this.$scope,
					controller: 'saleProductsController',
					controllerAs: 'saleProducts',
					size: 'lg'
			})
	}

	viewDocument(row) {
		console.log('No se ha podido conectar con el servicio',row);
		this.$scope.invoiceData = row;

		if (this.$scope.invoiceData.documentType == "FACT") {

			this.$scope.invoiceData = {
				invoiceId: this.$scope.invoiceData.documentId,
				clientName: this.$scope.invoiceData.clientName,
			}
			console.log('ver factura');
			const 	modalInstance  = this.$modal.open({
					template: require('../../../sales/clients-invoices/view/clients-invoices-view.html'),
					animation: true,
					scope: this.$scope,
					controller: 'clientsInvoicesViewController',
					controllerAs: 'clientsInvoicesView',
					size: 'lg'
			})

		}

		if (this.$scope.invoiceData.documentType == "NN") { 
			this.$scope.invoiceData = {
				documentId:	this.$scope.invoiceData.documentId,
				clientName:	this.$scope.invoiceData.clientName,
			}
			console.log('ver documento nn');
			const 	modalInstance  = this.$modal.open({
					template: require('../../../sales/documents-nn/view/documents-nn-view.html'),
					animation: true,
					scope: this.$scope,
					controller: 'documentsNnViewController',
					controllerAs: 'documentsNnView',
					size: 'lg'
			})
		}

		if (this.$scope.invoiceData.documentType == "BOL") { 
			this.$scope.ballotsData = {
				ballotId:	this.$scope.invoiceData.documentId,
				clientName:	this.$scope.invoiceData.clientName,
			}
			console.log('Boleta');
			const 	modalInstance  = this.$modal.open({
					template: require('../../../sales/ballots/view/ballots-view.html'),
					animation: true,
					scope: this.$scope,
					controller: 'ballotsViewController',
					controllerAs: 'ballotsView',
					size: 'lg'
			})
		}
	}

	viewItem(row) {
		this.$scope.itemsData = row;
		const 	modalInstance  = this.$modal.open({
				template: require('../../../items/items/view/items-view.html'),
				animation: true,
				scope: this.$scope,
				controller: 'itemsViewController',
				controllerAs: 'itemsView',
				size: 'lg'
		})
	}
}   

itemsSoldListController.$inject = ['$scope','UserInfoConstant','$timeout','reportsItemsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService', '$rootScope', 'uiGridExporterConstants', 'uiGridExporterService', '$translate'];

