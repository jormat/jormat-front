
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsPurchasedListController
* # As Controller --> itemsPurchasedList														
*/

export default class itemsPurchasedListController{

        constructor($scope,UserInfoConstant,$timeout,reportsItemsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.viewItem= viewItem
            vm.viewInvoice = viewInvoice
            vm.searchData=searchData
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');
            

            $scope.itemsPurchasedGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'reporte_items_comprados.csv',
                columnDefs: [
                   
                    { 
                      name:'Id',
                      field: 'itemId',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.itemsPurchasedList.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name:'Item',
                      field: 'itemDescription',
                      width: '32%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },
                     { 
                      name:'Referencia',
                      field: 'references',  
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Proveedor',
                      field: 'providerName',
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">{{row.entity.providerName}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Factura',
                      field: 'invoiceCode',
                      width: '10%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<a ng-click="grid.appScope.itemsPurchasedList.viewInvoice(row.entity)"><span tooltip="Código factura" class="badge bg-accent">{{row.entity.invoiceCode}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Precio',
                      field: 'price',
                      width: '6%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">${{row.entity.price}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Cant',
                      field: 'quantityItems',
                      width: '5%' ,
                      enableFiltering: false
                    },
                    { 
                      name:'Fecha',
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'  
                    }
                ]
            };
            
          
         	reportsItemsServices.reports.getItemsPurchased().$promise.then((dataReturn) => {
         		  $scope.itemsPurchasedGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function viewInvoice(row){
            console.log('now brother',$scope.invoiceData)
              $scope.invoiceData = row;
              var modalInstance  = $modal.open({
                      template: require('../../../purchases/invoices-provider/view/invoices-provider-view.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'invoicesProviderViewController',
                      controllerAs: 'invoicesProviderView',
                      size: 'lg'
              })
            }

            function searchData() {
              reportsItemsServices.reports.getItemsPurchased().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.itemsPurchasedGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

itemsPurchasedListController.$inject = ['$scope','UserInfoConstant','$timeout','reportsItemsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

