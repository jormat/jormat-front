
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsFaultyListController
* # As Controller --> itemsFaultyList														
*/

export default class itemsFaultyListController{

        constructor($scope,UserInfoConstant,$timeout,reportsItemsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.viewItem= viewItem
            vm.viewAfterSale = viewAfterSale
            vm.searchData=searchData
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');
            

            $scope.faultyItemsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'reporte_items_con_falla.csv',
                columnDefs: [
                   
                    { 
                      name:'Id',
                      field: 'itemId',  
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.itemsFaultyList.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name:'Item',
                      field: 'itemDescription',
                      width: '33%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },
                     { 
                      name:'Referencia',
                      field: 'references',  
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  
                    },
                    
                    { 
                      name:'Post Venta',
                      field: 'afterSaleId',
                      width: '9%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<a ng-click="grid.appScope.itemsFaultyList.viewAfterSale(row.entity)"><span tooltip="Código Post Venta" class="badge bg-warning">{{row.entity.afterSaleId}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Precio',
                      field: 'price',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">${{row.entity.price}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Cant',
                      field: 'quantityItems',
                      width: '6%' ,
                      enableFiltering: false
                    },
                    { 
                      name:'Origen',
                      field: 'origin',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">{{row.entity.origin}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Fecha',
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'  
                    }
                ]
            };
            
          
         	reportsItemsServices.reports.getFaultyItems().$promise.then((dataReturn) => {
         		  $scope.faultyItemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function viewAfterSale(row){
              $scope.afterData = row;
              var modalInstance  = $modal.open({
                      template: require('../../../sales/after-sale/view/after-sale-view.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'afterSaleViewController',
                      controllerAs: 'afterSaleView',
                      size: 'lg'
              })
            }

            function searchData() {
              reportsItemsServices.reports.getFaultyItems().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.faultyItemsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

itemsFaultyListController.$inject = ['$scope','UserInfoConstant','$timeout','reportsItemsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

