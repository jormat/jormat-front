/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> userSalesController
* # As Controller --> userSales														
*/
  export default class userSalesController {

    constructor($scope, $filter,$rootScope, $http,salesReportServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
        vm.loadInvoices = loadInvoices
        vm.setName = setName 
        vm.viewDocument = viewDocument
        vm.loadRentability = loadRentability
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
	   
        // loadInvoices()

        salesReportServices.users.getUsers().$promise.then((dataReturn) => {
                $scope.users = dataReturn.data;
                console.log('users',$scope.users);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        i18nService.setCurrentLang('es');

        $scope.invoicesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'reporte_ventas_usuario.csv',
                exporterPdfHeader: { 
                    text: "Reporte emitido emitidas entre los días : " + moment($scope.startDate).format("YYYY-MM-DD") +" y "+ moment($scope.endDate).format("YYYY-MM-DD"),
                    // text: "Reporte facturas por fecha", 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [0,20,0,0] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },
                columnDefs: [
                    
                    
                    { 
                      name:'N° documento',
                      field: 'documentId',  
                      width: '11%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase" style="font-size: 15px;" ng-click="grid.appScope.userSales.viewDocument(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<span style="font-size: 12px;" class="label bg-warning">{{row.entity.type}}</span>' +
                                 '</div>'
                    },
                    { 
                      name: 'Cliente', 
                      field: 'clientName', 
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<span>{{row.entity.clientName | uppercase}}</span>' +
                                 '</div>'
                    },
                    
                    { 
                      name:'Total',
                      field: 'total',
                      width: '12%',
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p style="font-size: 15px;"><strong> ${{row.entity.total}}</strong></p>' +
                                 '</div>'
                    },

                    
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '13%'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 11px;" class="label bg-secondary">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    }
                ]
            }


        function setName(model){
            $scope.userId = model.id
            $scope.userName = model.name
        }

        function loadRentability(model){
            let data = {
                userId : $scope.userId,
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            console.log('load new reporte');

                var modalInstance  = $modal.open({
                        template: require('./profitability-sales/profitability-sales.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'profitabilitySalesController',
                        controllerAs: 'profitabilitySales',
                        size: 'lg'
                })
        }


        function viewDocument(row){
                $scope.invoiceData = row;

                if ($scope.invoiceData.type == "Factura") {

                    $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName
                    }
                    console.log('ver factura');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.type == "Documento NN") { 

                    $scope.invoiceData = {
                        documentId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName
                    }
                    console.log('ver documento nn');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/documents-nn/view/documents-nn-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'documentsNnViewController',
                            controllerAs: 'documentsNnView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.type == "Nota Credito") { 

                    $scope.creditNoteData = {
                        creditNoteId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName 
                    }
                    console.log('ver nota credito nn');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/credit-notes/view/credit-notes-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'creditNotesViewController',
                            controllerAs: 'creditNotesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.type == "Boleta") { 

                    $scope.ballotsData = {
                        ballotId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName
                    }
                    console.log('Boleta');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/ballots/view/ballots-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }

            }


        function loadInvoices(){

            let model = {
                userId : $scope.userId,
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            salesReportServices.reports.getUserSales(model).$promise.then((dataReturn) => {
              $scope.invoicesGrid.data = dataReturn.data

              if ($scope.invoicesGrid.data.length == 0){
                      ngNotify.set('Las fechas ingresadas no poseen documentos asociadas ','warn')
                  }else{
                      console.log('fechas con data');
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }
    }

  }

  userSalesController.$inject = ['$scope', '$filter','$rootScope', '$http','salesReportServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
