/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> profitabilitySalesController
* # As Controller --> profitabilitySales														
*/
  export default class profitabilitySalesController {

    constructor($scope, $filter,$rootScope, $http,salesReportServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService,$modalInstance,staffServices) {

    	var vm = this
        vm.setName = setName
        vm.calculate = calculate 
        vm.cancel = cancel
        vm.viewClientDetails = viewClientDetails
        vm.showMesaggeNotData = showMesaggeNotData
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
	   
        salesReportServices.users.getUsers().$promise.then((dataReturn) => {
                $scope.users = dataReturn.data;
                console.log('users',$scope.users);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        function setName(model){

            $scope.userId = model.id
            $scope.userName = model.lastName
            $scope.id = model.id


            let params = {
            userId: model.id
            }
          

            staffServices.users.getUsersDetails(params).$promise.then((dataReturn) => {
                      $scope.user = dataReturn.data
                      $scope.userDetails  = { 
                           rut : $scope.user[0].rut,
                           fullName : $scope.user[0].fullName
                      }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            staffServices.users.getUserWarehouses(params).$promise.then((dataReturn) => {
                  $scope.warehouses = dataReturn.data

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

        i18nService.setCurrentLang('es');

      

        function showMesaggeNotData(model){

                $scope.messageInvoices = true
                  ngNotify.set( 'No existes documentos emitidos entre '+ model.startDate +' y '+ model.endDate , {
                  sticky: true,
                  button: true,
                  type : 'warn'
                })
        }

        function viewClientDetails(row){ 

            var url = $state.href("app.invoicesByClient",{ 
                clientId: row.clientId,
                startDate:moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate:moment($scope.endDate).format("YYYY-MM-DD").toString()
            });

            window.open(url,'_blank',"width=600,height=700");
        }


        function calculate(){
            console.log(' calculate porcentaje')

            let model = {
                userId : $scope.userId,
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            var startDate = moment($scope.startDate)
            var endDate= moment($scope.endDate)
            
            $scope.days = endDate.diff(startDate, 'days')

            salesReportServices.reports.getNetSalesUser(model).$promise.then((dataReturn) => {
              $scope.netSales = dataReturn.data
              $scope.netSalesValue = $scope.netSales[0].suma

              if ($scope.netSales[0].suma == null){showMesaggeNotData(model) }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })


            salesReportServices.reports.getSellingCostsUser(model).$promise.then((dataReturn) => {
              $scope.SellingCosts = dataReturn.data
              $scope.SellingCostsUser = $scope.SellingCosts[0].suma
              console.log('$scope.SellingCosts',$scope.SellingCosts)
              console.log('value',$scope.SellingCostsUser)

              // if ($scope.SellingCosts.length == 0){showMesaggeNotData(model)}

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            // $scope.contributionMargin = $scope.netSales[0].suma - $scope.sellingCosts[0].suma
        }

        function cancel(){
                $modalInstance.dismiss('chao');
           }

	    function print() {
			window.print();
		}



    }

  }

  profitabilitySalesController.$inject = ['$scope', '$filter','$rootScope', '$http','salesReportServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService','$modalInstance','staffServices'];
