/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> incomeStatementController
* # As Controller --> incomeStatement														
*/
  export default class incomeStatementController {

    constructor($scope, $filter,$rootScope, $http,salesReportServices,warehousesServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
        vm.loadReport = loadReport
        vm.setName = setName
        vm.print = print
        vm.valuesLoad = valuesLoad
        vm.newAction = newAction
        $scope.buttonShowResult = true 
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")

        $scope.contributionMargin = 0
        $scope.profitBeforeTax = 0
        $scope.incomeTax = 0
        $scope.utility = 0
        $scope.buttonShowResult = true
	   
        function setName(value){

            if (value == 1) { $scope.warehouseName = "Los Ángeles"}
            if (value == 2) { $scope.warehouseName = "Chillán"}
            if (value == 3) { $scope.warehouseName = "Constitución"}
            if (value == 4) { $scope.warehouseName = "Temuco"}
        }

        function newAction(){
            $scope.buttonShowResult = true
            $scope.loadButton = false
        }

        function loadReport(){

            
            let model = {
                warehouseId : $scope.warehouseId,
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            console.log('.modelo',model)

            // servicio llama ventas neto
            salesReportServices.reports.getNetSales(model).$promise.then((dataReturn) => {
              $scope.netSales = dataReturn.data
              $scope.resultPanel = true
              $scope.loadButton = true
              $scope.buttonShowResult = false

              },(err) => {
                  console.log('No se ha podido conectar con el servicio getNetSales',err);
              })

            // servicio llama costos de venta 
            salesReportServices.reports.getSellingCosts(model).$promise.then((dataReturn) => {
              $scope.sellingCosts = dataReturn.data

              },(err) => {
                  console.log('No se ha podido conectar con el servicio getSellingCosts',err);
              })

            // servicio llama otros gastos 
            salesReportServices.reports.getOtherExpenses(model).$promise.then((dataReturn) => {
              $scope.otherExpenses = dataReturn.data

              },(err) => {
                  console.log('No se ha podido conectar con el servicio getOtherExpenses',err);
              })

            salesReportServices.reports.getOtherExpensesList(model).$promise.then((dataReturn) => {
              $scope.otherExpensesList = dataReturn.data
              console.log('list costos',$scope.otherExpensesList);

              $scope.sumExpence = 0
              var i = 0
                     for (i=0; i<$scope.otherExpensesList.length; i++) {
                         $scope.sumExpence = $scope.sumExpence + $scope.otherExpensesList[i].suma  
                      }
              console.log('suma costos',
                );

              },(err) => {
                  console.log('No se ha podido conectar con el servicio getOtherExpensesList',err);
              })

            $scope.contributionMargin = 0
            $scope.profitBeforeTax = 0
            $scope.incomeTax = 0
            $scope.utility = 0
            

        }

        function valuesLoad() {

            $scope.contributionMargin = $scope.netSales[0].suma - $scope.sellingCosts[0].suma
            // $scope.profitBeforeTax = Math.round($scope.contributionMargin - $scope.otherExpenses[0].suma)
            $scope.profitBeforeTax = Math.round($scope.contributionMargin - $scope.sumExpence)
            $scope.incomeTax = Math.round($scope.profitBeforeTax * (27/100))
            $scope.utility = Math.round($scope.profitBeforeTax - $scope.incomeTax)
              
            }

        function print() {
              window.print();
            }

	  

    }

  }

  incomeStatementController.$inject = ['$scope', '$filter','$rootScope', '$http','salesReportServices','warehousesServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
