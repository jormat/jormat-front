/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> salesReportServices
* file for call at services for providers
*/

class salesReportServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var reports = $resource(SERVICE_URL_CONSTANT.jormat + '/reports/:route',{},{
      
        getUserSales: {
          method:'GET',
          params : {
            route: 'user-sales'
            }
          },

        getWarehouseSales: {
          method:'GET',
          params : {
            route: 'warehouse-sales'
            }
          },

        getNetSales: {
          method:'GET',
          params : {
            route: 'net-sales'
            }
          },

        getSellingCosts: {
          method:'GET',
          params : {
            route: 'selling-costs'
            }
          },

        getOtherExpenses: {
          method:'GET',
          params : {
            route: 'other-expenses'
            }
          },

        getOtherExpensesList: {
          method:'GET',
          params : {
            route: 'other-expenses-list'
            }
          },

          getNetSalesUser: {
              method:'GET',
              params : {
                route: 'net-sales-user'
                }
              },

          getSellingCostsUser: {
              method:'GET',
              params : {
                route: 'selling-costs-user'
                }
          }
    })

    var users = $resource(SERVICE_URL_CONSTANT.jormat + '/users/:route',{},{
      
        getUsers: {
          method:'GET',
          params : {
            route: 'getUsers'
            }
          }
    })

    return { reports : reports,
             users : users
             
           }
  }
}

  salesReportServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.salesReportServices', [])
  .service('salesReportServices', salesReportServices)
  .name;