
routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default function routes($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode(true);
      //$urlRouterProvider.otherwise('/');

  $stateProvider
     .state('app.itemsSold', {
            abstract: false,
            url: '/reportes/items-vendidos',
            template: require('./items/sold/items-sold-list.html'),
            controller: 'itemsSoldListController',
            controllerAs:'itemsSoldList'
      })
     .state('app.itemsOutputs', {
            abstract: false,
            url: '/reportes/items-despachados',
            template: require('./items/outputs/items-outputs-list.html'),
            controller: 'itemsOutputsListController',
            controllerAs:'itemsOutputsList'
      })
     .state('app.itemsPurchased', {
            abstract: false,
            url: '/reportes/items-comprados',
            template: require('./items/purchased/items-purchased-list.html'),
            controller: 'itemsPurchasedListController',
            controllerAs:'itemsPurchasedList'
      })

     .state('app.inventory', {
            abstract: false,
            url: '/reportes/inventario-items',
            template: require('./items/inventory/items-inventory.html'),
            controller: 'itemsInventoryController',
            controllerAs:'itemsInventory'
      })

     .state('app.dailyInventory', {
            abstract: false,
            url: '/reportes/inventario-diario',
            template: require('./items/daily-inventory/daily-inventory.html'),
            controller: 'dailyInventoryController',
            controllerAs:'dailyInventory'
      })

     .state('app.warehouseInventory', {
            abstract: false,
            url: '/reportes/inventario-sucursal',
            template: require('./items/warehouse-inventory/warehouse-inventory.html'),
            controller: 'warehouseInventoryController',
            controllerAs:'warehouseInventory'
      })

     .state('app.userSales', {
            abstract: false,
            url: '/reportes/ventas-usuarios',
            template: require('./sales/user-sales/user-sales.html'),
            controller: 'userSalesController',
            controllerAs:'userSales'
      })

     .state('app.warehouseSales', {
            abstract: false,
            url: '/reportes/ventas-sucursales',
            template: require('./sales/warehouse-sales/warehouse-sales.html'),
            controller: 'warehouseSalesController',
            controllerAs:'warehouseSales'
      })

     .state('app.incomeStatement', {
            abstract: false,
            url: '/reportes/estado-resultados',
            template: require('./sales/income-statement/income-statement.html'),
            controller: 'incomeStatementController',
            controllerAs:'incomeStatement'
      })

     .state('app.faultyItems', {
            abstract: false,
            url: '/reportes/items-falla',
            template: require('./items/faulty/items-faulty-list.html'),
            controller: 'itemsFaultyListController',
            controllerAs:'itemsFaultyList'
      })

      .state('app.salesPowerBI', {
            abstract: false,
            url: '/reportes/ventas-power-bi',
            template: require('./sales/sales-power-bi/sales-power-bi.html'),
            controller: 'salesPowerBiController',
            controllerAs:'salesPowerBi'
      })

      .state('app.salesItemsPBI', {
            abstract: false,
            url: '/reportes/ventas-repuestos-pbi',
            template: require('./sales/sales-items/sales-items.html'),
            controller: 'salesItemsController',
            controllerAs:'salesItems'
      })

      .state('app.itemsNotSold', {
            abstract: false,
            url: '/reportes/items-no-vendidos',
            template: require('./items/not-sold/not-sold-pbi.html'),
            controller: 'notSoldPbiController',
            controllerAs:'notSoldPbi'
      })

     ;
 
}
