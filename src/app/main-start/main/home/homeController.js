/**
* @name APP u-Planning
* @autor over.martinez@u-planner.com
* @description
* Controlador principal
* @controller 
* # name: homeController
* # alias: home
*/

export default class homeController {

	constructor($scope, $state, $modal,$stateParams, ngNotify, UserInfoConstant) {
		var vm = this
		this.$scope = $scope
		this.$state = $state
		this.ngNotify = ngNotify
		vm.salesDailyView = salesDailyView
		this.$stateParams = $stateParams
		this.$state = $state
		this.$modal= $modal
		this.userinfo = null
		this.$scope.UserInfoConstant = UserInfoConstant
		this.$scope.picture= require("../../../../images/soporte.png");
		this.$scope.$watch('UserInfoConstant[0].details[0]', (details, before) => {
			if (details !== undefined) {
				this.userinfo = details.user
				this.main()
			}
		})

		console.log("entra controlador")

			$scope.chartConfig2 = {
				options: {
					chart: {
						type: 'area'
					}
				},
				xAxis: {
					categories: ['Lunes 01/10', 'Martes 02/10', 'Miercoles 03/10', 'Jueves 04/10', 'Viernes 05/10']
				},
				credits: {
					enabled: false
				},
				series: [{
					name: 'Los Ángeles',
					data: [1000000, 1200000, 800000,1800000, 900000]
				}, {
					name: 'Chillán',
					data: [650000, 1100000, 490000, 570000, 800000]
				}, {
					name: 'Consitución',
					data: [1100000, 1500000, 900000, 700000, 600000]
				},
				{
					name: 'Temuco',
					data: [980000, 570000, 600000, 850000, 120000]
				}],
				title: {
					text: 'Ventas por sucursal'
				},
				subtitle: {
					text: 'Porcentaje Ventas'
				},

				loading: false,
				size: {
					height: 340
				},

			}

		function salesDailyView() {
		    console.log('test')
			const $modalInstance  = $modal.open({
							template: require('../../daily-sales-report/daily-sales.html'),
							animation: true,
							scope: $scope,
							controller: 'dailySalesController',
							controllerAs: 'dailySales',
							size: 'lg'
					})
		}


	}

	

}

homeController.$inject = ['$scope', '$state', '$modal','$stateParams', 'ngNotify', '$timeout', 'UserInfoConstant']