/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> dailySalesController
* # As Controller --> dailySales														
*/
  export default class dailySalesController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,itemsServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
	    vm.cancel = cancel
        // vm.viewItem =viewItem
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
	   
        // loadInvoices()

        i18nService.setCurrentLang('es');

        itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        $scope.itemsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'stock_items_proveedor.csv',
                exporterMenuPdf: false,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'itemId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a style="font-size: 12px;" ng-click="grid.appScope.itemsProvider.viewItem(row.entity)"><strong>{{row.entity.itemId}}</strong></a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;"><strong>{{row.entity.references}}</strong></p>' +
                                 '</div>'  
                    },
                    { 
                      name: '$/Iva', 
                      field: 'vatPrice', 
                      enableFiltering: false,
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p style="font-size: 11px;"><strong>${{row.entity.vatPrice | pesosChilenos}}</strong></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Marca',
                      field: 'brand',
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 9px;">{{row.entity.brand}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cod. Marca',
                      field: 'brandCode',
                      width: '11%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 9px;">{{row.entity.brandCode}}</p>' +
                                 '</div>' 
                    },
                    {
                      name:'Proveedor',
                      field: 'providers',
                      width: '16%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="uppercase label bg-warning" style="font-size: 10px;">{{row.entity.providers}}</p>' +
                                 '</div>' 
                    

                    }, 
                    
                    
                      
                    { 
                      name: 'Cant.', 
                      field: 'generalStock', 
                      enableFiltering: false,
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-accent" style="font-size: 12px;"><strong>{{row.entity.generalStock}}</strong></p>' +
                                 '</div>' 
                    }
                ]
            }


	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}





    }

  }

  dailySalesController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','itemsServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
