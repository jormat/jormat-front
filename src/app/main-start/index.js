// import uirouter from 'angular-ui-router';
import routing from './main.route';
import run from './main.run';

// Definition for controllers & services home module'
import homeController from './main/home/homeController'
import dailySalesController from './daily-sales-report/dailySalesController'


export default angular.module('app.homeSuite', [])
.config(routing)

.controller('homeController', homeController)
.controller('dailySalesController', dailySalesController)

// Sidebar Construct
.run(run)
.constant("main", [{
	'title' : 'Inicio',
	'icon'  : 'mdi-home',
	'url'   : 'app.homeSuite'
}])
.name;
