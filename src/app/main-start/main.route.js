routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$translateProvider']
export default function routes($stateProvider, $urlRouterProvider, $locationProvider, $translateProvider) {
        $locationProvider.html5Mode(true)
        let config = require('../../../local.env.json')
        var clientName=config.clientName;
        config.AVAILABLE_LANG = config.AVAILABLE_LANG || ["es"]
        console.log(config);

        let translationsConfig = {}
        for (let i in config.AVAILABLE_LANG) {

            try {
                translationsConfig[config.AVAILABLE_LANG[i]] = require('../../lang/' + config.clientName + '_' + config.AVAILABLE_LANG[i] + '.json')
            } catch(error) {
                translationsConfig[config.AVAILABLE_LANG[i]] = require('../../lang/uplanner_' + config.AVAILABLE_LANG[i] + '.json')
            }

        }

        for (let lang in translationsConfig) {
            $translateProvider.translations(lang, translationsConfig[lang]);
        }

        $translateProvider.preferredLanguage('es');

        $urlRouterProvider.otherwise(config.HOMEPAGE)

		$stateProvider
        .state('app.homeSuite', {
            abstract: false,
            url: '/suite/inicio',
            template: require('./main/home/home-'+clientName+'.html'),
            controller: 'homeController',
            controllerAs:'home'
        })

}
