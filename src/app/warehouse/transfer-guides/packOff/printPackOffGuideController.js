/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> printPackOffGuideController
* # As Controller --> printPackOffGuide														
*/


  export default class printPackOffGuideController {

    constructor($scope, $filter,$rootScope, $http,transferGuidesServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.transferGuideId = $stateParams.idGuide

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        let params = {
                transferGuideId: $scope.transferGuideId
            }

        transferGuidesServices.guides.getGuideDetails(params).$promise.then((dataReturn) => {
              $scope.guide = dataReturn.data
              $scope.guideDetails  = { 
                    clientName : $scope.guide[0].clientName,
                    rut : $scope.guide[0].rut,
                    type : $scope.guide[0].type,
                    phone : $scope.guide[0].phone,
                    address : $scope.guide[0].address,
                    city : $scope.guide[0].city,
                    destination:$scope.guide[0].destination

                  }
              console.log('boaita',$scope.guideDetails);    
                            
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get guides Details',err);
              })

    	function cancel() {

		 window.close();
         
		}


        function print() {
              window.print();

        }


    }

  }

  printPackOffGuideController.$inject = ['$scope', '$filter','$rootScope', '$http','transferGuidesServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
