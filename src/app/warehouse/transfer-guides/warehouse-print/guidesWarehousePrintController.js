/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> guidesWarehousePrintController
* # As Controller --> guidesWarehousePrint														
*/


  export default class guidesWarehousePrintController {

    constructor($scope, $filter,$rootScope, $http,transferGuidesServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.validateGuide=validateGuide
        vm.printPackOff = printPackOff
        vm.rejectedGuide = rejectedGuide
        $scope.transferGuideId = $stateParams.idGuide

        let params = {
                transferGuideId: $scope.transferGuideId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })

        transferGuidesServices.guides.getGuideDetails(params).$promise.then((dataReturn) => {
              $scope.guides = dataReturn.data

              let model = {
                    transferGuideId : $scope.transferGuideId,
                    destination :$scope.guides[0].destinationId,
                    origin :$scope.guides[0].originId  
                }

                  transferGuidesServices.guides.getGuidesItemsLocation(model).$promise.then((dataReturn) => {
                  $scope.guidesItems = dataReturn.data
                  var subTotal = 0
                      var i = 0
                         for (i=0; i<$scope.guidesItems.length; i++) {
                             subTotal = subTotal + $scope.guidesItems[i].total  
                          }
                          
                          $scope.subTotality =  subTotal
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio getGuidesItems',err);
                  })

              },(err) => {
                  console.log('No se ha podido conectar con el servicio get guides Details',err);
              })

        

    	function cancel() {
			window.close();
		}

        function validateGuide() {
          $scope.btnValidate = true
            let model = {
                    transferGuideId : $scope.transferGuideId,
                    userId : $scope.userInfo.id,
                    destination :$scope.guides[0].destinationId,
                    itemsInvoices : $scope.guidesItems  
                }

                console.log('that',model)

                transferGuidesServices.guides.validateGuideAfterStock(model).$promise.then((dataReturn) => {

                    ngNotify.set('Ajuste stock exitoso','warn');
                    // $state.go("app.transferGuides")
                    updateStatusGuide(model)
                  },(err) => {
                      ngNotify.set('Error al generar ajuste stock guía','error')
                      $scope.btnValidate = false
                  })
        }


        function updateStatusGuide(model) {
                console.log('that',model)

                transferGuidesServices.guides.validateGuide(model).$promise.then((dataReturn) => {

                    ngNotify.set('Guía validada exitosamente','success');
                    $state.go("app.transferGuides")
                    //$modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al validar guía','error')
                      $scope.btnValidate = false
                  })
        }




        function rejectedGuide() {
            $scope.btnRejected = true
            let model = {
                    transferGuideId : $scope.transferGuideId,
                    userId : $scope.userInfo.id,
                    origin :$scope.guides[0].originId,
                    itemsInvoices : $scope.guidesItems  
                }

            transferGuidesServices.guides.rejectGuide(model).$promise.then((dataReturn) => {
                    ngNotify.set('Guia rechazada correctamente','success');
                    $state.go("app.transferGuides")
                    // $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al rechazar guía','error')
                      $scope.btnRejected = false
                  })
              
        }

        function print() {
              window.print();

        }

        function printPackOff() {
            console.log('imprimir PackOff');
            var url = $state.href("app.printPackOffGuide",{
                idGuide: $scope.transferGuideId
            });

            window.open(url,'_blank',"width=600,height=700");

        }

		


    }

  }

  guidesWarehousePrintController.$inject = ['$scope', '$filter','$rootScope', '$http','transferGuidesServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
