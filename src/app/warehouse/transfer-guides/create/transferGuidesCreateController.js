
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> transferGuidesCreateController
* # As Controller --> transferGuidesCreate														
*/

export default class transferGuidesCreateController{

        constructor($scope,UserInfoConstant,$timeout,transferGuidesServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$window,LOCAL_ENV_CONSTANT){
            var vm = this;
            this.$mdDialog = $mdDialog
            vm.createTransferGuide = createTransferGuide
            vm.createXML = createXML
            vm.validateType=validateType
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.setDestinationId = setDestinationId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.loadItemCSV = loadItemCSV
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getNet = getNet
            vm.getStock = getStock
            vm.searchClient = searchClient
            vm.loadItems = loadItems
            $scope.buttonMore = false
            $scope.parseInt = parseInt
            $scope.discount = 0
            $scope.warningItems = true
            $scope.transferButton = true
            $scope.saleButton = false
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            loadItems()

			warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
              $scope.warehousesTo = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<span ng-show="grid.appScope.transferButton"><button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.transferGuidesCreate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button></span>'+
                                   '<span ng-show="grid.appScope.saleButton" ><button ng-click="grid.appScope.transferGuidesCreate.loadItem(row.entity)" type="button" class="btn btn-success btn-xs">+</button></span>'+
                                   '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.transferGuidesCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '44%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}">{{row.entity.itemDescription}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                      } 
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '39%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                      }
                    }
                ]
            };
            

            function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            $scope.invoiceItems = []

            function loadItem(row){

              var i = 0
              for (i=0; i<$scope.invoiceItems.length; i++) {
                    var id = $scope.invoiceItems[i].itemId

                    if(id == row.itemId){
                      ngNotify.set('Item '+ row.itemId +' Duplicado en este Documento','warn')
                      $scope.invoiceItems.splice(index, 1)

                    }else{
                      console.log("item no puplicado",row.itemId)
                    }
                    
                  }
                  
                 $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription + '('+ row.brandCode+ ')',
                    price: row.netPrice,
                    minPrice: row.netPrice,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0
                });
                 getStock(row.itemId,1)

                 $scope.warningItems = false    
            }

            function loadItemCSV(row){
                 $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    referenceKey : row.referenceKey,
                    price: Number(row.netPrice),
                    oil: row.oil,
                    minPrice: Number(row.netPrice),
                    maxDiscount : 0,
                    quantity: Number(row.quantity),
                    disscount: 0

                });

                 getStock(row.itemId,row.quantity)
                 $scope.warningItems = false    
            }

            function getStock(itemId,quantity){
            
              let params = {
                itemId: itemId,
                warehouseId :$scope.warehouseId 
            }

            itemsServices.items.getStock(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data
              $scope.stock = $scope.item[0].stock
              $scope.notStock = 0
              $scope.warningQuantity = false
              $scope.buttonMore = false 
              if (quantity > $scope.stock) {

                ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock , {
                  sticky: true,
                  button: true,
                  type : 'warn'
              })
                $scope.notStock = 1
                $scope.warningQuantity = true
                $scope.buttonMore = true 

                
              }
              
              return false;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
    
            }

            function getNet() {
                var net = 0
                  var i = 0
                     for (i=0; i<$scope.invoiceItems.length; i++) {
                         var valueItems = $scope.invoiceItems[i] 
                         net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 

                      }

                      $scope.NET =  net
                      $scope.netoInvoice = net - (($scope.discount*net)/100) 
                      $scope.total = Math.round($scope.netoInvoice * 1.19)
                      $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                      return net;
                      
            }


            function removeItems(index) {

                $scope.invoiceItems.splice(index, 1)
                $scope.warningQuantity = false
                $scope.buttonMore = false

                var i = 0
                 for (i=0; i<$scope.invoiceItems.length; i++) {
                      var id = $scope.invoiceItems[i].itemId
                      var stockId = $scope.invoiceItems[i].quantity
                      console.log("item x",id,stockId)
                      getStock(id,stockId)
                    }
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                if (warehouseId == null) {
                    $scope.UserInfoConstant = UserInfoConstant
                    $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.warehouseId = $scope.warehouses[0].warehouseId
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            }else{
                $scope.warehouseId = warehouseId
             }
            }

            function setDestinationId(warehouseId){
                $scope.destinationId = warehouseId
            }

            function validateType(type){
            $scope.clientName = null
            $scope.clientId = null
            $scope.selectedRut = null                
            $scope.typeGuide = type
                if (type == 0) {
                    $scope.destination = true
                    $scope.saleButton = false
                    $scope.transferButton = true
                    $scope.panelInvoice = false

                }else{
                    $scope.destination = false
                    $scope.transferButton = false
                    $scope.saleButton = true

                }
            }

            function showPanel(value) {

                if (value == 1) {
                    if ($scope.typeGuide == 1 ) {
                        $scope.panelInvoice = true
                        $scope.invoiceItems = []
                        $scope.warningQuantity = false

                    }else{
                        $scope.panelInvoice = false
                    }
                }else{
                    if ($scope.warehouseId == $scope.destinationId) {
                        ngNotify.set('El destino no puede ser igual al origen','error')
                        $scope.panelInvoice = false
                    }else{
                       $scope.panelInvoice = true
                       $scope.invoiceItems = []
                       $scope.warningQuantity = false
                    }
                    
                }

            }

            function setName(clients) {
                console.log('please',clients)
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedRut = clients.rut
            }

            function createTransferGuide(value) {

                        let model = {
                            userId: $scope.userInfo.id,
                            clientId : $scope.clientId,
                            originId : $scope.warehouseId,
                            destinationId : $scope.destinationId,
                            type: Number($scope.type),
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            comment : $scope.observation,
                            itemsInvoices : $scope.invoiceItems  
                        }      
                        console.log('modelo',model,value) 

                        if (value == 1) {

                            transferGuidesServices.guides.createGuide(model).$promise.then((dataReturn) => {
                                ngNotify.set('Se ha creado la guía de traslado correctamente','success')
                                $scope.result = dataReturn.data
                                console.log('result',$scope.result)
                                createXML($scope.result)
                                $state.go('app.transferGuides')
                              },(err) => {
                                  ngNotify.set('Error al crear guía','error')
                              })

                        }else{ 
                            console.log('entra tipo de guia venta',value) 
                            transferGuidesServices.guides.createGuideSales(model).$promise.then((dataReturn) => {
                                ngNotify.set('Se ha creado la guía de venta correctamente','success')
                                $scope.result = dataReturn.data
                                console.log('result',$scope.result)
                                createXML($scope.result)
                                $state.go('app.transferGuides')
                              },(err) => {
                                  ngNotify.set('Error al crear guía de venta','error')
                              }) 

                        }    

                        
            }


            function createXML(transferGuideId) { 

              // $window.open('http://localhost/jormat-system/XML_guide.php?guideId='+transferGuideId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
              $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_guide.php?guideId='+transferGuideId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");  
              ngNotify.set('Documento enviado a SII N° GUI ' +transferGuideId+ ' / Validar en Facturas Chile','warn')
 
            }

            function searchClient (value) {

                let model = {
                    name: value
                }

                clientsServices.clients.getClientsByName(model).$promise.then((dataReturn) => {
                      $scope.clients = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

                return false;
              }
           
        }//FIN CONSTRUCTOR

        // Funciones
        
        uploadCsv() {

          console.log('enta fuction');
           this.confirm = this.$mdDialog.confirm({
                controller         : 'uploadCsvGuidesController',
                controllerAs       : 'uploadCsv',
                template           : require('./dialogs/uploadCsv/uploadCsvGuides.html'),
                locals              : {},
                animation          : true,
                parent             : angular.element(document.body),
                clickOutsideToClose: true,
            })
            this.$mdDialog.show(this.confirm).then((row) => {
                if (row) {
                   const dataReturn = row.data
                   if(dataReturn.length < 2) {
                       this.loadItemCSV(dataReturn[0])
                   }else {
                       for(const i in dataReturn){
                           this.loadItemCSV(dataReturn[i])
                       }
                   }
                   
                }
            })
        }
 

        
    }   

transferGuidesCreateController.$inject = ['$scope','UserInfoConstant','$timeout','transferGuidesServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$window','LOCAL_ENV_CONSTANT'];

