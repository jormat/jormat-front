
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> guidesViewController
* # As Controller --> guidesViewView														
*/


  export default class guidesViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,transferGuidesServices,$location,UserInfoConstant,ngNotify,$state,itemsServices) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.validateGuide=validateGuide
        vm.rejectedGuide = rejectedGuide
        vm.printPackOff = printPackOff 
        $scope.transferGuideId = $scope.guideData.transferGuideId
        $scope.clientName = $scope.guideData.clientName

        let params = {
                transferGuideId: $scope.transferGuideId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })

        transferGuidesServices.guides.getGuideDetails(params).$promise.then((dataReturn) => {
              $scope.guides = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get guides Details',err);
              })

        transferGuidesServices.guides.getGuidesItems(params).$promise.then((dataReturn) => {
              $scope.guidesItems = dataReturn.data
              var subTotal = 0
                  var i = 0
                     for (i=0; i<$scope.guidesItems.length; i++) {
                         subTotal = subTotal + $scope.guidesItems[i].total  
                      }
                      
                      $scope.subTotality =  subTotal
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getGuidesItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function validateGuide() {

          $scope.btnValidate = true
            let model = {
                    transferGuideId : $scope.transferGuideId,
                    userId : $scope.userInfo.id,
                    destination :$scope.guides[0].destinationId,
                    itemsInvoices : $scope.guidesItems  
                }

                console.log('that',model)

                transferGuidesServices.guides.validateGuideAfterStock(model).$promise.then((dataReturn) => {

                    ngNotify.set('Ajuste stock exitoso','warn');
                    // $state.reload("app.transferGuides")
                    // $modalInstance.dismiss('chao');
                    updateStatusGuide(model)

                    if($scope.guides[0].type == "Traslado"){

                        for(var  i in model.itemsInvoices){
                            let modelo = {
                                itemId: model.itemsInvoices[i].itemId,
                                originId:$scope.guides[0].originId,
                                quantity: model.itemsInvoices[i].quantityItems
                             }
            
                                itemsServices.items.getHistoric(modelo).$promise.then((dataReturn) => {
                                   $scope.item = dataReturn.data;

                                   let itemsValue = {
                                        documentId: $scope.transferGuideId,
                                        itemId: $scope.item[0].itemId,
                                        itemDescription: $scope.item[0].itemDescription,
                                        location : $scope.item[0].locations,
                                        previousStock: $scope.item[0].stock + modelo.quantity,
                                        price: $scope.item[0].vatPrice,
                                        quantity: modelo.quantity,
                                        originId: $scope.guides[0].originId,
                                        userId: $scope.userInfo.id,
                                        document: "GUI",
                                        type: "Salida -",
                                        generalStock: $scope.item[0].generalStock,
                                        currentprice: $scope.item[0].vatPrice,
                                        currentLocation: $scope.item[0].locations,
                                        destiny: $scope.guides[0].destinationId
                                    }
                                        
                                    console.log('itemsValue', itemsValue);

                                    itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                                        $scope.result2 = dataReturn.data;
                                        console.log('Historial del item actualizado correctamente');
                                    },(err) => {
                                        console.log('No se ha podido conectar con el servicio',err);
                                    })


                                    },(err) => {
                                          console.log('No se ha podido conectar con el servicio',err);
                                    })
                        }

                    }
                  },(err) => {
                      ngNotify.set('Error al validar guía','error')
                      $scope.btnValidate = false
                  })
        }


        function updateStatusGuide(model) {
                console.log('that',model)

                transferGuidesServices.guides.validateGuide(model).$promise.then((dataReturn) => {

                    ngNotify.set('Guía validada exitosamente','success');
                    $state.reload("app.transferGuides")
                    $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al validar guía','error')
                      $scope.btnValidate = false
                  })
        }

        function rejectedGuide() {
            $scope.btnRejected = true
            let model = {
                    transferGuideId : $scope.transferGuideId,
                    userId : $scope.userInfo.id,
                    origin :$scope.guides[0].originId,
                    itemsInvoices : $scope.guidesItems  
                }

                if($scope.guides[0].type == "Traslado"){

                    console.log('tipo Traslado',$scope.guides[0].type);
                     
                    transferGuidesServices.guides.rejectGuide(model).$promise.then((dataReturn) => {
                    ngNotify.set('Guia rechazada correctamente','success');
                    $modalInstance.dismiss('chao');
                     $state.reload("app.transferGuides")
                   },(err) => {
                       ngNotify.set('Error al rechazar guía','error')
                       $scope.btnRejected = false
                   })
 
 
                 }else{
                     
                     console.log('tipo Ventas',$scope.guides[0].type);
                     transferGuidesServices.guides.guideDecline(model).$promise.then((dataReturn) => {
                     ngNotify.set('Guia rechazada correctamente','success');
                     $modalInstance.dismiss('chao');
                     $state.reload("app.transferGuides")
                     },(err) => {
                       ngNotify.set('Error al rechazar guía','error')
                       $scope.btnRejected = false
                   })
 
 
                 }
              
        }

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

        function printPackOff() {
            console.log('imprimir PackOff');
            var url = $state.href("app.printPackOffGuide",{
                idGuide: $scope.transferGuideId
            });

            window.open(url,'_blank',"width=600,height=700");

        }

		


    }

  }

  guidesViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','transferGuidesServices','$location','UserInfoConstant','ngNotify','$state','itemsServices'];
