/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> transferGuidesServices
* file for call at services for transferGuides
*/

class transferGuidesServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var guides = $resource(SERVICE_URL_CONSTANT.jormat + '/guides/transfer/:route',{},{
      
        getGuides: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getGuideDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createGuide: {
            method:'POST',
            params : {
                route: ''
            }
        },
        disabledGuide: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        validateGuide: {
            method:'PUT',
            params : {
                route: 'validate'
            }
        },
        validateGuideAfterStock: {
            method:'PUT',
            params : {
                route: 'stock'
            }
        },
        rejectGuide: {
            method:'PUT',
            params : {
                route: 'rejected'
            }
        },
        createGuideSales: {
            method:'POST',
            params : {
                route: 'sales'
            }
        },
        getGuidesItems: {
            method:'GET',
            params : {
               route: 'items'
            }
        },
        getGuidesItemsLocation: {
            method:'GET',
            params : {
               route: 'items-locations'
            }
        },
        guideDecline: {
            method:'PUT',
            params : {
                route: 'decline'
            }
        }
    })

    return { guides : guides
             
           }
  }
}

  transferGuidesServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.transferGuidesServices', [])
  .service('transferGuidesServices', transferGuidesServices)
  .name;