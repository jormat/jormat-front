
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> transferGuidesListController
* # As Controller --> transferGuidesList														
*/

export default class transferGuidesListController{

        constructor($scope,UserInfoConstant,$timeout,transferGuidesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,$window,LOCAL_ENV_CONSTANT,i18nService){
            var vm = this
            vm.viewGuide = viewGuide
            vm.validateGuide= validateGuide
            vm.deleteGuide = deleteGuide
            vm.createXML = createXML
            vm.searchData=searchData
            vm.printGuide= printGuide
            vm.loadGuides = loadGuides
            vm.transformToInvoice = transformToInvoice
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }


            loadGuides()
            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.guidesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'transferGuideId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.transferGuidesList.viewGuide(row.entity)">{{row.entity.transferGuideId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> $ {{row.entity.total}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '9%'
                    },
                    { 
                      name:'Destino',
                      field: 'destination',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.destination}}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Solicitud', 
                      field: 'orderId', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<p> {{row.entity.orderId}}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<span class="{{(row.entity.statusName == \'Rechazada\')?\'label bg-danger\':\'label bg-warning\'}}">{{row.entity.statusName}}</span>'+
                                 '</div>'
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" badge bg-secondary font-italic" style="font-size: 12px;">{{row.entity.type}}</p>' +
                                 '</div>'   
                    },
                    
                    { 
                      name: 'Acciones',
                      width: '8%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadGuides(argument) {
          
            transferGuidesServices.guides.getGuides().$promise.then((dataReturn) => {
              $scope.guidesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }


            function searchData() {
              transferGuidesServices.guides.getGuides().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.guidesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewGuide(row){
	            $scope.guideData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/guides-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'guidesViewController',
	                    controllerAs: 'guidesView',
	                    size: 'lg'
	            })
	        }

	        function printGuide(row){
	          console.log('estalla',row)
              $state.go("app.transferGuidesPrint", { idGuide: row.transferGuideId});
            }

          function createXML(row) { 
              // $window.open('http://localhost/jormat-system/XML_guide.php?guideId='+row.transferGuideId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
              $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_guide.php?guideId='+row.transferGuideId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");  
            }

            function deleteGuide(row){
              $scope.guideData = row;
              var modalInstance  = $modal.open({
                      template: require('../delete/guides-delete.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'guidesDeleteController',
                      controllerAs: 'guidesDelete'
              })
            }

            function validateGuide(row){
              $scope.guideData = row;
              var modalInstance  = $modal.open({
                      template: require('../validate/guides-validate.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'guideValidateController',
                      controllerAs: 'guideValidate'
              })
            }


            function transformToInvoice(row) {
              console.log("riw",row)
              $state.go("app.clientsInvoicesUpdate", { idInvoice: row.transferGuideId, discount:0,type:'guia'});

            }


            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

transferGuidesListController.$inject = ['$scope','UserInfoConstant','$timeout','transferGuidesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','$window','LOCAL_ENV_CONSTANT','i18nService'];

