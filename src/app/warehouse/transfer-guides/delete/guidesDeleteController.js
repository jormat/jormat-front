/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> guidesDeleteController
* # As Controller --> guidesDelete														
*/


  export default class guidesDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,transferGuidesServices,$location,UserInfoConstant,ngNotify) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.deleteGuide = deleteGuide
	    $scope.transferGuideId = $scope.guideData.transferGuideId
	    $scope.clientName = $scope.guideData.clientName

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteGuide() {
			let model = {
                    transferGuideId : $scope.transferGuideId,
                    userId : $scope.userInfo.id
                }

                transferGuidesServices.guides.disabledGuide(model).$promise.then((dataReturn) => {

                    ngNotify.set('Guía eliminada exitosamente','success');
					         $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al eliminar guía','error')
                  })
		}
	    


    }

  }

  guidesDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','transferGuidesServices','$location','UserInfoConstant','ngNotify'];
