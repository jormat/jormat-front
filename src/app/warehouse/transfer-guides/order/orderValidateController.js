
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> orderValidateController
* # As Controller --> orderValidate														
*/

export default class orderValidateController{

        constructor($scope,UserInfoConstant,$timeout,orderRequestServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element,$window,$stateParams,quotationsServices,transferGuidesServices){
            var vm = this;
            vm.createGuide = createGuide
            vm.validateOrder = validateOrder
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.setName = setName
            vm.removeItems = removeItems
            vm.getStock = getStock
            vm.getNet = getNet
            vm.loadOrder = loadOrder
            vm.loadItems = loadItems
            $scope.orderCode= $stateParams.orderId 
            $scope.buttonInvoice = true
            $scope.parseInt = parseInt
            $scope.buttoncreateGuide = true
            $scope.discount = parseInt($stateParams.discount)
            $scope.type = $stateParams.type
            $scope.discount = 0
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }
            $scope.notStock = 0
            $scope.warningQuantity = false
            $scope.buttonMore = false
            $scope.invoiceItems = []
            $scope.observation = "Guía basada en solicitud de pedido N° " + $scope.orderCode
            

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                    }
                })
            //

            warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
              $scope.warehouses = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

            loadItems()
            loadOrder()

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.orderValidate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                   '</div>'
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.orderValidate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '44%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '39%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  
                    }
                ]
            }
            
          
            function loadItems(){
          
            let params = {
                    warehouseId: $stateParams.originId
                }

                itemsServices.items.getGeneralStock(params).$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            function setName(clients) {
                console.log('please',clients)
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedRut = clients.rut
            }

            function loadItem(row){
                 $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    referenceKey: row.referenceKey,
                    brand: row.brand,
                    price: row.netPrice,
                    minPrice: row.netPrice,
                    stock: row.stock,
                    generalStock: row.generalStock,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                })
                 getStock(row.itemId,1)

                 $scope.warningItems = false    
            }


            function loadOrder(){

                let params = {
                    orderId: $stateParams.orderId
                  }
          
            orderRequestServices.orders.getOrderDetails(params).$promise.then((dataReturn) => {
                  $scope.orderDetails = dataReturn.data

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            let model = {
                    orderId: $stateParams.orderId,
                    originId: $stateParams.originId,
                    destinationId: $stateParams.destinationId
                }

                orderRequestServices.orders.getOrderItems(model).$promise.then((dataReturn) => {
                      $scope.itemsOrders = dataReturn.data
                      for(var i in $scope.itemsOrders){
                            $scope.invoiceItems.push({
                                itemId: $scope.itemsOrders[i].itemId,
                                itemDescription: $scope.itemsOrders[i].itemDescription,
                                referenceKey: $scope.itemsOrders[i].referenceKey,
                                price: $scope.itemsOrders[i].price,
                                brand: $scope.itemsOrders[i].brand,
                                minPrice: $scope.itemsOrders[i].price,
                                stock : $scope.itemsOrders[i].stock,
                                quantity: $scope.itemsOrders[i].quantityItems,
                                generalStock: $scope.itemsOrders[i].stockGeneral
                         })
                            getStock($scope.itemsOrders[i].itemId,$scope.itemsOrders[i].quantityItems)

                    }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get Invoices',err);
                    })

            }

            
            function getStock(itemId,quantity){
            
              let params = {
                itemId: itemId,
                warehouseId :$stateParams.originId 
            }

            itemsServices.items.getStock(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data
              $scope.stock = $scope.item[0].stock
              $scope.notStock = 0
              // $scope.warningQuantity = false
              // $scope.buttonMore = false

              if (quantity > $scope.stock) {

                ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock , {
                  sticky: true,
                  button: true,
                  type : 'warn'
              })
                $scope.notStock = 1
                $scope.warningQuantity = true
                $scope.buttonMore = true
                
              }
              
              return false;

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
    
            }

            function getNet() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.price * valueItems.quantity); 
                  }
                  
                  $scope.NET =  net
                  $scope.netoInvoice = net - (($scope.discount*net)/100) 
                  $scope.total = Math.round($scope.netoInvoice * 1.19)
                  $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                  return net;
                      
            }

            function removeItems(index) {
                $scope.invoiceItems.splice(index, 1)
                $scope.warningQuantity = false
                $scope.buttonMore = false
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function validateOrder(value) {
             let model = {
                        orderId : parseInt($stateParams.orderId),
                        userId : $scope.userInfo.id,
                        statusValue : value 
                    }

                console.log('that',model)

                  orderRequestServices.orders.validateOrder(model).$promise.then((dataReturn) => {
                    console.log('solicitud actualizada exitosamente');
                    $state.go('app.orders')
                  },(err) => {
                      ngNotify.set('Error al validar orden','error')
                  })
                
            }


            function createGuide() {

                        let model = {
                            userId: $scope.userInfo.id,
                            clientId : $scope.clientId,
                            originId : parseInt($stateParams.originId),
                            destinationId : parseInt($stateParams.destinationId),
                            type: 0,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            comment : $scope.observation,
                            orderId : parseInt($stateParams.orderId),
                            itemsInvoices : $scope.invoiceItems  
                        }      

                        console.log('modelo',model) 

                        transferGuidesServices.guides.createGuide(model).$promise.then((dataReturn) => {
                                ngNotify.set('Se ha creado la guía de traslado correctamente','success')
                                validateOrder(2)
                                $state.go('app.orders')
                              },(err) => {
                                  ngNotify.set('Error al crear guía','error')
                              })
                    }


        
        }//FIN CONSTRUCTOR

        // Funciones
        
    }   

orderValidateController.$inject = ['$scope','UserInfoConstant','$timeout','orderRequestServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element','$window','$stateParams','quotationsServices','transferGuidesServices'];

