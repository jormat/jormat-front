/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> guideValidateController
* # As Controller --> guideValidate														
*/

  export default class guideValidateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,transferGuidesServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.validateGuide = validateGuide
	    $scope.transferGuideId = $scope.guideData.transferGuideId
	    $scope.clientName = $scope.guideData.clientName

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function validateGuide() {
			let model = {
                    transferGuideId : $scope.transferGuideId,
                    userId : $scope.userInfo.id
                }

                transferGuidesServices.guides.validateGuide(model).$promise.then((dataReturn) => {

                    ngNotify.set('Guía validada exitosamente','success');
					         $modalInstance.dismiss('chao');
                             $state.reload('app.transferGuides')
                  },(err) => {
                      ngNotify.set('Error al validar guía','error')
                  })
		}
	    
    }

  }

  guideValidateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','transferGuidesServices','$location','UserInfoConstant','ngNotify','$state'];
