

import routing from './warehouse.route';
import run from './warehouse.run';


//warehouses files
import warehousesListController from './warehouses/list/warehousesListController';
import warehousesViewController from './warehouses/view/warehousesViewController';
import warehousesCreateController from './warehouses/create/warehousesCreateController';
import warehousesUpdateController from './warehouses/update/warehousesUpdateController';
import warehousesDeleteController from './warehouses/delete/warehousesDeleteController';
import warehousesServices from './warehouses/warehousesServices';


//warehouses files
import warehouseDeliveriesListController from './warehouse-deliveries/list/warehouseDeliveriesListController';
import warehouseDeliveriesUpdateController from './warehouse-deliveries/update/warehouseDeliveriesUpdateController';
import warehouseDeliveriesServices from './warehouse-deliveries/warehouseDeliveriesServices';

//locations files
import locationsListController from './locations/list/locationsListController';
import locationsViewController from './locations/view/locationsViewController';
import locationsCreateController from './locations/create/locationsCreateController';
import locationsUpdateController from './locations/update/locationsUpdateController';
import locationsDeleteController from './locations/delete/locationsDeleteController';
import locationsServices from './locations/locationsServices';

//transfer Guides files
import transferGuidesListController from './transfer-guides/list/transferGuidesListController';
import guidesViewController from './transfer-guides/view/guidesViewController';
import guidesPrintController from './transfer-guides/print/guidesPrintController';
import guidesWarehousePrintController from './transfer-guides/warehouse-print/guidesWarehousePrintController';
import transferGuidesCreateController from './transfer-guides/create/transferGuidesCreateController';
import guideValidateController from './transfer-guides/validate/guideValidateController';
import guidesDeleteController from './transfer-guides/delete/guidesDeleteController';
import orderValidateController from './transfer-guides/order/orderValidateController'; 
import printPackOffGuideController from './transfer-guides/packOff/printPackOffGuideController';
import uploadCsvGuidesController from './transfer-guides/create/dialogs/uploadCsv/uploadCsvGuidesController';
import transferGuidesServices from './transfer-guides/transferGuidesServices';

//Guide stock Adjustment
import stockAdjustmentListController from './stock-adjustment/list/stockAdjustmentListController';
import stockAdjustmentViewController from './stock-adjustment/view/stockAdjustmentViewController';
import stockAdjustmentCreateController from './stock-adjustment/create/stockAdjustmentCreateController';
import stockAdjustmentServices from './stock-adjustment/stockAdjustmentServices';

//orders
import ordersListController from './order-request/list/ordersListController';
import orderViewController from './order-request/view/orderViewController';
import orderCreateController from './order-request/create/orderCreateController';
import orderPrintController from './order-request/print/orderPrintController';
import uploadCsvOrdersController from './order-request/create/dialogs/uploadCsv/uploadCsvOrdersController';
import orderRequestServices from './order-request/orderRequestServices';

//transportation files
import transportationListController from './transportation/list/transportationListController';
import transportationCreateController from './transportation/create/transportationCreateController';
import transportationUpdateController from './transportation/update/transportationUpdateController';
import transportationServices from './transportation/transportationServices'; 

export default angular.module('app.warehouse', [warehousesServices,warehouseDeliveriesServices,locationsServices,transferGuidesServices,stockAdjustmentServices,orderRequestServices,transportationServices])
  .config(routing)
  .controller('warehousesListController', warehousesListController)
  .controller('warehouseDeliveriesListController', warehouseDeliveriesListController)
  .controller('warehouseDeliveriesUpdateController', warehouseDeliveriesUpdateController)
  .controller('warehousesViewController', warehousesViewController)
  .controller('warehousesCreateController', warehousesCreateController)
  .controller('warehousesUpdateController', warehousesUpdateController)
  .controller('warehousesDeleteController', warehousesDeleteController)
  .controller('locationsListController', locationsListController)
  .controller('locationsViewController', locationsViewController)
  .controller('locationsCreateController', locationsCreateController)
  .controller('locationsUpdateController', locationsUpdateController)
  .controller('locationsDeleteController', locationsDeleteController)
  .controller('transferGuidesListController', transferGuidesListController)
  .controller('transferGuidesCreateController', transferGuidesCreateController) 
  .controller('orderValidateController', orderValidateController)
  .controller('guidesViewController', guidesViewController)
  .controller('guidesPrintController', guidesPrintController) 
  .controller('guidesWarehousePrintController', guidesWarehousePrintController)
  .controller('uploadCsvGuidesController', uploadCsvGuidesController)
  .controller('guidesDeleteController', guidesDeleteController)
  .controller('guideValidateController', guideValidateController) 
  .controller('printPackOffGuideController', printPackOffGuideController)
  .controller('stockAdjustmentListController', stockAdjustmentListController) 
  .controller('stockAdjustmentViewController', stockAdjustmentViewController)
  .controller('stockAdjustmentCreateController', stockAdjustmentCreateController)
  .controller('ordersListController', ordersListController)
  .controller('orderViewController', orderViewController)
  .controller('orderCreateController', orderCreateController)  
  .controller('orderPrintController', orderPrintController)     
  .controller('uploadCsvOrdersController', uploadCsvOrdersController) 
  .controller('transportationListController', transportationListController)  
  .controller('transportationCreateController', transportationCreateController)     
  .controller('transportationUpdateController', transportationUpdateController)  

  .run(run)
  .constant("warehouse", [{
        "title": "Bodega",
        "icon":"mdi-forklift",
        "subMenu":[
            
            // {
            // 'title' : 'Inventario',
            // 'url': '',
            // },
            {
            'title' : 'Guias de Despacho',
            'url': 'app.transferGuides',
            },
            {
            'title' : 'Ajuste Stock',
            'url': 'app.stockAdjustment',
            },
            {
            'title' : 'Solicitud de pedido',
            'url': 'app.orders',
            },
            {
            'title' : 'Ubicaciones',
            'url': 'app.locations',
            },
            {
            'title' : 'Inventario',
            "superMenu":[
            
                    {
                    'title' : 'Inventario diario',
                    'url': 'app.dailyInventory'
                    },
                    {
                    'title' : 'Inventario general',
                    'url': 'app.inventory'
                    },
                    {
                    'title' : 'Inventario sucursal',
                    'url': 'app.warehouseInventory'
                    }   
                ]
            },
            {
            'title' : 'Bodegas',
            'url': 'app.warehouses',
            },
            {
            'title' : 'Entregas Bodega',
            'url': 'app.warehouseDeliveries',
            },
            {
            'title' : 'Transportistas',
            'url': 'app.transportation',
            }
            
        ]
    }])
  .name;