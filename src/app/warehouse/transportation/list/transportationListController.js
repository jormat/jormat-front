
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> transportationListController
* # As Controller --> transportationList														
*/

export default class transportationListController{

        constructor($scope,UserInfoConstant,$timeout,transportationServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.searchData=searchData;
            vm.main=main;
            vm.transportationUpdate= transportationUpdate
            vm.transportationCreate = transportationCreate
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            main()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.transportationGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_transportistas.csv',
                columnDefs: [
                    { 
                      name:'Id',
                      field: 'transportationId',  
                      width: '5%' 
                    },
                    { 
                      name:'Nombre',
                      field: 'transportName',
                      width: '16%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<strong class="uppercase">{{row.entity.transportName}}</strong>' +
                                 '</div>'   
                    },
                    { 
                      name:'Codigo',
                      field: 'transportCode',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary">{{row.entity.transportCode}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Horario',
                      field: 'schedule',
                      width: '20%' ,
                      enableFiltering: false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase font-italic" >{{row.entity.schedule}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Telefono',
                      field: 'phone',
                      width: '12%' ,
                      enableFiltering: false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a href="https://wa.me/{{row.entity.phone}}" target="_blank"><p class="uppercase">{{row.entity.phone}}</p></a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Rastreo',
                      field: 'link',
                      width: '35%',
                      enableFiltering: false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a href="{{row.entity.link}}" target="_blank"><p class="badge bg-warning">{{row.entity.link}}</p></a>' +
                                 '</div>' 
                    },
                    { 
                      name: '',
                      width: '4%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          function main(){

            transportationServices.transportation.getTransportation().$promise.then((dataReturn) => {
              $scope.transportationGrid.data = dataReturn.data;
                 console.log('$scope.transportationGrid',$scope.transportationGrid);
             },(err) => {
                 console.log('No se ha podido conectar con el servicio',err);
             })

          }
         		

        function searchData() {
                  transportationServices.transportation.getTransportation().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.transportationGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }


        function transportationUpdate(row){
            $scope.transportationData = row;
                var modalInstance  = $modal.open({
                    template: require('../update/transportation-update.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'transportationUpdateController',
                    controllerAs: 'transportationUpdate',
                    size: 'lg'
              })
        }


        function transportationCreate(){
            console.log('entra')
                var modalInstance  = $modal.open({
                        template: require('../create/transportation-create.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'transportationCreateController',
                        controllerAs: 'transportationCreate',
                        size: 'lg'
                })
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

  transportationListController.$inject = ['$scope','UserInfoConstant','$timeout','transportationServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

