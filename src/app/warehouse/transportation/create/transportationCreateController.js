/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> transportationCreateController
* # As Controller --> transportationCreate														
*/


  export default class transportationCreateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,transportationServices,$location,UserInfoConstant,ngNotify,$state,clientsServices,warehousesServices) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.searchClient = searchClient;
        vm.transportationNew = transportationNew
        vm.setName = setName

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

    

        function setName(clients) {
                console.log('please',clients)
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedRut = clients.rut
            }


        function searchClient (value) {

            let model = {
                name: value
            }

            clientsServices.clients.getClientsDsByName(model).$promise.then((dataReturn) => {
                  $scope.clients = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            return false;
          }

	    let params = {
            checkId: $scope.checkId
          }


	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function transportationNew(row) {
            console.log('save transporte');
            let model = {
                            userId: $scope.userInfo.id,
                            name: $scope.name,
                            code : $scope.code,
                            phone : $scope.phone,
                            link : $scope.link,
                            schedule : $scope.schedule
                        }      

                        console.log('modelo',model)   

                        transportationServices.transportation.transportationCreate(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha ingresado correctamente el transporte','success')
                            $scope.result = dataReturn.data

                            $state.reload('app.transportation')
                            $modalInstance.dismiss('chao');

                          },(err) => {
                              ngNotify.set('Error al registrar transporte','error')
                          })


        }

		$scope.invoices = [];

        $scope.addInvoice = function(invoice) {
            
            $scope.invoices.push({'title': $scope.invoice, 'done':false})
            $scope.invoice = ''
            // $scope.saveButton  = false
        }

        $scope.deleteInvoice = function(index) {  
            $scope.invoices.splice(index, 1);
        }

    }

  }

  transportationCreateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','transportationServices','$location','UserInfoConstant','ngNotify','$state','clientsServices','warehousesServices'];
