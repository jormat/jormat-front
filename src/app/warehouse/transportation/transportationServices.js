/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> transportationServices
* file for call at services for transportation
*/

class transportationServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var transportation = $resource(SERVICE_URL_CONSTANT.jormat + '/warehouses/:route',{},{
      
      getTransportation: {
        method:'GET',
          params : {
            route: 'transportation'
            }
          },

        transportationCreate: {
        method:'POST',
        params : {
          route: 'transportation'
          }
        },
        
        transportationUpdate: {
          method:'PUT',
          params : {
            route: 'transportation'
            }
          }
    })

    return { transportation : transportation
             
           }
  }
}

transportationServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.transportationServices', [])
  .service('transportationServices', transportationServices)
  .name;