/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> transportationUpdateController
* # As Controller --> transportationUpdate														
*/

  export default class transportationUpdateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,transportationServices,$location,UserInfoConstant,ngNotify,$state,clientsServices,warehousesServices) {

    	var vm = this;
	    vm.cancel = cancel
        vm.transportationUpdate = transportationUpdate
        $scope.id = $scope.transportationData.transportationId

        let params = {
            transportationId: $scope.id 
          }

        transportationServices.transportation.getTransportation(params).$promise.then((dataReturn) => {
              $scope.transportation = dataReturn.data;
              console.log('$scope.transportation',$scope.transportation );

              $scope.transportationDetails  = { 
                    id : $scope.transportation[0].id,
                    transportName : $scope.transportation[0].transportName,
                    transportCode : $scope.transportation[0].transportCode,
                    schedule : $scope.transportation[0].schedule,
                    phone : $scope.transportation[0].phone,
                    link : $scope.transportation[0].link,
                    comment : $scope.transportation[0].comment
                  }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })


        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function transportationUpdate() {
            let model = {   
                            transportationId : $scope.id,
                            userId: $scope.userInfo.id,
                            name : $scope.transportationDetails.transportName,
                            code : $scope.transportationDetails.transportCode,
                            schedule : $scope.transportationDetails.schedule,
                            phone : $scope.transportationDetails.phone,
                            link : $scope.transportationDetails.link
                        }      

                        console.log('modelo',model)   

                        transportationServices.transportation.transportationUpdate(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha actualizado correctamente el transporte','success')
                            $scope.result = dataReturn.data

                            $state.reload('app.transportation')
                            $modalInstance.dismiss('chao');

                          },(err) => {
                              ngNotify.set('Error al actualizar transporte','error')
                          })


        }


    }

  }

  transportationUpdateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','transportationServices','$location','UserInfoConstant','ngNotify','$state','clientsServices','warehousesServices'];
