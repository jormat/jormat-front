/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> stockAdjustmentViewController
* # As Controller --> stockAdjustmentView														
*/


  export default class stockAdjustmentViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,stockAdjustmentServices,$location,UserInfoConstant,ngNotify,$state,itemsServices) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.validateGuide=validateGuide
        vm.rejectedGuide=rejectedGuide
        vm.webItemUpdate = webItemUpdate
        vm.groupValidation = groupValidation
        vm.kardexNew = kardexNew
        $scope.transferGuideId = $scope.guideData.transferGuideId
        $scope.clientName = $scope.guideData.clientName
        $scope.historyItems = []

        let params = {
                transferGuideId: $scope.transferGuideId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })

        stockAdjustmentServices.guides.getGuideDetails(params).$promise.then((dataReturn) => {
              $scope.guides = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get guides Details',err);
              })

        stockAdjustmentServices.guides.getGuidesItems(params).$promise.then((dataReturn) => {
              $scope.guidesItems = dataReturn.data
              var subTotal = 0
                var i = 0
                   for (i=0; i<$scope.guidesItems.length; i++) {
                       subTotal = subTotal + $scope.guidesItems[i].total  
                    }
                    
                    $scope.subTotality =  subTotal
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getGuidesItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function validateGuide(value) {
            $scope.btnValidate = true
            let model = {
                    transferGuideId : $scope.transferGuideId,
                    userId : $scope.userInfo.id,
                    origin :$scope.guides[0].originId,
                    itemsInvoices : $scope.guidesItems  
                }

                console.log('that',model)

                for(var  i in model.itemsInvoices){
                    let modelo = {
                        itemId: model.itemsInvoices[i].itemId,
                        originId :$scope.guides[0].originId,
                        quantity: model.itemsInvoices[i].quantityItems
                     }
    
                        itemsServices.items.getHistoric(modelo).$promise.then((dataReturn) => {
                           $scope.item = dataReturn.data;

                            $scope.historyItems.push({
                            itemId: $scope.item[0].itemId,
                            itemDescription: $scope.item[0].itemDescription,
                            location : $scope.item[0].locations,
                            previousStock: $scope.item[0].stock,
                            price: $scope.item[0].vatPrice,
                            quantity: modelo.quantity,
                            generalStock: $scope.item[0].generalStock,
                            currentprice: $scope.item[0].vatPrice,
                            currentLocation: $scope.item[0].locations
                            })
                                
                            console.log('that', $scope.historyItems);

                            if (i== $scope.historyItems.length-1) {
                                  
                                groupValidation(model,$scope.historyItems,value)
                                $scope.ultimo = "último registro";

                              } else {
                                $scope.ultimo = "item"+i;
                              }
                                console.log( $scope.ultimo); 
                               },(err) => {
                                console.log('No se ha podido conectar con el servicio',err);
                            })
                }
               

                
        }


        function webItemUpdate(itemId,stock) {

                let data = {
                                    sku : itemId.toString(),
                                    stock_quantity: stock
                                }

                                itemsServices.woocommerceAPI.itemUpdate(data).$promise.then((dataReturn) => {
                                  $scope.result = dataReturn.data;
                                  $scope.message= dataReturn.message;
                                  $scope.status= dataReturn.status;

                                  if ($scope.status==200  ) {

                                     ngNotify.set('Mensaje sitio web: '+ $scope.message,'info')
                                        $state.reload("app.stockAdjustment")
                                        $modalInstance.dismiss('chao');

                                      }else{
                                        console.log('Error actualizar stock web',$scope.message);
                                        ngNotify.set( 'Error actualización ITEM en el sitio web: ' + $scope.message, {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                        })

                                        $state.reload("app.stockAdjustment")
                                        $modalInstance.dismiss('chao');
                                     }
                                  
                                  },(err) => {
                                        console.log('Error actualizar stock web',err);
                                        ngNotify.set( 'Error API Web', {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                      })
                                  })

        }

        function rejectedGuide() {

            $scope.btnRejected = true

            let model = {
                    transferGuideId : $scope.transferGuideId,
                    userId : $scope.userInfo.id,
                    origin :$scope.guides[0].originId,
                    itemsInvoices : $scope.guidesItems  
                }

            stockAdjustmentServices.guides.validateGuideRejected(model).$promise.then((dataReturn) => {
                    ngNotify.set('Guia rechazada correctamente','success');
                    $state.reload("app.stockAdjustment")
                    $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al validar guía','error')
                      $scope.btnRejected = false
                  })
              
        }

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

        function groupValidation(model,historyItems,value) {

            if (value == 1) {

                stockAdjustmentServices.guides.validateGuide(model).$promise.then((dataReturn) => {
                ngNotify.set('Guia aceptada correctamente +','success');

                    for(var i in model.itemsInvoices){

                            console.log('entra',model.itemsInvoices)

                            if (model.itemsInvoices[i].webType == "SI"){
                                console.log('tipo web')

                                let modelo = {
                                    itemId : model.itemsInvoices[i].itemId
                                }

                                itemsServices.items.getGeneralStockWeb(modelo).$promise.then((dataReturn) => {
                                   $scope.result = dataReturn.data;
                                   $scope.stock = $scope.result[0].generalStock;

                                   webItemUpdate(modelo.itemId,$scope.stock)

                                  },(err) => {
                                      console.log('No se ha podido conectar con el servicio',err);
                                  })

                                 }
                            else{
                                console.log('normal')
                            }
                    }
                    $state.reload("app.stockAdjustment")
                    $modalInstance.dismiss('chao');
                    $scope.condition = "Entrada +"
                    kardexNew(historyItems,$scope.condition)


              },(err) => {
                  ngNotify.set('Error al validar guía','error')
                  $scope.btnValidate = false
              })

            }else{

              stockAdjustmentServices.guides.validateGuideDecrease(model).$promise.then((dataReturn) => {
                ngNotify.set('Guia aceptada correctamente -','success');

                   for(var i in model.itemsInvoices){

                            console.log('entra',model.itemsInvoices)

                            if (model.itemsInvoices[i].webType == "SI"){
                                console.log('tipo web')

                                let modelo = {
                                    itemId : model.itemsInvoices[i].itemId
                                }

                                itemsServices.items.getGeneralStockWeb(modelo).$promise.then((dataReturn) => {
                                   $scope.result = dataReturn.data;
                                   $scope.stock = $scope.result[0].generalStock;

                                   webItemUpdate(modelo.itemId,$scope.stock)

                                  },(err) => {
                                      console.log('No se ha podido conectar con el servicio',err);
                                  })

                                 }
                            else{
                                console.log('normal')
                                
                            }
                }

                $state.reload("app.stockAdjustment")
                $modalInstance.dismiss('chao');
                $scope.condition = "Salida -"
                kardexNew(historyItems,$scope.condition)

              },(err) => {
                  ngNotify.set('Error al validar guía','error')
                  $scope.btnValidate = false
              })

            }

        }

        function kardexNew(historyItems,condition) {

            for(var  i in historyItems){

                let itemsValue = {
                  documentId: $scope.transferGuideId,
                  itemId: historyItems[i].itemId,
                  itemDescription: historyItems[i].itemDescription,
                  location : historyItems[i].location,
                  previousStock: historyItems[i].previousStock,
                  price: historyItems[i].price,
                  quantity: historyItems[i].quantity,
                  originId: $scope.guides[0].originId,
                  userId: $scope.userInfo.id,
                  document: "AJU",
                  type: condition,
                  generalStock: historyItems[i].generalStock,
                  currentprice: historyItems[i].price,
                  currentLocation: historyItems[i].currentLocation,
                  destiny: $scope.guides[0].destinationId

                }
               
               console.log('that',itemsValue);
 
               itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                      $scope.result2 = dataReturn.data;
                      console.log('Historial del item actualizado correctamente');
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
              }
		}

    }

  }

  stockAdjustmentViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','stockAdjustmentServices','$location','UserInfoConstant','ngNotify','$state','itemsServices'];
