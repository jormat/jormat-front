
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> stockAdjustmentListController
* # As Controller --> stockAdjustmentList														
*/

export default class stockAdjustmentListController{

        constructor($scope,UserInfoConstant,$timeout,stockAdjustmentServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.viewGuide = viewGuide
            // vm.validateGuide= validateGuide
            // vm.deleteGuide = deleteGuide
            vm.searchData=searchData
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.guidesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'transferGuideId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.stockAdjustmentList.viewGuide(row.entity)">{{row.entity.transferGuideId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '34%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="{{(row.entity.type == \'Sobrante +\')?\'badge bg-secundary\':\'badge bg-danger\'}}">{{row.entity.type}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> $ {{row.entity.total}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '11%'
                    },
                  
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '11%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<span class="{{(row.entity.statusName == \'Rechazada\')?\'label bg-danger\':\'label bg-warning\'}}">{{row.entity.statusName}}</span>'+
                                 '</div>'
                    },
                    
                    { 
                      name: 'Acciones',
                      width: '9%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            stockAdjustmentServices.guides.getGuides().$promise.then((dataReturn) => {
              $scope.guidesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })


            function searchData() {
              stockAdjustmentServices.guides.getGuides().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.guidesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewGuide(row){
              console.log('that',row);
	            $scope.guideData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/stock-adjustment-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'stockAdjustmentViewController',
	                    controllerAs: 'stockAdjustmentView',
	                    size: 'lg'
	            })
	        }

            // function deleteGuide(row){
            //   $scope.guideData = row;
            //   var modalInstance  = $modal.open({
            //           template: require('../delete/guides-delete.html'),
            //           animation: true,
            //           scope: $scope,
            //           controller: 'guidesDeleteController',
            //           controllerAs: 'guidesDelete'
            //   })
            // }

            // function validateGuide(row){
            //   $scope.guideData = row;
            //   var modalInstance  = $modal.open({
            //           template: require('../validate/guides-validate.html'),
            //           animation: true,
            //           scope: $scope,
            //           controller: 'guideValidateController',
            //           controllerAs: 'guideValidate'
            //   })
            // }


            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

stockAdjustmentListController.$inject = ['$scope','UserInfoConstant','$timeout','stockAdjustmentServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

