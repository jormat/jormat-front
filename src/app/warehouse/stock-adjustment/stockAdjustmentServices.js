/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> stockAdjustmentServices
* file for call at services for stockAdjustment
*/

class stockAdjustmentServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var guides = $resource(SERVICE_URL_CONSTANT.jormat + '/guides/stock-adjustment/:route',{},{
      
        getGuides: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getGuideDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createGuide: {
            method:'POST',
            params : {
                route: ''
            }
        },
        disabledGuide: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        validateGuide: {
            method:'PUT',
            params : {
                route: 'validate'
            }
        },
        validateGuideDecrease: {
            method:'PUT',
            params : {
                route: 'decrease'
            }
        },
        validateGuideRejected: {
            method:'PUT',
            params : {
                route: 'rejected'
            }
        },
        getGuidesItems: {
            method:'GET',
            params : {
               route: 'items'
            }
        }
    })

    return { guides : guides
             
           }
  }
}

  stockAdjustmentServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.stockAdjustmentServices', [])
  .service('stockAdjustmentServices', stockAdjustmentServices)
  .name;