
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> warehouseDeliveriesListController
* # As Controller --> warehouseDeliveriesList
*/

export default class warehouseDeliveriesListController{
	constructor(uiGridExporterConstants,uiGridExporterService,$rootScope,$scope,UserInfoConstant,$timeout,warehouseDeliveriesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService,Dialogs){
		this.$scope = $scope
		this.Dialogs = Dialogs;
		this.$modal = $modal;
		this.ngNotify = ngNotify;
		this.wdSrv = warehouseDeliveriesServices;
		this.uiGridExporterService = uiGridExporterService
		this.uiGridExporterConstants = uiGridExporterConstants
		this.$filter = $filter;
		this.$timeout = $timeout
		this.$rootScope = $rootScope
		this.$interval = $interval
		this.$scope.selectedChecks = []
		this.searchOptions = {
			updateOn: 'default blur',
			debounce:{
				'default': 100,
				'blur': 0
			}
		}
		i18nService.setCurrentLang('es');

		this.tableInitializeMethod()

		this.$scope.UserInfoConstant = UserInfoConstant
		this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
			if (details !== undefined) {
				this.$scope.userInfo = details.user
				this.main()
			}
		})
	}

	main() {
		const searchText = ''
		this.params = {
			newPage    	   : 1,
			pageSize       : 2000,
			searchText
		}

		this.loadDocument()
		this.$rootScope.$on('$translateChangeSuccess', () => {
			this.tableInitializeMethod()
		})
	}

	searchData () {
		this.$scope.warehouseDeliveriesGrid.data = this.$filter('filter')(this.documents, this.searchText, undefined);
	}

	loadDocument(){
		this.wdSrv.documents.getDocumentListPaginated(this.params).$promise.then((dataReturn) => {
			this.documents = angular.copy(dataReturn.data)
			this.$scope.warehouseDeliveriesGrid.data = dataReturn.data;
			this.$scope.warehouseDeliveriesGrid.totalItems = dataReturn.count || 0
			this.$scope.warehouseDeliveriesGrid.data.forEach((row, index) => {
				row.sequence = index;
		})
		},(err) => {
			console.log('No se ha podido conectar con el servicio',err);
		})
	}

	updateDocument(row,op){
		this.$scope.isEdit = op;
		this.opt = {
			message : 'Ingrese un comentario',
			type : row.typeName,
			name: row.clientName,
			number:row.documentId,
			description: row.commentaryOut,
			opt: row.isOut
		}

		this.Dialogs.modalEnterDescription(this.opt).then((response) => {
			if(response.agree) {
				this.$scope.warehouseDeliveriesGrid.data[row.sequence].loading = 1
				this.paramsWarehouse = {
					userId : this.$scope.userInfo.id,
					typeDocumentId : row.typeId,
					documentId : row.documentId,
					commentary : response.description,
					option : response.option
				}

				this.wdSrv.documents.setDocumentList(this.paramsWarehouse).$promise.then((result) => {
					this.$scope.warehouseDeliveriesGrid.data[row.sequence] = result.data
					this.ngNotify.set('La entrega ha sido registrada exitosamente','warn');
				},(err) => {
					this.ngNotify.set('Error al actualizar documento','error')
				})
			}
		})
	}

	viewCommentary(row,op){
		this.$scope.isEdit = op;
		this.$scope.item = row;
		var modalInstance  = this.$modal.open({
			template: require('../update/warehouse-deliveries-update.html'),
			animation: true,
			scope: this.$scope,
			controller: 'warehouseDeliveriesUpdateController',
			controllerAs: 'warehouseDelivUpdate'
	})
	}

	viewDocument(row){
		this.$scope.document = row;

		if (this.$scope.document.typeName == "FACT") {
			this.$scope.invoiceData = {
				invoiceId: this.$scope.document.documentId,
				clientName:this.$scope.document.clientName
			}
			console.log('ver factura');
			var modalInstance  = this.$modal.open({
					template: require('../../../sales/clients-invoices/view/clients-invoices-view.html'),
					animation: true,
					scope: this.$scope,
					controller: 'clientsInvoicesViewController',
					controllerAs: 'clientsInvoicesView',
					size: 'lg'
			})
		}

		if (this.$scope.document.typeName == "NN") { 
			this.$scope.invoiceData = {
				documentId: this.$scope.document.documentId,
				clientName:this.$scope.document.clientName
			}
			console.log('ver documento nn');
			var modalInstance  = this.$modal.open({
				template: require('../../../sales/documents-nn/view/documents-nn-view.html'),
				animation: true,
				scope: this.$scope,
				controller: 'documentsNnViewController',
				controllerAs: 'documentsNnView',
				size: 'lg'
			})
		}

		if (this.$scope.document.typeName == "BOL") { 
			this.$scope.ballotsData = {
				ballotId: this.$scope.document.documentId,
				clientName:this.$scope.document.clientName
			}
			console.log('Boleta');
			var modalInstance  = this.$modal.open({
				template: require('../../../sales/ballots/view/ballots-view.html'),
				animation: true,
				scope: this.$scope,
				controller: 'ballotsViewController',
				controllerAs: 'ballotsView',
				size: 'lg'
			})
		}

		if (this.$scope.document.typeName == "NOV") { 
			this.$scope.saleNoteData = {
				saleNoteId: this.$scope.document.documentId,
				clientName:this.$scope.document.clientName 
			}
			console.log('Nota Venta');
			var modalInstance  = this.$modal.open({
				template: require('../../../sales/sale-note/view/sale-notes-view.html'),
				animation: true,
				scope: this.$scope,
				controller: 'saleNotesViewController',
				controllerAs: 'saleNotesView',
				size: 'lg'
			})
		}
	}

	dataDownload(items) {
		let dataReturn = []

		items.forEach((row) => {
			row = row.entity ? row.entity : row
			const rowData = [
				{ value: row.documentId },
				{ value: row.clientName },
				{ value: row.typeName },
				{ value: row.origin },
				{ value: row.userCreation },
				{ value: row.total },
				{ value: row.dateDoc },
				{ value: row.statusName },
				{ value: row.dsOut },
				{ value: row.dateOut },
				{ value: row.usName },
			]

			dataReturn.push(rowData)
		})

		return dataReturn
	}

	exportCSV() {
		const exportService = this.uiGridExporterService
		const dataFilter = this.dataDownload(this.gridApi.grid.rows)
		const grid = this.gridApi.grid
		const fileName = 'myfile.csv'

		exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
			const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
			let formatColums = []

			exportColumnHeaders.map((colums) => {
					formatColums.push(colums)
			})
			const csvContent = exportService.formatAsCsv(formatColums, dataFilter, grid.options.exporterCsvColumnSeparator)

			exportService.downloadFile(fileName, csvContent, grid.options.exporterOlderExcelCompatibility)
		}).catch(() => {
			this.ngNotify.set('ERROR', 'error')
		})
	}

	exportAllCSV() {
		const exportService = this.uiGridExporterService

		this.wdSrv.documents.getDocumentList().$promise.then(
			response => {
				if (!response.status) {
					const msn = response.messageError || response.message || this.$translate('MSJ_ERROR')

					this.ngNotify.set(msn, 'warn')
				} else {
					const dataFilter = this.dataDownload(response.data)
					const grid = this.gridApi.grid
					const fileName = 'myfile.csv'

					exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
						const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
						let formatColums = []

						exportColumnHeaders.map((colums) => {
								formatColums.push(colums)
						})

						const csvContent = exportService.formatAsCsv(formatColums, dataFilter, grid.options.exporterCsvColumnSeparator)

						exportService.downloadFile(fileName, csvContent, grid.options.exporterOlderExcelCompatibility)
					}).catch(() => {
						this.ngNotify.set('ERROR', 'error')
					})
				}
			},
			() => {
				this.ngNotify.set(this.$translate('MSJ_NO_DATA_RELATED_YOUR_SEARCH') + ' getDownloadAllCsv', 'error')
			},
		)
	}

	exportPDF() {
		const exportService = this.uiGridExporterService
		const dataFilter = this.dataDownload(this.gridApi.grid.rows)
		const grid = this.gridApi.grid

		exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
			const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
			let formatColums = []

			exportColumnHeaders.map((colums) => {
				if (colums.name !== 'href')
					formatColums.push(colums)
			})
			const content = exportService.prepareAsPdf(grid, formatColums, dataFilter)
			pdfMake.createPdf(content).open()
		}).catch(() => {
			this.ngNotify.set('ERROR', 'error')
		})
	}

	exportSelectedPDF() {
		if(this.$scope.selectedChecks.length === 0) return this.ngNotify.set('No existen filas seleccionados','warn')
		const exportService = this.uiGridExporterService
		const dataFilter = this.dataDownload(this.$scope.selectedChecks)
		const grid = this.gridApi.grid

		exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
			const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
			let formatColums = []

			exportColumnHeaders.map((colums) => {
				if (colums.name !== 'href')
					formatColums.push(colums)
			})
			const content = exportService.prepareAsPdf(grid, formatColums, dataFilter)
			pdfMake.createPdf(content).open()
		}).catch(() => {
			this.ngNotify.set('ERROR', 'error')
		})
	}

	tableInitializeMethod() {
		this.$scope.warehouseDeliveriesGrid = {
			enableFiltering: true,
			enableSelectAll: true,
			enableGridMenu: true,
			enableRowSelection: true,
			paginationPageSizes  : [ 500, 1000, 2000 ],
			paginationPageSize   : 2000,
			enablePaging         : true,
			useExternalPagination: true,
			exporterCsvFilename  : 'Listado_de_items.csv',
			exporterMenuPdf      : false,
			exporterMenuCsv      : false,
			exporterMenuAllData  : false,
			multiSelect          : true,
			exporterPdfHeader: { 
				text: "Listado de Entregas Bodega", 
				style: 'headerStyle',
				alignment: 'center'
			},
			isRowSelectable: (row) => {
				if (row.entity.dsOut == "Pendiente") {
					return true;
				} else {
					return false;
				}
			},
			exporterPdfFooter: function ( currentPage, pageCount ) {
				return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
			},
			exporterPdfCustomFormatter: function ( docDefinition ) {
				docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [20,20,20,20] };
				docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
				return docDefinition;
			},
			onRegisterApi: (gridApi) => {
				this.gridApi = gridApi

				this.$interval(() => {
					this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar vista como csv', action: () => {
						this.exportCSV()
					}, order: 100 } ])
					this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar todo como csv', action: () => {
						this.exportAllCSV()
					}, order: 100 } ])
					this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar vista como PDF', action: () => {
						this.exportPDF()
					}, order: 100 } ])
					this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar selección como PDF', action: () => {
						this.exportSelectedPDF()
					}, order: 100 } ])
				}, 0, 1)
				
				gridApi.pagination.on.paginationChanged(this.$scope, (newPage, pageSize) => {
					this.params.newPage = newPage
					this.params.pageSize = pageSize

					this.wdSrv.documents.getDocumentListPaginated(this.params).$promise.then(
						response => {
							if (!response.status) {
								const msn = response.messageError || response.message || 'ERROR'
			
								this.ngNotify.set(msn, 'warn')
							} else {
								this.documents = angular.copy(response.data)
								this.$scope.warehouseDeliveriesGrid.data = response.data
								this.$scope.warehouseDeliveriesGrid.totalItems = response.count || 0
								this.$scope.warehouseDeliveriesGrid.data.forEach((row, index) => {
									row.sequence = index;
								})
							}
						},
						() => {
							this.ngNotify.set('Sin respuesta getItemsMain', 'error')
						},
					)
				})

				gridApi.core.on.filterChanged(this.$scope, () => {
					const grid = gridApi.grid.renderContainers.body.grid
					let filters = {}

					for (let i in grid.columns) {
						if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term != undefined)) {
							filters[grid.columns[i].field] = grid.columns[i].filters[0].term
						}
					}

					this.params.newPage = 1
					this.params.filters = filters

					this.wdSrv.documents.getDocumentListPaginated(this.params).$promise.then(
						response => {
							if (!response.status) {
								const msn = response.messageError || response.message || 'ERROR'
			
								this.ngNotify.set(msn, 'warn')
							} else {
								this.documents = angular.copy(response.data)
								this.$scope.warehouseDeliveriesGrid.data = response.data
								this.$scope.warehouseDeliveriesGrid.totalItems = response.count || 0
								this.$scope.warehouseDeliveriesGrid.data.forEach((row, index) => {
									row.sequence = index;
								})
							}
						},
						() => {
							this.ngNotify.set('Sin respuesta getItemsMain', 'error')
						},
					)
				})

				gridApi.selection.on.rowSelectionChanged(this.$scope,(row,$index) => {
					this.$scope.selectedChecks = gridApi.selection.getSelectedRows()
				});
			},
			columnDefs: [

				{ 
					name:'N° Doc.',
					field: 'documentId',
					width: '5%',
					cellTemplate:	'<div class="ui-grid-cell-contents ">'+
									'<a class="badge bg-accent" ng-click="grid.appScope.warehouseDeliveriesList.viewDocument(row.entity)">{{row.entity.documentId}}</a>' +
									'</div>',
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }
				},
				{ 
                    name:'Cliente',
                    field: 'clientName',
                    width: '18%',
					cellTemplate:	'<div class="ui-grid-cell-contents">'+
									'<p><span class="uppercase" style="font-size: 11px;">{{row.entity.clientName}}</span></p>' +
									'</div>',
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }        
                },
				
				{ 
					name:'Tipo',
					field: 'typeName',
					width: '5%',
					cellTemplate:	'<div class="ui-grid-cell-contents ">'+
									'<p class=" label bg-warning" style="font-size: 10px;">{{row.entity.typeName}}</p>' +
									'</div>',
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }
				},
				{ 
					name:'Origen',
					field: 'origin',
					width: '7%',
					cellTemplate:	'<div class="ui-grid-cell-contents">'+
									'<p><span class="label bg-secundary" style="font-size: 9px;">{{row.entity.origin}}</span></p>' +
									'</div>',
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }
				},
				{ 
                    name:'Atiende',
                    field: 'userCreation',
                    width: '8%',
					cellTemplate:	'<div class="ui-grid-cell-contents">'+
									'<p><span class="label bg-secundary font-italic" style="font-size: 8px;">{{row.entity.userCreation}}</span></p>' +
									'</div>',
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   },
				   enableFiltering: true
				},
				{ 
					name:'Total ',
					field: 'total',
					width: '6%',
					cellTemplate:	'<div class="ui-grid-cell-contents ">'+
									'<p style="font-size: 12px;"><strong>${{row.entity.total}}</strong></p>' +
									'</div>' ,
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   },
				   enableFiltering: false
				},
				{ 
					name:'Fecha',
					field: 'dateDoc',
					width: '6%',
					cellTemplate:	'<div class="ui-grid-cell-contents ">'+
									'<p style="font-size:10px;">{{row.entity.dateDoc}}</p>' +
									'</div>'  ,
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }
				},
				{ 
					name:'Pago',
					field: 'statusName',
					width: '6%',
                    cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size:8px;" class="{{(row.entity.statusName == \'Pendiente\')?\'badge bg-danger\':\'badge bg-success\'}}">{{row.entity.statusName}}</p>' +
                                 '</div>' ,
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }
				},
				{ 
					name:'Entrega',
					field: 'dsOut',
					width: '8%',
					cellTemplate:	'<div class="ui-grid-cell-contents ">'+
									'<p class=" label bg-{{row.entity.styleOut}}" style="font-size: 10px;">{{row.entity.dsOut}}</p>' +
									'</div>',
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }
				},
				{ 
					name:'Hora',
					field: 'dateOut',
					width: '10%' ,
					cellTemplate:	'<div class="ui-grid-cell-contents ">'+
									'<p style="font-size:10px;">{{row.entity.dateOut}}</p>' +
									'</div>' ,
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }
				},
				{ 
					name:'Usuario',
					field: 'usName',
					width: '10%' ,
					cellTemplate:	'<div class="ui-grid-cell-contents">'+
									'<p><span class="label bg-secundary font-italic" style="font-size: 8px;">{{row.entity.usName}}</span></p>' +
									'</div>',
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }
				},
				{ 
					name: 'Acciones',
					width: '8%',
					field: 'href',
					enableFiltering: false,
					cellTemplate:	'<div class="ui-grid-cell-contents text-center">'+
									'<a tooltip-placement="top" tooltip="Entregar" style="font-size: 9px;" class="label bg-warning" ng-click="grid.appScope.warehouseDeliveriesList.updateDocument(row.entity,true)" ng-if="(!row.entity.isOut==1 || row.entity.isOut==2) && !row.entity.loading==1"><i class="mdi-package-variant-closed"></i> ENTREGAR</a>'+
									'<a tooltip-placement="top" tooltip="Ver" style="font-size: 9px;" class="label bg-info" ng-click="grid.appScope.warehouseDeliveriesList.viewCommentary(row.entity,false)" ng-if="row.entity.isOut!=null && row.entity.isOut!=2  && !row.entity.loading==1"><i class="mdi-eye"></i> VER</a>'+
									'<div ng-if="row.entity.loading == 1"><i class="fa text-dark fa-spinner fa-spin fa-sm fa-fw"></i></div>'+
									'</div>',
					cellClass: function(grid, row) {
                        if (row.entity.packOff === 1) {
                         return 'yellow';
                       }
                   }
				}
			]
		};

		this.$rootScope.$watch('sidenavState', () => {
			this.$timeout(() => {
				this.gridApi.grid.handleWindowResize()
			}, 500)
		})
	}
}

warehouseDeliveriesListController.$inject = ['uiGridExporterConstants','uiGridExporterService','$rootScope','$scope','UserInfoConstant','$timeout','warehouseDeliveriesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService','Dialogs'];

