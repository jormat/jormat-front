
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> warehouseDeliveriesUpdateController
* # As Controller --> warehouseDelivUpdate
*/

export default class warehouseDeliveriesUpdateController{
	constructor($scope,UserInfoConstant,$timeout,warehouseDeliveriesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modalInstance){
		this.$modalInstance = $modalInstance;
		this.$mdDialog = $mdDialog;
		this.wdSrv = warehouseDeliveriesServices;
		this.ngNotify = ngNotify;
		this.$scope = $scope;
		this.item = $scope.item;
		this.userinfo = UserInfoConstant[0].details;
	}//FIN CONSTRUCTOR

	// Funciones
	cancel() {
		this.$modalInstance.dismiss('chao');
	}

	updateComent(){
		this.params = {
			userId : this.userinfo[0].user.id,
			typeDocumentId : this.item.typeId,
			documentId : this.item.documentId,
			commentary : this.$scope.observation
		}

		this.wdSrv.documents.setDocumentList(this.params).$promise.then((dataReturn) => {
			this.ngNotify.set('Comentario agregado correctamente','success');
			this.cancel();
		},(err) => {
			this.ngNotify.set('Error al actualizar documento','error')
		})
	}
}

warehouseDeliveriesUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','warehouseDeliveriesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modalInstance'];

