/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> warehouseDeliveriesServices
* file for call at services for warehouses
*/

class warehouseDeliveriesServices {
	constructor($resource,SERVICE_URL_CONSTANT) {
		const documents = $resource(SERVICE_URL_CONSTANT.jormat + '/warehouseDeliveries/:route',{},{
		
			getDocumentList: {
				method:'GET',
				params : {
					route: 'list'
				}
			},
			getDocumentListPaginated: {
				method:'GET',
				params : {
					route: 'listPaginated'
				}
			},
			setDocumentList: {
				method:'POST',
				params : {
					route: 'update'
				}
			},
		})

		

    return { documents : documents
             
           }
  }
}

warehouseDeliveriesServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.warehouseDeliveriesServices', [])
  .service('warehouseDeliveriesServices', warehouseDeliveriesServices)
  .name;