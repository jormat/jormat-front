
routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default function routes($stateProvider, $urlRouterProvider, $locationProvider) {
	$locationProvider.html5Mode(true);
	//$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('app.warehouses', {
			abstract: false,
			url: '/sucursales',
			template: require('./warehouses/list/warehouses-list.html'),
			controller: 'warehousesListController',
			controllerAs:'warehousesList'
		})
		.state('app.warehousesCreate', {
			abstract: false,
			url: '/sucursales/crear-sucursal',
			template: require('./warehouses/create/warehouses-create.html'),
			controller: 'warehousesCreateController',
			controllerAs:'warehousesCreate'
		})
		.state('app.warehousesUpdate', {
			abstract: false,
			url: '/sucursales/actualizar-sucursal',
			template: require('./warehouses/update/warehouses-update.html'),
			controller: 'warehousesUpdateController',
			controllerAs:'warehousesUpdate'
		})
		.state('app.locations', {
			abstract: false,
			url: '/sucursales/ubicaciones',
			template: require('./locations/list/locations-list.html'),
			controller: 'locationsListController',
			controllerAs:'locationsList'
		})
		.state('app.locationsCreate', {
			abstract: false,
			url: '/sucursales/ubicaciones/crear-ubicaciones',
			template: require('./locations/create/locations-create.html'),
			controller: 'locationsCreateController',
			controllerAs:'locationsCreate'
		})
		.state('app.locationsUpdate', {
			abstract: false,
			url: '/sucursales/ubicaciones/actualizar-ubicaciones?id&name&code&warehouse',
			template: require('./locations/update/locations-update.html'),
			controller: 'locationsUpdateController',
			controllerAs:'locationsUpdate'
		})
		.state('app.transferGuides', {
			abstract: false,
			url: '/sucursales/guias-traslado',
			template: require('./transfer-guides/list/transfer-guides-list.html'),
			controller: 'transferGuidesListController',
			controllerAs:'transferGuidesList'
		})

		.state('app.transferGuidesCreate', {
			abstract: false,
			url: '/sucursales/guias-traslado/crear-guia',
			template: require('./transfer-guides/create/transfer-guides-create.html'),
			controller: 'transferGuidesCreateController',
			controllerAs:'transferGuidesCreate'
		})

		.state('app.transferGuidesPrint', {
			abstract: false,
			url: '/sucursales/guias-traslado/imprimir-guia?idGuide',
			template: require('./transfer-guides/print/guides-print.html'),
			controller: 'guidesPrintController',
			controllerAs:'guidesPrint'
		})

		.state('app.guidesWarehousePrint', {
			abstract: false,
			url: '/sucursales/guias-traslado/imprimir-guia-bodega?idGuide',
			template: require('./transfer-guides/warehouse-print/guides-warehouse-print.html'),
			controller: 'guidesWarehousePrintController',
			controllerAs:'guidesWarehousePrint'
		})

		.state('app.printPackOffGuide', {
			abstract: false,
			url: '/sucursales/guias-traslado/imprimir-despacho?idGuide',
			template: require('./transfer-guides/packOff/print-pack-off.html'),
			controller: 'printPackOffGuideController',
			controllerAs:'printPackOffGuide'
		})

		.state('app.orderValidate', {
			abstract: false,
			url: '/bodega/guias-traslado/validar-orden?orderId&originId&destinationId',
			template: require('./transfer-guides/order/order-validate.html'),
			controller: 'orderValidateController',
			controllerAs:'orderValidate'
		})

		.state('app.orders', {
			abstract: false,
			url: '/bodega/solicitud-pedido',
			template: require('./order-request/list/orders-list.html'),
			controller: 'ordersListController',
			controllerAs:'ordersList'
		})

		.state('app.ordersCreate', {
			abstract: false,
			url: '/bodega/solicitud-pedido/crear-orden',
			template: require('./order-request/create/order-create.html'),
			controller: 'orderCreateController',
			controllerAs:'orderCreate'
		})

		.state('app.ordersPrint', {
			abstract: false,
			url: '/bodega/solicitud-pedido/imprimir-orden?idOrder&originId&destinationId',
			template: require('./order-request/print/order-print.html'),
			controller: 'orderPrintController',
			controllerAs:'orderPrint'
		})

		.state('app.warehouseDeliveries', {
			abstract: false,
			url: '/bodega/entregas-bodega',
			template: require('./warehouse-deliveries/list/warehouse-deliveries-list.html'),
			controller: 'warehouseDeliveriesListController',
			controllerAs:'warehouseDeliveriesList'
		}) 

		.state('app.stockAdjustment', {
			abstract: false,
			url: '/sucursales/guias-ajuste-stock',
			template: require('./stock-adjustment/list/stock-adjustment-list.html'),
			controller: 'stockAdjustmentListController',
			controllerAs:'stockAdjustmentList'
		})
		.state('app.stockAdjustmentCreate', {
			abstract: false,
			url: '/sucursales/guias-ajuste-stock/crear-guia-ajuste',
			template: require('./stock-adjustment/create/stock-adjustment-create.html'),
			controller: 'stockAdjustmentCreateController',
			controllerAs:'stockAdjustmentCreate'
		})
		.state('app.transportation', {
			abstract: false,
			url: '/transportes',
			template: require('./transportation/list/transportation-list.html'),
			controller: 'transportationListController',
			controllerAs:'transportationList'
		})
}
