
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> warehousesListController
* # As Controller --> warehousesList														
*/

export default class warehousesListController{

        constructor($scope,UserInfoConstant,$timeout,warehousesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.searchData=searchData;
            vm.deleteWarehouses= deleteWarehouses;
            vm.viewWarehouses= viewWarehouses
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')

            $scope.warehousesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'warehouseId',  
                      width: '15%' 
                    },
                    { 
                      name:'Sucursal',
                      field: 'warehouseName',
                      width: '50%' 
                    },
                    { 
                      name:'Codigo ',
                      field: 'warehouseCode',
                      width: '25%' 
                    },
                    
                    { 
                      name: 'Acciones',
                      width: '10%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
         		warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
         		  $scope.warehousesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

           // $scope.warehousesGrid.data = [
           //    {
           //      "warehouseId": 1,
           //      "warehouseName": "Casa Matriz",
           //      "warehouseCode": "LA"
           //    },
           //    {
           //      "warehouseId": 2,
           //      "warehouseName": "Chillán",
           //      "warehouseCode": "CHILL"
           //    },
           //    {
           //      "warehouseId": 3,
           //      "warehouseName": "Consitución",
           //      "warehouseCode": "CONST"
           //    },
           //    {
           //      "warehouseId": 4,
           //      "warehouseName": "Temuco",
           //      "warehouseCode": "TEM"
           //    }
           //  ]

            function searchData() {
              warehousesServices.warehouses.getWarehouses().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.warehousesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

            function deleteWarehouses(row){
            // $scope.userinfo = UserInfoConstant[0].details;
            // $scope.simulationData = row.entity;
            $scope.data=$scope.warehousesGrid.data;
            $scope.index = $scope.warehousesGrid.data.indexOf(row.entity);
            var modalInstance  = $modal.open({
                    template: require('../delete/warehouses-delete.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'warehousesDeleteController',
                    controllerAs: 'warehousesDelete'
            })
        }

        function viewWarehouses(row){
            // $scope.userinfo = UserInfoConstant[0].details;
            // $scope.simulationData = row.entity;
            $scope.data=$scope.warehousesGrid.data;
            $scope.index = $scope.warehousesGrid.data.indexOf(row.entity);
            var modalInstance  = $modal.open({
                    template: require('../view/warehouses-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'warehousesViewController',
                    controllerAs: 'warehousesView',
            })
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

warehousesListController.$inject = ['$scope','UserInfoConstant','$timeout','warehousesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

