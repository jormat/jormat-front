/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> warehousesServices
* file for call at services for warehouses
*/

class warehousesServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var warehouses = $resource(SERVICE_URL_CONSTANT.jormat + '/warehouses/:route',{},{
      
      getWarehouses: {
        method:'GET',
        params : {
          route: 'list'
          }
        },

      getWarehousesByUser: {
        method:'GET',
        params : {
          route: 'listByUser'
          }
        }
    })

    return { warehouses : warehouses
             
           }
  }
}

  warehousesServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.warehousesServices', [])
  .service('warehousesServices', warehousesServices)
  .name;