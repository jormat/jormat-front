
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> orderCreateController
* # As Controller --> orderCreate														
*/

export default class orderCreateController{

        constructor($scope,UserInfoConstant,$timeout,orderRequestServices,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,$modal){
            this.$mdDialog = $mdDialog
            var vm = this;
            vm.createInvoice = createInvoice
            vm.validateType = validateType
            vm.validateCurrency = validateCurrency
            vm.setName = setName
            $scope.destinationView = false
            vm.setWarehouseId = setWarehouseId
            vm.setDestinationId = setDestinationId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.loadItems = loadItems
            vm.loadItemCSV = loadItemCSV
            vm.removeItems = removeItems
            vm.callWarehouses = callWarehouses
            vm.showPanel = showPanel
            vm.getNet = getNet
            vm.originSelectView = originSelectView
            $scope.internacionalInvoice = false
            $scope.nacionalInvoice = true
            $scope.parseInt = parseInt
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.priceVATest = 56000
            $scope.editables = {
      			    disccount: '0',
      			    quantity: '1'
      			}

            function validateCurrency(value){
                if (value == 'Euro') {
                    $scope.currencySymbol = '€'
                    $scope.currencyText = 'EUR'
                }else{
                    $scope.currencySymbol = '$'
                    $scope.currencyText = 'USD'
                }
            }


            function callWarehouses(){

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                                userId : $scope.userInfo.id
                            }

                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                          $scope.warehouses = dataReturn.data;
                          },(err) => {
                              console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
                
                //

                warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
                  $scope.warehousesTo = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                })

            }

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 // '<a class="label bg-accent" ng-click="grid.appScope.orderCreate.loadItem(row.entity)">+</a>' +
                                 '<button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.orderCreate.loadItem(row.entity)" type="button" class="btn btn-accent btn-xs">+</button>'+
                                 '</div>'
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.orderCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '44%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '39%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  
                    }
                ]
            };

            function setWarehouseId(warehouseId){
                $scope.warehouseId = warehouseId
                loadItems(warehouseId)
                if ($scope.warehouseId == $scope.destinationId) {
                        ngNotify.set('El destino no puede ser igual al origen','error')
                        $scope.panelOrder = false
                    }else{
                       $scope.panelOrder = true
                       $scope.invoiceItems = []
                    }
                
            }

            function setDestinationId(warehouseId){
                $scope.destinationId = warehouseId
            }

            function loadItems(warehouseId){
                $scope.buttonMore = true
                let params = {
                    warehouseId: warehouseId
                }

            itemsServices.items.getGeneralStock(params).$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
                  $scope.buttonMore = false
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
                  $scope.buttonMore = false
              })

            }
            
            function validateType(type) {

              $scope.providerName = ''
              $scope.providerId = ''
              callWarehouses()
              $scope.typeView = true
              $scope.typeOrder = type
              $scope.panelOrder = false
              $scope.warehouseId = null
              $scope.valueCurrency = ''
              $scope.invoiceItems = []
                if (type == 2) {

                    providersServices.providers.getProviders().$promise.then((dataReturn) => {
                        $scope.providers = dataReturn.data;
                        },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                    })
                    $scope.internacionalInvoice = true
                    $scope.nacionalInvoice = false
                    $scope.originView = false
                    $scope.destinationView = false
                    $scope.destinationId = ''
                    confirm("Usted a seleccionado tipo de solicitud de pedido Internacional, procure completar los datos del proveedor y origen de manera correcta junto con los valores de la divisa.  ¿Desea Continuar? ")

                }
                if (type == 1) {
                    
                    providersServices.providers.getProviders().$promise.then((dataReturn) => {
                        $scope.providers = dataReturn.data;
                        },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                    })

                    $scope.internacionalInvoice = false
                    $scope.nacionalInvoice = true
                    $scope.originView = true
                    $scope.currencyText = 'CPL'
                    $scope.currencySymbol = '$'
                    $scope.destinationView = false
                    $scope.destinationId = ''
                    confirm("Usted a seleccionado tipo de solicitud de pedido Nacional, procure completar los datos del proveedor y origen de manera correcta.  ¿Desea Continuar? ")
                }
                if (type == 3) {

                    warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
                      $scope.providers = dataReturn.data;
                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                    })

                    $scope.internacionalInvoice = false
                    $scope.nacionalInvoice = true
                    $scope.originView = true
                    $scope.currencyText = 'CPL'
                    $scope.currencySymbol = '$'
                    $scope.destinationView = true
                    confirm("Usted a seleccionado tipo de solicitud de pedido Abastecimiento, procure completar los datos del proveedor, origen y destino de manera correcta.  ¿Desea Continuar? ")

                    
                }
            }


            $scope.invoiceItems = []

            function loadItem(row){

              var i = 0
              for (i=0; i<$scope.invoiceItems.length; i++) {
                    var id = $scope.invoiceItems[i].itemId

                    if(id == row.itemId){
                      ngNotify.set('Item '+ row.itemId +' Duplicado en este Documento','warn')
                      $scope.invoiceItems.splice(index, 1)

                    }else{
                      console.log("item no puplicado",row.itemId)
                    }
                    
                  }
                console.log(row)
                 $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    referenceKey: row.referenceKey,
                    stock: row.stock,
                    generalStock: row.generalStock,
                    price: row.netPrice,
                    quantity: 1,
                    brand: row.brand

                });

                 $scope.warningItems = false    
            }


            function loadItemCSV(row){

                let params = { 
                    itemId: row.itemId,
                    warehouseId: $scope.warehouseId
                }

                itemsServices.items.getGeneralStockByItemId(params).$promise.then((dataReturn) => {
                    $scope.item = dataReturn.data;
                    console.log($scope.item,'$scope.item')

                    $scope.invoiceItems.push({
                        itemId: row.itemId,
                        itemDescription: row.itemDescription,
                        referenceKey : row.referenceKey,
                        stock: $scope.item[0].stock,
                        generalStock: $scope.item[0].generalStock,
                        price: Number(row.netPrice),
                        minPrice: Number(row.netPrice),
                        maxDiscount : 0,
                        quantity: Number(row.quantity),
                        brand: $scope.item[0].brand
     
                    });

                },(err) => {
                    console.log('No se ha podido conectar con el servicio',err);
                })

                $scope.warningItems = false 

                  
           }

            function getNet() {
                
                var net = 0
                  var i = 0
                     for (i=0; i<$scope.invoiceItems.length; i++) {
                         var valueItems = $scope.invoiceItems[i] 
                         net += Math.round(valueItems.price * valueItems.quantity); 

                      }

                      if ($scope.typeOrder != 2){

                        $scope.NET =  net
                        $scope.netoInvoice = net 
                        $scope.total = Math.round($scope.netoInvoice * 1.19)
                        $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                        return net;

                        }else{

                            $scope.NET =  net
                            $scope.netoInvoice = net
                            $scope.total = Math.round($scope.netoInvoice)
                            $scope.VAT = 0
                            return net;
                        }
                             
            }


            function removeItems(index) {

                $scope.invoiceItems.splice(index, 1);
            }

            function setName(providers) {

                $scope.providerName = providers.providerName
                $scope.providerId = providers.providerId
                
            }

            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }


            function originSelectView(){
                $scope.originView = true
            }

            function showPanel(value) {
             $scope.panelInvoice = true

             if (value == 1) {
                    if ($scope.typeOrder != 3 ) {
                        $scope.panelOrder = true
                        $scope.invoiceItems = []

                    }else{
                        $scope.panelOrder = false
                    }
                }else{
                    if ($scope.warehouseId == $scope.destinationId) {
                        ngNotify.set('El destino no puede ser igual al origen','error')
                        $scope.panelOrder = false
                    }else{
                       $scope.panelOrder = true
                       $scope.invoiceItems = []
                    }
                    
                }

            }

            function createInvoice() {

                        let model = {
                            userId: $scope.userInfo.id,
                            providerId : $scope.providerId,
                            originId : $scope.warehouseId,
                            destinationId : $scope.destinationId,
                            type: $scope.selected,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            total : $scope.total,
                            comment : $scope.observation,
                            currency : $scope.currency,
                            valueCurrency : $scope.valueCurrency,
                            itemsInvoices : $scope.invoiceItems 
                        }   

                        console.log('modelo',model)

                        orderRequestServices.orders.createOrder(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha creado el pedido correctamente','success')
                            $state.go('app.orders')
                          },(err) => {
                              ngNotify.set('Error al crear pedido','error')
                          })
                    }
           
        }//FIN CONSTRUCTOR

        uploadCsv() {
            this.confirm = this.$mdDialog.confirm({
                 controller         : 'uploadCsvOrdersController',
                 controllerAs       : 'uploadCsv',
                 template           : require('./dialogs/uploadCsv/upload-csv-orders.html'),
                 locals              : {},
                 animation          : true,
                 parent             : angular.element(document.body),
                 clickOutsideToClose: true,
             })
             this.$mdDialog.show(this.confirm).then((row) => {
                 if (row) {
                    const dataReturn = row.data
                    if(dataReturn.length < 2) {
                        this.loadItemCSV(dataReturn[0])
                    }else {
                        for(const i in dataReturn){
                            this.loadItemCSV(dataReturn[i])
                        }
                    }
                    
                 }
             })
         }

        // Funciones
        

    }   

orderCreateController.$inject = ['$scope','UserInfoConstant','$timeout','orderRequestServices','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','$modal'];

