
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> orderPrintController
* # As Controller --> 
														
*/


  export default class orderPrintController {

    constructor($scope, $filter,$rootScope, $http,orderRequestServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.validateOrder = validateOrder
        // vm.transformToorder = transformToorder
        $scope.orderId = $stateParams.idOrder

        let params = {
                orderId: $stateParams.idOrder,
                warehouseId: $stateParams.originId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        orderRequestServices.orders.getOrderDetails(params).$promise.then((dataReturn) => {
              $scope.order = dataReturn.data
              if ($scope.order[0].type == "Abastecimiento") {
                    $scope.typeSupplies = true
                }else{
                    $scope.typeSupplies = false

                }

                if ($scope.order[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.order[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                        $scope.currencyText = 'EUR'
                    }else{
                        $scope.currencySymbol = '$'
                        $scope.currencyText = 'USD'
                    }

                }else{
                    $scope.internationalType = false
                    $scope.currencyText = 'CPL'
                    $scope.currencySymbol = '$'
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get order Details',err);
              })

        let model = {
                orderId: $stateParams.idOrder,
                originId: $stateParams.originId,
                destinationId: $stateParams.destinationId
            }

        orderRequestServices.orders.getOrderItems(model).$promise.then((dataReturn) => {
              $scope.orderItems = dataReturn.data

              var subTotal = 0
              var i = 0
                 for (i=0; i<$scope.orderItems.length; i++) {
                     subTotal = subTotal + $scope.orderItems[i].total  
                  }
                  
                $scope.subTotality =  subTotal

              },(err) => {
                  console.log('No se ha podido conectar con el servicio get order items',err);
              })

    	function cancel() {

		 $state.go("app.orders")

		}

        function validateOrder(value) {
            
         let model = {
                    orderId : $scope.orderId,
                    userId : $scope.userInfo.id,
                    statusValue : value 
                }

                console.log('that',model)

                if ($scope.order[0].type == "Abastecimiento" && value ==2) {
                    $state.go("app.orderValidate", { orderId: $scope.orderId, originId: $stateParams.destinationId , destinationId: $stateParams.originId });

                }else{

                  orderRequestServices.orders.validateOrder(model).$promise.then((dataReturn) => {

                    ngNotify.set('solicitud validada exitosamente','success');
                    $state.go("app.orders")
                  },(err) => {
                      ngNotify.set('Error al validar orden','error')
                  })

                }

                
        }

        // function transformToorder(row){

        //   $state.go("app.clientsordersUpdate", { idorder: $scope.quotationId,discount:$scope.quotations[0].discount,type:'cotizacion'});

        // }

        function print() {
              window.print();

        }


    }

  }

  orderPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','orderRequestServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
