/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> invoicesProviderDeleteController
* # As Controller --> invoicesProviderDelete														
*/


  export default class invoicesProviderDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,invoicesProviderServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.deleteInvoice = deleteInvoice
	    $scope.invoiceId = $scope.invoiceData.invoiceId
	    $scope.rut = $scope.invoiceData.rut
	    $scope.providerName = $scope.invoiceData.providerName

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
        //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteInvoice() {
			let model = {
                    invoiceId : $scope.invoiceId,
                    userId : $scope.userInfo.id
                }

                invoicesProviderServices.invoicesProviders.disabledInvoice(model).$promise.then((dataReturn) => {
                    ngNotify.set('Factura eliminada exitosamente','success');
					         $modalInstance.dismiss('chao');
                             $state.reload("app.invoicesProvider");
                  },(err) => {
                      ngNotify.set('Error al eliminar factura','error')
                  })
		}
	    


    }

  }

  invoicesProviderDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','invoicesProviderServices','$location','UserInfoConstant','ngNotify','$state'];
