
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> invoicesProviderUpdateController
* # As Controller --> invoicesProviderUpdate														
*/

export default class invoicesProviderUpdateController{

        constructor($scope,UserInfoConstant,$timeout,invoicesProviderServices,providersServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams,warehousesServices,itemsServices){
            var vm = this
            vm.removeItems = removeItems
            vm.updateInvoice = updateInvoice
            vm.setName = setName
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

            providersServices.providers.getProviders().$promise.then((dataReturn) => {
                  $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            $scope.invoiceId = $stateParams.idInvoice

            let params = {
                invoiceId: $scope.invoiceId 
            }

            invoicesProviderServices.invoicesProviders.getInvoicesDetails(params).$promise.then((dataReturn) => {
              $scope.invoice = dataReturn.data;
              

              $scope.dataInvoice  = { 
                providerName : $scope.invoice[0].providerName,
                providerId : $scope.invoice[0].providerId,
                invoiceCode : $scope.invoice[0].invoiceCode,
                rut : $scope.invoice[0].rut,
                date : $scope.invoice[0].date,
                purchaseOrder : $scope.invoice[0].purchaseOrder,
                status : $scope.invoice[0].statusName,
                priceVAT : $scope.invoice[0].priceVAT,
                netPrice : $scope.invoice[0].netPrice,
                quantity : $scope.invoice[0].quantity,
                total : $scope.invoice[0].total,
                observation : $scope.invoice[0].comment
              }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
              $scope.warehouses = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getWarehouses',err);
            })

            itemsServices.items.getItems().$promise.then((dataReturn) => {
         		  $scope.items = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getItems',err);
              })


        function setName(providers) {
                
                $scope.dataInvoice  = { 
                    providerName : providers.providerName,
                    providerId : providers.providerId
                }
            }   

        function removeItems(index) {

                $scope.invoice[0].itemsInvoices.splice(index, 1);
            }

        function updateInvoice() {

                let model = {
                    userId: $scope.userInfo.id,
                    invoiceId: $stateParams.idInvoice,
                    clientId : $stateParams.idclient,
                    date : $scope.dataInvoice.date,
                    purchaseOrder : $scope.dataInvoice.purchaseOrder,
                    priceVAT : $scope.dataInvoice.priceVAT,
                    netPrice : $scope.dataInvoice.netPrice,
                    originId : $scope.warehouses.selected,
                    total : $scope.dataInvoice.total,
                    comment : $scope.dataInvoice.observation,
                    itemsInvoices : [{}] 
                }      

                invoicesProviderServices.invoicesProviders.updateInvoice(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha actualizado la factura correctamente','success')
                    $state.go('app.invoicesProvider')
                  },(err) => {
                      ngNotify.set('Error al actualizar factura','error')
                  })
            }
            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

invoicesProviderUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','invoicesProviderServices','providersServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams','warehousesServices','itemsServices'];

