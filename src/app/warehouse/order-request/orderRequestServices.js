/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> orderRequestServices
* file for call at services for providers
*/

class orderRequestServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var orders = $resource(SERVICE_URL_CONSTANT.jormat + '/orders/:route',{},{
      
        getOrders: {
            method:'GET',
            params : {
               route: 'list'
            }
        },

        getOrderItems: {
            method:'GET',
            params : {
               route: 'items'
            }
        },
        getOrderDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createOrder: {
            method:'POST',
            params : {
                route: ''
            }
        },
        updateOrder: {
            method:'PUT',
            params : {
                route: ''
            }
        },
        disabledOrder: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        validateOrder: {
            method:'PUT',
            params : {
                route: 'validate'
            }
        }
    })

    return { 

            orders : orders
           }
  }
}

  orderRequestServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.orderRequestServices', [])
  .service('orderRequestServices', orderRequestServices)
  .name;


  