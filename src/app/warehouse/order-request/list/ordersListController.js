
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> ordersListController
* # As Controller --> ordersList														
*/

export default class ordersListController{

        constructor($scope,UserInfoConstant,$timeout,orderRequestServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.viewOrder = viewOrder
            vm.printOrder = printOrder
            vm.searchData=searchData
            vm.loadOrders= loadOrders
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');
            loadOrders()

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.ordersGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_pedidos.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'orderId',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.ordersList.viewOrder(row.entity)">{{row.entity.orderId}}</a>' +
                                 '</div>'    
                    },
                    { 
                      name:'Proveedor',
                      field: 'providerName',
                      width: '33%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.providerName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.total}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '9%'
                    },
                    { 
                      name: 'Destino', 
                      field: 'destination', 
                      width: '9%'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p style="font-size: 13px;" > <strong>{{row.entity.type }}</strong></p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Moneda', 
                      field: 'currency',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" label bg-secundary" > {{row.entity.currency }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<span class="{{(row.entity.statusName == \'Rechazada\')?\'label bg-danger\':\'label bg-warning\'}}">{{row.entity.statusName}}</span>'+
                                 '</div>'
                    },
                    // { 
                    //   name: 'Usuario', 
                    //   field: 'userCreation', 
                    //   width: '12%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class=" badge bg-warning" style="font-size: 11px;">{{row.entity.userCreation}}</p>' +
                    //              '</div>'   
                    // },
                    
                    { 
                      name: '',
                      width: '6%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadOrders(){

              orderRequestServices.orders.getOrders().$promise.then((dataReturn) => {
                $scope.ordersGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

        
            function searchData() {
              orderRequestServices.orders.getOrders().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.ordersGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewOrder(row){
	            $scope.orderData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/order-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'orderViewController',
	                    controllerAs: 'orderView',
	                    size: 'lg'
	            })
	       }

            function printOrder(row){
             $state.go("app.ordersPrint", { idOrder: row.orderId,originId: row.originId,destinationId: row.destinationId});
            }

        
            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

ordersListController.$inject = ['$scope','UserInfoConstant','$timeout','orderRequestServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

