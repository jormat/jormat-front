/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> orderViewController
* # As Controller --> orderView														
*/


  export default class orderViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,orderRequestServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.validateOrder = validateOrder
        $scope.currencySymbol = '$'
        $scope.orderId = $scope.orderData.orderId
        $scope.providerName = $scope.orderData.providerName 
        $scope.warehouseId = $scope.orderData.originId 
        $scope.destionationId = $scope.orderData.destionationId 

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })
      
        let params = {
            orderId: $scope.orderId,
            warehouseId: $scope.warehouseId
          }

        orderRequestServices.orders.getOrderDetails(params).$promise.then((dataReturn) => {
              $scope.order = dataReturn.data
                if ($scope.order[0].type == 'Internacional') {
                    $scope.internationalType = true
                    if ($scope.order[0].currency == 'Euro') {
                        $scope.currencySymbol = '€'
                    }else{
                        $scope.currencySymbol = '$'
                    }

                }else{
                    $scope.internationalType = false
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get orders Details',err);
              })

        let model = {
                orderId: $scope.orderId,
                originId: $scope.warehouseId,
                destinationId: $scope.destinationId
            }

        orderRequestServices.orders.getOrderItems(model).$promise.then((dataReturn) => {
              $scope.orderItems = dataReturn.data
              var subTotal = 0
                  var i = 0
                     for (i=0; i<$scope.orderItems.length; i++) {
                         subTotal = subTotal + $scope.orderItems[i].total  
                      }
                      
                      $scope.subTotality =  subTotal
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get getordersItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function validateOrder(value) {
         let model = {
                    orderId : $scope.orderId,
                    userId : $scope.userInfo.id,
                    statusValue : value 
                }

                console.log('that',model)

                orderRequestServices.orders.validateOrder(model).$promise.then((dataReturn) => {

                    ngNotify.set('solicitud validada exitosamente','success');
                    $modalInstance.dismiss('chao');
                    $state.reload("app.orders")
                  },(err) => {
                      ngNotify.set('Error al validar orden','error')
                  })
        }

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

		
    }

  }

  orderViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','orderRequestServices','$location','UserInfoConstant','ngNotify','$state'];
