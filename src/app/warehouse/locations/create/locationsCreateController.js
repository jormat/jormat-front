
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> locationsCreateController
* # As Controller --> locationsCreate														
*/

export default class locationsCreateController{

        constructor($scope,UserInfoConstant,$timeout,locationsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices){
            var vm = this;
            vm.createLocation = createLocation
            vm.setId = setId
            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //
            
            function setId(warehouseId){
                $scope.warehouseId = warehouseId
            }

         	function createLocation() {

                let model = {
                  locationCode: $scope.locationCode,
                  locationName: $scope.locationName,
                  warehouseId: $scope.warehouseId
                }

                locationsServices.locations.createLocation(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado la ubicación correctamente','success')
                    $state.go('app.locations')
                  },(err) => {
                      ngNotify.set('Error al crear la ubicación','error')
                })

            }
            
        }//FIN CONSTRUCTOR

        // Funciones
    }   

locationsCreateController.$inject = ['$scope','UserInfoConstant','$timeout','locationsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices'];

