/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> locationsDeleteController
* # As Controller --> locationsDelete														
*/


  export default class locationsDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,locationsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.deletelocations = deletelocations;
	    // vm.user=$scope.userinfo[0].user.id;
	    $scope.locationId = $scope.locationData.locationId
	    $scope.locationCode = $scope.locationData.locationCode
	    $scope.locationName = $scope.locationData.locationName

	    vm.$translate = $filter('translate')
	        $rootScope.$on('$translateChangeSuccess', function () {
	   
	        })

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deletelocations() {
			let model = {  
                    "locationId": $scope.locationId,
                    "userId": 1
                }

            locationsServices.locations.disabledLocation(model).$promise.then((dataReturn) => {
                ngNotify.set('Se ha inactivado la ubicación correctamente','success')
                $modalInstance.dismiss('chao');
                $state.reload('app.locations')
              },(err) => {
                  ngNotify.set('Error al inactivar ubicación','error')
            })
		}

		


    }

  }

  locationsDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','locationsServices','$location','UserInfoConstant','ngNotify','$state' ];
