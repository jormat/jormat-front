/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> locationsServices
* file for call at services for locations
*/

class locationsServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var locations = $resource(SERVICE_URL_CONSTANT.jormat + '/locations/:route',{},{
      
      getLocations: {
        method:'GET',
        params : {
          route: 'list'
          }
        },

      viewLocation: {
        method:'GET',
        params : {
          route: ''
          }
        },

      createLocation: {
        method:'POST',
        params : {
          route: ''
          }
        },

      updateLocation: {
        method:'PUT',
        params : {
          route: ''
          }
        },

      disabledLocation: {
        method:'DELETE',
        params : {
          route: ''
          }
        }
    })

    return { locations : locations
             
           }
  }
}

  locationsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.locationsServices', [])
  .service('locationsServices', locationsServices)
  .name;