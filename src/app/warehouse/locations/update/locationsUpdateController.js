
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> locationsUpdateController
* # As Controller --> locationsUpdate														
*/

export default class locationsUpdateController{

        constructor($scope,UserInfoConstant,$timeout,locationsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,$stateParams){
            var vm = this;
            vm.updateLocation = updateLocation

             //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            $scope.locationName  = $stateParams.name
            $scope.locationCode  =  $stateParams.code
            $scope.locationId  =  $stateParams.id
            $scope.warehouse  =  $stateParams.warehouse
          
            function updateLocation() {

                    let model = {
                        locationId: $scope.locationId,
                        locationCode: $scope.locationCode,
                        locationName: $scope.locationName 
                    }

                    locationsServices.locations.updateLocation(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado la ubicación correctamente','success')
                        $state.go('app.locations')
                      },(err) => {
                          ngNotify.set('Error al actualizar ubicación','error')
                    })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

locationsUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','locationsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','$stateParams'];

