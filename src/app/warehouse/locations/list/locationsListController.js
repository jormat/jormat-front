
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> locationsListController
* # As Controller --> locationsList														
*/

export default class locationsListController{

        constructor($scope,UserInfoConstant,$timeout,locationsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.searchData=searchData;
            vm.deleteLocations= deleteLocations;
            vm.viewLocations= viewLocations
            vm.updateLocations = updateLocations
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')

            $scope.locationsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'locationId',  
                      width: '15%' 
                    },
                    { 
                      name:'Codigo',
                      field: 'locationCode',
                      width: '20%' 
                    },
                    { 
                      name:'Nombre ubicacion',
                      field: 'locationName',
                      width: '30%' 
                    },
                    { 
                      name:'Bodega',
                      field: 'warehouseName',
                      width: '20%' 
                    },
                    { 
                      name: 'Acciones',
                      width: '10%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
         		locationsServices.locations.getLocations().$promise.then((dataReturn) => {
         		  $scope.locationsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            function searchData() {
              locationsServices.locations.getLocations().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.locationsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

            function deleteLocations(row){
            $scope.locationData = row;
            $scope.index = $scope.locationsGrid.data.indexOf(row.entity);
            var modalInstance  = $modal.open({
                    template: require('../delete/locations-delete.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'locationsDeleteController',
                    controllerAs: 'locationsDelete'
            })
        }

        function viewLocations(row){
            $scope.locationData = row;
            var modalInstance  = $modal.open({
                    template: require('../view/locations-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'locationsViewController',
                    controllerAs: 'locationsView',
            })
        }

        function updateLocations(row){
            $state.go("app.locationsUpdate", { id:row.locationId,name: row.locationName,code:row.locationCode,warehouse: row.warehouseName});
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

locationsListController.$inject = ['$scope','UserInfoConstant','$timeout','locationsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

