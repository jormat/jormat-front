/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> locationsViewController
* # As Controller --> locationsView														
*/


  export default class locationsViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,locationsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.edit = edit;
	    $scope.locationId = $scope.locationData.locationId
	    $scope.locationName = $scope.locationData.locationName
	    $scope.locationCode = $scope.locationData.locationCode
	    $scope.warehouseName = $scope.locationData.warehouseName

	    let params = {
            locationId: $scope.locationId
          }

        locationsServices.locations.viewLocation(params).$promise.then((dataReturn) => {
         		  $scope.location = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function edit() {
			$state.go("app.locationsUpdate");
		}

		


    }

  }

  locationsViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','locationsServices','$location','UserInfoConstant','ngNotify','$state'];
