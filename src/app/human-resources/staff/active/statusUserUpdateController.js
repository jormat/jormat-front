/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> statusUserUpdateController
* # As Controller --> statusUserUpdate														
*/


  export default class statusUserUpdateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,staffServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.updateStatus = updateStatus;
	    $scope.userId = $scope.userId
        $scope.fullName = $scope.fullName
        $scope.status = $scope.status
        console.log('estado',$scope.status);

        if ($scope.status == 'Inactivo'){
           $scope.text = "Activar"
           $scope.value = 1
        }else{
            $scope.text = "Desactivar"
            $scope.value = 0
        }
        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
        //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function updateStatus() {
			let model = {
                    userId : $scope.userId,
                    value: $scope.value
                }

            staffServices.users.statusUserUpdate(model).$promise.then((dataReturn) => {
                ngNotify.set('Se ha actualizado el usuario correctamente','success')
                $modalInstance.dismiss('chao');
                $state.reload('app.staffUpdate')
              },(err) => {
                  ngNotify.set('Error al actualizar usuario','error')
            })
		}
    }

  }

  statusUserUpdateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','staffServices','$location','UserInfoConstant','ngNotify','$state'];
