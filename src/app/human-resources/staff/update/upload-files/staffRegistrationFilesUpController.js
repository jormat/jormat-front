/**
* @name APP jormat
* @autor 
* @description
* # name controller --> staffRegistrationFilesUpController
* # As Controller --> staffFiles														
*/
export default class staffRegistrationFilesUpController {
	constructor($rootScope, $scope, $mdDialog, $state, $stateParams, SERVICE_URL_CONSTANT, ngNotify, FileUploader, userId) {
		this.$rootScope = $rootScope
		this.$scope = $scope
		this.$mdDialog = $mdDialog
		this.$state = $state
		this.ngNotify = ngNotify
		this.$stateParams = $stateParams
		this.baseUrl = SERVICE_URL_CONSTANT
		this.data = []
		this.progressbarClass = 'active'
		this.loadProcess = {
			response: {},
			timer: null,
			currentStatus: 1,
			status: "Subiendo archivo al servidor",
		}
		$scope.uploader = new FileUploader()
		$scope.uploader.url = this.baseUrl.jormat + '/users/uploadFiles/' + userId,
			$scope.uploader.method = 'POST'
		$scope.uploader.alias = 'files'

		$scope.uploader.filters.push({
			name: 'customFilter',
			fn: function (item, options) {

				return this.queue.length < 4 && ('.pdf' === item.name.substr(-4, 4) || '.jpg' === item.name.substr(-4, 4) || 
					'.jpeg' === item.name.substr(-5, 5) || '.doc' === item.name.substr(-4, 4) || '.docx' === item.name.substr(-5, 5) || 
					'.xls' === item.name.substr(-4, 4) || '.png' === item.name.substr(-4, 4) || '.xlsx' === item.name.substr(-5, 5)) 
			}
		})

		$scope.uploader.onAfterAddingFile = (fileItem) => {

			$scope.uploader.uploadAll()

		}

		$scope.uploader.onSuccessItem = (fileItem, response, status, headers) => {
			this.loadProcess.response = response
			// this.fileId = this.loadProcess.response.id_file
			this.data = response.data
			if (response.status) {
				this.progressbarClass = 'success'
				this.progressLoad = 100
				ngNotify.set('Correcta carga', 'success')
			} else {
				ngNotify.set('Error en carga de archivo', 'error')
			}
		}

		$scope.uploader.onErrorItem = (fileItem, response, status, headers) => {
			this.progressbarClass = 'error'
			ngNotify.set(response.error + ', ErrorCod: ' + status, 'error')
			if (response.status == 0) {
				this.error(response.error)
			}
		}

		$scope.uploader.onProgressItem = (item, progress) => {
			this.progressLoad = progress > 0 ? progress / 2 : 0
		}

		setTimeout(() => {
			this.main()
		}, 1000)
	}

	main() {
		const filename = 'staff-register'
		this.uploadOptions = {
			type: '',
			title: 'Cargar Documento',
			headerColor: 'bg-primary',
			btnColor: 'bg-primary',
			pathCsvDownload: '&',
			csvDownloadFilename: filename,
			service: '',
			paramsToGoBack: {},
			loadParams: {
				userId: 1,
				sectionId: 2,
			},
			alias: 'fileUpload',
			grid: {
				useExternalPagination: false,
			},
		}
	}

	clickUpload() {
		uploadButton.click()
	}

	close() {

		this.$mdDialog.hide(false)
	}
}
staffRegistrationFilesUpController.$inject = ['$rootScope', '$scope', '$mdDialog', '$state', '$stateParams', 'SERVICE_URL_CONSTANT', 'ngNotify', 'FileUploader', 'userId']