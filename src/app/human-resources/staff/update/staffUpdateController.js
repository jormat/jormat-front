/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> staffUpdateController
* # As Controller --> staffUpdate														
*/


  export default class staffUpdateController {

    constructor($scope, $filter,$rootScope, $http,staffServices,$location,UserInfoConstant,ngNotify,$state,warehousesServices,$stateParams,$modal,banksServices, $mdDialog, SERVICE_URL_CONSTANT) {
		this.$mdDialog = $mdDialog
		this.ngNotify = ngNotify
    	var vm = this;
	    vm.setRole = setRole;
        vm.checkRut = checkRut;
        vm.updateUserInfo = updateUserInfo;
        vm.viewPass = viewPass; 
        vm.saveInfo = saveInfo
        vm.updateUserInfoOrganization = updateUserInfoOrganization
        vm.updateUserInfoAccess = updateUserInfoAccess
        vm.documentsLoad = documentsLoad
        vm.userActive = userActive
        vm.isapresShowList = isapresShowList 
        vm.updateUserInfoInstitutions = updateUserInfoInstitutions
        vm.updateUserBlockPersonalInfo = updateUserBlockPersonalInfo
		vm.uploadFiles = uploadFiles
		vm.openFile = openFile
		vm.deleteFile = deleteFile
		vm.viewImage = viewImage
        $scope.userId = $stateParams.idUser
        $scope.picture = require("../images/no_user.jpg")
        $scope.rutValidate = true
        $scope.passText = false
        $scope.passInput = true 
        $scope.organizationalInformationInfo = true
		$scope.files = [];
		$scope.getFiles = function () {
			const filesUrl = SERVICE_URL_CONSTANT.jormat + `/users/uploadFiles/` + $scope.userId
			$http.get(filesUrl).then(function(response) {
				$scope.files = response.data
			}); 
		}

        $scope.$watch('files', () => {
            $scope.getFiles()
		}, true)

        // afp's

        staffServices.users.getRetirements().$promise.then((dataReturn) => {
                  $scope.retirements = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
        //isapres

        staffServices.users.getIsapres().$promise.then((dataReturn) => {
                      $scope.isapres = dataReturn.data;
                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                  })

        //compensation-box

        staffServices.users.getCompensationBox().$promise.then((dataReturn) => {
                  $scope.compensationBox = dataReturn.data;
                  
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

        //banks

        banksServices.banks.getBanks().$promise.then((dataReturn) => {
                  $scope.banks = dataReturn.data;
                  console.log('data banks',$scope.banks)
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

        function isapresShowList(healthSystemName){
            if (healthSystemName == "ISAPRE") {
                $scope.isapresList = true
            } else { $scope.isapresList = false}
        }


        function checkRut(rut) {
                // Despejar Puntos
                var valor = rut.replace('.','');
                // Despejar Guión
                valor = valor.replace('-','');
                
                // Aislar cuerpo y Dígito Verificador
                $scope.cuerpo = valor.slice(0,-1);
                $scope.dv = valor.slice(-1).toUpperCase();
                
                // Formatear RUN
                rut = $scope.cuerpo + '-'+ $scope.dv
                
                // Si no cumple con el mínimo ej. (n.nnn.nnn)
                if($scope.cuerpo.length < 7) { 
                    // rut.setCustomValidity("RUT Incompleto");
                    $scope.textValidation = "Incompleto";
                    $scope.rutValidate = true;
                    console.log("rut Incompleto");  
                    return false;
                }
                
                // Calcular Dígito Verificador
                $scope.suma = 0;
                $scope.multiplo = 2;
                
                // Para cada dígito del $scope.cuerpo

                for($scope.i=1;$scope.i<=$scope.cuerpo.length;$scope.i++) {
                
                    // Obtener su Producto con el Múltiplo Correspondiente
                    $scope.index = $scope.multiplo * valor.charAt($scope.cuerpo.length -$scope.i);
                    
                    // Sumar al Contador General
                    $scope.suma = $scope.suma + $scope.index;
                    
                    // Consolidar Múltiplo dentro del rango [2,7]
                    if($scope.multiplo < 7) { $scope.multiplo = $scope.multiplo + 1; } else { $scope.multiplo = 2; }
              
                }
                
                // Calcular Dígito Verificador en base al Módulo 11
                $scope.dvEsperado = 11 - ($scope.suma % 11);
                
                // Casos Especiales (0 y K)
                $scope.dv = ($scope.dv == 'K')?10:$scope.dv;
                $scope.dv = ($scope.dv == 0)?11:$scope.dv;
                
                // Validar que el $scope.cuerpo coincide con su Dígito Verificador
                if($scope.dvEsperado != $scope.dv) { 
                    // rut.setCustomValidity("RUT Inválido");
                    console.log("RUT Inválido"); 
                    $scope.textValidation = "RUT Inválido";
                    $scope.rutValidate = true;
                    return false; 
                }
                
                // Si todo sale bien, eliminar errores (decretar que es válido)
                // rut.setCustomValidity('');
                $scope.textValidation = "RUT Válido";
                console.log("RUT valido");
                $scope.rutValidate = false
            }

        let params = {
            userId: $stateParams.idUser
          }


        staffServices.users.getUsersDetails(params).$promise.then((dataReturn) => {
                  $scope.user = dataReturn.data
                  $scope.userDetails  = { 
                        fullName : $scope.user[0].fullName, 
                        userName : $scope.user[0].userName,
                        rut : $scope.user[0].rut,
                        cityName : $scope.user[0].cityName,
                        address : $scope.user[0].address,
                        email : $scope.user[0].email,
                        phone : $scope.user[0].phone,
                        rol : $scope.user[0].rol,
                        birthdayDate : $scope.user[0].birthdayDate,
                        password : $scope.user[0].password,
                        day : $scope.user[0].day,
                        month : $scope.user[0].month,
                        year : $scope.user[0].year,
                        gender:$scope.user[0].gender,
                        genderId:$scope.user[0].genderId,
                        nationality:$scope.user[0].nationality,
                        startDate:$scope.user[0].startDate,
                        finishDate:$scope.user[0].finishDate,
                        documentType:$scope.user[0].documentType,
                        contractTypeName:$scope.user[0].contractTypeName,
                        retirementId:$scope.user[0].retirementId,
                        retirementName:$scope.user[0].retirementName,
                        healthSystemId:$scope.user[0].healthSystemId,
                        healthSystemName:$scope.user[0].healthSystemName,
                        isapreId:$scope.user[0].isapreId,
                        isapreName:$scope.user[0].isapreName,
                        percentagePlan:$scope.user[0].percentagePlan,
                        compensationBoxId:$scope.user[0].compensationBoxId,
                        compensationBoxName:$scope.user[0].compensationBoxName, 
                        baseSalary:$scope.user[0].baseSalary,
                        costCenterName:$scope.user[0].costCenterName,
                        costCenterId:$scope.user[0].costCenterId,
                        bankId:$scope.user[0].bankId,
                        bankName:$scope.user[0].bankName,
                        bankAccountType:$scope.user[0].bankAccountType,
                        accountNumber:$scope.user[0].accountNumber,
                        maritalStatus:$scope.user[0].maritalStatus,
                        numberOfChildren:$scope.user[0].numberOfChildren,
                        shirtSize:$scope.user[0].shirtSize,
                        shoeSize:$scope.user[0].shoeSize,
                        lastEmployer:$scope.user[0].lastEmployer,
                        scholarShip:$scope.user[0].scholarShip,
                        profession:$scope.user[0].profession,
                        observation:$scope.user[0].observation,
                        isActive:$scope.user[0].isActive
                  }

                  $scope.password2 = $scope.user[0].password
                  $scope.admissionDate = $scope.user[0].startDate
                  $scope.finishDate = $scope.user[0].finishDate
                  $scope.compensationBoxId = $scope.user[0].compensationBoxId
                  isapresShowList($scope.user[0].healthSystemName)

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })


        staffServices.users.getUserWarehouses(params).$promise.then((dataReturn) => {
                  $scope.warehouses = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        staffServices.users.getRole().$promise.then((dataReturn) => {
              $scope.role = dataReturn.data;
              console.log("roles",$scope.role);

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
              $scope.warehousesAll = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        function setRole(roleId){
            $scope.roleId = roleId
            console.log("rolId",$scope.roleId);
        }

        function viewPass(value){

            window.scrollTo(1000, 0)
            if (value == 1){
                $scope.passText = true
                $scope.passInput = false
            }else{
                $scope.passText = false
                $scope.passInput = true
            }


        }

        function saveInfo(value){
            if (value == 1){
                $scope.organizationalInformationInfo = false
                $scope.organizationalInformationUpdate = true
            }else{
                $scope.organizationalInformationInfo = true
                $scope.organizationalInformationUpdate = false
            }
        }
    
	    function updateUserInfo() {

            if ($scope.userDetails.gender == "Masculino"){
                $scope.gender = 1
             }
            else{$scope.gender = 2}

		let model = {
			userId: $scope.userId,
			fullName: $scope.userDetails.fullName,
			rut:  $scope.userDetails.rut,
			phone:  $scope.userDetails.phone,
			birthdayDate: $scope.userDetails.day+"/"+$scope.userDetails.month+"/"+$scope.userDetails.year,
			email:  $scope.userDetails.email,
			address:  $scope.userDetails.address,
			cityName:  $scope.userDetails.cityName,
			day:  $scope.userDetails.day,
			month:  $scope.userDetails.month,
			year:  $scope.userDetails.year,
			gender:  $scope.gender,
			nationality:  $scope.userDetails.nationality,
			startDate:  $scope.userDetails.startDate,
			finishDate:  $scope.userDetails.finishDate


		}

		console.log("modelo",model);

		staffServices.users.userUpdateInfo(model).$promise.then((dataReturn) => {
			ngNotify.set('Se ha actualizado el usuario correctamente','success')
			$state.reload('app.staffUpdate')
			},(err) => {
				ngNotify.set('Error al actualizar usuario','error')
		})
        }

		$scope.myUiSelect={model:{}}; // encapsulate your model inside an object.
		
		$scope.onSelect = function(item,model){
			console.log("selectedItem",item);
		}
		function updateUserInfoOrganization(){

			if ($scope.userDetails.contractTypeName == 'Indefinido') {
					$scope.contractType = 1
			}else{ $scope.contractType = 2}

			let model = {
				userId: $scope.userId,
				contractType: $scope.contractType,
				rolId: $scope.roleId,
				warehouses: $scope.myUiSelect.model
			}

			console.log("modelo",model);

			staffServices.users.userUpdateOrganization(model).$promise.then((dataReturn) => {
				staffServices.users.contractTypeUpdate(model).$promise.then((dataReturn) => {
						},(err) => {
							ngNotify.set('Error al actualizar contrato','error')
					})
				$state.reload('app.staffUpdate')
				ngNotify.set('Se ha actualizado el usuario correctamente','success')
				},(err) => {
					ngNotify.set('Error al actualizar usuario','error')
			})
		}
		function documentsLoad(userId,fullName) {
			$scope.userId = userId;
			$scope.fullName = fullName;
			const modalInstance  = $modal.open({
					template: require('../upload/documents-upload-user.html'),
					animation: true,
					scope: $scope,
					controller: 'documentsUploadUserController',
					controllerAs: 'documentsUploadUser',
					size: 'lg'
			})
		}
		function userActive(userId,fullName,status) {
			$scope.userId = userId;
			$scope.fullName = fullName;
			$scope.status = status;
			const modalInstance  = $modal.open({
					template: require('../active/status-user-update.html'),
					animation: true,
					scope: $scope,
					controller: 'statusUserUpdateController',
					controllerAs: 'statusUserUpdate'
			})
		}
		function updateUserInfoAccess() {

			let model = {
				userId: $scope.userId,
				userName: $scope.userDetails.userName,
				password:  $scope.userDetails.password
			}

			console.log("modelo",model);

			staffServices.users.userUpdateAccess(model).$promise.then((dataReturn) => {
				ngNotify.set('Se ha actualizado el usuario correctamente','success')
				$state.reload('app.staffUpdate')
				},(err) => {
					ngNotify.set('Error al actualizar usuario','error')
			})
		}
		function updateUserBlockPersonalInfo() {

			if ($scope.userDetails.costCenterName == 'MAT') {
					$scope.warehouseId = 1
			}
			if ($scope.userDetails.costCenterName == 'BOD-CHILL') {
					$scope.warehouseId = 2
			}
			if ($scope.userDetails.costCenterName == 'BOD-CONST') {
					$scope.warehouseId = 3
			}
			if ($scope.userDetails.costCenterName == 'BOD-TEM') {
					$scope.warehouseId = 4
			}
			if ($scope.userDetails.costCenterName == 'BOD-CONCE') {
					$scope.warehouseId = 6
			}
			if ($scope.userDetails.costCenterName == 'UNDEFINED') {
					$scope.warehouseId = 7
			}


			let model = {
				userId: $scope.userId,
				maritalStatus: $scope.userDetails.maritalStatus,
				numberOfChildren:  $scope.userDetails.numberOfChildren,
				shirtSize: $scope.userDetails.shirtSize,
				shoeSize: $scope.userDetails.shoeSize,
				warehouseId:  $scope.warehouseId,
				lastEmployer:  $scope.userDetails.lastEmployer,
				scholarShip: $scope.userDetails.scholarShip,
				profession: $scope.userDetails.profession,
				observation: $scope.userDetails.observation 
			}

			console.log("modelo",model);

			staffServices.users.updateUserBlockPersonalInfo(model).$promise.then((dataReturn) => {
				ngNotify.set('Se ha actualizado el usuario correctamente','success')
				$state.reload('app.staffUpdate')
				},(err) => {
					ngNotify.set('Error al actualizar usuario','error')
			})
		}
		function updateUserInfoInstitutions() {

				if ($scope.userDetails.compensationBoxName == 'LOS ANDES') {
					$scope.compensationBoxId = 1
			}
			if ($scope.userDetails.compensationBoxName == 'LA ARAUCANA') {
					$scope.compensationBoxId = 2
			}
			if ($scope.userDetails.compensationBoxName == '18 DE SEPTIEMBRE') {
					$scope.compensationBoxId = 3
			}
			if ($scope.userDetails.compensationBoxName == 'LOS HEROES') {
					$scope.compensationBoxId = 4
			}
			if ($scope.userDetails.compensationBoxName == 'GABIELA MISTRAL') {
					$scope.compensationBoxId = 5
			}


			if ($scope.userDetails.healthSystemName == 'ISAPRE') {
					$scope.healthSystemId = 2
			}else{ $scope.healthSystemId = 1}

			if ($scope.userDetails.retirementName == 'Capital') {
					$scope.retirementId = 1
			}
			if ($scope.userDetails.retirementName == 'Cuprum') {
					$scope.retirementId = 2
			}
			if ($scope.userDetails.retirementName == 'Habitat') {
					$scope.retirementId = 3
			}
			if ($scope.userDetails.retirementName == 'PlanVital') {
					$scope.retirementId = 4
			}
			if ($scope.userDetails.retirementName == 'Provida') {
					$scope.retirementId = 5
			}
			if ($scope.userDetails.retirementName == 'Modelo') {
					$scope.retirementId = 6
			}
			if ($scope.userDetails.retirementName == 'Uno') {
					$scope.retirementId = 7
			}


			if ($scope.userDetails.isapreName == 'CONSALUD') {
					$scope.isapreId = 1
			}
			if ($scope.userDetails.isapreName == 'BANMEDICA') {
					$scope.isapreId = 2
			}
			if ($scope.userDetails.isapreName == 'MAS VIDA') {
					$scope.isapreId = 3
			}
			if ($scope.userDetails.isapreName == 'COLMENA') {
					$scope.isapreId = 4
			}
			if ($scope.userDetails.isapreName == 'CRUZ BLANCA') {
					$scope.isapreId = 5
			}
			if ($scope.userDetails.isapreName == 'VIDA TRES') {
					$scope.isapreId = 6
			}

			

				if ($scope.userDetails.bankName == 'banco de chile') {
					$scope.bankId = 1
			}
			if ($scope.userDetails.bankName == 'Banco Scotiabank') {
					$scope.bankId = 2
			}
			if ($scope.userDetails.bankName == 'Banco Estado') {
					$scope.bankId = 3
			}
			if ($scope.userDetails.bankName == 'Banco BCI') {
					$scope.bankId = 4
			}
			if ($scope.userDetails.bankName == 'Banco ITAU') {
					$scope.bankId = 5
			}

			if ($scope.userDetails.bankName == 'Banco Santander') {
					$scope.bankId = 6
			}
			if ($scope.userDetails.bankName == 'Banco Edwards') {
					$scope.bankId = 9
			}
			if ($scope.userDetails.bankName == 'Banco Falabella') {
					$scope.bankId = 10
			}
			if ($scope.userDetails.bankName == 'Banco BICE') {
					$scope.bankId = 11
			}
			if ($scope.userDetails.bankName == 'banco del desarrollo') {
					$scope.bankId = 12
			}
			if ($scope.userDetails.bankName == 'Banco corpbanca') {
					$scope.bankId = 13
			}


			let model = {
				userId: $scope.userId,
				retirementId: $scope.retirementId,
				healthSystemId:  $scope.healthSystemId,
				isapreId: $scope.isapreId,
				percentagePlan: $scope.userDetails.percentagePlan,
				baseSalary: $scope.userDetails.baseSalary,
				compensationBoxId:  $scope.compensationBoxId,
				bankId: $scope.bankId,
				accountNumber: $scope.userDetails.accountNumber,
				bankAccountTypeId: $scope.userDetails.bankAccountType
			}

			console.log("modelo",model);

			staffServices.users.updateUserInfoInstitutions(model).$promise.then((dataReturn) => {
				ngNotify.set('Se ha actualizado el usuario correctamente','success')
				$state.reload('app.staffUpdate')
				},(err) => {
					ngNotify.set('Error al actualizar usuario','error')
			})
		}
		function uploadFiles() {
			const id = $scope.userId
	
			this.confirm = this.$mdDialog.confirm({
				controller: 'staffRegistrationFilesUpController',
				controllerAs: 'staffFiles',
				template: require('./upload-files/staff-upload.html'),
				locals: { userId: id },
				animation: true,
				parent: angular.element(document.body),
				clickOutsideToClose: true,
			})
			this.$mdDialog.show(this.confirm).then(() => {
				$state.reload('app.staffUpdate')
				
			})
		}
		function openFile (file) {
			if('.jpg' === file.name.substr(-4, 4) || '.jpeg' === file.name.substr(-5, 5) || '.png' === file.name.substr(-4, 4)){
				viewImage(file.name)
				// console.log(' file.name ::::: :::: :::: : :: :', file.name)
			}else {
				window.open(file.url2, '_blank');
			}
		}
		function deleteFile (file) {


      var response = confirm("¿Desea realmente eliminar el Archivo?");
      if ( response == true )
      {
         alert("Presione Aceptar para confirmar Eliminación")
         console.log('response',response)

         const json = {
          fileName : file.name,
          id : $scope.userId
        }
    
        staffServices.users.deleteFile(json).$promise.then((dataReturn) => {
          this.ngNotify.set('Archivo eliminado exitosamente', 'success')
          $state.reload('app.staffUpdate')
        }, (err) => {
          this.ngNotify.set('Error al eliminar archivo', 'error')
        })
      }else{
         alert("Presione Aceptar para cerrar Alerta")
         console.log('response',response)
      }

			
			
		}
		function viewImage(fileName) {
			$scope.importData = {
				userId: $scope.userId, 
				name : fileName
			}
			const 	modalInstance  = $modal.open({
														template: require('./../userImageView/userImageView.html'),
														animation: true,
														scope: $scope,
														controller: 'userImageViewController',
														controllerAs: 'userImageView'
													}) 
		}
    }


    

  }

  staffUpdateController.$inject = ['$scope', '$filter','$rootScope', '$http','staffServices','$location','UserInfoConstant','ngNotify','$state','warehousesServices','$stateParams','$modal','banksServices', '$mdDialog', 'SERVICE_URL_CONSTANT'];
