/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> documentsUploadUserController
* # As Controller --> documentsUploadUser														
*/


  export default class documentsUploadUserController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,staffServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.editBank = editBank;
	    vm.showPanel= showPanel
	    $scope.userId = $scope.userId
	    $scope.fullName = $scope.fullName
	    $scope.editView = true

	    let params = {
            userId: $scope.userId
          }
      

        // staffServices.users.getUsersDetails(params).$promise.then((dataReturn) => {
        //  		  $scope.user = dataReturn.data
        //  		  $scope.userDetails  = { 
	       //             rut : $scope.user[0].rut,
	       //             fullName : $scope.user[0].fullName
        //           }

        //       },(err) => {
        //           console.log('No se ha podido conectar con el servicio',err);
        //       })


        staffServices.users.getUserWarehouses(params).$promise.then((dataReturn) => {
         		  $scope.warehouses = dataReturn.data

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function showPanel(value) {

			if (value == 1) {

				$scope.editPanel = true
				$scope.editView = false 
				$scope.buttonSave = true
			}else{

				$scope.editPanel = false
				$scope.editView = true 
				$scope.buttonSave = false

			}
			
		}

		function editBank() {
			
			let params = {
	            bankId: $scope.bankId,
	            bankCode : $scope.bankDetails.bankCode,
		        bankName : $scope.bankDetails.bankName
	           }

	        staffServices.banks.updateBank(params).$promise.then((dataReturn) => {
	        	    $scope.result = dataReturn.data  
	         		ngNotify.set('Se ha actualizado correctamente el banco','success')     
	         		$state.reload('app.banks')
                    $modalInstance.dismiss('chao');
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio',err);
	              })
			
		}

		


    }

  }

  documentsUploadUserController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','staffServices','$location','UserInfoConstant','ngNotify','$state'];
