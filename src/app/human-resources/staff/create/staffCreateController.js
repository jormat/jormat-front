/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> staffCreateController
* # As Controller --> staffCreate														
*/


  export default class staffCreateController {

    constructor($scope, $filter,$rootScope, $http,staffServices,$location,UserInfoConstant,ngNotify,$state,warehousesServices,banksServices) {

    	var vm = this;
	    vm.setRole = setRole;
        vm.setRetirements = setRetirements
        vm.setIsapre = setIsapre
        vm.setCompensationBox = setCompensationBox
        vm.setBank = setBank
        vm.setCenter = setCenter
        vm.checkRut = checkRut
        vm.createUser = createUser
        vm.typeHealthSystem = typeHealthSystem
        $scope.picture = require("../images/no_user.jpg")
        $scope.rutValidate = true
        $scope.day = 1
        $scope.month = 1
        $scope.year = 1980

            function checkRut(rut) {
                // Despejar Puntos
                var valor = rut.replace('.','');
                // Despejar Guión
                valor = valor.replace('-','');
                
                // Aislar cuerpo y Dígito Verificador
                $scope.cuerpo = valor.slice(0,-1);
                $scope.dv = valor.slice(-1).toUpperCase();
                
                // Formatear RUN
                rut = $scope.cuerpo + '-'+ $scope.dv
                
                // Si no cumple con el mínimo ej. (n.nnn.nnn)
                if($scope.cuerpo.length < 7) { 
                    // rut.setCustomValidity("RUT Incompleto");
                    $scope.textValidation = "Incompleto";
                    $scope.rutValidate = true;
                    console.log("rut Incompleto");  
                    return false;
                }
                
                // Calcular Dígito Verificador
                $scope.suma = 0;
                $scope.multiplo = 2;
                
                // Para cada dígito del $scope.cuerpo

                for($scope.i=1;$scope.i<=$scope.cuerpo.length;$scope.i++) {
                
                    // Obtener su Producto con el Múltiplo Correspondiente
                    $scope.index = $scope.multiplo * valor.charAt($scope.cuerpo.length -$scope.i);
                    
                    // Sumar al Contador General
                    $scope.suma = $scope.suma + $scope.index;
                    
                    // Consolidar Múltiplo dentro del rango [2,7]
                    if($scope.multiplo < 7) { $scope.multiplo = $scope.multiplo + 1; } else { $scope.multiplo = 2; }
              
                }
                
                // Calcular Dígito Verificador en base al Módulo 11
                $scope.dvEsperado = 11 - ($scope.suma % 11);
                
                // Casos Especiales (0 y K)
                $scope.dv = ($scope.dv == 'K')?10:$scope.dv;
                $scope.dv = ($scope.dv == 0)?11:$scope.dv;
                
                // Validar que el $scope.cuerpo coincide con su Dígito Verificador
                if($scope.dvEsperado != $scope.dv) { 
                    // rut.setCustomValidity("RUT Inválido");
                    console.log("RUT Inválido"); 
                    $scope.textValidation = "RUT Inválido";
                    $scope.rutValidate = true;
                    return false; 
                }
                
                // Si todo sale bien, eliminar errores (decretar que es válido)
                // rut.setCustomValidity('');
                $scope.textValidation = "RUT Válido";
                console.log("RUT valido");
                $scope.rutValidate = false
            }

        staffServices.users.getRole().$promise.then((dataReturn) => {
              $scope.role = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        staffServices.users.getRetirements().$promise.then((dataReturn) => {
              $scope.retirements = dataReturn.data;
              console.log('afps',$scope.retirements);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        staffServices.users.getIsapres().$promise.then((dataReturn) => {
              $scope.isapres = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        staffServices.users.getCompensationBox().$promise.then((dataReturn) => {
              $scope.compensationBox = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        banksServices.banks.getBanks().$promise.then((dataReturn) => {
                  $scope.banks = dataReturn.data;
                  console.log('banks',$scope.banks);
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

        // staffServices.users.getHealthSystem().$promise.then((dataReturn) => {
        //       $scope.healthSystem = dataReturn.data;
        //       console.log('salud',$scope.retirements);
        //       },(err) => {
        //           console.log('No se ha podido conectar con el servicio',err);
        //     })

        warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
              $scope.warehouses = dataReturn.data;
              console.log('warehouses',$scope.warehouses);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })


        function typeHealthSystem(value){
            console.log("value",$scope.value);
            if ( value == 2 ) {
                $scope.typeHealthSystemShow = true

            }else{
                $scope.typeHealthSystemShow = false
            }
        }

        function setRole(roleId){
            $scope.roleId = roleId
            console.log("rolId",$scope.roleId);
        }

        function setRetirements(retirementId){
            $scope.retirementId = retirementId
            console.log("retirementId",$scope.retirementId);
        }

        function setIsapre(isapreId){
            $scope.isapreId = isapreId
            console.log("isapreId",$scope.isapreId);
        }

        function setCompensationBox(compensationBoxId){
            $scope.compensationBoxId = compensationBoxId
            console.log("compensationBoxId",$scope.compensationBoxId);
        }

        function setBank(bankId){
            $scope.bankId = bankId
            console.log("bankId",$scope.bankId);
        }

        function setCenter(warehouseId){
            $scope.warehouseId = warehouseId
            console.log("warehouseId",$scope.warehouseId);
        }

        $scope.myUiSelect={model:{}}; // encapsulate your model inside an object.
          
        $scope.onSelect = function(item,model){
            console.log("selectedItem",item);
          }

    
	    function createUser() {

                let model = {
                    fullName: $scope.fullName,
                    rut: $scope.rut,
                    gender: $scope.gender,
                    nationality: $scope.nationality,
                    phone: $scope.phone,
                    birthdayDate: $scope.day+"/"+$scope.month+"/"+$scope.year,
                    email: $scope.email,
                    address: $scope.address,
                    cityName: $scope.city,
                    userName: $scope.userName,
                    password: $scope.password,
                    rolId: $scope.roleId,
                    day: $scope.day,
                    month: $scope.month,
                    year: $scope.year,
                    startDate: $scope.startDate,
                    finishDate: $scope.finishDate,
                    documentType: $scope.documentType,
                    warehouses:$scope.myUiSelect.model,
                    retirementId:  $scope.retirementId,
                    healthSystemId: $scope.healthSystemId,
                    isapreId:  $scope.isapreId,
                    percentagePlan: $scope.percentagePlan,
                    compensationBoxId: $scope.compensationBoxId,
                    bankId:  $scope.bankId,
                    accountNumber: $scope.accountNumber, 
                    baseSalary: $scope.baseSalary,
                    bankId:  $scope.bankId,
                    bankAccountType: $scope.bankAccountType,
                    maritalStatus:  $scope.maritalStatus,
                    numberOfChildren: $scope.numberOfChildren,
                    warehouseId:  $scope.warehouseId,
                    shirtSize: $scope.shirtSize,
                    shoeSize:  $scope.shoeSize,
                    lastEmployer: $scope.lastEmployer,
                    scholarShip:  $scope.scholarShip,
                    profession:  $scope.profession
                }

                console.log("modelo",model);

                staffServices.users.userCreate(model).$promise.then((dataReturn) => {
                    $scope.uderId = dataReturn.data;
                    ngNotify.set('Se ha creado el usuario correctamente','success')
                    $state.go("app.staffUpdate", { idUser: $scope.uderId});
                    // $state.go('app.staff')
                  },(err) => {
                      ngNotify.set('Error al crear usuario','error')
                })
            }

	  

		


    }

  }

  staffCreateController.$inject = ['$scope', '$filter','$rootScope', '$http','staffServices','$location','UserInfoConstant','ngNotify','$state','warehousesServices','banksServices'];
