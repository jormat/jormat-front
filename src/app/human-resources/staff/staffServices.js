/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> staffServices
* file for call at services for staff
*/

class staffServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var users = $resource(SERVICE_URL_CONSTANT.jormat + '/users/:route',{},{
      
      getUsers: {
        method:'GET',
          params : {
            route: 'list'
            }
          },

      getUsersDetails: {
        method:'GET',
          params : {
            route: ''
            }
          },

       getRole: {
        method:'GET',
        params : {
          route: 'getRole'
          }
        },
        
        getUserWarehouses: {
          method:'GET',
          params : {
            route: 'warehouses'
            }
          },

        userCreate: {
          method:'POST',
          params : {
            route: ''
            }
          },

        userUpdateInfo: {
          method:'PUT',
          params : {
            route: 'info'
            }
          },

        userUpdateOrganization: {
          method:'PUT',
          params : {
            route: 'organization'
            }
          },

        userUpdateAccess: {
          method:'PUT',
          params : {
            route: 'access'
            }
          },

        getRetirements: {
          method:'GET',
          params : {
            route: 'retirements'
            }
          },

        getIsapres: {
          method:'GET',
          params : {
            route: 'isapres'
            }
          },

        getCompensationBox: {
          method:'GET',
          params : {
            route: 'compensation-box'
            }
          },

        statusUserUpdate: {
          method:'PUT',
          params : {
            route: 'status'
            }
          },

        contractTypeUpdate: {
          method:'PUT',
          params : {
            route: 'contract-type'
            }
          },

        updateUserInfoInstitutions: {
          method:'PUT',
          params : {
            route: 'institutions'
            }
          },
		updateUserBlockPersonalInfo: {
			method:'PUT',
			params : {
				route: 'personal-info'
				}
		},
		deleteFile: {
			method:'DELETE',
			params: {
				route: 'file'
			}
		},

        keyValidate: {
          method:'GET',
          params : {
            route: 'key-validate'
            }
          }
    })

    return { users : users
             
           }
  }
}

  staffServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.staffServices', [])
  .service('staffServices', staffServices)
  .name;