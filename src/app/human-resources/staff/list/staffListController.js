
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> staffListController
* # As Controller --> staffList														
*/

export default class staffListController{

        constructor($scope,UserInfoConstant,$timeout,staffServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.searchData=searchData;
            vm.viewUser= viewUser
            vm.userCreate = userCreate
            vm.updateUser= updateUser
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.usersGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_personal.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'id',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.staffList.viewUser(row.entity)">{{row.entity.id}}</a>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.active == 'Inactive') {
                                return 'critical';
                            }
                       }  
                    },
                    { 
                      name:'Nombre',
                      field: 'fullName',
                      width: '23%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;">{{row.entity.fullName}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.active == 'Inactive') {
                                return 'critical';
                            }
                       } 
                    },
                    ,
                    { 
                      name:'Rut',
                      field: 'rut',
                      width: '10%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class=" badge bg-accent" style="font-size: 11px;">{{row.entity.rut}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.active == 'Inactive') {
                                return 'critical';
                            }
                       } 
                    },
                    { 
                      name:'Telefono',
                      field: 'phone',
                      width: '12%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.phone}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.active == 'Inactive') {
                                return 'critical';
                            }
                       } 
                    },
                    { 
                      name:'Rol',
                      field: 'rol',
                      width: '16%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning">{{row.entity.rol}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.active == 'Inactive') {
                                return 'critical';
                            }
                       } 
                    },
                    
                    { 
                      name:'Fecha Nacimiento',
                      field: 'birthday_date',
                      width: '14%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<p><strong>{{row.entity.birthday_date  | date:\'dd/MM/yyyy\'}}</strong></p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.active == 'Inactive') {
                                return 'critical';
                            }
                       } 
                    },
                    { 
                      name:'Usuario',
                      field: 'userName',
                      width: '15%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secundary font-italic" style="font-size: 11px;">{{row.entity.userName}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.active == 'Inactive') {
                                return 'critical';
                            }
                       } 
                    },
                    
                    

                    { 
                      name: '',
                      width: '5%', 
                      enableFiltering: false,
                      field: 'href',

                      cellTemplate: $scope.dropdownMenu,
                      cellClass: function(grid, row) {
                        if (row.entity.active == 'Inactive') {
                                return 'critical';
                            }
                       } 
                    }
                ]
            };
            
          
         		staffServices.users.getUsers().$promise.then((dataReturn) => {
         		  $scope.usersGrid.data = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

        function searchData() {
                staffServices.users.getUsers().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.usersGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }


        function viewUser(row){
                $scope.userData = row;
                var modalInstance  = $modal.open({
                        template: require('../view/staff-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'staffViewController',
                        controllerAs: 'staffView',
                })
        }

        function userCreate(){
                var modalInstance  = $modal.open({
                        template: require('../create/staff-create.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'staffCreateController',
                        controllerAs: 'staffCreate',
                })
        }

        function updateUser(row){
            $state.go("app.staffUpdate", { idUser: row.id});
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

staffListController.$inject = ['$scope','UserInfoConstant','$timeout','staffServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

