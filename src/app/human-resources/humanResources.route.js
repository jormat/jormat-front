
routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$translateProvider']

export default function routes($stateProvider, $urlRouterProvider, $locationProvider, $translateProvider) {
        $locationProvider.html5Mode(true)
       
		$stateProvider
        .state('app.staff', {
            abstract: false,
            url: '/personal',
            template: require('./staff/list/staff-list.html'),
            controller: 'staffListController',
            controllerAs:'staffList'
      })

        .state('app.staffCreate', {
            abstract: false,
            url: '/personal/crear-usuario',
            template: require('./staff/create/staff-create.html'),
            controller: 'staffCreateController',
            controllerAs:'staffCreate'
      })

        .state('app.staffUpdate', {
            abstract: false,
            url: '/personal/actualizar-usuario?idUser',
            template: require('./staff/update/staff-update.html'),
            controller: 'staffUpdateController',
            controllerAs:'staffUpdate'
      })

        .state('app.retirements', {
            abstract: false,
            url: '/instituciones/prevision',
            template: require('./institutions/retirements/retirements-view.html'),
            controller: 'retirementsViewController',
            controllerAs:'retirementsView'
      })

        .state('app.compensationBox', {
            abstract: false,
            url: '/instituciones/cajas-compensacion',
            template: require('./institutions/compensation-box/compensation-box-view.html'),
            controller: 'compensationBoxViewController',
            controllerAs:'compensationBoxView'
      })

        .state('app.isapres', {
            abstract: false,
            url: '/instituciones/isapres',
            template: require('./institutions/isapres/isapres-view.html'),
            controller: 'isapresViewController',
            controllerAs:'isapresView'
      })

        .state('app.remunerations', {
            abstract: false,
            url: '/personal/remuneraciones',
            template: require('./remunerations/list/remunerations-list.html'),
            controller: 'remunerationsListController',
            controllerAs:'remunerationsList'
      })

}
