/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> remunerationsServices
* file for call at services to checks
*/

class remunerationsServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var checks = $resource(SERVICE_URL_CONSTANT.jormat + '/users/remunerations/:route',{},{

      getStaff: {
        method:'GET',
        params : {
          route: 'list'
          }
        },

      getChecksDetails: {
        method:'GET',
        params : {
          route: ''
          }
        },

      checksCreate: {
        method:'POST',
        params : {
          route: ''
          }
        },

      checksStatusUpdate: {
        method:'PUT',
        params : {
          route: 'status'
          }
        },

      checkUpdate: {
        method:'PUT',
        params : {
          route: ''
          }
        },

        getHistoricChecks: {
        method:'GET',
        params : {
          route: 'historic'
          }
        }
    })

    var banks = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/banks/:route',{},{

      getBanks: {
        method:'GET',
        params : {
          route: 'list'
          }
        }
    })

    return { checks : checks,
             banks:banks
             
           }
  }
}

  remunerationsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.remunerationsServices', [])
  .service('remunerationsServices', remunerationsServices)
  .name;