
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> remunerationsListController
* # As Controller --> remunerationsList														
*/

export default class remunerationsListController{

        constructor($scope,UserInfoConstant,$timeout,staffServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService,uiGridSelectionConstants){
            var vm = this
            $scope.selectedChecks = []
            $scope.day = 30
            $scope.set = 0
            vm.searchData=searchData
            // vm.checkCreate= checkCreate
            // vm.checkView= checkView
            // vm.updateStatusCheck = updateStatusCheck
            // vm.checkUpdate = checkUpdate
            // vm.getHistoricalChecks = getHistoricalChecks
            vm.loadStaffs = loadStaffs
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            loadStaffs()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.usersGrid = {
                enableFiltering: true,
                enableSelectAll: false,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'Listado_de_trabajadores.csv',
                // isRowSelectable: function(row) {
                //   if(row.entity.statusName == "PEN" || row.entity.statusName == "DEP"){
                //           return true;
                //         } else {
                //           return false;
                //         }
                // },
                onRegisterApi: function(gridApi){
                  $scope.gridApi = gridApi;

                  gridApi.selection.on.rowSelectionChanged($scope,function(row,$index){
                    $scope.selectedChecks = gridApi.selection.getSelectedRows()
                    // $scope.selectedChecks = gridApi.selection.selectAllRows()  
                  });
                },
                columnDefs: [
                    { 
                      name:'id',
                      field: 'id',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.remunerationsList.checkView(row.entity)">{{row.entity.id}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Nombre',
                      field: 'fullName',
                      width: '24%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;"><strong>{{row.entity.fullName}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Rut',
                      field: 'rut',
                      width: '11%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-warning uppercase" style="font-size: 12px;">{{row.entity.rut}}</p>' +
                                 '</div>'  
                    },
                    // { 
                    //   name:'N° Cuenta',
                    //   field: 'account',
                    //   width: '11%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.account}}</strong></p>' +
                    //              '</div>' 
                    // },
                    //  { 
                    //   name:'Periodo',
                    //   field: 'bankName',
                    //   width: '12%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="uppercase label bg-warning" style="font-size: 10px;">{{row.entity.bankName}}</p>' +
                    //              '</div>' 
                    // },
                    { 
                      name: 'Dias trabajados', 
                      field: 'day',
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p><input type="text" ng-init="30"  ng-model= "grid.appScope.day" class="form-control"></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Dias licencia',
                      field: 'invoices',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p><input type="text" ng-init="0"  ng-model="grid.appScope.set" class="form-control"></p>' +
                                 '</div>'  
                    },
                   
                    { 
                      name:'Vacaciones',
                      field: 'shareValue',
                      width: '10%',
                      // aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p><input type="text" ng-init="0" ng-model="grid.appScope.set" class="form-control"></p>' +
                                 '</div>' 
                    },
                    
                    { 
                      name: 'Dias Falla', 
                      field: 'origin', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p><input type="text" ng-init="0" ng-model="grid.appScope.set" class="form-control"></p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Total', 
                      field: 'date',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: '',
                      width: '4%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          function loadStaffs(){
            staffServices.users.getUsers().$promise.then((dataReturn) => {
              $scope.usersGrid.data = dataReturn.data;
              console.log('$scope.usersGrid.data',$scope.usersGrid.data);
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

          }
         	

        function searchData() {
             staffServices.users.getUsers().$promise.then((dataReturn) => {
              $scope.usersGrid.data = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
            }

        // function checkCreate(){
        //         var modalInstance  = $modal.open({
        //                 template: require('../create/checks-create.html'),
        //                 animation: true,
        //                 scope: $scope,
        //                 controller: 'checksCreateController',
        //                 controllerAs: 'checksCreate',
        //                 size: 'lg'
        //         })
        // }

        // function checkUpdate(row){
        //   $scope.checksData = row;
        //         var modalInstance  = $modal.open({
        //                 template: require('../update/checks-update.html'),
        //                 animation: true,
        //                 scope: $scope,
        //                 controller: 'checksUpdateController',
        //                 controllerAs: 'checksUpdate',
        //                 size: 'lg'
        //         })
        // }

        // function updateStatusCheck(){
        //     console.log('cheques seleccionados',$scope.selectedChecks)
        //     $scope.checksData = $scope.selectedChecks;
        //     var modalInstance  = $modal.open({
        //                 template: require('../actions/actions-check.html'),
        //                 animation: true,
        //                 scope: $scope,
        //                 controller: 'actionsCheckController',
        //                 controllerAs: 'actionsCheck'
        //         }) 
        // }

        // function getHistoricalChecks(){

        //     var modalInstance  = $modal.open({
        //                 template: require('../historical/historical-checks.html'),
        //                 animation: true,
        //                 scope: $scope,
        //                 controller: 'historicalChecksController',
        //                 controllerAs: 'historicalChecks',
        //                 size: 'lg'
        //         }) 
        // }


        // function checkView(row){
        //     $scope.checksData = row;
        //     var modalInstance  = $modal.open({
        //             template: require('../view/checks-view.html'),
        //             animation: true,
        //             scope: $scope,
        //             controller: 'checksViewController',
        //             controllerAs: 'checksView'
        //     })
        // }

  

        

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

remunerationsListController.$inject = ['$scope','UserInfoConstant','$timeout','staffServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService','uiGridSelectionConstants'];

