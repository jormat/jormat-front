/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> actionsCheckController
* # As Controller --> actionsCheck													
*/


  export default class actionsCheckController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,checksServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.edit = edit;
        vm.updateStatusCheck = updateStatusCheck
        vm.selectActions = selectActions
	    $scope.checks = $scope.checksData
        $scope.buttonName = 'Actualizar'

        console.log('$scope.checkes',$scope.checks );

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        function selectActions(value){

            $scope.value = value
            if (value == 4) {$scope.buttonName = 'Depositar'}
            if (value == 3) {$scope.buttonName = 'Rechazar'}
            if (value == 2) {$scope.buttonName = 'Validar'}
             
        }

        function updateStatusCheck() {
            for(var i in $scope.checks){

                let params = {
                    value: $scope.value,
                    userId : $scope.userInfo.id,
                    checkId: $scope.checks[i].checkId,
                    day: moment($scope.day).format("YYYY-MM-DD")
              }

            checksServices.checks.checksStatusUpdate(params).$promise.then((dataReturn) => {
                    console.log('update check Ok');
                    $scope.result = dataReturn.data
                    $state.reload('app.checks')
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
            }

            // $state.reload('app.checks')
            // $modalInstance.dismiss('chao');
            ngNotify.set('Se han actualizado correctamente los cheques','success')
        }

	    
	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function edit() {
			$state.go("app.itemsUpdate");
		}

    }

  }

  actionsCheckController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','checksServices','$location','UserInfoConstant','ngNotify','$state'];
