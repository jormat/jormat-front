/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> checksCreateController
* # As Controller --> checksCreate														
*/


  export default class checksCreateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,checksServices,$location,UserInfoConstant,ngNotify,$state,clientsServices,warehousesServices) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.searchClient = searchClient;
        vm.newCheck = newCheck
        vm.setOriginId = setOriginId
        vm.setName = setName
        vm.setBankId = setBankId
        vm.setDepositDate = setDepositDate
        $scope.depositDate = ''

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        checksServices.banks.getBanks().$promise.then((dataReturn) => {
              $scope.banks = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
              $scope.warehousesTo = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        function setName(clients) {
                console.log('please',clients)
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedRut = clients.rut
            }


        function searchClient (value) {

            let model = {
                name: value
            }

            clientsServices.clients.getClientsDsByName(model).$promise.then((dataReturn) => {
                  $scope.clients = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            return false;
          }

	    let params = {
            checkId: $scope.checkId
          }


	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function setDepositDate() {
            
            $scope.depositDate = ''
        }

        function setOriginId(originId){
            $scope.originId = originId
        }

        function setBankId(bankId){
            console.log('setBankId',bankId)
            $scope.bankId = bankId
        }

        function newCheck() {
            console.log('save bank check');
            let model = {
                            userId: $scope.userInfo.id,
                            associatedName : $scope.associatedName,
                            serialNumber : $scope.serialNumber,
                            account : $scope.account,
                            origin : $scope.originId,
                            shareValue : $scope.shareValue,
                            bankId : $scope.bankId,
                            day : moment($scope.day).format("YYYY-MM-DD"),
                            clientId : $scope.clientId,
                            phone : $scope.phone,
                            depositDate : moment($scope.depositDate).format("YYYY-MM-DD"),
                            shareDetails : $scope.shareDetails, 
                            invoices : $scope.invoices 
                        }      

                        console.log('modelo',model)   

                        checksServices.checks.checksCreate(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha ingresado correctamente el cheque','success')
                            $scope.result = dataReturn.data

                            if ($scope.depositDate != '') { 

                                    let params = {
                                        value: 4,
                                        userId : $scope.userInfo.id,
                                        checkId: $scope.result,
                                        day: moment($scope.depositDate).format("YYYY-MM-DD")
                                }

                                checksServices.checks.checksStatusUpdate(params).$promise.then((dataReturn) => {
                                console.log('update check Ok');
                                $scope.result = dataReturn.data
                              },(err) => {
                                  console.log('No se ha podido conectar con el servicio',err);
                              })

                            }

                            $state.reload('app.checks')
                            $modalInstance.dismiss('chao');

                          },(err) => {
                              ngNotify.set('Error al registrar cheque','error')
                          })


        }

		$scope.invoices = [];

        $scope.addInvoice = function(invoice) {
            
            $scope.invoices.push({'title': $scope.invoice, 'done':false})
            $scope.invoice = ''
            // $scope.saveButton  = false
        }

        $scope.deleteInvoice = function(index) {  
            $scope.invoices.splice(index, 1);
        }

    }

  }

  checksCreateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','checksServices','$location','UserInfoConstant','ngNotify','$state','clientsServices','warehousesServices'];
