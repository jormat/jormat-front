
// import uirouter from 'angular-ui-router';
import routing from './humanResources.route';
import run from './humanResources.run';

// Definition for controllers & services humanResources module'
import staffListController from './staff/list/staffListController'
import staffViewController from './staff/view/staffViewController'
import staffCreateController from './staff/create/staffCreateController'
import staffUpdateController from './staff/update/staffUpdateController'
import staffRegistrationFilesUpController from './staff/update/upload-files/staffRegistrationFilesUpController'
import userImageViewController from './staff/userImageView/userImageViewController'
import documentsUploadUserController from './staff/upload/documentsUploadUserController'
import statusUserUpdateController from './staff/active/statusUserUpdateController'
import staffServices from './staff/staffServices';

import retirementsViewController from './institutions/retirements/retirementsViewController' 
import compensationBoxViewController from './institutions/compensation-box/compensationBoxViewController'
import isapresViewController from './institutions/isapres/isapresViewController'
import institutionsServices from './institutions/institutionsServices';

// remunerations
import remunerationsListController from './remunerations/list/remunerationsListController'
import remunerationsServices from './remunerations/remunerationsServices';

export default angular.module('app.humanResources', [staffServices,institutionsServices,remunerationsServices])
.config(routing)

.controller('staffListController', staffListController)
.controller('remunerationsListController', remunerationsListController)
.controller('staffViewController', staffViewController)
.controller('staffCreateController', staffCreateController)
.controller('staffUpdateController', staffUpdateController)
.controller('staffRegistrationFilesUpController', staffRegistrationFilesUpController)
.controller('userImageViewController', userImageViewController)
.controller('retirementsViewController', retirementsViewController)
.controller('compensationBoxViewController', compensationBoxViewController)
.controller('isapresViewController', isapresViewController) 
.controller('documentsUploadUserController', documentsUploadUserController) 
.controller('statusUserUpdateController', statusUserUpdateController)
// Sidebar Construct
.run(run)
.constant("humanResources", [{
        "title": "R.R.H.H",
        "icon":"mdi-account-multiple",
        "subMenu":[
            {
            'title' : 'Personal',
            'url': 'app.staff',
            },
            {
            'title' : 'Remuneraciones',
            'url': 'app.remunerations',
            },

            {
            'title' : 'Instituciones',
            "superMenu":[
            
                    {
                    'title' : 'Previsión',
                    'url': 'app.retirements'
                    },
                    {
                    'title' : 'Isapres',
                    'url': 'app.isapres',
                    },
                    {
                    'title' : 'Cajas de compensación',
                    'url': 'app.compensationBox',
                    }      
                ]
            }
        ]
    }])
.name;
