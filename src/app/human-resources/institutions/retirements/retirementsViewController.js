
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> retirementsViewController
* # As Controller --> retirementsView														
*/

export default class retirementsViewController{

        constructor($scope,UserInfoConstant,$timeout,staffServices,institutionsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.createRetirement = createRetirement
            vm.searchData=searchData
            vm.loadRetirements = loadRetirements
            vm.retirementsAdd = retirementsAdd  
            vm.retirementUpdate = retirementUpdate
            vm.retirementUpdateFunction = retirementUpdateFunction
            $scope.createButton = true
            
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }
            loadRetirements()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.retirementsGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'listado_afp.csv',
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'id',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a>{{row.entity.id}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Nombre',
                      field: 'name',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="uppercase"> <strong>{{row.entity.name}}</strong></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Codigo',
                      field: 'code',
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="label bg-warning uppercase"> <strong>{{row.entity.code}}</strong></p>' +
                                 '</div>'
                    },

                    { 
                      name:'Tasa',
                      field: 'rate',  
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a>{{row.entity.rate}}</a>' +
                                 '</div>' 
                    }, 
                    { 
                      name: 'S.I.S', 
                      field: 'sis', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="font-italic">{{row.entity.sis}}</p>' +
                                 '</div>'   
                    },
                    { 
                      name: '',
                      width: '10%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadRetirements(){

                staffServices.users.getRetirements().$promise.then((dataReturn) => {
                  $scope.retirementsGrid.data = dataReturn.data;
                  console.log('data afp',$scope.retirementsGrid.data)
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            }


            function searchData() {
              staffServices.users.getRetirements().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.retirementsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function retirementsAdd() {


                let model = {
                    name: $scope.name,
                    code: $scope.code,
                    rate: $scope.rate,
                    sis: $scope.sis
                }

                console.log('entra create',model);

                institutionsServices.institutions.retirementsCreate(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado la institución correctamente','success')
                    createRetirement(2)
                    loadRetirements()
                  },(err) => {
                      ngNotify.set('Error al crear institución','error')
                      createRetirement(1)

                })
            }


            function createRetirement(value){
              
              if (value == 2) {$scope.createRetirement = false}
              if (value == 1) {$scope.createRetirement = true}
              if (value == 0) {
                $scope.createRetirement = false
                $scope.updateRetirement = false
                $scope.createButton = true
              }
            
              
            }

            
    
            function retirementUpdate(row) {

              $scope.retirementId = row.id
              $scope.retirementName  =  row.name
              $scope.retirementCode  =  row.code
              $scope.retirementRate  = row.rate
              $scope.retirementSis  =  row.sis

              $scope.updateRetirement = true
              $scope.createButton = false

   
            }


            function retirementUpdateFunction() {

                let model = {
                      id: $scope.retirementId,
                      name: $scope.retirementName,
                      code: $scope.retirementCode,
                      rate: $scope.retirementRate,
                      sis:  $scope.retirementSis
                    }

                console.log ('vamos actualizar retirementUpdate',model)

                    institutionsServices.institutions.retirementsUpdate(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado la AFP correctamente','success')
                        loadRetirements()
                        createRetirement(0)
                      },(err) => {
                          ngNotify.set('Error al actualizar AFP','error')
                    })
            }



      


            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

retirementsViewController.$inject = ['$scope','UserInfoConstant','$timeout','staffServices','institutionsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

