/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> institutionsServices
* file for call at services for certificates
*/

class institutionsServices {

	constructor($resource,SERVICE_URL_CONSTANT) {
		
		var institutions = $resource(SERVICE_URL_CONSTANT.jormat + '/users/institutions/:route',{},{
			
				retirementsCreate: {
						method:'POST',
						params : {
								route: 'retirements'
						}
				},
				retirementsUpdate: {
						method:'PUT',
						params : {
							 route: 'retirements'
						}
				},
				isapresCreate: {
						method:'POST',
						params : {
							 route: 'isapres'
						}
				},
				isapresUpdate: {
						method:'PUT',
						params : {
							 route: 'isapres'
						}
				},
				compensationBoxCreate: {
						method:'POST',
						params : {
								route: 'compensation-box'
						}
				},
				compensationBoxUpdate: {
						method:'PUT',
						params : {
								route: 'compensation-box'
						}
				}
		})

		return { institutions : institutions
						 
					 }
	}
}

	institutionsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

	export default  angular.module('services.institutionsServices', [])
	.service('institutionsServices', institutionsServices)
	.name;