
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> isapresViewController
* # As Controller --> isapresView														
*/

export default class isapresViewController{

        constructor($scope,UserInfoConstant,$timeout,staffServices,institutionsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.searchData=searchData
            vm.loadIsapres = loadIsapres
            vm.isapresAdd = isapresAdd 
            vm.createIsapre = createIsapre  
            vm.isapreUpdate = isapreUpdate 
            vm.isapreUpdateFunction = isapreUpdateFunction
            $scope.createButton = true
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            loadIsapres()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')

             $scope.isapresGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'listado_afp.csv',
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'id',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.certificatesList.viewVoucher(row.entity)">{{row.entity.id}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Nombre',
                      field: 'name',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="uppercase"> <strong>{{row.entity.name}}</strong></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Codigo',
                      field: 'code',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="label bg-warning uppercase"> <strong>{{row.entity.code}}</strong></p>' +
                                 '</div>'
                    },

                    { 
                      name:'Porcentaje',
                      field: 'rate',  
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.certificatesList.viewDocument(row.rate)">{{row.entity.rate}}</a>' +
                                 '</div>' 
                    }, 
                    // { 
                    //   name: 'Usuario', 
                    //   field: 'userName', 
                    //   width: '10%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class=" label bg-warning font-italic" style="font-size: 10px;">{{row.entity.userName}}</p>' +
                    //              '</div>'   
                    // },
                    { 
                      name: '',
                      width: '10%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            
            function loadIsapres(){

                staffServices.users.getIsapres().$promise.then((dataReturn) => {
                  $scope.isapresGrid.data = dataReturn.data;
                  console.log('data isapres',$scope.isapresGrid.data)
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            }


            function searchData() {
              staffServices.users.getIsapres().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.isapresGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }


            function isapresAdd() {


                let model = {
                    name: $scope.name,
                    code: $scope.code,
                    rate: $scope.rate
                }

                console.log('entra create',model);

                institutionsServices.institutions.isapresCreate(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado la institución correctamente','success')
                    createIsapre(2)
                    loadIsapres()
                  },(err) => {
                      ngNotify.set('Error al crear institución','error')
                      createIsapre(1)

                })
            }


            function createIsapre(value){
              
              if (value == 2) {$scope.createIsapre = false}
              if (value == 1) {$scope.createIsapre = true}
              if (value == 0) {
                $scope.createIsapre = false
                $scope.updateIsapre = false
                $scope.createButton = true
              }
            
              
            }

            
    
            function isapreUpdate(row) {

              $scope.isapreId = row.id
              $scope.isapreName  =  row.name
              $scope.isapreCode  =  row.code
              $scope.isapreRate  = row.rate

              $scope.updateIsapre = true
              $scope.createButton = false

   
            }


            function isapreUpdateFunction() {

                let model = {
                      id: $scope.isapreId,
                      name: $scope.isapreName,
                      code: $scope.isapreCode,
                      rate: $scope.isapreRate
                    }

                console.log ('vamos actualizar isapreUpdate',model)

                    institutionsServices.institutions.isapresUpdate(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado la isapre correctamente','success')
                        loadIsapres()
                        createIsapre(0)
                      },(err) => {
                          ngNotify.set('Error al actualizar isapre','error')
                    })
            }



      


            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

isapresViewController.$inject = ['$scope','UserInfoConstant','$timeout','staffServices','institutionsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

