
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> compensationBoxViewController
* # As Controller --> compensationBoxView														
*/

export default class compensationBoxViewController{

        constructor($scope,UserInfoConstant,$timeout,staffServices,institutionsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.createCertificate = createCertificate
            vm.searchData=searchData
            vm.loadCompensationBox = loadCompensationBox
            vm.compensationBoxAdd = compensationBoxAdd 
            vm.createCompensationBox = createCompensationBox  
            vm.compensationBoxUpdate = compensationBoxUpdate 
            vm.compensationBoxUpdateFunction = compensationBoxUpdateFunction
            $scope.createButton = true
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }
            loadCompensationBox()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.compensationBoxGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'listado_cajas_compensacion.csv',
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'id',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.certificatesList.viewVoucher(row.entity)">{{row.entity.id}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Nombre',
                      field: 'name',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="uppercase"> <strong>{{row.entity.name}}</strong></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Codigo',
                      field: 'code',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p class="label bg-warning uppercase"> <strong>{{row.entity.code}}</strong></p>' +
                                 '</div>'
                    },

                    { 
                      name:'Porcentaje',
                      field: 'rate',  
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.certificatesList.viewDocument(row.rate)">{{row.entity.rate}}</a>' +
                                 '</div>' 
                    }, 
                    // { 
                    //   name: 'Usuario', 
                    //   field: 'userName', 
                    //   width: '10%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class=" label bg-warning font-italic" style="font-size: 10px;">{{row.entity.userName}}</p>' +
                    //              '</div>'   
                    // },
                    { 
                      name: '',
                      width: '10%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadCompensationBox(){

                staffServices.users.getCompensationBox().$promise.then((dataReturn) => {
                  $scope.compensationBoxGrid.data = dataReturn.data;
                  console.log('data',$scope.compensationBoxGrid.data)
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            }


            function searchData() {
              staffServices.users.getCompensationBox().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.compensationBoxGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }


            function createCertificate(value){
              
              if (value == 2) {
                $scope.createCertificate = false

              }else{

                $scope.createCertificate = true
              }
            
              
            }

            function compensationBoxAdd() {


                let model = {
                    name: $scope.name,
                    code: $scope.code,
                    rate: $scope.rate
                }

                console.log('entra create',model);

                institutionsServices.institutions.compensationBoxCreate(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado la institución correctamente','success')
                    createCompensationBox(2)
                    loadCompensationBox()
                  },(err) => {
                      ngNotify.set('Error al crear institución','error')
                      createCompensationBox(1)

                })
            }


            function createCompensationBox(value){

              console.log('entra value',value);
              if (value == 2) {$scope.createCompensationBox = false}
              if (value == 1) {$scope.createCompensationBox = true}
              if (value == 0) {
                $scope.createCompensationBox = false
                $scope.updateCompensationBox = false
                $scope.createButton = true
              }
            
              
            }

            
    
            function compensationBoxUpdate(row) {

              $scope.compensationBoxId = row.id
              $scope.compensationBoxName  =  row.name
              $scope.compensationBoxCode  =  row.code
              $scope.compensationBoxRate  = row.rate

              $scope.updateCompensationBox = true
              $scope.createButton = false

   
            }


            function compensationBoxUpdateFunction() {

                let model = {
                      id: $scope.compensationBoxId,
                      name: $scope.compensationBoxName,
                      code: $scope.compensationBoxCode,
                      rate: $scope.compensationBoxRate
                    }

                console.log ('vamos actualizar compensationBoxUpdate',model)

                    institutionsServices.institutions.compensationBoxUpdate(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado la compensationBox correctamente','success')
                        loadCompensationBox()
                        createCompensationBox(0)
                      },(err) => {
                          ngNotify.set('Error al actualizar compensationBox','error')
                    })
            }



      


            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

compensationBoxViewController.$inject = ['$scope','UserInfoConstant','$timeout','staffServices','institutionsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

