
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> categoriesUpdateController
* # As Controller --> categoriesUpdate														
*/

export default class categoriesUpdateController{

        constructor($scope,UserInfoConstant,$timeout,categoriesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams){
            var vm = this;
            vm.updateCategory = updateCategory
            
            $scope.nameCategory  = $stateParams.name
            $scope.codeCategory  =  $stateParams.code
          
            function updateCategory() {

                    let model = {
                        "categoryId": $stateParams.idCategory,
                        "categoryCode": $scope.codeCategory,
                        "categoryName": $scope.nameCategory
                    }

                    categoriesServices.categories.updateCategory(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado la categoria correctamente','success')
                        $state.go('app.categories')
                      },(err) => {
                          ngNotify.set('Error al actualizar categoria','error')
                    })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

categoriesUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','categoriesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams'];

