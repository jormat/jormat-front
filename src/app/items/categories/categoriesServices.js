/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> categoriesServices
* file for call at services for providers
*/

class categoriesServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var categories = $resource(SERVICE_URL_CONSTANT.jormat + '/categories/:route',{},{
      
      getCategories: {
        method:'GET',
          params : {
            route: 'list'
            }
          },

       createCategory: {
        method:'POST',
        params : {
          route: ''
          }
        },
        
        viewCategory: {
          method:'GET',
          params : {
            route: ''
            }
          },

        updateCategory: {
          method:'PUT',
          params : {
            route: ''
            }
          },

        disabledCategory: {
          method:'DELETE',
          params : {
            route: ''
            }
          }
    })

    return { categories : categories
             
           }
  }
}

  categoriesServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.categoriesServices', [])
  .service('categoriesServices', categoriesServices)
  .name;