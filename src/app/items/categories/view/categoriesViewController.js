/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> categoriesViewController
* # As Controller --> categoriesView														
*/


  export default class categoriesViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,categoriesServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.edit = edit;
	    $scope.categoryId = $scope.categoryData.categoryId
	    $scope.categoryName = $scope.categoryData.categoryName
	    $scope.categoryCode = $scope.categoryData.categoryCode

	    let params = {
            categoryId: $scope.categoryId
          }

        categoriesServices.categories.viewCategory(params).$promise.then((dataReturn) => {
         		  $scope.category = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function edit() {
			$state.go("app.categoriesUpdate");
		}

		


    }

  }

  categoriesViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','categoriesServices','$location','UserInfoConstant','ngNotify','$state'];
