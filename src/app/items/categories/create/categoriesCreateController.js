
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> categoriesCreateController
* # As Controller --> categoriesCreate														
*/

export default class categoriesCreateController{

        constructor($scope,UserInfoConstant,$timeout,categoriesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter){
            var vm = this;
            vm.createCategory = createCategory
            
          
         	function createCategory() {

                let model = {
                    categoryName: $scope.categoryName,
                    categoryCode: $scope.categoryCode
                }

                categoriesServices.categories.createCategory(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado el categoria correctamente','success')
                    $state.go('app.categories')
                  },(err) => {
                      ngNotify.set('Error al crear categoria','error')
                })
            }
                
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

categoriesCreateController.$inject = ['$scope','UserInfoConstant','$timeout','categoriesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter'];

