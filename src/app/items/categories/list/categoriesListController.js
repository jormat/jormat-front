
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> categoriesListController
* # As Controller --> categoriesList														
*/

export default class categoriesListController{

        constructor($scope,UserInfoConstant,$timeout,categoriesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.searchData=searchData;
            vm.deleteCategories= deleteCategories;
            vm.viewCategories= viewCategories
            vm.updateCategory= updateCategory
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.categoriesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_categorias.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'categoryId',  
                      width: '15%' 
                    },
                    { 
                      name:'Codigo',
                      field: 'categoryCode',
                      width: '25%' 
                    },
                    { 
                      name:'Nombre Categoria',
                      field: 'categoryName',
                      width: '50%' 
                    },
                    { 
                      name: 'Acciones',
                      width: '10%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
         		categoriesServices.categories.getCategories().$promise.then((dataReturn) => {
         		  $scope.categoriesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        function searchData() {
                  categoriesServices.categories.getCategories().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.categoriesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }

        function deleteCategories(row){
                $scope.categoryData = row;
                var modalInstance  = $modal.open({
                        template: require('../delete/categories-delete.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'categoriesDeleteController',
                        controllerAs: 'categoriesDelete'
                })
        }

        function viewCategories(row){
                $scope.categoryData = row;
                var modalInstance  = $modal.open({
                        template: require('../view/categories-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'categoriesViewController',
                        controllerAs: 'categoriesView',
                })
        }

        function updateCategory(row){
            $state.go("app.categoriesUpdate", { idCategory: row.categoryId,name: row.categoryName,code:row.categoryCode});
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

categoriesListController.$inject = ['$scope','UserInfoConstant','$timeout','categoriesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

