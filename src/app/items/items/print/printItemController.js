/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> printItemController
* # As Controller --> printItem														
*/


  export default class printItemController {

    constructor($scope, $filter,$rootScope, $http,itemsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams,SERVICE_URL_CONSTANT) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.itemId = $stateParams.idItem
        $scope.itemName = $stateParams.itemName
        $scope.warehouseName = $stateParams.warehouseName 
        $scope.references = $stateParams.references
        $scope.location = $stateParams.location
        $scope.baseUrl = SERVICE_URL_CONSTANT
        $scope.picture= require("../../../../images/unknown.png");
        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        $scope.imageUrl = $scope.baseUrl.jormat + '/image/image/'+ $scope.itemId

        let model = {
                itemId: $scope.itemId
            }

            itemsServices.items.itemsList(model).$promise.then((dataReturn) => {
                $scope.item = dataReturn.data;
                console.log('print',$scope.item )
                $scope.vatPrice = Math.round(($scope.item[0].netPrice)*1.19)
    
               
            },(err) => {
                console.log('No se ha podido conectar con el servicio',err);
            })

    	function cancel() {

		 window.close();
         
		}


        function print() {
              window.print();

        }


    }

  }

  printItemController.$inject = ['$scope', '$filter','$rootScope', '$http','itemsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams','SERVICE_URL_CONSTANT'];
