/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsDeleteController
* # As Controller --> itemsDelete														
*/


  export default class itemsDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,itemsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.deleteItem = deleteItem;
	    $scope.itemId = $scope.itemsData.itemId
	    $scope.itemName = $scope.itemsData.itemDescription
	    vm.index=$scope.index;
	    vm.grid=$scope.data;

	    vm.$translate = $filter('translate')
	        $rootScope.$on('$translateChangeSuccess', function () {
	   
	        })

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
        //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteItem() {

			let model = {
                    itemId : $scope.itemId,
                    userId : $scope.userInfo.id
                }

                itemsServices.items.itemDisabled(model).$promise.then((dataReturn) => {

                    ngNotify.set('Item dado de baja exitosamente','success');
					$state.reload("app.items");
					$modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al eliminar Item','error')
                  })
			
		}

    }

  }

  itemsDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','itemsServices','$location','UserInfoConstant','ngNotify','$state'];
