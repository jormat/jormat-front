
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> itemsCloneController
* # As Controller --> itemsClone													
*/

export default class itemsCloneController{

        constructor($scope,UserInfoConstant,$timeout,itemsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams,warehousesServices,locationsServices,providersServices,categoriesServices,$window,wishlistServices){
            var vm = this;
            vm.callLocations= callLocations
            vm.cloneItem= cloneItem
            vm.updateBodegas= updateBodegas
            vm.webItemCreate = webItemCreate
            vm.internationalType = internationalType
            $scope.type = $stateParams.type
            $scope.locationsInput = true
            $scope.internationalPanel  = false
            $scope.id = $stateParams.idItem
            $scope.title = "Clonar Item"
            $scope.subTitle = "Clonar/Copiar Item"
            $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
            let params = {
                itemId: $stateParams.idItem
            }

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

            if ($scope.type == "wishlist") {

                $scope.title = "Crear Item"
                $scope.subTitle = "Crear Item desde Lista de Deseos"
                $scope.parameters = JSON.parse($stateParams.parameters)
                $scope.referencias = $scope.parameters.references
                console.log('stateParams proviene de lista de deseos',$scope.type,$scope.parameters)

                $scope.itemDetails  = { 
                  name : $scope.parameters.itemDescription,
                  brand: $scope.parameters.brand,
                  brandCode: $scope.parameters.brandCode,
                  createdDate : $scope.CurrentDate,
                  maxDiscount:  0,
                  criticalStock:  0,
                  observation:  "Item creado desde lista de Deseos wishListId:"+ $scope.parameters.wishListId,

                }

                $scope.itemReferences = $scope.referencias.split(','); 
                console.log('itemReferences',$scope.itemReferences)

                $scope.references=[]
                if($scope.itemReferences.length>0){

                    for(var i in $scope.itemReferences){
                      console.log('itemReferences x',$scope.itemReferences[i])
                      $scope.references.push({title:$scope.itemReferences[i],done:false})
                    }
                  
                }
            }
            
            itemsServices.items.itemsList(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data;

              if ($scope.item[0].type == 'Internacional') {
                $scope.internationalPanel  = true
              }
    
              $scope.itemDetails  = { 
                name : $scope.item[0].itemName,
                netPurchaseValue : $scope.item[0].netPurchaseValue, 
                netPrice : $scope.item[0].netPrice,
                vatPrice : $scope.item[0].vatPrice,
                purchasePrice : $scope.item[0].purchasePrice, 
                mayorPrice : $scope.item[0].mayorPrice,  
                createdDate : $scope.item[0].createdDate, 
                competitionPrice : $scope.item[0].competitionPrice,
                maxDiscount : parseInt($scope.item[0].maxDiscount),
                observation: $scope.item[0].observation,
                brand: $scope.item[0].brand,
                brandCode: $scope.item[0].brandCode,
                webType: $scope.item[0].webType,
                driveLink: $scope.item[0].driveLink,
                web: $scope.item[0].web,
                freeMarket: $scope.item[0].freeMarket,
                offerIs: $scope.item[0].offerIs,
                freeMarketCode: $scope.item[0].freeMarketCode,
                oil: $scope.item[0].oil,
                criticalStock: $scope.item[0].criticalStock,
                type: $scope.item[0].type,
                currency: $scope.item[0].currency,
                currencyValue: $scope.item[0].currencyValue,
                providers: [],
                categories:  [],
                locations: $scope.item[0].warehouses[0].locations,
                stock: $scope.item[0].warehouses[0].stock,
                references:   $scope.item[0].references,
                warehouses:$scope.item[0].warehouses

              }

              $scope.references=[]
              if($scope.item[0].references.length>0){
                  for(var i in $scope.item[0].references){
                    $scope.references.push({title:$scope.item[0].references[i],done:false})
                  }
                
              }

              for(var i in $scope.item[0].providers){
                  $scope.itemDetails.providers.push({
                      'providerName': $scope.item[0].providers[i].providerName,
                      'providerId': $scope.item[0].providers[i].providerCode 
                  })
              }

              
              for(var i in $scope.item[0].categories){
                  $scope.itemDetails.categories.push({
                      'categoryName': $scope.item[0].categories[i].categoryName,
                      'categoryCode': $scope.item[0].categories[i].categoryCode 
                  })
              }


              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            providersServices.providers.getProviders().$promise.then((dataReturn) => {
              $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

            categoriesServices.categories.getCategories().$promise.then((dataReturn) => {
              $scope.categories = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            
            function internationalType(value) {
              if (value == "Internacional") {
                $scope.internationalPanel  = true
              }else{
                $scope.internationalPanel  = false
              }
            }

            function callLocations(warehouseId) {

                let params = {
                    warehouseId : warehouseId
                }

                locationsServices.locations.getLocations(params).$promise.then((dataReturn) => {
                  $scope.locations = dataReturn.data;
                  $scope.locationsInput = false
                  $scope.locations.selected = []
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                })

                let params2 = {
                    itemId : $stateParams.idItem,
                    warehouseId : warehouseId
                }
                itemsServices.items.getLocations(params2).$promise.then((dataReturn) => {
                  $scope.itemDetails.stock = dataReturn.stock[0].stock
                  $scope.itemDetails.locations = dataReturn.data == null ? [] : dataReturn.data
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                })
            }

            function cloneItem() {

                var tempRef=[]

                for(var t in $scope.references){
                  tempRef.push($scope.references[t].title)
                }

                if ($scope.itemDetails.type == "Nacional"){

                    $scope.itemDetails.currency = "Peso Chileno"
                    $scope.itemDetails.currencyValue = $scope.itemDetails.purchasePrice
                 }

                let model = {
                    itemId: $stateParams.idItem,
                    userId: $scope.userInfo.id,
                    itemDescription: $scope.itemDetails.name,
                    purchasePrice:  $scope.itemDetails.purchasePrice,
                    netPurchaseValue:  $scope.itemDetails.netPurchaseValue,
                    netPrice: $scope.itemDetails.netPrice,
                    mayorPrice: $scope.itemDetails.mayorPrice,
                    maxDiscount:  $scope.itemDetails.maxDiscount,
                    competitionPrice:  $scope.itemDetails.competitionPrice,
                    observation:  $scope.itemDetails.observation,
                    brand:  $scope.itemDetails.brand,
                    brandCode:  $scope.itemDetails.brandCode,
                    webType:  $scope.itemDetails.webType,
                    driveLink:  $scope.itemDetails.driveLink,
                    web:  $scope.itemDetails.web,
                    freeMarket:  $scope.itemDetails.freeMarket,
                    offerIs:  $scope.itemDetails.offerIs,
                    freeMarketCode:  $scope.itemDetails.freeMarketCode,
                    oil:  $scope.itemDetails.oil,
                    type:  $scope.itemDetails.type,
                    currency:  $scope.itemDetails.currency,
                    currencyValue:  $scope.itemDetails.currencyValue,
                    criticalStock:  $scope.itemDetails.criticalStock,
                    isKit:0,
                    providersIds:[],
                    references:tempRef,
                    categoriesIds:[],
                    warehouses:[{
                                warehouseId: 1, 
                                locationsIds: [419]
                            },
                            {
                                warehouseId: 2, 
                                locationsIds: [1096]
                            },
                            {
                                warehouseId: 3, 
                                locationsIds: [1615]
                            },
                            {
                                warehouseId: 4, 
                                locationsIds: [2626]
                            },
                            {
                                warehouseId: 5, 
                                locationsIds: [3117] 
                            },
                            {
                                warehouseId: 6, 
                                locationsIds: [3396] 
                            }

                        ]
                }

                // $scope.itemDetails.providersIds=[]
                for(var i in  $scope.itemDetails.providers){
                    model.providersIds.push($scope.itemDetails.providers[i].providerId)
                }

                //   $scope.itemDetails.categoriesIds=[]
                if(Object.keys( $scope.itemDetails.categories).length>0){
                    for(var i in  $scope.itemDetails.categories){
                           if(typeof $scope.itemDetails.categories[i].categoryId ==='undefined')
                           model.categoriesIds.push($scope.itemDetails.categories[i].categoryCode)
                           else
                           model.categoriesIds.push($scope.itemDetails.categories[i].categoryId)
                      }
                }else{
                    ngNotify.set('Error al actualizar category','error')  
                    return false;
                }

                if (Object.entries(model.references).length === 0) {
                          ngNotify.set('Debe asociar referencias al ITEM ','error')
                        }else{

                            itemsServices.items.itemCreate(model).$promise.then((dataReturn) => {
                              $scope.msg = dataReturn.data;
                                ngNotify.set('Se ha creado el Item correctamente','success')
                                

                                const row = {
                                    itemId: dataReturn.data,
                                    itemDescription: $scope.itemDetails.name

                                  }

                                  let itemsValue = {
                                    documentId: null,
                                    itemId: $scope.msg,
                                    itemDescription: $scope.itemDetails.name,
                                    location : "undefined",
                                    previousStock: 0,
                                    currentStock: 0,
                                    price: $scope.itemDetails.netPrice,
                                    quantity: null,
                                    originId: undefined,
                                    userId: $scope.userInfo.id,
                                    document: "------",
                                    type: "Creación",
                                    generalStock: 0,
                                    currentprice: $scope.itemDetails.netPrice,
                                    currentLocation: "undefined",
                                    destiny: undefined
              
                                  }
                                 
                                 console.log('that',itemsValue);
                   
                                 itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                                        $scope.result2 = dataReturn.data;
                                        console.log('Historial del item actualizado correctamente');
                                    },(err) => {
                                        console.log('No se ha podido conectar con el servicio',err);
                                    })

                                  // $state.go('app.itemsDetail', row)

                                  if ($scope.type == "wishlist") {

                                    const update = {
                                      itemId: dataReturn.data,
                                      wishListId : $scope.parameters.wishListId

                                    }
                                    wishlistServices.wishlist.wishlistUpdateId(update).$promise.then((dataReturn) => {
                                      console.log('Se asocia ID creado a item lista deseos');
                                      
                                      },(err) => {
                                          console.log('No se ha podido conectar con el servicio',err);
                                    })
                                  }
                                
                                if (model.webType == 'SI'){
                                      webItemCreate(row,model)
                                      console.log('tipo web',row,model);    
                                    }else{ 
                                        $state.go('app.itemsDetail', row)
                                    }
                                   },(err) => {
                                      ngNotify.set('Error al crear Item','error')
                                      // $scope.saveButton  = false
                                })


                        }
            }


            function webItemCreate(row,model) {

                if (model.observation == undefined) {

                    $scope.webDescription = model.itemDescription

                }else{

                    $scope.webDescription = model.observation
                }

                let webModel = {
                    sku: row.itemId,
                    name: model.itemDescription,
                    type: "simple",
                    regular_price: model.netPrice*1.19,
                    description: $scope.webDescription,
                    short_description:model.itemDescription,
                    manage_stock: true,
                    stock_quantity: 0

                }

                console.log('modelo',webModel);

                itemsServices.woocommerce.itemCreate(webModel).$promise.then((dataReturn) => {
                  $scope.result= dataReturn.data;
                  $scope.message= dataReturn.message;
                  console.log('mensaje',$scope.message); 
                  console.log('permalink',$scope.result.permalink);

                  let data = {
                                    itemId: row.itemId,
                                    web: $scope.result.permalink
                                }

                      itemsServices.items.permalinkAdd(data).$promise.then((dataReturn) => {
                          $scope.msg = dataReturn.data;
                          console.log('link creado',$scope.msg);
                          ngNotify.set('Repuesto creado en sitio web correctamente','info')
                          $window.open($scope.result.permalink, "_blank");
                          $state.go('app.itemsDetail', row)

                          },(err) => {
                              console.log('Error al crear crear items',err);
                              console.log('Error al crear crear items',$scope.message);

                              ngNotify.set( 'Error API Web: ' + $scope.message, {
                                sticky: true,
                                button: true,
                                type : 'warn'
                            })
                              $state.go('app.itemsDetail', row)
                          })

                  },(err) => {
                      console.log('Error para conectar con API Woocommerce',err);

                      ngNotify.set( 'Error API Web: ', {
                        sticky: true,
                        button: true,
                        type : 'error'
                    })
                      $state.go('app.itemsDetail', row)
                  })
             
            }

            function updateBodegas() {
              console.log('$scope.itemDetails.stock : ',$scope.itemDetails.stock)
              let params = {
                  itemId : $stateParams.idItem,
                  userId: $scope.userInfo.id,
                  stock : $scope.itemDetails.stock == undefined ? 0 : $scope.itemDetails.stock,
                  warehouseId : $scope.itemDetails.warehouses[0].warehouseId,
                  locations : $scope.itemDetails.locations
              }
              itemsServices.items.itemWarehouseUpdate(params).$promise.then((dataReturn) => {
                  ngNotify.set('Parametros item/bodega actualizado exitosamente','success')
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                })

            }

            // $scope.references = [];

            $scope.addReference = function() {
                console.log('entra a wea')
              $scope.references.push({'title': $scope.reference, 'done':false})
              $scope.reference = ''
            }

            $scope.deleteReference = function(index) {  
              $scope.references.splice(index, 1);
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

itemsCloneController.$inject = ['$scope','UserInfoConstant','$timeout','itemsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams','warehousesServices','locationsServices','providersServices','categoriesServices','$window','wishlistServices'];

