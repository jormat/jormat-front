/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> relatedItemsController
* # As Controller --> relatedItems														
*/
  export default class relatedItemsController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,itemsServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
	    vm.cancel = cancel
        $scope.itemId = $scope.itemsData.itemId
	    $scope.itemName = $scope.itemsData.itemDescription
   
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
	   
        // loadInvoices()
        

        $scope.closeWindows = function() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');

          }
        

        i18nService.setCurrentLang('es');

        let model = {
            itemId : $scope.itemId
        }

        itemsServices.items.getListItemsRelations(model).$promise.then((dataReturn) => {
                  $scope.relatedItems = dataReturn.data;
                  console.log('relatedItems',$scope.relatedItems);

                  $scope.references=[]
                if($scope.relatedItems.length>0){
                    for(var i in $scope.relatedItems){
                        $scope.references.push({itemId:$scope.relatedItems[i].itemId,done:false,'itemDescription':$scope.relatedItems[i].itemDescription})
                    }
                    
                }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

              itemsServices.items.getItemsQuickView().$promise.then((dataReturn) => {
                $scope.itemsGrid.data = dataReturn.data;
            },(err) => {
                console.log('No se ha podido conectar con el servicio',err);
            })

        $scope.itemsGrid = {
                enableFiltering: true,
                columnDefs : [
                    { 
                        name: '',
                        field: 'href',
                        width: '6%', 
                        enableFiltering: false,
                        cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                     '<button ng-disabled="grid.appScope.itemId == row.entity.itemId" ng-click="grid.appScope.addReference(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                     '</div>'
                      },
                    {
                        name            : 'id',
                        field           : 'itemId',
                        cellTemplate    : '<div class="ui-grid-cell-contents ">'+
                                            '<a ng-click="grid.appScope.viewItem(row.entity, true)">{{row.entity.itemId}}</a>' +
                                            '</div>',
                        width           : '11%'
                    },
                    {
                        name        : 'Descripción',
                        field       : 'itemDescription',
                        width       : '35%',
                        cellTemplate: '<div class="ui-grid-cell-contents ">'+
                                        '<p class="uppercase" style="font-size: 12px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                        '</div>',
                        enablePinning  : false,
                        enableFiltering: true
                    },
                    {
                        name        : 'Referencias',
                        field       : 'references',
                        width       : '31%',
                        cellTemplate: '<div class="ui-grid-cell-contents ">'+
                                        '<p class="uppercase text-muted" style="font-size: 11px;"><strong>{{row.entity.references}}</strong></p>' +
                                        '</div>',
                        enableFiltering: true,
                        enablePinning  : false
                    },
                    {
                        name        : 'Marca',
                        field       : 'brand',
                        width       : '15%',
                        cellTemplate: '<div class="ui-grid-cell-contents ">'+
                                        '<p class="uppercase badge bg-warning" style="font-size: 10px;">{{row.entity.brand}}</p>' +
                                        '</div>' ,
                        enableFiltering : true,
                        enablePinning   : false,
                        enableColumnMenu: false,
                        enableSorting   : false
                    }
                ]
            }


	    function cancel() {
            window.close();
			console.log('cerrando modal');
			this.$modalInstance.dismiss('chao');
		}


        $scope.viewItem = function(row) {
            $scope.itemsData = row;
                const modalInstance  = $modal.open({
                    template: require('../view/items-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'itemsViewController',
                    controllerAs: 'itemsView',
                    size: 'lg'
                })
        }

            $scope.addReference = function(row) {

                let contador = 0;
                    for(var i in $scope.references){

                        if($scope.references[i].itemId == row.itemId){
                            ngNotify.set('Item '+ row.itemId +' ya esta asociado','warn')
                            contador++;

                          }else{
                            console.log("item no puplicado")
                            
                          }   
                    }

                    console.log(contador)
                    if (contador == 0){

                        $scope.references.push({'itemId': row.itemId, 'done':false,'itemDescription':row.itemDescription})

                        let model = {
                            itemId : $scope.itemId,
                            relatedItem:row.itemId
                        }

                        itemsServices.items.relatedItems(model).$promise.then((dataReturn) => {
                            $scope.request = dataReturn.data;
                            ngNotify.set('Item relacionado correctamente', 'success')
                        },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                    
               

            }

            $scope.deleteReference = function(index,relatedItem) {  
              $scope.references.splice(index, 1);

              let model = {
                itemId : $scope.itemId,
                relatedItem:relatedItem
              }

              itemsServices.items.relatedItemsDelete(model).$promise.then((dataReturn) => {
                $scope.request = dataReturn.data;
                ngNotify.set('Se elimina relacion con item correctamente', 'success')
                },(err) => {
                    console.log('No se ha podido conectar con el servicio',err);
                })

              
            }
    }

  }

  relatedItemsController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','itemsServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
