/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsObservationsController
* # As Controller --> itemsObservations														
*/


  export default class itemsObservationsController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,itemsServices,$location,UserInfoConstant,ngNotify,$state,warehousesServices) {

    	var vm = this
	    vm.cancel = cancel
	    vm.edit = edit
        vm.loadObservation = loadObservation
        vm.updateObservation = updateObservation
	    $scope.itemId = $scope.itemsData.itemId
	    $scope.itemName = $scope.itemsData.itemDescription

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

	    let params = {
            itemId: $scope.itemId
          }

	    itemsServices.items.itemsList(params).$promise.then((dataReturn) => {
         		  $scope.item = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        function loadObservation(id) {
            $scope.warehouseId = id
            let params = {
                itemId: $scope.itemId,
                warehouseId : id
            }

            itemsServices.items.getObservations(params).$promise.then((dataReturn) => {
                  $scope.observation = dataReturn.data;

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

        function updateObservation() {
            
            let params = {
                itemId: $scope.itemId,
                userId : $scope.userInfo.id,
                warehouseId : $scope.warehouseId,
                comment : $scope.observation[0].observation
            }

            itemsServices.items.updateObservation(params).$promise.then((dataReturn) => {
                  ngNotify.set('Se ha asociado la observación al Item de manera correcta','success')
                  
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

        function cancel() {
            console.log('cerrando modal');
            $modalInstance.dismiss('chao');
        }

		function edit() {
			$state.go("app.itemsUpdate");
		}

		


    }

  }

  itemsObservationsController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','itemsServices','$location','UserInfoConstant','ngNotify','$state','warehousesServices'];
