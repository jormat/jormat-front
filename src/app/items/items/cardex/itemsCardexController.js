/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsCardexController
* # As Controller --> itemsCardex														
*/
  export default class itemsCardexController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,reportsItemsServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService,$window) {

    	var vm = this
	    vm.cancel = cancel
        vm.viewDocument = viewDocument 
        vm.historicalView = historicalView
        $scope.itemId = $scope.itemsData.itemId
        $scope.itemName = $scope.itemsData.itemDescription
        $scope.brand = $scope.itemsData.brand
        $scope.categories = $scope.itemsData.categories


        let model = {
            itemId: $scope.itemId
        }

        i18nService.setCurrentLang('es');

        

        $scope.itemsGridCardex = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'kardex_item_'+$scope.itemId+'.csv',
                columnDefs: [
                   
                    { 
                      name:'Id Doc.',
                      field: 'documentId',
                      width: '9%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<a class="badge bg-accent" ng-click="grid.appScope.itemsCardex.viewDocument(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>'
                    },
                    { 
                      name:'Documento',
                      field: 'codeDoc',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning">{{row.entity.codeDoc}}</p>' +
                                 '</div>'  
                    },
                     { 
                      name:'Cant.',
                      field: 'outputs',
                      width: '8%' ,
                      enableFiltering: false,
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<a><span class="label bg-secundary">{{row.entity.outputs}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'tipo',
                      field: 'type',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-info">{{row.entity.type}}</p>' +
                                 '</div>'  
                    },
                   
                    { 
                      name:'Fecha',
                      field: 'fecha',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.fecha}}</p>' +
                                 '</div>' 
                    },

                    { 
                      name:'Hora',
                      field: 'time',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p style="font-size: 10px;">{{row.entity.time}}</p>' +
                                 '</div>' 
                    },

                    

                    { 
                      name:'Origen',
                      field: 'origin',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase ">{{row.entity.origin}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Destino',
                      field: 'destination',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase ">{{row.entity.destination}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'usuario',
                      field: 'userName',
                      width: '15%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secundary"  style="font-size: 10px;">{{row.entity.userName}}</p>' +
                                 '</div>' 
                    },
                ]
        }

        reportsItemsServices.reports.getCardexItems(model).$promise.then((dataReturn) => {
              $scope.itemsGridCardex.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        function viewDocument(row){
                $scope.invoiceData = row;

                if ($scope.invoiceData.codeDoc == "FACT") {

                    $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName
                    }
                    console.log('ver factura');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.codeDoc == "DOC") { 

                    $scope.invoiceData = {
                        documentId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName 
                    }
                    console.log('ver documento nn');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/documents-nn/view/documents-nn-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'documentsNnViewController',
                            controllerAs: 'documentsNnView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.codeDoc == "NOT") { 

                    $scope.creditNoteData = {
                        creditNoteId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName  
                    }
                    console.log('ver nota credito nn');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/credit-notes/view/credit-notes-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'creditNotesViewController',
                            controllerAs: 'creditNotesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.codeDoc == "BOL") { 

                    $scope.ballotsData = {
                        ballotId:$scope.invoiceData.documentId ,
                        clientName:$scope.invoiceData.clientName 
                    }
                    console.log('Boleta');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/ballots/view/ballots-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.codeDoc == "FACT-PROV") { 

                    $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId,
                        providerName:$scope.invoiceData.clientName  
                    }
                    console.log('factura proveedor');
                    var modalInstance  = $modal.open({
                        template: require('../../../purchases/invoices-provider/view/invoices-provider-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'invoicesProviderViewController',
                        controllerAs: 'invoicesProviderView',
                        size: 'lg'
                })

                }


                if ($scope.invoiceData.codeDoc == "AJU") { 

                    $scope.guideData = {
                        transferGuideId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName  
                    }
                    console.log('ajuste stock');
                    var modalInstance  = $modal.open({
                            template: require('../../../warehouse/stock-adjustment/view/stock-adjustment-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'stockAdjustmentViewController',
                            controllerAs: 'stockAdjustmentView',
                            size: 'lg'
                    })

                }


                if ($scope.invoiceData.codeDoc == "GUI") { 

                    $scope.guideData = {
                        transferGuideId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName  
                    }
                    console.log('guias');
                    var modalInstance  = $modal.open({
                            template: require('../../../warehouse/transfer-guides/view/guides-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'guidesViewController',
                        controllerAs: 'guidesView',
                            size: 'lg'
                    })

                }

            }


	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function historicalView() {

            const url = $state.href("app.itemsHistoryView", { 
				idItem: $scope.itemId, 
                itemDescription: $scope.itemName
				});

			$window.open(url,'_blank');
		}



    }

  }

  itemsCardexController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','reportsItemsServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService','$window'];
