/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> itemsServices
* file for call at services for items
*/

class itemsServices {

	constructor($resource,SERVICE_URL_CONSTANT) {
		
		var items = $resource(SERVICE_URL_CONSTANT.jormat + '/items/:route',{},{

				getItems: {
					method:'GET',
					params : {
					route: 'list'
					}
				},
				getItemsQuickView: {
					method:'GET',
					params : {
					route: 'list-quick-view'
					}
				},
				getListItemsRelations: {
					method:'GET',
					params : {
					route: 'list-items-relations'
					}
				},
				getItemsMain: {
					method:'GET',
					params : {
					route: 'list-main'
					}
				},

				getItemsFreeMarket: {
					method:'GET',
					params : {
					route: 'free-market'
					}
				},
				itemsList: {
					method:'GET',
					params : {
						route: ''
						}
				},
				getLocations: {
					method:'GET',
					params : {
						route: 'locations'
						}
				},
				itemCreate: {
					method:'POST',
					params : {
						route: ''
						}
				},
				itemUpdate: {
					method:'PUT',
					params : {
						route: ''
						}
				},
				itemWarehouseUpdate: {
					method:'PUT',
					params : {
						route: 'itemWarehouseUpdate'
						}
				},
				itemDisabled: {
					method:'DELETE',
					params : {
						route: ''
						}
				},
				getObservations: {
					method:'GET',
					params : {
						route: 'observations'
						}
				},
				updateObservation: {
					method:'PUT',
					params : {
						route: 'observations'
						}
				},
				getStock: {
					method:'GET',
					params : {
						route: 'stock'
						}
				},

				getGeneralStockWeb: {
						method:'GET',
						params : {
							route: 'general-stock-web' 
							}
						},
				getGeneralStock: {
						method:'GET',
						params : {
							route: 'general-stock'
							}
						},
				getGeneralStockByItemId: {
						method:'GET',
						params : {
							route: 'general-stock-id' 
							}
						},

				getItemsTransit: {
						method:'GET',
						params : {
							route: 'transit'
							}
						},

				getImportInTransitItems: {
						method:'GET',
						params : {
							route: 'import-transit'
							}
						},

				statusFail: {
						method:'PUT',
						params : {
							route: 'status-fail'
							}
						},
				statusPriority: {
						method:'PUT',
						params : {
							route: 'priority'
							}
						},

				permalinkAdd: {
						method:'PUT',
						params : {
						  route: 'permalink'
						  }
						},


				downloadItemsPDF:{
					method:'GET',
					params : {
						route: 'downloadItemsPDF'
					}
				},
				getHistoric:{
					method:'GET',
					params : {
						route: 'historic'
					}
				},
				historicalItems: {
						method:'POST',
						params : {
								route: 'historical'
						}
				},
				historicalTableItems: {
						method:'GET',
						params : {
								route: 'historical-table'
						}
				},
				getCurrentInfoItem: {
					method:'GET',
					params : {
						route: 'current-info'
						}
				},
				relatedItems: {
						method:'POST',
						params : {
								route: 'items-relations'
						}
				},

				relatedItemsDelete: {
					method:'DELETE',
					params : {
							route: 'items-relations'
					}
			}
		})

		var woocommerce = $resource(SERVICE_URL_CONSTANT.woocommerce + '/firmare-api/woocommerce/:route',{},{
			
				itemCreate: {
					method:'POST',
					params : {
						route: 'create'
						}
					}
		})

		var woocommerceAPI = $resource(SERVICE_URL_CONSTANT.woocommerce + '/firmare-api/woocommerce/:sku/:route',{ sku: '@sku' },{
			
				
				itemUpdate: {
					method:'POST',
					// transformResponse: function(data, headersGetter){       
					// 	return angular.fromJson(data.stock_quantity);     
					//     },
					params : {
						route: 'update'
						}
					}


		})

		// console.log('link API',woocommerce)

		return { 

				items : items,
				woocommerce:woocommerce,
				woocommerceAPI:woocommerceAPI
						 
				}
	}
}

	itemsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

	export default  angular.module('services.itemsServices', [])
	.service('itemsServices', itemsServices)
	.name;