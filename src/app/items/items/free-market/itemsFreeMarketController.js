/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsFreeMarketController
* # As Controller --> itemsFreeMarket														
*/
  export default class itemsFreeMarketController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,itemsServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
	    vm.cancel = cancel
        vm.viewItem =viewItem
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
	   
        // loadInvoices()

        i18nService.setCurrentLang('es');

        itemsServices.items.getItemsFreeMarket().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        $scope.itemsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'items_mercado_libre.csv',
                exporterMenuPdf: false,
                columnDefs : [
                    {
                        name            : 'id',
                        field           : 'itemId',
                        cellTemplate    : '<div class="ui-grid-cell-contents ">'+
                                            '<a ng-click="grid.appScope.itemsFreeMarket.viewItem(row.entity, true)">{{row.entity.itemId}}</a>' +
                                            '</div>',
                        width           : '7%',
                        cellClass       : function(grid, row) {
                            if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
                            if (row.entity.priority === 1) { 
                                return 'yellow';
                            }

                            if (row.entity.failure === 1) { 
                                return 'red';
                            }
                        }
                    },
                    {
                        name        : 'Descripción',
                        field       : 'itemDescription',
                        width       : '31%',
                        cellTemplate: '<div class="ui-grid-cell-contents ">'+
                                        '<p class="uppercase" style="font-size: 12px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                        '</div>',
                        enablePinning  : false,
                        enableFiltering: true,
                        cellClass: function(grid, row) {
                            if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
                            if (row.entity.priority === 1) {
                                return 'yellow';
                            }
                            if (row.entity.failure === 1) { 
                                return 'red';
                            }
                        }
                    },
                    {
                        name        : 'Referencias',
                        field       : 'references',
                        width       : '27%',
                        cellTemplate: '<div class="ui-grid-cell-contents ">'+
                                        '<p class="uppercase text-muted" style="font-size: 11px;"><strong>{{row.entity.references}}</strong></p>' +
                                        '</div>',
                        enableFiltering: true,
                        enablePinning  : false,
                        cellClass: function(grid, row) {
                            if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
                            if (row.entity.priority === 1) {
                                return 'yellow';
                            }
                            if (row.entity.failure === 1) { 
                                return 'red';
                            }
                        }
                    },
                    {
                        name        : 'Marca',
                        field       : 'brand',
                        width       : '10%',
                        cellTemplate: '<div class="ui-grid-cell-contents ">'+
                                        '<p class="uppercase badge bg-warning" style="font-size: 10px;">{{row.entity.brand}}</p>' +
                                        '</div>' ,
                        enableFiltering : true,
                        enablePinning   : false,
                        enableColumnMenu: false,
                        enableSorting   : false,
                        cellClass: function(grid, row) {
                            if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
                            if (row.entity.priority === 1) {
                                return 'yellow';
                            }
                            if (row.entity.failure === 1) { 
                                return 'red';
                            }
                        }
                    },
                    {
                        name        : 'Precio/Iva',
                        field       : 'vatPrice',
                        width       : '10%',
                        cellTemplate: '<div class="ui-grid-cell-contents ">'+
                                        '<p >$ {{row.entity.vatPrice | pesosChilenos}}</p>' +
                                        '</div>',
                        enableFiltering : false,
                        enablePinning: false,
                        cellClass: function(grid, row) {
                            if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
                            if (row.entity.priority === 1) {
                                return 'yellow';
                            }
                            if (row.entity.failure === 1) { 
                                return 'red';
                            }
                        }
                    },
                    {
                        name        : 'A mano',
                        field       : 'generalStock',
                        width       : '7%',
                        cellTemplate: '<div class="ui-grid-cell-contents ">'+
                                        '<p tooltip="Stock a Mano"><strong>{{row.entity.generalStock}}</strong></p>' +
                                        '</div>',
                        enablePinning: false,
                        enableFiltering: true,
                        cellClass: function(grid, row) {
                            if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
                            if (row.entity.priority === 1) {
                                return 'yellow';
                            }
                            if (row.entity.failure === 1) { 
                                return 'red';
                            }
                        }
                    },
                    {
                        name        : '',
                        width       : '7%', 
                        field       : 'href',
                        enableFiltering: false,
                        enableHiding : false,
                        cellTemplate: '<div class="ui-grid-cell-contents ">'+
                                        '<a href="{{row.entity.freeMarket}}" target="_bank" class="uppercase label bg-secondary" style="font-size: 9px;">Link</a>' +
                                        '</div>' ,
                        cellClass   : function(grid, row) {
                            if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
                            if (row.entity.priority === 1) {
                                return 'yellow';
                            }
                            if (row.entity.failure === 1) { 
                                return 'red';
                            }
                            
                        }
                    },
                ]
            }


	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function viewItem(row) {
            $scope.itemsData = row;
                const modalInstance  = $modal.open({
                    template: require('../view/items-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'itemsViewController',
                    controllerAs: 'itemsView',
                    size: 'lg'
                })
        }



    }

  }

  itemsFreeMarketController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','itemsServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
