/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsViewController
* # As Controller --> itemsView														
*/


export default class itemsViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,itemsServices,$location,UserInfoConstant,ngNotify,$state,$modal, $window, SERVICE_URL_CONSTANT) {
		this.$scope = $scope
		this.$modalInstance = $modalInstance
		this.$modal = $modal
		this.$state = $state
		this.$window = $window
		this.ngNotify = ngNotify
		this.baseUrl = SERVICE_URL_CONSTANT
		$scope.baseUrl = SERVICE_URL_CONSTANT
		this.itemsServices = itemsServices.items
	    this.$scope.itemId = this.$scope.itemsData.itemId
	    this.$scope.itemName = this.$scope.itemsData.itemDescription
        this.$scope.transitItemsTable = false
        this.$scope.transitItemsTableText = false
        this.$scope.importInTransitTable = false
        this.$scope.importInTransitItemsTableText = false
		this.selected = ''
		this.disabled = true
		this.$scope.picture= require("../../../../images/unknown.png");
		
		this.$scope.UserInfoConstant = UserInfoConstant
		this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
			if (details !== undefined) {
				this.$scope.userInfo = details.user
				this.main()
			}
		})


		let model = {
            itemId : this.$scope.itemId
        }

		$scope.loadImg = function(id) { 
			$scope.imageUrl2 = $scope.baseUrl.jormat + '/image/image/'+ id
			return $scope.imageUrl2;
		 }

        itemsServices.items.getListItemsRelations(model).$promise.then((dataReturn) => {
                  $scope.relatedItems = dataReturn.data;
				  if($scope.relatedItems.length>0){
					$scope.relatedItemsShow = true  
                  }
                  console.log('relatedItems',$scope.relatedItems);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
	}
	main() {
		this.$scope.imageUrl = this.baseUrl.jormat + '/image/image/'+ this.$scope.itemId
		let params = { 
			itemId: this.$scope.itemId,
			userId: this.$scope.userInfo.id
		}

		this.itemsServices.itemsList(params).$promise.then((dataReturn) => {
			this.$scope.item = dataReturn.data;
			this.$scope.locations = this.$scope.item[0].warehouses;
			console.log('print',this.$scope.locations[0].locations  )

			if (this.$scope.item[0].currency == "Euro"){
				this.$scope.simbol = "€"
			}
			else{
				this.$scope.simbol = "$"
			}
			
			if(this.$scope.item[0].status == 1){
					this.$scope.statusName= "Activo"
			}else{
					this.$scope.statusName= "Inactivo"
			}
			if(this.$scope.item[0].isKit == 0){
					this.$scope.switchKit= false
			}else{
					this.$scope.switchKit= true
			}
			this.$scope.warehouses = this.$scope.item[0].warehouses
			this.$scope.vatPrice = Math.round((this.$scope.item[0].netPrice)*1.19)
		},(err) => {
			console.log('No se ha podido conectar con el servicio',err);
		})

		this.itemsServices.getItemsTransit(params).$promise.then((dataReturn) => {
			this.$scope.inTransit = dataReturn.data;
			if (this.$scope.inTransit.length == 0){
				this.$scope.transitItemsTable = false
				this.$scope.transitItemsTableText = true
			}else{
				this.$scope.transitItemsTable = true
				this.$scope.transitItemsTableText = false
		  }
			
		},(err) => {
			console.log('No se ha podido conectar con el servicio',err);
		})


		this.itemsServices.getImportInTransitItems(params).$promise.then((dataReturn) => {
			
			this.$scope.importInTransit = dataReturn.data;
			console.log('entra servicio',this.$scope.importInTransit);
			if (this.$scope.importInTransit.length == 0){
				this.$scope.importInTransitTable = false
				this.$scope.importInTransitItemsTableText = true
			}else{
				this.$scope.importInTransitTable = true
				this.$scope.importInTransitItemsTableText = false
		  }
			
		},(err) => {
			console.log('No se ha podido conectar con el servicio',err);
		})


	}

	cancel() {
		console.log('cerrando modal');
		this.$modalInstance.dismiss('chao');
	}

	failStatus(value){

		this.$scope.itemsData = {
			itemId: this.$scope.itemId,
			value: value,
			itemDescription: this.$scope.itemName
		}
		const modalInstance  = this.$modal.open({
				template: require('../status-fail/status-fail.html'),
				animation: true,
				scope: this.$scope,
				controller: 'statusFailController',
				controllerAs: 'statusFail'
		})
	}

	wishlistAdd() {

			this.$scope.itemsData = {
				itemId: this.$scope.itemId
		}
            const 	modalInstance  = this.$modal.open({
							template: require('../wishlist/wishlist-items.html'),
							animation: true,
							scope: this.$scope,
							controller: 'wishlistItemsController',
							controllerAs: 'wishlistItems'

					})
		}

	cloneItem() {
			this.$state.go("app.itemsClone", { idItem: this.$scope.itemId });
		}

	updateItem() {
		console.log('entra',this.$scope.itemId)
			this.$state.go("app.itemsUpdate", { itemId: this.$scope.itemId });
		}

	guideView(guideId,clientName) {
		this.$scope.guideData = {
			transferGuideId: guideId,
			clientName: clientName
		}
			const modalInstance  = this.$modal.open({
					template: require('../../../warehouse/transfer-guides/view/guides-view.html'),
					animation: true,
					scope: this.$scope,
					controller: 'guidesViewController',
					controllerAs: 'guidesView',
					size: 'lg'
			})
	}

	importView(importId,clientName) {
		this.$scope.invoiceData = {
			importId: importId,
			providerName: clientName
		}
			const modalInstance  = this.$modal.open({
					template: require('../../../purchases/imports/view/imports-view.html'),
					animation: true,
					scope: this.$scope,
					controller: 'importsViewController',
	                controllerAs: 'importsView',
	                size: 'lg'
			})
	}

	transformToInvoice() {
              this.$state.go("app.clientsInvoicesUpdate", { idInvoice: this.$scope.itemId, discount:0,type:'items'});

            }

    itemQuote() {
              this.$state.go("app.quotationsCreate", { idItem: this.$scope.itemId});

        }

        viewImage() {
		this.$scope.itemsData = {
			itemId: this.$scope.itemId
		}
		const 	modalInstance  = this.$modal.open({
													template: require('../imageView/imageView.html'),
													animation: true,
													scope: this.$scope,
													controller: 'imageViewController',
													controllerAs: 'imageView'
												})
	}
	
	edit() {
		this.$state.go("app.itemsUpdate");
	}

	disabledButton() {
		console.log('disabled')
		this.disabled = false
	}

	print() {

		if (this.selected.locations == undefined) {

			this.ngNotify.set(' Seleccione Bodega','warn')
		}
		let locations = ''
		this.selected.locations.forEach(location => {
			locations += location.locationName + ' , '
			// locations += location.locationName
		})

		const params = {
			iditems: this.$scope.itemId,
			itemName: this.$scope.itemName.toUpperCase(),
			clientId: this.selected.clientId,
			warehouseName: this.selected.warehouseCode,
			warehouseId: this.selected.warehouseId,
			references: this.$scope.item[0].brandCode.toUpperCase(),
			location: locations
		}
		if(this.selected != '') {
			const path = this.baseUrl.jormat + '/items/downloadItemsPDF?iditems=' + params.iditems + '&itemName=' + params.itemName + '&warehouseName=' + params.warehouseName + '&references=' + params.references + '&location=' + params.location
			this.$window.open(path)
		}else {
			this.ngNotify.set(' Seleccione Bodega','warn')
		}
	}

	relatedItems(row) {

		this.$scope.itemsData = {
			itemId: this.$scope.itemId,
			itemDescription:row.itemName
		}

		const 	modalInstance  = this.$modal.open({
						template: require('../related/related-items.html'),
						animation: true,
						scope: this.$scope,
						controller: 'relatedItemsController',
						controllerAs: 'relatedItems',
						size: 'lg'
				})
	}

	itemView(row){
		this.$scope.itemsData = row;
		const modalInstance  = this.$modal.open({
				template: require('../view/items-view.html'),
				animation: true,
				scope: this.$scope,
				controller: 'itemsViewController',
				controllerAs: 'itemsView',
				size: 'lg'
		})
	}

	itemPrint(row){

		console.log('imprimir repuesto'); 

		const url = this.$state.href("app.printItems", { 
			idItem: this.$scope.itemId
			});

		this.$window.open(url,'_blank',"width=600,height=500");

	  // $state.go("app.warehouseQuotationPrint", { idQuotation: $scope.quotationId,warehouse:$scope.originId});

	}
}

itemsViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','itemsServices','$location','UserInfoConstant','ngNotify','$state','$modal', '$window', 'SERVICE_URL_CONSTANT' ];
