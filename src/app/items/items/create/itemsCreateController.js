
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> itemsCreateController
* # As Controller --> itemsCreate														
*/

export default class itemsCreateController{

        constructor($scope,UserInfoConstant,$timeout,itemsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,providersServices,categoriesServices,warehousesServices,locationsServices,applicationsServices,$window){
            var vm = this;
            // vm.callLocations = callLocations
            vm.$mdDialog = $mdDialog
            vm.createItem = createItem
            $scope.locationsInput = true
            $scope.checked  = false
            $scope.providerValue=[] 
            $scope.applicationsValue=[]
            $scope.categoriesItem=[]
            $scope.saveButton  = false
            vm.webItemCreate = webItemCreate
            $scope.maxDiscount = 0
            $scope.criticalStock = 0
            $scope.internationalPanel  = false
            vm.internationalType = internationalType
            $scope.picture= require("../../../../images/unknown.png");


            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })
          
         	providersServices.providers.getProviders().$promise.then((dataReturn) => {
              $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

            categoriesServices.categories.getCategories().$promise.then((dataReturn) => {
              $scope.categories = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

            // warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
            //   $scope.warehouses = dataReturn.data;
            //   },(err) => {
            //       console.log('No se ha podido conectar con el servicio',err);
            // })

            // function callLocations(warehouseId) {

            //     let params = {
            //         warehouseId : warehouseId
            //     }

            //     locationsServices.locations.getLocations(params).$promise.then((dataReturn) => {
            //       $scope.locations = dataReturn.data;
            //       $scope.locationsInput = false
            //       $scope.locations.selected = []
            //       },(err) => {
            //           console.log('No se ha podido conectar con el servicio',err);
            //     })
            // }

            applicationsServices.applications.getApplicationsList().$promise.then((dataReturn) => {
              $scope.applications = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            function webItemCreate(row,model) {

                if (model.observation == undefined) {

                    $scope.webDescription = model.itemDescription

                }else{

                    $scope.webDescription = model.observation
                }

                let webModel = {
                    sku: row.itemId,
                    name: model.itemDescription,
                    type: "simple",
                    regular_price: model.netPrice*1.19,
                    description: $scope.webDescription,
                    short_description:model.itemDescription,
                    manage_stock: true,
                    stock_quantity: 0

                }

                console.log('modelo',webModel);

                itemsServices.woocommerce.itemCreate(webModel).$promise.then((dataReturn) => {
                  $scope.result= dataReturn.data;
                  $scope.message= dataReturn.message;
                  console.log('mensaje',$scope.message); 
                  console.log('permalink',$scope.result.permalink);

                  let data = {
                                    itemId: row.itemId,
                                    web: $scope.result.permalink
                                }

                      itemsServices.items.permalinkAdd(data).$promise.then((dataReturn) => {
                          $scope.msg = dataReturn.data;
                          console.log('link creado',$scope.msg);
                          ngNotify.set('Repuesto creado en sitio web correctamente','info')
                          $window.open($scope.result.permalink, "_blank");
                          $state.go('app.itemsDetail', row)

                          },(err) => {
                              console.log('Error al crear crear items',err);
                              console.log('Error al crear crear items',$scope.message);

                              ngNotify.set( 'Error API Web: ' + $scope.message, {
                                sticky: true,
                                button: true,
                                type : 'warn'
                            })
                              $state.go('app.itemsDetail', row)
                          })

                  },(err) => {
                      console.log('Error para conectar con API Woocommerce',err);

                      ngNotify.set( 'Error API Web: ', {
                        sticky: true,
                        button: true,
                        type : 'error'
                    })
                      $state.go('app.itemsDetail', row)
                  })
             
            }

            function createItem() {
              $scope.saveButton  = true

                if($scope.name && $scope.providerValue.selected.length>0 
                    && $scope.categories.length>0 && $scope.applicationsValue.selected.length>0){

                        var reference=[];
                        for(var i in $scope.references){
                            reference.push($scope.references[i].title)
                        }
                        var locationIds=[]
                      if(typeof $scope.locations!=='undefined')
                           for(var i in $scope.locations.selected){
                              locationIds.push($scope.locations.selected[i].locationId)
                          }
                      // var dWarehouses=[]
                      //  if(typeof $scope.warehouses.selected!=='undefined'){
                      //   dWarehouses=[{
                      //       warehouseId:$scope.warehouses.selected.warehouseId,
                      //       locationsIds:locationIds
                      //   }]
                      //  }

                    let model = {
                        userId: $scope.userInfo.id,
                        itemDescription: $scope.name,
                        purchasePrice:  $scope.purchasePrice,
                        netPrice: $scope.netPrice, 
                        netPurchaseValue: $scope.netPurchaseValue,
                        maxDiscount:  $scope.maxDiscount,
                        competitionPrice:  $scope.competitionPrice,
                        observation:  $scope.observation,
                        brand:  $scope.brand,
                        brandCode:  $scope.brandCode,
                        criticalStock:  $scope.criticalStock,
                        type:  $scope.type,
                        currency:  $scope.currency,
                        currencyValue:  $scope.currencyValue,
                        oil:  $scope.oil,
                        offerIs:  $scope.offerIs,
                        isKit:0,
                        webType:  $scope.webType,
                        driveLink:$scope.driveLink,
                        freeMarket:$scope.freeMarket,
                        freeMarketCode:$scope.freeMarketCode,
                        mayorPrice:$scope.mayorPrice,
                        web:$scope.web,
                        providersIds:[],
                        applicationsIds:[],
                        references:reference,
                        categoriesIds:[],
                        // warehouses:dWarehouses
                        warehouses:[{
                                warehouseId: 1, 
                                locationsIds: [419]
                            },
                            {
                                warehouseId: 2, 
                                locationsIds: [1096]
                            },
                            {
                                warehouseId: 3, 
                                locationsIds: [1615]
                            },
                            {
                                warehouseId: 4, 
                                locationsIds: [2626]
                            },
                            {
                                warehouseId: 5, 
                                locationsIds: [3117] 
                            },
                            {
                                warehouseId: 6, 
                                locationsIds: [3396] 
                            }

                        ]
                    }
                     //$scope.itemDetails.providersIds=[]
                      for(var i in  $scope.providerValue.selected){
                        model.providersIds.push($scope.providerValue.selected[i].providerId)
                      }

                      //$scope.itemDetails.applicationsIds=[]
                      for(var i in  $scope.applicationsValue.selected){
                        model.applicationsIds.push($scope.applicationsValue.selected[i].applicationId)
                      }
    
                     //$scope.itemDetails.categoriesIds=[]
                      for(var i in $scope.categoriesItem.selected){
                        model.categoriesIds.push($scope.categoriesItem.selected[i].categoryId)
                      }

                      if (Object.entries(model.references).length === 0) {
                          ngNotify.set('Ingresar las referencias a item','warn')
                        }else{

                            console.log('model create item',model);

                            itemsServices.items.itemCreate(model).$promise.then((dataReturn) => {
                              	$scope.msg = dataReturn.data;
                                const row = {
                                  itemId: dataReturn.data,
                                  itemDescription: ''
                                }

                                ngNotify.set('Se ha creado el Item correctamente','success')

                                let itemsValue = {
                                    documentId: null,
                                    itemId: $scope.msg,
                                    itemDescription: $scope.name,
                                    location : "undefined",
                                    previousStock: 0,
                                    currentStock: 0,
                                    price: $scope.netPrice,
                                    quantity: null,
                                    originId: undefined,
                                    userId: $scope.userInfo.id,
                                    document: "------",
                                    type: "Creación",
                                    generalStock: 0,
                                    currentprice: $scope.netPrice,
                                    currentLocation: "undefined",
                                    destiny: undefined
              
                                  }
                                 
                                 console.log('that',itemsValue);
                   
                                 itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                                        $scope.result2 = dataReturn.data;
                                        console.log('Historial del item actualizado correctamente');
                                    },(err) => {
                                        console.log('No se ha podido conectar con el servicio',err);
                                    })
                                
                                

                                if (model.webType == 'SI'){
                                      webItemCreate(row,model)
                                      console.log('tipo web',row,model);    
                                }else{ 
                                    $state.go('app.itemsDetail', row)
                                }

								
                              },(err) => {
                                  ngNotify.set('Error al crear Item','error')
                                  $scope.saveButton  = false
                              })


                        }

                }


            }

            $scope.references = [];

            $scope.addReference = function() {
              $scope.references.push({'title': $scope.reference, 'done':false})
              $scope.reference = ''
              $scope.saveButton  = false
            }

            $scope.deleteReference = function(index) {  
              $scope.references.splice(index, 1);
            }

            function internationalType(value) {
              if (value == "Internacional") {
                $scope.internationalPanel  = true
              }else{
                $scope.internationalPanel  = false

              }
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
    }   

itemsCreateController.$inject = ['$scope','UserInfoConstant','$timeout','itemsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','providersServices','categoriesServices','warehousesServices','locationsServices','applicationsServices','$window'];

