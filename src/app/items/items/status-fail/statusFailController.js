/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> statusFailController
* # As Controller --> statusFail														
*/


  export default class statusFailController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,itemsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.updateItem = updateItem;
	    $scope.itemId = $scope.itemsData.itemId
	    $scope.itemName = $scope.itemsData.itemDescription
	    $scope.value = $scope.itemsData.value
	    $scope.observation == ''
	    vm.index=$scope.index;
	    vm.grid=$scope.data;

	    vm.$translate = $filter('translate')
	        $rootScope.$on('$translateChangeSuccess', function () {
	   
	        })

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
        //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function updateItem() {

			let model = {
                    itemId : $scope.itemId,
                    value : $scope.value,
                    observation : $scope.observation,
                    userId : $scope.userInfo.id
                }

                itemsServices.items.statusFail(model).$promise.then((dataReturn) => {

                    ngNotify.set('Item actualizado exitosamente','warn');
					
					$modalInstance.dismiss('chao');
					$state.reload("app.itemsDetail");
                  },(err) => {
                      ngNotify.set('Error al modificar Item','error')
                  })
			
		}

    }

  }

  statusFailController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','itemsServices','$location','UserInfoConstant','ngNotify','$state'];
