/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> wishlistItemsController
* # As Controller --> wishlistItems													
*/


  export default class wishlistItemsController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,itemsServices,$location,UserInfoConstant,ngNotify,$state,providersServices,wishlistServices) {

    	var vm = this;
	    vm.cancel = cancel;
        vm.updateWishlist = updateWishlist
        vm.setNameProviders = setNameProviders
	    $scope.items = $scope.itemsData

 

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        providersServices.providers.getProviders().$promise.then((dataReturn) => {
              $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        let params = {
                itemId: $scope.items.itemId
            }

        itemsServices.items.itemsList(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data;

              if ($scope.item[0].type == 'Internacional') {
                $scope.internationalPanel  = true
              }
    
              $scope.itemDetails  = { 
                name : $scope.item[0].itemName,
                netPurchaseValue : $scope.item[0].netPurchaseValue, 
                netPrice : $scope.item[0].netPrice,
                vatPrice : $scope.item[0].vatPrice,
                purchasePrice : $scope.item[0].purchasePrice,  
                createdDate : $scope.item[0].createdDate, 
                competitionPrice : $scope.item[0].competitionPrice,
                maxDiscount : parseInt($scope.item[0].maxDiscount),
                observation: $scope.item[0].observation,
                brand: $scope.item[0].brand,
                brandCode: $scope.item[0].brandCode,
                oil: $scope.item[0].oil,
                criticalStock: $scope.item[0].criticalStock,
                type: $scope.item[0].type,
                currency: $scope.item[0].currency,
                currencyValue: $scope.item[0].currencyValue,
                providers: [],
                categories:  [],
                locations: $scope.item[0].warehouses[0].locations,
                stock: $scope.item[0].warehouses[0].stock,
                references:   $scope.item[0].references,
                warehouses:$scope.item[0].warehouses

              }


              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        function updateWishlist() {
            console.log('new items add');
            let model = {
                            userId: $scope.userInfo.id,
                            itemId : $scope.items.itemId ,
                            itemName : $scope.itemDetails.name,
                            brand : $scope.itemDetails.brand,
                            brandCode : $scope.itemDetails.brandCode,
                            providerId : $scope.providerId, 
                            type : $scope.itemDetails.type, 
                            comments : "Sin comentario",
                            quantities : $scope.quantities,
                            references : $scope.itemDetails.references 
                        }      

                        console.log('modelo',model)   

                        wishlistServices.wishlist.wishlistAddItems(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha ingresado el Repuesto correctamente','success')
                            $scope.result = dataReturn.data

                            for(var i in model.references){
                                $scope.reference = model.references[i]

                                console.log('modelo1',model.references) 
                                let params = {
                                    wishlistId : $scope.result,
                                    reference : $scope.reference
                                } 
                                
                                console.log('params',params) 

                                wishlistServices.wishlist.referencesAddItems(params).$promise.then((dataReturn) => {
                                    console.log('Se ha asociado la referencia correctamente')
                                    $scope.result = dataReturn.data

                                     $state.reload('app.wishlist')
                                     $modalInstance.dismiss('chao');

                              },(err) => {
                                  ngNotify.set('Error al asociar referencia','error')
                              })
                            }
                          },(err) => {
                              ngNotify.set('Error al registrar item','error')
                          })
        }

	    
	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function setNameProviders(providers) {

                $scope.providerName = providers.providerName
                $scope.providerId = providers.providerId
            }


    }

  }

  wishlistItemsController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','itemsServices','$location','UserInfoConstant','ngNotify','$state','providersServices','wishlistServices'];
