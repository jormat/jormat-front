
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsListController
* # As Controller --> itemsList														
*/

export default class itemsListController{

        constructor($rootScope,$scope,UserInfoConstant,$timeout,itemsServices,ngNotify,$state,$mdDialog,$interval,uiGridExporterConstants,$http,$filter,$modal,i18nService, uiGridExporterService){
            var vm = this
			this.$scope = $scope
			this.$timeout = $timeout
			vm.searchData = searchData
			//vm.historyView = historyView
			this.uiGridExporterConstants = uiGridExporterConstants
			this.ngNotify = ngNotify
			this.$state = $state
			this.uiGridExporterService = uiGridExporterService
			this.$modal= $modal
			this.itemsServices = itemsServices
			this.$interval = $interval
			this.$rootScope = $rootScope
			this.i18nService = i18nService
			this.$mdDialog = $mdDialog
			this.i18nService.setCurrentLang('es')
			this.$scope.dropdownMenu = require('./dropdownActionsMenu.html')
			this.params = {
				newPage: 1,
				pageSize: 1000
			}
            this.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

			this.tableInitializeMethod()

			this.$scope.UserInfoConstant = UserInfoConstant
			this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
				if (details !== undefined) {
					this.$scope.userInfo = details.user
					this.main()
				}
			})


			function searchData() {
                  itemsServices.items.getItems().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.gridOptions.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

			/*function historyView(row) {
					this.$scope.itemsData = row;
					const 	modalInstance  = this.$modal.open({
									template: require('../historyView/historyView.html'),
									animation: true,
									scope: this.$scope,
									controller: 'historyViewController',
									controllerAs: 'HistoryView',
									size: 'lg'
							})
				}*/
		  
                
        }

		main() {
			const searchText = ''
			this.params = {
				newPage    	   : 1,
				pageSize       : 2000,
				searchText
			}

			this.getItemsData()
			this.$rootScope.$on('$translateChangeSuccess', () => {
				this.tableInitializeMethod()
			})
		}

		// searchData() {
		// 	this.itemsServices.items.getItems(this.params).$promise
		// 		.then((data) => {
		// 			// this.items = angular.copy(data.data)
		// 			this.$scope.data = data.data;
		// 			this.$scope.gridOptions.data = $filter('filter')(data.data, vm.searchText, undefined);
		// 			// this.$scope.gridOptions.totalItems = data.count || 0
		// 		})
		// }


		printItem(row) {
			console.log('imprimir PackOff');
			var url = this.$state.href("app.printItems",{
				idItem: row.itemId
			})

			window.open(url,'_blank',"width=600,height=700");
		}

		itemQuote(row) {
              this.$state.go("app.quotationsCreate", { idItem: row.itemId});

        }

		viewItem(row, modal = false) {

			this.$scope.itemsData = row;
                const modalInstance  = this.$modal.open({
                    template: require('../view/items-view.html'),
                    animation: true,
                    scope: this.$scope,
                    controller: 'itemsViewController',
                    controllerAs: 'itemsView',
                    size: 'lg'
                })

            // codigo sachiro
			// this.$scope.itemsData = row;
			// if (modal) {
			// 	this.confirm = this.$mdDialog.confirm({
			// 		controller  : 'gridDialogController',
			// 		controllerAs: 'gridDialog',
			// 		template    : require('../dialog/gridDialog.html'),
			// 		locals           : { itemsData: this.$scope.itemsData },
			// 		animation          : true,
			// 		parent             : angular.element(document.body),
			// 		clickOutsideToClose: true,
			// 	})
				
			// 	this.$mdDialog.show(this.confirm).then((row) => {
			// 		if (!row) {
			// 			this.getItemsData()
			// 		}
			// 	})
			// } else {
			// 	this.$state.go('app.itemsDetail', row)
			// }
		}


		productSheetView(row) {
			this.$scope.itemsData = row;
			this.$state.go('app.itemsDetail', row)
		}

		updateItem(row) {
			this.$state.go("app.itemsUpdate", row);
		}

		cloneItem(row) {
			this.$state.go("app.itemsClone", { idItem: row.itemId});
		}

		deleteItem(row) {
			this.$scope.itemsData = row
			const 	modalInstance  = this.$modal.open({
							template: require('../delete/items-delete.html'),
							animation: true,
							scope: this.$scope,
							controller: 'itemsDeleteController',
							controllerAs: 'itemsDelete'
					})
		}

		itemsFreeMarketView() {
			const 	modalInstance  = this.$modal.open({
							template: require('../free-market/items-free-market.html'),
							animation: true,
							scope: this.$scope,
							controller: 'itemsFreeMarketController',
							controllerAs: 'itemsFreeMarket',
							size: 'lg'
					})
		}

		seeObservations(row) {
			this.$scope.itemsData = row;
            const 	modalInstance  = this.$modal.open({
							template: require('../observations/items-observations.html'),
							animation: true,
							scope: this.$scope,
							controller: 'itemsObservationsController',
							controllerAs: 'itemsObservations'

					})
		}

		wishlistAdd(row) {
			this.$scope.itemsData = row;
            const 	modalInstance  = this.$modal.open({
							template: require('../wishlist/wishlist-items.html'),
							animation: true,
							scope: this.$scope,
							controller: 'wishlistItemsController',
							controllerAs: 'wishlistItems'

					})
		}

		transformToInvoice(row) {
              this.$state.go("app.clientsInvoicesUpdate", { idInvoice: row.itemId, discount:0,type:'items'});

            }

		cardexItem(row) {
			this.$scope.itemsData = row;
            const 	modalInstance  = this.$modal.open({
							template: require('../cardex/items-cardex.html'),
							animation: true,
							scope: this.$scope,
							controller: 'itemsCardexController',
							controllerAs: 'itemsCardex',
							size: 'lg'
					})
		}

		relatedItems(row) {
			this.$scope.itemsData = row;
            const 	modalInstance  = this.$modal.open({
							template: require('../related/related-items.html'),
							animation: true,
							scope: this.$scope,
							controller: 'relatedItemsController',
							controllerAs: 'relatedItems',
							size: 'lg'
					})
		}

		priorityUpdate(value, row) {
			console.log('this.userInfo.id',this.$scope.userInfo.id)
			let model = {
				value: value,
				userId : this.$scope.userInfo.id,
				itemId: row.itemId
			}

			


			this.itemsServices.items.statusPriority(model).$promise.then(() => {
				this.ngNotify.set('Se ha actualizado item correctamente','success')
				this.getItemsData()
				},(error) => {
					this.ngNotify.set('Error al dar prioridad ' + error,'error')
			})
		}

		itemsProviders() {
			const 	modalInstance  = this.$modal.open({
					template: require('../items-provider/items-provider.html'),
					animation: true,
					scope: this.$scope,
					controller: 'itemsProviderController',
					controllerAs: 'itemsProvider',
					size: 'lg'
			})
		}

		getItemsData() {
			this.itemsServices.items.getItemsMain(this.params).$promise
				.then((data) => {
					this.items = angular.copy(data.data)
					this.$scope.gridOptions.data = data.data
					this.$scope.gridOptions.totalItems = data.count || 0
				})
		}

		dataDownload(items) {
			let dataReturn = []
	
			items.forEach((row) => {
				row = row.entity ? row.entity : row
				const rowData = [
					{ value: row.itemId },
					{ value: row.itemDescription },
					{ value: row.references },
					{ value: row.brand },
					{ value: row.vatPrice },
					{ value: row.generalStock },
				]
	
				dataReturn.push(rowData)
			})
			console.log(' dataReturn : ', dataReturn)
			return dataReturn
		}

		exportCSV() {
			const exportService = this.uiGridExporterService
			const dataFilter = this.dataDownload(this.gridApi.grid.rows)
			const grid = this.gridApi.grid
			const fileName = 'myfile.csv'
	
			exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
				const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
				let formatColums = []
	
				exportColumnHeaders.map((colums) => {
						formatColums.push(colums)
				})
				const csvContent = exportService.formatAsCsv(formatColums, dataFilter, grid.options.exporterCsvColumnSeparator)
	
				exportService.downloadFile(fileName, csvContent, grid.options.exporterOlderExcelCompatibility)
			}).catch(() => {
				this.ngNotify.set('ERROR', 'error')
			})
		}

		exportAllCSV() {
			const exportService = this.uiGridExporterService
	
			this.itemsServices.items.getItems().$promise.then(
				response => {
					if (!response.status) {
						const msn = response.messageError || response.message || this.$translate('MSJ_ERROR')
	
						this.ngNotify.set(msn, 'warn')
					} else {
						const dataFilter = this.dataDownload(response.data)
						const grid = this.gridApi.grid
						const fileName = 'myfile.csv'
	
						exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
							const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
							let formatColums = []
	
							exportColumnHeaders.map((colums) => {
									formatColums.push(colums)
							})
	
							const csvContent = exportService.formatAsCsv(formatColums, dataFilter, grid.options.exporterCsvColumnSeparator)
	
							exportService.downloadFile(fileName, csvContent, grid.options.exporterOlderExcelCompatibility)
						}).catch(() => {
							this.ngNotify.set('ERROR', 'error')
						})
					}
				},
				() => {
					this.ngNotify.set(this.$translate('MSJ_NO_DATA_RELATED_YOUR_SEARCH') + ' getDownloadAllCsv', 'error')
				},
			)
		}

		tableInitializeMethod() {
			this.$scope.gridOptions = {
				paginationPageSizes  : [ 500, 1000, 2000 ],
				paginationPageSize   : 2000,
				enablePaging         : true,
				useExternalPagination: true,
				enableFiltering      : true,
				enableGridMenu       : true,
				enableSelectAll      : true,
				exporterCsvFilename  : 'Listado_de_items.csv',
				exporterMenuPdf      : false,
				exporterMenuCsv      : false,
				exporterMenuAllData  : false,
				multiSelect          : true,
				onRegisterApi        : (gridApi) => {
					this.gridApi = gridApi
	
					this.$interval(() => {
						this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar vista como csv', action: () => {
							this.exportCSV()
						}, order: 100 } ])
						this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar todo como csv', action: () => {
							this.exportAllCSV()
						}, order: 100 } ])
					}, 0, 1)
	
					gridApi.pagination.on.paginationChanged(this.$scope, (newPage, pageSize) => {
						this.params.newPage = newPage
						this.params.pageSize = pageSize
	
						this.itemsServices.items.getItemsMain(this.params).$promise.then(
							response => {
								if (!response.status) {
									const msn = response.messageError || response.message || 'ERROR'
				
									this.ngNotify.set(msn, 'warn')
								} else {
									this.items = angular.copy(response.data)
									this.$scope.gridOptions.data = response.data
									this.$scope.gridOptions.totalItems = response.count || 0
								}
							},
							() => {
								this.ngNotify.set('Sin respuesta getItemsMain', 'error')
							},
						)
					})
	
					gridApi.core.on.filterChanged(this.$scope, () => {
						const grid = gridApi.grid.renderContainers.body.grid
						let filters = {}
	
						for (let i in grid.columns) {
							if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term != undefined)) {
								filters[grid.columns[i].field] = grid.columns[i].filters[0].term
							}
						}

						this.params.newPage = 1
						this.params.filters = filters
	
						this.itemsServices.items.getItemsMain(this.params).$promise.then(
							response => {
								if (!response.status) {
									const msn = response.messageError || response.message || 'ERROR'
				
									this.ngNotify.set(msn, 'warn')
								} else {
									this.items = angular.copy(response.data)
									this.$scope.gridOptions.data = response.data
									this.$scope.gridOptions.totalItems = response.count || 0
								}
							},
							() => {
								this.ngNotify.set('Sin respuesta getItemsMain', 'error')
							},
						)
					})
				},
				exporterHeaderFilter: (displayName) => {
					if (displayName === 'id') {
						return ''
					} else {
						return displayName
					}
				},
				exporterFieldCallback: (grid, row, col, input) => {
					// if( col.name == 'Índice' ){
					//   return grid.renderContainers.body.visibleRowCache.indexOf(row)+1;
					// } else {
					return input
					// }
				},
				columnDefs : [
					{
						name            : 'id',
						field           : 'itemId',
						cellTemplate    : '<div class="ui-grid-cell-contents ">'+
											'<a ng-click="grid.appScope.itemsList.viewItem(row.entity, true)">{{row.entity.itemId}}</a>' +
											'</div>',
						width			: '7%',
						cellClass		: function(grid, row) {
							if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
							if (row.entity.priority === 1) { 
								return 'yellow';
							}

							if (row.entity.failure === 1) { 
								return 'red';
							}
						}
					},
					{
						name       	: 'Descripción',
						field      	: 'itemDescription',
						width		: '33%',
						cellTemplate: '<div class="ui-grid-cell-contents ">'+
										'<p class="uppercase" style="font-size: 12px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
										'</div>',
						enablePinning  : false,
						enableFiltering: true,
						cellClass: function(grid, row) {
							if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
							if (row.entity.priority === 1) {
								return 'yellow';
							}
							if (row.entity.failure === 1) { 
								return 'red';
							}
						}
					},
					{
						name        : 'Referencias',
						field       : 'references',
						width       : '30%',
						cellTemplate: '<div class="ui-grid-cell-contents ">'+
										'<p class="uppercase text-muted" style="font-size: 11px;"><strong>{{row.entity.references}}</strong></p>' +
										'</div>',
						enableFiltering: true,
						enablePinning  : false,
						cellClass: function(grid, row) {
							if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
							if (row.entity.priority === 1) {
								return 'yellow';
							}
							if (row.entity.failure === 1) { 
								return 'red';
							}
						}
					},
					{
						name        : 'Marca',
						field       : 'brand',
						width       : '10%',
						cellTemplate: '<div class="ui-grid-cell-contents ">'+
										'<p class="uppercase badge bg-warning" style="font-size: 11px;">{{row.entity.brand}}</p>' +
										'</div>' ,
						enableFiltering : true,
						enablePinning   : false,
						enableColumnMenu: false,
						enableSorting   : false,
						cellClass: function(grid, row) {
							if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
							if (row.entity.priority === 1) {
								return 'yellow';
							}
							if (row.entity.failure === 1) { 
								return 'red';
							}
						}
					},
					{
						name        : 'Precio/Iva',
						field       : 'vatPrice',
						width       : '10%',
						cellTemplate: '<div class="ui-grid-cell-contents ">'+
										'<p >$ {{row.entity.vatPrice | pesosChilenos}}</p>' +
										'</div>',
						enableFiltering : false,
						enablePinning: false,
						cellClass: function(grid, row) {
							if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
							if (row.entity.priority === 1) {
								return 'yellow';
							}
							if (row.entity.failure === 1) { 
								return 'red';
							}
						}
					},
					{
						name        : 'A mano',
						field       : 'generalStock',
						width       : '6%',
						cellTemplate: '<div class="ui-grid-cell-contents ">'+
										'<p tooltip="Stock a Mano"><strong>{{row.entity.generalStock}}</strong></p>' +
										'</div>',
						enablePinning: false,
						enableFiltering: false,
						cellClass: function(grid, row) {
							if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
							if (row.entity.priority === 1) {
								return 'yellow';
							}
							if (row.entity.failure === 1) { 
								return 'red';
							}
						}
					},
					{
						name		: '',
						width		: '4%', 
						field		: 'href',
						enableFiltering: false,
						enableHiding : false,
						cellTemplate: this.$scope.dropdownMenu,
						cellClass	: function(grid, row) {
							if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                                return 'critical';
                            }
							if (row.entity.priority === 1) {
								return 'yellow';
							}
							if (row.entity.failure === 1) { 
								return 'red';
							}
							
						}
					},
				],
			}
	
			this.$rootScope.$watch('sidenavState', () => {
				this.$timeout(() => {
					console.log('there was a resize')
					this.gridApi.grid.handleWindowResize()
				}, 500)
			})
		}

		uploadCsv() {
			
            this.confirm = this.$mdDialog.confirm({
                controller         : 'uploadCsvItemController',
                controllerAs       : 'uploadCsvItem',
                template           : require('./items/uploadCsv/uploadCsv.html'),
                locals           	 : {},
                animation          : true,
                parent             : angular.element(document.body),
                clickOutsideToClose: true,
            })
            this.$mdDialog.show(this.confirm).then((row) => {
                if (row) {
					const dataReturn = row.data
					if(dataReturn.length < 2) {
						this.loadItemCSV(dataReturn[0])
					}else {
						for(const i in dataReturn){
							this.loadItemCSV(dataReturn[i])
						}
					}
 					
                }
            })
        }
	}   

itemsListController.$inject = ['$rootScope','$scope','UserInfoConstant','$timeout','itemsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService', 'uiGridExporterService'];

