        
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> itemsUpdateController
* # As Controller --> itemsUpdate														
*/

export default class itemsUpdateController{

        constructor($scope,UserInfoConstant,$timeout,itemsServices,ngNotify,$state,$interval,uiGridConstants,$http,$filter,$stateParams,warehousesServices,locationsServices,providersServices,categoriesServices, $mdDialog,applicationsServices,$window){
            var vm = this;
            vm.$mdDialog = $mdDialog
            this.$scope = $scope
            vm.callLocations= callLocations
            vm.updateItem= updateItem
            vm.updateBodegas= updateBodegas
            vm.internationalType = internationalType
            vm.webItemCreate = webItemCreate
            $scope.locationsInput = true
			$scope.internationalPanel  = false
            $scope.updateLocationOption  = false
			this.$stateParams = $stateParams
            $scope.id = this.$stateParams.itemId
			console.log(' $stateParams ::: ', this.$stateParams)
            let params = {
                itemId: $stateParams.itemId
            }
			this.$state = $state

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })
            
            itemsServices.items.itemsList(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data;
              $scope.previousPrice  = $scope.item[0].netPrice
                if ($scope.item[0].type == 'Internacional'){
                    $scope.internationalPanel  = true
                }
    
                $scope.itemDetails  = { 
                name : $scope.item[0].itemName,
                netPurchaseValue : $scope.item[0].netPurchaseValue, 
                netPrice : $scope.item[0].netPrice,
                vatPrice : $scope.item[0].vatPrice,
                purchasePrice : $scope.item[0].purchasePrice, 
                mayorPrice : $scope.item[0].mayorPrice,   
                createdDate : $scope.item[0].createdDate, 
                competitionPrice : $scope.item[0].competitionPrice,
                maxDiscount : parseInt($scope.item[0].maxDiscount),
                observation: $scope.item[0].observation,
                brand: $scope.item[0].brand,
                brandCode: $scope.item[0].brandCode, 
                webType: $scope.item[0].webType,
                driveLink: $scope.item[0].driveLink,
                web: $scope.item[0].web,
                freeMarket: $scope.item[0].freeMarket,
                offerIs: $scope.item[0].offerIs,
                freeMarketCode: $scope.item[0].freeMarketCode,
                oil: $scope.item[0].oil,
                criticalStock: $scope.item[0].criticalStock,
                type: $scope.item[0].type,
                currency: $scope.item[0].currency,
                currencyValue: $scope.item[0].currencyValue,
                providers: [],
                applications:  [],
                categories:  [],
                locations: $scope.item[0].warehouses[0].locations,
                stock: $scope.item[0].warehouses[0].stock,
                references:   $scope.item[0].references,
                warehouses:$scope.item[0].warehouses

                }

                $scope.references=[]
                if($scope.item[0].references.length>0){
                    for(var i in $scope.item[0].references){
                        $scope.references.push({title:$scope.item[0].references[i],done:false})
                    }
                    
                }

                for(var i in $scope.item[0].providers){
                    $scope.itemDetails.providers.push({
                        'providerName': $scope.item[0].providers[i].providerName,
                        'providerId': $scope.item[0].providers[i].providerCode 
                    })
                }

                for(var i in $scope.item[0].applications){
                    $scope.itemDetails.applications.push({
                        'applicationName': $scope.item[0].applications[i].applicationName,
                        'applicationId': $scope.item[0].applications[i].applicationCode 
                    })
                }

                
                for(var i in $scope.item[0].categories){
                    $scope.itemDetails.categories.push({
                        'categoryName': $scope.item[0].categories[i].categoryName,
                        'categoryCode': $scope.item[0].categories[i].categoryCode 
                    })
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            providersServices.providers.getProviders().$promise.then((dataReturn) => {
              $scope.providers = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

            categoriesServices.categories.getCategories().$promise.then((dataReturn) => {
              $scope.categories = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

            applicationsServices.applications.getApplicationsList().$promise.then((dataReturn) => {
              $scope.applications = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

            //function to show userId
            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    let model = {
                        userId : $scope.userInfo.id
                    }
                    warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                        $scope.warehouses = dataReturn.data;
                        console.log('$scope.warehouses',$scope.warehouses)
                        },(err) => {
                        console.log('No se ha podido conectar con el servicio',err);
                    })
                }
            })
            
            function internationalType(value) {
              if (value == "Internacional") {
                $scope.internationalPanel  = true
              }else{
                $scope.internationalPanel  = false
              }
            }

            function callLocations(warehouseId) {

                let params = {
                    warehouseId : warehouseId
                }

                locationsServices.locations.getLocations(params).$promise.then((dataReturn) => {
                  $scope.locations = dataReturn.data;
                  $scope.locationsInput = false
                  $scope.locations.selected = []
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                })

                let params2 = {
                    itemId : this.$stateParams.itemId,
                    warehouseId : warehouseId
                }
                
                itemsServices.items.getLocations(params2).$promise.then((dataReturn) => {
                  $scope.itemDetails.stock = dataReturn.stock[0].stock
                  $scope.itemDetails.locations = dataReturn.data == null ? [] : dataReturn.data
                  $scope.updateLocationOption  = true

                        itemsServices.items.getCurrentInfoItem(params2).$promise.then((dataReturn) => {
                            $scope.currentInfo= dataReturn.data
                        
                        },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                })
                
            }

            function updateItem() {

                var tempRef=[]

                for(var t in $scope.references){
                  tempRef.push($scope.references[t].title)
                }

                if ($scope.itemDetails.type == "Nacional"){

                    $scope.itemDetails.currency = "Peso Chileno"
                    $scope.itemDetails.currencyValue = $scope.itemDetails.purchasePrice
                 }

                let model = {
                    itemId: this.$stateParams.itemId,
                    userId: $scope.userInfo.id,
                    itemDescription: $scope.itemDetails.name,
                    purchasePrice:  $scope.itemDetails.purchasePrice,
                    netPurchaseValue:  $scope.itemDetails.netPurchaseValue,
                    netPrice: $scope.itemDetails.netPrice,
                    mayorPrice: $scope.itemDetails.mayorPrice,
                    maxDiscount:  $scope.itemDetails.maxDiscount,
                    competitionPrice:  $scope.itemDetails.competitionPrice,
                    observation:  $scope.itemDetails.observation,
                    brand:  $scope.itemDetails.brand,
                    brandCode:  $scope.itemDetails.brandCode,
                    webType:  $scope.itemDetails.webType,
                    driveLink:  $scope.itemDetails.driveLink,
                    web:  $scope.itemDetails.web,
                    freeMarket:  $scope.itemDetails.freeMarket,
                    oil:  $scope.itemDetails.oil,
                    offerIs:  $scope.itemDetails.offerIs,
                    freeMarketCode:  $scope.itemDetails.freeMarketCode,
                    type:  $scope.itemDetails.type,
                    currency:  $scope.itemDetails.currency,
                    currencyValue:  $scope.itemDetails.currencyValue,
                    criticalStock:  $scope.itemDetails.criticalStock,
                    isKit:0,
                    providersIds:[],
                    applicationsIds:[],
                    references:tempRef,
                    categoriesIds:[]
                }

                // $scope.itemDetails.providersIds=[]
                for(var i in  $scope.itemDetails.providers){
                    model.providersIds.push($scope.itemDetails.providers[i].providerId)
                }

                // $scope.itemDetails.applicationsIds=[]
                for(var i in  $scope.itemDetails.applications){
                    model.applicationsIds.push($scope.itemDetails.applications[i].applicationId)
                }

                //   $scope.itemDetails.categoriesIds=[]
                if(Object.keys( $scope.itemDetails.categories).length>0){
                    for(var i in  $scope.itemDetails.categories){
                           if(typeof $scope.itemDetails.categories[i].categoryId ==='undefined')
                           model.categoriesIds.push($scope.itemDetails.categories[i].categoryCode)
                           else
                           model.categoriesIds.push($scope.itemDetails.categories[i].categoryId)
                      }
                }else{
                    ngNotify.set('Error al actualizar category','error')  
                    return false;
                }

                if (Object.entries(model.references).length === 0) {
                          ngNotify.set('Debe asociar referencias al ITEM ','error')
                        }else{ 
                              console.log('modelo a update',model)

                            itemsServices.items.itemUpdate(model).$promise.then((dataReturn) => {
                                $scope.msg = dataReturn.data;
                                const row = {
                                    itemId: dataReturn.data,
                                    itemDescription: ''
                                }
                                ngNotify.set('Se ha actualizado el Item correctamente','success')

                                itemsServices.items.getGeneralStockWeb(row).$promise.then((dataReturn) => {
                                    $scope.generalStock = dataReturn.data;
                                    $scope.totalStock = $scope.generalStock[0].generalStock

                                    if($scope.previousPrice != model.netPrice){

                                        console.log('Comparación location: ',$scope.previousPrice,model.netPrice)
                    
                                        let itemsValue = {
                                            documentId: null,
                                            itemId: model.itemId,
                                            itemDescription: model.itemDescription,
                                            location : null,
                                            previousStock: null,
                                            currentStock: null,
                                            price: $scope.previousPrice,
                                            quantity: null,
                                            originId: null,
                                            userId: $scope.userInfo.id,
                                            document: "------",
                                            type: "Modificación",
                                            generalStock: $scope.totalStock,
                                            currentprice: model.netPrice,
                                            currentLocation: null,
                                            destiny: undefined
                        
                                            }
                                         
                                            console.log('that',itemsValue);
                           
                                            itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                                                $scope.result2 = dataReturn.data;
                                                console.log('Historial del item actualizado correctamente');
                                            },(err) => {
                                                console.log('No se ha podido conectar con el servicio',err);
                                            })
                    
                                     }

                                    },(err) => {
                                        console.log('No se ha podido conectar con el servicio',err);
                                  })

                                if (model.webType == 'SI'){
                                      webItemCreate(row,model)
                                      console.log('tipo web',row,model);    
                                }else{ 
                                    // this.$state.go('app.itemsDetail', row)
                                    this.$state.go('app.items')
                                }
                                
                              },(err) => {
                                  ngNotify.set('Error al actualizar Item','error')
                                  // $scope.saveButton  = false
                              })


                        }
            }

            function webItemCreate(row,model) {

                if (model.observation == undefined) {

                    $scope.webDescription = model.itemDescription

                }else{

                    $scope.webDescription = model.observation
                }

                let webModel = {
                    sku: row.itemId,
                    name: model.itemDescription,
                    type: "simple",
                    regular_price: model.netPrice*1.19,
                    description: $scope.webDescription,
                    short_description:model.itemDescription,
                    manage_stock: true,
                    stock_quantity: 0

                }

                console.log('modelo',webModel);

                itemsServices.woocommerce.itemCreate(webModel).$promise.then((dataReturn) => {
                  $scope.result= dataReturn.data;
                  $scope.message= dataReturn.message;
                  console.log('mensaje',$scope.message); 
                  console.log('permalink',$scope.result.permalink);

                  let data = {
                                    itemId: row.itemId,
                                    web: $scope.result.permalink
                                }

                      itemsServices.items.permalinkAdd(data).$promise.then((dataReturn) => {
                          $scope.msg = dataReturn.data;
                          console.log('link creado',$scope.msg);
                          ngNotify.set('Repuesto creado en sitio web correctamente','info')
                          $window.open($scope.result.permalink, "_blank");
                          // $state.go('app.itemsDetail', row)
                          $state.go('app.items')

                          },(err) => {
                              console.log('Atualización error',err);
                              console.log('Error al crear item web',$scope.message);

                              ngNotify.set('API Web: ' + $scope.message,'info')
                              // $state.go('app.itemsDetail', row)
                              $state.go('app.items')
                          })

                  },(err) => {
                      console.log('Error para conectar con API Woocommerce',err);

                      ngNotify.set( 'Error API Web: ', {
                        sticky: true,
                        button: true,
                        type : 'error'
                    })
                      $state.go('app.items')
                  })
             
            }

            function updateBodegas() {
              console.log('$scope.itemDetails.stock : ',$scope.itemDetails.stock)
              let params = {
                  itemId : this.$stateParams.itemId,
                  userId: $scope.userInfo.id,
                  stock : $scope.itemDetails.stock == undefined ? 0 : $scope.itemDetails.stock,
                  warehouseId : $scope.itemDetails.warehouses[0].warehouseId,
                  locations : $scope.itemDetails.locations
              }

              itemsServices.items.itemWarehouseUpdate(params).$promise.then((dataReturn) => {
                  ngNotify.set('Parametros item/bodega actualizado exitosamente','success')

                  itemsServices.items.getCurrentInfoItem(params).$promise.then((dataReturn) => {
                    $scope.updatedItemInfo= dataReturn.data

                    if($scope.currentInfo[0].locationGroup != $scope.updatedItemInfo[0].locationGroup){

                        console.log('Comparación location: ',$scope.currentInfo[0].locationGroup,$scope.updatedItemInfo[0].locationGroup)
    
                        let itemsValue = {
                            documentId: null,
                            itemId: params.itemId,
                            itemDescription: $scope.updatedItemInfo[0].dsName,
                            location : $scope.currentInfo[0].locationGroup,
                            previousStock: $scope.currentInfo[0].stockCurrent,
                            currentStock: $scope.updatedItemInfo[0].stockCurrent,
                            price: $scope.currentInfo[0].price,
                            quantity: null,
                            originId: params.warehouseId,
                            userId: $scope.userInfo.id,
                            document: "------",
                            type: "Movimiento",
                            generalStock: $scope.updatedItemInfo[0].generalStock,
                            currentprice: $scope.updatedItemInfo[0].price,
                            currentLocation: $scope.updatedItemInfo[0].locationGroup,
                            destiny: undefined
        
                            }
                         
                            console.log('that',itemsValue);
           
                            itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                                $scope.result2 = dataReturn.data;
                                console.log('Historial del item actualizado correctamente');
                            },(err) => {
                                console.log('No se ha podido conectar con el servicio',err);
                            })
    
                        }

                      },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                      })

                  },(err) => {
                    console.log('No se ha podido conectar con el servicio',err);
                })

            }

            $scope.references = [];

            $scope.addReference = function() {
                console.log('entra a wea')
              $scope.references.push({'title': $scope.reference, 'done':false})
              $scope.reference = ''
            }

            $scope.deleteReference = function(index) {  
              $scope.references.splice(index, 1);
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        uploadIMG() {
            let id = this.$scope.id
            this.confirm = this.$mdDialog.confirm({
                controller: 'itemsImageUpController',
                controllerAs: 'itemsImage',
                template: require('./../view/upload-image/image-upload.html'),
                locals: { itemId: id},
                animation: true,
                parent: angular.element(document.body),
                clickOutsideToClose: true,
            })
            this.$mdDialog.show(this.confirm).then(() => {
				this.$state.go('app.itemsDetail', this.$stateParams)
            })
        }
 

        
    }   

itemsUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','itemsServices','ngNotify','$state','$interval','uiGridConstants','$http','$filter','$stateParams','warehousesServices','locationsServices','providersServices','categoriesServices','$mdDialog','applicationsServices','$window'];
