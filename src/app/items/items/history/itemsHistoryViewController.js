/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> itemsHistoryViewController
* # As Controller --> itemsHistoryView														
*/
  export default class itemsHistoryViewController {

    constructor($scope, $filter,$rootScope, $http,itemsServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService,$stateParams) {

    	var vm = this
	    vm.cancel = cancel
        vm.viewDocument = viewDocument
        vm.tableLoad= tableLoad
        vm.viewItem = viewItem
        $scope.itemId = $stateParams.idItem
        $scope.itemName = $stateParams.itemDescription


        let model = {
            itemId: $scope.itemId
        }

        tableLoad()

        i18nService.setCurrentLang('es');

    
        $scope.itemsGridCardex = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'hitorial_item_'+$scope.itemId+'.csv',
                columnDefs: [
                   
                    { 
                      name:'Id Doc.',
                      field: 'documentId',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<a class="badge bg-accent" ng-click="grid.appScope.itemsHistoryView.viewDocument(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>'
                    },
                    { 
                      name:'DOC.',
                      field: 'typeDocument',
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning">{{row.entity.typeDocument}}</p>' +
                                 '</div>'  
                    },
                    { 
                        name:'TIPO',
                        field: 'type',
                        width: '9%',
                        cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<p class="label bg-info">{{row.entity.type}}</p>' +
                                   '</div>'  
                      },
                     
                      { 
                        name:'FECHA',
                        field: 'fecha',
                        width: '8%',
                        cellTemplate:'<div class="ui-grid-cell-contents">'+
                                   '<p style="font-size: 11px;"><strong>  {{row.entity.fecha}} </strong></p>' +
                                   '</div>' 
                      },
  
                      { 
                        name:'Hora',
                        field: 'time',
                        width: '7%',
                        enableFiltering: false,
                        cellTemplate:'<div class="ui-grid-cell-contents">'+
                                   '<p style="font-size: 10px;">{{row.entity.time}}</p>' +
                                   '</div>' 
                      },
                    
                      { 
                        name:'PREVIO',
                        field: 'previousStock',
                        width: '7%' ,
                        enableFiltering: false,
                        cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                   '<a><span tooltip-placement="bottom" tooltip="Stock previo ajuste" class="label bg-secundary">{{row.entity.previousStock}}</span></a>' +
                                   '</div>'
                      },
                     { 
                      name:'Cant.',
                      field: 'outputs',
                      width: '6%' ,
                      enableFiltering: false,
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<a><span class="label bg-warning">{{row.entity.outputs}}</span></a>' +
                                 '</div>'
                    },
                    { 
                        name:'ACTUAL',
                        field: 'currentStock',
                        width: '7%' ,
                        enableFiltering: false,
                        cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                   '<a><span tooltip-placement="bottom" tooltip="Stock post ajuste" class="label bg-secundary">{{row.entity.currentStock}}</span></a>' +
                                   '</div>'
                      },
                    

                    

                    { 
                      name:'Origen',
                      field: 'origin',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase ">{{row.entity.origin}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Destino',
                      field: 'destination',
                      width: '7%',
                      enableFiltering: false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 11px;" class="uppercase ">{{row.entity.destination}}</p>' +
                                 '</div>'  
                    },

                    { 
                        name:'General',
                        field: 'generalstock',
                        width: '6%' ,
                        enableFiltering: false,
                        cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                   '<a><span tooltip-placement="bottom" tooltip="Stock general post Ajuste" class="label bg-secundary">{{row.entity.generalstock}}</span></a>' +
                                   '</div>'
                      },
                    
                    { 
                      name:'UBI',
                      field: 'location',
                      width: '7%',
                      enableFiltering: false,
                      cellTemplate:'<div class="ui-grid-cell-contents " style="text-align: center;" >'+
                                 '<a> <i class="mdi-magnify" title="{{ row.entity.previouslocation}} -> {{row.entity.currentLocation }}" style="font-size: 22px;"></i></a>' +
                                 '</div>' 
                    },
                    { 
                        name:'PRECIO',
                        field: 'price',
                        width: '8%',
                        enableFiltering: false,
                        cellTemplate:'<div class="ui-grid-cell-contents" style="text-align: center;" >'+
                                   '<a><strong><i class="mdi-magnify" style="font-size: 22px;" tooltip-placement="bottom" tooltip="${{row.entity.prevPrice | pesosChilenos}} >> ${{row.entity.currentPrice | pesosChilenos}}" ></i></strong></a>' +
                                   '</div>' 
                      },
                    { 
                        name:'user',
                        field: 'userName',
                        width: '7%',
                        cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<p class="badge bg-info"  style="font-size: 9px;">{{row.entity.userName}}</p>' +
                                   '</div>' 
                      }
                ]
        }

        function tableLoad(){

            itemsServices.items.historicalTableItems(model).$promise.then((dataReturn) => {
                $scope.itemsGridCardex.data = dataReturn.data;
                },(err) => {
                    console.log('No se ha podido conectar con el servicio',err);
                })



        }

        function viewItem(row, modal = false) {
            $scope.itemsData = {
                itemId:$scope.itemId,
                itemDescription:$scope.itemName 
            }
                const modalInstance  = $modal.open({
                    template: require('../view/items-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'itemsViewController',
                    controllerAs: 'itemsView',
                    size: 'lg'
                })
            }

        function viewDocument(row){
                $scope.invoiceData = row;

                if ($scope.invoiceData.typeDocument == "FAC") {

                    $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName
                    }
                    console.log('ver factura');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.typeDocument == "NN") { 

                    $scope.invoiceData = {
                        documentId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName 
                    }
                    console.log('ver documento nn');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/documents-nn/view/documents-nn-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'documentsNnViewController',
                            controllerAs: 'documentsNnView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.typeDocument == "NC") { 

                    $scope.creditNoteData = {
                        creditNoteId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName  
                    }
                    console.log('ver nota credito nn');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/credit-notes/view/credit-notes-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'creditNotesViewController',
                            controllerAs: 'creditNotesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.typeDocument == "BOL") { 

                    $scope.ballotsData = {
                        ballotId:$scope.invoiceData.documentId ,
                        clientName:$scope.invoiceData.clientName 
                    }
                    console.log('Boleta');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/ballots/view/ballots-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.typeDocument == "PRO") { 

                    $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId,
                        providerName:$scope.invoiceData.clientName  
                    }
                    console.log('factura proveedor');
                    var modalInstance  = $modal.open({
                        template: require('../../../purchases/invoices-provider/view/invoices-provider-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'invoicesProviderViewController',
                        controllerAs: 'invoicesProviderView',
                        size: 'lg'
                })

                }


                if ($scope.invoiceData.typeDocument == "AJU") { 

                    $scope.guideData = {
                        transferGuideId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName  
                    }
                    console.log('ajuste stock');
                    var modalInstance  = $modal.open({
                            template: require('../../../warehouse/stock-adjustment/view/stock-adjustment-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'stockAdjustmentViewController',
                            controllerAs: 'stockAdjustmentView',
                            size: 'lg'
                    })

                }


                if ($scope.invoiceData.typeDocument == "GUI") { 

                    $scope.guideData = {
                        transferGuideId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName  
                    }
                    console.log('guias');
                    var modalInstance  = $modal.open({
                            template: require('../../../warehouse/transfer-guides/view/guides-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'guidesViewController',
                        controllerAs: 'guidesView',
                            size: 'lg'
                    })

                }


                if ($scope.invoiceData.typeDocument == "DEV") { 

                    $scope.supplierReturnData = {
                        returnId:$scope.invoiceData.documentId,
                        providerName:$scope.invoiceData.clientName  
                    }
                    console.log('devolucion');
                    var modalInstance  = $modal.open({
                            template: require('../../../purchases/supplier-return/view/supplier-return-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'supplierReturnViewController',
                        controllerAs: 'supplierReturnView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.typeDocument == "ANU") { 

                    $scope.voidDocumentData = {
                        id:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName  
                    }
                    console.log('anulacion');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/void-documents/view/void-documents-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'voidDocumentsViewController',
                        controllerAs: 'voidDocumentsView',
                            size: 'lg'
                    })

                }

            }


	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}



    }

  }

  itemsHistoryViewController.$inject = ['$scope', '$filter','$rootScope', '$http','itemsServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService','$stateParams'];
