

import routing from './items.route';
import run from './items.run';

//items files
import itemsListController from './items/list/itemsListController';
import itemsViewController from './items/view/itemsViewController';
import itemsDetailController from './items/detail/itemsDetailController';
import itemsCreateController from './items/create/itemsCreateController';
import itemsUpdateController from './items/update/itemsUpdateController';
import itemsCloneController from './items/clone/itemsCloneController';
import itemsDeleteController from './items/delete/itemsDeleteController';
import itemsObservationsController from './items/observations/itemsObservationsController';
import itemsProviderController from './items/items-provider/itemsProviderController';
import itemsCardexController from './items/cardex/itemsCardexController';  
import itemsHistoryViewController from './items/history/itemsHistoryViewController';
import statusFailController from './items/status-fail/statusFailController'; 
import printItemController from './items/print/printItemController'; 
import wishlistItemsController from './items/wishlist/wishlistItemsController'; 
import itemsFreeMarketController from './items/free-market/itemsFreeMarketController';
import relatedItemsController from './items/related/relatedItemsController';
import itemsServices from './items/itemsServices';
import itemsImageUpController from './items/view/upload-image/itemsImageUpController';
import imageViewController from './items/imageView/imageViewController';
import gridDialogController from './items/dialog/gridDialogController';
import uploadCsvItemController from './items/list/items/uploadCsv/uploadCsvItemController';

//categories files
import categoriesListController from './categories/list/categoriesListController';
import categoriesViewController from './categories/view/categoriesViewController';
import categoriesCreateController from './categories/create/categoriesCreateController';
import categoriesUpdateController from './categories/update/categoriesUpdateController';
import categoriesDeleteController from './categories/delete/categoriesDeleteController';
import categoriesServices from './categories/categoriesServices';


//trucks files
import trucksListController from './trucks/list/trucksListController'; 
import trucksCreateController from './trucks/create/trucksCreateController';
import trucksUpdateController from './trucks/update/trucksUpdateController';
import trucksDeleteController from './trucks/delete/trucksDeleteController';
import trucksServices from './trucks/trucksServices'; 

//applications files
import applicationsListController from './applications/list/applicationsListController'; 
import applicationsCreateController from './applications/create/applicationsCreateController';
import applicationsUpdateController from './applications/update/applicationsUpdateController';
import applicationsServices from './applications/applicationsServices';

//catalogs files
import catalogsController from './catalogs/list/catalogsController';
import catalogsServices from './catalogs/catalogsServices';

export default angular.module('app.items', [itemsServices,categoriesServices,trucksServices,applicationsServices,catalogsServices])
  .config(routing)

  .controller('imageViewController', imageViewController)
  .controller('itemsImageUpController', itemsImageUpController)
  .controller('itemsListController', itemsListController)
  .controller('itemsViewController', itemsViewController)
  .controller('itemsDetailController', itemsDetailController)
  .controller('itemsCreateController', itemsCreateController)
  .controller('itemsUpdateController', itemsUpdateController) 
  .controller('itemsCloneController', itemsCloneController)
  .controller('itemsDeleteController', itemsDeleteController)
  .controller('gridDialogController', gridDialogController)
  .controller('printItemController', printItemController)
  .controller('categoriesListController', categoriesListController)
  .controller('categoriesViewController', categoriesViewController)
  .controller('categoriesCreateController', categoriesCreateController)
  .controller('categoriesUpdateController', categoriesUpdateController)
  .controller('categoriesDeleteController', categoriesDeleteController)
  .controller('itemsObservationsController', itemsObservationsController)
  .controller('itemsProviderController', itemsProviderController)
  .controller('itemsCardexController', itemsCardexController) 
  .controller('itemsHistoryViewController', itemsHistoryViewController)
  .controller('itemsFreeMarketController', itemsFreeMarketController) 
  .controller('relatedItemsController', relatedItemsController) 
  .controller('statusFailController', statusFailController)
  .controller('trucksListController', trucksListController)
  .controller('trucksCreateController', trucksCreateController) 
  .controller('trucksUpdateController', trucksUpdateController)
  .controller('trucksDeleteController', trucksDeleteController) 
  .controller('wishlistItemsController', wishlistItemsController) 
  .controller('applicationsListController', applicationsListController)
  .controller('applicationsCreateController', applicationsCreateController)
  .controller('applicationsUpdateController', applicationsUpdateController) 
  .controller('catalogsController', catalogsController)
  .controller('uploadCsvItemController', uploadCsvItemController)
  .run(run)
  .constant("items", [{
        "title": "Items",
        "icon":"mdi-engine",
        "subMenu":[
            {
            'title' : 'Repuestos',
            'url': 'app.items'
            },
            {
            'title' : 'Categorias',
            'url': 'app.categories'
            },
            {
            'title' : 'Camiones',
            'url': 'app.trucks'
            },
            {
            'title' : 'Aplicaciones',
            'url': 'app.apply'
            },
            {
            'title' : 'Catalogos',
            'url': 'app.catalogs'
            }
        
        ]
    }])
  .name;