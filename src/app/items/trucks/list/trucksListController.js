
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> trucksListController
* # As Controller --> trucksList														
*/

export default class trucksListController{

        constructor($scope,UserInfoConstant,$timeout,trucksServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.searchData=searchData;
            vm.trucksUpdate= trucksUpdate
            vm.trucksCreate = trucksCreate
            vm.trucksDelete = trucksDelete
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.trucksGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_camiones.csv',
                columnDefs: [
                    { 
                      name:'Id',
                      field: 'id',  
                      width: '5%' 
                    },
                    { 
                      name:'Patente',
                      field: 'patent',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style=" font-size: 12px;"><strong>{{row.entity.patent}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Marca',
                      field: 'brand',
                      width: '13%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-secundary" style="font-size: 11px;">{{row.entity.brand}}</p>' +
                                 '</div>'   
                    },
                    { 
                      name:'Modelo',
                      field: 'model',
                      width: '11%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.model}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Año',
                      field: 'year',
                      width: '5%' 
                    },
                    { 
                      name:'Chasis',
                      field: 'chassis',
                      width: '11%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase font-italic" style="font-size: 12px;">{{row.entity.chassis}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '16%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 10px;">{{row.entity.clientName}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Rut',
                      field: 'rut',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;"><strong>{{row.entity.rut}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'N° Motor',
                      field: 'motor_number',
                      width: '11%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">{{row.entity.motor_number}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Estado',
                      field: 'isActive',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="{{(row.entity.isActive == \'Activo\')?\'badge bg-warning\':\'badge bg-danger\'}}">{{row.entity.isActive}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name: '',
                      width: '4%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
         		trucksServices.trucks.getTrucks().$promise.then((dataReturn) => {
         		  $scope.trucksGrid.data = dataReturn.data;
                  console.log('$scope.trucksGrid',$scope.trucksGrid);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        function searchData() {
                  trucksServices.trucks.getTrucks().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.trucksGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }


        function trucksUpdate(row){
            $scope.trucksData = row;
                var modalInstance  = $modal.open({
                    template: require('../update/trucks-update.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'trucksUpdateController',
                    controllerAs: 'trucksUpdate',
                    size: 'lg'
              })
        }

        function trucksDelete(row){
            $scope.trucksData = row;
                var modalInstance  = $modal.open({
                    template: require('../delete/trucks-delete.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'trucksDeleteController',
                    controllerAs: 'trucksDelete'
              })
        }

        function trucksCreate(){
            console.log('entra')
                var modalInstance  = $modal.open({
                        template: require('../create/trucks-create.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'trucksCreateController',
                        controllerAs: 'trucksCreate',
                        size: 'lg'
                })
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

trucksListController.$inject = ['$scope','UserInfoConstant','$timeout','trucksServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

