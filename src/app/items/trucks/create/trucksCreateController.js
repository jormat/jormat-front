/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> trucksCreateController
* # As Controller --> trucksCreate														
*/


  export default class trucksCreateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,trucksServices,$location,UserInfoConstant,ngNotify,$state,clientsServices,warehousesServices) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.searchClient = searchClient;
        vm.trucksNew = trucksNew
        vm.setName = setName

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

    

        function setName(clients) {
                console.log('please',clients)
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedRut = clients.rut
            }


        function searchClient (value) {

            let model = {
                name: value
            }

            clientsServices.clients.getClientsDsByName(model).$promise.then((dataReturn) => {
                  $scope.clients = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            return false;
          }

	    let params = {
            checkId: $scope.checkId
          }


	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function trucksNew() {
            console.log('save bank check');
            let model = {
                            userId: $scope.userInfo.id,
                            clientId : $scope.clientId,
                            patent : $scope.patent,
                            brand : $scope.brand,
                            model : $scope.model,
                            chassis : $scope.chassis,
                            year : $scope.year,
                            motorNumber : $scope.motorNumber
                        }      

                        console.log('modelo',model)   

                        trucksServices.trucks.trucksCreate(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha ingresado correctamente el camión','success')
                            $scope.result = dataReturn.data

                            $state.reload('app.trucks')
                            $modalInstance.dismiss('chao');

                          },(err) => {
                              ngNotify.set('Error al registrar camion','error')
                          })


        }

		$scope.invoices = [];

        $scope.addInvoice = function(invoice) {
            
            $scope.invoices.push({'title': $scope.invoice, 'done':false})
            $scope.invoice = ''
            // $scope.saveButton  = false
        }

        $scope.deleteInvoice = function(index) {  
            $scope.invoices.splice(index, 1);
        }

    }

  }

  trucksCreateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','trucksServices','$location','UserInfoConstant','ngNotify','$state','clientsServices','warehousesServices'];
