/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> trucksUpdateController
* # As Controller --> trucksUpdate														
*/

  export default class trucksUpdateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,trucksServices,$location,UserInfoConstant,ngNotify,$state,clientsServices,warehousesServices) {

    	var vm = this;
	    vm.cancel = cancel
	    vm.searchClient = searchClient
        vm.trucksUpdate = trucksUpdate
        vm.setName = setName
        $scope.id = $scope.trucksData.id

        let params = {
            truckId: $scope.id 
          }

        trucksServices.trucks.getTruckDetails(params).$promise.then((dataReturn) => {
              $scope.truck = dataReturn.data;
              console.log('$scope.truck',$scope.truck );

              $scope.truckDetails  = { 
                    id : $scope.truck[0].id,
                    patent : $scope.truck[0].patent,
                    year : $scope.truck[0].year,
                    clientName : $scope.truck[0].clientName,
                    brand : $scope.truck[0].brand,
                    date : $scope.truck[0].date,
                    model : $scope.truck[0].model,
                    motor_number : $scope.truck[0].motor_number,
                    chassis : $scope.truck[0].chassis,
                    isActive : $scope.truck[0].isActive
                  }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })


        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        function setName(clients) {
                console.log('please',clients)
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedRut = clients.rut
            }


        function searchClient (value) {

            let model = {
                name: value
            }

            clientsServices.clients.getClientsDsByName(model).$promise.then((dataReturn) => {
                  $scope.clients = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            return false;
        }


	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function trucksUpdate() {
            console.log('update trucks');
            let model = {   
                            truckId : $scope.id,
                            userId: $scope.userInfo.id,
                            patent : $scope.truckDetails.patent,
                            brand : $scope.truckDetails.brand,
                            model : $scope.truckDetails.model,
                            chassis : $scope.truckDetails.chassis,
                            year : $scope.truckDetails.year,
                            motor_number : $scope.truckDetails.motor_number,
                            isActive : $scope.statusTrucks
                        }      

                        console.log('modelo',model)   

                        trucksServices.trucks.trucksUpdate(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha actualizado correctamente el camión','success')
                            $scope.result = dataReturn.data

                            $state.reload('app.trucks')
                            $modalInstance.dismiss('chao');

                          },(err) => {
                              ngNotify.set('Error al actualizar camion','error')
                          })


        }


    }

  }

  trucksUpdateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','trucksServices','$location','UserInfoConstant','ngNotify','$state','clientsServices','warehousesServices'];
