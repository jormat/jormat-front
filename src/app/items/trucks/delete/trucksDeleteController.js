/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> trucksDeleteController
* # As Controller --> trucksDelete														
*/


  export default class trucksDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,trucksServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.deleteTrucks = deleteTrucks;
	    $scope.id = $scope.trucksData.id
        $scope.clientName = $scope.trucksData.clientName
        console.log('$scope.trucksData',$scope.trucksData);
        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
        //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteTrucks() {
			let params = {
            truckId: $scope.id,
            userId: $scope.userInfo.id
          }

            trucksServices.trucks.trucksDisabled(params).$promise.then((dataReturn) => {
                ngNotify.set('Se ha inactivado el camión correctamente','success')
                $modalInstance.dismiss('chao');
                $state.reload('app.trucks')
              },(err) => {
                  ngNotify.set('Error al inactivar truck','error')
            })
		}
    }

  }

  trucksDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','trucksServices','$location','UserInfoConstant','ngNotify','$state'];
