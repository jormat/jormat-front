/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> trucksServices
* file for call at services for trucks
*/

class trucksServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var trucks = $resource(SERVICE_URL_CONSTANT.jormat + '/items/:route',{},{
      
      getTrucks: {
        method:'GET',
          params : {
            route: 'trucks'
            }
          },

       trucksCreate: {
        method:'POST',
        params : {
          route: 'trucks'
          }
        },
        
        getTruckDetails: {
          method:'GET',
          params : {
            route: 'trucks-details'
            }
          },

        trucksUpdate: {
          method:'PUT',
          params : {
            route: 'trucks-update'
            }
          },

        trucksDisabled: {
          method:'DELETE',
          params : {
            route: 'trucks'
            }
          }
    })

    return { trucks : trucks
             
           }
  }
}

  trucksServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.trucksServices', [])
  .service('trucksServices', trucksServices)
  .name;