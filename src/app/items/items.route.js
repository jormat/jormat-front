
routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default function routes($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode(true);
      //$urlRouterProvider.otherwise('/');

  $stateProvider
     .state('app.items', {
            abstract: false,
            url: '/items',
            template: require('./items/list/items-list.html'),
            controller: 'itemsListController',
            controllerAs:'itemsList'
      })
     .state('app.itemsCreate', {
            abstract: false,
            url: '/items/crear-item',
            template: require('./items/create/items-create.html'),
            controller: 'itemsCreateController',
            controllerAs:'itemsCreate'
        })
     .state('app.itemsUpdate', {
            abstract: false,
            url: '/items/modificar-item?itemId&itemDescription',
            template: require('./items/update/items-update.html'),
            controller: 'itemsUpdateController',
            controllerAs:'itemsUpdate'
        })
	.state('app.itemsDetail', {
		abstract: false,
		url: '/items/detalle-item?idItem?itemId&itemDescription',
		template: require('./items/detail/items-detail.html'),
		controller: 'itemsDetailController',
		controllerAs:'itemsDetail'
	})
     .state('app.itemsImage',{
        abstract: false,
            url: '/items/view/upload-image',
            template: require('./items/view/upload-image/image-upload.html'),
            controller: 'itemsImageUpController',
            controllerAs:'itemsImage'
     })
     .state('app.itemsClone', {
            abstract: false,
            url: '/items/clonar-item?idItem&parameters&type',
            template: require('./items/clone/items-clone.html'),
            controller: 'itemsCloneController',
            controllerAs:'itemsClone'
        })

      .state('app.itemsHistoryView',{
            abstract: false,
                url: '/items/historial?idItem&itemDescription',
                template: require('./items/history/items-history.html'),
                controller: 'itemsHistoryViewController',
                controllerAs:'itemsHistoryView'
         })


     .state('app.categories', {
            abstract: false,
            url: '/categorias',
            template: require('./categories/list/categories-list.html'),
            controller: 'categoriesListController',
            controllerAs:'categoriesList'
      })
     .state('app.categoriesCreate', {
            abstract: false,
            url: '/categorias/crear-categoria',
            template: require('./categories/create/categories-create.html'),
            controller: 'categoriesCreateController',
            controllerAs:'categoriesCreate'
        })
     .state('app.categoriesUpdate', {
            abstract: false,
            url: '/categorias/actualizar-categoria?idCategory&name&code',
            template: require('./categories/update/categories-update.html'),
            controller: 'categoriesUpdateController',
            controllerAs:'categoriesUpdate'
        })

      .state('app.printItems', {
              abstract: false,
              url: '/items/imprimir-item?idItem&itemName&warehouseName&references&location',
              template: require('./items/print/print-item.html'),
              controller: 'printItemController',
              controllerAs:'printItem'
          })

      .state('app.trucks', {
            abstract: false,
            url: '/ficha-camiones',
            template: require('./trucks/list/trucks-list.html'),
            controller: 'trucksListController',
            controllerAs:'trucksList'
      })

      .state('app.apply', {
            abstract: false,
            url: '/items/aplicaciones',
            template: require('./applications/list/applications-list.html'),
            controller: 'applicationsListController',
            controllerAs:'applicationsList'
      })

      .state('app.catalogs', {
            abstract: false,
            url: '/items/catalogos',
            template: require('./catalogs/list/catalogs.html'),
            controller: 'catalogsController',
            controllerAs:'catalogs'
      })
 
}
