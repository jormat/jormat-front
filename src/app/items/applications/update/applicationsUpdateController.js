/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> applicationsUpdateController
* # As Controller --> applicationsUpdate														
*/

  export default class applicationsUpdateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,applicationsServices,$location,UserInfoConstant,ngNotify,$state,clientsServices,warehousesServices) {

    	var vm = this;
	    vm.cancel = cancel
        vm.applicationsUpdate = applicationsUpdate
        $scope.applicationId = $scope.appData.applicationId

        let params = {
            applicationId: $scope.applicationId 
          }

        applicationsServices.applications.getApplicationDetails(params).$promise.then((dataReturn) => {

              $scope.application = dataReturn.data;
              console.log('$scope.application',$scope.application);

              $scope.applicationDetails  = { 
                    applicationName : $scope.application[0].applicationName,
                    applicationCode : $scope.application[0].applicationCode
                  }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })


        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function applicationsUpdate() {

            console.log('apps save');
            let model = {   
                            applicationId : $scope.applicationId,
                            applicationName: $scope.applicationDetails.applicationName,
                            applicationCode : $scope.applicationDetails.applicationCode
                        }      

                        console.log('modelo',model)   

                        applicationsServices.applications.updateApplications(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha actualizado correctamente la Aplicación','success')
                            $scope.result = dataReturn.data

                            $state.reload('app.apply')
                            $modalInstance.dismiss('chao');

                          },(err) => {
                              ngNotify.set('Error al actualizar Aplicación','error')
                          })


        }


    }

  }

  applicationsUpdateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','applicationsServices','$location','UserInfoConstant','ngNotify','$state','clientsServices','warehousesServices'];
