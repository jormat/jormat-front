
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> applicationsListController
* # As Controller --> applicationsList														
*/

export default class applicationsListController{

        constructor($scope,UserInfoConstant,$timeout,applicationsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.searchData=searchData;
            vm.applicationsCreate= applicationsCreate;
            vm.applicationsUpdate= applicationsUpdate
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.applicationsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_aplicaciones.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'applicationId',  
                      width: '15%' 
                    },
                    { 
                      name:'Codigo',
                      field: 'applicationCode',
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase"><strong>{{row.entity.applicationCode}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Nombre Aplicación',
                      field: 'applicationName',
                      width: '50%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.applicationName}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name: 'Acciones',
                      width: '10%', 
                      enableFiltering: false,
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
         		applicationsServices.applications.getApplicationsList().$promise.then((dataReturn) => {
         		  $scope.applicationsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        function searchData() {
                  applicationsServices.applications.getApplicationsList().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.applicationsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }

        function applicationsCreate(){
                var modalInstance  = $modal.open({
                        template: require('../create/applications-create.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'applicationsCreateController',
                        controllerAs: 'applicationsCreate'
                })
        }

        function applicationsUpdate(row){
                $scope.appData = row;
                var modalInstance  = $modal.open({
                        template: require('../update/applications-update.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'applicationsUpdateController',
                        controllerAs: 'applicationsUpdate',
                })
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

applicationsListController.$inject = ['$scope','UserInfoConstant','$timeout','applicationsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

