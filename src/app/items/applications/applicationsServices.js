/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> applicationsServices
* file for call at services for providers
*/

class applicationsServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var applications = $resource(SERVICE_URL_CONSTANT.jormat + '/categories/:route',{},{
      
      getApplicationsList: {
        method:'GET',
          params : {
            route: 'applications'
            }
          },

       applicationsCreate: {
        method:'POST',
        params : {
          route: 'applications'
          }
        },
        
        getApplicationDetails: {
          method:'GET',
          params : {
            route: 'applications-details'
            }
          },

        updateApplications: {
          method:'PUT',
          params : {
            route: 'applications'
            }
          }
    })

    return { applications : applications
             
           }
  }
}

  applicationsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.applicationsServices', [])
  .service('applicationsServices', applicationsServices)
  .name;