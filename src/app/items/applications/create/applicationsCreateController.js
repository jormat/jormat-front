/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> applicationsCreateController
* # As Controller --> applicationsCreate														
*/


  export default class applicationsCreateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,applicationsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.createApp = createApp;
	    

	    function createApp() {

                let model = {
                    descriptionName: $scope.descriptionName,
                    code: $scope.code
                }

                console.log('Modelo',model);

                applicationsServices.applications.applicationsCreate(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado la aplicación correctamente','success')
                    $modalInstance.dismiss('chao');
                    $state.reload('app.apply')
                  },(err) => {
                      ngNotify.set('Error al crear aplicación ','error')
                })
            }

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		


    }

  }

  applicationsCreateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','applicationsServices','$location','UserInfoConstant','ngNotify','$state'];
