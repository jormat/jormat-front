
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> catalogsController
* # As Controller --> catalogs                            
*/

export default class catalogsController{
	constructor($scope,UserInfoConstant,$timeout,catalogsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService,categoriesServices,applicationsServices,SERVICE_URL_CONSTANT,itemsServices,providersServices){
		this.$mdDialog = $mdDialog
		var vm = this;
		vm.searchData=searchData;
		vm.loadItems= loadItems;
		vm.viewItem = viewItem
		vm.itemLoadGrid = itemLoadGrid
		$scope.rows = 0
		vm.print= print
		vm.showFilters = showFilters
		vm.loadImg= loadImg
		vm.loadCatalogue= loadCatalogue
		vm.loadAplications= loadAplications
		vm.loadCategories = loadCategories
    vm.loadProviders = loadProviders
    vm.offerfunction = offerfunction
		$scope.arrow=true
		$scope.baseUrl = SERVICE_URL_CONSTANT
		$scope.picture= require("../../../../images/unknown.png");
		vm.searchOptions = {
			updateOn: 'default blur',
			debounce:{
				'default': 100,
				'blur': 0
			}
		}
		$scope.aplications = []
		$scope.categoriesSelected = []
    $scope.providersSelected = []
		$scope.catalogueItems = []
		$scope.is_disabled = true
		$scope.description = ''
		$scope.brand = ''

		loadGrid()

		$scope.UserInfoConstant = UserInfoConstant
		$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
			if (details !== undefined) {
				$scope.userInfo = details.user
				$scope.nameUser = $scope.userInfo.usLastName
			}
		})

		$scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-click="grid.appScope.catalogs.itemLoadGrid(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                   '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 12px;" ng-click="grid.appScope.catalogs.viewItem(row.entity)"> <strong> {{row.entity.itemId}} </strong></a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;">{{row.entity.references}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }  
                    },
                    { 
                      name:'Marca',
                      field: 'brand',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secondary uppercase text-muted" style="font-size: 11px;"><strong> {{row.entity.brand}}</strong></p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    }
                ]
            };

		$scope.$watch('description', function(val) {
			changeButtonDisabled()
		});

		$scope.$watch('brand', function(val) {
			changeButtonDisabled()
		});

		i18nService.setCurrentLang('es');

		$scope.dropdownMenu = require('./dropdownActionsMenu.html')

		function loadImg(id) {
			$scope.imageUrl = $scope.baseUrl.jormat + '/image/image/'+ id
			return $scope.imageUrl;

		}

		 function loadGrid(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }
		
		categoriesServices.categories.getCategories().$promise.then((dataReturn) => {
			$scope.categories = dataReturn.data;
		},(err) => {
			console.log('No se ha podido conectar con el servicio',err);
		})

		applicationsServices.applications.getApplicationsList().$promise.then((dataReturn) => {
			$scope.applications = dataReturn.data;
		},(err) => {
				console.log('No se ha podido conectar con el servicio',err);
		})

    providersServices.providers.getProviders().$promise.then((dataReturn) => {
      $scope.providers = dataReturn.data;
      },(err) => {
          console.log('No se ha podido conectar con el servicio',err);
    })

        function searchData() {
                  catalogsServices.mailServers.getMailServers().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.mailServersGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }

        function loadItems(){

         let params = {
				   filters : {
              description:$scope.description,
              brand:$scope.brand
            }
          }

      if ($scope.offer == 1) {
          params.filters.offer = 1
      }

			if ($scope.aplications.length) {
				params.filters.aplication = [...new Set($scope.aplications.map(a => a.applicationId))]
			}

			if ($scope.categoriesSelected.length) {
				params.filters.category = [...new Set($scope.categoriesSelected.map(c => c.categoryId))]
			}

      if ($scope.providersSelected.length) {
				params.filters.provider = [...new Set($scope.providersSelected.map(p => p.providerId))]
			}
			
      console.log('params',params);

			catalogsServices.catalogs.getCatalogue(params).$promise.then((dataReturn) => {
				$scope.catalogueItems = dataReturn.data;
				console.log('catalogo listado',$scope.catalogueItems.length);
				$scope.rows = $scope.catalogueItems.length
			},(err) => {
				console.log('No se ha podido conectar con el servicio',err);
			})
               
        }

        function print() {
              window.print();

        }


        function showFilters(value){
            
            if (value == "up") {$scope.arrow=false}
            if (value == "down") {$scope.arrow=true}
            
        }

     function itemLoadGrid(row){

     	let params = { 
			itemId: row.itemId,
			userId: $scope.userInfo.id
		}

		itemsServices.items.itemsList(params).$promise.then((dataReturn) => {
            $scope.item = dataReturn.data;
			console.log('items details',$scope.item);

			// var aplicationsValue = $scope.item[0].applicationP
			// var aplicationsText = aplicationsValue.slice(2)

			// var categoryValue = $scope.item[0].categoryP
			// var categoryText = categoryValue.slice(2, -9);

			loadImg(row.itemId)
                    $scope.catalogueItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    brand: row.brand,
                    web: $scope.item[0].web,
                    applications: $scope.item[0].applicationP,
                    categoryCode: $scope.item[0].categoryP
                 })

            console.log('catalogueItems',$scope.catalogueItems)
            $scope.rows = $scope.rows + 1

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })          
            }

        function loadCatalogue() {
              let model = {
                        userId: $scope.userInfo.id,
                        applicationsIds:[],
                        categoriesIds:[],
                        providersIds:[]
                    }

            }

        function   loadAplications(applicationsValue){
                 $scope.aplications= applicationsValue
				 changeButtonDisabled()
                 console.log('etso',$scope.aplications);

        }

        function   loadCategories(categoriesItem){
                 $scope.categoriesSelected= categoriesItem
				 changeButtonDisabled()
                 console.log('etso',$scope.categoriesSelected);

        }

        function loadProviders(providersItem){
          $scope.providersSelected= providersItem
          changeButtonDisabled()
          console.log('providers',$scope.providersSelected);

        }

         function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function offerfunction() {

              $scope.is_disabled = false
            }

		function changeButtonDisabled() {
			let validate = true

			if ($scope.aplications.length) {
				validate = false
			}

			if ($scope.categoriesSelected.length) {
				validate = false
			}

      if ($scope.providersSelected.length) {
				validate = false
			}

			if ($scope.description && $scope.description !== undefined) {
				validate = false
			}

			if ($scope.brand && $scope.brand !== undefined) {
				validate = false
			} 
  

			$scope.is_disabled = validate
		  }
    } 
}   

catalogsController.$inject = ['$scope','UserInfoConstant','$timeout','catalogsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService','categoriesServices','applicationsServices','SERVICE_URL_CONSTANT','itemsServices','providersServices'];

