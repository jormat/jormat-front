/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> catalogsServices
* file for call at mail Services
*/

class catalogsServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var catalogs = $resource(SERVICE_URL_CONSTANT.jormat + '/items/catalogs/:route',{},{
      
      getCatalogue: {
        method:'GET',
          params : {
            route: ''
            }
          },

    })

    return { catalogs : catalogs
             
           }
  }
}

  catalogsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.catalogsServices', [])
  .service('catalogsServices', catalogsServices)
  .name;