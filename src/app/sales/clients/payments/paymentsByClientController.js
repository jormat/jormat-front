/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentsByClientController
* # As Controller --> paymentsByClient														
*/


  export default class paymentsByClientController {

    constructor($scope, $filter,$rootScope, $http,clientsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.transformToInvoice = transformToInvoice
        $scope.clientId = $stateParams.clientId
        $scope.startDate = $stateParams.startDate
        $scope.endDate = $stateParams.endDate

        $scope.start = moment($scope.startDate).format("YY/MM/DD").toString()
        $scope.end = moment($scope.endDate).format("YY/MM/DD").toString()

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        let model = {
                clientId: $scope.clientId,
                startDate: $scope.start,
                endDate: $scope.end
            }

            console.log('#content',model)

            clientsServices.clients.clientAccount(model).$promise.then((dataReturn) => {
              $scope.dataClient= dataReturn.data
              $scope.total = dataReturn.total

              console.log('test',$scope.dataClient);

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

    	function cancel() {

		 window.close();
         
		}

        function transformToInvoice(row){

          $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.quotationId,discount:$scope.quotations[0].discount,type:'cotizacion'});

        }

        function print() {
              window.print();

        }


    }

  }

  paymentsByClientController.$inject = ['$scope', '$filter','$rootScope', '$http','clientsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
