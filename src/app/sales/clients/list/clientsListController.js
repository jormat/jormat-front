
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> clientsListController
* # As Controller --> clientsList														
*/

export default class clientsListController{

        constructor($scope,UserInfoConstant,$timeout,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this
            vm.searchData=searchData
            vm.deleteClient= deleteClient
            vm.viewClient= viewClient
            vm.clientHistoryView=clientHistoryView
            vm.updateClient = updateClient
            vm.accountStatusView = accountStatusView
            vm.showPayments = showPayments
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')

            $scope.clientsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'listado_clientes.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'clientId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-accent " style="font-size: 10px;"><strong>{{row.entity.clientId}}</strong></p>' +
                                 '</div>' 
                      
                    },
                    { 
                      name:'Nombre Cliente',
                      field: 'fullName',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;"><strong>{{row.entity.fullName}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Rut',
                      field: 'rut',
                      width: '10%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 12px;"><strong>{{row.entity.rut}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Telefono', 
                      field: 'mobile', 
                      width: '10%'
                    },
                    
                    { 
                      name: 'Email', 
                      field: 'email',  
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.email}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name: 'Bloqueo', 
                      field: 'isLocked',
                      // cellFilter: 'date', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="{{(row.entity.isLocked == \'Activo\')?\'badge bg-warning\':\'badge bg-danger\'}}">{{row.entity.isLocked}}</p>' +
                                 '</div>'
                      // cellTemplate:'<div class="ui-grid-cell-contents ">'+
                      //            '<p class="uppercase">{{row.entity.isLocked}}</p>' +

                      //            '</div>',
                      // cellClass: function(grid, row, col, rowRenderIndex, colRenderIndex){
                      //       if (row.entity.isLocked === 'Activo'){
                      //           return 'green-text-table';
                      //       }
                      //       if  (row.entity.isLocked === 'Bloqueado'){
                      //           return 'red-text-table';
                      //       }
                      //   }

                    },
                    { 
                      name: 'Categoria', 
                      field: 'category',
                      // cellFilter: 'date', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="{{(row.entity.category == \'SI\')?\'label bg-accent\':\'label bg-secondary\'}}">{{row.entity.category}}</p>' +
                                 '</div>'
                     
                    

                    },
                    { 
                      name: '',
                      width: '10%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
     	clientsServices.clients.getClients().$promise.then((dataReturn) => {
     		  $scope.clientsGrid.data = dataReturn.data;
          },(err) => {
              console.log('No se ha podido conectar con el servicio',err);
          })

        function searchData() {
          clientsServices.clients.getClients().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.clientsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
        }

        function deleteClient(row){
            console.log('estoy aki',row)
        	$scope.clientData = row;
            var modalInstance  = $modal.open({
                template: require('../delete/clients-delete.html'),
                animation: true,
                scope: $scope,
                controller: 'clientsDeleteController',
                controllerAs: 'clientsDelete'
            })
        }

        function viewClient(row){
            $scope.clientData = row;
            var modalInstance  = $modal.open({
                    template: require('../view/clients-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'clientsViewController',
                    controllerAs: 'clientsView',
            })
        }

        function clientHistoryView(row){
            $scope.clientData = row;
            var modalInstance  = $modal.open({
                    template: require('../history/client-history.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'clientHistoryController',
                    controllerAs: 'clientHistory',
                    size: 'lg'
            })
        }


        function showPayments(row){
            $scope.clientData = row;
            var modalInstance  = $modal.open({
                    template: require('../history-payments/client-payments.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'clientPaymentsController',
                    controllerAs: 'clientPayments',
                    size: 'lg'
            })
        }

        function updateClient(row){
            $state.go("app.clientsUpdate", { idClient: row.clientId});
        }

        function accountStatusView(row){
            $state.go("app.accountStatus", { idClient: row.clientId});
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

clientsListController.$inject = ['$scope','UserInfoConstant','$timeout','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

