/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> clientHistoryController
* # As Controller --> clientHistory														
*/
  export default class clientHistoryController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,clientsServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
	    vm.cancel = cancel
	    vm.edit = edit
        vm.viewDocument = viewDocument
	    $scope.clientId = $scope.clientData.clientId
	    $scope.fullName = $scope.clientData.fullName
        $scope.rut = $scope.clientData.rut

        console.log( 'data selecionada',$scope.clientData)

	    let params = {
            clientId: $scope.clientId
          }

        i18nService.setCurrentLang('es');

        $scope.clientHistoryGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'historial_cliente_'+ $scope.fullName +'.csv',
                enableGridMenu: true,
                exporterPdfHeader: { 
                    text: "Historial cliente Id:"+ $scope.clientId +" / "+ $scope.fullName +" Rut: " + $scope.rut, 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [0,20,0,0] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },
                columnDefs: [
                   { 
                      name: 'Tipo Documento', 
                      field: 'type', 
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-accent">{{row.entity.type}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'N° Documento',
                      field: 'documentId',  
                      enableFiltering: true,
                      width: '15%' ,
                      filterCellFiltered: true, 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase" style="font-size: 15px;" ng-click="grid.appScope.clientHistory.viewDocument(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name: 'total', 
                      field: 'total',  
                      width: '18%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 15px;" >${{row.entity.total}}</p>' +
                                 '</div>'  
                    },
                    
                    
                    { 
                      name: 'Origen', 
                      field: 'origin',  
                      width: '15%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.origin}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Estado Documento',
                      field: 'statusName',
                      width: '18%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="badge bg-warning">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date', 
                      width: '16%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    }
                    
                ]
             };

        clientsServices.clients.getClientHistory(params).$promise.then((dataReturn) => {
              $scope.clientHistoryGrid.data = dataReturn.data;

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

        function viewDocument(row){
                $scope.invoiceData = row;

                if ($scope.invoiceData.type == "Factura") {

                    $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId,
                        clientName:$scope.fullName
                    }
                    console.log('ver factura');
                    var modalInstance  = $modal.open({
                            template: require('../../clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.type == "Documento NN") { 

                    $scope.invoiceData = {
                        documentId:$scope.invoiceData.documentId,
                        clientName:$scope.fullName
                    }
                    console.log('ver documento nn');
                    var modalInstance  = $modal.open({
                            template: require('../../documents-nn/view/documents-nn-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'documentsNnViewController',
                            controllerAs: 'documentsNnView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.type == "Nota Credito") { 

                    $scope.creditNoteData = {
                        creditNoteId:$scope.invoiceData.documentId,
                        clientName:$scope.fullName 
                    }
                    console.log('ver nota credito nn');
                    var modalInstance  = $modal.open({
                            template: require('../../credit-notes/view/credit-notes-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'creditNotesViewController',
                            controllerAs: 'creditNotesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.type == "Boleta") { 

                    $scope.ballotsData = {
                        ballotId:$scope.invoiceData.documentId,
                        clientName:$scope.fullName
                    }
                    console.log('Boleta');
                    var modalInstance  = $modal.open({
                            template: require('../../ballots/view/ballots-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }

            }

		function edit() {
			$state.go("app.providersUpdate");
		}


    }

  }

  clientHistoryController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','clientsServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
