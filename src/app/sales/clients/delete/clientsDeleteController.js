/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> clientsDeleteController
* # As Controller --> clientsDelete														
*/

  export default class clientsDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,clientsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.deleteClient = deleteClient;
	    $scope.clientId = $scope.clientData.clientId
	    $scope.rut = $scope.clientData.rut
	    $scope.clientName = $scope.clientData.fullName

      //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
        //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteClient() {
			let model = {
                    clientId : $scope.clientId,
                    userId : $scope.userInfo.id
                }

                clientsServices.clients.disabledClient(model).$promise.then((dataReturn) => {

                    ngNotify.set('Cliente dado de baja exitosamente','success');
					         $modalInstance.dismiss('chao');
                   $state.reload("app.clients");
                  },(err) => {
                      ngNotify.set('Error al eliminar Cliente','error')
                  })
		}


    }

  }

  clientsDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','clientsServices','$location','UserInfoConstant','ngNotify','$state'];
