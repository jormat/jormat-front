
/**
* @name APP jormat
* @autor jaime.salazar
* @description
* # name controller --> clientsUpdateController
* # As Controller --> clientsUpdate														
*/

export default class clientsUpdateController{

        constructor($scope,UserInfoConstant,$timeout,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams){
            var vm = this;
            vm.updateClient = updateClient
            vm.updateLocked = updateLocked
            vm.activeCredit = activeCredit
            vm.changeCategory = changeCategory

            let params = {
                clientId: $stateParams.idClient
            }
            
            clientsServices.clients.getClientsDetails(params).$promise.then((dataReturn) => {
              $scope.client = dataReturn.data;

              $scope.clientDetails  = { 
                name : $scope.client[0].fullName,
                email : $scope.client[0].email,
                rut : $scope.client[0].rut,
                cityName : $scope.client[0].cityName,
                address : $scope.client[0].address,
                address2 : $scope.client[0].address2,
                phone : $scope.client[0].phone,
                mobile : $scope.client[0].mobile,
                web : $scope.client[0].web,
                commercialBusiness : $scope.client[0].commercialBusiness,
                customerManager : $scope.client[0].customerManager,
                isCredit : $scope.client[0].isCredit,
                category : $scope.client[0].category,
                categoryValue : $scope.client[0].categoryValue,
                observation : $scope.client[0].blockingReason,
                locked: $scope.client[0].isLocked
              }

              $scope.category = $scope.client[0].categoryValue
              console.log('categoria cliente',$scope.category)

                if ($scope.client[0].isLocked == "Activo") {
                        $scope.lockedButton = true
                        $scope.unlockingButton = false
                    }
                if ($scope.client[0].isLocked == "Bloqueado"){
                        $scope.unlockingButton = true
                        $scope.lockedButton = false
                    }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            function activeCredit(value) {

                let model = {

                  clientId: $stateParams.idClient,
                  value: value
                }

                clientsServices.clients.activeCredit(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha actualizado el crédito del cliente de manera correcta','success')
                    $state.go('app.clients')
                  },(err) => {
                      ngNotify.set('Error al actualizar crédito','error')
                  })
            }

            function updateLocked(value) {

                let model = {

                  clientId: $stateParams.idClient,
                  blockingReason: $scope.clientDetails.observation,
                  isLocked: value
                }

                clientsServices.clients.lockedClient(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha bloqueado/desbloqueado el cliente correctamente','success')
                    $state.go('app.clients')
                  },(err) => {
                      ngNotify.set('Error al actualizar cliente','error')
                  })
            }

            function changeCategory(value) {
              if(value == "SI"){
                $scope.category = 1
                console.log('categoria cliente',$scope.category)
                $scope.clientDetails.commercialBusiness = ""
              } else{
                $scope.category = 0
                console.log('categoria cliente',$scope.category)
                $scope.clientDetails.commercialBusiness = $scope.client[0].commercialBusiness
              }
          }
          
         	function updateClient() {

                let model = {

                  clientId: $stateParams.idClient,
                  clientName: $scope.clientDetails.name,
                  phone: $scope.clientDetails.phone,
                  mobile: $scope.clientDetails.mobile,
                  rut: $scope.clientDetails.rut,
                  email: $scope.clientDetails.email,
                  address: $scope.clientDetails.address,
                  address2: $scope.clientDetails.address2,
                  cityName: $scope.clientDetails.cityName,
                  web: $scope.clientDetails.web,
                  customerManager: $scope.clientDetails.customerManager,
                  commercialBusiness: $scope.clientDetails.commercialBusiness,
                  category: $scope.category,
                  observation : $scope.clientDetails.observation
                }

                clientsServices.clients.updateClient(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha actualizado el cliente correctamente','success')
                    $state.go('app.clients')
                  },(err) => {
                      ngNotify.set('Error al actualizar cliente','error')
                  })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

clientsUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams'];

