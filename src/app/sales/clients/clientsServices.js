/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> clientsServices
* file for call at services for clients
*/

class clientsServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var clients = $resource(SERVICE_URL_CONSTANT.jormat + '/clients/:route',{},{
      
        getClients: {
            method:'GET',
            params : {
               route: 'list'
            }
        },

        getClientsByName: {
            method:'GET',
            params : {
               route: 'name'
            }
        }, 

        getClientsByNameCategory: {
            method:'GET',
            params : {
               route: 'category'
            }
        }, 

        getClientsDsByName: {
            method:'GET',
            params : {
               route: 'dsname'
            }
        },

        getClientsRut: {
            method:'GET',
            params : {
               route: 'rut'
            }
        },

        getClientHistory: {
            method:'GET',
            params : {
               route: 'history'
            }
        },
        getClientsDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createClient: {
            method:'POST',
            params : {
                route: ''
            }
        },
        updateClient: {
            method:'PUT',
            params : {
                route: ''
            }
        },
        lockedClient: {
            method:'PUT',
            params : {
                route: 'locked'
            }
        },
        disabledClient: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        activeCredit: {
            method:'PUT',
            params : {
                route: 'credit'
            }
        },
        clientsPayments: {
            method:'GET',
            params : {
                route: 'payments'
            }
        },

        getClientPayments: {
            method:'GET',
            params : {
                route: 'client-payments'
            }
        },
        clientAccount: {
            method:'GET',
            params : {
               route: 'account'
            }
        },
        getInvoicesNotificationClient: {
            method:'GET',
            params : {
               route: 'invoices-notification'
            }
        },
        getNotesNotificationClient: {
            method:'GET',
            params : {
               route: 'notes-notification'
            }
        },
        getChecksNotificationClient: {
            method:'GET',
            params : {
               route: 'checks-notification'
            }
        },
        updateAddress: {
            method:'PUT',
            params : {
                route: 'address'
            }
        }
    })

    return { clients : clients
             
           }
  }
}

  clientsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.clientsServices', [])
  .service('clientsServices', clientsServices)
  .name;