
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> clientsCreateController
* # As Controller --> clientsCreate														
*/

export default class clientsCreateController{

        constructor($scope,UserInfoConstant,$timeout,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter){
            var vm = this
            vm.createClient = createClient
            vm.removeItems = removeItems
            vm.checkRut = checkRut
            $scope.rutValidate = true

            function checkRut(rut) {
                // Despejar Puntos
                var valor = rut.replace('.','');
                // Despejar Guión
                valor = valor.replace('-','');
                
                // Aislar cuerpo y Dígito Verificador
                $scope.cuerpo = valor.slice(0,-1);
                $scope.dv = valor.slice(-1).toUpperCase();
                
                // Formatear RUN
                rut = $scope.cuerpo + '-'+ $scope.dv
                
                // Si no cumple con el mínimo ej. (n.nnn.nnn)
                if($scope.cuerpo.length < 7) { 
                    // rut.setCustomValidity("RUT Incompleto");
                    $scope.textValidation = "Incompleto";
                    $scope.rutValidate = true;
                    console.log("rut Incompleto");  
                    return false;
                }
                
                // Calcular Dígito Verificador
                $scope.suma = 0;
                $scope.multiplo = 2;
                
                // Para cada dígito del $scope.cuerpo

                for($scope.i=1;$scope.i<=$scope.cuerpo.length;$scope.i++) {
                
                    // Obtener su Producto con el Múltiplo Correspondiente
                    $scope.index = $scope.multiplo * valor.charAt($scope.cuerpo.length -$scope.i);
                    
                    // Sumar al Contador General
                    $scope.suma = $scope.suma + $scope.index;
                    
                    // Consolidar Múltiplo dentro del rango [2,7]
                    if($scope.multiplo < 7) { $scope.multiplo = $scope.multiplo + 1; } else { $scope.multiplo = 2; }
              
                }
                
                // Calcular Dígito Verificador en base al Módulo 11
                $scope.dvEsperado = 11 - ($scope.suma % 11);
                
                // Casos Especiales (0 y K)
                $scope.dv = ($scope.dv == 'K')?10:$scope.dv;
                $scope.dv = ($scope.dv == 0)?11:$scope.dv;
                
                // Validar que el $scope.cuerpo coincide con su Dígito Verificador
                if($scope.dvEsperado != $scope.dv) { 
                    // rut.setCustomValidity("RUT Inválido");
                    console.log("RUT Inválido"); 
                    $scope.textValidation = "RUT Inválido";
                    $scope.rutValidate = true;
                    return false; 
                }
                
                // Si todo sale bien, eliminar errores (decretar que es válido)
                // rut.setCustomValidity('');
                $scope.textValidation = "RUT Válido";
                console.log("RUT valido");
                $scope.rutValidate = false
            }

            function removeItems(index) {

                $scope.invoice[0].itemsInvoices.splice(index, 1);
            }
            
          
         	function createClient() {

                $scope.rutValidate = true

                let model = {
                    fullName: $scope.fullName,
                    rut: $scope.rut,
                    phone: $scope.phone,
                    mobile: $scope.mobile,
                    email: $scope.email,
                    address: $scope.address,
                    address2: $scope.address2,
                    cityName: $scope.city,
                    web: $scope.address,
                    region: $scope.region,
                    credit: $scope.credit,
                    commercialBusiness: $scope.bussiness,
                    customerManager: $scope.customerManager
                }

                console.log("modelo",model);

                clientsServices.clients.getClientsRut(model).$promise.then((dataReturn) => {
                    $scope.resultRut = dataReturn.data;
                    console.log('largo',$scope.resultRut.length)

                    if ($scope.resultRut.length == 0) {

                        clientsServices.clients.createClient(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha creado el cliente correctamente','success')
                            $state.go('app.clients')
                          },(err) => {
                              ngNotify.set('Error al crear cliente','error')
                              $scope.rutValidate = false
                        })

                    }else{

                        ngNotify.set('Ya existe cliente con este R.U.T','error')
                        $scope.rutValidate = false
            
                    }

                  },(err) => {
                      ngNotify.set('Error al consultar rut cliente','error')
                      $scope.rutValidate = false
                })

                
            }


            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

clientsCreateController.$inject = ['$scope','UserInfoConstant','$timeout','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter'];

