/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> accountStatusController
* # As Controller --> accountStatus												
*/
  export default class accountStatusController {

    constructor($scope, $filter,$rootScope, $http,clientsServices,$stateParams,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
        vm.loadDocuments = loadDocuments
        vm.print = print
        vm.viewDocument = viewDocument
        vm.loadPDF = loadPDF 
        vm.setPayments = setPayments
        vm.printDetailsWindows = printDetailsWindows
        $scope.clientId = $stateParams.idClient

        let params = {
                clientId: $stateParams.idClient
            }
            
            clientsServices.clients.getClientsDetails(params).$promise.then((dataReturn) => {
              $scope.client = dataReturn.data;

              $scope.clientDetails  = { 
                name : $scope.client[0].fullName,
                email : $scope.client[0].email,
                rut : $scope.client[0].rut,
                cityName : $scope.client[0].cityName,
                address : $scope.client[0].address,
                phone : $scope.client[0].phone,
                mobile : $scope.client[0].mobile,
                web : $scope.client[0].web,
                commercialBusiness : $scope.client[0].commercialBusiness,
                customerManager : $scope.client[0].customerManager,
                observation : $scope.client[0].blockingReason,
                locked: $scope.client[0].isLocked
              }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

	   
        i18nService.setCurrentLang('es');

        $scope.invoicesGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'Cuenta_cliente_'+ $scope.fullName +'.csv',
                enableGridMenu: true,
                showColumnFooter: true,
                exporterPdfHeader: { 
                    text: "Cuenta cliente Id:"+ $scope.clientId +" / "+ $scope.fullName +" Rut: " + $scope.rut, 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [0,20,0,0] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },
                columnDefs: [
                   { 
                      name: 'Tipo Documento', 
                      field: 'type', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-danger">{{row.entity.type}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'N° Documento',
                      field: 'documentId',  
                      enableFiltering: true,
                      width: '13%' ,
                      filterCellFiltered: true, 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase" style="font-size: 15px;" ng-click="grid.appScope.accountStatus.viewDocument(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin',  
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.origin}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Estado Documento',
                      field: 'statusName',
                      width: '13%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="badge bg-warning">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.dateFilter | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'total', 
                      field: 'total',  
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 15px;" >${{row.entity.total}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name: 'Abono', 
                      field: 'pay',  
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase" target="_blank" style="font-size: 15px;" ng-click="grid.appScope.accountStatus.setPayments(row.entity)">{{row.entity.pay}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name: 'Pendiente', 
                      field: 'pending',  
                      width: '10%',
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 15px;" >${{row.entity.pending}}</p>' +
                                 '</div>'  
                    }
                    
                ]
             };

        function loadPDF(){
                var doc = new jsPDF();
                doc.autoTable({html:'#content'});

        }

        function printDetailsWindows(){ 

            var url = $state.href("app.paymentsByClient",{ 
                clientId: $stateParams.idClient,
                startDate:moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate:moment($scope.endDate).format("YYYY-MM-DD").toString()
            });

            window.open(url,'_blank',"width=600,height=700");
        }
    

        function loadDocuments(){

          $scope.start = moment($scope.startDate).format("YY/MM/DD").toString()
          $scope.end = moment($scope.endDate).format("YY/MM/DD").toString()

          /*if( $scope.start == $scope.end ){

            $scope.start = ''
            $scope.end = ''

          }*/

            let model = {
                clientId: $scope.clientId,
                startDate: $scope.start,
                endDate: $scope.end
            }

            clientsServices.clients.clientAccount(model).$promise.then((dataReturn) => {
              $scope.invoicesGrid.data = dataReturn.data

              if ($scope.invoicesGrid.data.length == 0){
                      $scope.messageInvoices = true
                      ngNotify.set( 'No existes documentos emitidos entre '+ model.startDate +' y '+ model.endDate , {
                      sticky: true,
                      button: true,
                      type : 'warn'
                  })
                  }else{
                      $scope.messageInvoices = false
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

        function viewDocument(row){

            $scope.invoiceData = row;

                if ($scope.invoiceData.type == "Factura") {

                    $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId
                    }
                    console.log('ver factura');
                    var modalInstance  = $modal.open({
                            template: require('../../clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.type == "Nota Credito") { 

                    $scope.creditNoteData = {
                        creditNoteId:$scope.invoiceData.documentId 
                    }
                    console.log('ver nota credito nn');
                    var modalInstance  = $modal.open({
                            template: require('../../credit-notes/view/credit-notes-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'creditNotesViewController',
                            controllerAs: 'creditNotesView',
                            size: 'lg'
                    })

                }


        }

	    function print() {
			window.print();
		}


        function setPayments(row){

            if (row.type == 'Nota Credito') {

                var url = $state.href("app.paymentNotesView", { 
                    noteId: row.documentId,
                    idClient: $scope.clientId,
                    clientName: $scope.client[0].fullName,
                    total: row.total,
                    date: row.date
                });

                window.open(url,'_blank');

            }else{

                var url = $state.href("app.paymentsView", { 
                    idInvoice: row.documentId,
                    idClient: $scope.clientId,
                    clientName: $scope.client[0].fullName,
                    total: row.total,
                    date: row.date
                });

                window.open(url,'_blank');
            }
            
        }



    }

  }

  accountStatusController.$inject = ['$scope', '$filter','$rootScope', '$http','clientsServices','$stateParams','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
