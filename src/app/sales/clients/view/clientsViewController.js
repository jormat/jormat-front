/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> clientsViewController
* # As Controller --> clientsView														
*/


  export default class clientsViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,clientsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.edit = edit;
	    $scope.clientId = $scope.clientData.clientId
	    $scope.clientName = $scope.clientData.clientName

	    let params = {
            clientId: $scope.clientId
          }

        clientsServices.clients.getClientsDetails(params).$promise.then((dataReturn) => {
         		  $scope.client = dataReturn.data
         		  if($scope.client[0].active == 1){
         		  	 	$scope.statusName= "Activo"
         		  }else{
         		    	$scope.statusName= "Inactivo"
         		  }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function edit() {
			$state.go("app.providersUpdate");
		}

		


    }

  }

  clientsViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','clientsServices','$location','UserInfoConstant','ngNotify','$state'];
