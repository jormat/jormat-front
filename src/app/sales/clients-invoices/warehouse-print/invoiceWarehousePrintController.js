/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> invoiceWarehousePrintController
* # As Controller --> invoiceWarehousePrint														
*/


  export default class invoiceWarehousePrintController {

    constructor($scope, $filter,$rootScope, $http,clientsInvoicesServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.invoiceId = $stateParams.idInvoice

        let params = {
                invoiceId: $scope.invoiceId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        clientsInvoicesServices.invoices.getInvoicesDetails(params).$promise.then((dataReturn) => {
              $scope.invoice = dataReturn.data
              if($scope.invoice[0].packOff == 'Si'){
				$scope.deliveryType =  "Despacho"
    			}else{
    				$scope.deliveryType =  "Mesón"
    		    }   

                let model = {
                        invoiceId: $scope.invoiceId,
                        originId: $scope.invoice[0].originId
                    }

                   clientsInvoicesServices.invoices.getInvoicesItems(model).$promise.then((dataReturn) => {
                    $scope.invoiceItems = dataReturn.data
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio get Invoices Details',err);
                  })
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get invoice Details',err);
              })

              

        	function cancel() {

    		  // $state.go("app.clientsInvoices")
               window.close();
    		}


             function print() {
              window.print();

        }


    }

  }

  invoiceWarehousePrintController.$inject = ['$scope', '$filter','$rootScope', '$http','clientsInvoicesServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
