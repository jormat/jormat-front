/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> invoicesByClientController
* # As Controller --> invoicesByClient														
*/


  export default class invoicesByClientController {

    constructor($scope, $filter,$rootScope, $http,clientsInvoicesServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.transformToInvoice = transformToInvoice
        $scope.clientId = $stateParams.clientId
        $scope.startDate = $stateParams.startDate
        $scope.endDate = $stateParams.endDate

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        let model = {
                clientId: $scope.clientId,
                startDate: $scope.startDate,
                endDate: $scope.endDate
            }

            console.log('#content',model)

            clientsInvoicesServices.invoices.getInvoicesByClient(model).$promise.then((dataReturn) => {
              $scope.dataClient= dataReturn.data
              $scope.total= dataReturn.total
              console.log('this',$scope.total);

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

    	function cancel() {

		 window.close();
         
		}

        function transformToInvoice(row){

          $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.quotationId,discount:$scope.quotations[0].discount,type:'cotizacion'});

        }

        function print() {
              window.print();

        }


    }

  }

  invoicesByClientController.$inject = ['$scope', '$filter','$rootScope', '$http','clientsInvoicesServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
