
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> clientsInvoicesUpdateController
* # As Controller --> clientsInvoicesUpdate														
*/

export default class clientsInvoicesUpdateController{

        constructor($scope,UserInfoConstant,$timeout,clientsInvoicesServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element,$window,$stateParams,quotationsServices,saleNoteServices,budgetsServices,LOCAL_ENV_CONSTANT,transferGuidesServices,transportationServices){
            var vm = this;
            vm.createInvoice = createInvoice
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getStock = getStock
            $scope.invoicedTrue = false
            vm.getNet = getNet
            vm.showGeneralDiscount = showGeneralDiscount
            vm.searchClient = searchClient
            vm.packOffSelect = packOffSelect
            vm.setTransportation = setTransportation
            vm.loadItems = loadItems
            vm.getNetTotal = getNetTotal
            vm.isCreditUpdate = isCreditUpdate
            vm.showSelectClients = showSelectClients
            vm.webItemUpdate = webItemUpdate
            vm.invoiceCreate = invoiceCreate
            vm.invoicedQuotation = invoicedQuotation
            $scope.invoiceCode= $stateParams.idInvoice 
            $scope.buttonInvoice = true
            $scope.parseInt = parseInt
            $scope.buttonCreateInvoice = true
            $scope.packOff = false
            $scope.panelUpdate = false
            // $scope.discount = parseInt($stateParams.discount)
            $scope.discount = parseFloat($stateParams.discount)
            $scope.type = $stateParams.type
            $scope.purchaseOrder = 0
            $scope.warningItems = true
            $scope.origenAdd = false
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
            $scope.priceVATest = 56000
            vm.searchData = searchData
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }
            $scope.notStock = 0
            $scope.warningQuantity = false
            $scope.buttonMore = false
            $scope.historyItems = []
            $scope.invoiceItems = []
            if ($scope.type == 'factura' ){
                $scope.clone = true
                $scope.quotation = false
                $scope.note = false
                $scope.budget = false
                $scope.guide = false
                $scope.observation = "Factura basada en factura N° " + $scope.invoiceCode
            }
            if ($scope.type == 'cotizacion' ){
                $scope.invoicedTrue = true
                $scope.panelUpdate = true
                $scope.clone = false
                $scope.note = false
                $scope.quotation = true
                $scope.budget = false
                $scope.guide = false
                $scope.observation = "Factura basada en cotización N° " + $scope.invoiceCode

                let params = {
                  quotationId: $scope.invoiceCode
                }
                quotationsServices.quotations.getQuotationsDetails(params).$promise.then((dataReturn) => {
                  $scope.quotation = dataReturn.data
                  $scope.clientName = $scope.quotation[0].clientName
                  $scope.clientId = $scope.quotation[0].clientId 
                  $scope.rut = $scope.quotation[0].rut

                  let params = {
                      clientId: $scope.clientId
                    }

                   clientsServices.clients.getClientsDetails(params).$promise.then((dataReturn) => {
                    $scope.clients = dataReturn.data

                    let clients = {
                      fullName :  $scope.clients[0].fullName,
                      clientId: $scope.clients[0].clientId,
                      rut: $scope.clients[0].rut,
                      address: $scope.clients[0].address,
                      phone: $scope.clients[0].phone,
                      city: $scope.clients[0].cityName,
                      locked: $scope.clients[0].locked,
                      isCredit: $scope.clients[0].isCredit
                     }

                     setName(clients)

                    },(err) => {
                        console.log('No se ha podido conectar con el servicio',err);
                    })

                  console.log( $scope.quotation)
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio get quotation Details',err);
                })
            }
            if ($scope.type == 'nota' ){
                $scope.clone = false
                $scope.quotation = false
                $scope.note = true
                 $scope.budget = false
                 $scope.guide = false
                $scope.observation = "Factura basada en nota de venta N° " + $scope.invoiceCode
            }

            if ($scope.type == 'presupuesto' ){
                $scope.clone = false
                $scope.quotation = false
                $scope.note = false
                $scope.budget = true
                $scope.guide = false
                $scope.observation = "Factura basada en presupuesto N° " + $scope.invoiceCode
            }

            if ($scope.type == 'guia' ){
                $scope.clone = false
                $scope.quotation = false
                $scope.note = false
                $scope.budget = false
                $scope.guide = true
                $scope.observation = "Factura basada en Guia despacho N° " + $scope.invoiceCode
            }

            if ($scope.type == 'items' ){
                $scope.clone = false
                $scope.quotation = false
                $scope.note = false
                $scope.budget = false
                $scope.guide = false
                // $scope.observation = "Crear factura a partir de item N° " + $scope.invoiceCode
            }

            function searchData() {
              itemsServices.items.getItems().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.itemsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
        }


            transportationServices.transportation.getTransportation().$promise.then((dataReturn) => {
              $scope.transportation = dataReturn.data;
                 console.log('$scope.transportationGrid',$scope.transportation);
             },(err) => {
                 console.log('No se ha podido conectar con el servicio',err);
             })
            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.origenAdd = true
                            console.log('$scope.warehouses',$scope.warehouses,$stateParams.discount)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            loadItems()
            setWarehouseId()

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.clientsInvoicesUpdate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                   '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.clientsInvoicesUpdate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }  
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{ row.entity.vatPrice | pesosChilenos }}</strong></p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    }
                ]
            }
            
          
            function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            function showSelectClients(){
                $scope.panelUpdate = false
                $scope.clientName = ''
                $scope.clientId = ''

            }

            function loadItem(row){
                
                var i = 0
                  for (i=0; i<$scope.invoiceItems.length; i++) {
                        var id = $scope.invoiceItems[i].itemId

                        if(id == row.itemId){
                          ngNotify.set('Item '+ row.itemId +' Duplicado en este Documento','warn')
                          $scope.invoiceItems.splice(index, 1)

                        }else{
                          console.log("item no puplicado",row.itemId)
                        }
                        
                      } 

                if (row.failure == 1) {
                   ngNotify.set( 'Item '+ row.itemId +' con FALLA, valide con supervisor antes de continuar', {
                        sticky: true,
                        button: true,
                        type : 'error'
                    }) 

                }else{

                    $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription  + ' (' + row.brandCode+ ')',
                    price: row.netPrice,
                    minPrice: row.netPrice,
                    netPurchaseValue: row.netPurchaseValue,
                    webType: row.webType,
                    oil: row.oil,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                })

                    getStock(row.itemId,1)
                    if (row.oil == 1) {
                      $scope.discountButton = true
                      $scope.discount = 0
                    }
                    $scope.warningItems = false 
                }
                    
            }

            function showGeneralDiscount(value){

                if (value ==  0 || value ==  null ) {
                    console.log('muestra boton',value); 
                    $scope.discountButton = false

                }else{
                    console.log('esconde boton',value); 
                    $scope.discountButton = true
                    $scope.discount = 0

                }
            
            }

            function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            
            function getStock(itemId,quantity){
            
              let params = {
                itemId: itemId,
                warehouseId :$scope.warehouseId 
              }

            itemsServices.items.getStock(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data
              $scope.stock = $scope.item[0].stock
              $scope.notStock = 0
              // $scope.warningQuantity = false
              // $scope.buttonMore = false

              if (quantity > $scope.stock) {

                ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock , {
                  sticky: true,
                  button: true,
                  type : 'warn'
              })
                $scope.notStock = 1
                $scope.warningQuantity = true
                $scope.buttonMore = true
                
              }
              
              return false;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
    
            }

            function getNet() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 
                  }
                  
                  $scope.NET =  net
                  $scope.netoInvoice = net - (($scope.discount*net)/100) 
                  $scope.total = Math.round($scope.netoInvoice * 1.19)
                  $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                  return net;
                      
            }

            function getNetTotal() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.netPurchaseValue * valueItems.quantity); 

                  }
                  
                  $scope.netTotal =  net
                  return $scope.netTotal;
                      
            }

            function removeItems(index,oil) {

              console.log('oil',oil)

                $scope.invoiceItems.splice(index, 1)
                $scope.warningQuantity = false
                $scope.buttonMore = false

                if (oil == 1) { 
                  $scope.discountButton = false
                }

               var i = 0
               for (i=0; i<$scope.invoiceItems.length; i++) {
                    var id = $scope.invoiceItems[i].itemId
                    var stockId = $scope.invoiceItems[i].quantity
                    console.log("item x",id,stockId)
                    getStock(id,stockId)
                  }
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                $scope.warehouseId = warehouseId
                console.log('origen seleccionado',$scope.warehouseId)
            }

            function setName(clients) {
                console.log(clients)
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedTwo = clients.rut
                $scope.address = clients.address
                $scope.phone = clients.phone
                $scope.city = clients.city
                $scope.isCredit = clients.isCredit
                
                if (clients.locked == 1) {
                    ngNotify.set('Cliente bloqueado sin linea de Crédito','error')
                    $scope.buttonInvoice = false
                    $scope.lockedClient = true

                }else{
                    $scope.buttonInvoice = true
                    $scope.lockedClient = false
                }
            }

            function showPanel() {
                $scope.invoiceItems = []
                $scope.panelInvoice = true
                $scope.warningQuantity = false
                $scope.buttonMore = false

                if ($scope.type == 'factura' ){
                let params = {
                    invoiceId: $stateParams.idInvoice,
                    originId: $scope.warehouseId
                  }

                clientsInvoicesServices.invoices.getInvoicesItems(params).$promise.then((dataReturn) => {
                      $scope.itemsInvoicesClone = dataReturn.data
                      console.log('itemsInvoicesClone',$scope.itemsInvoicesClone);
                      for(var i in $scope.itemsInvoicesClone){
                            $scope.invoiceItems.push({
                                itemId: $scope.itemsInvoicesClone[i].itemId,
                                itemDescription: $scope.itemsInvoicesClone[i].itemDescription + ' ('+ $scope.itemsInvoicesClone[i].brandCode+ ')',
                                price: $scope.itemsInvoicesClone[i].price,
                                minPrice: $scope.itemsInvoicesClone[i].price,
                                oil: $scope.itemsInvoicesClone[i].oil,
                                netPurchaseValue: $scope.itemsInvoicesClone[i].netPurchaseValue,
                                webType: $scope.itemsInvoicesClone[i].webType,
                                maxDiscount : $scope.itemsInvoicesClone[i].maxDiscount,
                                quantity: $scope.itemsInvoicesClone[i].quantityItems,
                                disscount: $scope.itemsInvoicesClone[i].discount
                         })
                            getStock($scope.itemsInvoicesClone[i].itemId,$scope.itemsInvoicesClone[i].quantityItems)
                                if ($scope.itemsInvoicesClone[i].oil == 1) {
                                $scope.discountButton = true
                                // $scope.discount = 0
                              }

                    }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get Invoices',err);
                    })
                }

                if ($scope.type == 'cotizacion' ){
                  let params = {
                    quotationId: $scope.invoiceCode,
                    originId: $scope.warehouseId
                  }

                    quotationsServices.quotations.getQuotationsItems(params).$promise.then((dataReturn) => {
                          $scope.itemsQuotations = dataReturn.data
                          for(var i in $scope.itemsQuotations){
                                $scope.invoiceItems.push({
                                    itemId: $scope.itemsQuotations[i].itemId,
                                    itemDescription: $scope.itemsQuotations[i].itemDescription + ' ('+ $scope.itemsQuotations[i].brandCode+ ')',
                                    price: $scope.itemsQuotations[i].price,
                                    minPrice: $scope.itemsQuotations[i].price,
                                    oil: $scope.itemsQuotations[i].oil,
                                    netPurchaseValue: $scope.itemsQuotations[i].netPurchaseValue,
                                    webType: $scope.itemsQuotations[i].webType,
                                    maxDiscount : $scope.itemsQuotations[i].maxDiscount,
                                    quantity: $scope.itemsQuotations[i].quantityItems,
                                    disscount: $scope.itemsQuotations[i].discount
                             })
                                getStock($scope.itemsQuotations[i].itemId,$scope.itemsQuotations[i].quantityItems)
                                  if ($scope.itemsQuotations[i].oil == 1) {
                                  $scope.discountButton = true
                                  $scope.discount = 0
                                }
                        }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get quotations',err);
                        })
                }

                if ($scope.type == 'items' ){
                  let params = {
                    itemId: $scope.invoiceCode
                  }

                    itemsServices.items.itemsList(params).$promise.then((dataReturn) => {
                          $scope.itemDetails = dataReturn.data
                          // for(var i in $scope.itemDetails){
                                $scope.invoiceItems.push({
                                    itemId: params.itemId,
                                    itemDescription: $scope.itemDetails[0].itemName,
                                    price: $scope.itemDetails[0].netPrice,
                                    minPrice: $scope.itemDetails[0].netPrice,
                                    oil: $scope.itemDetails[0].oil,
                                    netPurchaseValue: $scope.itemDetails[0].netPurchaseValue,
                                    webType: $scope.itemDetails[0].webType,
                                    maxDiscount : $scope.itemDetails[0].maxDiscount,
                                    quantity: 1,
                                    disscount: 0
                             })
                                getStock(params.itemId,1)
                                  if ($scope.itemDetails[0].oil == 1) {
                                  $scope.discountButton = true
                                  // $scope.discount = 0
                                }
                        // }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get items',err);
                        })
                }

                if ($scope.type == 'nota' ){
                  let params = {
                    saleNoteId: $scope.invoiceCode
                  }

                    saleNoteServices.saleNote.getSaleNotesItems(params).$promise.then((dataReturn) => {
                          $scope.saleNoteItems = dataReturn.data
                          for(var i in $scope.saleNoteItems){
                                $scope.invoiceItems.push({
                                    itemId: $scope.saleNoteItems[i].itemId,
                                    itemDescription: $scope.saleNoteItems[i].itemDescription,
                                    price: $scope.saleNoteItems[i].price,
                                    oil: $scope.saleNoteItems[i].oil,
                                    minPrice: $scope.saleNoteItems[i].price,
                                    netPurchaseValue: $scope.saleNoteItems[i].netPurchaseValue,
                                    webType: $scope.saleNoteItems[i].webType,
                                    maxDiscount : $scope.saleNoteItems[i].maxDiscount,
                                    quantity: $scope.saleNoteItems[i].quantityItems,
                                    disscount: $scope.saleNoteItems[i].discount
                             })
                                getStock($scope.saleNoteItems[i].itemId,$scope.saleNoteItems[i].quantityItems)
                                if ($scope.saleNoteItems[i].oil == 1) {
                                  $scope.discountButton = true
                                  // $scope.discount = 0
                                }
                        }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get quotations',err);
                        })
                }


                if ($scope.type == 'guia' ){
                  let params = {
                    transferGuideId: $scope.invoiceCode
                  }

                    transferGuidesServices.guides.getGuidesItems(params).$promise.then((dataReturn) => {
                          $scope.itemsGuides = dataReturn.data
                          for(var i in $scope.itemsGuides){
                                $scope.invoiceItems.push({
                                    itemId: $scope.itemsGuides[i].itemId,
                                    itemDescription: $scope.itemsGuides[i].itemDescription,
                                    price: $scope.itemsGuides[i].price,
                                    oil: $scope.itemsGuides[i].oil,
                                    minPrice: $scope.itemsGuides[i].price,
                                    webType: $scope.itemsGuides[i].webType,
                                    maxDiscount : $scope.itemsGuides[i].maxDiscount,
                                    quantity: $scope.itemsGuides[i].quantityItems,
                                    disscount: 0
                             })
                                getStock($scope.itemsGuides[i].itemId,$scope.itemsGuides[i].quantityItems)
                                  if ($scope.itemsGuides[i].oil == 1) {
                                  $scope.discountButton = true
                                  // $scope.discount = 0
                                }
                        }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get quotations',err);
                        })
                }


                if ($scope.type == 'presupuesto' ){
                  let params = {
                    budgetId: $scope.invoiceCode
                  }

                    budgetsServices.budgets.getBudgetsItems(params).$promise.then((dataReturn) => {
                          $scope.budgetsItems = dataReturn.data
                          for(var i in $scope.budgetsItems){
                                $scope.invoiceItems.push({
                                    itemId: $scope.budgetsItems[i].itemId,
                                    itemDescription: $scope.budgetsItems[i].itemDescription,
                                    price: $scope.budgetsItems[i].price,
                                    minPrice: $scope.budgetsItems[i].price,
                                    oil: $scope.budgetsItems[i].oil,
                                    netPurchaseValue: $scope.budgetsItems[i].netPurchaseValue,
                                    webType: $scope.budgetsItems[i].webType,
                                    maxDiscount : $scope.budgetsItems[i].maxDiscount,
                                    quantity: $scope.budgetsItems[i].quantityItems,
                                    disscount: $scope.budgetsItems[i].discount
                             })
                                getStock($scope.budgetsItems[i].itemId,$scope.budgetsItems[i].quantityItems)
                                if ($scope.budgetsItems[i].oil == 1) {
                                  $scope.discountButton = true
                                  // $scope.discount = 0
                                }

                        }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio getBudgetsItems',err);
                        })
                }   
            }

            function packOffSelect(value){
              console.log('packOffSelect',value)
              if(value == true){
                $scope.transporteView = true
              }else{
                $scope.transporteView = false
              }
            }

            function setTransportation(transportation) {
              console.log(transportation)
              $scope.transportationName = transportation.transportName
              $scope.transportationId = transportation.transportationId
            }


            function isCreditUpdate(invoiceId,days) {

             let model = {
                invoiceId: invoiceId,
                days: days
                }

                clientsInvoicesServices.invoices.setCreditStatus(model).$promise.then((dataReturn) => {
                  $scope.result = dataReturn.data;
                  console.log('Se actualizo factura a tipo crédito',err);

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                })
            }

            function createInvoice() {

                        let model = {
                            userId: $scope.userInfo.id,
                            purchaseOrder: $scope.purchaseOrder,
                            clientId : $scope.clientId,
                            date : $scope.CurrentDate,
                            originId : $scope.warehouseId,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            netTotal : $scope.netTotal,
                            comment : $scope.observation,
                            packOff:  $scope.packOff,
                            receiver:  $scope.receiver, 
                            paymentmethodId : parseInt($scope.paymentmethodId),
                            transportationId:  $scope.transportationId, 
                            transportationName:  $scope.transportationName, 
                            itemsInvoices : $scope.invoiceItems 
                        }      

                        console.log('modelo',model)
                        for(var  i in model.itemsInvoices){
                          let modelo = {
                              itemId: model.itemsInvoices[i].itemId,
                              originId :$scope.warehouseId,
                              quantity: model.itemsInvoices[i].quantity
                           }
          
                              itemsServices.items.getHistoric(modelo).$promise.then((dataReturn) => {
                                 $scope.item = dataReturn.data;
      
                                  $scope.historyItems.push({
                                  itemId: $scope.item[0].itemId,
                                  itemDescription: $scope.item[0].itemDescription,
                                  location : $scope.item[0].locations,
                                  previousStock: $scope.item[0].stock,
                                  price: $scope.item[0].vatPrice,
                                  quantity: modelo.quantity,
                                  generalStock: $scope.item[0].generalStock,
                                  currentprice: $scope.item[0].vatPrice,
                                  currentLocation: $scope.item[0].locations
                                  })
                                      
                                  console.log('that', $scope.historyItems);
      
                                  if (i== $scope.historyItems.length-1) {
                                        
                                    invoiceCreate(model,$scope.historyItems)
                                      $scope.ultimo = "último registro";
                                    } else {
                                      $scope.ultimo = "item"+i;
                                    }
                                      console.log( $scope.ultimo); 
                                     },(err) => {
                                        console.log('No se ha podido conectar con el servicio',err);
                                  })
                      }

                        
            }

            function invoiceCreate(model,historyItems){

              clientsInvoicesServices.invoices.createInvoice(model).$promise.then((dataReturn) => {
                ngNotify.set('Se ha creado la factura correctamente','success')
                $scope.result = dataReturn.data

                console.log('result',$scope.result)

                if (model.paymentmethodId == 12) { isCreditUpdate($scope.result,30) }
                if (model.paymentmethodId == 17) { isCreditUpdate($scope.result,30) }
                if (model.paymentmethodId == 13) { isCreditUpdate($scope.result,60) }
                  
                 // $window.open('http://localhost/jormat-system/XML_invoice.php?invoiceId= '+$scope.result, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
                $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_invoice.php?invoiceId='+$scope.result, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
                ngNotify.set('Documento enviado a SII N° FAC ' +$scope.result+ ' / Validar en Facturas Chile','warn')
                $state.go('app.clientsInvoices')

                if($scope.invoicedTrue == true){
                  invoicedQuotation($scope.result)
                }

                for(var  i in historyItems){
                  $scope.item = dataReturn.data;

                  let itemsValue = {
                      documentId: $scope.result,
                      itemId: historyItems[i].itemId,
                      itemDescription: historyItems[i].itemDescription,
                      location : historyItems[i].location,
                      previousStock: historyItems[i].previousStock,
                      price: historyItems[i].price,
                      quantity: historyItems[i].quantity,
                      originId: $scope.warehouseId,
                      userId: $scope.userInfo.id,
                      document: "FAC",
                      type: "Salida -",
                      generalStock: historyItems[i].generalStock,
                      currentprice: historyItems[i].price,
                      currentLocation: historyItems[i].currentLocation,
                      destiny: undefined

                  }
                 
                 console.log('that',itemsValue);
   
                 itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                        $scope.result2 = dataReturn.data;
                        console.log('Historial del item actualizado correctamente');
                    },(err) => {
                        console.log('No se ha podido conectar con el servicio',err);
                    })
                }

                    if ($scope.type == 'nota' ){

                        let params = {
                                saleNoteId: $stateParams.idInvoice,
                                userId: $scope.userInfo.id
                            }
                        saleNoteServices.saleNote.saleNoteUpdate(params).$promise.then((dataReturn) => {
                        console.log('La nota de venta también fue actualizada');
                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                      })
                    }

                    for(var  i in model.itemsInvoices){

                    console.log('entra',model.itemsInvoices)

                    if (model.itemsInvoices[i].webType == "SI"){
                        console.log('tipo web')

                        let modelo = {
                            itemId : model.itemsInvoices[i].itemId
                        }

                        itemsServices.items.getGeneralStockWeb(modelo).$promise.then((dataReturn) => {
                           $scope.result = dataReturn.data;
                           $scope.stock = $scope.result[0].generalStock;

                           webItemUpdate(modelo.itemId,$scope.stock)

                          },(err) => {
                              console.log('No se ha podido conectar con el servicio',err);
                          })

                         }
                    else{
                        console.log('normal')
                    }
                    }    
              },(err) => {
                  ngNotify.set('Error al crear factura','error')
              })

            }

            function webItemUpdate(itemId,stock) {

                    let data = {
                                        sku : itemId.toString(),
                                        stock_quantity: stock
                                    }

                                    itemsServices.woocommerceAPI.itemUpdate(data).$promise.then((dataReturn) => {
                                      $scope.result = dataReturn.data;
                                      $scope.message= dataReturn.message;
                                      $scope.status= dataReturn.status;

                                      if ($scope.status==200  ) {

                                         ngNotify.set('Mensaje sitio web: '+ $scope.message,'info')

                                          }else{
                                            console.log('Error actualizar stock web',$scope.message);
                                            ngNotify.set( 'Error actualización ITEM en el sitio web: ' + $scope.message, {
                                                sticky: true,
                                                button: true,
                                                type : 'warn'
                                            })
                                            
                                         }
                                      
                                      },(err) => {
                                            console.log('Error actualizar stock web',err);
                                            ngNotify.set( 'Error API Web', {
                                                sticky: true,
                                                button: true,
                                                type : 'warn'
                                          })
                                      })

            }
            
            function invoicedQuotation(documentId) {
              let model = {
                documentId: documentId,
                quotationId: $scope.invoiceCode,
                docTypeId: 1
             }

            quotationsServices.quotations.invoicedQuotation(model).$promise.then((dataReturn) => {
              $scope.msg = dataReturn.data;
              console.log('facturado correctamente');
             },(err) => {
              console.log('No se ha podido conectar con el servicio',err);
             })

            }  

            function searchClient (value) {

                let model = {
                    name: value
                }

                clientsServices.clients.getClientsByName(model).$promise.then((dataReturn) => {
                      $scope.clients = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

                return false;
            }

        
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

clientsInvoicesUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','clientsInvoicesServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element','$window','$stateParams','quotationsServices','saleNoteServices','budgetsServices','LOCAL_ENV_CONSTANT','transferGuidesServices','transportationServices'];

