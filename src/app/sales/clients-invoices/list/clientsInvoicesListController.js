
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> clientsInvoicesListController
* # As Controller --> clientsInvoicesList														
*/

export default class clientsInvoicesListController{

        constructor($rootScope,$scope,UserInfoConstant,$timeout,clientsInvoicesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,$window,LOCAL_ENV_CONSTANT,i18nService, uiGridExporterService, $translate, uiGridExporterConstants){
            var vm = this
			this.$scope = $scope
			this.$translate = $translate
			this.$timeout = $timeout
			this.i18nService = i18nService
			this.LOCAL_ENV_CONSTANT = LOCAL_ENV_CONSTANT
			this.i18nService.setCurrentLang('es')
			this.clientsInvoicesServices = clientsInvoicesServices
			this.$rootScope = $rootScope
			this.ngNotify = ngNotify
			this.$modal = $modal
			this.$state = $state
			this.$window = $window
			this.$interval = $interval
			this.uiGridConstants = uiGridConstants
			this.uiGridExporterService = uiGridExporterService
			this.uiGridExporterConstants = uiGridExporterConstants
            this.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }
			this.$scope.dropdownMenu = require('./dropdownActionsMenu.html')
			this.tableInitializeMethod()

            this.$scope.UserInfoConstant = UserInfoConstant
			this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
				if (details !== undefined) {
					this.$scope.userInfo = details.user
					let model = {
						userId : this.$scope.userInfo.id
					}
					this.main()
				}
			})
		}

		main() {
			this.searchText = ''
			this.params = {
				newPage    	   : 1,
				pageSize       : 2000,
				searchText	   : this.searchText
			}
			this.loadInvoices()
			this.$rootScope.$on('$translateChangeSuccess', () => {
				this.tableInitializeMethod()
			})
		}

		dataDownload(items) {
			let dataReturn = []
	
			items.forEach((row) => {
				row = row.entity ? row.entity : row
				const rowData = [
					{ value: row.invoiceId },
					{ value: row.clientName },
					{ value: row.rut },
					{ value: row.total },
					{ value: row.origin },
					{ value: row.purchaseOrder },
					{ value: row.date },
					{ value: row.statusName },
				]
	
				dataReturn.push(rowData)
			})
			console.log(' dataReturn : ', dataReturn)
			return dataReturn
		}

		exportCSV() {
			const exportService = this.uiGridExporterService
			const dataFilter = this.dataDownload(this.gridApi.grid.rows)
			const grid = this.gridApi.grid
			const fileName = 'facturas_clientes.csv'
	
			exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
				const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
				let formatColums = []
	
				exportColumnHeaders.map((colums) => {
						formatColums.push(colums)
				})
				const csvContent = exportService.formatAsCsv(formatColums, dataFilter, grid.options.exporterCsvColumnSeparator)
	
				exportService.downloadFile(fileName, csvContent, grid.options.exporterOlderExcelCompatibility)
			}).catch(() => {
				this.ngNotify.set('ERROR', 'error')
			})
		}

		exportAllCSV() {
			const exportService = this.uiGridExporterService
	
			this.clientsInvoicesServices.invoices.getInvoicesPages().$promise.then(
				response => {
					if (!response.status) {
						const msn = response.messageError || response.message || this.$translate('MSJ_ERROR')
	
						this.ngNotify.set(msn, 'warn')
					} else {
						const dataFilter = this.dataDownload(response.data)
						const grid = this.gridApi.grid
						const fileName = 'facturas_clientes.csv'
	
						exportService.loadAllDataIfNeeded(grid, this.uiGridExporterConstants.ALL, this.uiGridExporterConstants.VISIBLE).then(() => {
							const exportColumnHeaders = exportService.getColumnHeaders(grid, this.uiGridExporterConstants.ALL)
							let formatColums = []
	
							exportColumnHeaders.map((colums) => {
									formatColums.push(colums)
							})
	
							const csvContent = exportService.formatAsCsv(formatColums, dataFilter, grid.options.exporterCsvColumnSeparator)
	
							exportService.downloadFile(fileName, csvContent, grid.options.exporterOlderExcelCompatibility)
						}).catch(() => {
							this.ngNotify.set('ERROR', 'error')
						})
					}
				},
				() => {
					this.ngNotify.set(this.$translate('MSJ_NO_DATA_RELATED_YOUR_SEARCH') + ' getDownloadAllCsv', 'error')
				},
			)
		}

		tableInitializeMethod() {
			this.$scope.invoicesGrid = {
				paginationPageSizes  : [ 500, 1000, 2000 ],
				paginationPageSize   : 2000,
				enablePaging         : true,
				useExternalPagination: true,
                enableFiltering      : true,
				enableGridMenu       : true,
				enableSelectAll      : true,
                showColumnFooter     : true,
				exporterMenuPdf      : false,
				exporterMenuCsv      : false,
				exporterMenuAllData  : false,
				multiSelect          : true,
                exporterCsvFilename: 'facturas_clientes.csv',
                onRegisterApi        : (gridApi) => {
					this.gridApi = gridApi
	
					this.$interval(() => {
						this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar vista como csv', action: () => {
							this.exportCSV()
						}, order: 100 } ])
						this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar todo como csv', action: () => {
							this.exportAllCSV()
						}, order: 100 } ])
					}, 0, 1)
	
					gridApi.pagination.on.paginationChanged(this.$scope, (newPage, pageSize) => {
						this.params.newPage = newPage
						this.params.pageSize = pageSize
	
						this.clientsInvoicesServices.invoices.getInvoicesPages(this.params).$promise.then(
							response => {
								if (!response.status) {
									const msn = response.messageError || response.message || 'ERROR'
				
									this.ngNotify.set(msn, 'warn')
								} else {
									this.invoices = angular.copy(response.data)
									this.$scope.invoicesGrid.data = response.data
									this.$scope.invoicesGrid.totalItems = response.count || 0
								}
							},
							() => {
								this.ngNotify.set('Sin respuesta getItemsMain', 'error')
							},
						)
					})
	
					gridApi.core.on.filterChanged(this.$scope, () => {
						const grid = gridApi.grid.renderContainers.body.grid
						let filters = {}
	
						for (let i in grid.columns) {
							if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term != undefined)) {
								filters[grid.columns[i].field] = grid.columns[i].filters[0].term
							}
						}

						this.params.newPage = 1
						this.params.filters = filters
	
						this.clientsInvoicesServices.invoices.getInvoicesPages(this.params).$promise.then(
							response => {
								if (!response.status) {
									const msn = response.messageError || response.message || 'ERROR'
				
									this.ngNotify.set(msn, 'warn')
								} else {
									this.invoices = angular.copy(response.data)
									this.$scope.invoicesGrid.data = response.data
									this.$scope.invoicesGrid.totalItems = response.count || 0
								}
							},
							() => {
								this.ngNotify.set('Sin respuesta getItemsMain', 'error')
							},
						)
					})
				},
				columnDefs: [
                    { 
                      	name:'Factura',
                      	field: 'invoiceId',  
                      	width: '7%',
                      	cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a tooltip-placement="right" tooltip="Ver factura" ng-click="grid.appScope.clientsInvoicesList.viewInvoice(row.entity)">{{row.entity.invoiceId}}</a>' +
                                 '</div>',
                      	cellClass: function(grid, row) {
                      		if (row.entity.statusName === 'Vencida') {
								return 'critical';
							}
                     	}  
                    },
					{ 
						name:'Cliente',
						field: 'clientName',
						width: '30%',
						cellTemplate:'<div class="ui-grid-cell-contents ">'+
									'<a tooltip-placement="right" tooltip="Ver estado cuenta" class="uppercase" ng-click="grid.appScope.clientsInvoicesList.accountStatusView(row.entity)">{{row.entity.clientName}}</a> <span tooltip="Código cliente" class="badge bg-info">{{row.entity.clientId}}</span>' +
									'</div>',
						cellClass: function(grid, row) {
							if (row.entity.statusName === 'Vencida') {
								return 'critical';
							}
                     	}        
                    },
                    { 
						name:'Rut',
						field: 'rut',
						width: '11%',
						cellTemplate:'<div class="ui-grid-cell-contents">'+
									'<p><span tooltip="Rut" >{{row.entity.rut}}</span></p>' +
									'</div>',
						cellClass: function(grid, row) {
							if (row.entity.statusName === 'Vencida') {
								return 'critical';
							}
                     	}
					},
                    { 
						name:'Total',
						field: 'total',
						width: '11%',
						aggregationHideLabel: false,
						aggregationType: this.uiGridConstants.aggregationTypes.sum,
						cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> $<strong> {{ row.entity.total | pesosChilenos }}</strong> </p>' +
                                 '</div>',
                      	cellClass: function(grid, row) {
                      		if (row.entity.statusName === 'Vencida') {
                        		return 'critical';
                      		}
                     	} 
                    },
                    { 
						name: 'Origen', 
						field: 'origin', 
						width: '9%',
						cellClass: function(grid, row) {
                      		if (row.entity.statusName === 'Vencida') {
                        		return 'critical';
                      		}
                     	} 
                    },
                     { 
						name:'Orden',
						field: 'purchaseOrder',
						width: '8%',
						cellTemplate:'<div class="ui-grid-cell-contents">'+
									'<p> {{row.entity.purchaseOrder}}</p>' +
									'</div>',
						cellClass: function(grid, row) {
							if (row.entity.statusName === 'Vencida') {
								return 'critical';
							}
                     	} 
                    },
                    { 
						name: 'Fecha', 
						field: 'date',
						width: '8%',
						cellTemplate:'<div class="ui-grid-cell-contents">'+
									'<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
									'</div>',
						cellClass: function(grid, row) {
							if (row.entity.statusName === 'Vencida') {
								return 'critical';
							}
                     	} 
                    },
                    { 
						name: 'Estado', 
						field: 'statusName', 
						width: '8%',
						cellTemplate:'<div class="ui-grid-cell-contents ">'+
									'<p class="badge bg-{{row.entity.style}}">{{row.entity.statusName}}</p>' +
									'</div>',
						cellClass: function(grid, row) {
                      		if (row.entity.statusName === 'Vencida') {
                        		return 'critical';
                      		}
                     	} 
                    },
                    { 
						name: 'Acciones',
						width: '8%', 
						field: 'href',
						cellTemplate: this.$scope.dropdownMenu,
						cellClass: function(grid, row) {
							if (row.entity.statusName === 'Vencida') {
								return 'critical';
							}
                     	},
						enableFiltering: false
                    }
                ]
            }
		}

		loadInvoices(){
			this.clientsInvoicesServices.invoices.getInvoicesPages(this.params).$promise.then((dataReturn) => {
				this.invoices = angular.copy(dataReturn.data)
				this.$scope.invoicesGrid.data = dataReturn.data
				this.$scope.invoicesGrid.totalItems = dataReturn.count || 0
			},(err) => {
				console.log('No se ha podido conectar con el servicio',err);
			})
		}

		print() {
			this.$window.print();
		}

		accountStatusView(row) {
			const url = this.$state.href("app.accountStatus", { 
				idClient: row.clientId
				});

			this.$window.open(url,'_blank');
		}

		printInvoice(row){
			this.$state.go("app.invoicePrint", { idInvoice: row.invoiceId});

			// const url = this.$state.href("app.invoicePrint", { 
			// 	idInvoice: row.invoiceId
			// 	});

			// this.$window.open(url,'_blank');
		}  


		warehouseInvoicePrint(row){

            console.log('imprimir print bodega'); 

            const url = this.$state.href("app.warehouseInvoicePrint", { 
				idInvoice: row.invoiceId
				});

			this.$window.open(url,'_blank');

          // $state.go("app.warehouseQuotationPrint", { idQuotation: $scope.quotationId,warehouse:$scope.originId});

        }

		searchData() {
			this.params.searchText = this.searchText
			this.params.newPage = 1

			this.clientsInvoicesServices.invoices.getInvoicesPages(this.params).$promise.then(
				response => {
					if (!response.status) {
						const msn = response.messageError || response.message || 'ERROR'
	
						this.ngNotify.set(msn, 'warn')
					} else {
						this.invoices = angular.copy(response.data)
						this.$scope.invoicesGrid.data = response.data
						this.$scope.invoicesGrid.totalItems = response.count || 0
					}
				},
				() => {
					this.ngNotify.set('Sin respuesta getItemsMain', 'error')
				},
			)
		}

        viewInvoice(row){
			this.$scope.invoiceData = row;
			const modalInstance  = this.$modal.open({
					template: require('../view/clients-invoices-view.html'),
					animation: true,
					scope: this.$scope,
					controller: 'clientsInvoicesViewController',
					controllerAs: 'clientsInvoicesView',
					size: 'lg'
			})
		}

		invoicesReport(row){
			this.$scope.invoiceData = row;
			this.$state.go("app.clientsInvoicesReport");
			// var modalInstance  = $modal.open({
			//         template: require('../report/invoices-report.html'),
			//         animation: true,
			//         scope: $scope,
			//         controller: 'invoicesReportController',
			//         controllerAs: 'invoicesReport',
			//         size: 'lg'
			// })
		}

		clone(row){
			console.log('entra',row);
			this.$state.go("app.clientsInvoicesUpdate", { idInvoice: row.invoiceId,discount:row.discount,type:'factura'});
		}

		createXML(row) { 
			// $window.open('http://localhost/jormat-system/XML_invoice.php?invoiceId='+row.invoiceId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
			this.$window.open(this.LOCAL_ENV_CONSTANT.pathXML+'XML_invoice.php?invoiceId='+row.invoiceId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
		}

		deleteInvoice(row){
			this.$scope.invoiceData = row;
			const modalInstance  = this.$modal.open({
					template: require('../delete/clients-invoices-delete.html'),
					animation: true,
					scope: this.$scope,
					controller: 'clientsInvoicesDeleteController',
					controllerAs: 'clientsInvoicesDelete'
			})
		}

		updateDocument(row) {
			const params = {
				userId : this.$scope.userInfo.id,
				typeDocumentId : 1,
				documentId : row.invoiceId,
				commentary : "Entrega Rápida",
				option : 7
			}
	
			this.clientsInvoicesServices.documents.setDocumentList(params).$promise.then((dataReturn) => {
				this.ngNotify.set('La entrega ha sido registrada exitosamente / Factura: '+ row.invoiceId,'warn');
				this.loadInvoices()
			},(err) => {
				this.ngNotify.set('Error al actualizar documento','error')
			})
			
		}
    }   

clientsInvoicesListController.$inject = ['$rootScope','$scope','UserInfoConstant','$timeout','clientsInvoicesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','$window','LOCAL_ENV_CONSTANT','i18nService', 'uiGridExporterService', '$translate', 'uiGridExporterConstants'];




