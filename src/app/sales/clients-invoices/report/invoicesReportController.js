/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> invoicesReportController
* # As Controller --> invoicesReport														
*/
  export default class invoicesReportController {

    constructor($scope, $filter,$rootScope, $http,clientsInvoicesServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
        vm.loadInvoices = loadInvoices
        vm.print = print
        vm.viewInvoice = viewInvoice
        vm.viewClientDetails = viewClientDetails
        vm.loadPDF = loadPDF
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
	   
        // loadInvoices()

        i18nService.setCurrentLang('es');

        $scope.invoicesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'facturas_emitidas_'+ moment($scope.startDate).format("YYYY-MM-DD") +'_y_'+ moment($scope.endDate).format("YYYY-MM-DD")+'.csv',
                exporterPdfHeader: { 
                    text: "Facturas emitidas entre los días : " + moment($scope.startDate).format("YYYY-MM-DD") +" y "+ moment($scope.endDate).format("YYYY-MM-DD"),
                    // text: "Reporte facturas por fecha", 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [0,20,0,0] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },
                columnDefs: [
                    
                    { 
                      name:'Id',
                      field: 'clientId',
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p><span tooltip="Código cliente" class="badge bg-info">{{row.entity.clientId}}</span></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase" ng-click="grid.appScope.invoicesReport.viewClientDetails(row.entity)">{{row.entity.clientName}}</a>' +
                                 '</div>'        
                    },
                    { 
                      name:'N° Factura',
                      field: 'invoiceId',  
                      width: '11%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.invoicesReport.viewInvoice(row.entity)">{{row.entity.invoiceId}}</a>' +
                                 '</div>'  
                    },
                    
                    { 
                      name:'Total',
                      field: 'total',
                      width: '11%',
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{row.entity.total}}</p>' +
                                 '</div>'
                    },

                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '10%'
                    },
                     { 
                      name:'Orden',
                      field: 'purchaseOrder',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.purchaseOrder}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 11px;" class="badge bg-{{row.entity.style}}">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    }
                ]
            }

        function loadPDF(){
                var doc = new jsPDF();
                doc.autoTable({html:'#content'});
        }

        function viewClientDetails(row){ 

            var url = $state.href("app.invoicesByClient",{ 
                clientId: row.clientId,
                startDate:moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate:moment($scope.endDate).format("YYYY-MM-DD").toString()
            });

            window.open(url,'_blank',"width=600,height=700");
        }


        function loadInvoices(){

            let model = {
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            clientsInvoicesServices.invoices.getReporInvoices(model).$promise.then((dataReturn) => {
              $scope.invoicesGrid.data = dataReturn.data

              if ($scope.invoicesGrid.data.length == 0){
                      $scope.messageInvoices = true
                      ngNotify.set( 'No existes facturas creadas entre '+ model.startDate +' y '+ model.endDate , {
                      sticky: true,
                      button: true,
                      type : 'warn'
                  })
                  }else{
                      $scope.messageInvoices = false
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

        function viewInvoice(row){
                $scope.invoiceData = row;
                var modalInstance  = $modal.open({
                        template: require('../view/clients-invoices-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'clientsInvoicesViewController',
                        controllerAs: 'clientsInvoicesView',
                        size: 'lg'
                })
           }

	    function print() {
			window.print();
		}



    }

  }

  invoicesReportController.$inject = ['$scope', '$filter','$rootScope', '$http','clientsInvoicesServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
