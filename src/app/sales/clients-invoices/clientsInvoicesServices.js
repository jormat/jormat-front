/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description 23223
* # name Services --> clientsInvoicesServices
* file for call at services for providers
*/

class clientsInvoicesServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    const invoices = $resource(SERVICE_URL_CONSTANT.jormat + '/invoices/:route',{},{
      
        getInvoices: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getInvoicesPages: {
            method:'GET',
            params : {
               route: 'list-pages'
            }
        },
        getInvoicesDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createInvoice: {
            method:'POST',
            params : {
                route: ''
            }
        },
        updateInvoice: {
            method:'PUT',
            params : {
                route: ''
            }
        },
        disabledInvoice: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        getInvoicesItems: {
            method:'GET',
            params : {
               route: 'items'
            }
        },
        getReporInvoices: { 
            method:'GET',
            params : {
               route: 'report'
            }
        },
        getInvoicesByClient: {
            method:'GET',
            params : {
               route: 'clients'
            }
        },
        updatePackOff: {
            method:'DELETE',
            params : {
               route: 'packoff'
            }
        },
        setCreditStatus: {
            method:'PUT',
            params : {
                route: 'credit'
            }
        },
        getExpirationInvoices: {
            method:'GET',
            params : {
               route: 'expiration'
            }
        },
        setStatusInvoices: {
            method:'PUT',
            params : {
                route: 'status'
            }
        }
    })

    const documents = $resource(SERVICE_URL_CONSTANT.jormat + '/warehouseDeliveries/:route',{},{
        setDocumentList: {
            method:'POST',
            params : {
                route: 'update'
            }
        },
    })

	return {
		invoices : invoices,
		documents: documents
	}
  }
}

  clientsInvoicesServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.clientsInvoicesServices', [])
  .service('clientsInvoicesServices', clientsInvoicesServices)
  .name;