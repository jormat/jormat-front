/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> clientsInvoicesViewController
* # As Controller --> clientsInvoicesView
*/

export default class clientsInvoicesViewController {
	constructor($scope, $filter,$rootScope, $http,$modalInstance,clientsInvoicesServices,$location,UserInfoConstant,ngNotify,$state,$stateParams, Dialogs) {
		this.btnVisible = false;
		this.ngNotify = ngNotify
		this.$scope = $scope;
		this.Dialogs = Dialogs;
		this.$state = $state;
		this.UserInfoConstant = UserInfoConstant[0].details;
		this.$modalInstance = $modalInstance;
		this.clientsInvoicesSrv = clientsInvoicesServices
		this.$scope.invoiceId = this.$scope.invoiceData.invoiceId
		this.$scope.clientName = this.$scope.invoiceData.clientName

		this.loadData();
	}

	loadData(){
		let params = {
			invoiceId: this.$scope.invoiceId
		}

		this.clientsInvoicesSrv.invoices.getInvoicesDetails(params).$promise.then((dataReturn) => {
			this.$scope.invoice = dataReturn.data
			
			this.btnVisible = (this.$scope.invoice[0].userOut) ? false : true;
				if(this.$scope.invoice[0].packOff == 'Si'){
					this.$scope.deliveryType =  "Despacho"
				}else{
					this.$scope.deliveryType =  "Mesón"
				}

					let model = {
						invoiceId: this.$scope.invoiceId,
						originId: this.$scope.invoice[0].originId
					}

				this.clientsInvoicesSrv.invoices.getInvoicesItems(model).$promise.then((dataReturn) => {
					this.$scope.invoiceItems = dataReturn.data

					    var subTotal = 0
			            var i = 0
			                 for (i=0; i<this.$scope.invoiceItems.length; i++) {
			                     subTotal = subTotal + this.$scope.invoiceItems[i].total  
			                  }
			                  
			                  this.$scope.subTotality =  subTotal
				},(err) => {
					console.log('No se ha podido conectar con el servicio get Invoices items',err);
				})
		},(err) => {
			console.log('No se ha podido conectar con el servicio get Invoices Details',err);
		})

	}

	cancel() {
		this.$modalInstance.dismiss('chao');
	}

	updatePackOff() {
		this.params = {
					userId : this.UserInfoConstant[0].user.id,
					invoiceId : this.$scope.invoiceId
				}

		console.log('parametros',this.params);
		
		this.clientsInvoicesSrv.invoices.updatePackOff(this.params).$promise.then((dataReturn) => {
					this.ngNotify.set('La factura a sido actualizada con exito','warn');
				},(err) => {
					this.ngNotify.set('Error al actualizar documento','error')
				})
	}

	printPackOff() {
		console.log('imprimir PackOff');
		var url = this.$state.href("app.printPackOff",{
			idInvoice: this.$scope.invoiceId
		});

		window.open(url,'_blank',"width=600,height=700");

	}

	clone() {
		this.$state.go("app.clientsInvoicesUpdate", { idInvoice: this.$scope.invoiceId,discount:this.$scope.invoice[0].discount,type:'factura'});

	}

	updateDocument(){
		
		this.$scope.isEdit = true;

		this.params = {
					userId : this.UserInfoConstant[0].user.id,
					typeDocumentId : this.$scope.invoice[0].typeId,
					documentId : this.$scope.invoiceId,
					commentary : "Entrega Rápida",
                    option : 7
				}
		
		this.clientsInvoicesSrv.documents.setDocumentList(this.params).$promise.then((dataReturn) => {
					this.ngNotify.set('La entrega ha sido registrada exitosamente','warn');
					this.$state.reload('app.warehouseDeliveries')
					this.$modalInstance.dismiss('chao');
				},(err) => {
					this.ngNotify.set('Error al actualizar documento','error')
				})
	}
}
clientsInvoicesViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','clientsInvoicesServices','$location','UserInfoConstant','ngNotify','$state','$stateParams', 'Dialogs'];
