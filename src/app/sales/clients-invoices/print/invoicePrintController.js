/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> invoicePrintController
* # As Controller --> invoicePrint														
*/


  export default class invoicePrintController {

    constructor($scope, $filter,$rootScope, $http,clientsInvoicesServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.warehouseInvoicePrint = warehouseInvoicePrint
        vm.updatePackOff= updatePackOff
        vm.printPackOff = printPackOff
        $scope.invoiceId = $stateParams.idInvoice

        let params = {
                invoiceId: $scope.invoiceId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        clientsInvoicesServices.invoices.getInvoicesDetails(params).$promise.then((dataReturn) => {
              $scope.invoice = dataReturn.data
              if($scope.invoice[0].packOff == 'Si'){
				$scope.deliveryType =  "Despacho"
    			}else{
    				$scope.deliveryType =  "Mesón"
    		    }   

                let model = {
                        invoiceId: $scope.invoiceId,
                        originId: $scope.invoice[0].originId
                    }

                   clientsInvoicesServices.invoices.getInvoicesItems(model).$promise.then((dataReturn) => {
                    $scope.invoiceItems = dataReturn.data

                      var subTotal = 0
                      var i = 0

                         for (i=0; i<$scope.invoiceItems.length; i++) {
                             subTotal = subTotal + $scope.invoiceItems[i].total  
                          }

                          $scope.subTotality =  subTotal
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio get Invoices Details',err);
                  })
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get invoice Details',err);
              })

              

        	function cancel() {

    		  $state.go("app.clientsInvoices")
    		}


             function print() {
              window.print();

        }

        function warehouseInvoicePrint(){

            console.log('imprimir print bodega');
            var url = $state.href("app.warehouseInvoicePrint",{
                idInvoice: $scope.invoiceId
            });

        window.open(url,'_blank',"width=600,height=700");

          // $state.go("app.warehouseQuotationPrint", { idQuotation: $scope.quotationId,warehouse:$scope.originId});

        }

        function updatePackOff() {
          let params = {
                        userId : $scope.userInfo.id,
                        invoiceId : $scope.invoiceId
                    }

            console.log('parametros',params);
            
            clientsInvoicesServices.invoices.updatePackOff(params).$promise.then((dataReturn) => {
                        ngNotify.set('La factura a sido actualizada con exito','warn');
                        $state.reload("app.invoicePrint")
                    },(err) => {
                        ngNotify.set('Error al actualizar documento','error')
                    })
        }

            function printPackOff() {
                console.log('imprimir PackOff');
                var url = $state.href("app.printPackOff",{
                    idInvoice: $scope.invoiceId
                });

                window.open(url,'_blank',"width=600,height=700");

            }




    }

  }

  invoicePrintController.$inject = ['$scope', '$filter','$rootScope', '$http','clientsInvoicesServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
