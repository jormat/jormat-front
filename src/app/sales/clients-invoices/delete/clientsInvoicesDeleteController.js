/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description asada
* # name controller --> clientsInvoicesDeleteController
* # As Controller --> clientsInvoicesDelete														
*/


  export default class clientsInvoicesDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,clientsInvoicesServices,$location,UserInfoConstant,ngNotify) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.deleteInvoice = deleteInvoice
	    $scope.invoiceId = $scope.invoiceData.invoiceId
	    $scope.rut = $scope.invoiceData.rut
	    $scope.clientName = $scope.invoiceData.clientName

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteInvoice() {
			let model = {
                    invoiceId : $scope.invoiceId,
                    userId : $scope.userInfo.id
                }

                clientsInvoicesServices.invoices.disabledInvoice(model).$promise.then((dataReturn) => {

                    ngNotify.set('Factura eliminada exitosamente','success');
					         $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al eliminar factura','error')
                  })
		}
	    


    }

  }

  clientsInvoicesDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','clientsInvoicesServices','$location','UserInfoConstant','ngNotify'];
