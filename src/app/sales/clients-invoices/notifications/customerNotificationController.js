/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> customerNotificationController
* # As Controller --> customerNotification													
*/


  export default class customerNotificationController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,clientsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
        vm.viewMore = viewMore
        $scope.isCredit = $scope.isCredit
	    $scope.clientId = $scope.clientId
        
        if ($scope.isCredit == 'Crédito'){
            $scope.creditValue = 'SI'
        }else{
            $scope.creditValue = 'NO'
        }

        let params = {
                clientId: $scope.clientId
            }

        clientsServices.clients.getInvoicesNotificationClient(params).$promise.then((dataReturn) => {
                $scope.invoicesPending = dataReturn.data
                $scope.invoicesSum = $scope.invoicesPending.length
                console.log('invoices', $scope.invoicesPending.length);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        clientsServices.clients.getNotesNotificationClient(params).$promise.then((dataReturn) => {
                $scope.notes = dataReturn.data
                $scope.notesSum = $scope.notes.length
                console.log('notes', $scope.notesSum);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        clientsServices.clients.getChecksNotificationClient(params).$promise.then((dataReturn) => {
                $scope.checks = dataReturn.data
                $scope.checksSum = $scope.checks.length
                console.log('checks', $scope.checksSum);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })
	    
	    function cancel() {
			$modalInstance.dismiss('chao');
		}

        function viewMore() {

            var url = $state.href("app.accountStatus",{ 
                idClient: $scope.clientId
            });

            window.open(url,'_blank');
        }



    }

  }

  customerNotificationController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','clientsServices','$location','UserInfoConstant','ngNotify','$state'];
