/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> printPackOffController
* # As Controller --> printPackOff														
*/


  export default class printPackOffController {

    constructor($scope, $filter,$rootScope, $http,clientsInvoicesServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.invoiceId = $stateParams.idInvoice

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        let model = {
                invoiceId: $scope.invoiceId
            }

        clientsInvoicesServices.invoices.getInvoicesDetails(model).$promise.then((dataReturn) => {
              $scope.invoice = dataReturn.data
              $scope.invoiceDetails  = { 
                    clientName : $scope.invoice[0].clientName,
                    rut : $scope.invoice[0].rut,
                    phone : $scope.invoice[0].phone,
                    city : $scope.invoice[0].city,
                    address : $scope.invoice[0].address,
                    transportationName : $scope.invoice[0].transportationName

                  }
                  if($scope.invoice[0].receiver == "undefined" ){
                    $scope.observation = ""

                  }else{
                    $scope.observation = "Recepciona:"+$scope.invoice[0].receiver

                  }
              console.log('boaita',$scope.invoiceDetails);    
                            
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

    	function cancel() {

		 window.close();
         
		}


        function print() {
              window.print();

        }


    }

  }

  printPackOffController.$inject = ['$scope', '$filter','$rootScope', '$http','clientsInvoicesServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
