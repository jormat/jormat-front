
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> afterSaleCreateController
* # As Controller --> afterSaleCreate														
*/

export default class afterSaleCreateController{

        constructor($scope,UserInfoConstant,$timeout,afterSaleServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal){
            var vm = this;
            vm.createAfterSale = createAfterSale
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.removeItems = removeItems
            vm.getNet = getNet
            vm.showPanel = showPanel
            vm.searchClient = searchClient
            vm.loadItems = loadItems
            $scope.parseInt = parseInt
            $scope.discount = 0
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            loadItems()

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="label bg-accent" ng-click="grid.appScope.afterSaleCreate.loadItem(row.entity)">+</a>' +
                                 '</div>'
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.afterSaleCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '44%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '39%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  
                    }
                ]
            };
            
            function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            $scope.invoiceItems = []

            function loadItem(row){
                 $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    price: row.netPrice,
                    minPrice: row.netPrice,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                });

                 $scope.warningItems = false    
            }

            function getNet() {
                var net = 0
                  var i = 0
                     for (i=0; i<$scope.invoiceItems.length; i++) {
                         var valueItems = $scope.invoiceItems[i] 
                         net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 

                      }

                      $scope.NET =  net
                      $scope.netoInvoice = net - (($scope.discount*net)/100) 
                      $scope.total = Math.round($scope.netoInvoice * 1.19)
                      $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                      return net;
                      
            }


            function removeItems(index) {

                $scope.invoiceItems.splice(index, 1);
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                $scope.warehouseId = warehouseId
            }

            function showPanel() {
                $scope.panelInvoice = true
            }

            function setName(clients) {
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedRut = clients.rut
            }

            function createAfterSale() {

                        let model = {
                            userId: $scope.userInfo.id,
                            documentId: $scope.documentId,
                            clientId : $scope.clientId,
                            originId : $scope.warehouseId,
                            type: Number($scope.type),
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            comment : $scope.observation,
                            itemsInvoices : $scope.invoiceItems 
                        }      

                        console.log('modelo',model)

                        afterSaleServices.afterSale.createAfterSale(model).$promise.then((dataReturn) => {
                                ngNotify.set('Se ha creado el documento de manera correcta','success')
                                    $state.go('app.afterSale')
                                  },(err) => {
                                      ngNotify.set('Error al crear documento ','error')
                                  })    

                        
                    }


            function searchClient (value) {

                let model = {
                    name: value
                }

                clientsServices.clients.getClientsByName(model).$promise.then((dataReturn) => {
                      $scope.clients = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

                return false;
            }
           
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

afterSaleCreateController.$inject = ['$scope','UserInfoConstant','$timeout','afterSaleServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal'];

