/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> afterSaleViewController
* # As Controller --> afterSaleView														
*/


  export default class afterSaleViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,afterSaleServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.validateNote = validateNote
        $scope.afterSaleId = $scope.afterData.afterSaleId
        $scope.clientName = $scope.afterData.clientName

        //function to show userId

        $scope.UserInfoConstant = UserInfoConstant
        $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
            if (details !== undefined) {
                $scope.userInfo = details.user
            }
        })
            
        let params = {
                afterSaleId: $scope.afterSaleId
              }

        afterSaleServices.afterSale.getAfterSaleDetails(params).$promise.then((dataReturn) => {
              $scope.afterSale = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getAfterSaleDetails',err);
              })

        afterSaleServices.afterSale.getAfterSaleItems(params).$promise.then((dataReturn) => {
              $scope.afterSaleItems = dataReturn.data
              console.log('that',$scope.afterSaleItems);
              var subTotal = 0
                  var i = 0
                     for (i=0; i<$scope.afterSaleItems.length; i++) {
                         subTotal = subTotal + $scope.afterSaleItems[i].total  
                      }
                      
                      $scope.subTotality =  subTotal
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getAfterSaleItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function validateNote() {
            let model = {
                    creditNoteId : $scope.creditNoteId,
                    userId : $scope.userInfo.id 
                }

                console.log('that',model)

                afterSaleServices.creditNotes.validateNote(model).$promise.then((dataReturn) => {

                    ngNotify.set('Nota de crédito actualizada exitosamente','success');
                    $state.reload("app.creditNotes")
                    $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al validar Nota','error')
                  })
        }

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

		


    }

  }

  afterSaleViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','afterSaleServices','$location','UserInfoConstant','ngNotify','$state'];
