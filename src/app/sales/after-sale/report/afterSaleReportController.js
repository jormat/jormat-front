/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> afterSaleReportController
* # As Controller --> afterSaleReport														
*/
  export default class afterSaleReportController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,reportsItemsServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
	    vm.cancel = cancel
        vm.loadItems = loadItems
        vm.viewItem = viewItem
        vm.afterSaleView= afterSaleView
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
	   
        loadItems()

        i18nService.setCurrentLang('es');

        $scope.itemsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'notas_credito.csv',
                columnDefs: [
                    { 
                      name:'Item',
                      field: 'itemId',  
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.afterSaleReport.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      width: '27%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.itemDescription}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Referencias',
                      field: 'references',  
                      width: '16%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">{{row.entity.references}}</p>' +
                                 '</div>'  
                    },
                    
                    { 
                      name:'Cliente Id',
                      field: 'clientId',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<p tooltip="Código Cliente"><span class="label bg-info text-center" >{{row.entity.clientId}}</span></p>' +
                                 '</div>'  
                    },
                    { 
                      name:'N° Doc.',
                      field: 'documentId',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<a ng-click="grid.appScope.afterSaleReport.afterSaleView(row.entity)"><span tooltip="Código documento post venta" class="badge bg-accent">{{row.entity.documentId}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Precio',
                      field: 'price',
                      width: '9%' ,
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">${{row.entity.price}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Un.',
                      field: 'quantityItems',
                      width: '6%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<a><span class="label bg-secundary">{{row.entity.quantityItems}}</span></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Fecha',
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'BOD',
                      field: 'origin',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary">{{row.entity.origin}}</p>' +
                                 '</div>'  
                    }
                ]
            }


        function loadItems(){

            reportsItemsServices.reports.getFaultyItems().$promise.then((dataReturn) => {
              $scope.itemsGrid.data = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

        function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }


        function afterSaleView(row){
                $scope.afterData = row;
                var modalInstance  = $modal.open({
                        template: require('../view/after-sale-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'afterSaleViewController',
                        controllerAs: 'afterSaleView',
                        size: 'lg'
                })
              }

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}



    }

  }

  afterSaleReportController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','reportsItemsServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
