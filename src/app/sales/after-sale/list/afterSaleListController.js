
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> afterSaleListController
* # As Controller --> afterSaleList														
*/

export default class afterSaleListController{

        constructor($scope,UserInfoConstant,$timeout,afterSaleServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,$window,LOCAL_ENV_CONSTANT,i18nService){
            var vm = this
            vm.viewDocument = viewDocument 
            vm.afterSaleView = afterSaleView
            vm.viewNotePayments = viewNotePayments
            vm.deleteCreditNote = deleteCreditNote
            vm.searchData=searchData
            vm.createXML =createXML
            vm.loadNotes = loadNotes
            vm.afterSaleReport = afterSaleReport
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            function afterSaleReport(row){
                $scope.documentData = row;
                var modalInstance  = $modal.open({
                        template: require('../report/after-sale-report.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'afterSaleReportController',
                        controllerAs: 'afterSaleReport',
                        size: 'lg'
                })
           }


            loadNotes()
            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.afterSaleGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'post_venta.csv',
                showColumnFooter: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'afterSaleId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.afterSaleList.afterSaleView(row.entity)">{{row.entity.afterSaleId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '8%',
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> <strong> ${{row.entity.total}}</strong> </p>' +
                                 '</div>'
                    },
                    { 
                      name:'Documento',
                      field: 'documentId',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents text-center ">'+
                                 '<a ng-click="grid.appScope.afterSaleList.viewDocument(row.entity)"> <p class="badge bg-warning">{{row.entity.documentId}}</p></a>' +
                                 '</div>'
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-secondary font-italic" style="font-size: 10px;">{{row.entity.type}}</p>' +
                                 '</div>'   
                    },
                    
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '10%'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    
                    // { 
                    //   name: 'Estado', 
                    //   field: 'statusName', 
                    //   width: '9%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="{{(row.entity.statusName == \'Reportado\')?\'badge bg-accent\':\'badge bg-danger\'}}">{{row.entity.statusName}}</p>' +
                    //              '</div>'
                    // },
                    
                    { 
                      name: 'Usuario', 
                      field: 'userCreation', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-info font-italic" style="font-size: 10px;">{{row.entity.userCreation}}</p>' +
                                 '</div>'   
                    },
                    
                    { 
                      name: '',
                      width: '7%', 
                      enableFiltering: false,
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadNotes(){

            afterSaleServices.afterSale.getAfterSale().$promise.then((dataReturn) => {
              $scope.afterSaleGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }


            function searchData() {
              afterSaleServices.afterSale.getAfterSale().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.afterSaleGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function afterSaleView(row){
	            $scope.afterData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/after-sale-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'afterSaleViewController',
	                    controllerAs: 'afterSaleView',
	                    size: 'lg'
	            })
	          }

             function viewDocument(row){
                $scope.documentData = row;

                if ($scope.documentData.type == "Factura") {

                    $scope.invoiceData = {
                        invoiceId:$scope.documentData.documentId
                    }
                    console.log('ver factura');
                    var modalInstance  = $modal.open({
                            template: require('../../clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if ($scope.documentData.type == "Documento NN") { 

                    $scope.invoiceData = {
                        documentId:$scope.documentData.documentId 
                    }
                    console.log('ver documento nn');
                    var modalInstance  = $modal.open({
                            template: require('../../documents-nn/view/documents-nn-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'documentsNnViewController',
                            controllerAs: 'documentsNnView',
                            size: 'lg'
                    })

                }

                if ($scope.documentData.type == "Boleta") { 

                    $scope.ballotsData = {
                        ballotId:$scope.documentData.documentId 
                    }
                    console.log('Boleta');
                    var modalInstance  = $modal.open({
                            template: require('../../ballots/view/ballots-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }

            }

            function createXML(row) { 
              // $window.open('http://localhost/jormat-system/XML_note.php?noteId='+row.creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
              $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_note.php?noteId='+row.creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
 
            }

            function viewNotePayments(row){

              console.log('row',row)
              
              $state.go("app.paymentNotesView",{ 
                    noteId: row.creditNoteId,
                    idClient: row.clientId,
                    clientName: row.clientName,
                    total: row.total,
                    date: row.date
                });

            }


            function deleteCreditNote(row){
              $scope.creditNoteData = row;
              // var modalInstance  = $modal.open({
              //         template: require('../delete/credit-notes-delete.html'),
              //         animation: true,
              //         scope: $scope,
              //         controller: 'creditNotesDeleteController',
              //         controllerAs: 'creditNotesDelete'
              // })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

afterSaleListController.$inject = ['$scope','UserInfoConstant','$timeout','afterSaleServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','$window','LOCAL_ENV_CONSTANT','i18nService'];

