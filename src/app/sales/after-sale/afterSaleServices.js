/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> afterSaleServices
* file for call at services for after Sale
*/

class afterSaleServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var afterSale = $resource(SERVICE_URL_CONSTANT.jormat + '/documents/after-sale/:route',{},{
      
        getAfterSale: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getAfterSaleDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        getAfterSaleItems: {
            method:'GET',
            params : {
                route: 'items'
            }
        },
        createAfterSale: {
            method:'POST',
            params : {
                route: ''
            }
        },
        validateAfterSale: {
            method:'PUT',
            params : {
                route: 'validate'
            }
        },

        createAfterSaleFailure: {
            method:'POST',
            params : {
                route: 'failure'
            }
        }
    })

    return { afterSale : afterSale
             
           }
  }
}

  afterSaleServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.afterSaleServices', [])
  .service('afterSaleServices', afterSaleServices)
  .name;