/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> creditNotesServices
* file for call at services for Credit Notes
*/

class creditNotesServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var creditNotes = $resource(SERVICE_URL_CONSTANT.jormat + '/notes/credit/:route',{},{
      
        getCreditNotes: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getCreditNotesDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createCreditNotes: {
            method:'POST',
            params : {
                route: ''
            }
        },
        validateNote: {
            method:'PUT',
            params : {
                route: 'validate'
            }
        },

        createCreditNotesFailure: {
            method:'POST',
            params : {
                route: 'failure'
            }
        },
        disabledCreditNotes: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        getCreditNotesItems: {
            method:'GET',
            params : {
                route: 'items'
            }
        },
        getReportNotes: {
            method:'GET',
            params : {
                route: 'report'
            }
        }
    })

    var invoices = $resource(SERVICE_URL_CONSTANT.jormat + '/invoices/:route',{},{
        getListInvoices: {
            method:'GET',
            params : {
                route: 'list'
            }
        }
    })

    return { 
        creditNotes : creditNotes,
        invoices : invoices
    }
  }
}

  creditNotesServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.creditNotesServices', [])
  .service('creditNotesServices', creditNotesServices)
  .name;