
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> paymentNotesViewController
* # As Controller --> paymentNotesView														
*/

export default class paymentNotesViewController{

        constructor($scope,UserInfoConstant,$timeout,paymentsServices,clientsServices,clientsInvoicesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams,warehousesServices,itemsServices,$modal){
            var vm = this
            vm.noteView = noteView 
            vm.invoiceView = invoiceView
            vm.loadInvoice = loadInvoice
            vm.deletePayment = deletePayment
            vm.createPayment = createPayment
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.payment = 0

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user,
                        $scope.userId = $scope.userInfo.id,
                        getPayments($scope.userId)
                    }
                })
            //

            $scope.noteId = $stateParams.noteId
            $scope.clientId = $stateParams.idClient
            $scope.clientName = $stateParams.clientName
            $scope.total = $stateParams.total
            $scope.date = $stateParams.date
            $scope.status = $stateParams.status

            if ($scope.pending == 0) {
                $scope.statusPayment = 'Aplicada'
                
            }else{
                $scope.statusPayment = 'No Aplicada'
            }

        
            function getPayments(userId){
                let params = {
                    noteId: $stateParams.noteId,
                    userId: userId
                }

                console.log('params',params)
                paymentsServices.payments.getNotePayments(params).$promise.then((dataReturn) => {
                  $scope.paymentsGrid = dataReturn.data;
                  console.log('taht',$scope.paymentsGrid )
                  var sumPayments = 0
                  var i = 0
                     for (i=0; i<$scope.paymentsGrid.length; i++) {
                         sumPayments = sumPayments + $scope.paymentsGrid[i].total  
                      }

                      $scope.sum =  sumPayments
                      $scope.pending = $stateParams.total - $scope.sum
                      if ($scope.pending == 0) {
                            $scope.statusPayment = 'Aplicada'
                            paymentsServices.payments.updateNotesPayment(params).$promise.then((dataReturn) => {
                                ngNotify.set('Esta nota de credito ha sido aplicada completamente','warn')
                            },(err) => {
                                console.log('No se ha podido conectar con el servicio para actualizar nota',err);
                            })
                        }else{
                            $scope.statusPayment = 'No Aplicada'
                        }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err)
                  })

            }
            
            function deletePayment(paymentId,total){
                  $scope.paymentsId = paymentId;
                  $scope.total = total;
                  $scope.noteId = $stateParams.noteId
                  var modalInstance  = $modal.open({
                          template: require('../pay-delete/payments-note-delete.html'),
                          animation: true,
                          scope: $scope,
                          controller: 'paymentsNoteDeleteController',
                          controllerAs: 'paymentsNoteDelete'
                  })
            } 

            function noteView(noteId,clientName){
                $scope.creditNoteData = {
                    creditNoteId:noteId,
                    clientName:clientName
                }

                var modalInstance  = $modal.open({
                        template: require('../view/credit-notes-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'creditNotesViewController',
                        controllerAs: 'creditNotesView',
                        size: 'lg'
                })
            }

            function invoiceView(items){

                let model = {
                    invoiceId: items.document
                }

                clientsInvoicesServices.invoices.getInvoicesDetails(model).$promise.then((dataReturn) => {
                        $scope.invoiceDetails = dataReturn.data;
                        console.log('details invoices',$scope.invoiceDetails[0].clientName)
                        loadInvoice($scope.invoiceDetails[0].clientName,model.invoiceId)
                      },(err) => {
                        console.log('No se ha podido cargar el clientName',err)
                     })   
            }


            function loadInvoice(clientName,invoiceId){

                $scope.invoiceData = {
                    invoiceId:invoiceId,
                    clientName:clientName
                }

                var modalInstance  = $modal.open({
                        template: require('../../clients-invoices/view/clients-invoices-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'clientsInvoicesViewController',
                        controllerAs: 'clientsInvoicesView',
                        size: 'lg'
                })
            }

            function createPayment() {

                    let model = {
                        userId: $scope.userInfo.id,
                        noteId: $stateParams.noteId,
                        total : $scope.payment,
                        numberDocument : $scope.document,
                        comment : $scope.observation
                    }

                    console.log('model',model)  

                     paymentsServices.payments.createNotesPayment(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado el abono correctamente','success')
                        getPayments(model.userId)
                        $scope.observation = ''
                        $scope.payment = ''
                        $scope.document = ''
                      },(err) => {
                        ngNotify.set('Error al ingresar abono','error')
                     })
                }
            
        }//FIN CONSTRUCTOR

        // Funciones
        
        
    }   

paymentNotesViewController.$inject = ['$scope','UserInfoConstant','$timeout','paymentsServices','clientsServices','clientsInvoicesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams','warehousesServices','itemsServices','$modal'];

