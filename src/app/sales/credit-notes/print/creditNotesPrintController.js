/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> creditNotesPrintController
* # As Controller --> creditNotesPrint														
*/


   export default class creditNotesPrintController {

    constructor($scope, $filter,$rootScope, $http,creditNotesServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

        var vm = this
        vm.print=print
        vm.validateNote = validateNote
        $scope.creditNoteId = $stateParams.creditNoteId
        $scope.clientName = $stateParams.clientName

        //function to show userId

        $scope.UserInfoConstant = UserInfoConstant
        $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
            if (details !== undefined) {
                $scope.userInfo = details.user
            }
        })
            
        let params = {
                creditNoteId: $scope.creditNoteId
              }

        creditNotesServices.creditNotes.getCreditNotesDetails(params).$promise.then((dataReturn) => {
              $scope.creditNotes = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getCreditNotesDetails',err);
              })

        creditNotesServices.creditNotes.getCreditNotesItems(params).$promise.then((dataReturn) => {
              $scope.creditNotesItems = dataReturn.data

              var subTotal = 0
                var i = 0
                   for (i=0; i<$scope.creditNotesItems.length; i++) {
                       subTotal = subTotal + $scope.creditNotesItems[i].total  
                    }
                    
                    $scope.subTotality =  subTotal
                    
              console.log('that',$scope.creditNotesItems);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getCreditNotesItems',err);
              })

      function print() {
              window.print();

        }


        function validateNote() {
            let model = {
                    creditNoteId : $scope.creditNoteId,
                    userId : $scope.userInfo.id 
                }

                console.log('that',model)

                creditNotesServices.creditNotes.validateNote(model).$promise.then((dataReturn) => {

                    ngNotify.set('Nota de crédito actualizada exitosamente','success');
                    $state.reload("app.creditNotes")
                    $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al validar Nota','error')
                  })
        }

    }

  }

  creditNotesPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','creditNotesServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
