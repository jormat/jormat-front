/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> creditNotesDeleteController
* # As Controller --> creditNotesDelete														
*/


  export default class creditNotesDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,creditNotesServices,$location,UserInfoConstant,ngNotify) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.deleteCreditNote = deleteCreditNote
	    $scope.creditNoteId = $scope.creditNoteData.creditNoteId
	    // $scope.rut = $scope.creditNoteData.rut
	    $scope.clientName = $scope.creditNoteData.clientName

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteCreditNote() {
			let model = {
                    creditNoteId : $scope.creditNoteId,
                    userId : $scope.userInfo.id
                }

                creditNotesServices.creditNotes.disabledCreditNotes(model).$promise.then((dataReturn) => {

                    ngNotify.set('Nota de crédito eliminada exitosamente','success');
					         $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al eliminar nota','error')
                  })
		}
	    


    }

  }

  creditNotesDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','creditNotesServices','$location','UserInfoConstant','ngNotify'];
