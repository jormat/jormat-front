/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentsNoteDeleteController
* # As Controller --> paymentsNoteDelete														
*/


	export default class paymentsNoteDeleteController {

		constructor($scope, $filter,$rootScope, $http,$modalInstance,paymentsServices,$location,UserInfoConstant,ngNotify,$state) {

			var vm = this;
			vm.cancel = cancel;
			vm.deletePayment = deletePayment
			$scope.paymentsId = $scope.paymentsId
			$scope.noteId = $scope.noteId
			$scope.total = $scope.total;

			//function to show userId

				$scope.UserInfoConstant = UserInfoConstant
				$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
					if (details !== undefined) {
							$scope.userInfo = details.user
					}
				})
				
				//

			function cancel() {

			 $modalInstance.dismiss('chao');

		    }

		    function deletePayment() {
			    let model = {
						paymentId : $scope.paymentsId,
						noteId : $scope.noteId,
						userId : $scope.userInfo.id
				    }

				paymentsServices.notesPayments.disabledNotesPayment(model).$promise.then((dataReturn) => {
					ngNotify.set('Abono/pago eliminado exitosamente','success');
					$state.reload("app.paymentNotesView")
				    $modalInstance.dismiss('chao');
				},(err) => {
					ngNotify.set('Error al eliminar abono','error')
				})
		}
			
		}

	}

	paymentsNoteDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','paymentsServices','$location','UserInfoConstant','ngNotify','$state'];
