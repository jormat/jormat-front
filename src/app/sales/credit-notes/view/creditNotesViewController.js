/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> creditNotesViewController
* # As Controller --> creditNotesView														
*/


  export default class creditNotesViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,creditNotesServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.validateNote = validateNote
        $scope.creditNoteId = $scope.creditNoteData.creditNoteId
        $scope.clientName = $scope.creditNoteData.clientName

        //function to show userId

        $scope.UserInfoConstant = UserInfoConstant
        $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
            if (details !== undefined) {
                $scope.userInfo = details.user
            }
        })
            
        let params = {
                creditNoteId: $scope.creditNoteId
              }

        creditNotesServices.creditNotes.getCreditNotesDetails(params).$promise.then((dataReturn) => {
              $scope.creditNotes = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getCreditNotesDetails',err);
              })

        creditNotesServices.creditNotes.getCreditNotesItems(params).$promise.then((dataReturn) => {
              $scope.creditNotesItems = dataReturn.data

              var subTotal = 0
                var i = 0
                   for (i=0; i<$scope.creditNotesItems.length; i++) {
                       subTotal = subTotal + $scope.creditNotesItems[i].total  
                    }
                    
                    $scope.subTotality =  subTotal
                    
              console.log('that',$scope.creditNotesItems);
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getCreditNotesItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function validateNote() {
            let model = {
                    creditNoteId : $scope.creditNoteId,
                    userId : $scope.userInfo.id 
                }

                console.log('that',model)

                creditNotesServices.creditNotes.validateNote(model).$promise.then((dataReturn) => {

                    ngNotify.set('Nota de crédito actualizada exitosamente','success');
                    $state.reload("app.creditNotes")
                    $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al validar Nota','error')
                  })
        }

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

		


    }

  }

  creditNotesViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','creditNotesServices','$location','UserInfoConstant','ngNotify','$state'];
