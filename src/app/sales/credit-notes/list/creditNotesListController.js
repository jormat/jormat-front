
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> creditNotesListController
* # As Controller --> creditNotesList														
*/

export default class creditNotesListController{

        constructor($scope,UserInfoConstant,$timeout,creditNotesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,$window,LOCAL_ENV_CONSTANT,i18nService){
            var vm = this
            vm.viewInvoice = viewInvoice 
            vm.viewCreditNote = viewCreditNote
            vm.viewNotePayments = viewNotePayments
            vm.deleteCreditNote = deleteCreditNote
            vm.searchData=searchData
            vm.createXML =createXML
            vm.loadNotes = loadNotes
            vm.notesReport = notesReport
            vm.creditNotesPrint = creditNotesPrint
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            function notesReport(row){
                console.log('maria juana')
                // $scope.invoiceData = row;
                var modalInstance  = $modal.open({
                        template: require('../report/notes-report.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'notesReportController',
                        controllerAs: 'notesReport',
                        size: 'lg'
                })
           }


            loadNotes()
            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.creditNotesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'notas_credito.csv',
                showColumnFooter: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'creditNoteId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.creditNotesList.viewCreditNote(row.entity)">{{row.entity.creditNoteId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '26%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Rut',
                      field: 'rut',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p>{{row.entity.rut}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Factura',
                      field: 'invoiceId',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.creditNotesList.viewInvoice(row.entity)"> <p class="badge bg-accent">{{row.entity.invoiceId}}</p></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '8%',
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> <strong> ${{ row.entity.total | pesosChilenos }}</strong></p>' +
                                 '</div>'
                    },

                    { 
                      name:'Pendiente',
                      field: 'pending',
                      width: '8%',
                      aggregationHideLabel: false,
                      // aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{ row.entity.pending | pesosChilenos }}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '9%'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-secondary font-italic" style="font-size: 10px;">{{row.entity.type}}</p>' +
                                 '</div>'   
                    },
                    
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="{{(row.entity.statusName == \'Aplicada\')?\'badge bg-warning\':\'badge bg-danger\'}}" style="font-size: 10px;">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    
                    // { 
                    //   name: 'Usuario', 
                    //   field: 'userCreation', 
                    //   width: '10%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class=" badge bg-secondary font-italic" style="font-size: 12px;">{{row.entity.userCreation}}</p>' +
                    //              '</div>'   
                    // },
                    
                    { 
                      name: '',
                      width: '6%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadNotes(){

            creditNotesServices.creditNotes.getCreditNotes().$promise.then((dataReturn) => {
              $scope.creditNotesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }


            function searchData() {
              creditNotesServices.creditNotes.getCreditNotes().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.creditNotesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewCreditNote(row){
	            $scope.creditNoteData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/credit-notes-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'creditNotesViewController',
	                    controllerAs: 'creditNotesView',
	                    size: 'lg'
	            })
	          }

            function viewInvoice(row){
                $scope.invoiceData = row
                var modalInstance  = $modal.open({
                        template: require('../../clients-invoices/view/clients-invoices-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'clientsInvoicesViewController',
                        controllerAs: 'clientsInvoicesView',
                        size: 'lg'
                })
            }

            function createXML(row) { 
              // $window.open('http://localhost/jormat-system/XML_note.php?noteId='+row.creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
              $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_note.php?noteId='+row.creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
 
            }

            function viewNotePayments(row){

              console.log('row',row)
              
              $state.go("app.paymentNotesView",{ 
                    noteId: row.creditNoteId,
                    idClient: row.clientId,
                    clientName: row.clientName,
                    total: row.total,
                    date: row.date
                });

            }

            function creditNotesPrint(row){

              console.log('row',row)
              
              $state.go("app.creditNotesPrint",{ 
                    creditNoteId: row.creditNoteId,
                    clientName: row.clientName
                });

            }


            function deleteCreditNote(row){
              $scope.creditNoteData = row;
              var modalInstance  = $modal.open({
                      template: require('../delete/credit-notes-delete.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'creditNotesDeleteController',
                      controllerAs: 'creditNotesDelete'
              })
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

creditNotesListController.$inject = ['$scope','UserInfoConstant','$timeout','creditNotesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','$window','LOCAL_ENV_CONSTANT','i18nService'];

