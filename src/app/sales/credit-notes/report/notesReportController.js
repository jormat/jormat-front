/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> notesReportController
* # As Controller --> notesReport														
*/
  export default class notesReportController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,creditNotesServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
	    vm.cancel = cancel
        vm.loadInvoices = loadInvoices
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
	   
        // loadInvoices()

        i18nService.setCurrentLang('es');

        $scope.invoicesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'notas_credito_desde_'+moment($scope.startDate).format("YYYY-MM-DD")+'.csv',
                exporterPdfHeader: { 
                    text: "Notas de credito emitidas entre los días : " + moment($scope.startDate).format("YYYY-MM-DD") +" y "+ moment($scope.endDate).format("YYYY-MM-DD"),
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [0,20,0,0] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },
                columnDefs: [
                    { 
                      name:'id',
                      field: 'creditNoteId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.creditNotesList.viewCreditNote(row.entity)">{{row.entity.creditNoteId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '33%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Factura',
                      field: 'invoiceId',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.creditNotesList.viewInvoice(row.entity)"> <p class="badge bg-accent">{{row.entity.invoiceId}}</p></a>' +
                                 '</div>'
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '12%',
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                   '<p> ${{row.entity.total}}</p>' +
                                   '</div>'
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '11%'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Tipo', 
                      field: 'type', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-secondary font-italic" style="font-size: 10px;">{{row.entity.type}}</p>' +
                                 '</div>'   
                    },
                    
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 9px;" class="{{(row.entity.statusName == \'Aplicada\')?\'badge bg-warning\':\'badge bg-danger\'}}">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    
                    // { 
                    //   name: 'Usuario', 
                    //   field: 'userCreation', 
                    //   width: '10%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class=" badge bg-secondary font-italic" style="font-size: 12px;">{{row.entity.userCreation}}</p>' +
                    //              '</div>'   
                    // },
                    
                   
                ]
            }


        function loadInvoices(){

            let model = {
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            creditNotesServices.creditNotes.getReportNotes(model).$promise.then((dataReturn) => {
              $scope.invoicesGrid.data = dataReturn.data

              if ($scope.invoicesGrid.data.length == 0){
                      $scope.messageInvoices = true
                  }else{
                      $scope.messageInvoices = false
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}



    }

  }

  notesReportController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','creditNotesServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
