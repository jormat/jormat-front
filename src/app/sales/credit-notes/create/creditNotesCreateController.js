
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> creditNotesCreateController
* # As Controller --> creditNotesCreate														
*/

export default class creditNotesCreateController{

				constructor($scope,UserInfoConstant,$timeout,creditNotesServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$window,LOCAL_ENV_CONSTANT,clientsInvoicesServices){
						var vm = this;
						vm.createCreditNote = createCreditNote
						vm.setName = setName
						vm.createXML = createXML
						vm.setInvoice = setInvoice
						vm.setWarehouseId = setWarehouseId
						vm.viewItem = viewItem
						vm.loadItem = loadItem
						vm.removeItems = removeItems
						vm.getNet = getNet
						vm.showPanel = showPanel
						vm.searchClient = searchClient
						vm.itemsPush = itemsPush
						vm.loadItems = loadItems
						vm.webItemUpdate = webItemUpdate
                        vm.noteCreate = noteCreate
						$scope.parseInt = parseInt
						$scope.discount = 0
						$scope.warningItems = true
						$scope.currencySymbol = '$'
						$scope.date = new Date()
						$scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
						$scope.priceVATest = 56000
						$scope.editables = {
										disccount: '0',
										quantity: '1'
								}
                        $scope.historyItems = []
						vm.searchData = searchData

						function searchData() {
							itemsServices.items.getItems().$promise
							.then(function(data){
								$scope.data = data.data;
								$scope.itemsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
							});
						}

						//function to show userId
								$scope.UserInfoConstant = UserInfoConstant
								$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
										if (details !== undefined) {
												$scope.userInfo = details.user
												let model = {
														userId : $scope.userInfo.id
												}
												warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
														$scope.warehouses = dataReturn.data;
														console.log('$scope.warehouses',$scope.warehouses)
														},(err) => {
														console.log('No se ha podido conectar con el servicio',err);
												})
										}
								})
						//

						loadItems()

						$scope.itemsGrid = {
								enableFiltering: true,
								enableHorizontalScrollbar :0,
								columnDefs: [
										{ 
											name: '',
											field: 'href',
											enableFiltering: false,
											width: '5%', 
											cellTemplate:'<div class="ui-grid-cell-contents ">'+
																 '<a class="label bg-accent" ng-click="grid.appScope.creditNotesCreate.loadItem(row.entity)">+</a>' +
																 '</div>'
										},
										{ 
											name:'id',
											field: 'itemId', 
											enableFiltering: true,
											width: '10%',
											cellTemplate:'<div class="ui-grid-cell-contents ">'+
																 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.creditNotesCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
																 '</div>' 
										},
										{ 
											name:'Descripción',
											field: 'itemDescription',
											enableFiltering: true,
											width: '44%',
											cellTemplate:'<div class="ui-grid-cell-contents ">'+
																 '<p class="uppercase" style="font-size: 11px;" >{{row.entity.itemDescription}}</p>' +
																 '</div>' 
										},
										{ 
											name:'Referencias',
											field: 'references',
											enableFiltering: true,
											width: '39%',
											cellTemplate:'<div class="ui-grid-cell-contents ">'+
																 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
																 '</div>'  
										}
								]
						};
						
						function loadItems(){
								itemsServices.items.getItems().$promise.then((dataReturn) => {
										$scope.itemsGrid.data = dataReturn.data;
								},(err) => {
										console.log('No se ha podido conectar con el servicio',err);
								})
						}

						$scope.invoiceItems = []

						function loadItem(row){
							var i = 0
			                  for (i=0; i<$scope.invoiceItems.length; i++) {
			                        var id = $scope.invoiceItems[i].itemId

			                        if(id == row.itemId){
			                          ngNotify.set('Item '+ row.itemId +' Duplicado en este Documento','warn')
			                          $scope.invoiceItems.splice(index, 1)

			                        }else{
			                          console.log("item no puplicado",row.itemId)
			                        }
			                        
			                      }
			                      
								 $scope.invoiceItems.push({
										itemId: row.itemId,
										itemDescription: row.itemDescription,
										price: row.netPrice,
										minPrice: row.netPrice,
										webType: row.webType,
										maxDiscount : row.maxDiscount,
										quantity: 1,
										disscount: 0

								});

								 $scope.warningItems = false    
						}

						function getNet() {
								var net = 0
									var i = 0
										 for (i=0; i<$scope.invoiceItems.length; i++) {
												 var valueItems = $scope.invoiceItems[i] 
												 net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 

											}

											$scope.NET =  net
											$scope.netoInvoice = net - (($scope.discount*net)/100) 
											$scope.total = Math.round($scope.netoInvoice * 1.19)
											$scope.VAT = Math.round($scope.total - $scope.netoInvoice)
											return net;
											
						}


						function removeItems(index) {

								$scope.invoiceItems.splice(index, 1);
						}


						function viewItem(row){
								$scope.itemsData = row;
								var modalInstance  = $modal.open({
												template: require('../../../items/items/view/items-view.html'),
												animation: true,
												scope: $scope,
												controller: 'itemsViewController',
												controllerAs: 'itemsView',
												size: 'lg'
								})
						}

						function setWarehouseId(warehouseId){
								$scope.warehouseId = warehouseId
						}

						function showPanel() {
								$scope.panelInvoice = true
						}

						function setName(clients) {
								$scope.clientName = clients.fullName
								$scope.clientId = clients.clientId
								$scope.selectedRut = clients.rut
								getListInvoice()
						}

						function setInvoice(fac) {
							 $scope.invoiceId = fac.invoiceId
						}

						function itemsPush(fac) {
							 $scope.invoiceId = fac.invoiceId
							 $scope.invoiceItems = []

							 let params = {
										invoiceId: $scope.invoiceId,
										originId: 1
									}

								clientsInvoicesServices.invoices.getInvoicesItems(params).$promise.then((dataReturn) => {
											$scope.itemsInvoicesClone = dataReturn.data
											for(var i in $scope.itemsInvoicesClone){
														$scope.invoiceItems.push({
																itemId: $scope.itemsInvoicesClone[i].itemId,
																itemDescription: $scope.itemsInvoicesClone[i].itemDescription,
																price: $scope.itemsInvoicesClone[i].price,
																minPrice: $scope.itemsInvoicesClone[i].price,
																oil: $scope.itemsInvoicesClone[i].oil,
																netPurchaseValue: $scope.itemsInvoicesClone[i].netPurchaseValue,
																webType: $scope.itemsInvoicesClone[i].webType,
																maxDiscount : $scope.itemsInvoicesClone[i].discount,
																quantity: $scope.itemsInvoicesClone[i].quantityItems,
																disscount: $scope.itemsInvoicesClone[i].discount
												 })

														$scope.warningItems = false   
														

										}

										},(err) => {
													console.log('No se ha podido conectar con el servicio get items Invoices',err);
										})
						}

						function getListInvoice(){
								let param = {
										dsCode: $scope.selectedRut
								}

								creditNotesServices.invoices.getListInvoices(param).$promise.then((dataReturn) => {
										$scope.listInvoice = dataReturn.data;
								},(err) => {
										console.log('No se ha podido conectar con el servicio',err);
								})
								
						}

						function createCreditNote() {

							let model = {
									userId: $scope.userInfo.id,
									invoiceId: $scope.invoiceId,
									clientId : $scope.clientId,
									originId : $scope.warehouseId,
									type: Number($scope.type),
									priceVAT : $scope.VAT,
									netPrice : $scope.NET,
									discount : $scope.discount,
									total : $scope.total,
									comment : $scope.observation,
									itemsInvoices : $scope.invoiceItems 
							}

                            for(var  i in model.itemsInvoices){
                              let modelo = {
                                  itemId: model.itemsInvoices[i].itemId,
                                  originId :$scope.warehouseId,
                                  quantity: model.itemsInvoices[i].quantity
                               }
              
                                  itemsServices.items.getHistoric(modelo).$promise.then((dataReturn) => {
                                     $scope.item = dataReturn.data;
          
                                      $scope.historyItems.push({
                                      itemId: $scope.item[0].itemId,
                                      itemDescription: $scope.item[0].itemDescription,
                                      location : $scope.item[0].locations,
                                      previousStock: $scope.item[0].stock,
                                      price: $scope.item[0].vatPrice,
                                      quantity: modelo.quantity,
                                      generalStock: $scope.item[0].generalStock,
                                      currentprice: $scope.item[0].vatPrice,
                                      currentLocation: $scope.item[0].locations
                                      })
                                          
                                      console.log('that', $scope.historyItems);
          
                                      if (i== $scope.historyItems.length-1) {
                                          noteCreate(model,$scope.historyItems)
                                          $scope.ultimo = "último registro";
                                        } else {
                                          $scope.ultimo = "item"+i;
                                        }
                                          console.log( $scope.ultimo); 
                                         },(err) => {
                                          console.log('No se ha podido conectar con el servicio',err);
                                      })
                          }					
				        }
                        
                        function noteCreate(model,historyItems){

                              if(model.type == 1) {
                                  creditNotesServices.creditNotes.createCreditNotes(model).$promise.then((dataReturn) => {
                                       ngNotify.set('Se ha creado la nota de crédito correctamente','success')
                                          $scope.result = dataReturn.data
                                          console.log('result',$scope.result)
                                          createXML($scope.result)
                                          $state.go('app.creditNotes')

                                          for(var  i in historyItems){

                                                $scope.item = dataReturn.data;
                          
                                                let itemsValue = {
                                                  documentId: $scope.result,
                                                  itemId: historyItems[i].itemId,
                                                  itemDescription: historyItems[i].itemDescription,
                                                  location : historyItems[i].location,
                                                  previousStock: historyItems[i].previousStock,
                                                  price: historyItems[i].price,
                                                  quantity: historyItems[i].quantity,
                                                  originId: $scope.warehouseId,
                                                  userId: $scope.userInfo.id,
                                                  document: "NC",
                                                  type: "Entrada +",
                                                  generalStock: historyItems[i].generalStock,
                                                  currentprice: historyItems[i].price,
                                                  currentLocation: historyItems[i].currentLocation,
                                                  destiny: undefined
                          
                                                }
                                               
                                               console.log('that',itemsValue);
                                 
                                               itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                                                      $scope.result2 = dataReturn.data;
                                                      console.log('Historial del item actualizado correctamente');
                                                  },(err) => {
                                                      console.log('No se ha podido conectar con el servicio',err);
                                                  })
                                            }

                                    for(var  i in model.itemsInvoices){
                                         console.log('entra',model.itemsInvoices)

                                      if (model.itemsInvoices[i].webType == "SI"){
                                          console.log('tipo web')
                                          let modelo = {
                                              itemId : model.itemsInvoices[i].itemId
                                          }
                                          itemsServices.items.getGeneralStockWeb(modelo).$promise.then((dataReturn) => {
                                            $scope.result = dataReturn.data;
                                            $scope.stock = $scope.result[0].generalStock;
                                            webItemUpdate(modelo.itemId,$scope.stock)

                                          },(err) => {
                                            console.log('No se ha podido conectar con el servicio',err);
                                          })
                                        }
                                      else{
                                       console.log('normal')
                                     }
                                    }
                                  },(err) =>{
                                          ngNotify.set('Error al crear nota de credito','error')
                                 })

                              }else{
                                    creditNotesServices.creditNotes.createCreditNotesFailure(model).$promise.then((dataReturn) => {
                                          ngNotify.set('Se ha creado la nota de credito x falla correctamente','success')
                                                $scope.result = dataReturn.data
                                                console.log('result',$scope.result)
                                                createXML($scope.result)
                                                $state.go('app.creditNotes')
                                                },(err) => {
                                                ngNotify.set('Error al crear nota de credito ','error')
                                          })
                            }  

                        }

						function webItemUpdate(itemId,stock) {

                          let data = {
                                    sku : itemId.toString(),
                                    stock_quantity: stock
                                }

                                itemsServices.woocommerceAPI.itemUpdate(data).$promise.then((dataReturn) => {
                                  $scope.result = dataReturn.data;
                                  $scope.message= dataReturn.message;
                                  $scope.status= dataReturn.status;

                                  if ($scope.status==200  ) {

                                     ngNotify.set('Mensaje sitio web: '+ $scope.message,'info')

                                      }else{
                                        console.log('Error actualizar stock web',$scope.message);
                                        ngNotify.set( 'Error actualización ITEM en el sitio web: ' + $scope.message, {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                        })
                                        
                                     }
                                  
                                  },(err) => {
                                        console.log('Error actualizar stock web',err);
                                        ngNotify.set( 'Error API Web', {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                      })
                                  })

                        }

						function createXML(creditNoteId) { 

							// $window.open('http://localhost/jormat-system/XML_note.php?noteId='+creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
							$window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_note.php?noteId='+creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
							ngNotify.set('Documento enviado a SII N° NC ' +creditNoteId+ ' / Validar en Facturas Chile','warn')
 
						}

						function searchClient (value) {

								let model = {
										name: value
								}

								clientsServices.clients.getClientsByName(model).$promise.then((dataReturn) => {
											$scope.clients = dataReturn.data;
									},(err) => {
											console.log('No se ha podido conectar con el servicio',err);
									})

								return false;
						}
					 
				}//FIN CONSTRUCTOR

				// Funciones
				
 

				
		}   

creditNotesCreateController.$inject = ['$scope','UserInfoConstant','$timeout','creditNotesServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$window','LOCAL_ENV_CONSTANT','clientsInvoicesServices'];

