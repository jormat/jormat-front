/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> documentsNnViewController
* # As Controller --> documentsNnView														
*/


export default class documentsNnViewController {
	constructor($scope, $filter,$rootScope, $http,$modalInstance,documentsNnServices,$location,UserInfoConstant,ngNotify,$state,$modal,Dialogs ) {
		this.btnVisible = false;
		this.$scope = $scope;
		this.Dialogs = Dialogs;
		this.$state = $state;
		this.ngNotify = ngNotify
		this.UserInfoConstant = UserInfoConstant[0].details;
		this.$modalInstance = $modalInstance;
		this.$modal = $modal;
		this.documentsNnSrv = documentsNnServices;
		this.$scope.documentId = this.$scope.invoiceData.documentId;
		this.$scope.clientName = this.$scope.invoiceData.clientName;

		this.loadData();
	}

	loadData(){
		let params = {
			documentId: this.$scope.documentId
		}

		this.documentsNnSrv.documents.getDocumentDetails(params).$promise.then((dataReturn) => {
			this.$scope.document = dataReturn.data
			this.btnVisible = (this.$scope.document[0].userOut) ? false : true;
		},(err) => {
			console.log('No se ha podido conectar con el servicio get Document Details',err);
		})
		this.documentsNnSrv.documents.getDocumentItems(params).$promise.then((dataReturn) => {
			this.$scope.documentItems = dataReturn.data
			var subTotal = 0
	              var i = 0
	                 for (i=0; i<this.$scope.documentItems.length; i++) {
	                     subTotal = subTotal + this.$scope.documentItems[i].total  
	                  }
	                  
	                  this.$scope.subTotality =  subTotal
		},(err) => {
			console.log('No se ha podido conectar con el servicio get Invoices Details',err);
		})
	}

	cancel(){
		this.$modalInstance.dismiss('chao');
	}

	payDocument(value){
		// this.$scope.value = value
		// this.$scope.total = this.$scope.document[0].total
		// const modalInstance  = this.$modal.open({
		// 		template: require('../payment/payment-nn.html'),
		// 		animation: true,
		// 		scope: this.$scope,
		// 		controller: 'paymentNnController',
		// 		controllerAs: 'paymentNn'
		// })


		this.$state.go("app.paymentNn", { 
                idDocument: this.$scope.documentId,
                idClient: this.$scope.document[0].clientId,
                clientName: this.$scope.clientName,
                total: this.$scope.document[0].total,
                date: this.$scope.document[0].date


            });
	}

	print(divPrint) {
		const printContents = document.getElementById(divPrint).innerHTML;
		const popupWin = window.open('', '_blank');
		popupWin.document.open();
		popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
		this.$modalInstance.dismiss('chao');
	}

	printPackOff() {
		console.log('imprimir PackOff');
		var url = this.$state.href("app.printPackOffNn",{
			idDocument: this.$scope.documentId
		});

		window.open(url,'_blank',"width=600,height=700");

	}

	updatePackOff() {
		this.params = {
					userId : this.UserInfoConstant[0].user.id,
					documentId : this.$scope.documentId
				}

		console.log('parametros',this.params);
		
		this.documentsNnSrv.documents.updatePackOff(this.params).$promise.then((dataReturn) => {
					this.ngNotify.set('NN a sido actualizada con exito','warn');
					this.loadData();
				},(err) => {
					this.ngNotify.set('Error al actualizar NN','error')
				})
	}

	updateDocument(){
		this.$modalInstance.dismiss('chao');
		this.$scope.isEdit = true;
		
		this.params = {
					userId : this.UserInfoConstant[0].user.id,
					typeDocumentId : this.$scope.document[0].typeId,
					documentId : this.$scope.documentId,
					commentary : "Entrega Rápida",
                    option : 7
				}

				this.documentsNnSrv.documentsnn.setDocumentList(this.params).$promise.then((dataReturn) => {
					this.ngNotify.set('La entrega ha sido registrada exitosamente','warn');
					this.$state.reload('app.warehouseDeliveries')
					this.$modalInstance.dismiss('chao');
				},(err) => {
					this.ngNotify.set('Error al actualizar documento','error')
				})
	}
}
documentsNnViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','documentsNnServices','$location','UserInfoConstant','ngNotify','$state','$modal','Dialogs'];
