/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentsNnDeleteController
* # As Controller --> paymentsNnDelete														
*/


	export default class paymentsNnDeleteController {

		constructor($scope, $filter,$rootScope, $http,$modalInstance,paymentsServices,$location,UserInfoConstant,ngNotify,$state) {

			var vm = this;
			vm.cancel = cancel;
			vm.deletePayment = deletePayment
			$scope.paymentsId = $scope.paymentsId
			$scope.documentId = $scope.documentId
			$scope.total = $scope.total;

			//function to show userId

				$scope.UserInfoConstant = UserInfoConstant
				$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
					if (details !== undefined) {
							$scope.userInfo = details.user
					}
				})
				
				//

			function cancel() {

			 $modalInstance.dismiss('chao');

		    }

		    function deletePayment() {
			    let model = {
						paymentId : $scope.paymentsId,
						documentId : $scope.documentId,
						userId : $scope.userInfo.id
				    }

				    console.log('modelo',model)

				paymentsServices.nnPayments.disabledNnPayment(model).$promise.then((dataReturn) => {
					ngNotify.set('Abono/pago eliminado exitosamente','success');
					$state.reload("app.paymentNn")
				    $modalInstance.dismiss('chao');
				},(err) => {
					ngNotify.set('Error al eliminar abono','error')
				})
		}
			
		}

	}

	paymentsNnDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','paymentsServices','$location','UserInfoConstant','ngNotify','$state'];
