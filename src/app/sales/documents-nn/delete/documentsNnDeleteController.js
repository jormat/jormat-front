/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description asada
* # name controller --> documentsNnDeleteController
* # As Controller --> documentsNnDelete														
*/


  export default class documentsNnDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,documentsNnServices,$location,UserInfoConstant,ngNotify) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.deleteInvoice = deleteInvoice
	    $scope.invoiceId = $scope.invoiceData.invoiceId
	    $scope.rut = $scope.invoiceData.rut
	    $scope.clientName = $scope.invoiceData.clientName

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteInvoice() {
			let model = {
                    invoiceId : $scope.invoiceId,
                    userId : $scope.userInfo.id
                }

                documentsNnServices.invoices.disabledInvoice(model).$promise.then((dataReturn) => {

                    ngNotify.set('Factura eliminada exitosamente','success');
					         $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al eliminar factura','error')
                  })
		}
	    


    }

  }

  documentsNnDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','documentsNnServices','$location','UserInfoConstant','ngNotify'];
