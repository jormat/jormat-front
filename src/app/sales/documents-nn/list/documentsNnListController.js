
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> documentsNnListController
* # As Controller --> documentsNnList														
*/

export default class documentsNnListController{

        constructor($scope,UserInfoConstant,$timeout,documentsNnServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.viewInvoice = viewInvoice
            vm.printDocument= printDocument
            vm.loadDocuments = loadDocuments
            vm.searchData=searchData
            vm.paymentNn = paymentNn
            vm.cloneNn = cloneNn
            vm.updateDocument = updateDocument
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            loadDocuments()

            //function to show userId
            $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                    }
                })
            //
            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.invoicesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'documentos_nn.csv',
                showColumnFooter: true,
                columnDefs: [
                    
                    { 
                      name:'Id Cliente',
                      field: 'clientId',
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p><span tooltip="Código cliente" class="badge bg-info">{{row.entity.clientId}}</span></p>' +
                                 '</div>'
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '22%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 12px;" class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'RUT',
                      field: 'rut',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 12px;" class="uppercase">{{row.entity.rut}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Documento',
                      field: 'documentId',  
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.documentsNnList.viewInvoice(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>'  
                    },
                    
                    { 
                      name:'Total',
                      field: 'total',
                      width: '8%',
                      aggregationHideLabel: false,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{ row.entity.total | pesosChilenos }}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Pendiente',
                      field: 'pending',
                      width: '10%',
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.pending | pesosChilenos}}</p>' +
                                 '</div>'        
                    },

                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '9%'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 10px;" class="{{(row.entity.statusName == \'Pagado\')?\'badge bg-warning\':\'badge bg-danger\'}}">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Usuario', 
                      field: 'userCreation', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" badge bg-secondary font-italic" style="font-size: 10px;">{{row.entity.userCreation}}</p>' +
                                 '</div>'   
                    },
                    
                    { 
                      name: '',
                      width: '6%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadDocuments(){

            documentsNnServices.documents.getDocuments().$promise.then((dataReturn) => {
              $scope.invoicesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            function paymentNn(row){

            $state.go("app.paymentNn", { 
                idDocument: row.documentId,
                idClient: row.clientId,
                clientName: row.clientName,
                total: row.total,
                date: row.date


            });

            }

            function cloneNn(row){

                 $state.go("app.documentsNnUpdate", { idQuotation: row.documentId, discount:row.discount,type:'nn'});

            }
            
            function searchData() {
              documentsNnServices.documents.getDocuments().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.invoicesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewInvoice(row){
	            $scope.invoiceData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/documents-nn-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'documentsNnViewController',
	                    controllerAs: 'documentsNnView',
	                    size: 'lg'
	            })
	          }

            function printDocument(row){
              $state.go("app.nnPrint", { idDocument: row.documentId});
            }

            function updateDocument(row) {

                let params = {
                    userId : $scope.userInfo.id,
                    typeDocumentId : 4,
                    documentId : row.documentId,
                    commentary : "Entrega Rápida",
                    option : 7
                }
        
                documentsNnServices.documentsnn.setDocumentList(params).$promise.then((dataReturn) => {
                    ngNotify.set('La entrega ha sido registrada exitosamente / Documento NN: '+ row.documentId,'warn');
                    loadDocuments()
                },(err) => {
                    ngNotify.set('Error al actualizar documento','error')
                })
             
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

documentsNnListController.$inject = ['$scope','UserInfoConstant','$timeout','documentsNnServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

