
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> documentsNnUpdateController
* # As Controller --> documentsNnUpdate														
*/

export default class documentsNnUpdateController{

        constructor($scope,UserInfoConstant,$timeout,documentsNnServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element,$stateParams,quotationsServices,budgetsServices,transportationServices){
            var vm = this;
            vm.createInvoice = createInvoice
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.removeItems = removeItems
            vm.setTransportation = setTransportation
            vm.packOffSelect = packOffSelect
            vm.invoicedQuotation = invoicedQuotation
            $scope.invoiceCode= $stateParams.idQuotation
            $scope.type = $stateParams.type
            $scope.invoicedTrue = false
            vm.showPanel = showPanel
            vm.getStock = getStock
            vm.getNet = getNet
            vm.searchClient = searchClient
            vm.loadItems = loadItems
            vm.getNetTotal = getNetTotal
            vm.webItemUpdate = webItemUpdate
            $scope.parseInt = parseInt
            $scope.buttonCreateInvoice = true
            $scope.discount = 0
            $scope.packOff = false
            // $scope.discount = parseInt($stateParams.discount)
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }
                vm.searchData = searchData

                function searchData() {
                  itemsServices.items.getItems().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.itemsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }

                transportationServices.transportation.getTransportation().$promise.then((dataReturn) => {
                  $scope.transportation = dataReturn.data;
                     console.log('$scope.transportationGrid',$scope.transportation);
                 },(err) => {
                     console.log('No se ha podido conectar con el servicio',err);
                 })



            if ($scope.type == 'cotizacion' ){
                $scope.note = true
                $scope.budget = false
                $scope.nn = false
                $scope.observation = "NN basado en cotización N° " + $scope.invoiceCode
                $scope.invoicedTrue = true
            }
      
            if ($scope.type == 'presupuesto' ){
                $scope.quotation = false
                $scope.budget = true
                $scope.nn = false
                $scope.observation = "NN basado en presupuesto N° " + $scope.invoiceCode
            }

            if ($scope.type == 'nn' ){
                $scope.quotation = false
                $scope.budget = false
                $scope.nn = true
                $scope.observation = "NN clonado basado en documento N° " + $scope.invoiceCode
            }

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.warehouseId = $scope.warehouses[0] 
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            setWarehouseId()
            loadItems()

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.documentsNnUpdate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.documentsNnUpdate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      } 
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{ row.entity.vatPrice | pesosChilenos }}</strong></p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    }
                ]
            };
            

          function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            $scope.invoiceItems = []

            function loadItem(row){
                 
                if (row.failure == 1) {

                  ngNotify.set( 'Item '+ row.itemId +' con FALLA, valide con supervisor antes de continuar', {
                      sticky: true,
                      button: true,
                      type : 'error'
                  })

                }else{

                    $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    price: row.netPrice,
                    oil: row.oil,
                    minPrice: row.netPrice,
                    netPurchaseValue: row.netPurchaseValue,
                    webType: row.webType,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                })
                    getStock(row.itemId,1)
                    if (row.oil == 1) {
                      $scope.discountButton = true
                      // $scope.discount = 0
                    }
                    $scope.warningItems = false

                }
                         
            }

            function getStock(itemId,quantity){
            
              let params = {
                itemId: itemId,
                warehouseId :$scope.warehouseId 
            }

            itemsServices.items.getStock(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data
              $scope.stock = $scope.item[0].stock
              $scope.notStock = 0
              $scope.warningQuantity = false
              $scope.buttonMore = false 
              if (quantity > $scope.stock) {

                ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock ,'warn')
                $scope.notStock = 1
                $scope.warningQuantity = true
                $scope.buttonMore = true              
            }
              
              return false;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
    
            }

            function getNet() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 

                  }
                  
                  $scope.NET =  net
                  $scope.netoInvoice = net - (($scope.discount*net)/100) 
                  $scope.total = Math.round($scope.netoInvoice)
                  // $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                  $scope.VAT = 0
                  return net;
                      
            }

            function getNetTotal() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.netPurchaseValue * valueItems.quantity); 

                  }
                  
                  $scope.netTotal =  net
                  return $scope.netTotal;
                      
            }


            function removeItems(index,oil) {
              // $scope.model = $scope.invoiceItems.length - 1
                $scope.invoiceItems.splice(index, 1)
                $scope.warningQuantity = false
                $scope.buttonMore = false

                if (oil == 1) { 
                  $scope.discountButton = false
                 }
                // if($scope.invoiceItems.length <= 0){
                //    $scope.warningQuantity = false
                // }else{
                //   $scope.warningQuantity = true
                // }

                 var i = 0
                 for (i=0; i<$scope.invoiceItems.length; i++) {
                      var id = $scope.invoiceItems[i].itemId
                      var stockId = $scope.invoiceItems[i].quantity
                      console.log("item x",id,stockId)
                      getStock(id,stockId)
                    }
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                
                if (warehouseId == null) {
                    $scope.UserInfoConstant = UserInfoConstant
                    $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.warehouseId = $scope.warehouses[0].warehouseId
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            }else{

                $scope.warehouseId = warehouseId
             }
            }

            function setName(clients) {
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedTwo = clients.rut
            }

            function showPanel() {
                $scope.panelInvoice = true
                $scope.invoiceItems = []
                $scope.warningQuantity = false
                $scope.buttonMore = false


                if ($scope.type == 'cotizacion' ){
                  let params = {
                    quotationId: $scope.invoiceCode,
                    originId: $scope.warehouseId
                  }

                    quotationsServices.quotations.getQuotationsItems(params).$promise.then((dataReturn) => {
                          $scope.itemsQuotations = dataReturn.data
                          for(var i in $scope.itemsQuotations){
                                $scope.invoiceItems.push({
                                    itemId: $scope.itemsQuotations[i].itemId,
                                    itemDescription: $scope.itemsQuotations[i].itemDescription,
                                    price: $scope.itemsQuotations[i].price,
                                    oil: $scope.itemsQuotations[i].oil,
                                    minPrice: $scope.itemsQuotations[i].price,
                                    netPurchaseValue: $scope.itemsQuotations[i].netPurchaseValue,
                                    webType: $scope.itemsQuotations[i].webType,
                                    maxDiscount : $scope.itemsQuotations[i].maxDiscount,
                                    quantity: $scope.itemsQuotations[i].quantityItems,
                                    disscount: $scope.itemsQuotations[i].discount
                             })
                                getStock($scope.itemsQuotations[i].itemId,$scope.itemsQuotations[i].quantityItems)
                                if ($scope.itemsQuotations[i].oil == 1) {
                                    $scope.discountButton = true
                                    // $scope.discount = 0
                                  }

                        }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get quotations',err);
                        })
                }



                if ($scope.type == 'presupuesto' ){
                  let params = {
                    budgetId: $scope.invoiceCode
                  }

                    budgetsServices.budgets.getBudgetsItems(params).$promise.then((dataReturn) => {
                          $scope.budgetsItems = dataReturn.data
                          for(var i in $scope.budgetsItems){
                                $scope.invoiceItems.push({
                                    itemId: $scope.budgetsItems[i].itemId,
                                    itemDescription: $scope.budgetsItems[i].itemDescription,
                                    price: $scope.budgetsItems[i].price,
                                    minPrice: $scope.budgetsItems[i].price,
                                    netPurchaseValue: $scope.budgetsItems[i].netPurchaseValue,
                                    webType: $scope.budgetsItems[i].webType,
                                    maxDiscount : $scope.budgetsItems[i].maxDiscount,
                                    quantity: $scope.budgetsItems[i].quantityItems,
                                    disscount: $scope.budgetsItems[i].discount
                             })
                                getStock($scope.budgetsItems[i].itemId,$scope.budgetsItems[i].quantityItems)

                        }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get getBudgetsItems',err);
                        })
                }

                if ($scope.type == 'nn' ){
                  let params = {
                    documentId: $scope.invoiceCode
                  }

                    documentsNnServices.documents.getDocumentItems(params).$promise.then((dataReturn) => {
                          $scope.documentItems = dataReturn.data
                          for(var i in $scope.documentItems){
                                $scope.invoiceItems.push({
                                    itemId: $scope.documentItems[i].itemId,
                                    itemDescription: $scope.documentItems[i].itemDescription,
                                    price: $scope.documentItems[i].price,
                                    oil: $scope.documentItems[i].oil,
                                    minPrice: $scope.documentItems[i].price,
                                    netPurchaseValue: $scope.documentItems[i].netPurchaseValue,
                                    webType: $scope.documentItems[i].webType,
                                    maxDiscount : $scope.documentItems[i].maxDiscount,
                                    quantity: $scope.documentItems[i].quantityItems,
                                    disscount: $scope.documentItems[i].discount
                             })
                                getStock($scope.documentItems[i].itemId,$scope.documentItems[i].quantityItems)
                                if ($scope.documentItems[i].oil == 1) {
                                    $scope.discountButton = true
                                    // $scope.discount = 0
                                  }
                        }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get NN Items',err);
                        })
                }
            }

            function createInvoice() {

                        let model = {
                            userId: $scope.userInfo.id,
                            // purchaseOrder: $scope.purchaseOrder,
                            clientId : $scope.clientId,
                            date : $scope.CurrentDate,
                            originId : $scope.warehouseId,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            netTotal : $scope.netTotal,
                            comment : $scope.observation,
                            packOff:  $scope.packOff,
                            transportationId:  $scope.transportationId, 
                            transportationName:  $scope.transportationName, 
                            paymentmethodId:  $scope.paymentmethodId, 
                            itemsInvoices : $scope.invoiceItems 
                        }      

                        console.log('modelo',model)   

                        documentsNnServices.documents.createDocument(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha creado el documento correctamente','success')
                            $state.go('app.documentsNn')
                            $scope.result = dataReturn.data

                            if($scope.invoicedTrue == true){
                              invoicedQuotation($scope.result)
                            }

                            for(var  i in model.itemsInvoices){

                                      console.log('entra',model.itemsInvoices)

                                      if (model.itemsInvoices[i].webType == "SI"){
                                          console.log('tipo web')

                                          let modelo = {
                                              itemId : model.itemsInvoices[i].itemId
                                          }

                                          itemsServices.items.getGeneralStockWeb(modelo).$promise.then((dataReturn) => {
                                             $scope.result = dataReturn.data;
                                             $scope.stock = $scope.result[0].generalStock;

                                             webItemUpdate(modelo.itemId,$scope.stock)

                                            },(err) => {
                                                console.log('No se ha podido conectar con el servicio',err);
                                            })

                                           }
                                      else{
                                          console.log('normal')
                                      }
                                   }

                          },(err) => {
                              ngNotify.set('Error al crear documento','error')
                          })
                    }


          function webItemUpdate(itemId,stock) {

                let data = {
                                    sku : itemId.toString(),
                                    stock_quantity: stock
                                }

                                itemsServices.woocommerceAPI.itemUpdate(data).$promise.then((dataReturn) => {
                                  $scope.result = dataReturn.data;
                                  $scope.message= dataReturn.message;
                                  $scope.status= dataReturn.status;

                                  if ($scope.status==200  ) {

                                     ngNotify.set('Mensaje sitio web: '+ $scope.message,'info')

                                      }else{
                                        console.log('Error actualizar stock web',$scope.message);
                                        ngNotify.set( 'Error actualización ITEM en el sitio web: ' + $scope.message, {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                        })
                                        
                                     }
                                  
                                  },(err) => {
                                        console.log('Error actualizar stock web',err);
                                        ngNotify.set( 'Error API Web', {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                      })
                                  })

            }

            function packOffSelect(value){
              console.log('packOffSelect',value)
              if(value == true){
                $scope.transporteView = true
              }else{
                $scope.transporteView = false
              }
              

            }

            
            function setTransportation(transportation) {
              console.log(transportation)
              $scope.transportationName = transportation.transportName
              $scope.transportationId = transportation.transportationId
            }


                
          function searchClient (value) {

            let model = {
                name: value
            }

            clientsServices.clients.getClientsByName(model).$promise.then((dataReturn) => {
                  $scope.clients = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            return false;
          }

          function invoicedQuotation(documentId) {
            let model = {
              documentId: documentId,
              quotationId: $scope.invoiceCode,
              docTypeId: 4
           }

          quotationsServices.quotations.invoicedQuotation(model).$promise.then((dataReturn) => {
            $scope.msg = dataReturn.data;
            console.log('facturado correctamente');
           },(err) => {
            console.log('No se ha podido conectar con el servicio',err);
           })

          } 

        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

documentsNnUpdateController.$inject =  ['$scope','UserInfoConstant','$timeout','documentsNnServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element','$stateParams','quotationsServices','budgetsServices','transportationServices'];

