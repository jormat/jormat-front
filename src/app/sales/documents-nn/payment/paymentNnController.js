/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentNnController
* # As Controller --> paymentNn														
*/


  export default class paymentNnController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,documentsNnServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.payDocument = payDocument;
        $scope.typeView = $scope.value
	    $scope.documentId = $scope.documentId
	    $scope.total = $scope.total
	    console.log('lorea',$scope.total)

	     //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {

                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

	    let params = {
            documentId: $scope.documentId
          }

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function payDocument() {
              let params = {
                    documentId: $scope.documentId,
                    paymentMethodId: $scope.paymentMethod,
                    userId: $scope.userInfo.id
                }

                console.log('model',params);
                //automatic pay nn
                    documentsNnServices.documents.payDocument(params).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado el documento de manera correctamente','success')
                        if ($scope.typeView == 1) {
                            $state.reload('app.documentsNn')
                        }else  {
                            $state.reload('app.nnPrint')
                        }
                        $modalInstance.dismiss('chao');

                      },(err) => {
                        ngNotify.set('Error al actualizar documento nn pago','error')
                     })
        }


    }

  }

  paymentNnController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','documentsNnServices','$location','UserInfoConstant','ngNotify','$state'];
