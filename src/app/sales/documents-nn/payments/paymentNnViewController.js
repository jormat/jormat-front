
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> paymentNnViewController
* # As Controller --> paymentNnView														
*/

export default class paymentNnViewController{

        constructor($scope,UserInfoConstant,$timeout,paymentsServices,clientsServices,clientsInvoicesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams,warehousesServices,itemsServices,$modal){
            var vm = this
            vm.nnView = nnView 
            vm.deletePayment = deletePayment
            vm.createPayment = createPayment
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.payment = 0

    
            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user,
                        $scope.userId = $scope.userInfo.id
                        // getPayments($scope.userId)
                        getPayments($stateParams.idDocument,$scope.userId)
                    }
                })
            //

            paymentsServices.paymentForms.getPaymentForms().$promise.then((dataReturn) => {
              $scope.paymentForms = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getPaymentForms',err);
            })

            $scope.documentId = $stateParams.idDocument
            $scope.clientId = $stateParams.idClient
            $scope.clientName = $stateParams.clientName
            $scope.total = $stateParams.total
            $scope.date = $stateParams.date
            $scope.status = $stateParams.status

            if ($scope.pending == 0) {
                $scope.statusPayment = 'Pagado'
                
            }else{
                $scope.statusPayment = 'Sin Pagar'
            }

        
            function getPayments(documentId,userId){
                let params = {
                    documentId: documentId,
                    userId: userId
                }

                console.log('eso',$stateParams.idDocument,$scope.documentId)

                console.log('params',params)
                paymentsServices.payments.getNnPayments(params).$promise.then((dataReturn) => {
                  $scope.paymentsGrid = dataReturn.data;
                  console.log('taht',$scope.paymentsGrid )
                  var sumPayments = 0
                  var i = 0
                     for (i=0; i<$scope.paymentsGrid.length; i++) {
                         sumPayments = sumPayments + $scope.paymentsGrid[i].total  
                      }

                      $scope.sum =  sumPayments
                      $scope.pending = $stateParams.total - $scope.sum
                      if ($scope.pending == 0) {
                            $scope.statusPayment = 'Pagado'
                            paymentsServices.payments.updateNnStatus(params).$promise.then((dataReturn) => {
                                ngNotify.set('Documento NN pagado completamente','warn')
                            },(err) => {
                                console.log('No se ha podido conectar con el servicio para actualizar NN',err);
                            })
                        }else{
                            $scope.statusPayment = 'Sin Pagar' 
                        }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err)
                  })

            }
            
            function deletePayment(paymentId,total){
                  $scope.paymentsId = paymentId;
                  $scope.total = total;
                  $scope.documentId = $stateParams.idDocument
                  var modalInstance  = $modal.open({
                          template: require('../pay-delete/payments-nn-delete.html'),
                          animation: true,
                          scope: $scope,
                          controller: 'paymentsNnDeleteController',
                          controllerAs: 'paymentsNnDelete'
                  })
            } 

            function nnView(documentId,clientName){
                $scope.invoiceData = {
                    documentId:documentId,
                    clientName:clientName
                }

                var modalInstance  = $modal.open({
                        template: require('../view/documents-nn-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'documentsNnViewController',
                        controllerAs: 'documentsNnView',
                        size: 'lg'
                })
            }

        
            function createPayment() {

                    let model = {
                        userId: $scope.userInfo.id,
                        documentId: $stateParams.idDocument,
                        total : $scope.payment,
                        numberDocument : $stateParams.idDocument,
                        paymentFormId : $scope.paymentFormId,
                        // paymentFormId : $scope.paymentForms.selected.paymentFormId, 
                        // numberDocument : $scope.document,
                        comment : $scope.observation
                    }

                    console.log('model',model)  

                     paymentsServices.payments.createNnPayment(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado el abono correctamente','success')
                        getPayments($stateParams.idDocument,model.userId)
                        $scope.observation = ''
                        $scope.payment = ''
                        $scope.document = ''
                      },(err) => {
                        ngNotify.set('Error al ingresar abono','error')
                     })
                }
            
        }//FIN CONSTRUCTOR

        // Funciones
        
        
    }   

paymentNnViewController.$inject = ['$scope','UserInfoConstant','$timeout','paymentsServices','clientsServices','clientsInvoicesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams','warehousesServices','itemsServices','$modal'];

