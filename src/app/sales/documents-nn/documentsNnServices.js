/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> documentsNnServices
* file for call at services for providers
*/

class documentsNnServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    const documents = $resource(SERVICE_URL_CONSTANT.jormat + '/documents/nn/:route',{},{

        getDocuments: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getDocumentDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createDocument: {
            method:'POST',
            params : {
                route: ''
            }
        },
        payDocument: {
            method:'PUT',
            params : {
                route: 'pay'
            }
        },
        updateDocument: {
            method:'PUT',
            params : {
                route: ''
            }
        },
        disabledInvoice: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        getDocumentItems: {
            method:'GET',
            params : {
               route: 'items'
            }
        },
        updatePackOff: {
            method:'PUT',
            params : {
               route: 'packoff'
            }
        }
    })

    const documentsnn = $resource(SERVICE_URL_CONSTANT.jormat + '/warehouseDeliveries/:route',{},{
        setDocumentList: {
            method:'POST',
            params : {
                route: 'update'
            }
        },
    })

	return {
		documents 	: documents,
		documentsnn : documentsnn
	}
  }
}

  documentsNnServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.documentsNnServices', [])
  .service('documentsNnServices', documentsNnServices)
  .name;