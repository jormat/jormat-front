/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> printPackOffNnController
* # As Controller --> printPackOffNn														
*/


  export default class printPackOffNnController {

    constructor($scope, $filter,$rootScope, $http,documentsNnServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.documentId = $stateParams.idDocument

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        let model = {
                documentId: $scope.documentId
            }

        documentsNnServices.documents.getDocumentDetails(model).$promise.then((dataReturn) => {
              $scope.document = dataReturn.data
              console.log('data document',$scope.document);
              $scope.documentDetails  = { 
                    clientName : $scope.document[0].clientName,
                    rut : $scope.document[0].rut,
                    phone : $scope.document[0].phone,
                    city : $scope.document[0].city,
                    address : $scope.document[0].address,
                    transportationName : $scope.document[0].transportationName 

                  }
                  if($scope.document[0].receiver == null ){
                    $scope.observation = ""

                  }else{
                    $scope.observation = "Recepciona:"+$scope.document[0].receiver

                  }
              console.log('nn',$scope.documentDetails);    
                            
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get NN Details',err);
              })

    	function cancel() {

		 window.close();
         
		}


        function print() {
              window.print();

        }


    }

  }

  printPackOffNnController.$inject = ['$scope', '$filter','$rootScope', '$http','documentsNnServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
