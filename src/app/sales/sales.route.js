
routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default function routes($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode(true);
      //$urlRouterProvider.otherwise('/');

  $stateProvider
     .state('app.clientsInvoices', {
            abstract: false,
            url: '/facturas-clientes',
            template: require('./clients-invoices/list/clients-invoices-list.html'),
            controller: 'clientsInvoicesListController',
            controllerAs:'clientsInvoicesList'
      })
     .state('app.clientsInvoicesCreate', {
            abstract: false,
            url: '/facturas-clientes/crear-factura',
            template: require('./clients-invoices/create/clients-invoices-create.html'),
            controller: 'clientsInvoicesCreateController',
            controllerAs:'clientsInvoicesCreate'
        })
     .state('app.clientsInvoicesUpdate', {
            abstract: false,
            url: '/facturas-clientes/nueva-factura?idInvoice&discount&type',
            template: require('./clients-invoices/update/clients-invoices-update.html'),
            controller: 'clientsInvoicesUpdateController',
            controllerAs:'clientsInvoicesUpdate'
        })

     .state('app.invoicesCreateAccessKey', {
            abstract: false,
            url: '/facturas-clientes/acceso-especial',
            template: require('./clients-invoices/create-access-key/invoices-create-key.html'),
            controller: 'invoicesCreateKeyController',
            controllerAs:'invoicesCreateKey'
        })

     .state('app.clientsInvoicesReport', {
            abstract: false,
            url: '/facturas-clientes/reporte-facturas',
            template: require('./clients-invoices/report/invoices-report.html'),
            controller: 'invoicesReportController',
            controllerAs:'invoicesReport'
        })

     .state('app.invoicePrint', {
            abstract: false,
            url: '/facturas-clientes/ver-factura?idInvoice',
            template: require('./clients-invoices/print/invoice-print.html'),
            controller: 'invoicePrintController',
            controllerAs:'invoicePrint'
        })

     .state('app.warehouseInvoicePrint', {
            abstract: false,
            url: '/facturas-clientes/imprimir-factura?idInvoice',
            template: require('./clients-invoices/warehouse-print/invoice-warehouse-print.html'),
            controller: 'invoiceWarehousePrintController',
            controllerAs:'invoiceWarehousePrint'
        })

     .state('app.printPackOff', {
            abstract: false,
            url: '/facturas-clientes/imprimir-despacho?idInvoice',
            template: require('./clients-invoices/packOff/print-pack-off.html'),
            controller: 'printPackOffController',
            controllerAs:'printPackOff'
        })

     .state('app.invoicesByClient', {
            abstract: false,
            url: '/facturas-clientes/listado-facturas?clientId&startDate&endDate',
            template: require('./clients-invoices/clients/invoices-by-clients.html'),
            controller: 'invoicesByClientController',
            controllerAs:'invoicesByClient'
        })

     .state('app.documentsNn', {
            abstract: false,
            url: '/documento-nn',
            template: require('./documents-nn/list/documents-nn-list.html'),
            controller: 'documentsNnListController',
            controllerAs:'documentsNnList'
      })
     .state('app.documentsNnCreate', {
            abstract: false,
            url: '/documento-nn/crear-documento',
            template: require('./documents-nn/create/documents-nn-create.html'),
            controller: 'documentsNnCreateController',
            controllerAs:'documentsNnCreate'
        })
        .state('app.nnPrint', {
                abstract: false,
                url: '/documento-nn/imprimir-documento?idDocument',
                template: require('./documents-nn/print/nn-print.html'),
                controller: 'nnPrintController',
                controllerAs:'nnPrint'
            })

        .state('app.nnWarehousePrint', {
            abstract: false,
            url: '/documento-nn/formato-bodega?idDocument&warehouse',
            template: require('./documents-nn/warehouse-print/nn-warehouse-print.html'),
            controller: 'nnWarehousePrintController',
            controllerAs:'nnWarehousePrint'
        })

     .state('app.documentsNnUpdate', {
            abstract: false,
            url: '/documento-nn/nuevo-documento?idQuotation&discount&type',
            template: require('./documents-nn/update/documents-nn-update.html'),
            controller: 'documentsNnUpdateController',
            controllerAs:'documentsNnUpdate'
        })

     .state('app.printPackOffNn', {
            abstract: false,
            url: '/documento-nn/imprimir-despacho?idDocument',
            template: require('./documents-nn/pack-off/print-pack-off-nn.html'),
            controller: 'printPackOffNnController',
            controllerAs:'printPackOffNn'
        })

     .state('app.paymentNn', {
            abstract: false,
            url: '/documento-nn/pagos?idDocument&idClient&clientName&total&date',
            template: require('./documents-nn/payments/payment-nn-view.html'),
            controller: 'paymentNnViewController',
            controllerAs:'paymentNnView'
        })

     .state('app.clients', {
            abstract: false,
            url: '/clientes',
            template: require('./clients/list/clients-list.html'),
            controller: 'clientsListController',
            controllerAs:'clientsList'
        })
     .state('app.clientsCreate', {
            abstract: false,
            url: '/clientes/crear-clientes',
            template: require('./clients/create/clients-create.html'),
            controller: 'clientsCreateController',
            controllerAs:'clientsCreate'
        }) 
     .state('app.clientsUpdate', {
            abstract: false,
            url: '/clientes/actualizar-cliente?idClient',
            template: require('./clients/update/clients-update.html'),
            controller: 'clientsUpdateController',
            controllerAs:'clientsUpdate'
        })
     .state('app.paymentsByClient', {
            abstract: false,
            url: '/clientes/pagos-cliente?clientId&startDate&endDate',
            template: require('./clients/payments/payments-by-clients.html'),
            controller: 'paymentsByClientController',
            controllerAs:'paymentsByClient'
        })
     .state('app.accountStatus', {
            abstract: false,
            url: '/clientes/cuenta-cliente?idClient',
            template: require('./clients/account/account-status.html'),
            controller: 'accountStatusController',
            controllerAs:'accountStatus'
        })
     .state('app.payments', {
            abstract: false,
            url: '/pagos-clientes',
            template: require('./payments/list/payments-list.html'),
            controller: 'paymentsListController',
            controllerAs:'paymentsList'
      })

     .state('app.paymentsView', {
            abstract: false,
            url: '/pagos-clientes/visualizar-pago?idInvoice&idClient&clientName&total&date',
            template: require('./payments/view/payments-view.html'),
            controller: 'paymentsViewController',
            controllerAs:'paymentsView'
        })

     .state('app.paymentsReport', {
            abstract: false,
            url: '/pagos-clientes/reporte-pagos',
            template: require('./payments/report/payments-report.html'),
            controller: 'paymentsReportController',
            controllerAs:'paymentsReport'
        })

     .state('app.officePayments', {
            abstract: false,
            url: '/pagos/pagos-sucursal',
            template: require('./office-payments/list/office-payments-table.html'),
            controller: 'officePaymentsListController',
            controllerAs:'officePaymentsList'
        })

     .state('app.officePaymentsCreate', {
            abstract: false,
            url: '/pagos/pagos-sucursal/crear-pago',
            template: require('./office-payments/create/office-payments-create.html'),
            controller: 'officePaymentsCreateController',
            controllerAs:'officePaymentsCreate'
        })

     .state('app.officePaymentsPrint', {
            abstract: false,
            url: '/pagos/pagos-sucursal/visualizar-pago?paymentId',
            template: require('./office-payments/print/office-payments-print.html'),
            controller: 'officePaymentsPrintController',
            controllerAs:'officePaymentsPrint'
        })

     .state('app.creditNotes', {
            abstract: false,
            url: '/notas-credito',
            template: require('./credit-notes/list/credit-notes-list.html'),
            controller: 'creditNotesListController',
            controllerAs:'creditNotesList'
      })
     .state('app.creditNotesCreate', {
            abstract: false,
            url: '/notas-credito/crear-nota',
            template: require('./credit-notes/create/credit-notes-create.html'),
            controller: 'creditNotesCreateController',
            controllerAs:'creditNotesCreate'
        })

     .state('app.creditNotesPrint', {
            abstract: false,
            url: '/notas-credito/imprimir?creditNoteId&clientName',
            template: require('./credit-notes/print/credit-notes-print.html'),
            controller: 'creditNotesPrintController',
            controllerAs:'creditNotesPrint'
        })

     .state('app.paymentNotesView', {
            abstract: false,
            url: '/notas-credito/visualizar-nota?noteId&idClient&clientName&total&date',
            template: require('./credit-notes/payments/payment-notes-view.html'),
            controller: 'paymentNotesViewController',
            controllerAs:'paymentNotesView'
        })

     .state('app.ballots', {
            abstract: false,
            url: '/boletas?clientCategory&clientId',
            template: require('./ballots/list/ballots-list.html'),
            controller: 'ballotsListController',
            controllerAs:'ballotsList'
      })

     .state('app.ballotsCreate', {
            abstract: false,
            url: '/boletas/crear-boleta',
            template: require('./ballots/create/ballots-create.html'),
            controller: 'ballotsCreateController',
            controllerAs:'ballotsCreate'
        })

     .state('app.ballotsPrint', {
            abstract: false,
            url: '/boletas/imprimir-boleta?idBallot&warehouse',
            template: require('./ballots/print/ballots-print.html'),
            controller: 'ballotsPrintController',
            controllerAs:'ballotsPrint'
        })

        .state('app.warehouseBallotPrint', {
            abstract: false,
            url: '/boletas/imprimir-boleta-bodega?idBallot&warehouse',
            template: require('./ballots/warehouse-print/warehouse-ballots-print.html'),
            controller: 'warehouseBallotsPrintController',
            controllerAs:'warehouseBallotsPrint'
        })

     .state('app.ballotPayments', {
            abstract: false,
            url: '/boletas/pagos?idBallot&idClient&clientName&total&date',
            template: require('./ballots/payments/payments-ballot-view.html'),
            controller: 'paymentsBallotViewController',
            controllerAs:'paymentsBallotView'
        })

     .state('app.printPackOffBallot', {
            abstract: false,
            url: '/boletas/imprimir-despacho?idBallot',
            template: require('./ballots/pack-off/print-pack-off.html'),
            controller: 'printPackOffBallotController',
            controllerAs:'printPackOffBallot'
        })

     .state('app.quotations', {
            abstract: false,
            url: '/cotizaciones?clientCategory&clientId',
            template: require('./quotations/list/quotations-list.html'),
            controller: 'quotationsListController',
            controllerAs:'quotationsList'
      })

     .state('app.quotationsPrint', {
            abstract: false,
            url: '/cotizaciones/imprimir-cotizacion?idQuotation&warehouse',
            template: require('./quotations/print/quotations-print.html'),
            controller: 'quotationsPrintController',
            controllerAs:'quotationsPrint'
        })

     .state('app.quotationsCreate', {
            abstract: false,
            url: '/cotizaciones/crear-cotizacion?idItem',
            template: require('./quotations/create/quotations-create.html'),
            controller: 'quotationsCreateController',
            controllerAs:'quotationsCreate'
        })

     .state('app.quotationsUpdate', {
            abstract: false,
            url: '/cotizaciones/editar-cotizacion?idQuotation&discount&warehouse',
            template: require('./quotations/update/quotations-update.html'),
            controller: 'quotationsUpdateController',
            controllerAs:'quotationsUpdate'
        })

    .state('app.warehouseQuotationPrint', {
          abstract: false,
          url: '/cotizaciones/imprimir-cotizacion-bodega?idQuotation&warehouse',
          template: require('./quotations/warehouse-print/warehouse-print.html'),
          controller: 'warehousePrintController',
          controllerAs:'warehousePrint'
        })

    .state('app.imageQuotationsPrint', {
            abstract: false,
            url: '/cotizaciones/imprimir-cotizacion-distribuidor?idQuotation&warehouse',
            template: require('./quotations/image-print/image-quotations-print.html'),
            controller: 'imageQuotationsPrintController',
            controllerAs:'imageQuotationsPrint'
        })

    .state('app.quotationsCompanyPrintController', {
            abstract: false,
            url: '/cotizaciones/imprimir-cotizacion-sin-data?idQuotation&warehouse',
            template: require('./quotations/company-print/quotations-company-print.html'),
            controller: 'quotationsCompanyPrintController',
            controllerAs:'quotationsCompanyPrint'
        })

     .state('app.checks', {
            abstract: false,
            url: '/cheques',
            template: require('./checks/list/checks-list.html'),
            controller: 'checksListController',
            controllerAs:'checksList'
      })

     .state('app.saleNote', {
            abstract: false,
            url: '/notas-de-venta',
            template: require('./sale-note/list/sale-note-list.html'),
            controller: 'saleNoteListController',
            controllerAs:'saleNoteList'
      })

     .state('app.saleNotePrint', {
            abstract: false,
            url: '/notas-de-venta/imprimir-nota?idNote',
            template: require('./sale-note/print/sale-note-print.html'),
            controller: 'saleNotePrintController',
            controllerAs:'saleNotePrint'
        })

     .state('app.saleNoteCreate', {
            abstract: false,
            url: '/notas-de-venta/crear-nota',
            template: require('./sale-note/create/sale-note-create.html'),
            controller: 'saleNoteCreateController',
            controllerAs:'saleNoteCreate'
        })

     .state('app.saleNoteUpdate', {
            abstract: false,
            url: '/notas-de-venta/editar-nota?idNote&discount&warehouse',
            template: require('./sale-note/update/sale-note-update.html'),
            controller: 'saleNotesUpdateController',
            controllerAs:'saleNotesUpdate'
        })

     .state('app.afterSale', {
            abstract: false,
            url: '/post-venta',
            template: require('./after-sale/list/after-sale-list.html'),
            controller: 'afterSaleListController',
            controllerAs:'afterSaleList'
      })
     
     .state('app.afterSaleCreate', {
            abstract: false,
            url: '/post-venta/crear-documento',
            template: require('./after-sale/create/after-sale-create.html'),
            controller: 'afterSaleCreateController',
            controllerAs:'afterSaleCreate'
        })

       .state('app.voidDocuments', {
              abstract: false,
              url: '/anulacion-documentos',
              template: require('./void-documents/list/void-documents-list.html'),
              controller: 'voidDocumentsListController',
              controllerAs:'voidDocumentsList'
        })

       .state('app.voidDocumentsCreate', {
              abstract: false,
              url: '/anulacion-documentos/anular-documento',
              template: require('./void-documents/create/void-document.html'),
              controller: 'voidDocumentController',
              controllerAs:'voidDocument'
        })

       .state('app.budgets', {
            abstract: false,
            url: '/presupuestos',
            template: require('./budgets/list/budgets-list.html'),
            controller: 'budgetsListController',
            controllerAs:'budgetsList'
      })

       .state('app.budgetsPrint', {
            abstract: false,
            url: '/presupuestos/imprimir-presupuesto?idBudge',
            template: require('./budgets/print/budgets-print.html'),
            controller: 'budgetsPrintController',
            controllerAs:'budgetsPrint'
        })

       .state('app.budgetsCreate', {
            abstract: false,
            url: '/presupuestos/crear-presupuesto',
            template: require('./budgets/create/budgets-create.html'),
            controller: 'budgetsCreateController',
            controllerAs:'budgetsCreate'
        })


       .state('app.budgetsUpdate', {
            abstract: false,
            url: '/presupuestos/editar-presupuesto?idBudge&discount&warehouse',
            template: require('./budgets/update/budgets-update.html'),
            controller: 'budgetsUpdateController',
            controllerAs:'budgetsUpdate'
        })


       .state('app.exemptInvoices', {
            abstract: false,
            url: '/documentos-exentos',
            template: require('./exempt-invoices/list/exempt-invoices-list.html'),
            controller: 'exemptInvoicesListController',
            controllerAs:'exemptInvoicesList'
        })

       .state('app.exemptInvoicesCreate', {
            abstract: false,
            url: '/documentos-exentos/crear-factura',
            template: require('./exempt-invoices/create/exempt-invoices-create.html'),
            controller: 'exemptInvoicesCreateController',
            controllerAs:'exemptInvoicesCreate'
        })

     // .state('app.paymentNotesView', {
     //        abstract: false,
     //        url: '/notas-credito/visualizar-nota?noteId&idClient&clientName&total&date',
     //        template: require('./credit-notes/payments/payment-notes-view.html'),
     //        controller: 'paymentNotesViewController',
     //        controllerAs:'paymentNotesView'
     //    })
      ;
 
}
