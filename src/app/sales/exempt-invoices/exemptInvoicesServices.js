/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> exemptInvoicesServices
* file for call at services for exemptInvoices
*/

class exemptInvoicesServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    const exemptInvoices = $resource(SERVICE_URL_CONSTANT.jormat + '/documents/exempt/:route',{},{

        getExemptInvoices: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getDocumentDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createDocument: {
            method:'POST',
            params : {
                route: ''
            }
        },
        payDocument: {
            method:'PUT',
            params : {
                route: 'pay'
            }
        },
        updateDocument: {
            method:'PUT',
            params : {
                route: ''
            }
        },
        disabledInvoice: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        getDocumentItems: {
            method:'GET',
            params : {
               route: 'items'
            }
        }
    })

    const exemptDocuments = $resource(SERVICE_URL_CONSTANT.jormat + '/warehouseDeliveries/:route',{},{
        setDocumentList: {
            method:'POST',
            params : {
                route: 'update'
            }
        }
    })



	return {
		exemptInvoices : exemptInvoices,
        exemptDocuments : exemptDocuments
	}
  }
}

  exemptInvoicesServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.exemptInvoicesServices', [])
  .service('exemptInvoicesServices', exemptInvoicesServices)
  .name;