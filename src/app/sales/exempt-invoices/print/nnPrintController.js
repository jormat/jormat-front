/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> nnPrintController
* # As Controller --> nnPrint														
*/


  export default class nnPrintController {

    constructor($scope, $filter,$rootScope, $http,documentsNnServices,$location,UserInfoConstant,ngNotify,$state,$stateParams,$modal) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.payDocument = payDocument
        $scope.documentId = $stateParams.idDocument

        let params = {
                documentId: $scope.documentId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        documentsNnServices.documents.getDocumentDetails(params).$promise.then((dataReturn) => {
              $scope.document = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Document Details',err);
              })
        documentsNnServices.documents.getDocumentItems(params).$promise.then((dataReturn) => {
              $scope.documentItems = dataReturn.data
              var subTotal = 0
                var i = 0
                   for (i=0; i<$scope.documentItems.length; i++) {
                       subTotal = subTotal + $scope.documentItems[i].total  
                    }
                    
                    $scope.subTotality =  subTotal
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

        	function cancel() {

    		  $state.go("app.documentsNn")
    		}

           function payDocument(value){
                // $scope.value = value
                // $scope.documentId = $scope.documentId
                // $scope.total = $scope.document[0].total
                // var modalInstance  = $modal.open({
                //         template: require('../payment/payment-nn.html'),
                //         animation: true,
                //         scope: $scope,
                //         controller: 'paymentNnController',
                //         controllerAs: 'paymentNn'
                // })

                $state.go("app.paymentNn", { 
                  idDocument: $scope.documentId,
                  idClient: $scope.document[0].clientId,
                  clientName: $scope.document[0].clientName,
                  total: $scope.document[0].total,
                  date: $scope.document[0].date


                 });
              }

             function print() {
              window.print();

        }


    }

  }

  nnPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','documentsNnServices','$location','UserInfoConstant','ngNotify','$state','$stateParams','$modal'];
