/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> exemptInvoicesPayController
* # As Controller --> exemptInvoicesPay												
*/


  export default class exemptInvoicesPayController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,exemptInvoicesServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.payDocument = payDocument;
        $scope.typeView = $scope.value
	    $scope.documentId = $scope.documentId
	    $scope.total = $scope.total
	    console.log('lorea',$scope.total)

	     //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {

                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

	    let params = {
            documentId: $scope.documentId
          }

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function payDocument() {
              let params = {
                    documentId: $scope.documentId,
                    paymentMethodId: $scope.paymentMethod,
                    userId: $scope.userInfo.id
                }

                console.log('model',params);
                //automatic pay nn
                    exemptInvoicesServices.exemptInvoices.payDocument(params).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha pagado el documento exento de manera correctamente','success')
                        
                        $state.reload('app.exemptInvoices')
                        $modalInstance.dismiss('chao');

                      },(err) => {
                        ngNotify.set('Error al actualizar documento exento pago','error')
                     })
        }


    }

  }

  exemptInvoicesPayController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','exemptInvoicesServices','$location','UserInfoConstant','ngNotify','$state'];
