
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> exemptInvoicesListController
* # As Controller --> exemptInvoicesList														
*/

export default class exemptInvoicesListController{

        constructor($scope,UserInfoConstant,$timeout,exemptInvoicesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService,$window,LOCAL_ENV_CONSTANT){
            var vm = this
            vm.viewInvoice = viewInvoice
            vm.printDocument= printDocument
            vm.loadDocuments = loadDocuments
            vm.searchData=searchData
            vm.paymentNn = paymentNn
            vm.cloneNn = cloneNn
            vm.createXML = createXML
            vm.updateDocument = updateDocument
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            loadDocuments()

            //function to show userId
            $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                    }
                })
            //
            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.invoicesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'documentos_nn.csv',
                showColumnFooter: true,
                columnDefs: [
                    
                    { 
                      name:'Folio',
                      field: 'documentId',  
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.exemptInvoicesList.viewInvoice(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>'  
                    },
                    // { 
                    //   name:'Id Cliente',
                    //   field: 'clientId',
                    //   width: '7%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents">'+
                    //              '<p><span tooltip="Código cliente" class="badge bg-info">{{row.entity.clientId}}</span></p>' +
                    //              '</div>'
                    // },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '35%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    
                    
                    { 
                      name:'Total',
                      field: 'total',
                      width: '10%',
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{row.entity.total}}</p>' +
                                 '</div>'
                    },

                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '11%'
                    },
                    // { 
                    //   name:'Orden',
                    //   field: 'purchaseOrder',
                    //   width: '9%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents">'+
                    //              '<p> {{row.entity.purchaseOrder}}</p>' +
                    //              '</div>'
                    // },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="{{(row.entity.statusName == \'Pagado\')?\'badge bg-warning\':\'badge bg-danger\'}}">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Usuario', 
                      field: 'userCreation', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" badge bg-secondary font-italic" style="font-size: 11px;">{{row.entity.userCreation}}</p>' +
                                 '</div>'   
                    },
                    
                    { 
                      name: 'Acciones',
                      enableFiltering: false,
                      width: '9%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadDocuments(){

            exemptInvoicesServices.exemptInvoices.getExemptInvoices().$promise.then((dataReturn) => {
              $scope.invoicesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            function paymentNn(row){

            $state.go("app.paymentNn", { 
                idDocument: row.documentId,
                idClient: row.clientId,
                clientName: row.clientName,
                total: row.total,
                date: row.date


            });

            }

            function cloneNn(row){

                 $state.go("app.documentsNnUpdate", { idQuotation: row.documentId, discount:row.discount,type:'nn'});

            }

            function createXML(row) { 
              // $window.open('http://localhost/jormat-system/XML_exempt.php?documentId='+row.documentId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
              $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_exempt.php?documentId='+row.documentId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
 
            }
            
            function searchData() {
              exemptInvoicesServices.exemptInvoices.getExemptInvoices().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.invoicesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewInvoice(row){
	            $scope.invoiceData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/exempt-invoices-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'exemptInvoicesViewController',
	                    controllerAs: 'exemptInvoicesView',
	                    size: 'lg'
	            })
	          }

            function printDocument(row){
              $state.go("app.nnPrint", { idDocument: row.documentId});
            }

            function updateDocument(row) {

                // let params = {
                //     userId : $scope.userInfo.id,
                //     typeDocumentId : 4,
                //     documentId : row.documentId,
                //     commentary : "Entrega Rápida",
                //     option : 1
                // }
        
                // exemptInvoicesServices.documentsnn.setDocumentList(params).$promise.then((dataReturn) => {
                //     ngNotify.set('La entrega ha sido registrada exitosamente / Documento NN: '+ row.documentId,'warn');
                //     loadDocuments()
                // },(err) => {
                //     ngNotify.set('Error al actualizar documento','error')
                // })
             
            }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

exemptInvoicesListController.$inject = ['$scope','UserInfoConstant','$timeout','exemptInvoicesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService','$window','LOCAL_ENV_CONSTANT'];

