
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> exemptInvoicesCreateController
* # As Controller --> exemptInvoicesCreate														
*/

export default class exemptInvoicesCreateController{

        constructor($scope,UserInfoConstant,$timeout,exemptInvoicesServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element){
            var vm = this;
            vm.createInvoice = createInvoice
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getStock = getStock
            vm.getNet = getNet
            vm.searchClient = searchClient
            vm.loadItems = loadItems
            vm.getNetTotal = getNetTotal
            $scope.parseInt = parseInt
            $scope.buttonCreateInvoice = true
            $scope.discount = 0
            $scope.purchaseOrder = 0
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.warehouseId = $scope.warehouses[0] 
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            setWarehouseId()
            loadItems()

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.exemptInvoicesCreate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.exemptInvoicesCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      } 
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{row.entity.vatPrice}}</strong></p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                         return 'critical';
                       }
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    }
                ]
            };
            

          function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            $scope.invoiceItems = []

            function loadItem(row){
                 
                if (row.failure == 1) {

                  ngNotify.set( 'Item '+ row.itemId +' con FALLA, valide con supervisor antes de continuar', {
                      sticky: true,
                      button: true,
                      type : 'error'
                  })

                }else{

                    $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    price: row.netPrice,
                    oil: row.oil,
                    minPrice: row.netPrice,
                    netPurchaseValue: row.netPurchaseValue,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                })
                    getStock(row.itemId,1)
                    if (row.oil == 1) {
                      $scope.discountButton = true
                      // $scope.discount = 0
                    }
                    $scope.warningItems = false

                }
                         
            }

            function getStock(itemId,quantity){
            
              let params = {
                itemId: itemId,
                warehouseId :$scope.warehouseId 
            }

            itemsServices.items.getStock(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data
              $scope.stock = $scope.item[0].stock
              $scope.notStock = 0
              $scope.warningQuantity = false
              $scope.buttonMore = false 
              if (quantity > $scope.stock) {

                ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock ,'warn')
                $scope.notStock = 1
                $scope.warningQuantity = true
                $scope.buttonMore = true              
            }
              
              return false;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
    
            }

            function getNet() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 

                  }
                  
                  $scope.NET =  net
                  $scope.netoInvoice = net - (($scope.discount*net)/100) 
                  $scope.total = Math.round($scope.netoInvoice)
                  // $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                  $scope.VAT = 0
                  return net;
                      
            }

            function getNetTotal() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.netPurchaseValue * valueItems.quantity); 

                  }
                  
                  $scope.netTotal =  net
                  return $scope.netTotal;
                      
            }


            function removeItems(index,oil) {
              // $scope.model = $scope.invoiceItems.length - 1
                $scope.invoiceItems.splice(index, 1)
                $scope.warningQuantity = false
                $scope.buttonMore = false

                if (oil == 1) { 
                  $scope.discountButton = false
                 }
                // if($scope.invoiceItems.length <= 0){
                //    $scope.warningQuantity = false
                // }else{
                //   $scope.warningQuantity = true
                // }
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                
                if (warehouseId == null) {
                    $scope.UserInfoConstant = UserInfoConstant
                    $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.warehouseId = $scope.warehouses[0].warehouseId
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            }else{

                $scope.warehouseId = warehouseId
             }
            }

            function setName(clients) {
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedTwo = clients.rut
            }

            function showPanel() {
                $scope.panelInvoice = true
                $scope.invoiceItems = []
                $scope.warningQuantity = false
            }

            function createInvoice() {

                        let model = {
                            userId: $scope.userInfo.id,
                            purchaseOrder: $scope.purchaseOrder,
                            clientId : $scope.clientId,
                            date : $scope.CurrentDate,
                            originId : $scope.warehouseId,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            netTotal : $scope.netTotal,
                            comment : $scope.observation,
                            itemsInvoices : $scope.invoiceItems 
                        }      

                        console.log('modelo',model)   

                        exemptInvoicesServices.exemptInvoices.createDocument(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha creado la factura exenta correctamente','success')
                            $state.go('app.exemptInvoices')
                          },(err) => {
                              ngNotify.set('Error al crear documento','error')
                          })
                    }


                
          function searchClient (value) {

            let model = {
                name: value
            }

            clientsServices.clients.getClientsByName(model).$promise.then((dataReturn) => {
                  $scope.clients = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            return false;
          }

        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

exemptInvoicesCreateController.$inject = ['$scope','UserInfoConstant','$timeout','exemptInvoicesServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element'];

