/**
* @description
* Controller to confirmations
* @controller 
* # name: confirmationClientController
* # alias: confirm
*/

export default class confirmationClientController {
	constructor($scope, $mdDialog, ngNotify,quotationsServices, clientsServices,$modal,$state,$modalInstance,$window) {
		var vm = this
		this.$scope = $scope
		vm.createQuotation = createQuotation
		$scope.model = $scope.quotationsData
		vm.close = close
		vm.save = save
		vm.saveAnon = saveAnon
		ngNotify = ngNotify
		this.$scope.rutValidate = true
		this.idClient= 4934

		function save(){
			$scope.rutValidate = true
	
			let model = {
				fullName: $scope.fullName,
				rut: $scope.rut,
				mobile: $scope.mobile,
				email: $scope.email,
				category:0
			}
	
				clientsServices.clients.getClientsRut(model).$promise.then((dataReturn) => {
				$scope.resultRut = dataReturn.data;
				if ($scope.resultRut.length == 0) {
	
					clientsServices.clients.createClient(model).$promise.then((dataReturn) => {
							ngNotify.set('Se ha creado el cliente correctamente','success')
							
                            $scope.data = dataReturn.data;
							
							createQuotation($scope.quotationsData,dataReturn.data)
							$state.go('app.quotations');
							$modalInstance.dismiss('chao');
							
					 	 },(err) => {
						  	ngNotify.set('Error al crear cliente','error')
						  	$scope.rutValidate = false
						})
	
					}else{
						//cliente ya se encuentra registrado, actualiza categoria
						$scope.data = dataReturn.data;
						$scope.clientId = $scope.data[0].id;
						$scope.category = $scope.data[0].category;
						//$scope.categoryClient = 0
						createQuotation($scope.model,$scope.clientId,$scope.category)
					}

			  },(err) => {
				  ngNotify.set('Error al consultar rut cliente','error')
				  $scope.rutValidate = false
			})
			
		}
		function saveAnon(){
			createQuotation($scope.quotationsData,this.idClient)
			$state.go('app.quotations');
			$modalInstance.dismiss('chao');
		}

		function close() {
			$modalInstance.dismiss('chao');
		}


		function createQuotation(model,clientId,category) {
			//close()
			$scope.modelo = {
					userId: $scope.userInfo.id,
                      clientId : clientId,
                      date : $scope.CurrentDate,
                      originId : model.originId,
                      priceVAT : model.priceVAT,
                      netPrice : model.netPrice,
                      discount : model.discount,
                      total : model.total,
                      comment : model.observation,
                      name : model.name,
                      phone : model.phone,
                      itemsInvoices : $scope.invoiceItems  
			}

				console.log("categoryclient1:::", category)

                 quotationsServices.quotations.createQuotation($scope.modelo).$promise.then((dataReturn) => {
					ngNotify.set('Se ha creado la cotizacion correctamente','success')
                      $scope.result = dataReturn.data

						$state.go('app.quotations', { clientCategory: category, clientId:clientId});
						$modalInstance.dismiss('chao');

                    },(err) => {
                        ngNotify.set('Error al crear cotizacion','error')
                    })

		
		}

	}//fin constructor

	validate() {
		if (this.$scope.fullName === undefined) {
			return this.ngNotify.set('Agregue nombre cliente','warn')
		}


	}

	checkRut(rut) {
		if(rut === undefined) {
			this.$scope.textValidation = "Digite Rut";
			this.$scope.rutValidate = true;  
			return false;
		}

		let valor = rut.replace('.','')
		valor = valor.replace('-','')
		this.$scope.cuerpo = valor.slice(0,-1)
		this.$scope.dv = valor.slice(-1).toUpperCase()
		rut = this.$scope.cuerpo + '-'+ this.$scope.dv
		if(this.$scope.cuerpo.length < 7) {
			this.$scope.textValidation = "Incompleto";
			this.$scope.rutValidate = true;  
			return false;
		}
		
		this.$scope.suma = 0
		this.$scope.multiplo = 2

		for(this.$scope.i=1;this.$scope.i<=this.$scope.cuerpo.length;this.$scope.i++) {
			this.$scope.index = this.$scope.multiplo * valor.charAt(this.$scope.cuerpo.length -this.$scope.i)
			this.$scope.suma = this.$scope.suma + this.$scope.index

			if(this.$scope.multiplo < 7) { this.$scope.multiplo = this.$scope.multiplo + 1; } else { this.$scope.multiplo = 2; }
	  
		}
		
		this.$scope.dvEsperado = 11 - (this.$scope.suma % 11)
		this.$scope.dv = (this.$scope.dv == 'K')?10:this.$scope.dv
		this.$scope.dv = (this.$scope.dv == 0)?11:this.$scope.dv
		
		if(this.$scope.dvEsperado != this.$scope.dv) {
			this.$scope.textValidation = "RUT Inválido";
			this.$scope.rutValidate = true;
			return false; 
		}
		
		this.$scope.textValidation = "RUT Válido";
		this.$scope.rutValidate = false
	}
}

confirmationClientController.$inject = ['$scope', '$mdDialog', 'ngNotify','quotationsServices','clientsServices', '$modal', '$state', '$modalInstance', '$window']