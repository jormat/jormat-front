
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> quotationsCreateController
* # As Controller --> quotationsCreate														
*/

export default class quotationsCreateController{

  constructor($scope,UserInfoConstant,$timeout,quotationsServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element,$stateParams){
      this.$mdDialog = $mdDialog
      var vm = this;
      vm.createQuotation = createQuotation
      vm.setName = setName
      vm.ItemsCreate = ItemsCreate
      vm.setWarehouseId = setWarehouseId
      vm.viewItem = viewItem
      vm.loadItem = loadItem
      vm.loadItemCSV = loadItemCSV
      vm.removeItems = removeItems
      vm.showPanel = showPanel
      vm.getStock = getStock
      vm.getNet = getNet
      vm.showGeneralDiscount = showGeneralDiscount
      vm.searchClient = searchClient
      vm.loadItems = loadItems
      vm.confirmationClient= confirmationClient;
      vm.validateClient = validateClient
      $scope.itemId= $stateParams.idItem
      $scope.buttonInvoice = true
      $scope.parseInt = parseInt
      $scope.buttoncreateQuotation = true
      $scope.discount = 0
      $scope.purchaseOrder = 0
      $scope.warningItems = true
      $scope.currencySymbol = '$'
      $scope.origenAdd = false
      $scope.date = new Date()
      $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
      $scope.priceVATest = 56000

      vm.searchData = searchData

      function searchData() {
        itemsServices.items.getItems().$promise
        .then(function(data){
            $scope.data = data.data;
            $scope.itemsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
        });
      }
      
      $scope.editables = {
              disccount: '0',
              quantity: '1'
          }

      $scope.searchOptions = {
            updateOn: 'default blur',
            debounce:{
                'default': 100,
                'blur': 0
              }
          }

      

      if ($stateParams.idItem !== undefined) {

              $scope.warningItemsView = true

          }

      //function to show userId
          $scope.UserInfoConstant = UserInfoConstant
          $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
              if (details !== undefined) {
                  $scope.userInfo = details.user
                  let model = {
                      userId : $scope.userInfo.id
                  }
                  warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                      $scope.warehouses = dataReturn.data;
                      // $scope.warehouseId = $scope.warehouses[0] 
                      console.log('$scope.warehouses',$scope.warehouses)
                      $scope.origenAdd = true
                      },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
              }
          })
      //

      setWarehouseId()
      loadItems()
      getNet()

      function ItemsCreate() {
        var url = $state.href("app.itemsCreate");

        window.open(url,'_blank',"width=600,height=700");

    }

      $scope.itemsGrid = {
          enableFiltering: true,
          enableHorizontalScrollbar :0,
          columnDefs: [
              { 
                name: '',
                field: 'href',
                enableFiltering: false,
                width: '5%', 
                cellTemplate:'<div class="ui-grid-cell-contents ">'+
                             '<button ng-click="grid.appScope.quotationsCreate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                             '</div>',
                cellClass: function(grid, row) {
                    if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                          return 'critical';
                      }
                    if (row.entity.priority === 1) {
                      return 'yellow';
                    }
                    if (row.entity.failure === 1) { 
                      return 'red';
                    }
                }
              },
              { 
                name:'id',
                field: 'itemId', 
                enableFiltering: true,
                width: '10%',
                cellTemplate:'<div class="ui-grid-cell-contents ">'+
                           '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.quotationsCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                           '</div>' ,
                cellClass: function(grid, row) {
                  if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                          return 'critical';
                      }
                    if (row.entity.priority === 1) {
                      return 'yellow';
                    }
                    if (row.entity.failure === 1) { 
                      return 'red';
                    }
                }
              },
              { 
                name:'Descripción',
                field: 'itemDescription',
                enableFiltering: true,
                width: '42%',
                cellTemplate:'<div class="ui-grid-cell-contents ">'+
                           '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                           '</div>' ,
                cellClass: function(grid, row) {
                  if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                          return 'critical';
                      }
                    if (row.entity.priority === 1) {
                      return 'yellow';
                    }
                    if (row.entity.failure === 1) { 
                      return 'red';
                    }
                }
              },
              { 
                name:'Referencias',
                field: 'references',
                enableFiltering: true,
                width: '30%',
                cellTemplate:'<div class="ui-grid-cell-contents ">'+
                           '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                           '</div>',
                cellClass: function(grid, row) {
                  if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                          return 'critical';
                      }
                    if (row.entity.priority === 1) {
                      return 'yellow';
                    }
                    if (row.entity.failure === 1) { 
                      return 'red';
                    }
                }  
              },
              { 
                name:'Precio',
                field: 'vatPrice',
                enableFiltering: true,
                width: '13%',
                cellTemplate:'<div class="ui-grid-cell-contents ">'+
                           '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{ row.entity.vatPrice | pesosChilenos }}</strong></p>' +
                           '</div>',
                cellClass: function(grid, row) {
                  if (row.entity.generalStock <= row.entity.criticalStock && row.entity.criticalStock !== 0) {
                          return 'critical';
                      }
                    if (row.entity.priority === 1) {
                      return 'yellow';
                    }
                    if (row.entity.failure === 1) { 
                      return 'red';
                    }
                }  
              }
          ]
      };
      
     function loadItems(){

      itemsServices.items.getItems().$promise.then((dataReturn) => {
            $scope.itemsGrid.data = dataReturn.data;
        },(err) => {
            console.log('No se ha podido conectar con el servicio',err);
        })
     }
      

      $scope.invoiceItems = []

      function loadItem(row){
          
          var i = 0
            for (i=0; i<$scope.invoiceItems.length; i++) {
                  var id = $scope.invoiceItems[i].itemId

                  if(id == row.itemId){
                    ngNotify.set('Item '+ row.itemId +' Duplicado en este Documento','warn')
                    $scope.invoiceItems.splice(index, 1)

                  }else{
                    console.log("item no puplicado",row.itemId)
                  }
                  
                }

          if (row.failure == 1) { 
              ngNotify.set( 'Item '+ row.itemId +' con FALLA, valide con supervisor antes de continuar', {
      sticky: true,
      button: true,
      type : 'error'
              }) 
          }else{
              $scope.invoiceItems.push({
      itemId: row.itemId,
      itemDescription: row.itemDescription,
      price: row.netPrice,
      oil: row.oil,
      minPrice: row.netPrice,
      maxDiscount : row.maxDiscount,
      quantity: 1,
      disscount: 0
              })

              getStock(row.itemId,1)

              if (row.oil == 1) {
      $scope.discountButton = true
      $scope.discount = 0
              }

              $scope.warningItems = false
          }
  getNet()
}

       function loadItemCSV(row){
          console.log('carga file',row)

          let params = {
                itemId: row.itemId
              }

              itemsServices.items.itemsList(params).$promise.then((dataReturn) => {
                    $scope.itemDetails = dataReturn.data
                    $scope.netPrice= $scope.itemDetails[0].netPrice
                    $scope.itemDescription= $scope.itemDetails[0].itemName

                    $scope.invoiceItems.push({
                      itemId: row.itemId,
                      itemDescription: $scope.itemDescription,
                      referenceKey : row.referenceKey,
                      price: $scope.netPrice,
                      oil: row.oil,
                      minPrice: Number(row.netPrice),
                      maxDiscount : 0,
                      quantity: Number(row.quantity),
                      disscount: 0

                  });

                   getStock(row.itemId,row.quantity)
                   if (row.oil == 1) {
                        $scope.discountButton = true
                        // $scope.discount = 0
                      }
                   $scope.warningItems = false   
       getNet()      

              },(err) => {
                    console.log('No se ha podido conectar con el servicio get items',err);
                  })


            
      }

      function showGeneralDiscount(value){

          if (value ==  0 || value ==  null ) {
              console.log('muestra boton',value); 
              $scope.discountButton = false

          }else{
              console.log('esconde boton',value); 
              $scope.discountButton = true
              $scope.discount = 0

          }
      
      }

      function getStock(itemId,quantity){
      
        let params = {
          itemId: itemId,
          warehouseId :$scope.warehouseId 
      }

      itemsServices.items.getStock(params).$promise.then((dataReturn) => {
        $scope.item = dataReturn.data
        $scope.stock = $scope.item[0].stock
        $scope.notStock = 0
        $scope.warningQuantity = false
        if (quantity > $scope.stock) {

          ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock ,'warn')
          $scope.notStock = 1
          $scope.warningQuantity = true
        }
        
        return false;
        },(err) => {
            console.log('No se ha podido conectar con el servicio',err);
        })

      }

      function getNet() {
          let net = 0
  if ($scope.invoiceItems && $scope.invoiceItems.length){
    for (let i = 0; i < $scope.invoiceItems.length; i++) {
      let valueItems = $scope.invoiceItems[i] 
      net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100);
    }
  }
            
  $scope.NET =  net
  $scope.netoInvoice = net - (($scope.discount*net)/100) 
  $scope.total = Math.round($scope.netoInvoice * 1.19)
  $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
  return net;
}


      function removeItems(index,oil) {
          $scope.invoiceItems.splice(index, 1)
          $scope.warningQuantity = false

          if (oil == 1) { 
    $scope.discountButton = false
  }

  getNet()
      }


      function viewItem(row){
          $scope.itemsData = row;
          var modalInstance  = $modal.open({
                  template: require('../../../items/items/view/items-view.html'),
                  animation: true,
                  scope: $scope,
                  controller: 'itemsViewController',
                  controllerAs: 'itemsView',
                  size: 'lg'
          })
      }

      function setWarehouseId(warehouseId){
          
      //     if (warehouseId == null) {
      //         $scope.UserInfoConstant = UserInfoConstant
      //         $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
      //         if (details !== undefined) {
      //             $scope.userInfo = details.user
      //             let model = {
      //                 userId : $scope.userInfo.id
      //             }
      //             warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
      //                 $scope.warehouses = dataReturn.data;
      //                 $scope.warehouseId = $scope.warehouses[0].warehouseId
      //                 console.log('$scope.warehouses',$scope.warehouses)
      //                 },(err) => {
      //                 console.log('No se ha podido conectar con el servicio',err);
      //             })
      //         }
      //     })
      // }else{

          $scope.warehouseId = warehouseId
          console.log('origen seleccionado',$scope.warehouseId);
       // }
      }

      function setName(clients) {
          console.log(clients)
          $scope.clientName = clients.fullName
          $scope.clientId = clients.clientId
          $scope.selectedTwo = clients.rut
          if (clients.locked == 1) {
              ngNotify.set('OJO este cliente esta bloqueado','warn')
              $scope.lockedClient = true

          }else{
              $scope.buttonInvoice = true
              $scope.lockedClient = false
          }
      }

      function showPanel() {

          $scope.panelInvoice = true
          $scope.warningQuantity = false

        if ($stateParams.idItem == undefined) {

              $scope.invoiceItems = []

          }else{

              let params = {
                itemId: $stateParams.idItem
              }

              itemsServices.items.itemsList(params).$promise.then((dataReturn) => {
                    $scope.itemDetails = dataReturn.data
                          $scope.invoiceItems.push({
                              itemId: params.itemId,
                              itemDescription: $scope.itemDetails[0].itemName,
                              price: $scope.itemDetails[0].netPrice,
                              minPrice: $scope.itemDetails[0].netPrice,
                              oil: $scope.itemDetails[0].oil,
                              netPurchaseValue: $scope.itemDetails[0].netPurchaseValue,
                              maxDiscount : $scope.itemDetails[0].maxDiscount,
                              quantity: 1,
                              disscount: 0
                       })
                          getStock(params.itemId,1)
                            if ($scope.itemDetails[0].oil == 1) {
                            $scope.discountButton = true
                            // $scope.discount = 0
                          }

                          $scope.warningItemsView = false

              },(err) => {
                    console.log('No se ha podido conectar con el servicio get items',err);
                  })

              $scope.warningItems = false

          }
          
      }

      function createQuotation() {

        $scope.model = {
                      userId: $scope.userInfo.id,
                      clientId : $scope.clientId,
                      date : $scope.CurrentDate,
                      originId : $scope.warehouseId,
                      priceVAT : $scope.VAT,
                      netPrice : $scope.NET,
                      discount : $scope.discount,
                      total : $scope.total,
                      comment : $scope.observation,
                      name : $scope.name,
                      phone : $scope.phone,
                      itemsInvoices : $scope.invoiceItems 
                  }      

                  console.log('modelo',$scope.model) 

                  quotationsServices.quotations.createQuotation($scope.model).$promise.then((dataReturn) => {
                      ngNotify.set('Se ha creado la cotizacion correctamente','success')
                      $scope.result = dataReturn.data
                      $state.go('app.quotations')

                    },(err) => {
                        ngNotify.set('Error al crear cotizacion','error')
                    })
              }


  function searchClient (value) {

      let model = {
          name: value
      }

      clientsServices.clients.getClientsByNameCategory(model).$promise.then((dataReturn) => {
            $scope.clients = dataReturn.data;
        },(err) => {
            console.log('No se ha podido conectar con el servicio',err);
        })

      return false;
    }

   
    function confirmationClient(){
      $scope.model = {
        userId: $scope.userInfo.id,
        date : $scope.CurrentDate,
        originId : $scope.warehouseId,
        priceVAT : $scope.VAT,
        netPrice : $scope.NET,
        discount : $scope.discount,
        total : $scope.total,
        comment : $scope.observation,
        name : $scope.name,
        phone : $scope.phone,
        itemsInvoices : $scope.invoiceItems 
    } 

      $scope.quotationsData = $scope.model
      var modalInstance  = $modal.open({
              template: require('./dialogs/confirm/confirmationClient.html'),
              animation: true,
              scope: $scope,
              controller: 'confirmationClientController',
              controllerAs: 'confirmationClient'
      })
}

  function validateClient(){
   let model= {
    clientId : $scope.clientId
   }
    if(model.clientId==undefined){
      confirmationClient()
    }else{
      createQuotation()
    }
   
  }

  }//FIN CONSTRUCTOR


  uploadCsv() {
     this.confirm = this.$mdDialog.confirm({
          controller         : 'uploadCsvQuotationsController',
          controllerAs       : 'uploadCsv',
          template           : require('./dialogs/uploadCsv/uploadCsvQuotations.html'),
          locals              : {},
          animation          : true,
          parent             : angular.element(document.body),
          clickOutsideToClose: true,
      })
      this.$mdDialog.show(this.confirm).then((row) => {
          if (row) {
             const dataReturn = row.data
             if(dataReturn.length < 2) {
                 this.loadItemCSV(dataReturn[0])
             }else {
                 for(const i in dataReturn){
                     this.loadItemCSV(dataReturn[i])
                 }
             }
             
          }
      })
  }

  // Funciones
}   

quotationsCreateController.$inject = ['$scope','UserInfoConstant','$timeout','quotationsServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element','$stateParams'];