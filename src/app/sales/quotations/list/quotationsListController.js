
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> quotationsListController
* # As Controller --> quotationsList														
*/

export default class quotationsListController{

        constructor($scope,UserInfoConstant,$timeout,quotationsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService,$stateParams,clientsServices){
            var vm = this
            vm.viewQuotations = viewQuotations
            vm.printQuotation = printQuotation
            $scope.categoryClient = $stateParams.clientCategory
            $scope.clientId = $stateParams.clientId
            vm.transformToInvoice = transformToInvoice
            vm.transformToNN = transformToNN
            vm.updateQuotation = updateQuotation
            vm.searchData=searchData
            vm.viewInvoice = viewInvoice
            vm.loadQuotations = loadQuotations
            vm.transformToNote = transformToNote
            vm.consultCategory = consultCategory
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            loadQuotations()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.quotationsGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'listado_cotizaciones.csv',
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'quotationId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.quotationsList.viewQuotations(row.entity)">{{row.entity.quotationId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '32%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Rut',
                      field: 'rut',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.rut}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secondary uppercase">{{row.entity.origin}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{ row.entity.total | pesosChilenos }}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '8%'
                    },
                    { 
                      name: 'Facturado', 
                      field: 'invoiced', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents text-center">'+
                                 '<a ng-click="grid.appScope.quotationsList.viewInvoice(row.entity)"><p class="font-italic" style="font-size: 12px;">{{row.entity.invoiced}}</p></a>' +
                                 '</div>'   
                    },
                    { 
                      name: 'Usuario', 
                      field: 'userCreation', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" badge bg-warning font-italic" style="font-size: 10px;">{{row.entity.userCreation}}</p>' +
                                 '</div>'   
                    },
                    
                    { 
                      name: '',
                      width: '7%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadQuotations(){

                quotationsServices.quotations.getQuotations().$promise.then((dataReturn) => {
                  $scope.quotationsGrid.data = dataReturn.data;
                  if($scope.categoryClient==0){
                     consultCategory($scope.clientId)
                  }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            }


            function searchData() {
              quotationsServices.quotations.getQuotations().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.quotationsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }


            function printQuotation(row){
            console.log(row)
              $state.go("app.quotationsPrint", { idQuotation: row.quotationId,warehouse: row.warehouseId});
            }

            function viewQuotations(row){
	            $scope.quotationData = row;
              console.log(row)
	            var modalInstance  = $modal.open({
	                    template: require('../view/quotations-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'quotationsViewController',
	                    controllerAs: 'quotationsView',
	                    size: 'lg'
	            })
	          }

            function viewInvoice(row){

              if(row.code == "FACT"){
                $scope.invoiceData = row;
                const modalInstance  = $modal.open({
                    template: require('../../clients-invoices/view/clients-invoices-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'clientsInvoicesViewController',
                    controllerAs: 'clientsInvoicesView',
                    size: 'lg'
                })

              }else{
                
                  $scope.invoiceData = {
                  documentId:row.invoiceId
              }
	              var modalInstance  = $modal.open({
	                    template: require('../../documents-nn/view/documents-nn-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'documentsNnViewController',
	                    controllerAs: 'documentsNnView',
	                    size: 'lg'
	              })
              }

              


            }

            function transformToInvoice(row) {
              $state.go("app.clientsInvoicesUpdate", { idInvoice: row.quotationId, discount:row.discount,type:'cotizacion'});

            }

            function transformToNote(row) {
              $state.go("app.saleNoteUpdate", { idNote: row.quotationId, discount:row.discount,type:'nota'});

            }

            function transformToNN(row) {
              $state.go("app.documentsNnUpdate", { idQuotation: row.quotationId, discount:row.discount,type:'cotizacion'});

        }

        function updateQuotation(row) {
              $state.go("app.quotationsUpdate", { idQuotation: row.quotationId, discount:row.discount,warehouse:row.warehouseId});

        }

        function consultCategory(clientId){
            $scope.clientId = clientId;
            var modalInstance  = $modal.open({
                template: require('../../quotations/create/dialogs/updateClients/updateClient.html'),
                animation: true,
                scope: $scope,
                controller: 'updateClientController',
                controllerAs: 'clientController'
            })
        }



            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

quotationsListController.$inject = ['$scope','UserInfoConstant','$timeout','quotationsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService','$stateParams','clientsServices'];

