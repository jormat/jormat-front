/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> quotationsViewController
* # As Controller --> quotationsView														
*/


  export default class quotationsViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,quotationsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
    	vm.transformToInvoice = transformToInvoice
      	$scope.quotationId = $scope.quotationData.quotationId
      	$scope.clientName = $scope.quotationData.clientName
      	$scope.originId = $scope.quotationData.warehouseId

	      let params = {
	            quotationId: $scope.quotationId,
	            originId: $scope.originId
	          }

	        quotationsServices.quotations.getQuotationsDetails(params).$promise.then((dataReturn) => {
	              $scope.quotation = dataReturn.data
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get quotation Details',err);
	              })

	        quotationsServices.quotations.getQuotationsItems(params).$promise.then((dataReturn) => {
	              $scope.quotationsItems = dataReturn.data
	              var subTotal = 0
	              var i = 0
	                 for (i=0; i<$scope.quotationsItems.length; i++) {
	                     subTotal = subTotal + $scope.quotationsItems[i].total  
	                  }
	                  
	                  $scope.subTotality =  subTotal

	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get quotation items',err);
	              })

	    	function cancel() {
				$modalInstance.dismiss('chao');
			}

			function transformToInvoice() {
              $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.quotationId,discount:$scope.quotation[0].discount,type:'cotizacion'});

        }

    }

  }

  quotationsViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','quotationsServices','$location','UserInfoConstant','ngNotify','$state'];
