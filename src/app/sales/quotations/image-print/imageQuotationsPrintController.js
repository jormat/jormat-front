/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> imageQuotationsPrintController
* # As Controller --> imageQuotationsPrint														
*/


  export default class imageQuotationsPrintController {

    constructor($scope, $filter,$rootScope, $http,quotationsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams,SERVICE_URL_CONSTANT) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.baseUrl = SERVICE_URL_CONSTANT
        vm.transformToInvoice = transformToInvoice
        vm.warehouseQuotationPrint= warehouseQuotationPrint
        vm.loadImg = loadImg
        $scope.quotationId = $stateParams.idQuotation
        $scope.originId = $stateParams.warehouse
        $scope.picture= require("../../../../images/unknown.png");

        let params = {
                quotationId: $scope.quotationId,
                originId: $scope.originId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        quotationsServices.quotations.getQuotationsDetails(params).$promise.then((dataReturn) => {
              $scope.quotations = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get quotations Details',err);
              })

        quotationsServices.quotations.getQuotationsItems(params).$promise.then((dataReturn) => {
              $scope.quotationsItems = dataReturn.data
              var subTotal = 0
              var i = 0
                 for (i=0; i<$scope.quotationsItems.length; i++) {
                     subTotal = subTotal + $scope.quotationsItems[i].total  
                  }

                  for(var j in $scope.quotationsItems){

                    if ($scope.quotationsItems[j].discount !== 0) {
                         console.log("true",$scope.quotationsItems[j])
                         $scope.isDiscount =  true
                     }
                }
                  $scope.subTotality =  subTotal
              

              },(err) => {
                  console.log('No se ha podido conectar con el servicio get quotations Items',err);
              })

    	function cancel() {

		 window.close();
		}

        function warehouseQuotationPrint(){

            console.log('imprimir print ');
            var url = $state.href("app.warehouseQuotationPrint",{
                idQuotation: $scope.quotationId,
                warehouse: $scope.originId
            });

        window.open(url,'_blank',"width=600,height=700");

          // $state.go("app.warehouseQuotationPrint", { idQuotation: $scope.quotationId,warehouse:$scope.originId});

        }

        function transformToInvoice(row){

          $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.quotationId,discount:$scope.quotations[0].discount,type:'cotizacion'});

        }

        function print() {
              window.print();

        }

        function loadImg(id) {
             $scope.imageUrl = $scope.baseUrl.jormat + '/image/image/'+ id
              return $scope.imageUrl;

        }


    }

  }

  imageQuotationsPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','quotationsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams','SERVICE_URL_CONSTANT'];
