/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> quotationsServices
* file for call at services for quotations
*/

class quotationsServices {

	constructor($resource,SERVICE_URL_CONSTANT) {
		
		var quotations = $resource(SERVICE_URL_CONSTANT.jormat + '/quotations/:route',{},{
			
				getQuotations: {
						method:'GET',
						params : {
							 route: 'list'
						}
				},
				getQuotationsDetails: {
						method:'GET',
						params : {
							 route: ''
						}
				},
				createQuotation: {
						method:'POST',
						params : {
								route: ''
						}
				},
				getQuotationsItems: {
						method:'GET',
						params : {
							 route: 'items'
						}
				},

				disabledQuotation: {
						method:'DELETE',
						params : {
								route: ''
						}
				},

				updateQuotation: {
						method:'PUT',
						params : {
								route: ''
						}
				},

				invoicedQuotation: {
						method:'PUT',
						params : {
								route: 'invoiced'
						}
				},
				getClientsRut: {
					method:'GET',
					params : {
					   route: 'rut'
					}
				},
				createClient: {
					method:'POST',
					params : {
						route: ''
					}
				}
		})

		return { quotations : quotations
						 
					 }
	}
}

	quotationsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

	export default  angular.module('services.quotationsServices', [])
	.service('quotationsServices', quotationsServices)
	.name;