
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> quotationsUpdateController
* # As Controller --> quotationsUpdate														
*/

export default class quotationsUpdateController{

        constructor($scope,UserInfoConstant,$timeout,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element,$window,$stateParams,quotationsServices){
            var vm = this;
            vm.updateQuotation = updateQuotation
            vm.viewItem = viewItem
            vm.ItemsCreate = ItemsCreate
            vm.loadItem = loadItem
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getStock = getStock
            vm.getNet = getNet
            vm.showGeneralDiscount = showGeneralDiscount
            vm.loadItems = loadItems
            $scope.quotationCode= $stateParams.idQuotation
            $scope.warehouseId= $stateParams.warehouse
            $scope.buttonInvoice = true
            $scope.parseInt = parseInt
            // $scope.discount = parseInt($stateParams.discount)
            $scope.discount = parseFloat($stateParams.discount)
            $scope.type = $stateParams.type
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }
            $scope.notStock = 0
            $scope.warningQuantity = false
            $scope.buttonMore = false
            $scope.invoiceItems = []
            vm.searchData = searchData

            function searchData() {
              itemsServices.items.getItems().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.itemsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

            showPanel()

            function ItemsCreate() {
              var url = $state.href("app.itemsCreate");

              window.open(url,'_blank',"width=600,height=700");

          }

          function ItemsCreate() {
            var url = $state.href("app.itemsCreate");
    
            window.open(url,'_blank',"width=600,height=700");
    
        }

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.warehouseId = $scope.warehouses[0] 
                            console.log('$scope.warehouses lulul',$scope.warehouses,$stateParams.discount)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            loadItems()
            

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-click="grid.appScope.quotationsUpdate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                   '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.quotationsUpdate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }  
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{row.entity.vatPrice}}</strong></p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }  
                    }
                ]
            };
            
          
            function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            function loadItem(row){

                  var i = 0
                  for (i=0; i<$scope.invoiceItems.length; i++) {
                        var id = $scope.invoiceItems[i].itemId

                        if(id == row.itemId){
                          ngNotify.set('Item '+ row.itemId +' Duplicado en este Documento','warn')
                          $scope.invoiceItems.splice(index, 1)

                        }else{
                          console.log("item no puplicado",row.itemId)
                        }
                        
                      }
                 
                  if (row.failure == 1) {

                   ngNotify.set( 'Item '+ row.itemId +' con FALLA, valide con supervisor antes de continuar', {
                      sticky: true,
                      button: true,
                      type : 'error'
                    }) 

                  }else{

                    $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    price: row.netPrice,
                    minPrice: row.netPrice,
                    oil: row.oil,
                    netPurchaseValue: row.netPurchaseValue,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                   })

                    getStock(row.itemId,1)
                    getNet()
                    if (row.oil == 1) {
                      $scope.discountButton = true
                      // $scope.discount = 0
                    }
                    $scope.warningItems = false 
                } 


            }

            function showGeneralDiscount(value){

                if (value ==  0 || value ==  null ) {
                    console.log('muestra boton',value); 
                    $scope.discountButton = false

                }else{
                    console.log('esconde boton',value); 
                    $scope.discountButton = true
                    $scope.discount = 0

                }
            
            }

            function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            
            function getStock(itemId,quantity){
            
              let params = {
                itemId: itemId,
                warehouseId :$stateParams.warehouse 
            }

            itemsServices.items.getStock(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data
              $scope.stock = $scope.item[0].stock
              $scope.notStock = 0
              $scope.warningQuantity = false
              if (quantity > $scope.stock) {

                ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock ,'warn')
                $scope.notStock = 1
                $scope.warningQuantity = true
              }
              
              return false;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
    
            }

            function getNet() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 
                  }
                  
                  $scope.subTotal =  net
                  $scope.netoInvoice = $scope.subTotal - (($scope.discount*$scope.subTotal)/100) 
                  $scope.total = Math.round($scope.netoInvoice * 1.19)
                  $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                  return net;
                      
            }

            function removeItems(index,oil) {
                $scope.invoiceItems.splice(index, 1)
                $scope.warningQuantity = false
                $scope.buttonMore = false

                if (oil == 1) { 
                $scope.discountButton = false
               }

               getNet()
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

       
            function showPanel() {
                $scope.invoiceItems = []
                $scope.panelInvoice = true
                $scope.warningQuantity = false
                $scope.buttonMore = false

                  let params = {
                    quotationId: $scope.quotationCode,
                    originId: $scope.warehouseId
                  }

                  quotationsServices.quotations.getQuotationsDetails(params).$promise.then((dataReturn) => {
                  $scope.quotation = dataReturn.data

                  $scope.quotationDetails  = { 
                        nameDescription : $scope.quotation[0].nameDescription,
                        phone : $scope.quotation[0].phone,
                        date : $scope.quotation[0].date,
                        origin : $scope.quotation[0].origin, 
                        warehouseId : $scope.quotation[0].warehouseId,
                        clientName : $scope.quotation[0].clientName,
                        clientId : $scope.quotation[0].clientId, 
                        comment : $scope.quotation[0].comment,
                        rut : $scope.quotation[0].rut
                    }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio get quotation Details',err);
                  })

                    quotationsServices.quotations.getQuotationsItems(params).$promise.then((dataReturn) => {
                          $scope.itemsQuotations = dataReturn.data
                          for(var i in $scope.itemsQuotations){
                                $scope.invoiceItems.push({
                                    itemId: $scope.itemsQuotations[i].itemId,
                                    itemDescription: $scope.itemsQuotations[i].itemDescription,
                                    price: $scope.itemsQuotations[i].price,
                                    oil: $scope.itemsQuotations[i].oil,
                                    minPrice: $scope.itemsQuotations[i].price,
                                    netPurchaseValue: $scope.itemsQuotations[i].netPurchaseValue,
                                    maxDiscount : $scope.itemsQuotations[i].maxDiscount,
                                    quantity: $scope.itemsQuotations[i].quantityItems,
                                    disscount: $scope.itemsQuotations[i].discount
                             })
                                getStock($scope.itemsQuotations[i].itemId,$scope.itemsQuotations[i].quantityItems)
                                if ($scope.itemsQuotations[i].oil == 1) {
                                $scope.discountButton = true
                                // $scope.discount = 0
                              }
                        }

                        getNet()

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio get quotations',err);
                        })
                 
            }

            function updateQuotation() {

                        let model = {
                            quotationId: $scope.quotationCode,
                            userId: $scope.userInfo.id,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            comment : $scope.quotationDetails.comment,
                            name : $scope.quotationDetails.nameDescription,
                            phone : $scope.quotationDetails.phone,
                            itemsInvoices : $scope.invoiceItems 
                        }      

                        console.log('modelo',model) 

                        quotationsServices.quotations.updateQuotation(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha actualizado la cotización correctamente','success')
                            $scope.result = dataReturn.data
                            $state.go('app.quotations')
                            

                          },(err) => {
                              ngNotify.set('Error al actualizar cotizacion','error')
                          })
                    }

    
        
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

quotationsUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element','$window','$stateParams','quotationsServices'];

