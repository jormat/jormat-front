
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> voidDocumentsListController
* # As Controller --> voidDocumentsList														
*/

export default class voidDocumentsListController{

        constructor($scope,UserInfoConstant,$timeout,voidDocumentsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,$window,LOCAL_ENV_CONSTANT,i18nService){
            var vm = this
            vm.viewInvoice = viewInvoice 
            vm.viewVoidDocument = viewVoidDocument
            vm.viewNotePayments = viewNotePayments
            vm.searchData=searchData
            vm.createXML =createXML
            vm.loadNotes = loadNotes
            vm.searchOptions = {
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }


            loadNotes()
            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.creditNotesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'documentos_anulados.csv',
                showColumnFooter: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'id',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.voidDocumentsList.viewVoidDocument(row.entity)">{{row.entity.id}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '29%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name:'N° Doc.',
                      field: 'documentId',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="badge bg-accent">{{row.entity.documentId}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '8%',
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{row.entity.total | pesosChilenos}}</p>' +
                                 '</div>'
                    },
                     { 
                      name: 'Tipo', 
                      field: 'typeDoc', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" badge bg-info font-italic" style="font-size: 11px;">{{row.entity.typeDoc}}</p>' +
                                 '</div>'   
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '11%'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Motivo', 
                      field: 'reason', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-secondary font-italic" style="font-size: 12px;">{{row.entity.reason}}</p>' +
                                 '</div>'   
                    },
                    
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="{{(row.entity.statusName == \'Aplicada\')?\'badge bg-warning\':\'badge bg-danger\'}}">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: '',
                      width: '5%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadNotes(){

            voidDocumentsServices.voidDocuments.getVoidDocuments().$promise.then((dataReturn) => {
              $scope.creditNotesGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }


            function searchData() {
              voidDocumentsServices.voidDocuments.getVoidDocuments().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.creditNotesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewVoidDocument(row){
	            $scope.voidDocumentData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/void-documents-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'voidDocumentsViewController',
	                    controllerAs: 'voidDocumentsView',
	                    size: 'lg'
	            })
	          }

            function viewInvoice(row){
                $scope.invoiceData = row
                var modalInstance  = $modal.open({
                        template: require('../../clients-invoices/view/clients-invoices-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'clientsInvoicesViewController',
                        controllerAs: 'clientsInvoicesView',
                        size: 'lg'
                })
            }

            function createXML(row) { 
              // $window.open('http://localhost/jormat-system/XML_note.php?noteId='+row.creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
              $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_note.php?noteId='+row.creditNoteId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
 
            }

            function viewNotePayments(row){

              console.log('row',row)
              
              $state.go("app.paymentNotesView",{ 
                    noteId: row.creditNoteId,
                    idClient: row.clientId,
                    clientName: row.clientName,
                    total: row.total,
                    date: row.date
                });

            }


            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

voidDocumentsListController.$inject = ['$scope','UserInfoConstant','$timeout','voidDocumentsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','$window','LOCAL_ENV_CONSTANT','i18nService'];

