/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> voidDocumentsServices
* file for call at services for Credit Notes
*/

class voidDocumentsServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var voidDocuments = $resource(SERVICE_URL_CONSTANT.jormat + '/documents/void/:route',{},{
      
        getVoidDocuments: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getVoidDocumentDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        getVoidDocumentItems: {
            method:'GET',
            params : {
                route: 'items'
            }
        },
        createVoidDocument: {
            method:'POST',
            params : {
                route: ''
            }
        },
        voidDocumentStatus: {
            method:'PUT',
            params : {
                route: 'status'
            }
        },

        createVoidDocumentFailure: {
            method:'POST',
            params : {
                route: 'failure'
            }
        },
        disabledCreditNotes: {
            method:'DELETE',
            params : {
                route: ''
            }
        },
        getReportNotes: {
            method:'GET',
            params : {
                route: 'report'
            }
        }
    })

    return { voidDocuments : voidDocuments
             
           }
  }
}

  voidDocumentsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.voidDocumentsServices', [])
  .service('voidDocumentsServices', voidDocumentsServices)
  .name;