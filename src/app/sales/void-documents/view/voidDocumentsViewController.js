/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> voidDocumentsViewController
* # As Controller --> voidDocumentsView														
*/


  export default class voidDocumentsViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,voidDocumentsServices,$location,UserInfoConstant,ngNotify,$state,documentsNnServices,ballotsServices) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.voidDocumentStatus = voidDocumentStatus
        $scope.documentId = $scope.voidDocumentData.id
        $scope.clientName = $scope.voidDocumentData.clientName

        //function to show userId

        $scope.UserInfoConstant = UserInfoConstant
        $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
            if (details !== undefined) {
                $scope.userInfo = details.user
            }
        })
            
        let params = {
                documentId: $scope.documentId
              }

        voidDocumentsServices.voidDocuments.getVoidDocumentDetails(params).$promise.then((dataReturn) => {
              $scope.voidDocument = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getVoidDocumentDetails',err);
              })

        voidDocumentsServices.voidDocuments.getVoidDocumentItems(params).$promise.then((dataReturn) => {
              $scope.voidDocumentItems = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getVoidDocumentItems',err);
              })

    	function cancel() {
			$modalInstance.dismiss('chao');
		}

        function voidDocumentStatus() {
            let model = {
                    documentId : $scope.documentId,
                    userId : $scope.userInfo.id 
                }

                console.log('that',model,$scope.voidDocument[0].typeDoc)

                voidDocumentsServices.voidDocuments.voidDocumentStatus(model).$promise.then((dataReturn) => {

                    ngNotify.set('documento actualizado exitosamente','success');
                    $state.reload("app.voidDocuments")
                    $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al validar Nota','error')
                  })

                let values = {
                    documentId : $scope.voidDocument[0].documentId,
                    userId : $scope.userInfo.id 
                }


                if ($scope.voidDocument[0].typeDoc == "NN") {
                          console.log('NN')

                            documentsNnServices.documents.updateDocument(values).$promise.then((dataReturn) => {
                                console.log('Se ha  anulado el pago del NN correctamente')
                                    // $state.go('app.voidDocuments')
                                  },(err) => {
                                      ngNotify.set('Error al anular documento','error')
                                  })
                        }
                if ($scope.voidDocument[0].typeDoc == "BOL") {
                            console.log('Boleta')

                            ballotsServices.ballots.updateBallot(values).$promise.then((dataReturn) => {
                                console.log('Se ha anulado el pago de BOL correctamente')
                                    // $state.go('app.voidDocuments')
                                  },(err) => {
                                      ngNotify.set('Error al anular documento','error')
                                  })
                        } 
        }

        function print(divPrint) {
              var printContents = document.getElementById(divPrint).innerHTML;
              var popupWin = window.open('', '_blank');
              popupWin.document.open();
              popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css"  /></head><body onload="window.print()">' + printContents + '</body></html>');
              // popupWin.document.close();
              $modalInstance.dismiss('chao');

        }

		


    }

  }

  voidDocumentsViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','voidDocumentsServices','$location','UserInfoConstant','ngNotify','$state','documentsNnServices','ballotsServices'];
