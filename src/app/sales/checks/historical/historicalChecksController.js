/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> historicalChecksController
* # As Controller --> historicalChecks														
*/

  export default class historicalChecksController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,checksServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants) {

    	var vm = this
    	vm.cancel=cancel
        vm.loadChecks = loadChecks

        // loadChecks()

        $scope.checksGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'reporte_cheques.csv',
                exporterPdfHeader: { 
                    text: "Reporte cheques ingresados", 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [20,20,20,20] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },

                    
                columnDefs: [
                    { 
                      name:'id',
                      field: 'checkId',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.checksList.checkView(row.entity)">{{row.entity.checkId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'N° Serie',
                      field: 'serialNumber',
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secundary uppercase" style="font-size: 10px;"><strong>{{row.entity.serialNumber}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Nombre Asociado',
                      field: 'associatedName',
                      width: '21%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;">{{row.entity.associatedName}}</p>' +
                                 '</div>'  
                    },
                    // { 
                    //   name:'N° Cuenta',
                    //   field: 'account',
                    //   width: '11%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.account}}</strong></p>' +
                    //              '</div>' 
                    // },
                     { 
                      name:'Banco',
                      field: 'bankName',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning" style="font-size: 10px;">{{row.entity.bankName}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Fecha', 
                      field: 'day',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p style="font-size: 11px;"> {{row.entity.day | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Facturas',
                      field: 'invoices',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;"><strong>{{row.entity.invoices}}</strong></p>' +
                                 '</div>'  
                    },
                   
                    { 
                      name:'Valor',
                      field: 'shareValue',
                      width: '8%',
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-accent" style="font-size: 12px;">$ {{row.entity.shareValue}}</p>' +
                                 '</div>' 
                    },
                    
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 11px;">{{row.entity.origin}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Ingreso', 
                      field: 'date',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p style="font-size: 11px;"> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    }, 
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '8%',
                      allowCellFocus : false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="badge bg-{{row.entity.styleStatus}}" style="font-size: 11px;">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    }
                ]
            };


        function loadChecks() {

            let model = {
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            checksServices.checks.getHistoricChecks(model).$promise.then((dataReturn) => {
              $scope.checksGrid.data = dataReturn.data

              console.log('modelo',model)

              if ($scope.checksGrid.data.length == 0){
                      $scope.messageInvoices = true
                      ngNotify.set( 'No existes cheques ingresados entre '+ model.startDate +' y '+ model.endDate , {
                      sticky: true,
                      button: true,
                      type : 'warn'
                  })
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }


    	function cancel() {
			$modalInstance.dismiss('chao');
		}


    }

  }

  historicalChecksController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','checksServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants'];
