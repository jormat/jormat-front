/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> checksServices
* file for call at services to checks
*/

class checksServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var checks = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/checks/:route',{},{

      getChecks: {
        method:'GET',
        params : {
          route: 'list'
          }
        },

      getChecksDetails: {
        method:'GET',
        params : {
          route: ''
          }
        },

      checksCreate: {
        method:'POST',
        params : {
          route: ''
          }
        },

      checksStatusUpdate: {
        method:'PUT',
        params : {
          route: 'status'
          }
        },

      checkUpdate: {
        method:'PUT',
        params : {
          route: ''
          }
        },

        getHistoricChecks: {
        method:'GET',
        params : {
          route: 'historic'
          }
        }
    })

    var banks = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/banks/:route',{},{

      getBanks: {
        method:'GET',
        params : {
          route: 'list'
          }
        }
    })

    return { checks : checks,
             banks:banks
             
           }
  }
}

  checksServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.checksServices', [])
  .service('checksServices', checksServices)
  .name;