
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> checksListController
* # As Controller --> checksList														
*/

export default class checksListController{

        constructor($scope,UserInfoConstant,$timeout,checksServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService,uiGridSelectionConstants){
            var vm = this
            $scope.selectedChecks = []
            vm.searchData=searchData
            vm.checkCreate= checkCreate
            vm.checkView= checkView
            vm.updateStatusCheck = updateStatusCheck
            vm.checkUpdate = checkUpdate
            vm.getHistoricalChecks = getHistoricalChecks
            vm.loadChecks = loadChecks
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            loadChecks()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.checksGrid = {
                enableFiltering: true,
                enableSelectAll: false,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'Listado_de_cheques.csv',
                isRowSelectable: function(row) {
                  if(row.entity.statusName == "PEN" || row.entity.statusName == "DEP"){
                          return true;
                        } else {
                          return false;
                        }
                },
                onRegisterApi: function(gridApi){
                  $scope.gridApi = gridApi;

                  gridApi.selection.on.rowSelectionChanged($scope,function(row,$index){
                    $scope.selectedChecks = gridApi.selection.getSelectedRows()
                    // $scope.selectedChecks = gridApi.selection.selectAllRows()  
                  });
                },
                columnDefs: [
                    { 
                      name:'id',
                      field: 'checkId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.checksList.checkView(row.entity)">{{row.entity.checkId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'N° Serie',
                      field: 'serialNumber',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secundary uppercase" style="font-size: 10px;"><strong>{{row.entity.serialNumber}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Nombre Asociado',
                      field: 'associatedName',
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;">{{row.entity.associatedName}}</p>' +
                                 '</div>'  
                    },
                    // { 
                    //   name:'N° Cuenta',
                    //   field: 'account',
                    //   width: '11%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.account}}</strong></p>' +
                    //              '</div>' 
                    // },
                     { 
                      name:'Banco',
                      field: 'bankName',
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning" style="font-size: 10px;">{{row.entity.bankName}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Fecha', 
                      field: 'day',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.day | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Facturas',
                      field: 'invoices',
                      width: '15%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;"><strong>{{row.entity.invoices}}</strong></p>' +
                                 '</div>'  
                    },
                   
                    { 
                      name:'Valor',
                      field: 'shareValue',
                      width: '8%',
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-accent" style="font-size: 12px;">$ {{row.entity.shareValue}}</p>' +
                                 '</div>' 
                    },
                    
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 11px;">{{row.entity.origin}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Ingreso', 
                      field: 'date',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    }, 
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '6%',
                      allowCellFocus : false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="badge bg-{{row.entity.styleStatus}}" style="font-size: 11px;">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: '',
                      width: '3%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          function loadChecks(){
            checksServices.checks.getChecks().$promise.then((dataReturn) => {
                  $scope.checksGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

          }
         	

        function searchData() {
              checksServices.checks.getChecks().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.checksGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

        function checkCreate(){
                var modalInstance  = $modal.open({
                        template: require('../create/checks-create.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'checksCreateController',
                        controllerAs: 'checksCreate',
                        size: 'lg'
                })
        }

        function checkUpdate(row){
          $scope.checksData = row;
                var modalInstance  = $modal.open({
                        template: require('../update/checks-update.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'checksUpdateController',
                        controllerAs: 'checksUpdate',
                        size: 'lg'
                })
        }

        function updateStatusCheck(){
            console.log('cheques seleccionados',$scope.selectedChecks)
            $scope.checksData = $scope.selectedChecks;
            var modalInstance  = $modal.open({
                        template: require('../actions/actions-check.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'actionsCheckController',
                        controllerAs: 'actionsCheck'
                }) 
        }

        function getHistoricalChecks(){

            var modalInstance  = $modal.open({
                        template: require('../historical/historical-checks.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'historicalChecksController',
                        controllerAs: 'historicalChecks',
                        size: 'lg'
                }) 
        }


        function checkView(row){
            $scope.checksData = row;
            var modalInstance  = $modal.open({
                    template: require('../view/checks-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'checksViewController',
                    controllerAs: 'checksView'
            })
        }

  

        

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

checksListController.$inject = ['$scope','UserInfoConstant','$timeout','checksServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService','uiGridSelectionConstants'];

