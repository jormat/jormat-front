/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> checksViewController
* # As Controller --> checksView														
*/


  export default class checksViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,checksServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.checkUpdateStatus = checkUpdateStatus;
	    $scope.checkId = $scope.checksData.checkId

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        loadCheck()

        function loadCheck() {
            let params = {
            checkId: $scope.checkId
          }

        checksServices.checks.getChecksDetails(params).$promise.then((dataReturn) => {

                  $scope.check = dataReturn.data;
                  console.log('$scope.check',$scope.check );
                
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

	    
	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function checkUpdateStatus(value) {
			 let params = {
                            value: value,
                            userId : $scope.userInfo.id,
                            checkId: $scope.checkId,
                            day: moment($scope.depositDate).format("YYYY-MM-DD")
                    }

                    checksServices.checks.checksStatusUpdate(params).$promise.then((dataReturn) => {
                    console.log('update check Ok');
                    $scope.result = dataReturn.data
                    $state.reload('app.checks')
                    loadCheck()
                    // $modalInstance.dismiss('chao');
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
		}

    }

  }

  checksViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','checksServices','$location','UserInfoConstant','ngNotify','$state'];
