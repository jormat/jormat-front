/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> checksUpdateController
* # As Controller --> checksUpdate														
*/


  export default class checksUpdateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,checksServices,$location,UserInfoConstant,ngNotify,$state,clientsServices,warehousesServices) {

    	var vm = this;
	    vm.cancel = cancel;
        vm.updateCheck = updateCheck
        vm.selectActions = selectActions
        vm.updateStatusCheck = updateStatusCheck
        $scope.checkId = $scope.checksData.checkId

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        loadCheck()

        function selectActions(value){
            console.log('value',value);
            $scope.value = value
             
        }


        function updateStatusCheck() {

                let params = {
                    value: $scope.value,
                    userId : $scope.userInfo.id,
                    checkId: $scope.checkId,
                    day: ''
              }

            checksServices.checks.checksStatusUpdate(params).$promise.then((dataReturn) => {
                    console.log('update check Ok');
                    $scope.result = dataReturn.data
                    $state.reload('app.checks')
                    // $modalInstance.dismiss('chao');
                    ngNotify.set('Se han actualizado correctamente los cheques','warn')
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            
        }

        function loadCheck() {
            let params = {
            checkId: $scope.checkId
          }

        checksServices.checks.getChecksDetails(params).$promise.then((dataReturn) => {

                  $scope.check = dataReturn.data;
                  $scope.checkDetails  = { 
                    associatedName : $scope.check[0].associatedName,
                    serialNumber : $scope.check[0].serialNumber,
                    day : $scope.check[0].day,
                    account : $scope.check[0].account,
                    bankName : $scope.check[0].bankName,
                    shareValue : $scope.check[0].shareValue,
                    shareDetails : $scope.check[0].shareDetails,
                    origin : $scope.check[0].origin,
                    phone : $scope.check[0].phone,
                    depositDate : $scope.check[0].depositDate,
                    userCreation : $scope.check[0].userCreation,
                    date : $scope.check[0].date,
                    userUpdate : $scope.check[0].userUpdate,
                    invoices : $scope.check[0].invoices,
                    associatedClient : $scope.check[0].associatedClient,
                    statusName : $scope.check[0].statusName

                  }

                  $scope.invoices=[]
                  if($scope.check[0].invoices.length>0){
                      for(var i in $scope.check[0].invoices){
                        $scope.invoices.push({title:$scope.check[0].invoices[i],done:false})
                      }
                    
                  }
                
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		} 

        function updateCheck() {
            console.log('update bank check');
            var tempRef=[]

                for(var t in $scope.invoices){
                  tempRef.push($scope.invoices[t].title)
                }

                let model = {
                    checkId: $scope.checkId,
                    associatedName: $scope.checkDetails.associatedName,
                    serialNumber:  $scope.checkDetails.serialNumber,
                    shareValue: $scope.checkDetails.shareValue,
                    account:  $scope.checkDetails.account,
                    phone:  $scope.checkDetails.phone,
                    shareDetails:  $scope.checkDetails.shareDetails,
                    invoices:tempRef,
                    userId: $scope.userInfo.id
                }    

                console.log('modelo',model)   

                checksServices.checks.checkUpdate(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha actualizado correctamente el cheque','success')
                    $scope.result = dataReturn.data
                    $state.reload('app.checks')
                    $modalInstance.dismiss('chao');

                  },(err) => {
                      ngNotify.set('Error al actualizar cheque','error')
                  })


        }

		$scope.invoices = [];

        $scope.addInvoice = function(invoice) {
            
            $scope.invoices.push({'title': $scope.invoice, 'done':false})
            $scope.invoice = ''
            // $scope.saveButton  = false
        }

        $scope.deleteInvoice = function(index) {  
            $scope.invoices.splice(index, 1);
        }

    }

  }

  checksUpdateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','checksServices','$location','UserInfoConstant','ngNotify','$state','clientsServices','warehousesServices'];
