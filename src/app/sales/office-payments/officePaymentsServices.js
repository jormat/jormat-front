/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> officePaymentsServices
* file for call at services for officePayments
*/

class officePaymentsServices {

	constructor($resource,SERVICE_URL_CONSTANT) {
		
		var officePayments = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/offices/:route',{},{
			
				getOfficePayments: {
						method:'GET',
						params : {
							 route: 'list'
						}
				},
				getOfficePaymentsDetails: {
						method:'GET',
						params : {
							 route: ''
						}
				},
				createPayments: {
						method:'POST',
						params : {
								route: ''
						}
				},
				getOfficePaymentsItems: {
						method:'GET',
						params : {
							 route: 'documents'
						}
				},
				getSalesByWarehouse: {
						method:'GET',
						params : {
								route: 'sales'
						}
				},
				officePaymentsUpdate: {
						method:'PUT',
						params : {
								route: 'validate'
						}
				},
				officePaymentsItemsUpdate: {
						method:'PUT',
						params : {
								route: 'documents-click'
						}
				}
		})

		return { officePayments : officePayments
						 
					 }
	}
}

	officePaymentsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

	export default  angular.module('services.officePaymentsServices', [])
	.service('officePaymentsServices', officePaymentsServices)
	.name;