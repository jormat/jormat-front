/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> officePaymentsPrintController
* # As Controller --> officePaymentsPrint														
*/

  export default class officePaymentsPrintController {

    constructor($scope, $filter,$rootScope, $http,officePaymentsServices,paymentsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams,$modal) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.validatePayment = validatePayment
        vm.getOfficePaymentsItems = getOfficePaymentsItems
        vm.documentValidate = documentValidate
        vm.viewDocument = viewDocument
        $scope.validateButton = false
        $scope.paymentId = $stateParams.paymentId
        $scope.clientName = ''

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        let params = {
              paymentId: $scope.paymentId
            }

          officePaymentsServices.officePayments.getOfficePaymentsDetails(params).$promise.then((dataReturn) => {
                $scope.officePayment = dataReturn.data
                },(err) => {
                    console.log('No se ha podido conectar con el servicio get officePayment Details',err);
                })

        getOfficePaymentsItems()

        function getOfficePaymentsItems(){

          officePaymentsServices.officePayments.getOfficePaymentsItems(params).$promise.then((dataReturn) => {
            $scope.officePaymentsItems = dataReturn.data

            },(err) => {
                console.log('No se ha podido conectar con el servicio get officePayment items',err);
            })

        }

    	function cancel() {

		 $state.go("app.officePayments")

		}

        function viewDocument(items){

            if (items.type == 'Factura') {

                $scope.invoiceData = {
                    invoiceId:items.documentId,
                    clientName:items.clientName
                }
                var modalInstance  = $modal.open({
                        template: require('../../clients-invoices/view/clients-invoices-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'clientsInvoicesViewController',
                        controllerAs: 'clientsInvoicesView',
                        size: 'lg'
                })
            }else{

                $scope.ballotsData = {
                    ballotId:items.documentId,
                    clientName:items.clientName
                }

                var modalInstance  = $modal.open({
                        template: require('../../ballots/view/ballots-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'ballotsViewController',
                        controllerAs: 'ballotsView',
                        size: 'lg'
                })

            }
                
        }

        function documentValidate(items,index) {

            let params = {
                invoiceId: items.documentId
            }

            let idContainer = {
                userId: $scope.userInfo.id,
                paymentItemId: items.id
            }

            let model = {
                    userId: $scope.userInfo.id,
                    invoiceId: items.documentId,
                    clientId : items.clientId,
                    total : items.pay,
                    paymentFormId : items.paymentFormId,
                    comment : 'Pago enviado desde '+  $scope.officePayment[0].origin
                }

 
            console.log(items,idContainer)

            officePaymentsServices.officePayments.officePaymentsItemsUpdate(idContainer).$promise.then((dataReturn) => {
                console.log('is click is 1')
              },(err) => {
                console.log('Error al actualizar isClick')
             })
            
            if (items.type == 'Factura') {

                //// automatic pay
                paymentsServices.payments.createPayment(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha generado el pago correctamente','warn')
                   
                    if (items.pay == items.pending) {
                        //// update document
                        paymentsServices.payments.updatePayment(params).$promise.then((dataReturn) => {
                            $scope.documentInvoices = dataReturn.data

                            ngNotify.set( 'La factura N°'+ items.documentId +' ahora se encuentra pagada', {
                              sticky: true,
                              button: true,
                              type : 'success'
                            })

                            },(err) => {
                                console.log('No se ha podido actualizar la Factura ',err);
                        })

                    }


                    getOfficePaymentsItems()

                  },(err) => {
                    ngNotify.set('Error al actualizar factura pago','error')
                 })
                
            }else{
                console.log('Documento tipo boleta',items.type);

                let paramsBallot = {
                    ballotId: items.documentId,
                    paymentMethodId: items.paymentFormId,
                    userId: $scope.userInfo.id,
                }
                //// automatic pay ballot
                    paymentsServices.payments.updateBallot(paramsBallot).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado la boleta de manera correctamente','success')
                        getOfficePaymentsItems()

                      },(err) => {
                        ngNotify.set('Error al actualizar boleta pago','error')
                     })
            }

        }

        function validatePayment(value){

          let model = {
                    officesPaymentId : $scope.paymentId,
                    userId : $scope.userInfo.id,
                    statusValue : value 
                }

                console.log('modelo',model)   

                officePaymentsServices.officePayments.officePaymentsUpdate(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha actualizado el pago de manera correcta','success')
                    // $scope.result = dataReturn.data
                    $state.go('app.officePayments')

                  },(err) => {
                      ngNotify.set('Error al actualizar pago','error')
                  })

        }

        function print() {
              window.print();

        }


    }

  }

  officePaymentsPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','officePaymentsServices','paymentsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams','$modal'];
