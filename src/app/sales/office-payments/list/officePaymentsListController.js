
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> officePaymentsListController
* # As Controller --> officePaymentsList														
*/

export default class officePaymentsListController{

        constructor($scope,UserInfoConstant,$timeout,officePaymentsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.viewOfficePayments = viewOfficePayments
            vm.paymentPrint = paymentPrint
            // vm.transformToInvoice = transformToInvoice
            vm.searchData=searchData
            vm.loadOfficePayments = loadOfficePayments
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            loadOfficePayments()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.paymentsGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'listado_pagos.csv',
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'paymentId',  
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.officePaymentsList.viewOfficePayments(row.entity)">{{row.entity.paymentId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Usuario', 
                      field: 'userCreation', 
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-secundary font-italic" style="font-size: 12px;">{{row.entity.userCreation}}</p>' +
                                 '</div>'   
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '16%'
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> <strong>${{row.entity.total | pesosChilenos}}</strong></p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '8%'
                    },
                    { 
                      name: 'Documentos', 
                      field: 'documents',
                      width: '22%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;"><strong>{{row.entity.documents}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<span class="label bg-{{row.entity.style}}">{{row.entity.statusName}}</span>'+
                                 '</div>'
                    },
                    
                    
                    { 
                      name: '',
                      width: '8%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadOfficePayments(){

                officePaymentsServices.officePayments.getOfficePayments().$promise.then((dataReturn) => {
                  $scope.paymentsGrid.data = dataReturn.data;
                  console.log('data',$scope.paymentsGrid.data)
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            }


            function searchData() {
              officePaymentsServices.officePayments.getOfficePayments().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.paymentsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }


            function paymentPrint(row){
            
              $state.go("app.officePaymentsPrint", { paymentId: row.paymentId});
            }

            function viewOfficePayments(row){
	            $scope.officePaymentsData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/office-payments-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'officePaymentsViewController',
	                    controllerAs: 'officePaymentsView',
	                    size: 'lg'
	            })
	          }

            // function transformToInvoice(row) {
            //   $state.go("app.clientsInvoicesUpdate", { idInvoice: row.quotationId, discount:row.discount,type:'cotizacion'});

            // }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

officePaymentsListController.$inject = ['$scope','UserInfoConstant','$timeout','officePaymentsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

