/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> officePaymentsViewController
* # As Controller --> officePaymentsView														
*/


  export default class officePaymentsViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,officePaymentsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
    	vm.transformToInvoice = transformToInvoice
      	$scope.paymentId = $scope.officePaymentsData.paymentId
      	$scope.clientName = $scope.officePaymentsData.userCreation

	      let params = {
	            paymentId: $scope.paymentId
	          }

	        officePaymentsServices.officePayments.getOfficePaymentsDetails(params).$promise.then((dataReturn) => {
	              $scope.officePayment = dataReturn.data
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get officePayment Details',err);
	              })

	        officePaymentsServices.officePayments.getOfficePaymentsItems(params).$promise.then((dataReturn) => {
	              $scope.officePaymentsItems = dataReturn.data
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get officePayment items',err);
	              })

	    	function cancel() {
				$modalInstance.dismiss('chao');
			}

			function transformToInvoice() {
              $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.paymentId,discount:$scope.officePayment[0].discount,type:'cotizacion'});

        }

    }

  }

  officePaymentsViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','officePaymentsServices','$location','UserInfoConstant','ngNotify','$state'];
