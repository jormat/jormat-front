
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> ballotsListController
* # As Controller --> ballotsList														
*/

export default class ballotsListController{

        constructor($scope,UserInfoConstant,$timeout,ballotsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService,$window,LOCAL_ENV_CONSTANT,$stateParams,clientsServices){
            var vm = this
            $scope.categoryClient = $stateParams.clientCategory
            $scope.clientId = $stateParams.clientId
            vm.viewBallots = viewBallots
            vm.loadBallots= loadBallots
            vm.deleteBallots = deleteBallots
            vm.searchData=searchData
            vm.createXML =createXML
            vm.printBallots = printBallots
            vm.paymentBallots = paymentBallots
            vm.updateDocument = updateDocument
            vm.consultCategory = consultCategory
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            loadBallots()

            //function to show userId
            $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                    }
                })
            //

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.ballotsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'listado_boletas.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'ballotId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.ballotsList.viewBallots(row.entity)">{{row.entity.ballotId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '28%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;"><strong>{{row.entity.clientName}}</strong></p>' +
                                 '</div>'        
                    },
                    { 
                      name: 'Rut', 
                      field: 'rut', 
                      width: '10%'
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '10%'
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '10%',
                      aggregationHideLabel: false,
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{row.entity.total | pesosChilenos}}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '8%'
                    },
                    { 
                      name: 'Usuario', 
                      field: 'userCreation', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" badge bg-secundary font-italic" style="font-size: 12px;">{{row.entity.userCreation}}</p>' +
                                 '</div>'   
                    },
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="{{(row.entity.statusName == \'Pagado\')?\'badge bg-warning\':\'badge bg-danger\'}}">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Acciones',
                      width: '8%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }


            function loadBallots(){
                ballotsServices.ballots.getBallots().$promise.then((dataReturn) => {
                  $scope.ballotsGrid.data = dataReturn.data;
                  if($scope.categoryClient==0){
                    consultCategory($scope.clientId)
                 }
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
            }

            function searchData() {
              ballotsServices.ballots.getBallots().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.ballotsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }

            function viewBallots(row){
	            $scope.ballotsData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/ballots-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'ballotsViewController',
	                    controllerAs: 'ballotsView',
	                    size: 'lg'
	            })
	          }


            function paymentBallots(row){

            $state.go("app.ballotPayments", { 
                idBallot: row.ballotId,
                idClient: row.clientId,
                clientName: row.clientName,
                total: row.total,
                date: row.date


            });

            }

            function printBallots(row){
              $state.go("app.ballotsPrint", { 
                idBallot: row.ballotId,
                warehouse: row.originId });
            }

           function createXML(row) { 
              // $window.open('http://localhost/jormat-system/XML_ballot.php?ballotId='+row.ballotId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
              $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_ballot.php?ballotId='+row.ballotId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
 
            }

            function deleteBallots(row){
              $scope.ballotData = row;
              var modalInstance  = $modal.open({
                      template: require('../delete/ballots-delete.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'ballotsDeleteController',
                      controllerAs: 'ballotsDelete'
              })
            }

            function updateDocument(row) {


                let params = {
                    userId : $scope.userInfo.id,
                    typeDocumentId : 6,
                    documentId : row.ballotId,
                    commentary : "Entrega Rápida",
                    option : 7
                }
        
                ballotsServices.documents.setDocumentList(params).$promise.then((dataReturn) => {
                    ngNotify.set('La entrega ha sido registrada exitosamente / Boleta: '+ row.ballotId,'warn');
                    loadBallots()
                },(err) => {
                    ngNotify.set('Error al actualizar documento','error')
                })
             
            }

            function consultCategory(idClient){
              $scope.idClient = idClient;
              var modalInstance  = $modal.open({
                  template: require('../create/updateBallotClients/updateBallotClient.html'),
                  animation: true,
                  scope: $scope,
                  controller: 'updateBallotClientController',
                  controllerAs: 'ballotClientController'
              })
          }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

ballotsListController.$inject = ['$scope','UserInfoConstant','$timeout','ballotsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService','$window','LOCAL_ENV_CONSTANT','$stateParams','clientsServices'];

