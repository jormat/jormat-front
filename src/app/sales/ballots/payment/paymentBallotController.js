/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentBallotController
* # As Controller --> paymentBallot														
*/


  export default class paymentBallotController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,paymentsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.ballotPay = ballotPay;
	    $scope.ballotId = $scope.ballotId
	    $scope.total = $scope.total
	    console.log('lorea',$scope.total)

	     //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {

                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

	    let params = {
            ballotId: $scope.ballotId
          }

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function ballotPay() {
              let paramsBallot = {
                    ballotId: $scope.ballotId,
                    paymentMethodId: $scope.paymentMethod,
                    userId: $scope.userInfo.id
                }

                console.log('model',paramsBallot);
                // automatic pay ballot
                    paymentsServices.payments.updateBallot(paramsBallot).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado la boleta de manera correctamente','success')
                        $state.reload('app.ballotsPrint')

                      },(err) => {
                        ngNotify.set('Error al actualizar boleta pago','error')
                     })

        }

		


    }

  }

  paymentBallotController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','paymentsServices','$location','UserInfoConstant','ngNotify','$state'];
