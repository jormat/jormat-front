/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> ballotsViewController
* # As Controller --> ballotsView
*/
export default class ballotsViewController {
	constructor($scope, $filter,$rootScope, $http,$modalInstance,ballotsServices,$location,UserInfoConstant,ngNotify,$state,Dialogs) {
		this.btnVisible = false;
		this.ngNotify = ngNotify
		this.$scope = $scope;
		this.Dialogs = Dialogs;
		this.$state = $state;
		this.UserInfoConstant = UserInfoConstant[0].details;
		this.$modalInstance = $modalInstance;
		this.ballotsSrv = ballotsServices;
		this.$scope.ballotId = this.$scope.ballotsData.ballotId;
		this.$scope.originId = this.$scope.ballotsData.originId;
		this.$scope.clientName = this.$scope.ballotsData.clientName;
		this.loadData();
	}

	loadData(){
		let params = {
			ballotId: this.$scope.ballotId,
			originId:this.$scope.originId

		}

		this.ballotsSrv.ballots.getBallotsDetails(params).$promise.then((dataReturn) => {
			this.$scope.ballot = dataReturn.data
			this.btnVisible = (this.$scope.ballot[0].userOut) ? false : true;
		},(err) => {
			console.log('No se ha podido conectar con el servicio get Invoices Details',err);
		})

		this.ballotsSrv.ballots.getBallotsItems(params).$promise.then((dataReturn) => {
			this.$scope.ballotsItems = dataReturn.data
			var subTotal = 0
	              var i = 0
	                 for (i=0; i<this.$scope.ballotsItems.length; i++) {
	                     subTotal = subTotal + this.$scope.ballotsItems[i].total  
	                  }
	                  
	                  this.$scope.subTotality =  subTotal
		},(err) => {
			console.log('No se ha podido conectar con el servicio get Invoices Details',err);
		})
	}

	cancel() {
		this.$modalInstance.dismiss('chao');
	}

	printPackOff() {
		console.log('imprimir PackOff');
		var url = this.$state.href("app.printPackOffBallot",{
			idBallot: this.$scope.ballotId
		});

		window.open(url,'_blank',"width=600,height=700");

	}

	updatePackOff() {
		this.params = {
					userId : this.UserInfoConstant[0].user.id,
					ballotId : this.$scope.ballotId
				}

		console.log('parametros',this.params);
		
		this.ballotsSrv.ballots.updatePackOff(this.params).$promise.then((dataReturn) => {
					this.ngNotify.set('Boleta a sido actualizada con exito','warn');
					this.loadData();
				},(err) => {
					this.ngNotify.set('Error al actualizar documento','error')
				})
	}

	updateDocument(){
		
		this.$scope.isEdit = true;

		this.params = {
					userId : this.UserInfoConstant[0].user.id,
					typeDocumentId : this.$scope.ballot[0].typeId,
					documentId : this.$scope.ballotId,
					commentary : "Entrega Rápida",
                    option : 7
				}

				this.ballotsSrv.documents.setDocumentList(this.params).$promise.then((dataReturn) => {
					this.ngNotify.set('La entrega ha sido registrada exitosamente','warn');
					this.$state.reload('app.warehouseDeliveries')
					this.$modalInstance.dismiss('chao');
				},(err) => {
					this.ngNotify.set('Error al actualizar documento','error')
				})
	}
}
ballotsViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','ballotsServices','$location','UserInfoConstant','ngNotify','$state','Dialogs'];
