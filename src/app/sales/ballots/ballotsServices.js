/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> ballotsServices
* file for call at services for ballots
*/

class ballotsServices {

	constructor($resource,SERVICE_URL_CONSTANT) {
		
		const ballots = $resource(SERVICE_URL_CONSTANT.jormat + '/ballots/:route',{},{
			
				getBallots: {
						method:'GET',
						params : {
							 route: 'list'
						}
				},
				getBallotsDetails: {
						method:'GET',
						params : {
							 route: ''
						}
				},
				createBallot: {
						method:'POST',
						params : {
								route: ''
						}
				},
				getBallotsItems: {
						method:'GET',
						params : {
							 route: 'items'
						}
				},
				updateBallot: {
						method:'PUT',
						params : {
								route: ''
						}
				},
				disabledBallot: {
						method:'DELETE',
						params : {
								route: ''
						}
				},
				updatePackOff: {
						method:'PUT',
						params : {
								route: 'packoff'
						}
				},
		})

		const documents = $resource(SERVICE_URL_CONSTANT.jormat + '/warehouseDeliveries/:route',{},{
			setDocumentList: {
				method:'POST',
				params : {
					route: 'update'
				}
			},
		})

		return {
			ballots : ballots,
			documents: documents
		}
	}
}

	ballotsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

	export default  angular.module('services.ballotsServices', [])
	.service('ballotsServices', ballotsServices)
	.name;