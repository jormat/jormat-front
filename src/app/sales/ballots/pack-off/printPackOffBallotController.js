/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> printPackOffBallotController
* # As Controller --> printPackOffBallot														
*/


  export default class printPackOffBallotController {

    constructor($scope, $filter,$rootScope, $http,ballotsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        $scope.ballotId = $stateParams.idBallot

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        let model = {
                ballotId: $scope.ballotId
            }

        ballotsServices.ballots.getBallotsDetails(model).$promise.then((dataReturn) => {
              $scope.ballot = dataReturn.data
              $scope.ballotDetails  = { 
                    clientName : $scope.ballot[0].clientName,
                    rut : $scope.ballot[0].rut,
                    phone : $scope.ballot[0].phone,
                    city : $scope.ballot[0].city,
                    address : $scope.ballot[0].address,
                    transportationName: $scope.ballot[0].transportationName

                  }
                  if($scope.invoice[0].receiver == "undefined" ){
                    $scope.observation = ""

                  }else{
                    $scope.observation = "Recepciona:"+$scope.ballot[0].receiver

                  }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

    	function cancel() {

		 window.close();
         
		}


        function print() {
              window.print();

        }


    }

  }

  printPackOffBallotController.$inject = ['$scope', '$filter','$rootScope', '$http','ballotsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
