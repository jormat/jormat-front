
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> ballotsCreateController
* # As Controller --> ballotsCreate														
*/

export default class ballotsCreateController{

        constructor($scope,UserInfoConstant,$timeout,ballotsServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element,$window,LOCAL_ENV_CONSTANT,transportationServices){
            var vm = this;
            vm.createBallot = createBallot
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.setTransportation = setTransportation
            vm.packOffSelect = packOffSelect
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getStock = getStock
            vm.getNet = getNet
            vm.showGeneralDiscount = showGeneralDiscount
            vm.searchClient = searchClient
            vm.loadItems = loadItems
            vm.webItemUpdate = webItemUpdate
            vm.invoiceCreate = invoiceCreate
            vm.validateClient = validateClient
            $scope.buttonInvoice = true
            $scope.packOff = false
            $scope.parseInt = parseInt
            $scope.buttoncreateBallot = true
            $scope.origenAdd = false
            $scope.discount = 0
            $scope.purchaseOrder = 0
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }
            $scope.historyItems = []
            vm.searchData = searchData

            function searchData() {
              itemsServices.items.getItems().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.itemsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

            transportationServices.transportation.getTransportation().$promise.then((dataReturn) => {
              $scope.transportation = dataReturn.data;
                 console.log('$scope.transportationGrid',$scope.transportation);
             },(err) => {
                 console.log('No se ha podido conectar con el servicio',err);
             })

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.origenAdd = true
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            loadItems()
            // setWarehouseId()

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-disabled="grid.appScope.buttonMore" ng-click="grid.appScope.ballotsCreate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                   '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.ballotsCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{row.entity.vatPrice}}</strong></p>' +
                                 '</div>'  ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    }
                ]
            };
            
          
            function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            function packOffSelect(value){
              console.log('packOffSelect',value)
              if(value == true){
                $scope.transporteView = true
              }else{
                $scope.transporteView = false
              }
              

            }

            
            function setTransportation(transportation) {
              console.log(transportation)
              $scope.transportationName = transportation.transportName
              $scope.transportationId = transportation.transportationId
            }

            $scope.invoiceItems = []
           
            function loadItem(row){

              var i = 0
              for (i=0; i<$scope.invoiceItems.length; i++) {
                    var id = $scope.invoiceItems[i].itemId

                    if(id == row.itemId){
                      ngNotify.set('Item '+ row.itemId +' Duplicado en este Documento','warn')
                      $scope.invoiceItems.splice(index, 1)

                    }else{
                      console.log("item no puplicado",row.itemId)
                    }
                    
                  }
                 
                
                if (row.failure == 1) { 

                  ngNotify.set( 'Item '+ row.itemId +' con FALLA, valide con supervisor antes de continuar', {
                        sticky: true,
                        button: true,
                        type : 'error'
                    })

                }else{

                    $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    oil: row.oil,
                    price:Math.round((row.netPrice)*1.19),
                    minPrice: Math.round((row.netPrice)*1.19),
                    webType: row.webType,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                   })

                   getStock(row.itemId,1)
                   if (row.oil == 1) {
                      $scope.discountButton = true
                      // $scope.discount = 0
                    }
                   $scope.warningItems = false  
                }
                        
            }

            function showGeneralDiscount(value){

                if (value ==  0 || value ==  null ) {
                    console.log('muestra boton',value); 
                    $scope.discountButton = false

                }else{
                    console.log('esconde boton',value); 
                    $scope.discountButton = true
                    $scope.discount = 0

                }
            
            }

            function getStock(itemId,quantity){
            
              let params = {
                itemId: itemId,
                warehouseId :$scope.warehouseId 
            }

            itemsServices.items.getStock(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data
              $scope.stock = $scope.item[0].stock
              $scope.notStock = 0
              $scope.warningQuantity = false
              $scope.buttonMore = false 
              if (quantity > $scope.stock) {

                ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock ,'warn')
                $scope.notStock = 1
                $scope.warningQuantity = true
                $scope.buttonMore = true
              }
              
              return false;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
    
            }

            function getNet() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 

                  }
                  
                  $scope.NET =  net
                  $scope.netoInvoice = net - (($scope.discount*net)/100) 
                  // $scope.total = Math.round($scope.netoInvoice * 1.19)
                  $scope.total = $scope.netoInvoice
                  $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                  return net;
                      
            }


            function removeItems(index,oil) {
                $scope.invoiceItems.splice(index, 1)
                $scope.warningQuantity = false
                $scope.buttonMore = false

                if (oil == 1) { 
                $scope.discountButton = false
               }

               var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var id = $scope.invoiceItems[i].itemId
                    var stockId = $scope.invoiceItems[i].quantity
                    console.log("item x",id,stockId)
                    getStock(id,stockId)
                }
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
              $scope.warehouseId = warehouseId
              console.log('origen seleccionado',$scope.warehouseId);
            }

            function setName(clients) {
                console.log(clients)
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedTwo = clients.rut
                if (clients.locked == 1) {
                    ngNotify.set('Cliente bloqueado imposible crear boleta','error')
                    $scope.buttonInvoice = false
                    $scope.lockedClient = true

                }else{
                    $scope.buttonInvoice = true
                    $scope.lockedClient = false
                }
            }

            function showPanel() {
                $scope.panelInvoice = true
                $scope.invoiceItems = []
                $scope.warningQuantity = false
                $scope.buttonMore = false
            }

            function createBallot() {

                        let model = {
                            userId: $scope.userInfo.id,
                            clientId : $scope.clientId,
                            date : $scope.CurrentDate,
                            originId : $scope.warehouseId,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            comment : $scope.observation,
                            packOff:  $scope.packOff,
                            transportationId:  $scope.transportationId, 
                            transportationName:  $scope.transportationName, 
                            paymentmethodId : $scope.paymentmethodId,
                            itemsInvoices : $scope.invoiceItems 
                        }      

                      
                       console.log("invoicesitems", $scope.invoiceItems);

                       for(var  i in model.itemsInvoices){
                        let modelo = {
                            itemId: model.itemsInvoices[i].itemId,
                            originId :$scope.warehouseId,
                            quantity: model.itemsInvoices[i].quantity
                         }
        
                            itemsServices.items.getHistoric(modelo).$promise.then((dataReturn) => {
                               $scope.item = dataReturn.data;
    
                                $scope.historyItems.push({
                                itemId: $scope.item[0].itemId,
                                itemDescription: $scope.item[0].itemDescription,
                                location : $scope.item[0].locations,
                                previousStock: $scope.item[0].stock,
                                price: $scope.item[0].vatPrice,
                                quantity: modelo.quantity,
                                generalStock: $scope.item[0].generalStock,
                                currentprice: $scope.item[0].vatPrice,
                                currentLocation: $scope.item[0].locations
                                })
                                    
                                console.log('that', $scope.historyItems);
    
                                if (i== $scope.historyItems.length-1) {
                                      
                                  invoiceCreate(model,$scope.historyItems)
                                    $scope.ultimo = "último registro";
                                  } else {
                                    $scope.ultimo = "item"+i;
                                  }
                                    console.log( $scope.ultimo); 
                                   },(err) => {
                                    console.log('No se ha podido conectar con el servicio',err);
                                })
                    }

                       
            }

            function invoiceCreate(model,historyItems){

                console.log('now',model,historyItems)

                ballotsServices.ballots.createBallot(model).$promise.then((dataReturn) => {
                  ngNotify.set('Se ha creado la boleta correctamente','success')
                  $scope.ballotId = dataReturn.data
                  
                  for(var  i in historyItems){

                    let itemsValue = {
                      documentId: $scope.ballotId,
                      itemId: historyItems[i].itemId,
                      itemDescription: historyItems[i].itemDescription,
                      location : historyItems[i].location,
                      previousStock: historyItems[i].previousStock,
                      price: historyItems[i].price,
                      quantity: historyItems[i].quantity,
                      originId: $scope.warehouseId,
                      userId: $scope.userInfo.id,
                      document: "BOL",
                      type: "Salida -",
                      generalStock: historyItems[i].generalStock,
                      currentprice: historyItems[i].price,
                      currentLocation: historyItems[i].currentLocation,
                      destiny: undefined

                    }
                   
                   console.log('that',itemsValue);
     
                   itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                          $scope.result2 = dataReturn.data;
                          console.log('Historial del item actualizado correctamente');
                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                      })
                  }

                  //$window.open('http://localhost/jormat-system/XML_ballot.php?ballotId='+$scope.ballotId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
                  $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_ballot.php?ballotId='+$scope.ballotId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
                  ngNotify.set('Documento enviado a SII N° BOL ' +$scope.ballotId+ ' / Validar en Facturas Chile','warn')
                  $state.go('app.ballots')

                  for(var  i in model.itemsInvoices){

                      console.log('entra',model.itemsInvoices)

                      if (model.itemsInvoices[i].webType == "SI"){
                          console.log('tipo web')

                          let modelo = {
                              itemId : model.itemsInvoices[i].itemId
                          }

                          itemsServices.items.getGeneralStockWeb(modelo).$promise.then((dataReturn) => {
                             $scope.result = dataReturn.data;
                             $scope.stock = $scope.result[0].generalStock;

                             webItemUpdate(modelo.itemId,$scope.stock)

                            },(err) => {
                                console.log('No se ha podido conectar con el servicio',err);
                            })

                           }
                      else{
                          console.log('normal')
                      }
                   }

                },(err) => {
                    ngNotify.set('Error al crear boleta','error')
                })


            }

            function webItemUpdate(itemId,stock) {

                let data = {
                                    sku : itemId.toString(),
                                    stock_quantity: stock
                                }

                                itemsServices.woocommerceAPI.itemUpdate(data).$promise.then((dataReturn) => {
                                  $scope.result = dataReturn.data;
                                  $scope.message= dataReturn.message;
                                  $scope.status= dataReturn.status;

                                  if ($scope.status==200  ) {

                                     ngNotify.set('Mensaje sitio web: '+ $scope.message,'info')

                                      }else{
                                        console.log('Error actualizar stock web',$scope.message);
                                        ngNotify.set( 'Error actualización ITEM en el sitio web: ' + $scope.message, {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                        })
                                        
                                     }
                                  
                                  },(err) => {
                                        console.log('Error actualizar stock web',err);
                                        ngNotify.set( 'Error API Web', {
                                            sticky: true,
                                            button: true,
                                            type : 'warn'
                                      })
                                  })

            }

            function searchClient (value) {

            let model = {
                name: value
            }

            clientsServices.clients.getClientsByNameCategory(model).$promise.then((dataReturn) => {
                  $scope.clients = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            return false;
            }

            function validateClient(){
              let model= {
               clientId : $scope.clientId
              }
               if(model.clientId==undefined){
                 confirmationClient()
               }else{
                 createBallot()
               }   
             }
             
             function confirmationClient(){
              $scope.model = {
                userId: $scope.userInfo.id,
                date : $scope.CurrentDate,
                originId : $scope.warehouseId,
                priceVAT : $scope.VAT,
                netPrice : $scope.NET,
                discount : $scope.discount,
                total : $scope.total,
                comment : $scope.observation,
                name : $scope.name,
                phone : $scope.phone,
                paymentmethodId : $scope.paymentmethodId,
                itemsInvoices : $scope.invoiceItems 
            } 
        
              $scope.ballottsData = $scope.model
              console.log("datos boleta::", $scope.ballottsData)
              var modalInstance  = $modal.open({
                      template: require('./confirmBallot/confirmationBallotClient.html'),
                      animation: true,
                      scope: $scope,
                      controller: 'confirmationBallotClientController',
                      controllerAs: 'confirmationBallotClient'
              })
        }
          


        }//FIN CONSTRUCTOR

        // Funciones
    }   

ballotsCreateController.$inject = ['$scope','UserInfoConstant','$timeout','ballotsServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element','$window','LOCAL_ENV_CONSTANT','transportationServices'];