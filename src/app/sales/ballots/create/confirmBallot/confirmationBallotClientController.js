/**
* @description
* Controller to confirmations
* @controller 
* # name: confirmationBallotClientController
* # alias: confirm
*/

export default class confirmationBallotClientController {
	constructor($scope, $mdDialog, ngNotify,ballotsServices, clientsServices,$modal,$state,$modalInstance,$window,itemsServices,LOCAL_ENV_CONSTANT) {
		var vm = this
		this.$scope = $scope
		vm.createBallots = createBallots
        vm.invoiceCreate = invoiceCreate
		$scope.model = $scope.ballottsData
		vm.close = close
		vm.save = save
        vm.saveAnon = saveAnon
		ngNotify = ngNotify
		this.$scope.rutValidate = true
        this.idClient=4934
        //console.log("datos boleta222:::",$scope.ballotsData)
		function save(){
			$scope.rutValidate = true
            //console.log("datos boleta222:::",$scope.model)
			let model = {
				fullName: $scope.fullName,
				rut: $scope.rut,
				mobile: $scope.mobile,
				email: $scope.email,
				category:0
			}
	
				clientsServices.clients.getClientsRut(model).$promise.then((dataReturn) => {
				$scope.resultRut = dataReturn.data;
				if ($scope.resultRut.length == 0) {
	
					clientsServices.clients.createClient(model).$promise.then((dataReturn) => {
							ngNotify.set('Se ha creado el cliente correctamente','success')
							
                            $scope.data = dataReturn.data;
							
							createBallots($scope.ballottsData,dataReturn.data)
							$state.go('app.ballots');
							$modalInstance.dismiss('chao');
							
					 	 },(err) => {
						  	ngNotify.set('Error al crear cliente','error')
						  	$scope.rutValidate = false
						})
	
					}else{
						//cliente ya se encuentra registrado, actualiza categoria
						$scope.data = dataReturn.data;
						$scope.clientId = $scope.data[0].id;
						$scope.category = $scope.data[0].category;
						//$scope.categoryClient = 0
						createBallots($scope.model,$scope.clientId,$scope.category)
					}

			  },(err) => {
				  ngNotify.set('Error al consultar rut cliente','error')
				  $scope.rutValidate = false
			})
			
		}
        function saveAnon(){
			createBallots($scope.ballottsData,this.idClient)
			$state.go('app.ballots');
			$modalInstance.dismiss('chao');
		}

		function close() {
			$modalInstance.dismiss('chao');
		}


		function createBallots(modelB,clientId,category) {
			//close()
			$scope.modelo = {
					userId: $scope.userInfo.id,
                      clientId : clientId,
                      date : $scope.CurrentDate,
                      originId : modelB.originId,
                      priceVAT : modelB.priceVAT,
                      netPrice : modelB.netprice,
                      discount : modelB.discount,
                      total : modelB.total,
                      comment : modelB.observation,
                      packOff:  $scope.packOff,
                      transportationId:  modelB.transportationId, 
                      transportationName:  modelB.transportationName, 
                      paymentmethodId : modelB.paymentmethodId,
                      itemsInvoices : $scope.invoiceItems,
                      categoryClient : category
			}
            console.log("datos boleta33::", $scope.modelo)
            for(var  i in modelB.itemsInvoices){
                let model = {
                    itemId: modelB.itemsInvoices[i].itemId,
                    originId :$scope.warehouseId,
                    quantity: modelB.itemsInvoices[i].quantity
                 }

                    itemsServices.items.getHistoric(model).$promise.then((dataReturn) => {
                       $scope.item = dataReturn.data;

                        $scope.historyItems.push({
                        itemId: $scope.item[0].itemId,
                        itemDescription: $scope.item[0].itemDescription,
                        location : $scope.item[0].locations,
                        previousStock: $scope.item[0].stock,
                        price: $scope.item[0].vatPrice,
                        quantity: model.quantity,
                        generalStock: $scope.item[0].generalStock,
                        currentprice: $scope.item[0].vatPrice,
                        currentLocation: $scope.item[0].locations
                        })
                            
                        console.log('that', $scope.historyItems);

                        if (i== $scope.historyItems.length-1) {
                              
                          invoiceCreate($scope.modelo,$scope.historyItems,$scope.modelo.categoryClient,$scope.modelo.clientId)
                            $scope.ultimo = "último registro";
                          } else {
                            $scope.ultimo = "item"+i;
                          }
                            console.log( $scope.ultimo); 
                           },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
            }

		
		}

        function invoiceCreate(model,historyItems,category,clientId){

            console.log('now',model,historyItems)

            ballotsServices.ballots.createBallot(model).$promise.then((dataReturn) => {
              ngNotify.set('Se ha creado la boleta correctamente','success')
              $scope.ballotId = dataReturn.data
              
              for(var  i in historyItems){

                let itemsValue = {
                  documentId: $scope.ballotId,
                  itemId: historyItems[i].itemId,
                  itemDescription: historyItems[i].itemDescription,
                  location : historyItems[i].location,
                  previousStock: historyItems[i].previousStock,
                  price: historyItems[i].price,
                  quantity: historyItems[i].quantity,
                  originId: $scope.warehouseId,
                  userId: $scope.userInfo.id,
                  document: "BOL",
                  type: "Salida -",
                  generalStock: historyItems[i].generalStock,
                  currentprice: historyItems[i].price,
                  currentLocation: historyItems[i].currentLocation,
                  destiny: undefined

                }
               
               console.log('that',itemsValue);
 
               itemsServices.items.historicalItems(itemsValue).$promise.then((dataReturn) => {
                      $scope.result2 = dataReturn.data;
                      console.log('Historial del item actualizado correctamente');
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
              }

              //$window.open('http://localhost/jormat/jormat-system/XML_ballot.php?ballotId='+$scope.ballotId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180");
              $window.open(LOCAL_ENV_CONSTANT.pathXML+'XML_ballot.php?ballotId='+$scope.ballotId, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=300,left=500,width=470,height=180"); 
              ngNotify.set('Documento enviado a SII N° BOL ' +$scope.ballotId+ ' / Validar en Facturas Chile','warn')
              $state.go('app.ballots', { clientCategory: category, clientId:clientId});

              for(var  i in model.itemsInvoices){

                  console.log('entra',model.itemsInvoices)

                  if (model.itemsInvoices[i].webType == "SI"){
                      console.log('tipo web')

                      let modelo = {
                          itemId : model.itemsInvoices[i].itemId
                      }

                      itemsServices.items.getGeneralStockWeb(modelo).$promise.then((dataReturn) => {
                         $scope.result = dataReturn.data;
                         $scope.stock = $scope.result[0].generalStock;

                         webItemUpdate(modelo.itemId,$scope.stock)

                        },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })

                       }
                  else{
                      console.log('normal')
                  }
               }

            },(err) => {
                ngNotify.set('Error al crear boleta','error')
            })


        }

	}//fin constructor

	validate() {
		if (this.$scope.fullName === undefined) {
			return this.ngNotify.set('Agregue nombre cliente','warn')
		}


	}

	checkRut(rut) {
		if(rut === undefined) {
			this.$scope.textValidation = "Digite Rut";
			this.$scope.rutValidate = true;  
			return false;
		}

		let valor = rut.replace('.','')
		valor = valor.replace('-','')
		this.$scope.cuerpo = valor.slice(0,-1)
		this.$scope.dv = valor.slice(-1).toUpperCase()
		rut = this.$scope.cuerpo + '-'+ this.$scope.dv
		if(this.$scope.cuerpo.length < 7) {
			this.$scope.textValidation = "Incompleto";
			this.$scope.rutValidate = true;  
			return false;
		}
		
		this.$scope.suma = 0
		this.$scope.multiplo = 2

		for(this.$scope.i=1;this.$scope.i<=this.$scope.cuerpo.length;this.$scope.i++) {
			this.$scope.index = this.$scope.multiplo * valor.charAt(this.$scope.cuerpo.length -this.$scope.i)
			this.$scope.suma = this.$scope.suma + this.$scope.index

			if(this.$scope.multiplo < 7) { this.$scope.multiplo = this.$scope.multiplo + 1; } else { this.$scope.multiplo = 2; }
	  
		}
		
		this.$scope.dvEsperado = 11 - (this.$scope.suma % 11)
		this.$scope.dv = (this.$scope.dv == 'K')?10:this.$scope.dv
		this.$scope.dv = (this.$scope.dv == 0)?11:this.$scope.dv
		
		if(this.$scope.dvEsperado != this.$scope.dv) {
			this.$scope.textValidation = "RUT Inválido";
			this.$scope.rutValidate = true;
			return false; 
		}
		
		this.$scope.textValidation = "RUT Válido";
		this.$scope.rutValidate = false
	}
}

confirmationBallotClientController.$inject = ['$scope', '$mdDialog', 'ngNotify','ballotsServices','clientsServices', '$modal', '$state', '$modalInstance', '$window','itemsServices','LOCAL_ENV_CONSTANT']