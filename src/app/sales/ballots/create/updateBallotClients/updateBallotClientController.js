/**
* @name APP jormat
* @autor fperez
* @description
* # name controller --> updateBallotClientController
* # As Controller --> ballotClientController														
*/


export default class updateBallotClientController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,paymentsServices,$location,UserInfoConstant,ngNotify,$state,$window,clientsServices) {
    	var vm = this;
    	vm.cancel = cancel;
	    vm.updateClient = updateClient
	    $scope.clientId =  $scope.idClient
        
            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })

            let model = {
                clientId:  $scope.clientId
              }
    
              clientsServices.clients.getClientsDetails(model).$promise.then((dataReturn) => {
                $scope.data = dataReturn.data;
                $scope.fullName = $scope.data[0].fullName;
                $scope.rut = $scope.data[0].rut;
                $scope.clientId = clientId;
                },(err) => {
                  ngNotify.set('Error al traer al cliente','error')
                  $scope.rutValidate = false
              })    
            

	    function cancel() {
            $modalInstance.dismiss('chao');
		}

        function updateClient(){
            $window.open('../app/clientes/actualizar-cliente?idClient='+$scope.clientId, '_self'); 
        } 
    }

  }

  updateBallotClientController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','paymentsServices','$location','UserInfoConstant','ngNotify','$state','$window','clientsServices'];
