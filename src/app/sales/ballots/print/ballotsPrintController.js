/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> ballotsPrintController
* # As Controller --> ballotsPrint														
*/


  export default class ballotsPrintController {

    constructor($scope, $filter,$rootScope, $http,ballotsServices,paymentsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams,$modal) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.ballotPay = ballotPay
        vm.warehouseBallotPrint = warehouseBallotPrint
        $scope.ballotId = $stateParams.idBallot
        $scope.originId = $stateParams.warehouse

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {

                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

       

        let params = {
            ballotId: $scope.ballotId,
            originId: $scope.originId
        }

        ballotsServices.ballots.getBallotsDetails(params).$promise.then((dataReturn) => {
              $scope.ballots = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

        ballotsServices.ballots.getBallotsItems(params).$promise.then((dataReturn) => {
              $scope.ballotsItems = dataReturn.data
              var subTotal = 0
              var i = 0
                 for (i=0; i<$scope.ballotsItems.length; i++) {
                     subTotal = subTotal + $scope.ballotsItems[i].total  
                  }
                  $scope.subTotality =  subTotal
                  
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
          })

    	function cancel() {

		 $state.go("app.ballots")
		}

        function print() {
              window.print();

        }

        function warehouseBallotPrint(){

            console.log('imprimir print ');
            var url = $state.href("app.warehouseBallotPrint",{
                idBallot: $scope.ballotId,
                warehouse: $scope.ballots[0].originId
            });

        window.open(url,'_blank',"width=600,height=700");

          // $state.go("app.warehouseQuotationPrint", { idQuotation: $scope.quotationId,warehouse:$scope.originId});

        }

         function ballotPay(){
                $scope.ballotId = $scope.ballotId
                $scope.total = $scope.ballots[0].total
                var modalInstance  = $modal.open({
                        template: require('../payment/payment-ballot.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'paymentBallotController',
                        controllerAs: 'paymentBallot',
                })
        }

    }

  }

  ballotsPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','ballotsServices','paymentsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams','$modal'];
