/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> warehouseBallotsPrintController
* # As Controller --> warehouseBallotsPrint														
*/


  export default class warehouseBallotsPrintController {

    constructor($scope, $filter,$rootScope, $http,ballotsServices,paymentsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams,$modal) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.ballotPay = ballotPay
        $scope.ballotId = $stateParams.idBallot
        $scope.originId = $stateParams.warehouse

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {

                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

       

        let params = {
            ballotId: $scope.ballotId,
            originId: $scope.originId
        }

        ballotsServices.ballots.getBallotsDetails(params).$promise.then((dataReturn) => {
              $scope.ballots = dataReturn.data
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
              })

        ballotsServices.ballots.getBallotsItems(params).$promise.then((dataReturn) => {
              $scope.ballotsItems = dataReturn.data
              var subTotal = 0
              var i = 0
                 for (i=0; i<$scope.ballotsItems.length; i++) {
                     subTotal = subTotal + $scope.ballotsItems[i].total  
                  }
                  $scope.subTotality =  subTotal
                  
              },(err) => {
                  console.log('No se ha podido conectar con el servicio get Invoices Details',err);
          })

    	function cancel() {

            window.close();
		}

        function print() {
              window.print();

        }

         function ballotPay(){
                $scope.ballotId = $scope.ballotId
                $scope.total = $scope.ballots[0].total
                var modalInstance  = $modal.open({
                        template: require('../payment/payment-ballot.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'paymentBallotController',
                        controllerAs: 'paymentBallot',
                })
        }

    }

  }

  warehouseBallotsPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','ballotsServices','paymentsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams','$modal'];
