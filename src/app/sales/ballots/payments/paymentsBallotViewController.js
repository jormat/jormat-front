
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> paymentsBallotViewController
* # As Controller --> paymentsBallotView														
*/

export default class paymentsBallotViewController{

        constructor($scope,UserInfoConstant,$timeout,paymentsServices,clientsServices,clientsInvoicesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams,warehousesServices,itemsServices,$modal){
            var vm = this
            vm.viewBallots = viewBallots 
            vm.deletePayment = deletePayment
            vm.createPayment = createPayment
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.payment = 0

    
            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user,
                        $scope.userId = $scope.userInfo.id
                        // getPayments($scope.userId)
                        getPayments($stateParams.idBallot,$scope.userId)
                    }
                })
            //

            paymentsServices.paymentForms.getPaymentForms().$promise.then((dataReturn) => {
              $scope.paymentForms = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getPaymentForms',err);
            })

            $scope.ballotId = $stateParams.idBallot
            $scope.clientId = $stateParams.idClient
            $scope.clientName = $stateParams.clientName
            $scope.total = $stateParams.total
            $scope.date = $stateParams.date
            $scope.status = $stateParams.status

            if ($scope.pending == 0) {
                $scope.statusPayment = 'Pagado'
                
            }else{
                $scope.statusPayment = 'Sin Pagar'
            }

        
            function getPayments(ballotId,userId){
                let params = {
                    ballotId: ballotId,
                    userId: userId
                }

                console.log('eso',$stateParams.idBallot,$scope.ballotId)

                console.log('params',params)
                paymentsServices.payments.getBallotPayments(params).$promise.then((dataReturn) => {
                  $scope.paymentsGrid = dataReturn.data;
                  console.log('taht',$scope.paymentsGrid )
                  var sumPayments = 0
                  var i = 0
                     for (i=0; i<$scope.paymentsGrid.length; i++) {
                         sumPayments = sumPayments + $scope.paymentsGrid[i].total  
                      }

                      $scope.sum =  sumPayments
                      $scope.pending = $stateParams.total - $scope.sum
                      if ($scope.pending == 0) {
                            $scope.statusPayment = 'Pagado'
                            paymentsServices.payments.updateBallotStatus(params).$promise.then((dataReturn) => {
                                ngNotify.set('Boleta pagada completamente','warn')
                            },(err) => {
                                console.log('No se ha podido conectar con el servicio para actualizar Boleta',err);
                            })
                        }else{
                            $scope.statusPayment = 'Sin Pagar' 
                        }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err)
                  })

            }
            
            function deletePayment(paymentId,total){
                  $scope.paymentsId = paymentId;
                  $scope.total = total;
                  $scope.ballotId = $stateParams.idBallot
                  var modalInstance  = $modal.open({
                          template: require('../pay-delete/payments-ballot-delete.html'),
                          animation: true,
                          scope: $scope,
                          controller: 'paymentsBallotDeleteController',
                          controllerAs: 'paymentsBallotDelete'
                  })
            } 

            function viewBallots(ballotId,clientName){
                $scope.ballotsData = {
                    ballotId:ballotId,
                    clientName:clientName
                }

                var modalInstance  = $modal.open({
                        template: require('../view/ballots-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'ballotsViewController',
                        controllerAs: 'ballotsViews',
                        size: 'lg'
                })
            }

        
            function createPayment() {

                    let model = {
                        userId: $scope.userInfo.id,
                        ballotId: $stateParams.idBallot,
                        total : $scope.payment,
                        numberDocument : $stateParams.idBallot,
                        paymentFormId : $scope.paymentFormId,
                        // paymentFormId : $scope.paymentForms.selected.paymentFormId, 
                        // numberDocument : $scope.document,
                        comment : $scope.observation
                    }

                    console.log('model',model)  

                     paymentsServices.payments.createBallotPayment(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado el abono correctamente','success')
                        getPayments($stateParams.idBallot,model.userId)
                        $scope.observation = ''
                        $scope.payment = ''
                        $scope.document = ''
                      },(err) => {
                        ngNotify.set('Error al ingresar abono','error')
                     })
                }
            
        }//FIN CONSTRUCTOR
        // Funciones
        
        
    }   

paymentsBallotViewController.$inject = ['$scope','UserInfoConstant','$timeout','paymentsServices','clientsServices','clientsInvoicesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams','warehousesServices','itemsServices','$modal'];

