/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description asada
* # name controller --> ballotsDeleteController
* # As Controller --> ballotsDelete														
*/


  export default class ballotsDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,ballotsServices,$location,UserInfoConstant,ngNotify) {

    	var vm = this;
    	vm.cancel = cancel;
	    vm.deleteBallot = deleteBallot
	    $scope.ballotId = $scope.ballotData.ballotId
	    $scope.rut = $scope.ballotData.rut
	    $scope.clientName = $scope.ballotData.clientName

	    //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteBallot() {
			let model = {
                    ballotId : $scope.ballotId,
                    userId : $scope.userInfo.id
                }

                ballotsServices.ballots.disabledBallot(model).$promise.then((dataReturn) => {

                    ngNotify.set('Boleta eliminada exitosamente','success');
					         $modalInstance.dismiss('chao');
                  },(err) => {
                      ngNotify.set('Error al eliminar boleta','error')
                  })
		}
	    


    }

  }

  ballotsDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','ballotsServices','$location','UserInfoConstant','ngNotify'];
