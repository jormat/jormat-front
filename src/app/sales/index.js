

import routing from './sales.route';
import run from './sales.run';

//clients invoices files
import clientsInvoicesListController from './clients-invoices/list/clientsInvoicesListController';
import clientsInvoicesViewController from './clients-invoices/view/clientsInvoicesViewController';
import clientsInvoicesCreateController from './clients-invoices/create/clientsInvoicesCreateController';
import clientsInvoicesUpdateController from './clients-invoices/update/clientsInvoicesUpdateController'; 
import invoicesCreateKeyController from './clients-invoices/create-access-key/invoicesCreateKeyController';
import clientsInvoicesDeleteController from './clients-invoices/delete/clientsInvoicesDeleteController';
import invoicesReportController from './clients-invoices/report/invoicesReportController';
import invoicesByClientController from './clients-invoices/clients/invoicesByClientController';
import customerNotificationController from './clients-invoices/notifications/customerNotificationController';
import invoicePrintController from './clients-invoices/print/invoicePrintController';
import printPackOffController from './clients-invoices/packOff/printPackOffController';
import invoiceWarehousePrintController from './clients-invoices/warehouse-print/invoiceWarehousePrintController';
import modalLoginController from './clients-invoices/modal-login/modalLoginController';
import clientsInvoicesServices from './clients-invoices/clientsInvoicesServices';

//clients documents nn
import documentsNnListController from './documents-nn/list/documentsNnListController';
import documentsNnViewController from './documents-nn/view/documentsNnViewController';
import documentsNnCreateController from './documents-nn/create/documentsNnCreateController';
import documentsNnUpdateController from './documents-nn/update/documentsNnUpdateController';
import documentsNnDeleteController from './documents-nn/delete/documentsNnDeleteController'; 
import nnPrintController from './documents-nn/print/nnPrintController';
import paymentNnViewController from './documents-nn/payments/paymentNnViewController';
import paymentNnController from './documents-nn/payment/paymentNnController';
import paymentsNnDeleteController from './documents-nn/pay-delete/paymentsNnDeleteController'; 
import printPackOffNnController from './documents-nn/pack-off/printPackOffNnController'; 
import nnWarehousePrintController from './documents-nn/warehouse-print/nnWarehousePrintController';
import documentsNnServices from './documents-nn/documentsNnServices';

//clients files
import clientsListController from './clients/list/clientsListController';
import clientsViewController from './clients/view/clientsViewController';
import clientsCreateController from './clients/create/clientsCreateController';
import clientHistoryController from './clients/history/clientHistoryController';
import clientsUpdateController from './clients/update/clientsUpdateController';
import clientsDeleteController from './clients/delete/clientsDeleteController';
import accountStatusController from './clients/account/accountStatusController';
import paymentsByClientController from './clients/payments/paymentsByClientController';
import clientPaymentsController from './clients/history-payments/clientPaymentsController';
import clientsServices from './clients/clientsServices';

///payments files
import paymentsListController from './payments/list/paymentsListController';
import paymentsViewController from './payments/view/paymentsViewController';
import paymentsDeleteController from './payments/delete/paymentsDeleteController';
import paymentsReportController from './payments/report/paymentsReportController';
import paymentsServices from './payments/paymentsServices';

///offices payments files
import officePaymentsListController from './office-payments/list/officePaymentsListController';
import officePaymentsViewController from './office-payments/view/officePaymentsViewController';
import officePaymentsCreateController from './office-payments/create/officePaymentsCreateController';
import officePaymentsPrintController from './office-payments/print/officePaymentsPrintController';
import officePaymentsServices from './office-payments/officePaymentsServices';

///credit notes files
import creditNotesListController from './credit-notes/list/creditNotesListController';
import creditNotesCreateController from './credit-notes/create/creditNotesCreateController';
import creditNotesViewController from './credit-notes/view/creditNotesViewController'; 
import creditNotesDeleteController from './credit-notes/delete/creditNotesDeleteController';
import paymentsNoteDeleteController from './credit-notes/pay-delete/paymentsNoteDeleteController';
import notesReportController from './credit-notes/report/notesReportController'; 
import paymentNotesViewController from './credit-notes/payments/paymentNotesViewController';
import creditNotesPrintController from './credit-notes/print/creditNotesPrintController';
import creditNotesServices from './credit-notes/creditNotesServices';

///ballots files
import ballotsListController from './ballots/list/ballotsListController';
import ballotsCreateController from './ballots/create/ballotsCreateController';
import ballotsViewController from './ballots/view/ballotsViewController';
import ballotsDeleteController from './ballots/delete/ballotsDeleteController';
import ballotsPrintController from './ballots/print/ballotsPrintController';
import paymentBallotController from './ballots/payment/paymentBallotController'; 
import paymentsBallotDeleteController from './ballots/pay-delete/paymentsBallotDeleteController';
import paymentsBallotViewController from './ballots/payments/paymentsBallotViewController'; 
import printPackOffBallotController from './ballots/pack-off/printPackOffBallotController'; 
import warehouseBallotsPrintController from './ballots/warehouse-print/warehouseBallotsPrintController';
import ballotsServices from './ballots/ballotsServices';
import confirmationBallotClientController from './ballots/create/confirmBallot/confirmationBallotClientController';
import updateBallotClientController from './ballots/create/updateBallotClients/updateBallotClientController';

///quotations files
import quotationsListController from './quotations/list/quotationsListController';
import quotationsCreateController from './quotations/create/quotationsCreateController';
import quotationsViewController from './quotations/view/quotationsViewController';
import quotationsDeleteController from './quotations/delete/quotationsDeleteController';
import quotationsPrintController from './quotations/print/quotationsPrintController';
import quotationsUpdateController from './quotations/update/quotationsUpdateController';
import warehousePrintController from './quotations/warehouse-print/warehousePrintController';   
import imageQuotationsPrintController from './quotations/image-print/imageQuotationsPrintController'; 
import quotationsCompanyPrintController from './quotations/company-print/quotationsCompanyPrintController'; 
import uploadCsvQuotationsController from './quotations/create/dialogs/uploadCsv/uploadCsvQuotationsController';
import confirmationClientController from './quotations/create/dialogs/confirm/confirmationClientController';
import quotationsServices from './quotations/quotationsServices';  
import updateClientController from './quotations/create/dialogs/updateClients/updateClientController';

///note sale files
import saleNoteListController from './sale-note/list/saleNoteListController';
import saleNoteCreateController from './sale-note/create/saleNoteCreateController';
import saleNotesViewController from './sale-note/view/saleNotesViewController';
import saleNotesUpdateController from './sale-note/update/saleNotesUpdateController';
import saleNotePrintController from './sale-note/print/saleNotePrintController';
import saleNoteServices from './sale-note/saleNoteServices';  

///checks files
import checksListController from './checks/list/checksListController'; 
import checksCreateController from './checks/create/checksCreateController';
import checksUpdateController from './checks/update/checksUpdateController';
import checksViewController from './checks/view/checksViewController'; 
import actionsCheckController from './checks/actions/actionsCheckController';
import historicalChecksController from './checks/historical/historicalChecksController';
import checksServices from './checks/checksServices';

///after sales files
import afterSaleListController from './after-sale/list/afterSaleListController';
import afterSaleCreateController from './after-sale/create/afterSaleCreateController';
import afterSaleViewController from './after-sale/view/afterSaleViewController'; 
import afterSaleDeleteController from './after-sale/delete/afterSaleDeleteController';
import afterSaleReportController from './after-sale/report/afterSaleReportController';
import afterSaleServices from './after-sale/afterSaleServices';

///void documents files
import voidDocumentsListController from './void-documents/list/voidDocumentsListController';
import voidDocumentsViewController from './void-documents/view/voidDocumentsViewController'; 
import voidDocumentController from './void-documents/create/voidDocumentController';
import voidDocumentsServices from './void-documents/voidDocumentsServices';

///exempt invoices files
import exemptInvoicesListController from './exempt-invoices/list/exemptInvoicesListController';
import exemptInvoicesViewController from './exempt-invoices/view/exemptInvoicesViewController'; 
import exemptInvoicesCreateController from './exempt-invoices/create/exemptInvoicesCreateController';
import exemptInvoicesPayController from './exempt-invoices/payment/exemptInvoicesPayController';
import exemptInvoicesServices from './exempt-invoices/exemptInvoicesServices';

///after sales files
import budgetsListController from './budgets/list/budgetsListController';
import budgetsCreateController from './budgets/create/budgetsCreateController';
import budgetsViewController from './budgets/view/budgetsViewController'; 
import budgetsPrintController from './budgets/print/budgetsPrintController';
import budgetsUpdateController from './budgets/update/budgetsUpdateController';
import budgetsServices from './budgets/budgetsServices';


export default angular.module('app.sales', [clientsInvoicesServices,documentsNnServices,clientsServices,paymentsServices,creditNotesServices,ballotsServices,quotationsServices,officePaymentsServices,checksServices,saleNoteServices,afterSaleServices,voidDocumentsServices,budgetsServices,exemptInvoicesServices])
  .config(routing)
  .controller('clientsInvoicesListController', clientsInvoicesListController)
  .controller('clientsInvoicesViewController', clientsInvoicesViewController)
  .controller('clientsInvoicesCreateController', clientsInvoicesCreateController)
  .controller('clientsInvoicesUpdateController', clientsInvoicesUpdateController) 
  .controller('invoicesCreateKeyController', invoicesCreateKeyController)
  .controller('modalLoginController', modalLoginController)
  .controller('printPackOffController', printPackOffController) 
  .controller('customerNotificationController', customerNotificationController) 
  .controller('invoicesReportController', invoicesReportController)
  .controller('invoicePrintController', invoicePrintController) 
  .controller('invoiceWarehousePrintController', invoiceWarehousePrintController)
  .controller('invoicesByClientController', invoicesByClientController)
  .controller('clientsInvoicesDeleteController', clientsInvoicesDeleteController)
  .controller('documentsNnListController', documentsNnListController)
  .controller('documentsNnViewController', documentsNnViewController)
  .controller('documentsNnCreateController', documentsNnCreateController)
  .controller('documentsNnUpdateController', documentsNnUpdateController)
  .controller('documentsNnDeleteController', documentsNnDeleteController)
  .controller('printPackOffNnController', printPackOffNnController)
  .controller('paymentNnController', paymentNnController)
  .controller('nnPrintController', nnPrintController) 
  .controller('nnWarehousePrintController', nnWarehousePrintController)
  .controller('paymentNnViewController', paymentNnViewController)
  .controller('paymentsNnDeleteController', paymentsNnDeleteController)
  .controller('clientsListController', clientsListController)
  .controller('clientsCreateController', clientsCreateController)
  .controller('clientsViewController', clientsViewController)
  .controller('clientHistoryController', clientHistoryController)
  .controller('clientsUpdateController', clientsUpdateController)
  .controller('accountStatusController', accountStatusController) 
  .controller('paymentsByClientController', paymentsByClientController)
  .controller('clientPaymentsController', clientPaymentsController)
  .controller('clientsDeleteController', clientsDeleteController) 
  .controller('paymentsListController', paymentsListController)
  .controller('paymentsViewController', paymentsViewController)
  .controller('paymentsDeleteController', paymentsDeleteController)
  .controller('paymentsReportController', paymentsReportController)
  .controller('officePaymentsListController', officePaymentsListController)
  .controller('officePaymentsViewController', officePaymentsViewController)
  .controller('officePaymentsCreateController', officePaymentsCreateController) 
  .controller('officePaymentsPrintController', officePaymentsPrintController)
  .controller('creditNotesListController', creditNotesListController)
  .controller('creditNotesCreateController', creditNotesCreateController)
  .controller('creditNotesViewController', creditNotesViewController)
  .controller('creditNotesDeleteController', creditNotesDeleteController) 
  .controller('creditNotesPrintController', creditNotesPrintController) 
  .controller('paymentsNoteDeleteController', paymentsNoteDeleteController)
  .controller('paymentNotesViewController', paymentNotesViewController)
  .controller('notesReportController', notesReportController)
  .controller('ballotsListController', ballotsListController)
  .controller('ballotsCreateController', ballotsCreateController)
  .controller('ballotsViewController', ballotsViewController)
  .controller('ballotsDeleteController', ballotsDeleteController)
  .controller('ballotsPrintController', ballotsPrintController) 
  .controller('confirmationBallotClientController', confirmationBallotClientController) 
  .controller('updateBallotClientController', updateBallotClientController)
  .controller('warehouseBallotsPrintController', warehouseBallotsPrintController)
  .controller('paymentsBallotViewController', paymentsBallotViewController) 
  .controller('paymentsBallotDeleteController', paymentsBallotDeleteController) 
  .controller('printPackOffBallotController', printPackOffBallotController)
  .controller('paymentBallotController', paymentBallotController)
  .controller('quotationsListController', quotationsListController)
  .controller('quotationsCreateController', quotationsCreateController)
  .controller('quotationsViewController', quotationsViewController)
  .controller('quotationsDeleteController', quotationsDeleteController)
  .controller('quotationsPrintController', quotationsPrintController) 
  .controller('quotationsUpdateController', quotationsUpdateController) 
  .controller('warehousePrintController', warehousePrintController) 
  .controller('quotationsCompanyPrintController', quotationsCompanyPrintController)
  .controller('imageQuotationsPrintController', imageQuotationsPrintController)
  .controller('quotationsCompanyPrintController', quotationsCompanyPrintController)  
  .controller('uploadCsvQuotationsController', uploadCsvQuotationsController)
  .controller('confirmationClientController', confirmationClientController)
  .controller('checksListController', checksListController)
  .controller('checksViewController', checksViewController)
  .controller('checksCreateController', checksCreateController) 
  .controller('checksUpdateController', checksUpdateController)
  .controller('historicalChecksController', historicalChecksController)
  .controller('actionsCheckController', actionsCheckController)
  .controller('saleNoteListController', saleNoteListController)
  .controller('saleNotesViewController', saleNotesViewController)
  .controller('saleNotePrintController', saleNotePrintController)
  .controller('saleNoteCreateController', saleNoteCreateController)
  .controller('saleNotesUpdateController', saleNotesUpdateController)
  .controller('afterSaleListController', afterSaleListController)
  .controller('afterSaleCreateController', afterSaleCreateController)
  .controller('afterSaleDeleteController', afterSaleDeleteController)
  .controller('afterSaleViewController', afterSaleViewController)
  .controller('afterSaleReportController', afterSaleReportController)
  .controller('voidDocumentsListController', voidDocumentsListController) 
  .controller('voidDocumentsViewController', voidDocumentsViewController) 
  .controller('voidDocumentController', voidDocumentController) 
  .controller('budgetsListController', budgetsListController) 
  .controller('budgetsViewController', budgetsViewController)
  .controller('budgetsPrintController', budgetsPrintController) 
  .controller('budgetsCreateController', budgetsCreateController) 
  .controller('budgetsUpdateController', budgetsUpdateController)
  .controller('exemptInvoicesListController', exemptInvoicesListController) 
  .controller('exemptInvoicesCreateController', exemptInvoicesCreateController) 
  .controller('exemptInvoicesViewController', exemptInvoicesViewController) 
  .controller('exemptInvoicesPayController', exemptInvoicesPayController)
  .controller('updateClientController', updateClientController)
  .run(run)
  .constant("sales", [{
        "title": "Ventas",
        "icon":"mdi-cart-outline",
        "subMenu":[
            {
            'title' : 'Facturas Clientes',
            'url': 'app.clientsInvoices'
            },
            {
            'title' : 'Documento NN',
            'url': 'app.documentsNn'
            },
            {
            'title' : 'Cotizaciones',
            'url': 'app.quotations',
            },
            {
            'title' : 'Boletas',
            'url': 'app.ballots',
            },
            {
            'title' : 'Nota de crédito',
            'url': 'app.creditNotes',
            },
            {
            'title' : 'Notas de venta',
            'url': 'app.saleNote',
            },
            {
            'title' : 'Presupuestos',
            'url': 'app.budgets',
            },
            {
            'title' : 'Post venta',
            'url': 'app.afterSale',
            },
            {
            'title' : 'Documentos exentos',
            'url': 'app.exemptInvoices',
            },
            {
            'title' : 'Anulación documentos',
            'url': 'app.voidDocuments',
            }
            
        ]
    }])
  .name;