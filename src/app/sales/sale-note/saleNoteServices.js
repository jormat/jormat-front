/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> saleNoteServices
* file for call at services for salenote
*/

class saleNoteServices {

	constructor($resource,SERVICE_URL_CONSTANT) {
		
		const saleNote = $resource(SERVICE_URL_CONSTANT.jormat + '/notes/sale/:route',{},{
			getSaleNotes: {
					method:'GET',
					params : {
							route: 'list'
					}
			},
			getSaleNotesDetails: {
					method:'GET',
					params : {
							route: ''
					}
			},
			noteCreate: {
					method:'POST',
					params : {
							route: ''
					}
			},
			getSaleNotesItems: {
					method:'GET',
					params : {
							route: 'items'
					}
			},
			saleNoteUpdate: {
					method:'PUT',
					params : {
							route: 'update'
					}
			},
			changeStatu: {
				method:'POST',
				params : {
						route: 'changeStatu'
				}
			},
		})

		const documents = $resource(SERVICE_URL_CONSTANT.jormat + '/warehouseDeliveries/:route',{},{
			setDocumentList: {
				method:'POST',
				params : {
					route: 'update'
				}
			},
		})

		return {
			saleNote : saleNote,
			documents : documents
		}
	}
}

	saleNoteServices.$inject=['$resource','SERVICE_URL_CONSTANT']

	export default  angular.module('services.saleNoteServices', [])
	.service('saleNoteServices', saleNoteServices)
	.name;