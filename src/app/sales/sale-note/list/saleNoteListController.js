
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> saleNoteListController
* # As Controller --> saleNoteList
*/

export default class saleNoteListController{
	constructor($scope,UserInfoConstant,$timeout,saleNoteServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
		this.saleNoteServices = saleNoteServices;
		this.UserInfoConstant = UserInfoConstant[0].details
		this.$scope = $scope;
		this.ngNotify = ngNotify
		this.$modal = $modal;
		this.$filter = $filter;
		this.$state = $state;
		this.searchOptions = {
			updateOn: 'default blur',
			debounce:{
				'default': 100,
				'blur': 0
			}
		}

		i18nService.setCurrentLang('es');
		this.$scope.dropdownMenu = require('./dropdownActionsMenu.html')

		this.$scope.saleNoteGrid = {
			enableFiltering: true,
			exporterCsvFilename: 'listado_notas_venta.csv',
			enableGridMenu: true,
			columnDefs: [
				{ 
					name:'id',
					field: 'saleNoteId',  
					width: '8%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<a ng-click="grid.appScope.saleNoteList.viewSaleNotes(row.entity)">{{row.entity.saleNoteId}}</a>' +
								'</div>' 
				},
				{ 
					name:'Cliente',
					field: 'clientName',
					width: '30%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p class="uppercase">{{row.entity.clientName}}</p>' +
								'</div>'        
				},
				{ 
					name: 'Origen', 
					field: 'origin', 
					width: '10%'
				},
				{ 
					name:'Total',
					field: 'total',
					width: '10%',
					cellTemplate:'<div class="ui-grid-cell-contents">'+
								'<p>$<strong>{{row.entity.total}}</strong></p>' +
								'</div>'
				},
				
				{ 
					name: 'Fecha', 
					field: 'date',
					width: '9%'
				},
				
				{ 
					name: 'Usuario', 
					field: 'userCreation', 
					width: '12%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<p class=" label bg-secundary font-italic" style="font-size: 10px;">{{row.entity.userCreation}}</p>' +
								'</div>'   
				},
					{ 
					name: 'Estado', 
					field: 'statusName',
					width: '8%',
					cellTemplate:'<div class="ui-grid-cell-contents ">'+
								'<div ng-if="row.entity.loading!=1"><p style="font-size: 10px;" class="{{(row.entity.statusName == \'Facturado\')?\'label bg-primary\':\'label bg-warning\'}}">{{row.entity.statusName}}</p></div>' +
								'<div ng-if="row.entity.loading==1"><i class="fa text-dark fa-spinner fa-spin fa-sm fa-fw"></i></div>'+
								'</div>'
				},
				{ 
					name: 'Bodega', 
					field: 'dsOut',
					width: '9%',
					cellTemplate:'<div class="ui-grid-cell-contents">'+
								'<p style="font-size: 10px;" class="badge bg-{{row.entity.styleOut}}">{{row.entity.dsOut}}</p>' +
								'</div>'
				},
				{ 
					name: '',
					width: '8%', 
					field: 'href',
					enableFiltering: false,
					cellTemplate: $scope.dropdownMenu
				}
			]
		}

		this.loadSaleNotes();
	}//FIN CONSTRUCTOR

	// Funciones
	loadSaleNotes(){
		this.saleNoteServices.saleNote.getSaleNotes().$promise.then((dataReturn) => {
			this.$scope.saleNoteGrid.data = dataReturn.data;
			this.$scope.saleNoteGrid.data.forEach((row, index) => {
				row.sequence = index;
			});
			this.www = angular.copy(dataReturn.data)
		},(err) => {
			console.log('No se ha podido conectar con el servicio',err);
		})
	}

	searchData(){
		this.$scope.saleNoteGrid.data = this.$filter('filter')(this.www, this.searchText, undefined);
	}

	printSaleNote(row){
		this.$state.go("app.saleNotePrint", { idNote: row.saleNoteId});
	}

	updateDocument(row){

		this.params = {
					userId : this.UserInfoConstant[0].user.id,
					typeDocumentId : 14,
					documentId : row.saleNoteId,
					commentary : "Entrega Rápida",
                    option : 1
				}
		
		this.saleNoteServices.documents.setDocumentList(this.params).$promise.then((dataReturn) => {
					this.ngNotify.set('La entrega ha sido registrada exitosamente','warn');
					this.loadSaleNotes();
				},(err) => {
					this.ngNotify.set('Error al actualizar documento','error')
				})
	}

	viewSaleNotes(row){
		this.$scope.saleNoteData = row;
		var modalInstance  = this.$modal.open({
			template: require('../view/sale-notes-view.html'),
			animation: true,
			scope: this.$scope,
			controller: 'saleNotesViewController',
			controllerAs: 'saleNotesView',
			size: 'lg'
		})
	}

	transformManual(row) {
		console.log('val => ', row)
		this.$scope.saleNoteGrid.data[row.sequence].loading = 1
		this.params = {
			userId : this.UserInfoConstant[0].user.id,
			documentId : row.saleNoteId,
		}

		this.saleNoteServices.saleNote.changeStatu(this.params).$promise.then((dataReturn) => {
			dataReturn.data.sequence = row.sequence
			this.$scope.saleNoteGrid.data[row.sequence] = angular.copy(dataReturn.data)
			this.$scope.saleNoteGrid.data[row.sequence].loading = 0
			this.ngNotify.set('Actualizado correctamente','success');
		},(err) => {
			this.ngNotify.set('Error al actualizar documento','error')
		})
	}

	transformToInvoice(row) {
		this.$state.go("app.clientsInvoicesUpdate", { idInvoice: row.saleNoteId, discount:row.discount,type:'nota'});
	}
}
saleNoteListController.$inject = ['$scope','UserInfoConstant','$timeout','saleNoteServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

