/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> saleNotePrintController
* # As Controller --> saleNotePrint														
*/


  export default class saleNotePrintController {

    constructor($scope, $filter,$rootScope, $http,saleNoteServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.transformToInvoice = transformToInvoice
        $scope.saleNoteId = $stateParams.idNote

        let params = {
                saleNoteId: $scope.saleNoteId
            }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        saleNoteServices.saleNote.getSaleNotesDetails(params).$promise.then((dataReturn) => {
                $scope.saleNote = dataReturn.data
                },(err) => {
                    console.log('No se ha podido conectar con el servicio get quotation Details',err);
                })

          saleNoteServices.saleNote.getSaleNotesItems(params).$promise.then((dataReturn) => {
                $scope.saleNoteItems = dataReturn.data
                var subTotal = 0
                  var i = 0
                     for (i=0; i<$scope.saleNoteItems.length; i++) {
                         subTotal = subTotal + $scope.saleNoteItems[i].total  
                      }
                      
                      $scope.subTotality =  subTotal
                },(err) => {
                    console.log('No se ha podido conectar con el servicio get quotation items',err);
                })

    	function cancel() {

		 $state.go("app.saleNote")
		}

        function transformToInvoice(row){

          $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.saleNoteId,discount:$scope.saleNote[0].discount,type:'nota'});

        }

        function print() {
              window.print();

        }


    }

  }

  saleNotePrintController.$inject = ['$scope', '$filter','$rootScope', '$http','saleNoteServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
