/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> saleNotesViewController
* # As Controller --> saleNotesView
*/
export default class saleNotesViewController {
	constructor($scope, $filter,$rootScope, $http,$modalInstance,saleNoteServices,$location,UserInfoConstant,ngNotify,$state,Dialogs) {
		this.btnVisible = false;
		this.$scope = $scope;
		this.Dialogs = Dialogs;
		this.$state = $state;
		this.ngNotify = ngNotify
		this.UserInfoConstant = UserInfoConstant[0].details;
		this.$modalInstance = $modalInstance;
		this.saleNoteSrv = saleNoteServices;
		this.$scope.saleNoteId = this.$scope.saleNoteData.saleNoteId;
		this.$scope.clientName = this.$scope.saleNoteData.clientName;

		this.loadData();

	     	
	}

	loadData(){
		let params = {
			saleNoteId: this.$scope.saleNoteId
		}

		this.saleNoteSrv.saleNote.getSaleNotesDetails(params).$promise.then((dataReturn) => {
			this.$scope.saleNote = dataReturn.data
			this.btnVisible = (this.$scope.saleNote[0].userOut) ? false : true;
		},(err) => {
			console.log('No se ha podido conectar con el servicio get quotation Details',err);
		})

		this.saleNoteSrv.saleNote.getSaleNotesItems(params).$promise.then((dataReturn) => {
			this.$scope.saleNoteItems = dataReturn.data
			var subTotal = 0
	              var i = 0
	                 for (i=0; i<this.$scope.saleNoteItems.length; i++) {
	                     subTotal = subTotal + this.$scope.saleNoteItems[i].total  
	                  }
	                  
	                  this.$scope.subTotality =  subTotal
		},(err) => {
			console.log('No se ha podido conectar con el servicio get quotation items',err);
		})
	}

	cancel() {
		this.$modalInstance.dismiss('chao');
	}

	transformToInvoice(row){
		this.$state.go("app.clientsInvoicesUpdate", { idInvoice: this.$scope.saleNoteId,discount:this.$scope.saleNote[0].discount,type:'nota'});
	}

	updateDocument(){
		this.$scope.isEdit = true;

		this.params = {
					userId : this.UserInfoConstant[0].user.id,
					typeDocumentId : this.$scope.saleNote[0].typeId,
					documentId : this.$scope.saleNoteId,
					commentary : "Entrega Rápida",
                    option : 1
				}

				this.saleNoteSrv.documents.setDocumentList(this.params).$promise.then((dataReturn) => {
					this.ngNotify.set('La entrega ha sido registrada exitosamente','warn');
					this.$state.reload('app.warehouseDeliveries')
					this.$modalInstance.dismiss('chao');
					this.$state.reload('app.saleNote')
				},(err) => {
					this.ngNotify.set('Error al actualizar documento','error')
				})
		
		
	}
}
saleNotesViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','saleNoteServices','$location','UserInfoConstant','ngNotify','$state','Dialogs'];
