
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> budgetsUpdateController
* # As Controller --> budgetsUpdate														
*/

export default class budgetsUpdateController{

        constructor($scope,UserInfoConstant,$timeout,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element,$window,$stateParams,budgetsServices){
            var vm = this;
            vm.budgetsUpdate = budgetsUpdate
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getStock = getStock
            vm.getNet = getNet
            vm.showGeneralDiscount = showGeneralDiscount
            vm.loadItems = loadItems
            vm.getNetTotal = getNetTotal
            $scope.budgetCode= $stateParams.idBudge
            $scope.warehouseId= $stateParams.warehouse
            $scope.buttonInvoice = true
            $scope.parseInt = parseInt
            // $scope.discount = parseInt($stateParams.discount)
            $scope.discount = $stateParams.discount
            $scope.type = $stateParams.type
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }
            $scope.notStock = 0
            $scope.warningQuantity = false
            $scope.buttonMore = false
            $scope.invoiceItems = []

            showPanel()

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.warehouseId = $scope.warehouses[0] 
                            console.log('$scope.warehouses lulul',$scope.warehouses,$stateParams.discount)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            loadItems()
            

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-click="grid.appScope.budgetsUpdate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                   '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.budgetsUpdate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }  
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{row.entity.vatPrice}}</strong></p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }  
                    }
                ]
            };
            
          
            function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            function loadItem(row){
                 
                  if (row.failure == 1) {

                   ngNotify.set( 'Item '+ row.itemId +' con FALLA, valide con supervisor antes de continuar', {
                      sticky: true,
                      button: true,
                      type : 'error'
                    }) 

                  }else{

                    $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    price: row.netPrice,
                    minPrice: row.netPrice,
                    netPurchaseValue: row.netPurchaseValue,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                   })

                    getStock(row.itemId,1)
                    $scope.warningItems = false 
                }     
            }

            function showGeneralDiscount(value){

                if (value ==  0 || value ==  null ) {
                    console.log('muestra boton',value); 
                    $scope.discountButton = false

                }else{
                    console.log('esconde boton',value); 
                    $scope.discountButton = true
                    $scope.discount = 0

                }
            
            }

            function loadItems(){
          
            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            }

            
            function getStock(itemId,quantity){
            
              let params = {
                itemId: itemId,
                warehouseId :$stateParams.warehouse 
            }

            itemsServices.items.getStock(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data
              $scope.stock = $scope.item[0].stock
              $scope.notStock = 0
              $scope.warningQuantity = false
              if (quantity > $scope.stock) {

                ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock ,'warn')
                $scope.notStock = 1
                $scope.warningQuantity = true
              }
              
              return false;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
    
            }

            function getNet() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 
                  }
                  
                  $scope.NET =  net
                  $scope.netoInvoice = net - (($scope.discount*net)/100) 
                  $scope.total = Math.round($scope.netoInvoice * 1.19)
                  $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                  return net;
                      
            }

            function getNetTotal() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.netPurchaseValue * valueItems.quantity); 

                  }
                  
                  $scope.netTotal =  net
                  return $scope.netTotal;
                      
            }

            function removeItems(index) {
                $scope.invoiceItems.splice(index, 1)
                $scope.warningQuantity = false
                $scope.buttonMore = false
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

       
            function showPanel() {
                $scope.invoiceItems = []
                $scope.panelInvoice = true
                $scope.warningQuantity = false
                $scope.buttonMore = false

                  let params = {
                    budgetId: $scope.budgetCode
                  }

                  budgetsServices.budgets.getBudgetsDetails(params).$promise.then((dataReturn) => {
                  $scope.budget = dataReturn.data

                  $scope.budgetDetails  = { 
                        nameDescription : $scope.budget[0].nameDescription,
                        phone : $scope.budget[0].phone,
                        date : $scope.budget[0].date,
                        origin : $scope.budget[0].origin, 
                        warehouseId : $scope.budget[0].warehouseId,
                        clientName : $scope.budget[0].clientName,
                        clientId : $scope.budget[0].clientId, 
                        comment : $scope.budget[0].comment,
                        rut : $scope.budget[0].rut
                    }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio get budget Details',err);
                  })

                    budgetsServices.budgets.getBudgetsItems(params).$promise.then((dataReturn) => {
                          $scope.budgetsItems = dataReturn.data
                          for(var i in $scope.budgetsItems){
                                $scope.invoiceItems.push({
                                    itemId: $scope.budgetsItems[i].itemId,
                                    itemDescription: $scope.budgetsItems[i].itemDescription,
                                    price: $scope.budgetsItems[i].price,
                                    minPrice: $scope.budgetsItems[i].price,
                                    netPurchaseValue: $scope.budgetsItems[i].netPurchaseValue,
                                    maxDiscount : $scope.budgetsItems[i].discount,
                                    quantity: $scope.budgetsItems[i].quantityItems,
                                    disscount: $scope.budgetsItems[i].discount
                             })
                                getStock($scope.budgetsItems[i].itemId,$scope.budgetsItems[i].quantityItems)

                        }

                    },(err) => {
                          console.log('No se ha podido conectar con el servicio getBudgets',err);
                        })
                 
            }

            function budgetsUpdate() {

                        let model = {
                            budgetId: $scope.budgetCode,
                            userId: $scope.userInfo.id,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            comment : $scope.budgetDetails.comment,
                            name : $scope.budgetDetails.nameDescription,
                            phone : $scope.budgetDetails.phone,
                            itemsInvoices : $scope.invoiceItems 
                        }      

                        console.log('modelo',model) 

                        budgetsServices.budgets.updateBudget(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha actualizado el presupuesto correctamente','success')
                            $scope.result = dataReturn.data
                            $state.go('app.budgets')
                            

                          },(err) => {
                              ngNotify.set('Error al actualizar presupuesto','error')
                          })
                    }

    
        
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

budgetsUpdateController.$inject = ['$scope','UserInfoConstant','$timeout','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element','$window','$stateParams','budgetsServices'];

