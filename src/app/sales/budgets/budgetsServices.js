/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> budgetsServices
* file for call at services for Budgets
*/

class budgetsServices {

	constructor($resource,SERVICE_URL_CONSTANT) {
		
		var budgets = $resource(SERVICE_URL_CONSTANT.jormat + '/documents/budgets/:route',{},{
			
				getBudgets: {
						method:'GET',
						params : {
							 route: 'list'
						}
				},
				getBudgetsDetails: {
						method:'GET',
						params : {
							 route: ''
						}
				},
				createBudget: {
						method:'POST',
						params : {
								route: ''
						}
				},
				getBudgetsItems: {
						method:'GET',
						params : {
							 route: 'items'
						}
				},

				disabledBudget: {
						method:'DELETE',
						params : {
								route: ''
						}
				},

				updateBudget: {
						method:'PUT',
						params : {
								route: ''
						}
				}
		})

		return { budgets : budgets
						 
					 }
	}
}

	budgetsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

	export default  angular.module('services.budgetsServices', [])
	.service('budgetsServices', budgetsServices)
	.name;