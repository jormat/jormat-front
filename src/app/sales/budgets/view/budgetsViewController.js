/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> budgetsViewController
* # As Controller --> budgetsView														
*/


  export default class budgetsViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,budgetsServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
    	vm.transformToInvoice = transformToInvoice
      	$scope.budgetId = $scope.budgetsData.budgetId
      	$scope.clientName = $scope.budgetsData.clientName

	      let params = {
	            budgetId: $scope.budgetId
	          }

	        budgetsServices.budgets.getBudgetsDetails(params).$promise.then((dataReturn) => {
	              $scope.budget = dataReturn.data
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get budget Details',err);
	              })

	        budgetsServices.budgets.getBudgetsItems(params).$promise.then((dataReturn) => {
	              $scope.budgetsItems = dataReturn.data
	              var subTotal = 0
	              var i = 0
	                 for (i=0; i<$scope.budgetsItems.length; i++) {
	                     subTotal = subTotal + $scope.budgetsItems[i].total  
	                  }
	                  
	                  $scope.subTotality =  subTotal

	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get budget items',err);
	              })

	    	function cancel() {
				$modalInstance.dismiss('chao');
			}

			function transformToInvoice() {
              $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.budgetId,discount:$scope.budget[0].discount,type:'presupuesto'});

        }

    }

  }

  budgetsViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','budgetsServices','$location','UserInfoConstant','ngNotify','$state'];
