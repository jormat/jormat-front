
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> budgetsListController
* # As Controller --> budgetsList														
*/

export default class budgetsListController{

        constructor($scope,UserInfoConstant,$timeout,quotationsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService,budgetsServices){
            var vm = this
            vm.budgetsView = budgetsView
            vm.printBudget = printBudget
            vm.transformToInvoice = transformToInvoice
            vm.transformToNN = transformToNN
            vm.budgetsUpdate = budgetsUpdate
            vm.searchData=searchData
            vm.loadBadgets = loadBadgets
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            loadBadgets()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.budgetsGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'listado_presupuestos.csv',
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'budgetId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.budgetsList.budgetsView(row.entity)">{{row.entity.budgetId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '33%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'        
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '13%'
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{row.entity.total}}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '12%'
                    },
                    { 
                      name: 'Usuario', 
                      field: 'userCreation', 
                      width: '15%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" badge bg-warning font-italic" style="font-size: 12px;">{{row.entity.userCreation}}</p>' +
                                 '</div>'   
                    },
                    
                    { 
                      name: 'Acciones',
                      width: '8%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadBadgets(){

                budgetsServices.budgets.getBudgets().$promise.then((dataReturn) => {
                  $scope.budgetsGrid.data = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            }


            function searchData() {
              budgetsServices.budgets.getBudgets().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.budgetsGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }


            function printBudget(row){
            
              $state.go("app.budgetsPrint", { idBudge: row.budgetId});
            }

            function budgetsView(row){
	            $scope.budgetsData = row;
	            var modalInstance  = $modal.open({
	                    template: require('../view/budgets-view.html'),
	                    animation: true,
	                    scope: $scope,
	                    controller: 'budgetsViewController',
	                    controllerAs: 'budgetsView',
	                    size: 'lg'
	            })
	          }

            function transformToInvoice(row) {
              $state.go("app.clientsInvoicesUpdate", { idInvoice: row.budgetId, discount:row.discount,type:'presupuesto'});

            }

            function transformToNN(row) {
              $state.go("app.documentsNnUpdate", { idQuotation: row.budgetId, discount:row.discount,type:'presupuesto'});

        }

        function budgetsUpdate(row) {
              $state.go("app.budgetsUpdate", { idBudge: row.budgetId, discount:row.discount,warehouse:row.warehouseId});

        }



            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

budgetsListController.$inject = ['$scope','UserInfoConstant','$timeout','quotationsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService','budgetsServices'];

