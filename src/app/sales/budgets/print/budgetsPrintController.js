/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> budgetsPrintController
* # As Controller --> budgetsPrint														
*/


  export default class budgetsPrintController {

    constructor($scope, $filter,$rootScope, $http,budgetsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.transformToInvoice = transformToInvoice
        $scope.budgetId = $stateParams.idBudge

        let params = {
                budgetId: $scope.budgetId
              }

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })
            budgetsServices.budgets.getBudgetsDetails(params).$promise.then((dataReturn) => {
                  $scope.budget = dataReturn.data
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio get budget Details',err);
                  })

            budgetsServices.budgets.getBudgetsItems(params).$promise.then((dataReturn) => {
                  $scope.budgetsItems = dataReturn.data
                  var subTotal = 0
                  var i = 0
                     for (i=0; i<$scope.budgetsItems.length; i++) {
                         subTotal = subTotal + $scope.budgetsItems[i].total  
                      }
                      
                      $scope.subTotality =  subTotal

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio get budget items',err);
                  })

    	function cancel() {

		 $state.go("app.budgets")
		}

        function transformToInvoice(row){

          $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.budgetId,discount:$scope.budget[0].discount,type:'presupuesto'});

        }

        function print() {
              window.print();

        }


    }

  }

  budgetsPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','budgetsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams'];
