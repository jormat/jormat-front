
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> budgetsCreateController
* # As Controller --> budgetsCreate														
*/

export default class budgetsCreateController{

        constructor($scope,UserInfoConstant,$timeout,budgetsServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element){
            this.$mdDialog = $mdDialog
            var vm = this;
            vm.budgetsCreate = budgetsCreate
            vm.setName = setName
            vm.setWarehouseId = setWarehouseId
            vm.viewItem = viewItem
            vm.loadItem = loadItem
            vm.loadItemCSV = loadItemCSV
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getStock = getStock
            vm.getNet = getNet
            vm.showGeneralDiscount = showGeneralDiscount
            vm.searchClient = searchClient
            vm.loadItems = loadItems
            $scope.buttonInvoice = true
            $scope.parseInt = parseInt
            $scope.buttonbudgetsCreate = true
            $scope.discount = 0
            $scope.purchaseOrder = 0
            $scope.warningItems = true
            $scope.currencySymbol = '$'
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
            $scope.priceVATest = 56000
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.warehouseId = $scope.warehouses[0] 
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            setWarehouseId()
            loadItems()

            $scope.itemsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-click="grid.appScope.budgetsCreate.loadItem(row.entity)" type="button" class="btn btn-accent btn-xs">+</button>'+
                                   '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'id',
                      field: 'itemId', 
                      enableFiltering: true,
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" style="font-size: 11px;" ng-click="grid.appScope.budgetsCreate.viewItem(row.entity)">{{row.entity.itemId}}</a>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Descripción',
                      field: 'itemDescription',
                      enableFiltering: true,
                      width: '42%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" tooltip-placement="right" tooltip=" A Mano {{row.entity.generalStock}}" >{{row.entity.itemDescription}}</p>' +
                                 '</div>' ,
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }
                    },
                    { 
                      name:'Referencias',
                      field: 'references',
                      enableFiltering: true,
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 10px;">{{row.entity.references}}</p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }  
                    },
                    { 
                      name:'Precio',
                      field: 'vatPrice',
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 13px;">$<strong> {{row.entity.vatPrice}}</strong></p>' +
                                 '</div>',
                      cellClass: function(grid, row) {
                        if (row.entity.priority === 1) {
                         return 'yellow';
                       }
                      }  
                    }
                ]
            };
            
           function loadItems(){

            itemsServices.items.getItems().$promise.then((dataReturn) => {
                  $scope.itemsGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
           }
            

            $scope.invoiceItems = []

            function loadItem(row){
                 
                  if (row.failure == 1) { 
                    
                    ngNotify.set( 'Item '+ row.itemId +' con FALLA, valide con supervisor antes de continuar', {
                      sticky: true,
                      button: true,
                      type : 'error'
                  }) 

                }else{

                    $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    price: row.netPrice,
                    minPrice: row.netPrice,
                    maxDiscount : row.maxDiscount,
                    quantity: 1,
                    disscount: 0

                    })

                    getStock(row.itemId,1)
                    $scope.warningItems = false

                }
                        
            }

             function loadItemCSV(row){
                 $scope.invoiceItems.push({
                    itemId: row.itemId,
                    itemDescription: row.itemDescription,
                    referenceKey : row.referenceKey,
                    price: Number(row.netPrice),
                    minPrice: Number(row.netPrice),
                    maxDiscount : 0,
                    quantity: Number(row.quantity),
                    disscount: 0

                });

                 getStock(row.itemId,row.quantity)
                 $scope.warningItems = false    
            }

            function showGeneralDiscount(value){

                if (value ==  0 || value ==  null ) {
                    console.log('muestra boton',value); 
                    $scope.discountButton = false

                }else{
                    console.log('esconde boton',value); 
                    $scope.discountButton = true
                    $scope.discount = 0

                }
            
            }

            function getStock(itemId,quantity){
            
              let params = {
                itemId: itemId,
                warehouseId :$scope.warehouseId 
            }

            itemsServices.items.getStock(params).$promise.then((dataReturn) => {
              $scope.item = dataReturn.data
              $scope.stock = $scope.item[0].stock
              $scope.notStock = 0
              $scope.warningQuantity = false
              if (quantity > $scope.stock) {

                ngNotify.set( 'Su sucursal no cuenta con stock suficiente para item ID '+ params.itemId +' - Actual: '+ $scope.stock ,'warn')
                $scope.notStock = 1
                $scope.warningQuantity = true
              }
              
              return false;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
    
            }

            function getNet() {
                var net = 0
                var i = 0
                for (i=0; i<$scope.invoiceItems.length; i++) {
                    var valueItems = $scope.invoiceItems[i] 
                    net += Math.round(valueItems.price * valueItems.quantity - (valueItems.disscount * (valueItems.price * valueItems.quantity)) / 100); 

                  }
                  
                  $scope.NET =  net
                  $scope.netoInvoice = net - (($scope.discount*net)/100) 
                  $scope.total = Math.round($scope.netoInvoice * 1.19)
                  $scope.VAT = Math.round($scope.total - $scope.netoInvoice)
                  return net;
                      
            }


            function removeItems(index) {
                $scope.invoiceItems.splice(index, 1)
                $scope.warningQuantity = false
            }


            function viewItem(row){
                $scope.itemsData = row;
                var modalInstance  = $modal.open({
                        template: require('../../../items/items/view/items-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'itemsViewController',
                        controllerAs: 'itemsView',
                        size: 'lg'
                })
            }

            function setWarehouseId(warehouseId){
                
                if (warehouseId == null) {
                    $scope.UserInfoConstant = UserInfoConstant
                    $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            $scope.warehouseId = $scope.warehouses[0].warehouseId
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            }else{

                $scope.warehouseId = warehouseId
             }
            }

            function setName(clients) {
                console.log(clients)
                $scope.clientName = clients.fullName
                $scope.clientId = clients.clientId
                $scope.selectedTwo = clients.rut
                if (clients.locked == 1) {
                    ngNotify.set('OJO este cliente esta bloqueado','warn')
                    $scope.lockedClient = true

                }else{
                    $scope.buttonInvoice = true
                    $scope.lockedClient = false
                }
            }

            function showPanel() {
                $scope.panelInvoice = true
                $scope.invoiceItems = []
                $scope.warningQuantity = false
            }

            function budgetsCreate() {

                        let model = {
                            userId: $scope.userInfo.id,
                            clientId : $scope.clientId,
                            date : $scope.CurrentDate,
                            originId : $scope.warehouseId,
                            priceVAT : $scope.VAT,
                            netPrice : $scope.NET,
                            discount : $scope.discount,
                            total : $scope.total,
                            comment : $scope.observation,
                            name : $scope.name,
                            phone : $scope.phone,
                            itemsInvoices : $scope.invoiceItems 
                        }      

                        console.log('modelo',model)   

                        budgetsServices.budgets.createBudget(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha creado el presupuesto correctamente','success')
                            $scope.result = dataReturn.data
                            $state.go('app.budgets')

                          },(err) => {
                              ngNotify.set('Error al crear presupuesto','error')
                          })
                    }




        function searchClient (value) {

            let model = {
                name: value
            }

            clientsServices.clients.getClientsByName(model).$promise.then((dataReturn) => {
                  $scope.clients = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

            return false;
          }

        }//FIN CONSTRUCTOR


        // uploadCsv() {
        //    this.confirm = this.$mdDialog.confirm({
        //         controller         : 'uploadCsvQuotationsController',
        //         controllerAs       : 'uploadCsv',
        //         template           : require('./dialogs/uploadCsv/uploadCsvQuotations.html'),
        //         locals              : {},
        //         animation          : true,
        //         parent             : angular.element(document.body),
        //         clickOutsideToClose: true,
        //     })
        //     this.$mdDialog.show(this.confirm).then((row) => {
        //         if (row) {
        //            const dataReturn = row.data
        //            if(dataReturn.length < 2) {
        //                this.loadItemCSV(dataReturn[0])
        //            }else {
        //                for(const i in dataReturn){
        //                    this.loadItemCSV(dataReturn[i])
        //                }
        //            }
                   
        //         }
        //     })
        // }

        // Funciones
    }   

budgetsCreateController.$inject = ['$scope','UserInfoConstant','$timeout','budgetsServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element'];