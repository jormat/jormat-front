/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> paymentsServices
* file for call at services for payments
*/

class paymentsServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var payments = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/:route',{},{
      
        getPayments: {
            method:'GET',
            params : {
               route: 'list'
            }
        },
        getPaymentsDetails: {
            method:'GET',
            params : {
               route: ''
            }
        },
        createPayment: {
            method:'POST',
            params : {
                route: ''
            }
        },
        updatePayment: {
            method:'PUT',
            params : {
                route: ''
            }
        },
        updateBallot: {
            method:'PUT',
            params : {
                route: 'ballot'
            }
        },
        disabledPayment: {
            method:'PUT',
            params : {
                route: 'delete'
            }
        },
        getPaymentsReport: {
            method:'GET',
            params : {
               route: 'report'
            }
        },
        getNotePayments: {
            method:'GET',
            params : {
               route: 'notes'
            }
        },
        updateNotesPayment: {
            method:'PUT',
            params : {
                route: 'notes'
            }
        },
        createNotesPayment: {
            method:'POST',
            params : {
                route: 'notes'
            }
        },
        getNnPayments: {
            method:'GET',
            params : {
               route: 'nn'
            }
        },
        createNnPayment: {
            method:'POST',
            params : {
                route: 'nn'
            }
        },
        updateNnStatus: {
            method:'PUT',
            params : {
                route: 'nn'
            }
        },
        getBallotPayments: {
            method:'GET',
            params : {
               route: 'ballot'
            }
        },
        createBallotPayment: {
            method:'POST',
            params : {
                route: 'ballot'
            }
        },
        updateBallotStatus: {
            method:'PUT',
            params : {
                route: 'ballot-status'
            }
        },
        getProvidersPayments: {
            method:'GET',
            params : {
               route: 'providers'
            }
        },
        createProviderPayment: {
            method:'POST',
            params : {
                route: 'providers'
            }
        },
        updateProviderInvoiceStatus: {
            method:'PUT',
            params : {
                route: 'providers-invoices-status'
            }
        },
        getPaymentsPages: {
            method:'GET',
            params : {
               route: 'Pagination'
            }
        }
    })

    var paymentForms = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/forms/:route',{},{
      
        getPaymentForms: {
            method:'GET',
            params : {
               route: 'list'
            }
        }
    })


    var notesPayments = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/notes/:route',{},{

        disabledNotesPayment: {
            method:'PUT',
            params : {
                route: 'delete'
            }
        }
    })


    var nnPayments = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/nn/:route',{},{

        disabledNnPayment: {
            method:'PUT',
            params : {
                route: 'delete'
            }
        }
    })

    var ballotPayments = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/ballot/:route',{},{

        disabledBallotPayment: {
            method:'PUT',
            params : {
                route: 'delete'
            }
        }
    })

    var providersPayments = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/providers/:route',{},{

        disabledProviderPayment: {
            method:'PUT',
            params : {
                route: 'delete'
            }
        }
    })

    return { 
            payments : payments,
            paymentForms : paymentForms,
            notesPayments: notesPayments,
            nnPayments:nnPayments,
            ballotPayments:ballotPayments,
            providersPayments:providersPayments
             
           }
  }
}

  paymentsServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.paymentsServices', [])
  .service('paymentsServices', paymentsServices)
  .name;