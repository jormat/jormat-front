/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentsReportController
* # As Controller --> paymentsReport														
*/
  export default class paymentsReportController {

    constructor($scope, $filter,$rootScope, $http,paymentsServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants,$modal,i18nService) {

    	var vm = this
        vm.loadPayments = loadPayments
        vm.print = print
        vm.viewInvoice = viewInvoice
        vm.loadPDF = loadPDF
        $scope.CurrentDate = moment($scope.date).format("YYYY-MM-DD")
	   
        // loadInvoices()

        i18nService.setCurrentLang('es');

        $scope.invoicesGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'facturas_emitidas_'+ moment($scope.startDate).format("YYYY-MM-DD") +'_y_'+ moment($scope.endDate).format("YYYY-MM-DD")+'.csv',
                exporterPdfHeader: { 
                    text: "Facturas emitidas entre los días : " + moment($scope.startDate).format("YYYY-MM-DD") +" y "+ moment($scope.endDate).format("YYYY-MM-DD"),
                    // text: "Reporte facturas por fecha", 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [0,20,0,0] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },
                columnDefs: [
                    { 
                      name:'Factura',
                      field: 'invoiceId',  
                      width: '6%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.paymentsReport.viewInvoice(row.entity)">{{row.entity.invoiceId}}</a>' +
                                 '</div>' 
                    },
                    
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      width: '28%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                        
                                 '</div>'        
                    },
                    { 
                      name:'id',
                      field: 'clientId',
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="badge bg-info uppercase" ng-click="grid.appScope.paymentsList.viewClient(row.entity)">{{row.entity.clientId}}</a>' +
                        
                                 '</div>'        
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                        
                                 '</div>'
                    },
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '10%'
                    },
                    { 
                      name:'Total',
                      field: 'total',
                      width: '9%',
                      // aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{row.entity.total}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Pendiente',
                      field: 'pending',
                      width: '9%',
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> ${{row.entity.pending}}</p>' +
                                 '</div>'
                    },
                    
                    
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="badge bg-{{row.entity.style}}">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Forma pago', 
                      field: 'paymentForm', 
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="font-italic label bg-secondary text-muted" style="font-size: 10px;">{{row.entity.paymentForm}}</p>' +
                                 '</div>'   
                    }
                ]
            }



        function loadPDF(){
                var doc = new jsPDF();
                doc.autoTable({html:'#content'});

        }


        function loadPayments(){

            let model = {
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            paymentsServices.payments.getPaymentsReport(model).$promise.then((dataReturn) => {
              $scope.invoicesGrid.data = dataReturn.data

              if ($scope.invoicesGrid.data.length == 0){
                      $scope.messageInvoices = true
                      ngNotify.set( 'No existes facturas creadas entre '+ model.startDate +' y '+ model.endDate , {
                      sticky: true,
                      button: true,
                      type : 'warn'
                  })
                  }else{
                      $scope.messageInvoices = false
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

        function viewInvoice(row){
                $scope.invoiceData = row;
                var modalInstance  = $modal.open({
                        template: require('../../clients-invoices/view/clients-invoices-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'clientsInvoicesViewController',
                        controllerAs: 'clientsInvoicesView',
                        size: 'lg'
                })
            }

	    function print() {
			window.print();
		}



    }

  }

  paymentsReportController.$inject = ['$scope', '$filter','$rootScope', '$http','paymentsServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants','$modal','i18nService'];
