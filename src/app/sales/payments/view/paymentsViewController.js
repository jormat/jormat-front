
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> paymentsViewController
* # As Controller --> paymentsView														
*/

export default class paymentsViewController{

        constructor($scope,UserInfoConstant,$timeout,paymentsServices,clientsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$stateParams,warehousesServices,itemsServices,$modal,clientsInvoicesServices){
            var vm = this
            vm.viewInvoice = viewInvoice 
            vm.deletePayment = deletePayment
            vm.createPayment = createPayment
            vm.getInvoiceDetails= getInvoiceDetails
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")

            getPayments()
            getInvoiceDetails()

            //function to show userId


            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            //

            function getInvoiceDetails(){
                  let params = {
                    invoiceId: $stateParams.idInvoice 
                }

                clientsInvoicesServices.invoices.getInvoicesDetails(params).$promise.then((dataReturn) => {
                  $scope.invoices = dataReturn.data;
                  $scope.status = $scope.invoices[0].statusName
                  console.log('servicio devuelve estado factura',$scope.invoices);

                  },(err) => {
                      console.log('No se ha podido llamar con el servicio getInvoicesDetails',err);
                })
            } 

            

            $scope.invoiceId = $stateParams.idInvoice
            $scope.clientId = $stateParams.idClient
            $scope.clientName = $stateParams.clientName
            $scope.total = parseInt($stateParams.total)
            $scope.date = $stateParams.date
            // $scope.pending = $stateParams.pending
            $scope.status = $stateParams.status

            if ($scope.pending == 0) {
                $scope.statusPayment = 'Cancelada'

                
            }else{
                $scope.statusPayment = 'Pendiente'
            }

        
            function getPayments(){
                let params = {
                    invoiceId: $stateParams.idInvoice 
                }
                paymentsServices.payments.getPaymentsDetails(params).$promise.then((dataReturn) => {
                  $scope.paymentsGrid = dataReturn.data;
                  var sumPayments = 0
                  var i = 0
                     for (i=0; i<$scope.paymentsGrid.length; i++) {
                         sumPayments = sumPayments + $scope.paymentsGrid[i].total  
                      }
                      $scope.sum =  sumPayments
                      $scope.pending = $stateParams.total - $scope.sum
                      if ($scope.pending == 0) {
                            $scope.statusPayment = 'Cancelada'
                            paymentsServices.payments.updatePayment(params).$promise.then((dataReturn) => {
                                ngNotify.set('Esta factura ha sido cancelada completamente','warn')
                            },(err) => {
                                console.log('No se ha podido conectar con el servicio getPaymentForms',err);
                            })
                        }else{
                            $scope.statusPayment = 'Pendiente'
                        }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err)
                  })

            }
            
            paymentsServices.paymentForms.getPaymentForms().$promise.then((dataReturn) => {
              $scope.paymentForms = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getPaymentForms',err);
            })

            function deletePayment(paymentsId){
                  $scope.paymentsId = paymentsId;
                  $scope.invoiceId = $stateParams.idInvoice
                  var modalInstance  = $modal.open({
                          template: require('../delete/payments-delete.html'),
                          animation: true,
                          scope: $scope,
                          controller: 'paymentsDeleteController',
                          controllerAs: 'paymentsDelete'
                  })
            } 

            function viewInvoice(invoiceId,clientName){
                $scope.invoiceData = {
                    invoiceId:invoiceId,
                    clientName:clientName
                }
                var modalInstance  = $modal.open({
                        template: require('../../clients-invoices/view/clients-invoices-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'clientsInvoicesViewController',
                        controllerAs: 'clientsInvoicesView',
                        size: 'lg'
                })
            }

            function createPayment() {

                    let model = {
                        userId: $scope.userInfo.id,
                        invoiceId: $stateParams.idInvoice,
                        clientId : $stateParams.idClient,
                        date : $scope.CurrentDate,
                        total : $scope.payment,
                        numberDocument : $scope.document,
                        paymentFormId : $scope.paymentFormId,
                        comment : $scope.observation
                    }     

                     paymentsServices.payments.createPayment(model).$promise.then((dataReturn) => {
                        ngNotify.set('Se ha actualizado el pago correctamente','success')
                        getInvoiceDetails()
                        getPayments()
                        $scope.observation = ''
                        $scope.payment = ''
                        $scope.document = ''
                      },(err) => {
                        ngNotify.set('Error al ingresar pago','error')
                     })
                }
            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

paymentsViewController.$inject = ['$scope','UserInfoConstant','$timeout','paymentsServices','clientsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$stateParams','warehousesServices','itemsServices','$modal','clientsInvoicesServices'];

