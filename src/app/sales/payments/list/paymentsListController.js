
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentsListController
* # As Controller --> paymentsList														
*/

export default class paymentsListController{

        constructor($scope,UserInfoConstant,$timeout,paymentsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService,clientsInvoicesServices,clientsServices,$rootScope){
          var vm = this
          this.$scope = $scope
          this.uiGridConstants = uiGridConstants
          this.paymentsServices=paymentsServices
          vm.setPayments= setPayments
            //vm.searchData=searchData
            vm.loadExpirationInvoices = loadExpirationInvoices
            this.$rootScope = $rootScope
            this.$interval = $interval
            this.$modal = $modal
			      this.$state = $state
            vm.print = print
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            //function to show userId
            this.$scope.UserInfoConstant = UserInfoConstant
            this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })

            this.$scope.dropdownMenu = require('./dropdownActionsMenu.html')
            this.tableInitializeMethod() 

            this.$scope.UserInfoConstant = UserInfoConstant
            this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
				    if (details !== undefined) {
              this.$scope.userInfo = details.user
					  let model = {
						userId :  this.$scope.userInfo.id
					  }
					 this.main()
				}
			})
            
            function print() {
              window.print();
            } 

            function loadExpirationInvoices(){

              $scope.date = new Date()

              clientsInvoicesServices.invoices.getExpirationInvoices().$promise.then((dataReturn) => {
                $scope.invoicesCreditList = dataReturn.data;
                ngNotify.set('Operación en segundo plano, puede seguir trabajando','warn')
                var i = 0
                     for (i=0; i<$scope.invoicesCreditList.length; i++) {
                            console.log($scope.invoicesCreditList[i]);
                           
                            $scope.expirationDate = new Date($scope.invoicesCreditList[i].expirationDate)
                            $scope.clientId = $scope.invoicesCreditList[i].clientId
                            $scope.dateInvoice = new Date($scope.invoicesCreditList[i].dateInvoice)
                            if($scope.expirationDate <= $scope.date){
                              console.log('Vencido / fecha vencimiento igual o menor a la fecha actual');
                              console.log('ExpiracionDate',$scope.expirationDate);
                              console.log('fecha hoy',$scope.date);
                              console.log('fecha facturas',$scope.dateInvoice);

                              const diffInDays = Math.floor(( $scope.expirationDate - $scope.dateInvoice ) / (1000 * 60 * 60 * 24));
                              console.log('dias',diffInDays);

                              //bloquear cliente
                              if (diffInDays >= 120) {

                                console.log('bloquear cliente',diffInDays);

                                let model = {

                                  clientId: $scope.clientId,
                                  blockingReason: 'Facturas fuera de plazo 120 días',
                                  isLocked: 1
                                }

                                clientsServices.clients.lockedClient(model).$promise.then((dataReturn) => {
                                    console.log('Se ha bloqueado el cliente por DTE fuera de plazo',diffInDays);
                                  },(err) => {
                                      console.log('Error al actualizar cliente',err);
                                  })
                              }

                              let params = {
                                invoiceId: $scope.invoicesCreditList[i].invoiceId,
                                value:10,
                                userId: $scope.userInfo.id
                                }
                
                                clientsInvoicesServices.invoices.setStatusInvoices(params).$promise.then((dataReturn) => {
                                  $scope.result = dataReturn.data;
                                  console.log('servicio devuelve estado factura',$scope.result);
                                  this.loadInvoicesPages(this.params)
                                  },(err) => {
                                      console.log('No se ha podido cambiar estado vencido factura',err);
                                })


                            }else{
                              console.log('fecha vencimiento mayor a la fecha actual');
                              console.log('ExpiracionDate',$scope.expirationDate);
                              console.log('fecha hoy',$scope.date);
                              console.log('fecha factura',$scope.dateInvoice);
                            }
                      }
                
                },(err) => {
                    console.log('No se ha podido conectar con el servicio',err);
                })
          }


            function setPayments(row){
              $state.go("app.paymentsView", { 
                idInvoice: row.invoiceId,
                idClient: row.clientId,
                clientName: row.clientName,
                total: row.total,
                date: row.date
              });
            }
            
        }//FIN CONSTRUCTOR


        main() {
          const searchText = ''
          this.params = {
            newPage    	   : 1,
            pageSize       : 2000,
            searchText	   : this.searchText
          }
          this.loadInvoicesPages()
          this.$rootScope.$on('$translateChangeSuccess', () => {
          this.tableInitializeMethod() 
          })
        }



        searchData() {
          this.params.searchText = this.searchText
          this.params.newPage = 1
          console.log("searchdata:", this.searchText)
          this.paymentsServices.payments.getPaymentsPages(this.params).$promise.then(
            response => {
              if (!response.status) {
                const msn = response.messageError || response.message || 'ERROR'
      
                this.ngNotify.set(msn, 'warn')
              } else {
                this.payments = angular.copy(response.data)
                this.$scope.paymentsGrid.data = response.data
                this.$scope.paymentsGrid.totalItems = response.count || 0
              }
            },
            () => {
              this.ngNotify.set('Sin respuesta getItemsMain', 'error')
            },
          )
        }


        accountStatusView(row) {

          const url = this.$state.href("app.accountStatus", { 
              idClient: row.clientId
            });

          window.open(url,'_blank');
        
      }

        viewClient(row){
          this.$scope.clientData = row;
          const modalInstance  = this.$modal.open({
                  template: require('../../clients/view/clients-view.html'),
                  animation: true,
                  scope: this.$scope,
                  controller: 'clientsViewController',
                  controllerAs: 'clientsView',
          })
      }

        viewInvoice(row){
          this.$scope.invoiceData = row;
          const modalInstance  = this.$modal.open({
                  template: require('../../clients-invoices/view/clients-invoices-view.html'),
                  animation: true,
                  scope: this.$scope,
                  controller: 'clientsInvoicesViewController',
                  controllerAs: 'clientsInvoicesView',
                  size: 'lg'
          })
      }


        tableInitializeMethod() {         
     
          this.$scope.paymentsGrid = {
            paginationPageSizes  : [ 500, 1000, 2000 ],
            paginationPageSize   : 2000,
            enablePaging         : true,
            useExternalPagination: true,
            enableFiltering      : true,
            enableGridMenu       : true,
            enableSelectAll      : true,
            showColumnFooter     : true,
            exporterMenuPdf      : false,
            exporterMenuCsv      : false,
            exporterMenuAllData  : false,
            multiSelect          : true,
            exporterCsvFilename: 'listado_pagos.csv',
            onRegisterApi        : (gridApi) => {
             this.gridApi = gridApi   
             
             this.$interval(() => {
              this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar vista como csv', action: () => {
                this.exportCSV()
              }, order: 100 } ])
              this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar todo como csv', action: () => {
                this.exportAllCSV()
              }, order: 100 } ])
            }, 0, 1)
      
              gridApi.pagination.on.paginationChanged(this.$scope, (newPage, pageSize) => {
                //revisar si llegan valores
                this.params.newPage = newPage
                this.params.pageSize = pageSize 
               
                //console.log("paramss:",this.params )
                this.paymentsServices.payments.getPaymentsPages(this.params).$promise.then(
                  
                  response => {

                    if (!response.status) {
                      const msn = response.messageError || response.message || 'ERROR'
                      this.ngNotify.set(msn, 'warn')
                    } else {
                      this.payments = angular.copy(response.data)             
                      this.$scope.paymentsGrid.data = response.data
                      this.$scope.paymentsGrid.totalItems = response.count || 0
                    } 
                  },
                  () => {
                    this.ngNotify.set('Sin respuesta getItemsMain', 'error')
                  },
                )
              })
      
              gridApi.core.on.filterChanged(this.$scope, () => {
                const grid = gridApi.grid.renderContainers.body.grid
                let filters = {}
      
                for (let i in grid.columns) {
                  if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term != undefined)) {
                    filters[grid.columns[i].field] = grid.columns[i].filters[0].term
                  }
                }
    
                this.params.newPage = 1
                this.params.filters = filters
      
                this.paymentsServices.payments.getPaymentsPages(this.params).$promise.then(
                  response => {
                    
                    if (!response.status) {
                      console.log("texto3");
                      const msn = response.messageError || response.message || 'ERROR'
            
                      this.ngNotify.set(msn, 'warn')
                    } else {
                      this.payments = angular.copy(response.data)
                      this.$scope.paymentsGrid.data = response.data
                      this.$scope.paymentsGrid.totalItems = response.count || 0
                    }
                   
                  },
                  () => {
                    this.ngNotify.set('Sin respuesta getItemsMain', 'error')
                  },
                )
              })
            },
            columnDefs: [
                        { 
                            name:'Factura',
                            field: 'invoiceId',  
                            width: '6%',
                            cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                     '<a tooltip-placement="right" tooltip="Ver factura" ng-click="grid.appScope.paymentsList.viewInvoice(row.entity)">{{row.entity.invoiceId}}</a>' +
                                     '</div>',
                            cellClass: function(grid, row) {
                              if (row.entity.statusName === 'Vencida') {
                    return 'critical';
                  }
                           }  
                        },
              { 
                name:'Cliente',
                field: 'clientName',
                width: '25%',
                cellTemplate:'<div class="ui-grid-cell-contents ">'+
                      '<a tooltip-placement="right" tooltip="Ver estado cuenta" class="uppercase" ng-click="grid.appScope.paymentsList.accountStatusView(row.entity)" style="font-size: 12px;"><strong>{{row.entity.clientName}}</strong></a>' +
                      '</div>',
                cellClass: function(grid, row) {
                  if (row.entity.statusName === 'Vencida') {
                    return 'critical';
                  }
                           }        
                },
                
                { 
                name:'id',
                field: 'clientId',
                width: '5%',
                cellTemplate:'<div class="ui-grid-cell-contents ">'+
                  '<a tooltip-placement="right" tooltip="Ver cliente" class="badge bg-info uppercase" ng-click="grid.appScope.paymentsList.viewClient(row.entity)">{{row.entity.clientId}}</a>' +
                  '</div>',
                cellClass: function(grid, row) {
                  if (row.entity.statusName === 'Vencida') {
                    return 'critical';
                    }
                  }        
                },
                { 
                  name: 'Rut', 
                  field: 'rut', 
                  width: '9%',
                  cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 11px;">{{row.entity.rut}}</p>' +
                                 '</div>', 
                  cellClass: function(grid, row) {
                                if (row.entity.statusName === 'Vencida') {
                                  return 'critical';
                      }
                    } 
                  },
                { 
                name: 'Fecha', 
                field: 'date',
                width: '7%',
                cellTemplate:'<div class="ui-grid-cell-contents">'+
                '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                '</div>',
                cellClass: function(grid, row) {
                 if (row.entity.statusName === 'Vencida') {
                    return 'critical';
                    }
                  } 
                },
                { 
                  name: 'Origen', 
                  field: 'origin', 
                  width: '8%',
                  cellClass: function(grid, row) {
                                if (row.entity.statusName === 'Vencida') {
                                  return 'critical';
                      }
                    } 
                  },
                  { 
                name:'Total',
                field: 'total',
                width: '8%',
                aggregationHideLabel: false,
                cellTemplate:'<div class="ui-grid-cell-contents">'+
                                     '<p style="font-size: 12px;"> $<strong> {{ row.entity.total | pesosChilenos }}</strong> </p>' +
                                     '</div>',
                            cellClass: function(grid, row) {
                              if (row.entity.statusName === 'Vencida') {
                                return 'critical';
                              }
                  } 
                },
                { 
                  name:'Pendiente',
                  field: 'pending',
                  width: '10%',
                  aggregationType: this.uiGridConstants.aggregationTypes.sum,
                  cellTemplate:'<div class="ui-grid-cell-contents">'+
                             '<p style="font-size: 12px;"> $<strong>{{row.entity.pending | pesosChilenos}}</strong></p>' +
                             '</div>',
                  cellClass: function(grid, row) {
                  if (row.entity.statusName === 'Vencida') {
                    return 'critical';
                  }
                 }
                },
                
                { 
                name: 'Estado', 
                field: 'statusName', 
                width: '8%',
                cellTemplate:'<div class="ui-grid-cell-contents ">'+
                      '<p style="font-size: 11px;" class="badge bg-{{row.entity.style}}">{{row.entity.statusName}}</p>' +
                      '</div>',
                cellClass: function(grid, row) {
                              if (row.entity.statusName === 'Vencida') {
                                return 'critical';
                              }
                           } 
                        },{ 
                          name: 'Forma pago', 
                          field: 'paymentForm', 
                          width: '9%',
                          cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="font-italic label bg-secondary text-muted" style="font-size: 9px;">{{row.entity.paymentForm}}</p>' +
                                 '</div>',
                          cellClass: function(grid, row) {
                          if (row.entity.statusName === 'Vencida') {
                            return 'critical';
                          }
                           }   
                          },
                        { 
                name: '',
                width: '6%', 
                field: 'href',
                cellTemplate:this.$scope.dropdownMenu,
                cellClass: function(grid, row) {
                  if (row.entity.statusName === 'Vencida') {
                    return 'critical';
                  }
                           },
                enableFiltering: false
                        }
                    ]
                }
        }

        loadInvoicesPages(){
               
          //console.log("texto001", params); 
          this.paymentsServices.payments.getPaymentsPages(this.params).$promise.then((dataReturn) => {
            this.payments = angular.copy(dataReturn.data)            
            this.$scope.paymentsGrid.data = dataReturn.data
            this.$scope.paymentsGrid.totalItems = dataReturn.count || 0
            this.$scope.paymentsGrid.data.forEach((row, index) => {
              row.sequence = index;
      })
            
            },(err) => {
                console.log('No se ha podido conectar con el servicio',err);
            })
         
        }       
    }   

paymentsListController.$inject = ['$scope','UserInfoConstant','$timeout','paymentsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService','clientsInvoicesServices','clientsServices','$rootScope'];

