/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> paymentVoucherServices
* file for call at services for paymentVoucher
*/

class paymentVoucherServices {

	constructor($resource,SERVICE_URL_CONSTANT) {
		
		var paymentVoucher = $resource(SERVICE_URL_CONSTANT.jormat + '/finance/vouchers/:route',{},{
			
				getVouchers: {
						method:'GET',
						params : {
							 route: 'list'
						}
				},
				getVoucherDetails: {
						method:'GET',
						params : {
							 route: ''
						}
				},
				createVoucher: {
						method:'POST',
						params : {
								route: ''
						}
				},
				getVouchersItems: {
						method:'GET',
						params : {
							 route: 'payments'
						}
				},
				getVouchersReport: {
						method:'GET',
						params : {
							 route: 'report'
						}
				},
				getSalesByWarehouse: {
						method:'GET',
						params : {
								route: 'sales'
						}
				},
				paymentVoucherUpdate: {
						method:'PUT',
						params : {
								route: 'validate'
						}
				},
				paymentVoucherItemsUpdate: {
						method:'PUT',
						params : {
								route: 'click'
						}
				},
				getVoucherPages: {
					method:'GET',
					params : {
						route: 'Pagination'
					}
				}
		})

		return { paymentVoucher : paymentVoucher
						 
					 }
	}
}

	paymentVoucherServices.$inject=['$resource','SERVICE_URL_CONSTANT']

	export default  angular.module('services.paymentVoucherServices', [])
	.service('paymentVoucherServices', paymentVoucherServices)
	.name;