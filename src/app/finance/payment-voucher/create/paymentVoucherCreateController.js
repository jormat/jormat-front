
/**
* @name APP jormat
* @autor 
* @description
* # name controller --> paymentVoucherCreateController
* # As Controller --> paymentVoucherCreate														
*/

export default class paymentVoucherCreateController{

        constructor($scope,UserInfoConstant,$timeout,paymentVoucherServices,paymentsServices,officePaymentsServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,warehousesServices,itemsServices,providersServices,$modal,$element){
            var vm = this;
            vm.createPayments = createPayments
            vm.setWarehouseId = setWarehouseId
            vm.viewDocument = viewDocument
            vm.loadItem = loadItem
            vm.removeItems = removeItems
            vm.showPanel = showPanel
            vm.getNet = getNet
            vm.loadDocuments = loadDocuments
            $scope.parseInt = parseInt
            $scope.warningItems = true 
            $scope.date = new Date()
            $scope.CurrentDate = moment($scope.date).format("DD-MM-YYYY")
            $scope.editables = {
                    disccount: '0',
                    quantity: '1'
                }

            //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                        let model = {
                            userId : $scope.userInfo.id
                        }
                        warehousesServices.warehouses.getWarehousesByUser(model).$promise.then((dataReturn) => {
                            $scope.warehouses = dataReturn.data;
                            console.log('$scope.warehouses',$scope.warehouses)
                            },(err) => {
                            console.log('No se ha podido conectar con el servicio',err);
                        })
                    }
                })
            //

            paymentsServices.paymentForms.getPaymentForms().$promise.then((dataReturn) => {
              $scope.paymentForms = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio getPaymentForms',err);
            })


            $scope.paymentsGrid = {
                enableFiltering: true,
                enableHorizontalScrollbar :0,
                columnDefs: [
                    { 
                      name: '',
                      field: 'href',
                      enableFiltering: false,
                      width: '5%', 
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                   '<button ng-click="grid.appScope.paymentVoucherCreate.loadItem(row.entity)" type="button" class="btn btn-primary btn-xs">+</button>'+
                                   '</div>'
                    },
                    { 
                      name:'id',
                      field: 'documentId', 
                      enableFiltering: true,
                      width: '13%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase text-muted" ng-click="grid.appScope.paymentVoucherCreate.viewDocument(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Tipo',
                      field: 'type',
                      enableFiltering: true,
                      width: '15%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p style="font-size: 13px;"><span class="label bg-warning">  {{row.entity.type}}</span></p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Cliente',
                      field: 'clientName',
                      enableFiltering: true,
                      width: '32%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" >{{row.entity.clientName}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Fecha',
                      field: 'date',
                      enableFiltering: true,
                      width: '14%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 11px;" >{{row.entity.date}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Total',
                      field: 'totalDocument',
                      enableFiltering: true,
                      width: '17%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p ><strong> ${{row.entity.totalDocument}}</strong></p>' +
                                 '</div>'  
                    }
                ]
            };
            
           function loadDocuments(warehouseId){

                let model = {
                    warehouseId : warehouseId
                }

                officePaymentsServices.officePayments.getSalesByWarehouse(model).$promise.then((dataReturn) => {
                      $scope.paymentsGrid.data = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })
           }
            

            $scope.paymentsItems = []

            function loadItem(row){

                 $scope.paymentsItems.push({
                    documentId: row.documentId,
                    type: row.type,
                    typeId: row.typeId,
                    date: row.date,
                    total: row.total, //valor pendiente pago documento
                    totalDocument: row.totalDocument, //valor total documento
                    paymentMethod: '',
                    pay: row.total //pago pendiente maximo/ideal

                }) 

                $scope.warningItems = false 
            }


            function getNet() {
                var total = 0
                var i = 0
                for (i=0; i<$scope.paymentsItems.length; i++) {
                    var valueItems = $scope.paymentsItems[i] 
                    total += Math.round(valueItems.pay); 

                  }
                  
                  $scope.total =  total
                  return total;
                      
            }


            function removeItems(index) {
                $scope.paymentsItems.splice(index, 1)
                $scope.warningQuantity = false
            }

            function viewDocument(row){

                $scope.invoiceData = row;

                if ($scope.invoiceData.type == "Factura") {

                     $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName
                    }

                    var modalInstance  = $modal.open({
                            template: require('../../../sales/clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if ($scope.invoiceData.type == "Boleta") { 

                    $scope.ballotsData = {
                        ballotId:$scope.invoiceData.documentId 
                    }

                    var modalInstance  = $modal.open({
                            template: require('../../../sales/ballots/view/ballots-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }
            }

            function setWarehouseId(warehouseId,warehouseCode){
                loadDocuments(warehouseId)
                $scope.warehouseId = warehouseId
                $scope.warehouseCode = warehouseCode
                
            }

            function showPanel() {
                
                $scope.panelInvoice = true
                $scope.paymentsItems = []
                $scope.warningQuantity = false
            }

            function createPayments() {

                        let model = {
                            userId: $scope.userInfo.id,
                            date : $scope.CurrentDate,
                            originId : $scope.warehouseId,
                            total : $scope.total,
                            comment : $scope.observation,
                            paymentsItems : $scope.paymentsItems 
                        }      

                        console.log('modelo',model)   

                        paymentVoucherServices.paymentVoucher.createVoucher(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha creado el comprobante correctamente','success')
                            // $scope.result = dataReturn.data
                            $state.go('app.voucher')

                          },(err) => {
                              ngNotify.set('Error al enviar pago','error')
                          })
                    }


        }//FIN CONSTRUCTOR

        // Funciones
    }   

paymentVoucherCreateController.$inject = ['$scope','UserInfoConstant','$timeout','paymentVoucherServices','paymentsServices','officePaymentsServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','warehousesServices','itemsServices','providersServices','$modal','$element'];