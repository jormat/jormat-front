/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentVoucherPrintController
* # As Controller --> paymentVoucherPrint														
*/

  export default class paymentVoucherPrintController {

    constructor($scope, $filter,$rootScope, $http,paymentVoucherServices,paymentsServices,$location,UserInfoConstant,ngNotify,$state,$stateParams,$modal,i18nService,uiGridConstants) {

    	var vm = this
    	vm.cancel=cancel
        vm.print=print
        vm.viewVoucher= viewVoucher
        vm.viewDocument = viewDocument
        vm.loadPaymentVoucher = loadPaymentVoucher

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                    $scope.nameUser = $scope.userInfo.usLastName
                }
            })

        loadPaymentVoucher()

            i18nService.setCurrentLang('es');
            
            $scope.voucherGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'listado_comprobantes_pagos.csv',
                enableGridMenu: true,
                showColumnFooter: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'id',  
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.paymentVoucherPrint.viewVoucher(row.entity)">{{row.entity.id}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Cliente', 
                      field: 'clientName', 
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.clientName}}</p>' +
                                 '</div>'   
                    },
                    { 
                      name:'Pago',
                      field: 'total',
                      width: '11%',
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> <strong>${{row.entity.total}}</strong></p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Forma pago', 
                      field: 'paymentForm', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-info" style="font-size: 12px;">{{row.entity.paymentForm}}</p>' +
                                 '</div>'
                    },
                    
                    { 
                      name:'Documento',
                      field: 'documentId',  
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.paymentVoucherPrint.viewDocument(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Tipo',
                      field: 'typeDoc',  
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class=" badge bg-accent" style="font-size: 10px;">{{row.entity.typeDoc}}</a>' +
                                 '</div>' 
                    },
                    
                    { 
                      name: 'Fecha', 
                      field: 'payDate',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.payDate | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Usuario', 
                      field: 'userCreation', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-warning font-italic" style="font-size: 10px;">{{row.entity.userCreation}}</p>' +
                                 '</div>'   
                    }
                ]
            }

            function loadPaymentVoucher(){

                let model = {
                    startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                    endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
                }

                paymentVoucherServices.paymentVoucher.getVouchersReport(model).$promise.then((dataReturn) => {
                  $scope.voucherGrid.data = dataReturn.data;
                  console.log('data',$scope.voucherGrid.data)

                    if ($scope.voucherGrid.data.length == 0){
                          $scope.messageInvoices = true
                          ngNotify.set( 'No existes pagos efectuados entre '+ model.startDate +' y '+ model.endDate , {
                          sticky: true,
                          button: true,
                          type : 'warn'
                      })
                      }else{
                          $scope.messageInvoices = false
                    }

                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            }

    	function cancel() {

		 $state.go("app.vouchers")

		}

        function viewVoucher(row){

                $scope.invoiceData = {
                    documentId:row.documentId,
                    clientName:row.clientName
                }

                if (row.typeDoc == "Factura") {

                     $scope.paymentVoucherData = row;
                        var modalInstance  = $modal.open({
                                template: require('../view/payment-voucher-view.html'),
                                animation: true,
                                scope: $scope,
                                controller: 'paymentVoucherViewController',
                                controllerAs: 'paymentVoucherView'
                        })
                    
                }

                if (row.typeDoc == "NN") { 

                    $scope.invoiceData = {
                        documentId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName 
                    }
                    console.log('ver documento nn');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/documents-nn/view/documents-nn-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'documentsNnViewController',
                            controllerAs: 'documentsNnView',
                            size: 'lg'
                    })

                }

                if (row.typeDoc == "Boleta") { 

                    $scope.ballotsData = {
                        ballotId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName 
                    }
                    console.log('Boleta');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/ballots/view/ballots-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }
              }

      
            function viewDocument(row){
                $scope.invoiceData = {
                    documentId:row.documentId,
                    clientName:row.clientName
                }

                if (row.typeDoc == "Factura") {

                    $scope.invoiceData = {
                        invoiceId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName
                    }
                    console.log('ver factura');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if (row.typeDoc == "NN") { 

                    $scope.invoiceData = {
                        documentId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName 
                    }
                    console.log('ver documento nn');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/documents-nn/view/documents-nn-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'documentsNnViewController',
                            controllerAs: 'documentsNnView',
                            size: 'lg'
                    })

                }

                if (row.typeDoc == "Boleta") { 

                    $scope.ballotsData = {
                        ballotId:$scope.invoiceData.documentId,
                        clientName:$scope.invoiceData.clientName 
                    }
                    console.log('Boleta');
                    var modalInstance  = $modal.open({
                            template: require('../../../sales/ballots/view/ballots-view.html'),
                            animation: true,
                            scope: $scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }

            }


  
        function print() {
              window.print();

        }


    }

  }

  paymentVoucherPrintController.$inject = ['$scope', '$filter','$rootScope', '$http','paymentVoucherServices','paymentsServices','$location','UserInfoConstant','ngNotify','$state','$stateParams','$modal','i18nService','uiGridConstants' ];
