/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentVoucherViewController
* # As Controller --> paymentVoucherView														
*/


  export default class paymentVoucherViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,paymentVoucherServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
    	vm.transformToInvoice = transformToInvoice
      	$scope.voucherId = $scope.paymentVoucherData.id
      	$scope.clientName = $scope.paymentVoucherData.userCreation

	      let params = {
	            voucherId: $scope.voucherId
	          }

	        paymentVoucherServices.paymentVoucher.getVoucherDetails(params).$promise.then((dataReturn) => {
	              $scope.voucher = dataReturn.data
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get voucher Details',err);
	              })

	        paymentVoucherServices.paymentVoucher.getVouchersItems(params).$promise.then((dataReturn) => {
	              $scope.vouchersItems = dataReturn.data
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get officePayment items',err);
	              })

	    	function cancel() {
				$modalInstance.dismiss('chao');
			}

			function transformToInvoice() {
              $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.paymentId,discount:$scope.officePayment[0].discount,type:'cotizacion'});

        }

    }

  }

  paymentVoucherViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','paymentVoucherServices','$location','UserInfoConstant','ngNotify','$state'];
