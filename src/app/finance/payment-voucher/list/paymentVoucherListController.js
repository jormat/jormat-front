
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> paymentVoucherListController
* # As Controller --> paymentVoucherList														
*/

export default class paymentVoucherListController{

        constructor($scope,UserInfoConstant,$timeout,paymentVoucherServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService,$rootScope){
            var vm = this
            this.$scope = $scope
            this.$rootScope = $rootScope
            this.uiGridConstants = uiGridConstants
            this.paymentVoucherServices=paymentVoucherServices
		        this.$filter = $filter;
            this.$timeout = $timeout
            this.$rootScope = $rootScope
            this.$interval = $interval
            this.$scope.selectedChecks = []
            this.$modal = $modal
           
            this.$state = $state

            this.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            this.$scope.dropdownMenu = require('./dropdownActionsMenu.html')
            i18nService.setCurrentLang('es');

		    this.tableInitializeMethod()

		    this.$scope.UserInfoConstant = UserInfoConstant
		    this.$scope.$watch('UserInfoConstant[0].details[0]', (details) => {
			if (details !== undefined) {
				this.$scope.userInfo = details.user
				this.main()
			    }
		    })
            

    
        }//FIN CONSTRUCTOR

        // Funciones

        main() {
            const searchText = ''
            this.params = {
                newPage    	   : 1,
                pageSize       : 2000,
                searchText
            }
    
            this.loadPaymentVoucherPages()
            this.$rootScope.$on('$translateChangeSuccess', () => {
            this.tableInitializeMethod()
            })
        }
    

              loadPaymentVoucherPages(){
                //console.log("paramsss:", this.params)
                  this.paymentVoucherServices.paymentVoucher.getVoucherPages(this.params).$promise.then((dataReturn) => {
                   this.paymentVoucher = angular.copy(dataReturn.data)
                    this.$scope.voucherGrid.data = dataReturn.data;
                    this.$scope.voucherGrid.totalItems = dataReturn.count || 0
                    this.$scope.voucherGrid.data.forEach((row, index) => {
                        row.sequence = index;
                })
                },(err) => {
                    console.log('No se ha podido conectar con el servicio',err);
                })
               
            }


        tableInitializeMethod() {         
     
            this.$scope.voucherGrid = {
              paginationPageSizes  : [ 500, 1000, 2000 ],
              paginationPageSize   : 2000,
              enablePaging         : true,
              useExternalPagination: true,
              enableFiltering      : true,
              enableGridMenu       : true,
              enableSelectAll      : true,
              showColumnFooter     : true,
              exporterMenuPdf      : false,
              exporterMenuCsv      : false,
              exporterMenuAllData  : false,
              multiSelect          : true,
              exporterCsvFilename: 'listado_comprobantes_pagos.csv',
              onRegisterApi        : (gridApi) => {
               this.gridApi = gridApi   
               
               this.$interval(() => {
                this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar vista como csv', action: () => {
                  this.exportCSV()
                }, order: 100 } ])
                this.gridApi.core.addToGridMenu(gridApi.grid, [ { title : 'Exportar todo como csv', action: () => {
                  this.exportAllCSV()
                }, order: 100 } ])
              }, 0, 1)
              
                this.gridApi.pagination.on.paginationChanged(this.$scope, (newPage, pageSize) => {
                  //revisar si llegan valores
                  this.params.newPage = newPage
                  this.params.pageSize = pageSize 
                 
                  //console.log("paramss:",this.params )
                  this.paymentVoucherServices.paymentVoucher.getVoucherPages(this.params).$promise.then(
                    
                    response => {
  
                      if (!response.status) {
                        const msn = response.messageError || response.message || 'ERROR'
                        this.ngNotify.set(msn, 'warn')
                      } else {
                        this.paymentVoucher = angular.copy(response.data)             
                        this.$scope.voucherGrid.data = response.data
                        this.$scope.voucherGrid.totalItems = response.count || 0
                      } 
                    },
                    () => {
                      this.ngNotify.set('Sin respuesta getItemsMain', 'error')
                    },
                  )
                })
        
                this.gridApi.core.on.filterChanged(this.$scope, () => {
                  const grid = gridApi.grid.renderContainers.body.grid
                  let filters = {}
        
                  for (let i in grid.columns) {
                    if ((grid.columns[i].filters[0].term != '') && (grid.columns[i].filters[0].term != undefined)) {
                      filters[grid.columns[i].field] = grid.columns[i].filters[0].term
                    }
                  }
      
                  this.params.newPage = 1
                  this.params.filters = filters
        
                  this.paymentVoucherServices.paymentVoucher.getVoucherPages(this.params).$promise.then(
                    response => {
                      
                      if (!response.status) {
                        console.log("texto3");
                        const msn = response.messageError || response.message || 'ERROR'
              
                        this.ngNotify.set(msn, 'warn')
                      } else {
                        this.paymentVoucher = angular.copy(response.data)
                        this.$scope.voucherGrid.data = response.data
                        this.$scope.voucherGrid.totalItems = response.count || 0
                      }
                     
                    },
                    () => {
                      this.ngNotify.set('Sin respuesta getItemsMain', 'error')
                    },
                  )
                })
              },
              columnDefs: [
                    { 
                        name:'id',
                        field: 'id',  
                        width: '7%',
                        cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.paymentVoucherList.viewVoucher(row.entity)">{{row.entity.id}}</a>' +
                                 '</div>',
                               
                    },
                    { 
                        name:'Cliente',
                        field: 'clientName',
                        width: '27%',
                        cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase" style="font-size: 12px;">{{row.entity.clientName}}</p>' +
                                 '</div>',
                         
                    },
                    { 
                        name:'Pago',
                        field: 'total',
                        width: '12%',
                        aggregationType: this.uiGridConstants.aggregationTypes.sum,
                        cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> <strong>${{row.entity.total}}</strong></p>' +
                                 '</div>',
                         
                    },
                    { 
                     name: 'forma pago', 
                        field: 'paymentForm',
                        width: '12%',
                        cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-info" style="font-size: 12px;">{{row.entity.paymentForm}}</p>' +
                                 '</div>',
                    },
                    { 
                        name: 'documento', 
                        field: 'documentId', 
                        width: '8%',
                        cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.paymentVoucherList.viewDocument(row.entity)">{{row.entity.documentId}}</a>' +
                                 '</div>'
                    },
                    { 
                        name:'tipo',
                        field: 'typeDoc',
                        width: '8%',
                        cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class=" badge bg-accent" style="font-size: 10px;">{{row.entity.typeDoc}}</a>' +
                                 '</div>'
                    },
                    { 
                        name: 'Fecha', 
                      field: 'payDate',
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.payDate | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    },
                  
                    {    
                        name: 'Usuario', 
                      field: 'userCreation', 
                      width: '12%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class=" label bg-warning font-italic" style="font-size: 10px;">{{row.entity.userCreation}}</p>' +
                                 '</div>'   
                    },
                    { 
                      name: 'Acciones',
                      width: '6%', 
                      field: 'href',
                      cellTemplate: this.$scope.dropdownMenu,
                      cellClass: function(grid, row) {
                        if (row.entity.statusName === 'Vencida') {
                          return 'critical';
                        }
                                 },
                      enableFiltering: false
                              }
                    
                      ]
                  }
          }

          viewVoucher(row){

            this.$scope.invoiceData = {
                documentId:row.documentId,
                clientName:row.clientName
            }

            if (row.typeDoc == "Factura") {

                 this.$scope.paymentVoucherData = row;
                    var modalInstance  = this.$modal.open({
                            template: require('../view/payment-voucher-view.html'),
                            animation: true,
                            scope: this.$scope,
                            controller: 'paymentVoucherViewController',
                            controllerAs: 'paymentVoucherView'
                    })
                
            }

            if (row.typeDoc == "NN") { 

                this.$scope.invoiceData = {
                    documentId:this.$scope.invoiceData.documentId,
                    clientName:this.$scope.invoiceData.clientName 
                }
                console.log('ver documento nn');
                const modalInstance  = this.$modal.open({
                        template: require('../../../sales/documents-nn/view/documents-nn-view.html'),
                        animation: true,
                        scope: this.$scope,
                        controller: 'documentsNnViewController',
                        controllerAs: 'documentsNnView',
                        size: 'lg'
                })

            }

            if (row.typeDoc == "Boleta") { 

                this.$scope.ballotsData = {
                    ballotId:this.$scope.invoiceData.documentId,
                    clientName:this.$scope.invoiceData.clientName 
                }
                console.log('Boleta');
                const modalInstance  = this.$modal.open({
                        template: require('../../../sales/ballots/view/ballots-view.html'),
                        animation: true,
                        scope: this.$scope,
                        controller: 'ballotsViewController',
                        controllerAs: 'ballotsView',
                        size: 'lg'
                })

            }
        }

        voucherPrint(row){
            
          this.$state.go("app.voucherPrint", { voucherId: row.voucherId});
        }

        searchData() {
          this.$scope.voucherGrid.data = this.$filter('filter')(this.paymentVoucher, this.searchText, undefined);
        }

         viewDocument(row){
                this.$scope.invoiceData = {
                    documentId:row.documentId,
                    clientName:row.clientName
                }

                if (row.typeDoc == "Factura") {

                    this.$scope.invoiceData = {
                        invoiceId:this.$scope.invoiceData.documentId,
                        clientName:this.$scope.invoiceData.clientName
                    }
                    console.log('ver factura');
                    const modalInstance  = this.$modal.open({
                            template: require('../../../sales/clients-invoices/view/clients-invoices-view.html'),
                            animation: true,
                            scope: this.$scope,
                            controller: 'clientsInvoicesViewController',
                            controllerAs: 'clientsInvoicesView',
                            size: 'lg'
                    })

                }

                if (row.typeDoc == "NN") { 

                    this.$scope.invoiceData = {
                        documentId:this.$scope.invoiceData.documentId,
                        clientName:this.$scope.invoiceData.clientName 
                    }
                    console.log('ver documento nn');
                    const modalInstance  = this.$modal.open({
                            template: require('../../../sales/documents-nn/view/documents-nn-view.html'),
                            animation: true,
                            scope: this.$scope,
                            controller: 'documentsNnViewController',
                            controllerAs: 'documentsNnView',
                            size: 'lg'
                    })

                }

                if (row.typeDoc == "Boleta") { 

                    this.$scope.ballotsData = {
                        ballotId:this.$scope.invoiceData.documentId,
                        clientName:this.$scope.invoiceData.clientName 
                    }
                    console.log('Boleta');
                    const modalInstance  = this.$modal.open({
                            template: require('../../../sales/ballots/view/ballots-view.html'),
                            animation: true,
                            scope: this.$scope,
                            controller: 'ballotsViewController',
                            controllerAs: 'ballotsView',
                            size: 'lg'
                    })

                }

            }


       

        
    }   

paymentVoucherListController.$inject = ['$scope','UserInfoConstant','$timeout','paymentVoucherServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService','$rootScope'];

