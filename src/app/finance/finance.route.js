
routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default function routes($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode(true);
      //$urlRouterProvider.otherwise('/');

  $stateProvider
     .state('app.banks', {
            abstract: false,
            url: '/bancos',
            template: require('./banks/list/banks-list.html'),
            controller: 'banksListController',
            controllerAs:'banksList'
      })

     .state('app.expenseControl', {
            abstract: false,
            url: '/control-gastos',
            template: require('./expense-control/list/expense-control-list.html'),
            controller: 'expenseControlListController',
            controllerAs:'expenseControlList'
      })

     .state('app.expenseControlCreate', {
            abstract: false,
            url: '/control-gastos/nuevo-ingreso',
            template: require('./expense-control/create/expense-control-create.html'),
            controller: 'expenseControlCreateController',
            controllerAs:'expenseControlCreate'
      })

     .state('app.expenseControlUpdate', {
            abstract: false,
            url: '/control-gastos/actualizar-gasto?expenseControlId',
            template: require('./expense-control/update/expense-control-update.html'),
            controller: 'expenseControlUpdateController',
            controllerAs:'expenseControlUpdate'
      })

     .state('app.voucher', {
            abstract: false,
            url: '/comprobantes-pago',
            template: require('./payment-voucher/list/payment-voucher-list.html'),
            controller: 'paymentVoucherListController',
            controllerAs:'paymentVoucherList'
      })

     .state('app.voucherPrint', {
            abstract: false,
            url: '/comprobantes-pago/reporte-pagos',
            template: require('./payment-voucher/print/payment-voucher-print.html'),
            controller: 'paymentVoucherPrintController',
            controllerAs:'paymentVoucherPrint'
      })

     .state('app.voucherCreate', {
            abstract: false,
            url: '/comprobantes-pago/crear-comprobante',
            template: require('./payment-voucher/create/payment-voucher-create.html'),
            controller: 'paymentVoucherCreateController',
            controllerAs:'paymentVoucherCreate'
      })
 
}
