/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> banksCreateController
* # As Controller --> banksCreate														
*/


  export default class banksCreateController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,banksServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.createBank = createBank;
	    

	    function createBank() {

                let model = {
                    bankName: $scope.bankName,
                    bankCode: $scope.bankCode
                }

                banksServices.banks.createBank(model).$promise.then((dataReturn) => {
                    ngNotify.set('Se ha creado el banco correctamente','success')
                    $modalInstance.dismiss('chao');
                    $state.reload('app.banks')
                  },(err) => {
                      ngNotify.set('Error al crear banco','error')
                })
            }

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		


    }

  }

  banksCreateController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','banksServices','$location','UserInfoConstant','ngNotify','$state'];
