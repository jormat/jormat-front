/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> banksServices
* file for call at services for banks
*/

class banksServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var banks = $resource(SERVICE_URL_CONSTANT.jormat + '/payments/banks/:route',{},{
      
      getBanks: {
        method:'GET',
          params : {
            route: 'list'
            }
          },

       createBank: {
        method:'POST',
        params : {
          route: ''
          }
        },
        
        viewBank: {
          method:'GET',
          params : {
            route: ''
            }
          },

        updateBank: {
          method:'PUT',
          params : {
            route: ''
            }
          },

        disabledBank: {
          method:'DELETE',
          params : {
            route: ''
            }
          }
    })

    return { banks : banks
             
           }
  }
}

  banksServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.banksServices', [])
  .service('banksServices', banksServices)
  .name;