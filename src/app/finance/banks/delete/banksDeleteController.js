/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> banksDeleteController
* # As Controller --> banksDelete														
*/


  export default class banksDeleteController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,categoriesServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.deleteCategories = deleteCategories;
	    $scope.categoryId = $scope.categoryData.categoryId
        $scope.categoryCode = $scope.categoryData.categoryCode
        $scope.categoryName = $scope.categoryData.categoryName

        //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
        //

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function deleteCategories() {
			let model = {  
                    categoryId: $scope.categoryId,
                    userId : $scope.userInfo.id
                }

            categoriesServices.categories.disabledCategory(model).$promise.then((dataReturn) => {
                ngNotify.set('Se ha inactivado la categoria correctamente','success')
                $modalInstance.dismiss('chao');
                $state.reload('app.categories')
              },(err) => {
                  ngNotify.set('Error al inactivar categoria','error')
            })
		}
    }

  }

  banksDeleteController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','categoriesServices','$location','UserInfoConstant','ngNotify','$state'];
