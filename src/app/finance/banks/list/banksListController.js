
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> banksListController
* # As Controller --> banksList														
*/

export default class banksListController{

        constructor($scope,UserInfoConstant,$timeout,banksServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            var vm = this;
            vm.searchData=searchData;
            vm.deletebanks= deletebanks;
            vm.viewBanks= viewBanks
            vm.bankCreate = bankCreate
            vm.updatebank= updatebank
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.banksGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_bancos.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'bankId',  
                      width: '15%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.banksList.viewBanks(row.entity)">{{row.entity.bankId}}</a>' +
                                 '</div>'  
                    },
                    { 
                      name:'Codigo',
                      field: 'code',
                      width: '25%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning">{{row.entity.code}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Nombre banco',
                      field: 'description',
                      width: '50%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.description}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Acciones',
                      width: '10%', 
                      field: 'href',
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
         		banksServices.banks.getBanks().$promise.then((dataReturn) => {
         		  $scope.banksGrid.data = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

        function searchData() {
                  banksServices.banks.getBanks().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.banksGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }

        function deletebanks(row){
                $scope.bankData = row;
                var modalInstance  = $modal.open({
                        template: require('../delete/banks-delete.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'banksDeleteController',
                        controllerAs: 'banksDelete'
                })
        }

        function viewBanks(row){
                $scope.bankData = row;
                var modalInstance  = $modal.open({
                        template: require('../view/banks-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'banksViewController',
                        controllerAs: 'banksView',
                })
        }

        function bankCreate(){
                var modalInstance  = $modal.open({
                        template: require('../create/banks-create.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'banksCreateController',
                        controllerAs: 'banksCreate',
                })
        }

        function updatebank(row){
            $state.go("app.banksUpdate", { idbank: row.bankId,name: row.bankName,code:row.bankCode});
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

banksListController.$inject = ['$scope','UserInfoConstant','$timeout','banksServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

