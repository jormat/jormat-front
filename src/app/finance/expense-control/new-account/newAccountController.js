/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> newAccountController
* # As Controller --> newAccount														
*/

  export default class newAccountController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,expenseControlServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
        vm.accountCreate = accountCreate;

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        function accountCreate() {

            let params = {
                    accountName: $scope.accountName,
                    accountCode: $scope.accountCode
                }

            // console.log('model',model);

            expenseControlServices.accounts.accountsCreate(params).$promise.then((dataReturn) => {
                  ngNotify.set('Se ha creado correctamente la cuenta','success')
                  $modalInstance.dismiss('chao');
                
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

	    function cancel() {
			$modalInstance.dismiss('chao');
		}

    }

  }

  newAccountController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','expenseControlServices','$location','UserInfoConstant','ngNotify','$state'];
