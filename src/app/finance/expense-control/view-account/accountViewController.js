/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> accountViewController
* # As Controller --> accountView														
*/

  export default class accountViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,expenseControlServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
        vm.loadAccountsDetails = loadAccountsDetails;
        vm.accountUpdate = accountUpdate;
        $scope.accountId = $scope.accountData.accountId

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        loadAccountsDetails()

        function loadAccountsDetails(){

            let params = {
                accountId: $scope.accountId
              }

            expenseControlServices.accounts.getAccountsDetails(params).$promise.then((dataReturn) => {
                  $scope.accounts = dataReturn.data;
                  $scope.accountsDetails  = { 
                        code: $scope.accounts[0].code,
                        description :  $scope.accounts[0].description
                      }
                  },(err) => {
                      console.log('No se ha podido listar las cuentas',err);
                })
        }

        function accountUpdate() {

            let model = {
                    accountName: $scope.accountsDetails.description,
                    accountCode: $scope.accountsDetails.code,
                    accountId: $scope.accountId
                }

            console.log('model',model);

            expenseControlServices.accounts.accountsUpdate(model).$promise.then((dataReturn) => {
                  ngNotify.set('Se ha actualizado correctamente la cuenta','success')
                  $modalInstance.dismiss('chao');
                
              },(err) => {
                  console.log('No se ha podido actualizar la cuenta',err);
              })
        }

	    function cancel() {
			$modalInstance.dismiss('chao');
		}

    }

  }

  accountViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','expenseControlServices','$location','UserInfoConstant','ngNotify','$state'];
