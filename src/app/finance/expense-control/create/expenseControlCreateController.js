/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> expenseControlCreateController
* # As Controller --> expenseControlCreate														
*/


  export default class expenseControlCreateController {

    constructor($scope, $filter,$rootScope, $http,expenseControlServices,$location,UserInfoConstant,ngNotify,$state,clientsServices,warehousesServices) {

    	var vm = this;
	    vm.cancel = cancel;
        vm.checkRut = checkRut
        $scope.rutValidate = true
        vm.createExpenseControl = createExpenseControl
        vm.setOriginId = setOriginId
        vm.setAccount = setAccount

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        expenseControlServices.accounts.getAccounts().$promise.then((dataReturn) => {
              $scope.accounts = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
              $scope.warehousesTo = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        function checkRut(rut) {
                // Despejar Puntos
                var valor = rut.replace('.','');
                // Despejar Guión
                valor = valor.replace('-','');
                
                // Aislar cuerpo y Dígito Verificador
                $scope.cuerpo = valor.slice(0,-1);
                $scope.dv = valor.slice(-1).toUpperCase();
                
                // Formatear RUN
                rut = $scope.cuerpo + '-'+ $scope.dv
                
                // Si no cumple con el mínimo ej. (n.nnn.nnn)
                if($scope.cuerpo.length < 7) { 
                    // rut.setCustomValidity("RUT Incompleto");
                    $scope.textValidation = "Incompleto";
                    $scope.rutValidate = true;
                    console.log("rut Incompleto");  
                    return false;
                }
                
                // Calcular Dígito Verificador
                $scope.suma = 0;
                $scope.multiplo = 2;
                
                // Para cada dígito del $scope.cuerpo

                for($scope.i=1;$scope.i<=$scope.cuerpo.length;$scope.i++) {
                
                    // Obtener su Producto con el Múltiplo Correspondiente
                    $scope.index = $scope.multiplo * valor.charAt($scope.cuerpo.length -$scope.i);
                    
                    // Sumar al Contador General
                    $scope.suma = $scope.suma + $scope.index;
                    
                    // Consolidar Múltiplo dentro del rango [2,7]
                    if($scope.multiplo < 7) { $scope.multiplo = $scope.multiplo + 1; } else { $scope.multiplo = 2; }
              
                }
                
                // Calcular Dígito Verificador en base al Módulo 11
                $scope.dvEsperado = 11 - ($scope.suma % 11);
                
                // Casos Especiales (0 y K)
                $scope.dv = ($scope.dv == 'K')?10:$scope.dv;
                $scope.dv = ($scope.dv == 0)?11:$scope.dv;
                
                // Validar que el $scope.cuerpo coincide con su Dígito Verificador
                if($scope.dvEsperado != $scope.dv) { 
                    // rut.setCustomValidity("RUT Inválido");
                    console.log("RUT Inválido"); 
                    $scope.textValidation = "RUT Inválido";
                    $scope.rutValidate = true;
                    return false; 
                }
                
                // Si todo sale bien, eliminar errores (decretar que es válido)
                // rut.setCustomValidity('');
                $scope.textValidation = "RUT Válido";
                console.log("RUT valido");
                $scope.rutValidate = false
        }

	    function cancel() {
			$state.go('app.expenseControl')
		}

        function setOriginId(originId){
            $scope.originId = originId
        }

        function setAccount(accountId){
            console.log('setBankId',accountId)
            $scope.accountId = accountId
        }

        function createExpenseControl() {
            console.log('save createExpenseControl');

            if ($scope.documentType == 1) {

                $scope.netValue = Math.round($scope.shareValue/1.19)

            }else{

                $scope.netValue = $scope.shareValue

            }

            let model = {
                            userId: $scope.userInfo.id,
                            folio : $scope.folio,
                            documentType : $scope.documentType,
                            responsable : $scope.responsable,
                            origin : $scope.originId,
                            shareValue : $scope.shareValue,
                            netValue : $scope.netValue,
                            accountId : $scope.accountId,
                            day : moment($scope.day).format("YYYY-MM-DD"),
                            providerName : $scope.providerName,
                            rut : $scope.rut,
                            details : $scope.details
                        }      

                        console.log('modelo',model)   

                        expenseControlServices.expenseControl.expenseControlCreate(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha ingresado correctamente el gasto','success')

                            $state.go('app.expenseControl')

                          },(err) => {
                              ngNotify.set('Error al ingresar gasto','error')
                          })

        }

    }

  }

  expenseControlCreateController.$inject = ['$scope', '$filter','$rootScope', '$http','expenseControlServices','$location','UserInfoConstant','ngNotify','$state','clientsServices','warehousesServices'];
