/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> accountsListController
* # As Controller --> accountsList														
*/


  export default class accountsListController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,expenseControlServices,$location,UserInfoConstant,ngNotify,$state,$modal) {

    	var vm = this;
	    vm.cancel = cancel;
        vm.loadAccounts = loadAccounts 
        vm.newAccount = newAccount
        vm.viewAccount = viewAccount
        vm.deleteAccount = deleteAccount

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        loadAccounts()

        $scope.accountsGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_cuentas.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'accountId',  
                      width: '8%' 
                    },
                    { 
                      name:'Código',
                      field: 'accountCode',
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary">{{row.entity.accountCode}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Nombre cuenta',
                      field: 'accountName',
                      width: '48%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.accountName}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Ver',
                      width: '7%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<a ng-click="grid.appScope.accountsList.viewAccount(row.entity)"><span class="label bg-accent"><i class="mdi-file-find"></i>Ver</span></a>' +
                                 '</div>' 
                    },
                    { 
                      name: '',
                      width: '10%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate:'<div class="ui-grid-cell-contents" has-permission="control-gastos-validate">'+
                                 ' <a ng-click="grid.appScope.accountsList.deleteAccount(row.entity)"><span class="label bg-danger"><i class="mdi-close"></i>Eliminar</span></a>' +
                                 '</div>' 
                    }
                ]
            };

        

        function loadAccounts(){

            expenseControlServices.accounts.getAccounts().$promise.then((dataReturn) => {
                  $scope.accountsGrid.data = dataReturn.data;
                  console.log('grid',$scope.accountsGrid);
                  },(err) => {
                      console.log('No se ha podido listar las cuentas',err);
                })
        }

        function newAccount(){
                var modalInstance  = $modal.open({
                        template: require('../new-account/new-account.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'newAccountController',
                        controllerAs: 'newAccount',
                })
        }

        function viewAccount(row){
            $scope.accountData = row;
                var modalInstance  = $modal.open({
                        template: require('../view-account/account-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'accountViewController',
                        controllerAs: 'accountView',
                })
        }

        function deleteAccount(row){

            let model = {
                            userId: $scope.userInfo.id,
                            accountId : row.accountId
                        }  

            expenseControlServices.accounts.accountsDelete(model).$promise.then((dataReturn) => {
                  ngNotify.set('Cuenta deshabilitado exitosamente','success');
                  },(err) => {
                      console.log('No se ha podido listar las cuentas',err);
                })
        }

	    function cancel() {
			$modalInstance.dismiss('chao');
		}

    }

  }

  accountsListController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','expenseControlServices','$location','UserInfoConstant','ngNotify','$state','$modal'];
