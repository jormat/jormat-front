
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> expenseControlListController
* # As Controller --> expenseControlList														
*/

export default class expenseControlListController{

        constructor($scope,UserInfoConstant,$timeout,expenseControlServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService,uiGridSelectionConstants){
            var vm = this
            $scope.selectedexpenseControl = []
            vm.searchData=searchData
            vm.expenseControlCreate= expenseControlCreate
            vm.expenseControlView= expenseControlView
            vm.expenseControlUpdate = expenseControlUpdate
            vm.loadExpenseControl = loadExpenseControl
            vm.deleteExpenseControl = deleteExpenseControl
            vm.expenseControlReport = expenseControlReport
            vm.viewAccounts = viewAccounts 
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            //function to show userId

            $scope.UserInfoConstant = UserInfoConstant
            $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                if (details !== undefined) {
                    $scope.userInfo = details.user
                }
            })
            
            //

            loadExpenseControl()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.expenseControlGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'Listado_de_gastos.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'expenseControlId',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.expenseControlList.expenseControlView(row.entity)">{{row.entity.expenseControlId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Folio',
                      field: 'documentId',
                      width: '11%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;"><strong>{{row.entity.documentId}}</strong></p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Documento',
                      field: 'documentType',
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 11px;"><strong>{{row.entity.documentType}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cuenta',
                      field: 'accountName',
                      width: '18%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning" style="font-size: 10px;">{{row.entity.accountName}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Valor',
                      field: 'shareValue',
                      width: '10%',
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-accent" style="font-size: 12px;">$ {{row.entity.shareValue}}</p>' +
                                 '</div>' 
                    },
                    // { 
                    //   name:'Rut',
                    //   field: 'rut',
                    //   width: '12%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="label bg-secundary uppercase" style="font-size: 10px;"><strong>{{row.entity.rut}}</strong></p>' +
                    //              '</div>' 
                    // },
                    { 
                      name:'Proveedor',
                      field: 'providerName',
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;">{{row.entity.providerName}}</p>' +
                                 '</div>'  
                    },
                    
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 11px;">{{row.entity.origin}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    }, 
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '7%',
                      allowCellFocus : false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="badge bg-{{row.entity.styleStatus}}" style="font-size: 11px;">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: '',
                      width: '3%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          function loadExpenseControl(){
            expenseControlServices.expenseControl.getExpenseControl().$promise.then((dataReturn) => {
                  $scope.expenseControlGrid.data = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

          }
         	

        function searchData() {
              expenseControlServices.expenseControl.getExpenseControl().$promise
              .then(function(data){
                  $scope.data = data.data;
                  $scope.expenseControlGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
              });
            }

        function expenseControlCreate(){
                $state.go("app.expenseControlCreate");
        }

        function expenseControlUpdate(row){
          $state.go("app.expenseControlUpdate", { expenseControlId: row.expenseControlId});
        }

        function expenseControlView(row){
            $scope.expenseControlData = row;
            var modalInstance  = $modal.open({
                    template: require('../view/expense-control-view.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'expenseControlViewController',
                    controllerAs: 'expenseControlView'
            })
        }

        
        function expenseControlReport(){
            console.log('etra')
            var modalInstance  = $modal.open({
                    template: require('../historical/historical-expense-control.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'historicalExpenseControlController',
                    controllerAs: 'historicalExpenseControl',
                    size:'lg'
            })
        }

        function viewAccounts(){
            var modalInstance  = $modal.open({
                    template: require('../account-list/accounts-list.html'),
                    animation: true,
                    scope: $scope,
                    controller: 'accountsListController',
                    controllerAs: 'accountsList',
                    size:'lg'
            })
        }

        function deleteExpenseControl(row){

            let model = {
                        userId: $scope.userInfo.id,
                        expenseControlId: row.expenseControlId
                    }

            expenseControlServices.expenseControl.expenseControlDelete(model).$promise.then((dataReturn) => {
                  ngNotify.set('Gasto deshabilitado exitosamente','success');
                  $state.reload('app.expenseControl')
              },(err) => {
                  console.log('No se ha podido eliminar el gasto',err);
              })

          }

            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

expenseControlListController.$inject = ['$scope','UserInfoConstant','$timeout','expenseControlServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService','uiGridSelectionConstants'];

