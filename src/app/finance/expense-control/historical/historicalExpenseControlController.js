/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> historicalExpenseControlController
* # As Controller --> historicalExpenseControl														
*/

  export default class historicalExpenseControlController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,expenseControlServices,$location,UserInfoConstant,ngNotify,$state,uiGridConstants) {

    	var vm = this
    	vm.cancel=cancel
        vm.loadExpenseControl = loadExpenseControl

        // loadExpenseControl()

        $scope.expenseControlGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                showColumnFooter: true,
                exporterCsvFilename: 'Listado_de_gastos.csv',
                exporterPdfHeader: { 
                    text: "Reporte gastos ingresados", 
                    style: 'headerStyle',
                    alignment: 'center'
                },
                    exporterPdfFooter: function ( currentPage, pageCount ) {
                      return { text: "www.importadorajormat.cl ", style: 'footerStyle' };
                    },
                    exporterPdfCustomFormatter: function ( docDefinition ) {
                      docDefinition.styles.headerStyle = { fontSize: 14, bold: true, margin: [20,20,20,20] };
                      docDefinition.styles.footerStyle = { fontSize: 10, bold: true ,alignment: 'center'};
                      return docDefinition;
                    },
                columnDefs: [
                    { 
                      name:'id',
                      field: 'expenseControlId',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.expenseControlList.expenseControlView(row.entity)">{{row.entity.expenseControlId}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name:'Folio',
                      field: 'documentId',
                      width: '11%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;"><strong>{{row.entity.documentId}}</strong></p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Doc.',
                      field: 'documentType',
                      width: '8%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 11px;"><strong>{{row.entity.documentType}}</strong></p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Cuenta',
                      field: 'accountName',
                      width: '18%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-warning" style="font-size: 10px;">{{row.entity.accountName}}</p>' +
                                 '</div>' 
                    },
                    { 
                      name:'Valor',
                      field: 'shareValue',
                      width: '12%',
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-accent" style="font-size: 12px;">$ {{row.entity.shareValue}}</p>' +
                                 '</div>' 
                    },
                    // { 
                    //   name:'Rut',
                    //   field: 'rut',
                    //   width: '12%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class="label bg-secundary uppercase" style="font-size: 10px;"><strong>{{row.entity.rut}}</strong></p>' +
                    //              '</div>' 
                    // },
                    { 
                      name:'Proveedor',
                      field: 'providerName',
                      width: '20%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 11px;">{{row.entity.providerName}}</p>' +
                                 '</div>'  
                    },
                    
                    { 
                      name: 'Origen', 
                      field: 'origin', 
                      width: '9%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase label bg-secundary" style="font-size: 11px;">{{row.entity.origin}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: 'Fecha', 
                      field: 'date',
                      width: '7%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.date | date:\'dd/MM/yyyy\'}}</p>' +
                                 '</div>'
                    }, 
                    { 
                      name: 'Estado', 
                      field: 'statusName', 
                      width: '7%',
                      allowCellFocus : false,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="badge bg-{{row.entity.styleStatus}}" style="font-size: 11px;">{{row.entity.statusName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: '',
                      width: '3%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          // function loadExpenseControl(){
          //   expenseControlServices.expenseControl.getExpenseControl().$promise.then((dataReturn) => {
          //         $scope.expenseControlGrid.data = dataReturn.data;
          //     },(err) => {
          //         console.log('No se ha podido conectar con el servicio',err);
          //     })

          // }


        function loadExpenseControl() {

            let model = {
                startDate: moment($scope.startDate).format("YYYY-MM-DD").toString(),
                endDate: moment($scope.endDate).format("YYYY-MM-DD").toString()
            }

            expenseControlServices.expenseControl.getHistoricExpenseControl(model).$promise.then((dataReturn) => {
              $scope.expenseControlGrid.data = dataReturn.data

              console.log('modelo',model)

              if ($scope.expenseControlGrid.data.length == 0){
                      $scope.messageInvoices = true
                      ngNotify.set( 'No existes gastos ingresados entre '+ model.startDate +' y '+ model.endDate , {
                      sticky: true,
                      button: true,
                      type : 'warn'
                  })
                }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }


    	function cancel() {
			$modalInstance.dismiss('chao');
		}


    }

  }

  historicalExpenseControlController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','expenseControlServices','$location','UserInfoConstant','ngNotify','$state','uiGridConstants'];
