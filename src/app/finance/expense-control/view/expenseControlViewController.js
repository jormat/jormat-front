/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> expenseControlViewController
* # As Controller --> expenseControlView														
*/


  export default class expenseControlViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,expenseControlServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
        vm.expenseControlUpdateStatus = expenseControlUpdateStatus
        vm.expenseControlArchived= expenseControlArchived
	    $scope.expenseControlId = $scope.expenseControlData.expenseControlId

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        loadExpenseControl()

        function loadExpenseControl() {
            let params = {
            expenseControlId: $scope.expenseControlId
          }

        expenseControlServices.expenseControl.getExpenseControlDetails(params).$promise.then((dataReturn) => {

                  $scope.expenseControl = dataReturn.data;
                  console.log('$scope.expenseControl',$scope.expenseControl );
                
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

	    function cancel() {
			$modalInstance.dismiss('chao');
		}

        function expenseControlUpdateStatus(value) {

            let params = {
             expenseControlId: $scope.expenseControlId, 
             userId : $scope.userInfo.id,
             value: value
            }

            ///// funcion para poner total 0 en documentos rechazados
            if (params.value == 3) {
                expenseControlServices.expenseControl.expenseControlRejected(params).$promise.then((dataReturn) => {
                    console.log('Valor documento gasto en 0');
                    },(err) => {
                        console.log('valores no seteados',err);
                    })
                }

            expenseControlServices.expenseControl.expenseControlUpdateStatus(params).$promise.then((dataReturn) => {
                  ngNotify.set('Se han actualizado correctamente el gasto','success')
                  $state.reload('app.expenseControl')
                  $modalInstance.dismiss('chao');
                
              },(err) => {
                  console.log('No se ha podido actualizar gasto',err);
              })
        }

        function expenseControlArchived() {

            let params = {
             expenseControlId: $scope.expenseControlId, 
             userId : $scope.userInfo.id
            }

            expenseControlServices.expenseControl.expenseControlArchived(params).$promise.then((dataReturn) => {
                  ngNotify.set('Se han archivado correctamente el gasto','success')
                  $state.reload('app.expenseControl')
                  $modalInstance.dismiss('chao');
                
              },(err) => {
                  console.log('No se ha podido archivar el gasto',err);
              })
        }

    }

  }

  expenseControlViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','expenseControlServices','$location','UserInfoConstant','ngNotify','$state'];
