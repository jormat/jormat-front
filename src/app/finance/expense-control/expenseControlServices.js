/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> expenseControlServices
* file for call at services to expense Control
*/

class expenseControlServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var expenseControl = $resource(SERVICE_URL_CONSTANT.jormat + '/finance/expenseControl/:route',{},{

      getExpenseControl: {
        method:'GET',
        params : {
          route: 'list'
          }
        },

      getExpenseControlDetails: {
        method:'GET',
        params : {
          route: ''
          }
        },

      expenseControlCreate: {
        method:'POST',
        params : {
          route: ''
          }
        },

      expenseControlUpdateStatus: {
        method:'PUT',
        params : {
          route: 'status'
          }
        },

      expenseControlArchived: {
        method:'PUT',
        params : {
          route: 'archived'
          }
        },

        expenseControlRejected: {
          method:'PUT',
          params : {
            route: 'rejected'
            }
          },

      expenseControlUpdate: {
        method:'PUT',
        params : {
          route: ''
          }
        },

      expenseControlDelete: {
        method:'PUT',
        params : {
          route: 'disabled'
          }
        },

      getHistoricExpenseControl: {
        method:'GET',
        params : {
          route: 'report'
          }
        },
    })

    var accounts = $resource(SERVICE_URL_CONSTANT.jormat + '/finance/accounts/:route',{},{

      getAccounts: {
        method:'GET',
        params : {
          route: 'list'
          }
        },

      getAccountsDetails: {
        method:'GET',
        params : {
          route: ''
          }
        },

      accountsCreate: {
        method:'POST',
        params : {
          route: ''
          }
        },

      accountsUpdate: {
        method:'PUT',
        params : {
          route: ''
          }
        },

      accountsDelete: {
        method:'PUT',
        params : {
          route: 'disabled'
          }
        }
    })


    return { 

            expenseControl : expenseControl,
            accounts : accounts

           }
  }
}

  expenseControlServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.expenseControlServices', [])
  .service('expenseControlServices', expenseControlServices)
  .name;