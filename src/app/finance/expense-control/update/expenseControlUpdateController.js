/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> expenseControlUpdateController
* # As Controller --> expenseControlUpdate														
*/

  export default class expenseControlUpdateController {

    constructor($scope, $filter,$rootScope, $http,expenseControlServices,$location,UserInfoConstant,ngNotify,$state,clientsServices,warehousesServices,$stateParams) {

    	var vm = this
	    vm.cancel = cancel
        $scope.rutValidate = true
        vm.expenseControlUpdate = expenseControlUpdate
        vm.setOriginId = setOriginId
        vm.setAccount = setAccount
        $scope.expenseControlId = $stateParams.expenseControlId
        vm.loadExpenseControl = loadExpenseControl

        //function to show userId
                $scope.UserInfoConstant = UserInfoConstant
                $scope.$watch('UserInfoConstant[0].details[0]', (details) => {
                    if (details !== undefined) {
                        $scope.userInfo = details.user
                    }
                })
            //

        loadExpenseControl()

        function loadExpenseControl() {
            let params = {
            expenseControlId: $scope.expenseControlId
          }

        expenseControlServices.expenseControl.getExpenseControlDetails(params).$promise.then((dataReturn) => {

                  $scope.expenseControl = dataReturn.data;
                  $scope.expenseControlDetails  = { 
                        userId: $scope.userInfo.id,
                        folio :  $scope.expenseControl[0].documentId,
                        documentType : $scope.expenseControl[0].documentType,
                        responsable : $scope.expenseControl[0].responsableName,
                        accountName : $scope.expenseControl[0].accountName,
                        date : $scope.expenseControl[0].date,
                        origin : $scope.expenseControl[0].origin,
                        shareValue : $scope.expenseControl[0].shareValue,
                        providerName : $scope.expenseControl[0].providerName,
                        rut : $scope.expenseControl[0].rut,
                        details : $scope.expenseControl[0].details
                      }
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })
        }

        expenseControlServices.accounts.getAccounts().$promise.then((dataReturn) => {
              $scope.accounts = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
              $scope.warehousesTo = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

	    function cancel() {
			$state.go('app.expenseControl')
		}

        function setOriginId(originId){
            $scope.originId = originId
        }

        function setAccount(accountId){
            $scope.accountId = accountId
        }

        function expenseControlUpdate() {
            console.log('save expenseControlUpdate documentType',$scope.expenseControlDetails.documentType,$scope.expenseControl[0].documentType);



            if ($scope.expenseControl[0].documentType == 'FACT') {

                $scope.netValue = Math.round($scope.expenseControlDetails.shareValue/1.19)

            }else{

                $scope.netValue = $scope.expenseControlDetails.shareValue

            }

            let model = {   
                            expenseControlId: $scope.expenseControlId,
                            userId: $scope.userInfo.id,
                            folio : $scope.expenseControlDetails.folio,
                            responsable : $scope.expenseControlDetails.responsable,
                            shareValue : $scope.expenseControlDetails.shareValue,
                            netValue : $scope.netValue,
                            providerName : $scope.expenseControlDetails.providerName,
                            details : $scope.expenseControlDetails.details
                        }      

                        console.log('modelo',model)   

                        expenseControlServices.expenseControl.expenseControlUpdate(model).$promise.then((dataReturn) => {
                            ngNotify.set('Se ha actualizado correctamente el gasto','success')

                            $state.go('app.expenseControl')

                          },(err) => {
                              ngNotify.set('Error al actualizar gasto','error')
                          })

        }

    }

  }

  expenseControlUpdateController.$inject = ['$scope', '$filter','$rootScope', '$http','expenseControlServices','$location','UserInfoConstant','ngNotify','$state','clientsServices','warehousesServices','$stateParams'];
