

import routing from './finance.route';
import run from './finance.run';

//banks files
import banksListController from './banks/list/banksListController'; 
import banksViewController from './banks/view/banksViewController';
import banksCreateController from './banks/create/banksCreateController';
import banksServices from './banks/banksServices';

//expense Control
import expenseControlListController from './expense-control/list/expenseControlListController'; 
import expenseControlViewController from './expense-control/view/expenseControlViewController'; 
import expenseControlCreateController from './expense-control/create/expenseControlCreateController';
import expenseControlUpdateController from './expense-control/update/expenseControlUpdateController';
import historicalExpenseControlController from './expense-control/historical/historicalExpenseControlController';
import expenseControlServices from './expense-control/expenseControlServices';

//voucher 
import paymentVoucherListController from './payment-voucher/list/paymentVoucherListController';
import paymentVoucherViewController from './payment-voucher/view/paymentVoucherViewController';
import paymentVoucherPrintController from './payment-voucher/print/paymentVoucherPrintController';
import paymentVoucherCreateController from './payment-voucher/create/paymentVoucherCreateController';    
import paymentVoucherServices from './payment-voucher/paymentVoucherServices';

//accounts
import accountsListController from './expense-control/account-list/accountsListController';
import accountViewController from './expense-control/view-account/accountViewController';
import newAccountController from './expense-control/new-account/newAccountController';


export default angular.module('app.finance', [banksServices,expenseControlServices,paymentVoucherServices])
  .config(routing)
  .controller('banksListController', banksListController)
  .controller('banksViewController', banksViewController)
  .controller('banksCreateController', banksCreateController)
  .controller('expenseControlListController', expenseControlListController)
  .controller('expenseControlViewController', expenseControlViewController)
  .controller('expenseControlCreateController', expenseControlCreateController)
  .controller('expenseControlUpdateController', expenseControlUpdateController)
  .controller('historicalExpenseControlController', historicalExpenseControlController)
  .controller('paymentVoucherListController', paymentVoucherListController)
  .controller('paymentVoucherViewController', paymentVoucherViewController)
  .controller('paymentVoucherPrintController', paymentVoucherPrintController)
  .controller('paymentVoucherCreateController', paymentVoucherCreateController)
  .controller('accountsListController', accountsListController)
  .controller('accountViewController', accountViewController)
  .controller('newAccountController', newAccountController)
  .run(run)
  .constant("finance", [{
        "title": "Finanzas",
        "icon":"mdi-cash-usd",
        "subMenu":[
            {
            'title' : 'Pagos',
            "superMenu":[
            
                    {
                    'title' : 'Pagos Facturas',
                    'url': 'app.payments'
                    },
                    {
                    'title' : 'Comprobantes de pago',
                    'url': 'app.officePayments'
                    },
                    {
                    'title' : 'Listado de Pagos',
                    'url': 'app.voucher'
                    },
                    {
                    'title' : 'Cheques',
                    'url': 'app.checks'
                    }      
                ]
            },
            {
            'title' : 'Control de gastos',
            'url': 'app.expenseControl'
            },
            {
            'title' : 'Reportes',
            "superMenu":[
            
                    {
                    'title' : 'Estado de resultado',
                    'url': 'app.incomeStatement'
                    },
                    {
                    'title' : 'Ventas Power BI',
                    'url': 'app.salesPowerBI'
                    }    
                ]
            },

            {
            'title' : 'Entidades',
            "superMenu":[
            
                    {
                    'title' : 'Clientes',
                    'url': 'app.clients'
                    },
                    {
                    'title' : 'Bancos',
                    'url': 'app.banks',
                    }     
                ]
            }
            
        ]
    }])
  .name;