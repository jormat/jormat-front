class modulePermissionSrv {

    constructor($resource,SERVICE_URL_CONSTANT) {

       
          var services = $resource(SERVICE_URL_CONSTANT.adminPanel +'/user-api/:route',{},{
          // var services = $resource(SERVICE_URL_CONSTANT.localJson +'/admin-panel/:routeJson',{},{
            
            getAllModules:{
              method:'GET',
              params : {
                route: 'getModuleByProduct'
                // routeJson: 'getGroups.json'
              }
            },
            insertModuleByProductName:{
              method:'POST',
              params : {
                route: 'insertModuleByProductName'
                // routeJson: 'getGroups.json'
              }
            },
             insertFeatureByName:{
              method:'POST',
              params : {
                route: 'insertFeatureByName'
                // routeJson: 'getGroups.json'
              }
            },
             getpermissions:{
              method:'GET',
              params : {
                route: 'getAllfeaturePermissions'
                // routeJson: 'getGroups.json'
              }
            },
             deletefeaturePermissions:{
              method:'POST',
              params : {
                route: 'deletefeaturePermissions'
                // routeJson: 'getGroups.json'
              }
            }



          });

          return {
            services : services
          };
        }

    }

  modulePermissionSrv.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.modulePermissionSrv', [])
  .service('modulePermissionSrv', modulePermissionSrv)
  .name;
