/**
* @name APP cmmApp
* @autor camila.montenegro@u-planner.com
* @description
* # name controller --> rolesListController
* # As Controller --> rolesList
* brief comment controller shows the view for the main page of assignTimeRange
*/

export default class rolesListController {

  constructor($scope,rolesServices,$resource,$mdDialog,$timeout,ngNotify,UserInfoConstant,$state,$modal,$location,$filter,$rootScope) {

	var vm = this;
	this.$scope = $scope;
	this.ngNotify = ngNotify;
	this.$state = $state;
	vm.services   = rolesServices.services;
	vm.modalDelete = modalDelete;
	vm.editRolesSave = editRolesSave;
	this.buttonActions = true
	this.$scope.grid = false;
	this.$scope.edit = true;
	this.$scope.titleList = true;
	this.$translate = $filter('translate')
	

	$scope.UserInfoConstant = UserInfoConstant;
	$scope.$watch('UserInfoConstant[0].details[0]', (details, before) => {
		if (details !== undefined) {

		   vm.user=details.user.id;
		   vm.USER_ID=details.user.id;
		   vm.init()

	   }
   });


	this.detailsRoles ={

					enableFiltering: true,
					enableColumnResizing: true,
					// data:$scope.gridData

					columnDefs: [

					// {
					// 	field: 'productName',
					// 	displayName: 'Producto',
					// },
					// {
					// 	field: 'mname',
					// 	displayName: 'Módulo',
					// },
					// {
					// 	field: 'fname',
					// 	displayName: 'Características',
					// 	width: '30%'
					// },
					// {
					// 	field: 'ds_permissions',
					// 	displayName: 'Permisos',
					// },
					// {
					// 	field: 'hasPermission',
					// 	displayName: 'Estado',
					// 	enableFiltering: false,
					// 	enableSorting: false,
					// 	width: '7%',
					// 	headerCellTemplate: '<div class="ui-grid-cell-contents text-center"><span class="text-center p-v"><label class="md-check"> \
					// 	<input type="checkbox" ng-model="checker.checked" parse-int ng-true-value="true" ng-false-value="false" ng-click="grid.appScope.rolesList.selectAllItems(checker.checked,grid)" ng-disabled="(grid.appScope.edit == true)"> \
					// 	<i class="green"></i></label></span></div>',
					// 	cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
					// 	<input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="true" ng-false-value="false" ng-disabled="(grid.appScope.edit == true)"> \
					// 	<i class="green"></i></label></span></div>'
					// }
					]
			};

			this.uiGridTrans();
			$rootScope.$on('$translateChangeSuccess', (event, data) => {
				this.uiGridTrans();
			})
			
			function modalDelete(){

				$scope.userinfo = UserInfoConstant[0].details;
				$scope.roleid = this.roleid;
				$scope.rolename= this.rolename;
				var modalInstance  = $modal.open({
					template: require('../modal-delete/modalDeleteRoles.html'),
					animation: true,
					scope: $scope,
					controller: 'modalDeleteRolesController',
					controllerAs: 'modalDeleteRoles'

				});
				
			}

			function editRolesSave(){

				$scope.isDisabled = true;

				var arrayRoles=[];
				angular.forEach(this.detailsRoles.data, function(value, key){
					if (value.hasPermission == true){
						arrayRoles.push({fid:value.fid,mid:value.mid,permission:value.ds_permissions}) 
					}
				});

				var rolesList=arrayRoles;
				let params  = { 
					id   : this.roleid,
					roles   : rolesList 
					
				}

				vm.saveEditRoles(params)
			}

   }//Fin constructor

   //functions




   uiGridTrans(){

	this.detailsRoles.columnDefs = [
		// {field: 'productName',      displayName: this.$translate('PRODUCT'),},
		{field: 'mname',            displayName: this.$translate('Módulo'),enableFiltering: true},
		{field: 'fname',            displayName: this.$translate('Características'), },
		{field: 'ds_permissions',   displayName: this.$translate('Permisos'), },
		{
			field: 'hasPermission',
			displayName: '',
			enableFiltering: false,
			enableSorting: false,
			width: '7%',
			headerCellTemplate:
			'<div class="ui-grid-cell-contents text-center">\
				<label class="md-check">\
					<input type="checkbox" ng-model="checker.checked" parse-int ng-true-value="true" ng-false-value="false" ng-click="grid.appScope.rolesList.selectAllItems(checker.checked,grid)" ng-disabled="(grid.appScope.edit == true)">\
					<i class="bg-accent"> </i>\
				</label>\
			</div>',
			cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
			<input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="true" ng-false-value="false" ng-disabled="(grid.appScope.edit == true)"> \
			<i class="green"></i></label></span></div>'

		}
	]
}

loadRoles(item){

	this.params = {

		id :item.roleid
	}

	this.roleid=item.roleid;
	this.rolename=item.rolename;
	this.getDetailsRolesFunction(this.params);
}


init(){



	this.services.getRoles({USER_ID:this.USER_ID}).$promise.then((dataReturn) => {
		if(dataReturn.data.lenght == 0){
		 this.ngNotify.set('no existen registros','warn');

	 }else{

		this.roles = []
		this.roleList = dataReturn.data;
		for (let i in this.roleList){
			this.roles.push({
				title: this.roleList[i].rolename,
				id_code: this.roleList[i].roleid,
				cod: "Código: " + this.roleList[i].roleid,
				badges: [
				{
					label: ( (this.roleList[i].is_active == 'Active') ? this.$translate('ACTIVE') : this.$translate('INACTIVE')),
					bg: ( (this.roleList[i].is_active == 'Active') ? "bg-success" : "bg-dark" ),
					// icon: "mdi-check"
				}
				]
			})
		}
	}
}, (err) => {
 this.ngNotify.set('No se ha podido conectar con el servicio','error');
})

}

onRoleClick(item){
	console.log('tttt',item)
	this.roleid=item.id_code;
	this.rolename=item.title;
	this.getDetailsRolesFunction({id:this.roleid});
	this.panelConfig = true
	this.buttonActions = false
}

selectAllItems(value,grid){
	let filters=[];
	let len=grid.columns.length;
	let count=false
	for (let i=0;i<len;i++){
		//
		 if(typeof grid.columns[i].filters[0].term!='undefined' && grid.columns[i].filters[0].term!=null){
		 	filters.push({"reg":new RegExp(grid.columns[i].filters[0].term,'i'),"id":i+1});
		 	count=true
		 }
		  
		else
		  filters.push({"reg":'',"id":0});
	}
	this.detailsRoles.data.forEach(function(item){
		let match=0;

			   for(var i in filters){

			   	  if(filters[i].id!=0 && count==true){

					if (item.productName.toString().match(filters[i].reg) && filters[i].id==1){
					  match++;
					  item.hasPermission=value;
					  // return
					}
					if (item.mname.toString().match(filters[i].reg) && filters[i].id==2){
					  match++;
					  item.hasPermission=value;
					   // return
					}
					if (item.fname.toString().match(filters[i].reg)  && filters[i].id==3){
					  match++;
					  item.hasPermission=value;
					   // return
					}
					if(item.ds_permissions.toString().match(filters[i].reg)  && filters[i].id==4){
					  match++;
					  item.hasPermission=value;
					   // return
					}
			   	  }else{
                        // item.hasPermission=value;
                      }

			   	  if (count==false){item.hasPermission=value;}



			   }

			});
}

getDetailsRolesFunction(params){
	console.log('parametros para tabla service roles',params)

	this.services.getDetailsRoles(params).$promise.then((dataReturn) => {
		if(dataReturn.data.lenght == 0){
			this.ngNotify.set('registro seleccionado no tiene registros','warn');

		}else{

			this.detailsRoles.data = dataReturn.data;
			this.$scope.grid=true
			console.log('details Roles',dataReturn.data);
		}
	}, (err) => {
		this.ngNotify.set('No se ha podido conectar con el servicio','error');
	})

}

editRoles(){

	this.$scope.editSave = true;
	this.$scope.edit = false;
	this.$scope.titleList = false;
	this.$scope.titleEdit = true;
	this.$scope.isDisabled = false;
	
}

cancelEditRoles(){

	this.$scope.editSave = false;
	this.$scope.edit = true;
	this.$scope.titleList = true;
	this.$scope.titleEdit = false;
	this.$scope.checker.checked=false

	this.getDetailsRolesFunction({id : this.roleid});
	
}

saveEditRoles(params){

	console.log('esto es lo que se envia',params);

	let updateRoles = {
	 roleid:params.id ,
	 values: params.roles
 }

 this.services.insertRolePermission(updateRoles).$promise.then((dataReturn) => {
	if(dataReturn.status == 0){
		this.ngNotify.set('Se ha modificado de manera correcta el rol','success');
						// this.$state.go(this.$state.current, {}, {reload: true});
						this.$scope.edit = true;
						this.$scope.editSave = false;
						this.$scope.titleList = true;
						this.$scope.titleEdit = false;

					}else{
						console.log('Error Servicio update check params enviados',dataRoles);
						this.$scope.isDisabled = false;
					}
				}, (err) => {
					this.ngNotify.set('No se ha podido conectar con el servicio','error');
					this.$scope.isDisabled = false;
				})

}

toggleList(isHide) {
	this.$scope.toggle = isHide
}



}// fin function principal
rolesListController.$inject=['$scope', 'rolesServices','$resource', '$mdDialog','$timeout','ngNotify','UserInfoConstant','$state','$modal','$location','$filter','$rootScope']





