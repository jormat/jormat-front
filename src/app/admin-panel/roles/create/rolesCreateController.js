/**
* @name APP cmmApp
* @autor camila.montenegro@u-planner.com
* @description
* # name controller --> rolesCreateController
* # As Controller --> rolesCreate
* brief comment controller creation roles users
*/

export default class rolesCreateController {

  constructor($scope,rolesServices,$resource,$mdDialog,$timeout,ngNotify,UserInfoConstant,$state,$modal,$location,$filter, $rootScope) {

    var vm = this
    this.$scope = $scope
    this.ngNotify = ngNotify
    this.$state = $state 
    vm.services   = rolesServices.services
    vm.save = save
    $scope.name = ''
    this.$scope.grid = false
    this.$translate = $filter('translate');

            //function devuelve id user

            $timeout(function(){
                $scope.userinfo = UserInfoConstant[0].details;
                console.log('id usuario', $scope.userinfo[0]);
                vm.user=$scope.userinfo[0].user.id;
                vm.USER_ID=$scope.userinfo[0].user.id;
            },500);


            this.detailsRoles ={

                enableFiltering: true,
            }
            this.uiGridTrans();

            $rootScope.$on('$translateChangeSuccess', (event, data) => {
                this.uiGridTrans();
            })



            vm.init()

            function save(){

                $scope.isDisabled = true;

                var arrayRoles=[];
                angular.forEach(this.detailsRoles.data, function(value, key){
                    if (value.hasPermission == true){
                        arrayRoles.push({fid:value.fid,mid:value.mid,permission:value.ds_permissions}) 
                    }
                });

                var rolesList=arrayRoles;
                let params  = { 
                    name   : $scope.name,
                    user_id : vm.USER_ID,
                    roles   : rolesList 
                    
                }

                vm.saveRoles(params)
            }

   }//Fin constructor

   //functions

   uiGridTrans(){
    this.detailsRoles.columnDefs = [
    // {field: 'productName',      displayName: this.$translate('PRODUCT'),},
    {field: 'mname',            displayName: this.$translate('Módulo'), },
    {field: 'fname',            displayName: this.$translate('Características'), },
    {field: 'ds_permissions',   displayName: this.$translate('Permisos'), },
    {
        field: 'hasPermission',
        displayName: 'STATUS',
        enableFiltering: false,
        enableSorting: false,
        width: '10%',
        headerCellTemplate:
        '<div class="ui-grid-cell-contents text-center">\
            <label class="md-check">\
                <input type="checkbox" ng-model="checker.checked" ng-click="grid.appScope.rolesCreate.selectAllItems(checker.checked,grid)">\
                <i class="bg-accent"> </i>\
            </label>\
        </div>',
        cellTemplate:'<div class="ui-grid-cell-contents text-center"><span class="text-center"><label class="md-check"> \
        <input type="checkbox" ng-model="row.entity.hasPermission" parse-int ng-true-value="true" ng-false-value="false" > \
        <i class="green"></i></label></span></div>'

    }
    ]
}

init(){

  this.getDetailsRolesFunction();

}

selectAllItems(value,grid){
  let filters=[];
  let len=grid.columns.length;
  let count=false
  for (let i=0;i<len;i++){
     if(typeof grid.columns[i].filters[0].term!='undefined' && grid.columns[i].filters[0].term!=null){
      filters.push({"reg":new RegExp(grid.columns[i].filters[0].term,'i'),"id":i+1});
      count=true
     }
     else
      filters.push({"reg":'',"id":0});
  }
  
  this.detailsRoles.data.forEach(function(item){
         for(var i in filters){

            if(filters[i].id!=0 && count==true){
          if (item.productName.toString().match(filters[i].reg) && filters[i].id==1){
             item.hasPermission=value;
          }
          if (item.mname.toString().match(filters[i].reg) && filters[i].id==2){
             item.hasPermission=value;
          }
          if (item.fname.toString().match(filters[i].reg)  && filters[i].id==3){
             item.hasPermission=value;
          }
          if(item.ds_permissions.toString().match(filters[i].reg)  && filters[i].id==4){
             item.hasPermission=value;
          }
            }
            if (count==false){item.hasPermission=value;}
         }
      });
}


// selectAllItems(value,grid){
//     let filters=[];
//     let len=grid.columns.length;
//     for (let i=0;i<len;i++){
//         filters.push(new RegExp(grid.columns[i].filters[0].term, "gi"));
//     }
//     this.detailsRoles.data.forEach(function(item){
//         let match=0;
//         if (item.mname.toString().match(filters[0])){
//             match++;
//         }
//         if (item.fname.toString().match(filters[1])){
//             match++;}
//             if (item.ds_permissions.toString().match(filters[2])){
//                 match++;}
//                 if (match==3){item.hasPermission=value;}
//             });
// }

getDetailsRolesFunction(){

    this.services.getDetailsRoles().$promise.then((dataReturn) => {
        if(dataReturn.data.lenght == 0){
            this.ngNotify.set('registro seleccionado no tiene registros','warn');

        }else{

            this.detailsRoles.data = dataReturn.data;
            this.$scope.grid=true
            console.log('details Roles',dataReturn.data);
        }
    }, (err) => {
        this.ngNotify.set('No se ha podido conectar con el servicio','error');
    })

}

saveRoles(params){
    console.log('data envia servicio save',params);

    if(params.roles.length > 0){
        this.services.saveRoles(params).$promise.then((dataReturn) => {
            if(dataReturn.status == 0){


                this.idRole = dataReturn.data;

                let dataRoles = {
                 roleid:this.idRole ,
                 values: params.roles
             }

             this.services.insertRolePermission(dataRoles).$promise.then((dataReturn) => {
                console.log('se han asociado los permisos correctamente a al rol creado');
            })

             this.ngNotify.set('Se ha creado el Rol correctamente','success');
             this.$state.go("app.roles");

         }else{
            this.ngNotify.set('Error con servicio check request','error');
            this.$scope.isDisabled = false;

        }
    }, (err) => {
        this.ngNotify.set('No se ha podido conectar con el servicio','error');
        this.$scope.isDisabled = false;
    })
    }else{
        this.ngNotify.set('Error , Seleccione permisos para nuevo Rol','error');
        this.$scope.isDisabled = false;

    }

}

}// fin function principal
rolesCreateController.$inject=['$scope', 'rolesServices','$resource', '$mdDialog','$timeout','ngNotify','UserInfoConstant','$state','$modal','$location','$filter', '$rootScope']





