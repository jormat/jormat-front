
/**
* @name APP cmmApp
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> modalDeleteRolesController
* # As Controller --> modalDeleteRoles
* controllador de modal para eliminar rol 
*/


  export default class modalDeleteRolesController {

  constructor($scope,$resource,$timeout,ngNotify,UserInfoConstant,rolesServices,$state,$modalInstance){

    
      var vm = this;
      vm.deleteRoles = deleteRoles;
      this.$state = $state;
      vm.cancel = cancel;
      this.$modalInstance = $modalInstance;
      this.ngNotify = ngNotify ;
      vm.services   = rolesServices.services;
      vm.id = $scope.roleid;
      vm.name = $scope.rolename;
      vm.user=$scope.userinfo[0].user.id;
     
    // /////////////////////////////

    function deleteRoles() {

          var params = {

            status:0,
            id_code : vm.id

          }

        vm.deleteRolesFunction(params)
         
        } 


    function cancel() {

          $modalInstance.dismiss('chao');
        }

    }//fin constructor

  
      deleteRolesFunction(params){
                console.log('data envia servicio save',params);

                this.services.deleteRoles(params).$promise.then((dataReturn) => {
                    if(dataReturn.status == 1){
                        this.ngNotify.set('El Rol ha sido desactivado de manera correcta','success');
                        this.$modalInstance.dismiss('chao');
                        this.$state.go(this.$state.current, {}, {reload: true});

                    }else{
                        this.ngNotify.set('Error ,problemas con el servicio no se ha podidio eliminar el Rol','error');
                    
                    }
                }, (err) => {
                    this.ngNotify.set('No se ha podido conectar con el servicio','error');
                })

            }

  }

  modalDeleteRolesController.$inject = ['$scope','$resource','$timeout','ngNotify','UserInfoConstant','rolesServices','$state','$modalInstance'];

