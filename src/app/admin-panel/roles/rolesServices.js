class rolesServices {

    constructor($resource,SERVICE_URL_CONSTANT) {

       
          var services = $resource(SERVICE_URL_CONSTANT.adminPanel +'/users/:route',{},{
          // var services = $resource(SERVICE_URL_CONSTANT.localJson +'/admin-panel/:routeJson',{},{
            
            getRoles:{
              method:'GET',
              params : {
                route: 'getRole'
                // routeJson: 'getRole.json'
              }
            },

            getDetailsRoles:{
              method:'GET',
              params : {
                route: 'getAllPermissionsByrole'
                // routeJson: 'getAllPermissionsByrole.json'
              }
            },

            getDetailsRolesActive:{
              method:'GET',
              params : {
                route: 'getAllPermissionsByroleActive'
                // routeJson: 'getAllPermissionsByroleActive.json'
              }
            },

            saveRoles:{
              method:'POST',
              params : {
                route: 'insertRoles'
                // routeJson: 'insertRoles.json'
              }
            },

            deleteRoles:{
              method:'POST',
              params : {
                route: 'deactiveRole'
              }
            },

            insertRolePermission:{
              method:'POST',
              params : {
                route: 'insertRolePermission'
              }
            },

            editRole:{
              method:'POST',
              params : {
                route: 'updateRole'
              }
            }

          });

          return {
            services : services
          };
        }

    }

  rolesServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.rolesServices', [])
  .service('rolesServices', rolesServices)
  .name;
