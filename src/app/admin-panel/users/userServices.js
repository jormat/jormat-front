/**
* @name APP adminPanel
* @autor jaime.salazar@u-planner.com
* @description
* # name Services --> userViewController
* services call backend for user in Admin Panel
*/

class userServices {

    constructor($resource,SERVICE_URL_CONSTANT) {

          var services = $resource(SERVICE_URL_CONSTANT.adminPanel +'/users/:route',{},{
            // var services = $resource(SERVICE_URL_CONSTANT.localJson +'/admin-panel/:routeJson',{},{

            
            getDetailsUser:{
              method:'GET',
              params : {
                route: 'getUserDetails'
                // routeJson: 'getUserDetails.json'
              }
            },

            getDataUsers:{
              method:'GET',
              params : {
                route: 'getUsers'
                // routeJson: 'getUser.json'
              }
            },

            saveUsers:{
              method:'POST',
              params : {
                route: 'insertUserInfo'
                // routeJson: 'insertUserInfo.json'
              }
            },
            getRoles:{
              method:'GET',
              params : {
                route: 'getRole'
                // routeJson: 'getRole.json'
              }
            },

            getGroups:{
              method:'GET',
              params : {
                route: 'getGroups'
                // routeJson: 'getGroups.json'
              }
            },

            getInstitutions:{
              method:'GET',
              params : {
                route: 'getInstitutions'
                // routeJson: 'getInstitutions.json'
              }
            },

            getattributes:{
              method:'GET',
              params : {
                route: 'get-attributes'
                // routeJson: 'get-attributes.json'
              }
            },

            deleteUser:{
              method:'POST',
              params : {
                route: 'deactiveUsers'
              }
            },

            updateUser:{
              method:'POST',
              params : {
                route: 'updateUserInfo'
              }
            },

            validateUser:{
              method:'GET',
              params : {
                route: 'validateUser'
              }
            },
           getUsersByName:{
              method:'GET',
              params : {
                route: 'getUsersByName'
              }
            }



          });

         
          return {
            services : services
          };
        }

    }

  userServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.userServices', [])
  .service('userServices', userServices)
  .name;
