/**
* @name APP adminPanel
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> userListController
* # As Controller --> userList
* brief comment controller the view for the list user active admin-Panel
*/

export default class userListController {

	constructor($scope,$resource,$timeout,ngNotify,UserInfoConstant,userServices,$state,$modal,LOCAL_ENV_CONSTANT,$filter,$rootScope) {


		var vm = this;
		this.ngNotify = ngNotify;
		this.$scope = $scope;
		vm.services   = userServices.services;
		vm.modalDelete  = modalDelete;
		this.$translate = $filter('translate')
		this.onChangeText = (input) => {
			console.log('input->', input)
			let params = {
				txt : input
			}
			this.services.getDataUsers(params).$promise.then((dataReturn) => {
				if(dataReturn.data.lenght == 0){
					this.ngNotify.set('registro seleccionado no tiene registros','warn');
				}else{
					this.users = []
					this.userList = dataReturn.data;
					for (let i in this.userList){
						this.users.push({
							title: this.userList[i].name,
							id_code: this.userList[i].id,
							cod: "Código: " + this.userList[i].id,
							badges: [{
								label: ((this.userList[i].active == 'Active') ? "ACTIVE" : "INACTIVE" ),
								bg: ( (this.userList[i].active == 'Active') ? "bg-success" : "bg-dark" ),
							}]
						})
					}
				}
			}, (err) => {
				this.ngNotify.set('No se ha podido conectar con el servicio','error');
			})
		}
		this.filter = {
			inputs: [{
				label: "SEARCH_TEXT", 
				typefield: "text",
				data: [], 
				onChange: this.onChangeText, 
			}]
		}
		vm.userEdit =userEdit;
			// $scope.notPicture= require("../images/no_user.jpg");
			$scope.pictures= require("../images/no_user.jpg");
			this.$scope.type=LOCAL_ENV_CONSTANT
			this.$scope.USER_API_TYPE=true

			if(LOCAL_ENV_CONSTANT.loginType=='adal'){ //LOCAL_ENV_CONSTANT.loginType=='web' || 
				this.$scope.USER_API_TYPE=false
			}
			
			vm.init()

			vm.selectedItemChange= function(item){
              // some stuff with selectedItem

              // console.log(searchText)
              if(typeof item==='undefined'){
              	vm.init()
              }else{
              	vm.loadUser(item)
              }
              

            // clear input
            // $scope.searchText = '';
            // $scope.selectedItem= undefined;

        };

        vm.searchUsers=function(alumns) {
        	let arr = []

        	if($.trim(alumns)!=""){
        		var charLength=alumns.length;

        		if(charLength>=4){

        			let params = {
        				name:encodeURIComponent (alumns)
        			}

						            // return arr

						            return  userServices.services.getUsersByName(params).$promise.then((dataReturn) => {
						            	if (dataReturn.status === true) {
						            		if(dataReturn.data.length>0){
						            			if(dataReturn.data[0].username!=null){
						            				vm.users = dataReturn.data;

						            				vm.username=vm.users[0].username;
						            				vm.name=vm.users[0].name;
						            				vm.id=vm.users[0].id;
						            				vm.params = {
						            					id : vm.users[0].id
						            				}
						            				vm.getDetailsUserFunction(vm.params);
						            				return dataReturn.data
						            			}
						            		}
						            	} 
						            	return  arr
						            }).catch((error) => {
						            	console.log(error,'',vm.users[0])
						            })

						        }
						        

						    }

						    return arr
						}


						function modalDelete(){
							
							$scope.userinfo = UserInfoConstant[0].details;
							$scope.idUser = this.id;
							$scope.nameUser= this.name;
							var modalInstance  = $modal.open({
								template: require('../modal-delete/modalDeleteUser.html'),
								animation: true,
								scope: $scope,
								controller: 'modalDeleteUserController',
								controllerAs: 'modalDeleteUser'

							});
							
						}
						function userEdit(id){
							$state.go('app.userEdit', {id:id});
						}

			}//fin constructor

			//functions
			
			loadUser(item){

				this.params = {

					id :item.id
				}

				this.name=item.name;
				this.username=item.username;
				this.id=item.id;
				this.getDetailsUserFunction(this.params);
			}

			
			init(params){

				this.services.getDataUsers(params).$promise.then((dataReturn) => {
					if(dataReturn.data.lenght == 0){
						this.ngNotify.set('registro seleccionado no tiene registros','warn');

					}else{

						this.users = []
						this.userList = dataReturn.data;
						for (let i in this.userList){
							this.users.push({
								title: this.userList[i].name,
								id_code: this.userList[i].id,
								cod: "Código: " + this.userList[i].id,
								badges: [{
									label: ((this.userList[i].active == 'Active') ? "ACTIVE" : "INACTIVE" ),
									bg: ( (this.userList[i].active == 'Active') ? "bg-success" : "bg-dark" ),
									// icon: "mdi-check"
								}]
							})
						}
						console.log('this.userList->', this.userList)

					}
				}, (err) => {
					this.ngNotify.set('No se ha podido conectar con el servicio','error');
				})

			}

			onUserClick(item){
				this.id=item.id_code;
				this.name=item.title;
				this.getDetailsUserFunction({id:this.id});
			}
			
			getDetailsUserFunction(params){

				this.services.getDetailsUser(params).$promise.then((dataReturn) => {
					if(dataReturn.data.lenght == 0){
						this.ngNotify.set('registro seleccionado no tiene registros','warn');

					}else{
						
						this.detailsUser = dataReturn.data;
						console.log('details users',dataReturn.data);
					}
				}, (err) => {
					this.ngNotify.set('No se ha podido conectar con el servicio','error');
				})

			}

			toggleList(isHide) {
				this.$scope.toggle = isHide
			}

			

		}

		userListController.$inject=['$scope','$resource','$timeout','ngNotify','UserInfoConstant','userServices','$state','$modal','LOCAL_ENV_CONSTANT','$filter','$rootScope']
