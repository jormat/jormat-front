/**
* @name APP adminPanel
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> userCreateController
* # As Controller --> userCreate
* brief comment controller the view for the list user active admin-Panel
*/

export default class userCreateController {
	constructor($scope,$resource,$timeout,ngNotify,UserInfoConstant,userServices,$state) {

        this.ngNotify = ngNotify;
        this.$state = $state;
        this.$scope = $scope;
        this.services = userServices.services;

        this.user  = { 
            name : '',
            email : '',
            picture : require("../images/no_user.jpg"),
            username : '',
            institution : 0,
            status : true, 
            password : '',
            roles : [],
            groups : [],
            id_attrib : null,
            ds_attrib : null,
            ngNotify : ngNotify,
            services : userServices.services,
            afterSave : true
        }
	   	this.init();
	}//fin constructor

	//functions

	save(){
		this.$scope.isDisabled = true;
        this.user.id_attrib=this.attrib,
        this.user.ds_attrib=this.attrib_info
        console.log('AKI PARAMETROS -> ',this.user);
        
        

		this.saveUser(this.user)
	}

	init(){
		this.getCataloguesRoles();
		this.getCataloguesGroups();
		this.getCataloguesInstitutions();
		this.getAttributes();
	}

	getCataloguesRoles(){
        this.services.getRoles().$promise.then((dataReturn) => {
            if(dataReturn.data.lenght == 0){
                this.ngNotify.set('registro seleccionado no tiene registros','warn');
            }else{  
                this.roles = dataReturn.data;
            }
        }, (err) => {
            this.ngNotify.set('No se ha podido conectar con el servicio','error');
        })
    }

    getCataloguesGroups(){
        this.services.getGroups().$promise.then((dataReturn) => {
            if(dataReturn.data.lenght == 0){
                this.ngNotify.set('registro seleccionado no tiene registros','warn');
            }else{    
                this.groups = dataReturn.data;
            }
        }, (err) => {
            this.ngNotify.set('No se ha podido conectar con el servicio','error');
        })
    }

    getCataloguesInstitutions(){
        this.services.getInstitutions().$promise.then((dataReturn) => {
            if(dataReturn.data.lenght == 0){
                this.ngNotify.set('registro seleccionado no tiene registros','warn');
            }else{     
                this.institutions = dataReturn.data;
            }
        }, (err) => {
            this.ngNotify.set('No se ha podido conectar con el servicio','error');
        })
    }

    getAttributes(){
    	let params = {
            ID_ATTRIB: 2
        }

        this.services.getattributes(params).$promise.then((dataReturn) => {
            if(dataReturn.data.lenght == 0){
                this.ngNotify.set('registro seleccionado no tiene registros','warn');
            }else{  
                this.attrib = dataReturn.data;
                this.attrib[0] = this.attrib[0] || {}
                this.attrib[0].OPTION = this.attrib[0].OPTION || []
                this.attrib_info = this.attrib[0].OPTION[2];
                console.log('Atributos',dataReturn.data);
            }
        }, (err) => {
            this.ngNotify.set('No se ha podido conectar con el servicio','error');
        })
    }
			
	saveUser(params){
		// let dataValidate  = { 
		// 	email   : this.$scope.userCreate.email,
  //           name : params.username
  //   	}

       // this.$scope.userCreate.user.institution=this.$scope.userCreate.user.institutions

       this.$scope.userCreate.user.isDisabled=true
       if(this.attrib=='')
        delete this.$scope.userCreate.user.id_attrib
       else
        this.$scope.userCreate.user.id_attrib=this.attrib

        this.$scope.userCreate.user.ds_attrib=this.attrib_info
        this.$scope.userCreate.user.institution=this.$scope.userCreate.user.institutions

		this.services.validateUser(this.$scope.userCreate.user).$promise.then((dataReturn) => {
			if(dataReturn.status == 1){
				this.ngNotify.set('El usuario ingresado ya existe , intentelo usando otro correo y usuario','error');
			}else{
				this.services.saveUsers(this.$scope.userCreate.user).$promise.then((dataReturn) => {
					if(dataReturn.status == 0){
						this.ngNotify.set('Se ha creado el usuario correctamente','success');
						this.$state.go("app.users");
					}else{
						this.ngNotify.set('Error , con el servicio','error');
						this.$scope.isDisabled = false;
					}
				}, (err) => {
					this.ngNotify.set('No se ha podido conectar con el servicio','error');
					this.$scope.isDisabled = false;
				})
			}
		}, (err) => {
			this.ngNotify.set('No se ha podido conectar con el servicio','error');
			this.$scope.isDisabled = false;
		})
	}

}

userCreateController.$inject=['$scope','$resource','$timeout','ngNotify','UserInfoConstant','userServices','$state']
