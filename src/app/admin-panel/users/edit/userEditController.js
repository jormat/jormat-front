/**
* @name APP adminPanel
* @autor jaime.salazar@u-planner.com
* @description
* # name controller --> userCreateController
* # As Controller --> userCreate
* brief comment controller the view for the list user active admin-Panel
*/

export default class userEditController {
	constructor($scope,$resource,$timeout,ngNotify,UserInfoConstant,userServices,$state,$location,LOCAL_ENV_CONSTANT) {
		this.ngNotify = ngNotify;
		this.$scope = $scope;
		this.$state = $state;
		this.services   = userServices.services;
		this.$scope.name = '';
		this.$scope.email = '';
		this.$scope.username = '';
		this.$scope.status =  true;
		this.$scope.institution = 0;
		this.$scope.password = '';
		this.rolesList=[];
		this.groupsList=[];
  	    this.id=$location.search().id;
  	    this.oldname='';
  	    this.$scope.picture= require("../images/no_user.jpg");
  	    this.$scope.type=LOCAL_ENV_CONSTANT

   		
		this.init();
	}//fin constructor

	//functions

	save(){
		this.$scope.isDisabled = true;


		let params  = { 
			username : this.username,
			name   : this.name ,
			email   : this.email ,
			picture : this.file,
	        username : this.username,
	        status : this.status, 
	        password : this.password,
	        roles : this.rolesList,
	        oldname:this.oldname,
	        user_id:this.id,
	    }
	    this.saveUser(params)
	}


	init(){
		this.getCataloguesRoles();
		this.getDetailsUserFunction({id:this.id});		
	}

	getDetailsUserFunction(params){
		this.services.getDetailsUser(params).$promise.then((dataReturn) => {
			if(dataReturn.data.lenght == 0){
				this.ngNotify.set('registro seleccionado no tiene registros','warn');
			}else{
				this.detailsUser = dataReturn.data;
	            this.name = this.detailsUser.fullname;
	            this.username = this.detailsUser.username;
	            this.password = this.detailsUser.prim_keys;
	            this.password2 = this.detailsUser.prim_keys;
				this.email = this.detailsUser.mail;
				this.oldname = this.detailsUser.username;
				// this.file,
				if(this.detailsUser.is_active=='Active')
				   this.status=true;
				else
				   this.status=false;


               	this.rolesList=this.detailsUser.roles;			
			}
		}, (err) => {
			this.ngNotify.set('No se ha podido conectar con el servicio','error');
		})
	}

	getCataloguesRoles(){
        this.services.getRoles().$promise.then((dataReturn) => {
            if(dataReturn.data.lenght == 0){
                this.ngNotify.set('registro seleccionado no tiene registros','warn');
            }else{
                this.roles = dataReturn.data;                
            }
        }, (err) => {
            this.ngNotify.set('No se ha podido conectar con el servicio','error');
        })
    }



			
	saveUser(params){
		this.services.updateUser(params).$promise.then((dataReturn) => {
			if(dataReturn.status == 0){
				this.ngNotify.set('Usuario editado correctamente','success');
				this.$state.go("app.users");
			}else{
				this.ngNotify.set('Error ,Usuario editado sin éxito','error');
				this.$scope.isDisabled = false;
			}
		}, (err) => {
			this.ngNotify.set('No se ha podido conectar con el servicio','error');
			this.$scope.isDisabled = false;
		})
	}

}

userEditController.$inject=['$scope','$resource','$timeout','ngNotify','UserInfoConstant','userServices','$state','$location','LOCAL_ENV_CONSTANT']
