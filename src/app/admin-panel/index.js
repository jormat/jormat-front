
// file Route & run app adminPanel
import routing from './adminPanel.route';
import run from './adminPanel.run';

// controllers & services module Users
import userListController from './users/list/userListController';
import userCreateController from './users/create/userCreateController';
import userEditController from './users/edit/userEditController';
import modalDeleteUserController from './users/modal-delete/modalDeleteUserController';
import userServices from './users/userServices';

// controllers & services module Roles
import rolesListController from './roles/list/rolesListController';
import rolesCreateController from './roles/create/rolesCreateController';
import modalDeleteRolesController from './roles/modal-delete/modalDeleteRolesController';
import rolesServices from './roles/rolesServices';

// controllers & services module groups


import modulesListController from './permissions/modules/modulesListController'

import permissionServices from './permissions/permissionServices';
import { listFilter } from '../../components/list-filter'


export default angular.module('app.adminPanel', [userServices,rolesServices,permissionServices])
  .config(routing)
  .controller('userListController', userListController)
  .controller('userCreateController', userCreateController)
  .controller('userEditController', userEditController)
  .controller('modalDeleteUserController', modalDeleteUserController)
  .controller('rolesListController', rolesListController)
  .controller('rolesCreateController', rolesCreateController)
  .controller('modalDeleteRolesController', modalDeleteRolesController)
  .controller('modulesListController', modulesListController)
  .component('listFilter',listFilter)
  .run(run)
  .constant("adminPanel", [{

    "title": "Admin-Panel",
    "icon":"mdi-account-settings-variant",
    "subMenu":[  
      // {  
      //   "title":"USER_ADMIN",
      //   "url":"app.users"
      // },
      {  
        "title":"Roles",
        "url":"app.roles"
      }
    ]    

}])
.name;