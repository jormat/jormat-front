
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> certificatesListController
* # As Controller --> certificatesList														
*/

export default class certificatesListController{

        constructor($scope,UserInfoConstant,$timeout,certificatesServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,warehousesServices,itemsServices,i18nService){
            var vm = this
            vm.createCertificate = createCertificate
            vm.searchData=searchData
            vm.loadcertificates = loadcertificates
            vm.checkRut = checkRut
            $scope.rutValidate = true
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            function checkRut(rut) {
                // Despejar Puntos
                var valor = rut.replace('.','');
                // Despejar Guión
                valor = valor.replace('-','');
                
                // Aislar cuerpo y Dígito Verificador
                $scope.cuerpo = valor.slice(0,-1);
                $scope.dv = valor.slice(-1).toUpperCase();
                
                // Formatear RUN
                rut = $scope.cuerpo + '-'+ $scope.dv
                
                // Si no cumple con el mínimo ej. (n.nnn.nnn)
                if($scope.cuerpo.length < 7) { 
                    // rut.setCustomValidity("RUT Incompleto");
                    $scope.textValidation = "Incompleto";
                    $scope.rutValidate = true;
                    console.log("rut Incompleto");  
                    return false;
                }
                
                // Calcular Dígito Verificador
                $scope.suma = 0;
                $scope.multiplo = 2;
                
                // Para cada dígito del $scope.cuerpo

                for($scope.i=1;$scope.i<=$scope.cuerpo.length;$scope.i++) {
                
                    // Obtener su Producto con el Múltiplo Correspondiente
                    $scope.index = $scope.multiplo * valor.charAt($scope.cuerpo.length -$scope.i);
                    
                    // Sumar al Contador General
                    $scope.suma = $scope.suma + $scope.index;
                    
                    // Consolidar Múltiplo dentro del rango [2,7]
                    if($scope.multiplo < 7) { $scope.multiplo = $scope.multiplo + 1; } else { $scope.multiplo = 2; }
              
                }
                
                // Calcular Dígito Verificador en base al Módulo 11
                $scope.dvEsperado = 11 - ($scope.suma % 11);
                
                // Casos Especiales (0 y K)
                $scope.dv = ($scope.dv == 'K')?10:$scope.dv;
                $scope.dv = ($scope.dv == 0)?11:$scope.dv;
                
                // Validar que el $scope.cuerpo coincide con su Dígito Verificador
                if($scope.dvEsperado != $scope.dv) { 
                    // rut.setCustomValidity("RUT Inválido");
                    console.log("RUT Inválido"); 
                    $scope.textValidation = "RUT Inválido";
                    $scope.rutValidate = true;
                    return false; 
                }
                
                // Si todo sale bien, eliminar errores (decretar que es válido)
                // rut.setCustomValidity('');
                $scope.textValidation = "RUT Válido";
                console.log("RUT valido");
                $scope.rutValidate = false
            }

            loadcertificates()

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            
            $scope.certificatesGrid = {
                enableFiltering: true,
                exporterCsvFilename: 'listado_comprobantes_pagos.csv',
                enableGridMenu: true,
                columnDefs: [
                    { 
                      name:'id',
                      field: 'id',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.certificatesList.viewVoucher(row.entity)">{{row.entity.id}}</a>' +
                                 '</div>' 
                    },
                    { 
                      name: 'Archivo de firma', 
                      field: 'file', 
                      width: '30%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="uppercase" style="font-size: 12px;">{{row.entity.file}}</a>' +
                                 '</div>'   
                    },
                    { 
                      name:'N° Serie',
                      field: 'serialNumber',
                      width: '10%',
                      aggregationType: uiGridConstants.aggregationTypes.sum,
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> <strong>{{row.entity.serialNumber}}</strong></p>' +
                                 '</div>'
                    },
                    
                    { 
                      name:'Password',
                      field: 'password',  
                      width: '10%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<input type="password" ng-model="row.entity.password" class="form-control">' +
                                 '</div>' 
                    },
                    
                    { 
                      name: 'Vencimiento', 
                      field: 'expirationDate',
                      width: '14%',
                      cellTemplate:'<div class="ui-grid-cell-contents">'+
                                 '<p> {{row.entity.expirationDate}}</p>' +
                                 '</div>'
                    }, 
                    // { 
                    //   name: 'Propietario', 
                    //   field: 'owner',
                    //   width: '9%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents">'+
                    //              '<p> {{row.entity.owner}}</p>' +
                    //              '</div>'
                    // },

                    { 
                      name:'Path',
                      field: 'path',  
                      width: '25%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a ng-click="grid.appScope.certificatesList.viewDocument(row.entity)">{{row.entity.path}}</a>' +
                                 '</div>' 
                    }, 
                    // { 
                    //   name: 'Usuario', 
                    //   field: 'userName', 
                    //   width: '10%',
                    //   cellTemplate:'<div class="ui-grid-cell-contents ">'+
                    //              '<p class=" label bg-warning font-italic" style="font-size: 10px;">{{row.entity.userName}}</p>' +
                    //              '</div>'   
                    // },
                    { 
                      name: '',
                      width: '6%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            }

            function loadcertificates(){

                certificatesServices.certificates.getCertificates().$promise.then((dataReturn) => {
                  $scope.certificatesGrid.data = dataReturn.data;
                  console.log('data',$scope.certificatesGrid.data)
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

            }


            function searchData() {
              certificatesServices.certificates.getCertificates().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.certificatesGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
            }


            function createCertificate(value){
              
              if (value == 2) {
                $scope.createCertificate = false

              }else{

                $scope.createCertificate = true
              }
            
              
            }



      


            
        }//FIN CONSTRUCTOR

        // Funciones
        
 

        
    }   

certificatesListController.$inject = ['$scope','UserInfoConstant','$timeout','certificatesServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','warehousesServices','itemsServices','i18nService'];

