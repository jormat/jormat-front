/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> certificatesServices
* file for call at services for certificates
*/

class certificatesServices {

	constructor($resource,SERVICE_URL_CONSTANT) {
		
		var certificates = $resource(SERVICE_URL_CONSTANT.jormat + '/billing/certificates/:route',{},{
			
				getCertificates: {
						method:'GET',
						params : {
							 route: 'list'
						}
				},
				getVoucherDetails: {
						method:'GET',
						params : {
							 route: ''
						}
				},
				createVoucher: {
						method:'POST',
						params : {
								route: ''
						}
				},
				getVouchersItems: {
						method:'GET',
						params : {
							 route: 'payments'
						}
				},
				getVouchersReport: {
						method:'GET',
						params : {
							 route: 'report'
						}
				},
				getSalesByWarehouse: {
						method:'GET',
						params : {
								route: 'sales'
						}
				},
				certificatesUpdate: {
						method:'PUT',
						params : {
								route: 'validate'
						}
				},
				certificatesItemsUpdate: {
						method:'PUT',
						params : {
								route: 'click'
						}
				}
		})

		return { certificates : certificates
						 
					 }
	}
}

	certificatesServices.$inject=['$resource','SERVICE_URL_CONSTANT']

	export default  angular.module('services.certificatesServices', [])
	.service('certificatesServices', certificatesServices)
	.name;