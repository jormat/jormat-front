/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> certificatesViewController
* # As Controller --> certificatesView														
*/


  export default class certificatesViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,certificatesServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this
    	vm.cancel=cancel
    	vm.transformToInvoice = transformToInvoice
      	$scope.voucherId = $scope.certificatesData.id
      	$scope.clientName = $scope.certificatesData.userCreation

	      let params = {
	            voucherId: $scope.voucherId
	          }

	        certificatesServices.certificates.getVoucherDetails(params).$promise.then((dataReturn) => {
	              $scope.voucher = dataReturn.data
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get voucher Details',err);
	              })

	        certificatesServices.certificates.getVouchersItems(params).$promise.then((dataReturn) => {
	              $scope.vouchersItems = dataReturn.data
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio get officePayment items',err);
	              })

	    	function cancel() {
				$modalInstance.dismiss('chao');
			}

			function transformToInvoice() {
              $state.go("app.clientsInvoicesUpdate", { idInvoice: $scope.paymentId,discount:$scope.officePayment[0].discount,type:'cotizacion'});

        }

    }

  }

  certificatesViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','certificatesServices','$location','UserInfoConstant','ngNotify','$state'];
