
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> foliosListController
* # As Controller --> folios														
*/

export default class foliosListController{

        constructor($scope,UserInfoConstant,$timeout,foliosServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            this.$mdDialog = $mdDialog
            var vm = this;
            vm.searchData=searchData;
            vm.deletefolios= deletefolios;
            vm.viewfolios= viewfolios
            vm.bankCreate = bankCreate
            vm.updatebank= updatebank
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.foliosGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_folios.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'loadId',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p>{{row.entity.loadId}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Folios',
                      field: 'folios',
                      width: '38%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 12px;">{{row.entity.folios}}</p>' +
                                 '</div>'
                    },

                    { 
                      name:'Fecha',
                      field: 'date',
                      width: '7%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.date}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Path',
                      field: 'path',
                      width: '22%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="label bg-warning">{{row.entity.path}}</a>' +
                                 '</div>'
                    },
                    { 
                      name:'Formato',
                      field: 'format',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">.{{row.entity.format}}</p>' +
                                 '</div>'
                    },

                    { 
                      name:'Usuario',
                      field: 'userName',
                      width: '12%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secundary">{{row.entity.userName}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: '',
                      width: '8%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
         		foliosServices.folios.getFolios().$promise.then((dataReturn) => {
         		  $scope.foliosGrid.data = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

        function searchData() {
                  foliosServices.folios.getFolios().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.foliosGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }

        function deletefolios(row){
                // $scope.bankData = row;
                // var modalInstance  = $modal.open({
                //         template: require('../delete/folios-delete.html'),
                //         animation: true,
                //         scope: $scope,
                //         controller: 'foliosDeleteController',
                //         controllerAs: 'foliosDelete'
                // })
        }

        function viewfolios(row){
                $scope.bankData = row;
                var modalInstance  = $modal.open({
                        template: require('../view/folios-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'foliosViewController',
                        controllerAs: 'foliosView',
                })
        }

        function bankCreate(){
                // var modalInstance  = $modal.open({
                //         template: require('../create/folios-create.html'),
                //         animation: true,
                //         scope: $scope,
                //         controller: 'foliosCreateController',
                //         controllerAs: 'foliosCreate',
                // })
        }

        function updatebank(row){
            $state.go("app.foliosUpdate", { idbank: row.bankId,name: row.bankName,code:row.bankCode});
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        uploadXML() {
           this.confirm = this.$mdDialog.confirm({
                controller         : 'uploadXMLController',
                controllerAs       : 'uploadXML',
                template           : require('./uploadXML/uploadXML.html'),
                locals              : {},
                animation          : true,
                parent             : angular.element(document.body),
                clickOutsideToClose: true,
            })
            this.$mdDialog.show(this.confirm).then((row) => {
                if (row) {
                   const dataReturn = row.data
                   if(dataReturn.length < 2) {
                       this.loadItemCSV(dataReturn[0])
                   }else {
                       for(const i in dataReturn){
                           this.loadItemCSV(dataReturn[i])
                       }
                   }
                   
                }
            })
        }
        
 

        
    }   

foliosListController.$inject = ['$scope','UserInfoConstant','$timeout','foliosServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

