/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> foliosServices
* file for call at services for folios
*/

class foliosServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var folios = $resource(SERVICE_URL_CONSTANT.jormat + '/billing/folios/:route',{},{
      
      getFolios: {
        method:'GET',
          params : {
            route: 'list'
            }
          },

       createBank: {
        method:'POST',
        params : {
          route: ''
          }
        },
        
        viewBank: {
          method:'GET',
          params : {
            route: ''
            }
          },

        updateBank: {
          method:'PUT',
          params : {
            route: ''
            }
          },

        disabledBank: {
          method:'DELETE',
          params : {
            route: ''
            }
          }
    })

    return { folios : folios
             
           }
  }
}

  foliosServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.foliosServices', [])
  .service('foliosServices', foliosServices)
  .name;