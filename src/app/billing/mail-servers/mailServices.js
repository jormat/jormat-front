/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> mailServices
* file for call at mail Services
*/

class mailServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var mailServers = $resource(SERVICE_URL_CONSTANT.jormat + '/billing/mails-servers/:route',{},{
      
      getMailServers: {
        method:'GET',
          params : {
            route: 'list'
            }
          },

       createBank: {
        method:'POST',
        params : {
          route: ''
          }
        }
    })

    return { mailServers : mailServers
             
           }
  }
}

  mailServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.mailServices', [])
  .service('mailServices', mailServices)
  .name;