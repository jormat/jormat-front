/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> foliosViewController
* # As Controller --> foliosView														
*/


  export default class foliosViewController {

    constructor($scope, $filter,$rootScope, $http,$modalInstance,foliosServices,$location,UserInfoConstant,ngNotify,$state) {

    	var vm = this;
	    vm.cancel = cancel;
	    vm.editBank = editBank;
	    vm.showPanel= showPanel
	    $scope.bankId = $scope.bankData.bankId
	    $scope.bankName = $scope.bankData.description
	    $scope.bankCode = $scope.bankData.code
	    $scope.editView = true

	    let params = {
            bankId: $scope.bankId
          }

        foliosServices.folios.viewBank(params).$promise.then((dataReturn) => {
         		  $scope.bank = dataReturn.data
         		  $scope.bankDetails  = { 
	                   bankCode : $scope.bank[0].code,
	                   bankName : $scope.bank[0].description
                  }

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })

        function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

	    function cancel() {
			console.log('cerrando modal');
			$modalInstance.dismiss('chao');
		}

		function showPanel(value) {

			if (value == 1) {

				$scope.editPanel = true
				$scope.editView = false 
				$scope.buttonSave = true
			}else{

				$scope.editPanel = false
				$scope.editView = true 
				$scope.buttonSave = false

			}
			
		}

		function editBank() {
			
			let params = {
	            bankId: $scope.bankId,
	            bankCode : $scope.bankDetails.bankCode,
		        bankName : $scope.bankDetails.bankName
	           }

	        foliosServices.folios.updateBank(params).$promise.then((dataReturn) => {
	        	    $scope.result = dataReturn.data  
	         		ngNotify.set('Se ha actualizado correctamente el banco','success')     
	         		$state.reload('app.folios')
                    $modalInstance.dismiss('chao');
	              },(err) => {
	                  console.log('No se ha podido conectar con el servicio',err);
	              })
			
		}

		


    }

  }

  foliosViewController.$inject = ['$scope', '$filter','$rootScope', '$http','$modalInstance','foliosServices','$location','UserInfoConstant','ngNotify','$state'];
