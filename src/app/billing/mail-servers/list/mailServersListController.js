
/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> mailServersListController
* # As Controller --> mailServersList														
*/

export default class mailServersListController{

        constructor($scope,UserInfoConstant,$timeout,mailServices,ngNotify,$state,$mdDialog,$interval,uiGridConstants,$http,$filter,$modal,i18nService){
            this.$mdDialog = $mdDialog
            var vm = this;
            vm.searchData=searchData;
            vm.deletefolios= deletefolios;
            vm.viewfolios= viewfolios
            vm.bankCreate = bankCreate
            $scope.mailserver = "Servidor de correo Jormat"
            $scope.port = 8080
            $scope.serverName = "smpt.importadorajormat.cl"
            $scope.user = "jormat"
            vm.updatebank= updatebank
            vm.searchOptions = {
              updateOn: 'default blur',
              debounce:{
                  'default': 100,
                  'blur': 0
                }
            }

            i18nService.setCurrentLang('es');

            $scope.dropdownMenu = require('./dropdownActionsMenu.html')
            

            $scope.mailServersGrid = {
                enableFiltering: true,
                enableGridMenu: true,
                exporterCsvFilename: 'Listado_de_folios.csv',
                columnDefs: [
                    { 
                      name:'id',
                      field: 'id',  
                      width: '5%',
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p>{{row.entity.id}}</p>' +
                                 '</div>'  
                    },
                    { 
                      name:'Descripción',
                      field: 'description',
                      width: '22%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase text-muted" style="font-size: 12px;">{{row.entity.description}}</p>' +
                                 '</div>'
                    },

                    { 
                      name:'Tipo',
                      field: 'type',
                      width: '10%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="uppercase">{{row.entity.type}}</p>' +
                                 '</div>'
                    },
                    { 
                      name:'Puerto',
                      field: 'port',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<a class="label bg-warning">{{row.entity.port}}</a>' +
                                 '</div>'
                    },
                    { 
                      name:'Nombre servidor',
                      field: 'serverName',
                      width: '26%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p>{{row.entity.serverName}}</p>' +
                                 '</div>'
                    },

                    { 
                      name:'SSL/TLS',
                      field: 'certification',
                      width: '10%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secundary">{{row.entity.certification}}</p>' +
                                 '</div>'
                    },

                    { 
                      name:'Fecha',
                      field: 'date',
                      width: '8%' ,
                      cellTemplate:'<div class="ui-grid-cell-contents ">'+
                                 '<p class="label bg-secundary">{{row.entity.date}}</p>' +
                                 '</div>'
                    },
                    { 
                      name: '',
                      width: '8%', 
                      field: 'href',
                      enableFiltering: false,
                      cellTemplate: $scope.dropdownMenu
                    }
                ]
            };
            
          
         		mailServices.mailServers.getMailServers().$promise.then((dataReturn) => {
         		  $scope.mailServersGrid.data = dataReturn.data;
                  },(err) => {
                      console.log('No se ha podido conectar con el servicio',err);
                  })

        function searchData() {
                  mailServices.mailServers.getMailServers().$promise
                  .then(function(data){
                      $scope.data = data.data;
                      $scope.mailServersGrid.data = $filter('filter')(data.data, vm.searchText, undefined);
                  });
                }

        function deletefolios(row){
                // $scope.bankData = row;
                // var modalInstance  = $modal.open({
                //         template: require('../delete/folios-delete.html'),
                //         animation: true,
                //         scope: $scope,
                //         controller: 'foliosDeleteController',
                //         controllerAs: 'foliosDelete'
                // })
        }

        function viewfolios(row){
                $scope.bankData = row;
                var modalInstance  = $modal.open({
                        template: require('../view/folios-view.html'),
                        animation: true,
                        scope: $scope,
                        controller: 'foliosViewController',
                        controllerAs: 'foliosView',
                })
        }

        function bankCreate(){
                // var modalInstance  = $modal.open({
                //         template: require('../create/folios-create.html'),
                //         animation: true,
                //         scope: $scope,
                //         controller: 'foliosCreateController',
                //         controllerAs: 'foliosCreate',
                // })
        }

        function updatebank(row){
            $state.go("app.foliosUpdate", { idbank: row.bankId,name: row.bankName,code:row.bankCode});
        }

            
        }//FIN CONSTRUCTOR

        // Funciones
        // uploadXML() {
        //    this.confirm = this.$mdDialog.confirm({
        //         controller         : 'uploadXMLController',
        //         controllerAs       : 'uploadXML',
        //         template           : require('./uploadXML/uploadXML.html'),
        //         locals              : {},
        //         animation          : true,
        //         parent             : angular.element(document.body),
        //         clickOutsideToClose: true,
        //     })
        //     this.$mdDialog.show(this.confirm).then((row) => {
        //         if (row) {
        //            const dataReturn = row.data
        //            if(dataReturn.length < 2) {
        //                this.loadItemCSV(dataReturn[0])
        //            }else {
        //                for(const i in dataReturn){
        //                    this.loadItemCSV(dataReturn[i])
        //                }
        //            }
                   
        //         }
        //     })
        // }
        
 

        
    }   

mailServersListController.$inject = ['$scope','UserInfoConstant','$timeout','mailServices','ngNotify','$state','$mdDialog','$interval','uiGridConstants','$http','$filter','$modal','i18nService'];

