

import routing from './billing.route';
import run from './billing.run';

//certificates files
import certificatesListController from './certificates/list/certificatesListController'; 
import certificatesViewController from './certificates/view/certificatesViewController';
import certificatesServices from './certificates/certificatesServices';

//folios
import foliosListController from './folios/list/foliosListController';
import uploadXMLController from './folios/list/uploadXML/uploadXMLController';
import foliosViewController from './folios/view/foliosViewController'; 
import foliosServices from './folios/foliosServices';

//company data
import companyDataController from './company-data/view/companyDataController'; 
import companyServices from './company-data/companyServices';

//mail services
import mailServersListController from './mail-servers/list/mailServersListController'; 
import mailServices from './mail-servers/mailServices';


export default angular.module('app.billing', [certificatesServices,foliosServices,companyServices,mailServices])
  .config(routing)
  .controller('certificatesListController', certificatesListController)
  .controller('certificatesViewController', certificatesViewController)
  .controller('foliosListController', foliosListController)
  .controller('foliosViewController', foliosViewController)
  .controller('uploadXMLController', uploadXMLController) 
  .controller('companyDataController', companyDataController)
  .controller('mailServersListController', mailServersListController)
  .run(run)
  .constant("billing", [{
        "title": "DTE's",
        "icon":"mdi-server-security",
        "subMenu":[
            {
            'title' : 'Folios',
            'url': 'app.folios'
            },
            {
            'title' : 'Certificados',
            'url': 'app.certificates'
            },
            {
            'title' : 'Datos empresa',
            'url': 'app.companyData'
            },
            {
            'title' : 'Servidor de Correo',
            'url': 'app.mailServers'
            }
            
        ]
    }])
  .name;