
routes.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider'];

export default function routes($stateProvider, $urlRouterProvider, $locationProvider) {
      $locationProvider.html5Mode(true);
      //$urlRouterProvider.otherwise('/');

  $stateProvider
     .state('app.folios', {
            abstract: false,
            url: '/documentacion-tributaria/folios',
            template: require('./folios/list/folios-list.html'),
            controller: 'foliosListController',
            controllerAs:'foliosList'
      })

     .state('app.foliosView', {
            abstract: false,
            url: '/documentacion-tributaria/folios/ver',
            template: require('./folios/view/folios-view.html'),
            controller: 'foliosViewController',
            controllerAs:'foliosView'
      })

     .state('app.certificates', {
            abstract: false,
            url: '/documentacion-tributaria/certificados',
            template: require('./certificates/list/certificates-list.html'),
            controller: 'certificatesListController',
            controllerAs:'certificatesList'
      })

     .state('app.certificatesView', {
            abstract: false,
            url: '/documentacion-tributaria/certificados/ver',
            template: require('./certificates/view/certificates-view.html'),
            controller: 'certificatesViewController',
            controllerAs:'certificatesView'
      })

     .state('app.companyData', {
            abstract: false,
            url: '/documentacion-tributaria/empresa',
            template: require('./company-data/view/company-data.html'),
            controller: 'companyDataController',
            controllerAs:'companyData'
      })

     .state('app.mailServers', {
            abstract: false,
            url: '/documentacion-tributaria/servidores-correo',
            template: require('./mail-servers/list/mail-servers-list.html'),
            controller: 'mailServersListController',
            controllerAs:'mailServersList'
      })
 
}
