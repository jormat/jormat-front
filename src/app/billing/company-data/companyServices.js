/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name Services --> companyServices
* file for call at services for company data
*/

class companyServices {

  constructor($resource,SERVICE_URL_CONSTANT) {
    
    var company = $resource(SERVICE_URL_CONSTANT.jormat + '/billing/company/:route',{},{
      
      getCompanyData: {
        method:'GET',
          params : {
            route: ''
            }
          },

       economicActivities: {
        method:'GET',
        params : {
          route: 'activities'
          }
        },
        
        viewBank: {
          method:'GET',
          params : {
            route: ''
            }
          },

        updateBank: {
          method:'PUT',
          params : {
            route: ''
            }
          },

        disabledBank: {
          method:'DELETE',
          params : {
            route: ''
            }
          }
    })

    return { company : company
             
           }
  }
}

  companyServices.$inject=['$resource','SERVICE_URL_CONSTANT']

  export default  angular.module('services.companyServices', [])
  .service('companyServices', companyServices)
  .name;