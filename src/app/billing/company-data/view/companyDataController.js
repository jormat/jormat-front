/**
* @name APP jormat
* @autor jsalazar@importadorajormat.cl
* @description
* # name controller --> companyDataController
* # As Controller --> companyData														
*/


  export default class companyDataController {

    constructor($scope, $filter,$rootScope, $http,companyServices,$location,ngNotify,$state,warehousesServices,$stateParams) {

    	var vm = this;
	    // vm.setRole = setRole;
       


        companyServices.company.getCompanyData().$promise.then((dataReturn) => {
                  $scope.company = dataReturn.data
                   console.log('services',$scope.company)
                  $scope.companyDetails  = { 
                        id : $scope.company[0].id,
                        businessName : $scope.company[0].businessName,
                        address : $scope.company[0].address,
                        cityName : $scope.company[0].cityName,
                        country : $scope.company[0].country,
                        serialNumber : $scope.company[0].serialNumber,
                        type : $scope.company[0].type,
                        description : $scope.company[0].description,
                        currency : $scope.company[0].currency,
                        phone : $scope.company[0].phone,
                        cellPhone : $scope.company[0].cellPhone,
                        email : $scope.company[0].email,
                        web : $scope.company[0].web,
                        dteEmail : $scope.company[0].dteEmail,
                        environmentServices : $scope.company[0].environmentServices,
                        generalOffice : $scope.company[0].generalOffice,
                        resolutionNumber : $scope.company[0].resolutionNumber,
                        resolutionDate : $scope.company[0].resolutionDate,
                        activities:  []
                  }

                  companyServices.company.economicActivities().$promise.then((dataReturn) => {
                      $scope.activities = dataReturn.data;
                      console.log("activities",$scope.activities);

                      for(var  i in $scope.activities){
                            $scope.companyDetails.activities.push({
                              'descriptionName': $scope.activities[i].descriptionName,
                              'code': $scope.activities[i].code 
                          })
                      }

                      console.log('push',$scope.companyDetails.activities)

                      },(err) => {
                          console.log('No se ha podido conectar con el servicio',err);
                   })

              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
              })


        

        warehousesServices.warehouses.getWarehouses().$promise.then((dataReturn) => {
              $scope.warehousesAll = dataReturn.data;
              },(err) => {
                  console.log('No se ha podido conectar con el servicio',err);
            })

        


           

            

    }

  }

  companyDataController.$inject = ['$scope', '$filter','$rootScope', '$http','companyServices','$location','ngNotify','$state','warehousesServices','$stateParams'];
