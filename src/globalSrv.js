class GlobalSrv {
	constructor ($resource, SERVICE_URL_CONSTANT, $cookies) {

		var localenv=require('../local.env.json')

		var userLogin = $resource(localenv.USER_API_URL+ '/api/user-api', {}, {
			POST: {
				method: 'POST'
			}
		})

	   var getUserInfo = $resource(localenv.USER_API_URL+ '/api/user-api/getUserInfo', {}, {
			POST: {
				method: 'POST'
			}
		})

		return {
			userLogin: userLogin,
			getUserInfo:getUserInfo
		}
	}
}
GlobalSrv.$inject = ['$resource', 'SERVICE_URL_CONSTANT','$cookies']
export default angular.module('services.GlobalSrv', []).service('GlobalSrv', GlobalSrv).name
